<?php
class Videos_Model_Videocomments {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Videos_Model_DbTable_Videocomments');
                }
                return $this->_dbTable;
        }

        public function savevideocomment($data) {
                return $this->getDbTable()->insert($data);
        }

}