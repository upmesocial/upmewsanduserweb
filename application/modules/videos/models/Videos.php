<?php
class Videos_Model_Videos {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Videos_Model_DbTable_Videos');
                }
                return $this->_dbTable;
        }

        public function insertVideo($data) {
                return $this->getDbTable()->insert($data);
        }

        //update video status 
        public function updateVideoStatus($updateAry,$jobid)
        {
            if(!empty($jobid) && $jobid != '0') {
                    try{
                            $res = $this->getDbTable()->update($updateAry, array('zencoder_job_id= ?' => $jobid));
                            if (false === $res) {
                                return 0;  // bool false returned, query failed
                            } else {
                                return 1;
                            }
                    } catch (Zend_Exception $zex){}
                }
        }

        public function getvideos($user_id) {
                /*$select    = $this->getDbTable()->select()
                                ->where("user_id =?",$user_id);*/
                $db = Zend_Db_Table::getDefaultAdapter();
                $select    = $db->select()
                            ->from(array('V'=>'tbl_videos'))
                            ->joinLeft(array('C'=>'tbl_video_comments'), 'C.video_id = V.video_id',array('cnt'=>'count(C.video_id)'))
                            ->where("V.user_id =?",$user_id)
                            ->group('V.video_id')
                            ->order('V.video_id ASC');
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getvideosbyvideoid($videoid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select    = $this->getDbTable()->select()
                                ->where("video_id =?",$videoid);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function getvideowithcomments($video_id,$limit='100',$userid,$user_type) {
                $select = $this->getDbTable()->select()
                                ->from(array('V'=>'tbl_videos'),array('video_id','video_name','thumbnail_path','user_id'))
                                ->joinLeft(array('C'=>'tbl_video_comments'),'C.video_id = V.video_id',array('user_posted'=>'user_id','comments','commented_date','user_type'))
                                ->joinLeft(array('U'=>'tbl_users'),'U.user_id = C.user_id AND C.user_type="U"',array('user_image'=>'profile_pic_path','username','firstname','lastname'))
                                ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = C.user_id AND C.user_type="B"',array('business_image'=>'image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname'))
                                ->joinLeft(array('V1'=>'tbl_videos'),'(V1.blast_id = V.video_id AND V1.user_id = "'.$userid.'" AND V1.user_type="'.$user_type.'")',array('video_id as isBlastedVideo'))
                                ->where("V.video_id =?",$video_id)
                                ->order("C.comment_id DESC")
                                ->limit($limit)
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getVideoCommentCnt($video_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C'=>'tbl_video_comments'),array('COUNT' =>"COUNT('C.video_id')"))
                                ->where("C.video_id =?",$video_id);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function deleteVideo($video) {
                @unlink(UPLOAD_PATH."videos/".$video['video_name']);
                @unlink(UPLOAD_PATH."videos/".$video['thumbnail_path']);
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE V.*,C.*,N.* FROM tbl_videos V
                                    LEFT JOIN tbl_video_comments C ON C.video_id = V.video_id
                                    LEFT JOIN tbl_notifications N ON (N.reference_id = V.video_id AND N.notifications_type = 'video_uploads')
                                    WHERE V.video_id = ".$video['video_id']);
                $id = $select->execute();
                return $id;
        }

        public function isAvailable($logged_User_Id,$logged_User_Type,$video_id) {
                $select = $this->getDbTable()->select()
                                ->from('tbl_videos',array('video_id'))
                                ->where('user_id = ?', $logged_User_Id)
                                ->where('user_type = ?', $logged_User_Type)
                                ->where('blast_id = ?',$video_id);
                //echo $select;exit;
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getdaycount($loggedUserId, $loggedUserType) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from('tbl_videos', array('COUNT(video_id) as count'))
                                ->where('user_id = ?',$loggedUserId)
                                ->where('user_type = ?',$loggedUserType)
                                ->where('blasted_date >= DATE(NOW())');
                //echo $select;exit;
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function blastVideo($dataAry) {
                $db = Zend_Db_Table::getDefaultAdapter();
                // for Blasting Photo
                /*$inserted = $db->insert('tbl_videos',$dataAry);
                return $inserted;*/
                return $this->getDbTable()->insert($dataAry);
        }

        public function getBusinessVideosByUserId($userid, $usertype) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                ->from(array('V'=>'tbl_videos'))
                                //->joinLeft(array('U'=>'tbl_users'),'U.user_id = V.user_id AND V.user_type="U"',array('user_image'=>'profile_pic_path','username','firstname','lastname'))
                                ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = V.user_id AND V.user_type="B"',array('business_user_image'=>'image_path','username','firstname','lastname'))
                                ->where("V.user_id =?", $userid)
                                ->where("V.user_type =?", $usertype)
                                ->setIntegrityCheck(false);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

}