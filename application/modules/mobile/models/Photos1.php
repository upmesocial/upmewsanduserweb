<?php
class Mobile_Model_Photos {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Mobile_Model_DbTable_Photos');
                }
                return $this->_dbTable;
        }

        //function to insert album details
        public function insert($data) {
                return $this->getDbTable()->insert($data);
        }

        public function update($data, $albumid) {
                if(!empty($albumid)) {
                        try {
                                $res = $this->getDbTable()->update($data, array('album_id = ?' => $albumid));
                                if (false === $res) {
                                    return 0;  // bool false returned, query failed
                                } else {
                                    return 1;
                                }
                        } catch (Zend_Exception $zex){}
                }
        }

        public function getalbumphotos($albumid,$userid,$user_type='U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('P'=>'tbl_photos'))
                                ->joinLeft(array('C'=>'tbl_photo_comments'), 'C.photo_id = P.photo_id',array('cnt'=>'count(C.photo_id)'))
                                ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$userid.'" AND P1.user_type="'.$user_type.'")',array('photo_id as isBlastedPhoto'))
                                ->where("P.album_id =?",$albumid)
                                ->where("P.blast_id = '0'")
                                ->group('P.photo_id')
                                ->order('P.photo_id ASC');
                $resultSet = $db->fetchAll($select);
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
                    //echo "<pre>";print_r($resultSet);exit;
                }
                if(count($resultSet) > 0) {
                    foreach($resultSet as $key => $photo) { 
                        if($photo['cnt'] > 0) {
                            $photoCmntSelect = $db->select()
                                    ->from(array('PC'=>'tbl_photo_comments'),array('PC.comments','PC.commented_date'))
                                    ->joinLeft(array('U' => 'tbl_users'), 'U.user_id = PC.user_id AND PC.user_type = "U"', array('commenter_id'=>'user_id','commenter_type'=>'user_type','profile_pic_path','commenter_name'=>'CONCAT(U.firstname, " ", U.lastname)'))
                                    ->joinLeft(array('B' => 'tbl_business_users'), 'B.business_id = PC.user_id AND PC.user_type = "B"', array('bcommenter_id'=>'business_id','bcommenter_type'=>'user_type','image_path','bcommenter_name'=>'CONCAT(B.firstname, " ", B.lastname)'))
                                    ->where("PC.photo_id =?",$photo['photo_id'])
                                    ->order('PC.commented_date DESC')
                                    ->limit(2);
                            $photoCommentsArr = array();
                            $photoCommentsArr = $db->fetchAll($photoCmntSelect);
							//echo '<pre>';print_r($photoCommentsArr);exit;
                            if(count($photoCommentsArr) > 0) {
                                $i =1;
                                foreach($photoCommentsArr as $comment){
                                   	//echo "<pre>";print_r($comment);exit;   
                                    $cmntIndex = 'comment'.$i;
                                    $cmterIdIndex = 'commenter_id'.$i;
                                    $cmterTypeIndex = 'commenter_type'.$i;
                                    $cmterPhotoIndex = 'profile_pic_path'.$i;
                                    $cmterNameIndex = 'commenter_name'.+$i;
                                    $cmtDateIndex = 'commented_date'.+$i;
                                    if($comment['comments'] != '') {
                                        $resultSet[$key][$cmntIndex]=$comment['comments'];
                                        $resultSet[$key][$cmtDateIndex]=$comment['commented_date'];
                                        if($comment['commenter_type'] != '') {
                                                $resultSet[$key][$cmterIdIndex]=$comment['commenter_id'];
                                                $resultSet[$key][$cmterTypeIndex]='U';
                                                $resultSet[$key][$cmterPhotoIndex]=$comment['profile_pic_path'];
                                                $resultSet[$key][$cmterNameIndex]=$comment['commenter_name'];
                                        }
                                        if($comment['bcommenter_type'] != '') {
                                                $resultSet[$key][$cmterIdIndex]=$comment['bcommenter_id'];
                                                $resultSet[$key][$cmterTypeIndex]='B';
                                                $resultSet[$key][$cmterPhotoIndex]=$comment['image_path'];
                                                $resultSet[$key][$cmterNameIndex]=$comment['bcommenter_name'];
                                        }
                                    }
                                    $i++;
                                }
                            }
                        }
                    }
                }
                return $resultSet;
        }

        public function deletePhoto($photoid) {
                $Photos = $this->getphotobyphotoid($photoid);
                foreach($Photos as $key=>$val) {
                        @unlink(UPLOAD_PATH."images/thumbnails/".$val['photo_modified_name']);
                        @unlink(UPLOAD_PATH."images/original/".$val['photo_modified_name']);
                        @unlink(UPLOAD_PATH."images/medium/".$val['photo_modified_name']);
                    }
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE P.*,C.*,N.* FROM tbl_photos P
                                    LEFT JOIN tbl_photo_comments C ON C.photo_id = P.photo_id
                                    LEFT JOIN tbl_notifications N ON (N.reference_id = P.photo_id AND N.notifications_type = 'photo_uploads')
                                    WHERE P.photo_id = ".$photoid);
                $id = $select->execute();
                return $id;
        }

        public function getphotobyphotoid($photoid) {
                $select = $this->getDbTable()->select()
                                ->where("photo_id =?",$photoid);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getphotosbyuserid($userid,$usertype) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                ->where("user_id ='".$userid."' AND user_type ='".$usertype."'");
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function albumidexistsrnot($album_id,$user_id,$user_type) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('tbl_photos'),array('photo_id'))
                            ->where("album_id =?",$album_id)
                            ->where("user_id =?",$user_id)
                            ->where("user_type =?",$user_type);
                $resultSet  = $db->fetchRow($select);
                return $resultSet;
        }

        public function getphotosbyuserids($ids,$types) {
                $db = Zend_Db_Table::getDefaultAdapter();
                //echo "SELECT P.* FROM tbl_photos P WHERE (P.user_id IN ('".$ids."') AND P.user_type IN ('".$types."'))";exit;
                $select = $db->query("SELECT P.* FROM tbl_photos P WHERE (P.user_id IN ('".$ids."') AND P.user_type IN ('".$types."'))");
                $resultSet = $select->execute();
                return $resultSet;
        }

        public function getphotocomments($photo_id,$limit='100') {
                $select = $this->getDbTable()->select()
                                ->from(array('C'=>'tbl_photo_comments'), array('user_posted'=>'user_id','comments','commented_date','user_type'))
                                ->joinLeft(array('U'=>'tbl_users'),'U.user_id = C.user_id AND C.user_type="U"',array('profile_pic_path','username','realname'=>'CONCAT(U.firstname, " ", U.lastname)','firstname','lastname'))
                                ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = C.user_id AND C.user_type="B"',array('image_path','business_username'=>'business_name','business_firstname'=>'firstname','business_lastname'=>'lastname'))
                                ->where("C.photo_id =?",$photo_id)
                                ->order("C.comment_id ASC")
                                ->limit($limit)
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getPhotoDetailsByPhotoId($userid, $user_type,$photo_id) {
             $select = $this->getDbTable()->select()
                                ->from(array('P'=>'tbl_photos'),array('photo_id','album_id','photo_name','description','photo_original_name','photo_modified_name','user_id','user_type','width','height','android_width','android_height'))
                                ->joinLeft(array('U'=>'tbl_users'),'U.user_id = P.user_id AND P.user_type="U"',array('profile_pic_path','username','realname'=>'CONCAT(U.firstname, " ", U.lastname)','firstname','lastname'))
                                ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$userid.'" AND P1.user_type="'.$user_type.'")',array('photo_id as isBlastedPhoto'))
                                ->where("P.photo_id =?",$photo_id)
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

//        public function getTimelineClickedImagesDetails($userId,$userType = 'U') {
//            /*
//             * SELECT   ID, date, name, type, userID, userType
//                    FROM (SELECT   photo_id ID, created_date date, photo_modified_name name, 'photo' type, user_id userID, user_type userType
//                          FROM tbl_photos WHERE photo_modified_name != ''
//                          UNION
//                          SELECT   video_id ID, created_date date, thumbnail_path name, 'video' type, user_id userID, 'U' userType
//                          FROM tbl_videos WHERE thumbnail_path != ''
//                          ORDER BY date DESC) AS A
//                   LEFT JOIN tbl_followers F ON F.follower_id = "1" AND F.follower_type = "U" AND F.user_id = A.userID AND F.user_type = A.userType
//                   WHERE F.follower_id = "1" AND F.follower_type = "U" AND F.user_id = A.userID AND F.user_type = A.userType
//                ORDER BY date DESC
//                LIMIT 30
//             */
//            $date = date('Y-m-d H:i:s');
//            
//                $db = Zend_Db_Table::getDefaultAdapter();
//                $select = "SELECT   ID, date, name, type, Path, userID, userType, BlastId, U.profile_pic_path, CONCAT(U.firstname, ' ', U.lastname) realname,U.username, BlastedDate
//                            FROM (SELECT   photo_id ID, created_date date, photo_modified_name name, 'photo' type, user_id userID, photo_modified_name Path, user_type userType, blast_id BlastId, blasted_date BlastedDate
//                                    FROM tbl_photos WHERE photo_modified_name != ''
//                                    UNION
//                                    SELECT   video_id ID, created_date date, thumbnail_path name, 'video' type, user_id userID, video_path Path,  'U' userType, blast_id BlastId, blasted_date BlastedDate
//                                    FROM tbl_videos WHERE thumbnail_path != ''
//                                    ORDER BY date DESC) AS A
//                            LEFT JOIN tbl_followers F ON F.user_id = '".$userId."' AND F.user_type = '".$userType."' AND F.follower_id = A.userID AND F.follower_type = A.userType
//                            LEFT JOIN tbl_users U ON U.user_id = userID
//                            WHERE ((F.user_id = '".$userId."' AND F.user_type = '".$userType."' AND F.follower_id = A.userID AND F.follower_type = A.userType) OR ('".$userId."' = A.userID AND '".$userType."' = A.userType)) AND date >= '".$date."' - INTERVAL 1 DAY
//                            ORDER BY date DESC
//                            LIMIT 30";
//
//                $resultSet = $db->fetchAll($select);
//                if($_SERVER['REMOTE_ADDR'] == '192.168.1.63') {
//                    //echo "<pre>".count($resultSet);print_r($resultSet);exit;
//                }
//                
//                foreach($resultSet as $key=>$det){
//                    if($det['type'] == 'photo'){
//                        
//                        if($_SERVER['REMOTE_ADDR'] == '192.168.1.63') {
//                            //echo "<pre>";print_r($det);exit;
//                            if($resultSet[$key]['BlastId'] != 0) {
//                                //echo $resultSet[$key]['BlastId'];exit;
//                                
//                                // to get all Blasted USers Based on Photo ID
//                                $blasedUsers = array();
//                                $blasedUsers = $this->getBlastedUserDetailsBasedOnPhotoId($resultSet[$key]['BlastId']);
//                                //echo "<pre>";print_r($blasedUsers);//exit;
//                                $resultSet[$key]['blastedUsers'] = $blasedUsers;
//                            }
//                        }
//                        
//                        
//                        // TO GET PHOTO COMMENTS
//                        $photoCmntSelect = $db->select()
//                                ->from(array('P'=>'tbl_photos'))
//                                ->joinLeft(array('C'=>'tbl_photo_comments'), 'C.photo_id = P.photo_id',array('cnt'=>'count(C.photo_id)'))
//                                ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$userId.'" AND P1.user_type="'.$userType.'")',array('P1.photo_id as isBlasted'))
//                                ->where("P.photo_id =?",$det['ID']);
//                        $photoCommentsArr = array();
//                        $photoCommentsArr = $db->fetchAll($photoCmntSelect);
//                        $resultSet[$key]['cmntCnt'] = $photoCommentsArr[0]['cnt'];
//                        $resultSet[$key]['isBlasted'] = $photoCommentsArr[0]['isBlasted'];
//                        //echo "<pre>";print_r($resultSet[$key]);exit;
//                        if($resultSet[$key]['cmntCnt'] > 0) {
//                            $photoCmntSelect = $db->select()
//                                    ->from(array('PC'=>'tbl_photo_comments'),array('PC.comments','PC.commented_date'))
//                                    ->joinLeft(array('U'=>'tbl_users'),'U.user_id = PC.user_id AND PC.user_type="U"',array('profile_pic_path','username','firstname','lastname', 'user_id', 'usertype'=>'user_type'))
//                                    ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = PC.user_id AND PC.user_type="B"',array('image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname', 'business_id', 'B_usertype'=>'user_type'))
//                                    ->where("PC.photo_id =?",$resultSet[$key]['ID'])
//                                    ->order('PC.commented_date DESC')
//                                    ->limit(2);
//                            $photoCommentsArr = array();
//                            $photoCommentsArr = $db->fetchAll($photoCmntSelect);
//                            if(count($photoCommentsArr) > 0) {
//                                $i =1;
//                                foreach($photoCommentsArr as $comment){
//                                    //echo "<pre>";print_r($comment);exit;
//                                    $cmntIndex = 'comment'.$i;
//                                    $name = 'commenter_name'.$i;
//                                    $id = 'commenter_id'.$i;
//                                    $useType = 'commenter_type'.$i;
//                                    $pic_path = 'profile_pic_path'.$i;
//                                    $date = 'commenter_date'.$i;
//                                    if($comment['comments'] != '')   {
//                                        $resultSet[$key][$cmntIndex]=$comment['comments'];
//                                        if($comment['usertype'] != ''){
//                                            $resultSet[$key][$id]=$comment['user_id'];
//                                            $resultSet[$key][$useType]='U';
//                                            $resultSet[$key][$pic_path]=$comment['profile_pic_path'];
//                                            $resultSet[$key][$name]=$comment['firstname']." ".$comment['lastname'];
//                                            $resultSet[$key][$date]=$comment['commented_date'];
//                                        }
//                                        if($comment['B_usertype'] != ''){
//                                            $resultSet[$key][$id]=$comment['business_id'];
//                                            $resultSet[$key][$useType]="B";
//                                            $resultSet[$key][$pic_path]=$comment['image_path'];
//                                            $resultSet[$key][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
//                                            $resultSet[$key][$date]=$comment['commented_date'];
//                                        }
//                                    }
//                                    $i++;
//                                }
//                                
//                            }
//                        }
//                    }
//                    if($det['type'] == 'video'){
//                        $videoCmntSelect = $db->select()
//                                ->from(array('V'=>'tbl_videos'))
//                                ->joinLeft(array('C'=>'tbl_video_comments'), 'C.video_id = V.video_id',array('cnt'=>'count(C.video_id)'))
//                                ->joinLeft(array('V1'=>'tbl_videos'),'(V1.blast_id = V.video_id AND V1.user_id = "'.$userId.'" AND V1.user_type="'.$userType.'")',array('V1.video_id as isBlasted')) 
//                                ->where("V.video_id =?",$det['ID']);
//                 
//                        $videoCommentsArr = array();
//                        $videoCommentsArr = $db->fetchAll($videoCmntSelect);
//                        $resultSet[$key]['cmntCnt'] = $videoCommentsArr[0]['cnt'];
//                        $resultSet[$key]['isBlasted'] = $videoCommentsArr[0]['isBlasted'];
//                        //echo "<pre>";print_r($resultSet);exit;
//                        if($resultSet[$key]['cmntCnt'] > 0) {
//                            $videoCmntSelect = $db->select()
//                                    ->from(array('VC'=>'tbl_video_comments'),array('VC.comments', 'VC.commented_date'))
//                                   ->joinLeft(array('U'=>'tbl_users'),'U.user_id = VC.user_id AND VC.user_type="U"',array('profile_pic_path','username','firstname','lastname', 'user_id', 'usertype'=>'user_type'))
//                                    ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = VC.user_id AND VC.user_type="B"',array('image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname', 'business_id', 'B_usertype'=>'user_type'))
//                                    ->where("VC.video_id =?",$resultSet[$key]['ID'])
//                                    ->order('VC.commented_date DESC')
//                                    ->limit(2);
//                            //echo $videoCmntSelect;exit;
//                            $videoCommentsArr = array();
//                            $videoCommentsArr = $db->fetchAll($videoCmntSelect);
//                            if(count($videoCommentsArr) > 0) {
//                                $i =1;
//                                foreach($videoCommentsArr as $key2=>$comment){
//                                    $cmntIndex = 'comment'.$i;
//                                    $name = 'commenter_name'.$i;
//                                    $id = 'commenter_id'.$i;
//                                    $useType = 'commenter_type'.$i;
//                                    $pic_path = 'profile_pic_path'.$i;
//                                    $date = 'commenter_date'.$i;
//                                        if($comment['comments'] != '')   {
//                                            $resultSet[$key][$cmntIndex]=$comment['comments'];
//                                            if($comment['usertype'] != ''){
//                                                $resultSet[$key][$id]=$comment['user_id'];
//                                                $resultSet[$key]['user_type']="U";
//                                                $resultSet[$key][$pic_path]=$comment['profile_pic_path'];
//                                                $resultSet[$key][$name]=$comment['firstname']." ".$comment['lastname'];
//                                                $resultSet[$key][$date]=$comment['commented_date'];
//                                            }
//                                            if($comment['B_usertype'] != ''){
//                                                $resultSet[$key][$id]=$comment['business_id'];
//                                                $resultSet[$key][$useType]="B";
//                                                $resultSet[$key][$pic_path]=$comment['image_path'];
//                                                $resultSet[$key][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
//                                                $resultSet[$key][$date]=$comment['commented_date'];
//                                            }
//                                    	}
//                                    $i++;
//                                }
//                            }
//                        }
//                    }
//                }
//                if($_SERVER['REMOTE_ADDR'] == '192.168.1.63') {
//                    echo "<pre>";print_r($resultSet);exit;
//                }
//                return $resultSet;
//        }

        public function getBlastedUserDetailsBasedOnPhotoId($photo_id, $logged_user_id, $limit='') {
             $db = Zend_Db_Table::getDefaultAdapter();
//             $select = $db->select()
//                                ->from(array('P' => 'tbl_photos'), array('photo_id'))
//                                ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array('user_id', 'username','realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username'))
////                                ->joinLeft(array('B' => 'tbl_business_users'), "P.user_id = B.business_id AND P.user_type = 'B'", array('business_id','b_username'=> 'username', 'b_firstname' =>'firstname', 'b_lastname'=>'lastname', 'image_path'))
//                                ->where("P.blast_id ='".$photo_id."'")
//                                ->order(array('P.created_date DESC'));
//             if($limit != ''){
//                 $select->limit($limit);
//             }
//             $resultset = $db->fetchAll($select);   
                 $qry = "SELECT `P`.`photo_id`, `U`.`user_id`, `U`.`username`, CONCAT(U.firstname, ' ', U.lastname) AS `realname`, `U`.`firstname`, `U`.`lastname`, `U`.`profile_pic_path`, `U`.`username` AS `uname`, if(U.user_id = '".$logged_user_id."', 1, 0) AS orderflag FROM `tbl_photos` AS `P` LEFT JOIN `tbl_users` AS `U` ON P.user_id = U.user_id AND P.user_type = 'U' WHERE (P.blast_id ='".$photo_id."') ORDER BY orderflag DESC,`P`.`created_date` DESC";
                 if($limit != '')   {
                    $qry .= " LIMIT ".$limit;
                   }
                 $select = $db->query($qry);
                 $resultset = $select->fetchAll();
                return $resultset;
        }

        public function getBlastedUserCountBasedOnPhotoId($photo_id) {
             $db = Zend_Db_Table::getDefaultAdapter();
             $select = $db->select()
                                ->from(array('P' => 'tbl_photos'), array('photoBlastUserCount'=>'COUNT(photo_id)'))
                                ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array(''))
//                                ->joinLeft(array('B' => 'tbl_business_users'), "P.user_id = B.business_id AND P.user_type = 'B'", array(''))
                                ->where("P.blast_id ='".$photo_id."'")
                                ->order(array('P.created_date DESC'));
             //echo $select;//exit;
                $resultset = $db->fetchAll($select);     
                return $resultset[0]['photoBlastUserCount'];
        }

        public function makeProfilePicture($user_id, $photo_id) {
             //echo $photo_id;exit;
             $db = Zend_Db_Table::getDefaultAdapter();
             $photoDetails = $this->getphotobyphotoid($photo_id);
             //echo "<pre>";print_r($photoDetails->photo_modified_name);exit;
             $data = array();
             $data['profile_pic_path'] = $photoDetails->photo_modified_name;
             //echo "<pre>";print_r($data);exit;
              if(!empty($photo_id)){
                        try{
                                $res = $db->update("tbl_users",$data, array('user_id = ?' => $user_id));
                                if (false === $res) {
                                    return 0;  // bool false returned, query failed
                                } else {
                                    return 1;
                                }
                        } catch (Zend_Exception $zex){}
                }
        }

        public function getTimelineClickedImagesVideosDetails($userId,$userType = 'U', $limit, $refresh_user_id=0, $refresh_user_date='', $orderType='') {            
                $db = Zend_Db_Table::getDefaultAdapter();
                $date = date('Y-m-d H:i:s');
                // FOR PHOTOS N VIDEOS FETCHING
//                $select = "SELECT ID, date, name, type, Path, userID, userType, BlastId, U.profile_pic_path, CONCAT(U.firstname, ' ', U.lastname) realname, U.username, BlastedDate
//                            FROM (SELECT   photo_id ID, created_date date, photo_modified_name name, 'photo' type, user_id userID, photo_modified_name Path, user_type userType, blast_id BlastId, blasted_date BlastedDate
//                                    FROM tbl_photos WHERE photo_modified_name != ''
//                                    UNION
//                                    SELECT   video_id ID, created_date date, thumbnail_path name, 'video' type, user_id userID, video_path Path,  'U' userType, blast_id BlastId, blasted_date BlastedDate
//                                    FROM tbl_videos WHERE thumbnail_path != ''
//                                    ORDER BY date DESC) AS A
//                            LEFT JOIN tbl_followers F ON F.user_id = '".$userId."' AND F.user_type = '".$userType."' AND F.follower_id = A.userID AND F.follower_type = A.userType
//                            LEFT JOIN tbl_users U ON U.user_id = userID
//                            WHERE ((F.user_id = '".$userId."' AND F.user_type = '".$userType."' AND F.follower_id = A.userID AND F.follower_type = A.userType) OR ('".$userId."' = A.userID AND '".$userType."' = A.userType)) AND date >= '".$date."' - INTERVAL 1 DAY
//                            ORDER BY date DESC
//                            LIMIT 30";
                
                $select = "SELECT ID, date, photoname, photodesc, name, type, width, height, android_width, android_height, Path, userID, userType, BlastId, U.profile_pic_path, CONCAT(U.firstname, ' ', U.lastname) realname, U.username, BlastedDate, U.user_level userLevel
                                    FROM (SELECT   photo_id ID, created_date date, photo_name photoname, description photodesc, photo_modified_name name, 'photo' type, width, height, android_width, android_height, user_id userID, photo_modified_name Path, user_type userType, blast_id BlastId, blasted_date BlastedDate
                                            FROM tbl_photos WHERE photo_modified_name != '' AND user_type != 'B') AS A
                                    LEFT JOIN tbl_followers F ON F.user_id = '".$userId."' AND F.user_type = '".$userType."' AND F.follower_id = A.userID AND F.follower_type = A.userType
                                    LEFT JOIN tbl_users U ON U.user_id = userID
                                    WHERE ((F.user_id = '".$userId."' AND F.user_type = '".$userType."' AND A.userType != 'B' AND F.follower_id = A.userID AND F.follower_type = A.userType) OR ('".$userId."' = A.userID AND '".$userType."' = A.userType)) AND date >= '".$date."' - INTERVAL 1 DAY";
                
                if($orderType == "previous") {
                    $select .= " AND ID != '".$refresh_user_id."' AND BlastId != '".$refresh_user_id."' AND date <= '".$refresh_user_date."'";
                }
                if($orderType == "next") {
                    $select .=" AND ID != '".$refresh_user_id."' AND BlastId != '".$refresh_user_id."' AND date >= '".$refresh_user_date."'";
                }
                $select .=" ORDER BY date DESC";
                if(($orderType == "previous" || $orderType == "") && $limit !='') {
                    $select .=" LIMIT ".$limit;
                }
                $resultSet = $db->fetchAll($select);
                $mainArray = array();
                $k = 0;
                $idsAry = array();
                $ids = array();
                foreach($resultSet as $key=>$det) {
                    if(!in_array($det['ID'], $idsAry)) {
                       if($det['type'] == 'photo') {
                           if($det['BlastId'] == 0) {
                               $mainArray[$k] = $det;
                               
                               $userLevelArr = array();
                                $select = $db->select()
                                            ->from(array('L' => 'tbl_user_levels'), array('user_level_name' => 'level_name'))
                                            ->where("L.level_no ='".$det['userLevel']."'");
                               $userLevelArr = $db->fetchAll($select);
                               $mainArray[$k]['user_level_name'] = $userLevelArr[0]['user_level_name'];
                               $mainArray[$k]['action_date'] = $det['date'];
                               $originalPhotoId = $det['ID'];
                               $pullToRefreshId = $det['ID'];
                           } // (Blast_id == 0)

                           if($det['BlastId'] != 0) {
                               // TO get Original Photo Details
                               $originalUserDetails = array();
                                $select = $db->select()
                                            ->from(array('P' => 'tbl_photos'), array('ID'=>'photo_id', 'date'=>'created_date', 'photoname' => 'photo_name', 'photodesc' => 'description', 'name'=>'photo_modified_name', 'path'=>'photo_modified_name', 'width', 'height', 'android_width', 'android_height', 'userID'=>'user_id', 'userType'=>'user_type', 'BlastId'=>'blast_id', 'BlastedDate'=>'blasted_date'))
                                            ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array('username','realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'profile_pic_path'))
                                            ->joinLeft(array('L' => 'tbl_user_levels'), "U.user_level = L.level_no", array('user_level_name' => 'level_name'))
                                            ->where("P.photo_id ='".$det['BlastId']."'");
                               $originalUserDetails = $db->fetchAll($select);
                               $mainArray[$k] = $originalUserDetails[0];
                               $mainArray[$k]['type'] = "photo";
                               $mainArray[$k]['date'] = $det['date'];
                               $mainArray[$k]['action_date'] = $det['date'];
                               $originalPhotoId = $originalUserDetails[0]['ID'];
                               // Fetching Blasted USer Count
                               $photoBlastUserCnt = $this->getBlastedUserCountBasedOnPhotoId($det['BlastId']);
                               $mainArray[$k]['blastedUserCnt'] = $photoBlastUserCnt;

                               // to get all Blasted USers Based on Photo ID
                               if($photoBlastUserCnt > 0) {
                                   $blasedUsers = array();
                                   $blasedUsers = $this->getBlastedUserDetailsBasedOnPhotoId($det['BlastId'], $userId);   
                                   $mainArray[$k]['blastedUsers'] = $blasedUsers;
                                   foreach($blasedUsers as $photoBlast){
                                       $idsAry[] = $photoBlast['photo_id'];
                                       $pullToRefreshId = $det['BlastId'];
                                       $idsAry[] = $det['BlastId'];
                                   }
                               }
                           } // Blasted_id != 0 end

                           // TO GET PHOTO COMMENTS
                               $photoCmntSelect = $db->select()
                                   ->from(array('P'=>'tbl_photos'))
                                   ->joinLeft(array('C'=>'tbl_photo_comments'), 'C.photo_id = P.photo_id',array('cnt'=>'count(C.photo_id)'))
                                   ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$userId.'" AND P1.user_type="'.$userType.'")',array('P1.photo_id as isBlasted'))
                                   ->where("P.photo_id =?",$originalPhotoId);
                               $photoCommentsArr = array();
                               $photoCommentsArr = $db->fetchAll($photoCmntSelect);
                               $mainArray[$k]['cmntCnt'] = $photoCommentsArr[0]['cnt'];
                               $mainArray[$k]['isBlasted'] = $photoCommentsArr[0]['isBlasted'];
                               if($mainArray[$k]['cmntCnt'] > 0) {
                                   $photoCmntSelect = $db->select()
                                           ->from(array('PC'=>'tbl_photo_comments'),array('PC.comments','PC.commented_date'))
                                           ->joinLeft(array('U'=>'tbl_users'),'U.user_id = PC.user_id AND PC.user_type="U"',array('profile_pic_path','username','firstname','lastname', 'user_id', 'usertype'=>'user_type'))
                                           ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = PC.user_id AND PC.user_type="B"',array('image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname', 'business_id', 'B_usertype'=>'user_type'))
                                           ->where("PC.photo_id =?",$originalPhotoId)
                                           ->order('PC.commented_date DESC')
                                           ->limit(2);
                                   $photoCommentsArr = array();
                                   $photoCommentsArr = $db->fetchAll($photoCmntSelect);
                                   if(count($photoCommentsArr) > 0) {
                                       $i =1;
                                       foreach($photoCommentsArr as $comment){;
                                           $cmntIndex = 'comment'.$i;
                                           $name = 'commenter_name'.$i;
                                           $id = 'commenter_id'.$i;
                                           $useType = 'commenter_type'.$i;
                                           $pic_path = 'profile_pic_path'.$i;
                                           $date = 'commenter_date'.$i;
                                           if($comment['comments'] != '')   {
                                               $mainArray[$k][$cmntIndex]=$comment['comments'];
                                               if($comment['usertype'] != ''){
                                                   $mainArray[$k][$id]=$comment['user_id'];
                                                   $mainArray[$k][$useType]='U';
                                                   $mainArray[$k][$pic_path]=$comment['profile_pic_path'];
                                                   $mainArray[$k][$name]=$comment['firstname']." ".$comment['lastname'];
                                                   $mainArray[$k][$date]=$comment['commented_date'];
                                               }
                                               if($comment['B_usertype'] != ''){
                                                   $mainArray[$k][$id]=$comment['business_id'];
                                                   $mainArray[$k][$useType]="B";
                                                   $mainArray[$k][$pic_path]=$comment['image_path'];
                                                   $mainArray[$k][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
                                                   $mainArray[$k][$date]=$comment['commented_date'];
                                               }
                                           }
                                           $i++;
                                       } // foreach
                                   } // commentArr Cnt
                               } // (cmntCNt > 0)
                       } // TYpe Photo End

//                       if($det['type'] == 'video'){
//                            if($det['BlastId'] == 0) {
//                               $mainArray[$k] = $det;
//                               $originalVideoId = $det['ID'];
//                               $pullToRefreshId = $det['ID']
//                            } // Blast_id == 0 end
//
//                            if($det['BlastId'] != 0) {
//                               // TO get Original Video Details
//                               $originalUserDetails = array();
//                                $select = $db->select()
//                                            ->from(array('V' => 'tbl_videos'), array('ID'=>'video_id', 'date'=>'created_date', 'name'=>'thumbnail_path', 'path'=>'video_path', 'userID'=>'user_id', 'userType'=>'user_type', 'BlastId'=>'blast_id', 'BlastedDate'=>'blasted_date'))
//                                            ->joinLeft(array('U' => 'tbl_users'), "V.user_id = U.user_id AND P.user_type = 'U'", array('username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'profile_pic_path'))
//                                            ->joinLeft(array('L' => 'tbl_user_levels'), "U.user_level = L.level_no", array('user_level_name' => 'level_name'))
//                                            ->where("V.video_id ='".$det['BlastId']."'");
//                               $originalUserDetails = $db->fetchAll($select);
//                               $mainArray[$k] = $originalUserDetails[0];
//                               $mainArray[$k]['type'] = "video";
//                               $mainArray[$k]['date'] = $det['date'];
//                               $mainArray[$k]['action_date'] = $det['date'];
//                               $originalVideoId = $originalUserDetails[0]['ID'];
//
//                               // to get all Blasted USers Based on Video ID
//                               $videoObj = new Mobile_Model_Videos();
//                               // Fetching Blasted USer Count
//                               $videoBlastUserCnt = $videoObj->getBlastedUserCountBasedOnVideoId($det['BlastId']);
//                               $mainArray[$k]['blastedUserCnt'] = $videoBlastUserCnt;
//
//                               // to get all Blasted USers Based on Video ID
//                               if($photoBlastUserCnt > 0) {
//                                   $blasedUsers = array();
//                                   $blasedUsers = $videoObj->getBlastedUserDetailsBasedOnVideoId($det['BlastId']);
//                                   $mainArray[$k]['blastedUsers'] = $blasedUsers;
//                                   foreach($blasedUsers as $photoBlast){
//                                       $idsAry[] = $photoBlast['video_id'];
//                                       $idsAry[] = $det['BlastId'];
//                                       $pullToRefreshId = $photoBlast['video_id'];
//                                   }
//                               }
//                           } // Original User Details if End
//
//                           // TO Get Video COmments
//                               $videoCmntSelect = $db->select()
//                                   ->from(array('V'=>'tbl_videos'))
//                                   ->joinLeft(array('C'=>'tbl_video_comments'), 'C.video_id = V.video_id',array('cnt'=>'count(C.video_id)'))
//                                   ->joinLeft(array('V1'=>'tbl_videos'),'(V1.blast_id = V.video_id AND V1.user_id = "'.$userId.'" AND V1.user_type="'.$userType.'")',array('V1.video_id as isBlasted')) 
//                                   ->where("V.video_id =?",$originalVideoId);
//
//                               $videoCommentsArr = array();
//                               $videoCommentsArr = $db->fetchAll($videoCmntSelect);
//                               $mainArray[$k]['cmntCnt'] = $videoCommentsArr[0]['cnt'];
//                               $mainArray[$k]['isBlasted'] = $videoCommentsArr[0]['isBlasted'];
//                               if($mainArray[$k]['cmntCnt'] > 0) {
//                                   $videoCmntSelect = $db->select()
//                                           ->from(array('VC'=>'tbl_video_comments'),array('VC.comments', 'VC.commented_date'))
//                                          ->joinLeft(array('U'=>'tbl_users'),'U.user_id = VC.user_id AND VC.user_type="U"',array('profile_pic_path','username','firstname','lastname', 'user_id', 'usertype'=>'user_type'))
//                                           ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = VC.user_id AND VC.user_type="B"',array('image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname', 'business_id', 'B_usertype'=>'user_type'))
//                                           ->where("VC.video_id =?",$originalVideoId)
//                                           ->order('VC.commented_date DESC')
//                                           ->limit(2);
//
//                                   $videoCommentsArr = array();
//                                   $videoCommentsArr = $db->fetchAll($videoCmntSelect);
//                                   if(count($videoCommentsArr) > 0) {
//                                       $i =1;
//                                       foreach($videoCommentsArr as $key2=>$comment){
//                                           $cmntIndex = 'comment'.$i;
//                                           $name = 'commenter_name'.$i;
//                                           $id = 'commenter_id'.$i;
//                                           $useType = 'commenter_type'.$i;
//                                           $pic_path = 'profile_pic_path'.$i;
//                                           $date = 'commenter_date'.$i;
//                                               if($comment['comments'] != '')   {
//                                                   $mainArray[$k][$cmntIndex]=$comment['comments'];
//                                                   if($comment['usertype'] != ''){
//                                                       $mainArray[$k][$id]=$comment['user_id'];
//                                                       $mainArray[$k]['user_type']="U";
//                                                       $mainArray[$k][$pic_path]=$comment['profile_pic_path'];
//                                                       $mainArray[$k][$name]=$comment['firstname']." ".$comment['lastname'];
//                                                       $mainArray[$k][$date]=$comment['commented_date'];
//                                                   }
//                                                   if($comment['B_usertype'] != ''){
//                                                       $mainArray[$k][$id]=$comment['business_id'];
//                                                       $mainArray[$k][$useType]="B";
//                                                       $mainArray[$k][$pic_path]=$comment['image_path'];
//                                                       $mainArray[$k][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
//                                                       $mainArray[$k][$date]=$comment['commented_date'];
//                                                   }
//                                               }
//                                           $i++;
//                                       } // foreach end
//                                   } // videoCommentArr count >  0 ent
//                               } // cmntCnt > 0 end
//
//                       }
                       $mainArray[$k]['pullToRefreshId'] = $pullToRefreshId;
                       $k++;
                    }
                }
                return $mainArray;
        }

        public function getAllTimelineBlastedUsers($logged_user_Id,$logged_user_type, $Id, $type, $limit) {
            $db = Zend_Db_Table::getDefaultAdapter();
            if($type == 'photo') {
                $select = $db->select()
                                   ->from(array('P' => 'tbl_photos'), array('photo_id'))
                                   ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array('user_id', 'username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username', 'user_type'))
   //                                ->joinLeft(array('B' => 'tbl_business_users'), "P.user_id = B.business_id AND P.user_type = 'B'", array('business_id','b_username'=> 'username', 'b_firstname' =>'firstname', 'b_lastname'=>'lastname', 'image_path'))
                                   ->where("P.blast_id ='".$Id."'")
                                   ->order(array('P.created_date DESC'));
            }
            if($type == 'video') {
                $select = $db->select()
                                   ->from(array('P' => 'tbl_videos'), array('video_id'))
                                   ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array('user_id', 'username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username', 'user_type'))
   //                                ->joinLeft(array('B' => 'tbl_business_users'), "P.user_id = B.business_id AND P.user_type = 'B'", array('business_id','b_username'=> 'username', 'b_firstname' =>'firstname', 'b_lastname'=>'lastname', 'image_path'))
                                   ->where("P.blast_id ='".$Id."'")
                                   ->order(array('P.created_date DESC'));
            }
            //echo $select;exit;
            $resultset = $db->fetchAll($select);
            return $resultset;
            
        }
        
        public function getPhotoUploadBlasts($user_id,$loggedUserID,$loggedUserType,$lastScribbleDate, $lastScribbleDate1,$followerIds,$limit, $type='') {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $this->getDbTable()->select()
                            ->from(array('P'=>'tbl_photos'))
                            ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array('user_id', 'username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username', 'user_type','user_level'))
                            ->joinLeft(array('L' => 'tbl_user_levels'), "U.user_level = L.id", array('user_level_name' => 'level_name'))
                            ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$loggedUserID.'" AND P1.user_type ="'.$loggedUserType.'")',array('P1.photo_id as isBlasted'))
                            ->where('FIND_IN_SET(P.user_id,"'.$followerIds.'") OR P.user_id = "'.$loggedUserID.'"')
                            ->where('P.user_type =?',$loggedUserType);
            if($lastScribbleDate != '') {///echo $lastScribbleDate.'-->'.$lastScribbleDate1;
                if($type == 'previous') {
                    $select = $select ->where('P.blasted_date BETWEEN "'.$lastScribbleDate1.'" AND "'.$lastScribbleDate.'"');
                } else {
                    $select = $select ->where('P.blasted_date >"'.$lastScribbleDate1.'"');
                }
            }
            $select = $select->order(array('P.blasted_date DESC'))
                             ->limit($limit)
                             ->setIntegrityCheck(false);
            //echo $select;exit;
            //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo $select;exit; }
            $photos = $this->getDbTable()->fetchAll($select);
            $photos = $photos->toArray();
            foreach($photos as $key=>$photo) {
                //echo "<pre>";print_r($photo);exit;
                if($photo['blast_id'] != 0) {
                    $select1 = $this->getDbTable()->select()
                                        ->from(array('P'=>'tbl_photos'))
                                        ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array('user_id', 'username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username', 'user_type','user_level'))
                                        ->joinLeft(array('L' => 'tbl_user_levels'), "U.user_level = L.id", array('user_level_name' => 'level_name'))
                                        ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$loggedUserID.'" AND P1.user_type ="'.$loggedUserType.'")',array('P1.photo_id as isBlasted'))
                                        ->where('P.photo_id =?',$photo['blast_id'])
                                        ->setIntegrityCheck(false);
                    //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo $select1; }
                    //echo $select1;exit;
                    $originalPhoto = $this->getDbTable()->fetchAll($select1);
                    $originalPhoto = $originalPhoto->toArray();
                    //echo "<pre>";print_r($originalPhoto);exit;
                    if(!empty($originalPhoto) && $originalPhoto[0] != '')
                        $photos[$key] = $originalPhoto[0];
                    else 
                        $photos[$key]['blast_id'] = 0;
                }
                $photos[$key]['name'] = $photos[$key]['photo_modified_name'];
            }
            //if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
                //$photos = array_map("unserialize", array_unique(array_map("serialize", $photos)));
                //echo "<pre>";print_r($input);print_r($photos);exit;
                $result = array();
                $newresult = array();
                foreach ($photos as $item) {
                    if (!array_key_exists($item['photo_modified_name'], $result)) {
                        $result[$item['photo_modified_name']] = $item;
                        $newresult[] = $result[$item['photo_modified_name']];
                        //$result[] = $item;
                    }
                }
              //  echo "<pre>";print_r($newresult);exit;
            //}
            
            return $newresult;
        }

}