<?php
class Mobile_Model_Uploadfiles {

        //upload file at s3 server
        public function uploadFileAtServer($filelocalPath,$fileFolder,$fileName)
        {

            require_once 'Zend/Service/Amazon/S3.php';
            $s3 = new Zend_Service_Amazon_S3("AKIAJVS7UO5YHROJYL6Q","HzeYpU+tZAuijZXIH0IsD2XjeI2sjbgq8UgXv6jQ");

            $fileBucketPath = "upmesocialdeveloper/".$fileFolder."/".$fileName;
            $localFilePath = $filelocalPath.$fileName;
            $ret = $s3->putFile($localFilePath , $fileBucketPath , array(Zend_Service_Amazon_S3::S3_ACL_HEADER => Zend_Service_Amazon_S3::S3_ACL_PUBLIC_WRITE));
            return true;
        }

        //upload all images at s3 server
        public function uploadImagesAtS3Server($filename,$userId)
        {
            $dir_upload = UPLOAD_PATH.'images/original/';
            $thumb_path = UPLOAD_PATH.'images/thumbnails/';
            $medium_path = UPLOAD_PATH.'images/medium/';
            $mobile_path = UPLOAD_PATH.'images/mobile/';
            $android_path = UPLOAD_PATH.'images/android/';
            $huddle_path = UPLOAD_PATH.'images/huddle/';

            $resp1 = $this->uploadFileAtServer($thumb_path,"images/thumbnails/".$userId,$filename);
            $resp2 = $this->uploadFileAtServer($medium_path,"images/medium/".$userId,$filename);
            $resp3 = $this->uploadFileAtServer($mobile_path,"images/mobile/".$userId,$filename);
            $resp4 = $this->uploadFileAtServer($android_path,"images/android/".$userId,$filename);
            $resp5 = $this->uploadFileAtServer($huddle_path,"images/huddle/".$userId,$filename);
            $resp6 = $this->uploadFileAtServer($dir_upload,"images/original/".$userId,$filename);

            //remove all files from local server
                unlink($thumb_path.$filename);
                unlink($medium_path.$filename);
                unlink($mobile_path.$filename);
                unlink($android_path.$filename);
                unlink($huddle_path.$filename);
                unlink($dir_upload.$filename);

                return true;
           
        }

        //upload all videos at s3 server
        public function uploadVideosAtS3Server($videoName,$thumbnailImageName,$userId)
        {
            $dir_upload = UPLOAD_PATH.'videos/';
            $folderName = "videos/uploaded/".$userId;

            $resp1 = $this->uploadFileAtServer($dir_upload,$folderName,$videoName);
            $resp2 = $this->uploadFileAtServer($dir_upload,$folderName,$thumbnailImageName);

            //remove all files from local server
            unlink($dir_upload.$videoName);
            unlink($dir_upload.$thumbnailImageName);

            return true;
        }

        //Encode video using zencoder
        public function videoEncode($fileId,$userId)
        {
            $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/logs/app.log');
            $logger = new Zend_Log($writer);
            $logger->info('videoEncode called  ....!!!');

            $client = new Zend_Http_Client('https://app.zencoder.com/api/v2/jobs', array('maxredirects' => 0,'timeout' => 60));

            $inputFile = "s3://upmesocialdeveloper/videos/uploaded/".$userId."/".$userId."_".$fileId.".mp4";
            $outputFile = $userId."/".$userId.$fileId."/".$userId."_".$fileId.".m3u8";

            $logger->info('inputFile  ..................'.$inputFile);
            $logger->info('outputFile  ..................'.$outputFile);

            $client->setHeaders(array('Content-Type' => 'application/json','Zencoder-Api-Key: 8db00a0366d54ca72660f51943fc5157'));
            $config = array(
                                'input'        => $inputFile,
                                'output'       =>array(
                                                         array(
                                                                "audio_bitrate" => 56,
                                                                "audio_sample_rate" => 22050,
                                                                "base_url" => "s3://upmesocialdeveloper/videos/encoded",
                                                                "decoder_bitrate_cap" => 900,
                                                                "decoder_buffer_size" => 2400,
                                                                "filename" => $outputFile,
                                                                "max_frame_rate" => 30,
                                                                "public" => 1,
                                                                "type" => "segmented",
                                                                "video_bitrate" => 600,
                                                                "width" => 400,
                                                                "format" => "ts",
                                                                "notifications" => array(
                                                                                        array(
                                                                                                "url" =>  "http://www.upmesocial.org/mobile/index/zencodercallback",
                                                                                                "format" =>  "json",
                                                                                                "event" =>  "output_finished",
                                                                                                "Accept" => "application/json",
                                                                                                'Content-Type' => 'application/json'
                                                                                            )
                                                                                        )
                                                                )
                                                        )
                              );


            $json = json_encode($config);
            $response = $client->setRawData($json, 'application/json')->request('POST');

            $responseObj = json_decode($response->getBody());
            $logger->info("response .............".$response->getBody()."<br>responseObj...............".$responseObj->id."     ".$responseObj->outputs[0]->id);
            $jobId = $responseObj->id;

            return $jobId;

        }

        //status of job find using this method.
        public function updateStatus($jobId)
        {
            $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/logs/app.log');
            $logger = new Zend_Log($writer);
            $apiUrl = "https://app.zencoder.com/api/v2/jobs/".$jobId."/progress.json";
            $logger->info("apiuel .............".$apiUrl);
            $client = new Zend_Http_Client($apiUrl);
            $client->setHeaders(array('Content-Type' => 'application/json','Zencoder-Api-Key: 8db00a0366d54ca72660f51943fc5157'));

            $response = $client->request('GET');

            $responseObj = json_decode($response->getBody());
            $logger->info("response .............".$response->getBody()."<br>responseObj...............".$responseObj->id."     ".$responseObj->state);

            return $responseObj->state;
        }

        //jsontest method
        public function test() {

            $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/logs/app.log');
            $logger = new Zend_Log($writer);
            $logger->info("start json decode .............");
            $myjson = "{\"output\":{\"md5_checksum\":null,\"video_codec\":\"h264\",\"url\":\"http://upmesocialdeveloper.s3.amazonaws.com/videos/encoded/504/5041391003179/504_1391003179.m3u8\",\"channels\":\"1\",\"audio_codec\":\"aac\",\"video_bitrate_in_kbps\":null,\"file_size_in_bytes\":98646,\"duration_in_ms\":1600,\"width\":128,\"format\":\"mpeg-ts\",\"total_bitrate_in_kbps\":null,\"frame_rate\":15.0,\"state\":\"finished\",\"audio_bitrate_in_kbps\":64,\"audio_sample_rate\":22050,\"height\":224,\"label\":null,\"id\":186060788},\"job\":{\"test\":true,\"submitted_at\":\"2014-01-29T13:46:36Z\",\"created_at\":\"2014-01-29T13:46:36Z\",\"updated_at\":\"2014-01-29T13:47:00Z\",\"state\":\"finished\",\"pass_through\":null,\"id\":75690818},\"input\":{\"md5_checksum\":null,\"video_codec\":\"h264\",\"channels\":\"1\",\"audio_codec\":\"aac\",\"video_bitrate_in_kbps\":116,\"file_size_in_bytes\":32300,\"duration_in_ms\":1617,\"width\":224,\"format\":\"mpeg4\",\"state\":\"finished\",\"total_bitrate_in_kbps\":137,\"frame_rate\":15.0,\"audio_bitrate_in_kbps\":21,\"audio_sample_rate\":22050,\"height\":128,\"id\":75668735}}";
            $myjsonObj = json_decode($myjson);
            $logger->info("finished json decode .............");

            $logger->info("job .............".$myjsonObj->job->id);
            
        }

        // //upload all videos at s3 server
        // public function uploadVideosAtS3Server($videoName,$thumbnailImageName)
        // {
        //     $dir_upload = UPLOAD_PATH.'videos/';

        //     $resp1 = $this->uploadFileAtServer($dir_upload,"videos",$videoName);
        //     $resp2 = $this->uploadFileAtServer($dir_upload,"videos",$thumbnailImageName);

        //     //remove all files from local server
        //     unlink($dir_upload.$videoName);
        //     unlink($dir_upload.$thumbnailImageName);
            
        //     return true;
        // }

}