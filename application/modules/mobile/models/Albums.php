<?php
class Mobile_Model_Albums {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Mobile_Model_DbTable_Albums');
                }
                return $this->_dbTable;
        }

        //function to insert album details
        public function insert($data){//echo 'db<Pre>';print_R($data);exit;
                return $this->getDbTable()->insert($data);
        }

        public function update($data, $albumid) {
                if(!empty($albumid)){
                        try{
                                $res = $this->getDbTable()->update($data, array('id = ?' => $albumid));
                                if (false === $res) {
                                    return 0;  // bool false returned, query failed
                                } else {
                                    return 1;
                                }
                        } catch (Zend_Exception $zex){}
                }
        }

        public function getalbumdetails($albumid,$userid,$usertype) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('tbl_albums'),array('album_name'))
                            ->where("id  =?",$albumid)
                            ->where("user_id =?",$userid)
                            ->where("user_type =?",$usertype);
                $resultSet = $db->fetchRow($select);
        }

        public function albumnameexistsrnot($data) {
                $album_name = $data['album_name'];
                $user_id = $data['user_id'];
                $user_type = $data['user_type'];
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('tbl_albums'),array('id'))
                            ->where("album_name =?",$album_name)
                            ->where("user_id =?",$user_id)
                            ->where("user_type =?",$user_type)
                            ->where("is_default = '1'");
                $resultSet  = $db->fetchRow($select);
                return $resultSet;
        }

        public function getAlbumsByUserId($user_id, $user_type) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('A'=>'tbl_albums'))
                            ->joinLeft(array('P'=>'tbl_photos'),'A.id = P.album_id AND P.cover_photo = 1',array('cover_photo'=>'photo_modified_name'))
                            ->where('A.user_id = ?', $user_id)
                            ->where('A.user_type = ?', $user_type);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getBusinessAlbumsByUserId($user_id, $user_type) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('A'=>'tbl_albums'))
                            ->joinLeft(array('P'=>'tbl_photos'),'A.id = P.album_id',array('photo_id','photo_modified_name'))
                            ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = A.user_id AND A.user_type="B"',array('business_user_image'=>'image_path','username','firstname','lastname'))
                            ->where('A.album_name = "Business Photos"')
                            ->where('A.user_id = ?', $user_id)
                            ->where('A.user_type = ?', $user_type);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function deleteAlbum($albumid) {
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
                    //echo $albumid;exit;
                }
                
                $Photos = $this->getalbumphotos($albumid);
                foreach($Photos as $key=>$val) {
                        @unlink(UPLOAD_PATH."images/thumbnails/".$val['photo_modified_name']);
                        @unlink(UPLOAD_PATH."images/original/".$val['photo_modified_name']);
                        @unlink(UPLOAD_PATH."images/medium/".$val['photo_modified_name']);
                }
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE A.*,P.*,C.*,N.* FROM tbl_albums A
                                    LEFT JOIN tbl_photos P ON P.album_id = A.id
                                    LEFT JOIN tbl_photo_comments C ON C.photo_id = P.photo_id
                                    LEFT JOIN tbl_notifications N ON (N.reference_id = P.photo_id AND (N.notifications_type = 'photo_uploads' OR N.notifications_type = 'photo_blasted'))
                                    WHERE A.id = ".$albumid);
                $id = $select->execute();
                return $id;
        }

        public function albumexistsrnot($albumid) {
                $select = $this->getDbTable()->select()
                                            ->where('id = ?', $albumid);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function albumexistsrnotwithisdefault($user_id, $user_type) {
                $select = $this->getDbTable()->select()
                                            ->where('user_id = ?', $user_id)
                                            ->where('user_type = ?', $user_type)
                                            ->where('is_default = 1');
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getalbumphotos($albumid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('tbl_photos'),array('photo_id','photo_modified_name'))
                            ->where("album_id =?",$albumid)
                            ->where("blast_id = '0'");
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

}