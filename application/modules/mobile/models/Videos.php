<?php

class Mobile_Model_Videos
{
    protected $_dbTable;

    public function setDbTable($dbTable) {
            if (is_string($dbTable)) {
                    $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                    throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
    }

    public function getDbTable() {
            if (null === $this->_dbTable) {
                    $this->setDbTable('Mobile_Model_DbTable_Videos');
            }
            return $this->_dbTable;
    }
    
    public function getVideoDetailsByVideoId($userid, $user_type,$video_id) {
        $select = $this->getDbTable()->select()
                           ->from(array('V'=>'tbl_videos'),array('video_id','video_name','user_id'))
                           ->joinLeft(array('U'=>'tbl_users'),'U.user_id = V.user_id AND V.user_type="U"',array('gender','profile_pic_path','username','realname'=>'CONCAT(U.firstname, " ",U.lastname)','firstname','lastname'))
                           ->joinLeft(array('V1'=>'tbl_videos'),'(V1.blast_id = V.video_id AND V1.user_id = "'.$userid.'" AND V1.user_type="'.$user_type.'")',array('video_id as isBlastedVideo'))
                           ->where("V.video_id=?",$video_id)
                           ->setIntegrityCheck(false);
           //echo $select;exit;
           $resultSet = $this->getDbTable()->fetchAll($select);
           return $resultSet;
   }
    
    public function getvideocomments($video_id,$limit='100') {
            $select = $this->getDbTable()->select()
                            ->from(array('C'=>'tbl_video_comments'), array('user_posted'=>'user_id','comments','commented_date','user_type'))
                            ->joinLeft(array('U'=>'tbl_users'),'U.user_id = C.user_id AND C.user_type="U"',array('gender','profile_pic_path','username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)','firstname','lastname'))
                            ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = C.user_id AND C.user_type="B"',array('image_path','business_username'=>'B.username','business_realname' => 'CONCAT(B.firstname, " ", B.lastname)','business_firstname'=>'firstname','business_lastname'=>'lastname'))
                            ->where("C.video_id =?",$video_id)
                            ->order("C.comment_id ASC")
                            ->limit($limit)
                            ->setIntegrityCheck(false);
            $resultSet = $this->getDbTable()->fetchAll($select);
            return $resultSet;
    }
    
    public function getvideosByUserID($userId,$logged_user_id,$user_type='U') {
            
                $db = Zend_Db_Table::getDefaultAdapter();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $select    = $db->select()
                                ->from(array('P'=>'tbl_videos'))
                                ->joinLeft(array('C'=>'tbl_video_comments'), 'C.video_id = P.video_id',array('cmntCnt'=>'count(C.video_id)'))
                                ->joinLeft(array('P1'=>'tbl_videos'),'(P1.blast_id = P.video_id AND P.user_id = "'.$logged_user_id.'" AND P.user_type="'.$user_type.'")',array('video_id as isBlasted'))
                                ->joinLeft(array('U' => 'tbl_users'), "P.user_id = U.user_id AND P.user_type = 'U'", array('user_id', 'username', 'gender','realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username', 'user_type','user_level'))
                                ->joinLeft(array('L' => 'tbl_user_levels'), "U.user_level = L.id", array('user_level_name' => 'level_name'))
                                ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$logged_user_id."' AND UD.user_type = 'U' AND P.video_id = UD.scribble_id AND type = 'video'", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints'), "U.user_id = CP.user_id", array('cool_points'))
                                ->where("P.user_id =?",$userId)
                                ->where("P.status ='finished'")
                                ->where("P.blast_id = '0'")
                                ->group('P.video_id')
                                ->order('P.video_id ASC');
                $resultSet = $db->fetchAll($select);
                if(count($resultSet) > 0) {
                    foreach($resultSet as $key => $video) { 

                       // to get Blasted Users Count
                        $videoBlastedUserCnt = $this->getBlastedUserCountBasedOnVideoId($video['video_id']);
                        
                        $blastedUserDetails = array();
                        //echo $videoBlastedUserCnt;exit;
                        if($videoBlastedUserCnt > 0) {
                            $limit = 2; // get 2 blasted users
                            $blastedUserDetails = $this->getBlastedUserDetailsBasedOnVideoId($video['video_id']);
                            $videoCount = 0;
                            foreach($blastedUserDetails as $blastUser)
                            {
                                $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                                $blastedUserDetails[$videoCount] = $blastUser;
                                $videoCount++; 
                            }
                            //echo "<pre>";print_r($blastedUserDetails);exit;
                        }

                        $resultSet[$key]['videoblastedUserCnt']=$videoBlastedUserCnt;
                        $resultSet[$key]['videoblastedUsers']=$blastedUserDetails;

                        if($video['cmntCnt'] > 0) {
                            $videoCmntSelect = $db->select()
                                    ->from(array('VC'=>'tbl_video_comments'),array('VC.comments','VC.commented_date'))
									->joinLeft(array('U'=>'tbl_users'),'U.user_id = VC.user_id AND VC.user_type="U"',array('profile_pic_path','username','gender','firstname','lastname', 'user_id', 'usertype'=>'user_type'))
                                    ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = VC.user_id AND VC.user_type="B"',array('image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname', 'business_id', 'B_usertype'=>'user_type'))
                                    ->where("VC.video_id =?",$video['video_id'])
                                    ->order('VC.commented_date DESC')
                                    ->limit(2);
                            $videoCommentsArr = array();
                            $videoCommentsArr = $db->fetchAll($videoCmntSelect);
							if(count($videoCommentsArr) > 0) {
                                $i =1;
                                foreach($videoCommentsArr as $comment){
                                    $cmntIndex = 'comment'.$i;
									$name = 'commenter_name'.$i;
                                    $id = 'commenter_id'.$i;
                                    $useType = 'commenter_type'.$i;
                                    $pic_path = 'profile_pic_path'.$i;
									$cmtDateIndex = 'commented_date'.+$i;
                                    $cmtGender = 'commenter_gender'.+$i;

                                    if($comment['comments'] != '')   {
                                        $resultSet[$key][$cmntIndex]=$comment['comments'];
										$resultSet[$key][$cmtDateIndex]=$comment['commented_date'];
                                        $resultSet[$key][$cmtGender]=$comment['gender'];
                                    	if($comment['usertype'] != ''){
                                            $resultSet[$key][$id]=$comment['user_id'];
                                            $resultSet[$key][$useType]='U';
                                            $resultSet[$key][$pic_path]=$comment['profile_pic_path'];
                                            $resultSet[$key][$name]=$comment['firstname']." ".$comment['lastname'];
                                        }
                                        if($comment['B_usertype'] != ''){
                                            $resultSet[$key][$id]=$comment['business_id'];
                                            $resultSet[$key][$useType]="B";
                                            $resultSet[$key][$pic_path]=$comment['image_path'];
                                            $resultSet[$key][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
                                        }
									}
                                    $i++;
                                }
                            }
                        }
                    }
                }
                return $resultSet;
        }
        


        public function getBlastedUserDetailsBasedOnVideoId($video_id) {
             $db = Zend_Db_Table::getDefaultAdapter();
             $select = $db->select()
                                ->from(array('V' => 'tbl_videos'), array('video_id','created_date'))
                                ->joinLeft(array('U' => 'tbl_users'), "V.user_id = U.user_id AND V.user_type = 'U'", array('user_id','username','gender', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username'))
                                ->joinLeft(array('B' => 'tbl_business_users'), "V.user_id = B.business_id AND V.user_type = 'B'", array('business_id','b_username'=> 'username', 'b_firstname' =>'firstname', 'b_lastname'=>'lastname', 'image_path'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints'), "V.user_id = CP.user_id ", array('cool_points'))
                                ->joinLeft(array('UL' => 'tbl_user_levels'), "U.user_level = UL.level_no ", array('user_level_name' => 'level_name'))
                                ->where("V.blast_id ='".$video_id."'")
                                ->order(array('V.created_date DESC'));
                $resultset = $db->fetchAll($select);                
                return $resultset;
        }
        
        public function getBlastedUserCountBasedOnVideoId($video_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('V' => 'tbl_videos'), array('videoBlastUserCount'=>'COUNT(video_id)'))
                                ->joinLeft(array('U' => 'tbl_users'), "V.user_id = U.user_id AND V.user_type = 'U'", array(''))
//                                ->joinLeft(array('B' => 'tbl_business_users'), "P.user_id = B.business_id AND P.user_type = 'B'", array(''))
                                ->where("V.blast_id ='".$video_id."'")
                                ->order(array('V.created_date DESC'));
                //echo $select;//exit;
                $resultset = $db->fetchAll($select);
                return $resultset[0]['videoBlastUserCount'];
        }


//Novasys code 
        public function getVideoUploadBlasts($user_id,$loggedUserID,$loggedUserType,$lastScribbleDate, $lastScribbleDate1,$followerIds,$limit, $type='') {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $this->getDbTable()->select()
                            ->from(array('V'=>'tbl_videos'))
                            ->joinLeft(array('U' => 'tbl_users'), "V.user_id = U.user_id AND V.user_type = 'U'", array('user_id','gender', 'username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username', 'user_type','user_level'))
                            ->joinLeft(array('L' => 'tbl_user_levels'), "U.user_level = L.id", array('user_level_name' => 'level_name'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'), "U.user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserID."' AND UD.user_type = 'U' AND V.video_id = UD.scribble_id AND type = 'video'", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('V1'=>'tbl_videos'),'(V1.blast_id = V.video_id AND V1.user_id = "'.$loggedUserID.'" AND V1.user_type ="'.$loggedUserType.'")',array('V1.video_id as isBlasted'))
                            ->where('FIND_IN_SET(V.user_id,"'.$followerIds.'") OR V.user_id = "'.$loggedUserID.'"')
                            ->where('V.user_type =?',$loggedUserType)
                            ->where("V.status = 'finished'");
                            // ->where("(
                            //             V.blast_id=0 AND V.user_id!='".$loggedUserID."') 
                            //             OR (V.blast_id!=0 AND V.user_id='".$loggedUserID."')
                            //             OR (V.blast_id!=0 AND V.user_id!='".$loggedUserID."')
                            //         ");
            if($lastScribbleDate != '') {
                if($type == 'previous') {
                    $select = $select ->where('V.blasted_date BETWEEN "'.$lastScribbleDate1.'" AND "'.$lastScribbleDate.'"');
                } else {
                    $select = $select ->where('V.blasted_date >"'.$lastScribbleDate1.'"');
                }
            }
            $select = $select->order(array('V.blasted_date DESC'))
                             ->limit($limit)
                             ->setIntegrityCheck(false);
            $videos = $this->getDbTable()->fetchAll($select);
            $videos = $videos->toArray();
            foreach($videos as $key=>$video) {
                if($loggedUserID == $video['user_id']) {
                    $videos[$key]['isBlasted'] = "2";
                }
                if($video['blast_id'] != 0) {
                    $select1 = $this->getDbTable()->select()
                                        ->from(array('V'=>'tbl_videos'), array('video_id', 'video_name', 'video_path', 'thumbnail_path', 'user_id', 'user_type','blast_id', 'blasted_date', 'created_date'))
                                        ->joinLeft(array('U' => 'tbl_users'), "V.user_id = U.user_id AND V.user_type = 'U'", array('user_id', 'gender','username', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'uname'=>'username', 'user_type','user_level'))
                                        ->joinLeft(array('L' => 'tbl_user_levels'), "U.user_level = L.id", array('user_level_name' => 'level_name'))
                                        ->joinLeft(array('CP' => 'tbl_user_coolpoints'), "U.user_id = CP.user_id", array('cool_points'))
                                        ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserID."' AND UD.user_type = 'U' AND V.video_id = UD.scribble_id AND type = 'video'", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                                        ->joinLeft(array('V1'=>'tbl_videos'),'(V1.blast_id = V.video_id AND V1.user_id = "'.$loggedUserID.'" AND V1.user_type ="'.$loggedUserType.'")',array('V1.video_id as isBlasted'))
                                        ->where('V.video_id =?',$video['blast_id'])
                                        ->setIntegrityCheck(false);
                    $originalVideo = $this->getDbTable()->fetchAll($select1);
                    $originalVideo = $originalVideo->toArray();
                    if(!empty($originalVideo) && $originalVideo[0] != '') {
                        $videos[$key] = $originalVideo[0];
                        $videos[$key]['blasted_date'] = $originalVideo[0]['created_date'] ;
                        if($loggedUserID == $originalVideo[0]['user_id']) {
                            $videos[$key]['isBlasted'] = "2";
                        }
                    } else {
                        $videos[$key]['blast_id'] = 0;
                    }
                    
                }
                $videos[$key]['name'] = $videos[$key]['video_path'];
            }
            $videos = array_map("unserialize", array_unique(array_map("serialize", $videos)));
            $result = array();
            $newresult = array();
            foreach ($videos as $item) {
                if (!array_key_exists($item['video_path'], $result)) {
                    $result[$item['video_path']] = $item;
                    $newresult[] = $result[$item['video_path']];
                }
            }
            return $newresult;
        }

          public function getVideoComment($mixedAry,$video){
               $db = Zend_Db_Table::getDefaultAdapter();
               $commentCnt = $this->getvideoCommentCnt($video);
               $videoCommentsArr = array();
                if($commentCnt > 0) {
                     $videoCmntSelect = $db->select()
                                          ->from(array('VC'=>'tbl_video_comments'),array('VC.comments', 'VC.commented_date'))
                                         ->joinLeft(array('U'=>'tbl_users'),'U.user_id = VC.user_id AND VC.user_type="U"',array('profile_pic_path','gender','username','firstname','lastname', 'user_id', 'usertype'=>'user_type'))
                                          ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = VC.user_id AND VC.user_type="B"',array('image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname', 'business_id', 'B_usertype'=>'user_type'))
                                          ->where("VC.video_id =?",$video['video_id'])
                                          ->order('VC.commented_date DESC')
                                          ->limit(2);
                    $videoCommentsArr = $db->fetchAll($videoCmntSelect);
                }//comment count 

                return $videoCommentsArr;
        }

        public function getvideoCommentCnt($video){
                $db = Zend_Db_Table::getDefaultAdapter();
                $videoCmntSelect = $db->select()
                                  ->from(array('V'=>'tbl_videos'))
                                  ->joinLeft(array('C'=>'tbl_video_comments'), 'C.video_id = V.video_id',array('cnt'=>'count(C.video_id)')) 
                                  ->where("V.video_id =?",$video['video_id']);
                              $videoCommentsArr = array();
                              $videoCommentsArr = $db->fetchAll($videoCmntSelect);
                              $videocmntCnt = $videoCommentsArr[0]['cnt'];
                
                return $videocmntCnt;
        }


}