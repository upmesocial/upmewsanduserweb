<?php
class Mobile_IndexController extends Zend_Controller_Action {

        public function init() {
                /* Initialize action controller here */
        }

        public function indexAction() {
                // action body
        }

        public function getuserinfoAction() {
                if($_SERVER['REMOTE_ADDR'] != '192.168.1.57') {
                        $this->_helper->layout->disableLayout();
                }
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $req = $this->getRequest();
                $uid=$req->getParam('user_id');
                if($uid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
                }
                $loggedUserID = $req->getParam('logged_user_id');
                if($loggedUserID == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!'));
                        return;
                }
                $limit = (($req->getParam('limit') != '') ? $req->getParam('limit'):30);
                $userDetAry =array();
                $userDetAry = $userObj->getUserDetails($uid);
                //echo '<pre>';print_r($userDetAry);exit;
                if(count($userDetAry) !=1){
                        $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist');
                        echo $myjson->customEncode($resultAry);
                        return true;
                }
                $loggeduserDetAry = $userObj->getUserDetails($loggedUserID);
                $userDetAry = $userDetAry->toArray();
                // to Get User Level Name
                $notificationObj = new Notifications_Model_Notifications();
                $cnt = $notificationObj->notificationcnt($uid,'U','1');
                $usrLvlObj = new User_Model_Userlevels();
                $usrlevelName = $usrLvlObj->getUserLevels($userDetAry['user_level']);
                $usrlevelName = $usrlevelName->toArray();
                $userDetAry['user_level_name'] = $usrlevelName['level_name'];
                $userDetAry['nooffollowers'] = $followObj->getFollowersCnt($uid);
                $userDetAry['nooffollowings'] = $followObj->getFollowingCnt($uid);
                $userDetAry['scribblecount'] = $scribbleObj->getUserPostedScribbleCnt($uid,$loggedUserID, $loggeduserDetAry['user_level']);
                $userDetAry['notificationcount'] = $cnt['notificationcnt'];
                if($loggedUserID == $uid) {
                        // Fetch LOgged USer Level
                        $usrLevelObj = new User_Model_Userlevels();
                        $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
                        $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];
                        // TO get User Scribbles
                        $parentScribbles =array();
                        $allScribbles =array();
                        $parentScribbles = $scribbleObj->getUserParentScribbles($uid,$loggedUserID,"U", $loggedUserLevel, $limit);
                        foreach($parentScribbles as $key =>$parentScrib) {
                                if($parentScrib['blast_id'] != 0) {
                                        $parentId = $parentScrib['blast_id'];
                                        $parentScrib['notifications_type'] = "scribble";
                                        $blastedScribbleInfo = $scribbleObj->getScribleInfo($parentId);
                                        $allScribbles[$key] = $blastedScribbleInfo;
                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                        //echo "<pre>";print_r($allScribbles);exit;
                                        //  check if the Parent scribble is Blasted
                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['blast_id']);
                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                        $blastedUsrAry = array();
                                        if($blastedUsrCnt > 0) {
                                                //to Get Blasted User Details of the Parent Scribble
                                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['blast_id'], $loggedUserID);
                                        }
                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                        // As USer can not Updown/ Blast his own scribbles
                                        $allScribbles[$key]['isUpDownScribble'] = '';
                                        $allScribbles[$key]['UpDownAction'] = '';
                                        $allScribbles[$key]['isBlastedScribble'] = $parentScrib['scribble_id'];
                                } else {
                                        $parentId = $parentScrib['scribble_id'];
                                        $parentScrib['notifications_type'] = "scribble";
                                        $allScribbles[$key] = $parentScrib;
                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                        //check if the Parent scribble is Blasted
                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                        $blastedUsrAry = array();
                                        if($blastedUsrCnt > 0) {
                                                //to Get Blasted User Details of the Parent Scribble
                                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                        }
                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                        // As USer can not Updown/ Blast his own scribbles
                                        $allScribbles[$key]['isUpDownScribble'] ='0';
                                        $allScribbles[$key]['UpDownAction'] ='0';
                                        $allScribbles[$key]['isBlastedScribble'] ='0';
                                }
                        }
                        $scribArray = $allScribbles;
                        $userDetAry['isFollowing'] = "2";
                        $userDetAry['isOtherUserFollowing'] = "2";
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isFollowing'] = "1";
                                // Get Scribbles only If Logged User is following Other User
                                // get logged User Level
                                $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                                $logged_user_level = $usrlevel[0]['user_level'];
                                $parentScribbles =array();
                                $allScribbles =array();
                                $parentScribbles = $scribbleObj->getUserParentScribbles($uid,$loggedUserID,"U", $logged_user_level, $limit);
                                foreach($parentScribbles as $key =>$parentScrib) {
                                        if($parentScrib['blast_id'] != 0) {
                                                $blastedScribbleInfo = array();
                                                $parentId = $parentScrib['blast_id'];
                                                $parentScrib['notifications_type'] = "scribble";
                                                $blastedScribbleInfo = $scribbleObj->getScribleInfo($parentId);
                                                $allScribbles[$key] = $blastedScribbleInfo;
                                                // TO check if Logged user BLASTED or Up or Down the parent Scribble
                                                if($blastedScribbleInfo['from_user_id'] == $loggedUserID){
                                                        $allScribbles[$key]['isUpDownScribble']="2";
                                                        $allScribbles[$key]['UpDownAction']="2";
                                                        $allScribbles[$key]['isBlastedScribble']="2";
                                                }
                                                if($blastedScribbleInfo['from_user_id'] != $loggedUserID){
                                                        //echo $blastedScribbleInfo[0]['from_user_id']."--".$loggedUserID."--".$parentId;print_r($blastedScribbleInfo);exit;
                                                        $updownAry = array();
                                                        $updownAry = $scribbleObj->getUpDownActionByUserId($parentId, $loggedUserID);
                                                        if(empty($updownAry)) {
                                                                $allScribbles[$key]['isUpDownScribble'] = "0";
                                                                $allScribbles[$key]['UpDownAction'] = "0";
                                                        }
                                                        if(!empty($updownAry)) {
                                                                $allScribbles[$key]['isUpDownScribble'] = $updownAry[0]['isUpDownScribble'];
                                                                $allScribbles[$key]['UpDownAction'] = $updownAry[0]['UpDownAction'];
                                                        }
                                                        $blastAry = array();
                                                        $blastAry = $scribbleObj->getBlastActionByUserId($parentId, $loggedUserID);
                                                        if(empty($blastAry)) {
                                                                $allScribbles[$key]['isBlastedScribble']="0";
                                                        }
                                                        if(!empty($blastAry)) {
                                                                $allScribbles[$key]['isBlastedScribble']=$blastAry[0]['isBlastedScribble'];
                                                        }
                                                }
                                                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                                $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                                //echo "<pre>";print_r($allScribbles);exit;
                                                //  check if the Parent scribble is Blasted
                                                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['blast_id']);
                                                $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                                $blastedUsrAry = array();
                                                if($blastedUsrCnt > 0) {
                                                        //to Get Blasted User Details of the Parent Scribble
                                                        $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['blast_id'], $loggedUserID);
                                                }
                                                $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                        } else {
                                                $parentId = $parentScrib['scribble_id'];
                                                $parentScrib['notifications_type'] = "scribble";
                                                $allScribbles[$key] = $parentScrib;
                                                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                                $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                                //check if the Parent scribble is Blasted
                                                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                                $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                                $blastedUsrAry = array();
                                                if($blastedUsrCnt > 0) {
                                                        //to Get Blasted User Details of the Parent Scribble
                                                        $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                                }
                                                $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                        }
                                }
                                $scribArray = $allScribbles;
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isFollowing'] = "0";
                                $scribArray = array();
                        }
                        // To check if other user following logged User or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($loggedUserID,  'U', $uid, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isOtherUserFollowing'] = "1";
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isOtherUserFollowing'] = "0";
                        }
                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($scribArray) > 0) {
                        $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                        foreach($scribArray as $key=>$scrib) {
                                if($scrib['isBlastedScribble'] == '')
                                    $scribArray[$key]['isBlastedScribble'] = "0";
                                $scribArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                                $scribArray[$key]['notifications_type'] = "scribble";
                                $scrib['notifications_type'] = "scribble";
                                // if($scrib['notifications_type'] != 'scribble') continue;
                                $scribArray[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                        }
                }
                $userDetAry['scribble_content'] = $scribArray;
                $notificationcnt = $notificationObj->notificationcnt($loggedUserID, "U", '1');
                $resultAry = array('service_status'=>'success',"content" => $userDetAry, "notificationcnt" => $notificationcnt['notificationcnt']);
                echo $myjson->customEncode($resultAry);
                return true;
        }

        public function getuserdataAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $req = $this->getRequest();
                $uid=$req->getParam('user_id');
                if($uid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
                }
                $loggedUserID = $req->getParam('logged_user_id');
                if($loggedUserID == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!'));
                        return;
                }
                $userDetAry =array();
                $userDetAry = $userObj->getUserDetails($uid);
                if(count($userDetAry) !=1){
                        $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist');
                        echo $myjson->customEncode($resultAry);
                        return true;
                }
                $loggeduserDetAry = $userObj->getUserDetails($loggedUserID);
                $userDetAry = $userDetAry->toArray();
                // to Get User Level Name
                $notificationObj = new Notifications_Model_Notifications();
                $cnt = $notificationObj->notificationcnt($uid,'U','1');
                $usrLvlObj = new User_Model_Userlevels();
                $usrlevelName = $usrLvlObj->getUserLevels($userDetAry['user_level']);
                $usrlevelName = $usrlevelName->toArray();
                $userDetAry['user_level_name'] = $usrlevelName['level_name'];
                $userDetAry['nooffollowers'] = $followObj->getFollowersCnt($uid);
                $userDetAry['nooffollowings'] = $followObj->getFollowingCnt($uid);
                $userDetAry['scribblecount'] = $scribbleObj->getUserPostedScribbleCnt($uid,$loggedUserID, $loggeduserDetAry['user_level']);
                $userDetAry['notificationcount'] = $cnt['notificationcnt'];
                if($loggedUserID == $uid) {
                        // Fetch LOgged USer Level
                        $userDetAry['isFollowing'] = "2";
                        $userDetAry['isOtherUserFollowing'] = "2";
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isFollowing'] = "1";
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isFollowing'] = "0";
                                $scribArray = array();
                        }
                        // To check if other user following logged User or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($loggedUserID,  'U', $uid, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isOtherUserFollowing'] = "1";
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isOtherUserFollowing'] = "0";
                        }
                        $userDetAry['firstname'] = $myjson->just_clean($userDetAry['firstname']);
                }
                $notificationcnt = $notificationObj->notificationcnt($loggedUserID, "U", '1');
                $resultAry = array('service_status'=>'success',"content" => $userDetAry, "notificationcnt" => $notificationcnt['notificationcnt']);
                echo $myjson->customEncode($resultAry);
                return true;
        }

        public function getuserscribblesdataAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $req = $this->getRequest();
                $uid=$req->getParam('user_id');
                if($uid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!',"responsecode" => '500'));
                        return;
                }
                if(!is_numeric($uid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!',"responsecode" => '500'));
                        return;
                }
                $loggedUserID = $req->getParam('logged_user_id');
                if($loggedUserID == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!',"responsecode" => '500'));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!',"responsecode" => '500'));
                        return;
                }
                $limit = (($req->getParam('limit') != '') ? $req->getParam('limit'):30);
                $usrLvlObj = new User_Model_Userlevels();
                if($loggedUserID == $uid) {
                        // Fetch LOgged USer Level
                        $usrLevelObj = new User_Model_Userlevels();
                        $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
                        $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];
                        // TO get User Scribbles
                        $parentScribbles =array();
                        $allScribbles =array();
                        $scribArray = array();
                        $parentScribbles = $scribbleObj->getUserParentScribbles($uid,$loggedUserID,"U", $loggedUserLevel, $limit);
                        foreach($parentScribbles as $key =>$parentScrib) {
                                if($parentScrib['blast_id'] != 0) {
                                        $parentId = $parentScrib['blast_id'];
                                        $parentScrib['notifications_type'] = "scribble";
                                        $blastedScribbleInfo = $scribbleObj->getScribleInfo($parentId);
                                        if(isset($blastedScribbleInfo)) {
                                                $allScribbles[$key] = $blastedScribbleInfo;
                                                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                                                $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                                //  check if the Parent scribble is Blasted
                                                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['blast_id']);
                                                $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                                $blastedUsrAry = array();
                                                if($blastedUsrCnt > 0) {
                                                        //to Get Blasted User Details of the Parent Scribble
                                                        $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['blast_id'], $loggedUserID);
                                                }
                                                $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                                // As USer can not Updown/ Blast his own scribbles
                                                $allScribbles[$key]['isUpDownScribble'] ='';
                                                $allScribbles[$key]['UpDownAction'] ='';
                                                $allScribbles[$key]['isBlastedScribble'] =$parentScrib['scribble_id'];
                                        }
                                } else {
                                        $parentId = $parentScrib['scribble_id'];
                                        $parentScrib['notifications_type'] = "scribble";
                                        $allScribbles[$key] = $parentScrib;
                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                        //  check if the Parent scribble is Blasted
                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                        $blastedUsrAry = array();
                                        if($blastedUsrCnt > 0) {
                                                //to Get Blasted User Details of the Parent Scribble
                                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                        }
                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                        
                                        
                                        // As USer can not Updown/ Blast his own scribbles
                                        $allScribbles[$key]['isUpDownScribble'] = '0';
                                        $allScribbles[$key]['UpDownAction'] = '0';
                                        $allScribbles[$key]['isBlastedScribble'] = '0';
                                }
                        }
                        $scribArray = $allScribbles;
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                // Get Scribbles only If Logged User is following Other User
                                // get logged User Level
                                $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                                $logged_user_level = $usrlevel[0]['user_level'];
                                $parentScribbles =array();
                                $allScribbles =array();
                                $parentScribbles = $scribbleObj->getUserParentScribbles($uid,$loggedUserID,"U", $logged_user_level, $limit);
                                $blastedScribbleInfo = array();
                                foreach($parentScribbles as $key =>$parentScrib) {
                                        if($parentScrib['blast_id'] != 0) {
                                                $parentId = $parentScrib['blast_id'];
                                                $parentScrib['notifications_type'] = "scribble";
                                                $blastedScribbleInfo = $scribbleObj->getScribleInfo($parentId);
                                                $allScribbles[$key] = $blastedScribbleInfo;
                                                // TO check if Logged user BLASTED or Up or Down the parent Scribble
                                                if($blastedScribbleInfo['from_user_id'] == $loggedUserID) {
                                                        $allScribbles[$key]['isUpDownScribble']="2";
                                                        $allScribbles[$key]['UpDownAction']="2";
                                                        $allScribbles[$key]['isBlastedScribble']="2";
                                                }
                                                if($blastedScribbleInfo['from_user_id'] != $loggedUserID) {
                                                        $updownAry = array();
                                                        $updownAry = $scribbleObj->getUpDownActionByUserId($parentId, $loggedUserID);
                                                        if(empty($updownAry)) {
                                                                $allScribbles[$key]['isUpDownScribble']="0";
                                                                $allScribbles[$key]['UpDownAction']="0";
                                                        }
                                                        if(!empty($updownAry)) {
                                                                $allScribbles[$key]['isUpDownScribble']= $updownAry[0]['isUpDownScribble'];
                                                                $allScribbles[$key]['UpDownAction']= $updownAry[0]['UpDownAction'];
                                                        }
                                                        $blastAry = array();
                                                        $blastAry = $scribbleObj->getBlastActionByUserId($parentId, $loggedUserID);
                                                        if(empty($blastAry)) {
                                                                $allScribbles[$key]['isBlastedScribble']="0";
                                                        }
                                                        if(!empty($blastAry)) {
                                                                $allScribbles[$key]['isBlastedScribble']=$blastAry[0]['isBlastedScribble'];
                                                        }
                                                }
                                                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                                $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                                //  check if the Parent scribble is Blasted
                                                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['blast_id']);
                                                $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                                $blastedUsrAry = array();
                                                if($blastedUsrCnt > 0) {
                                                        //to Get Blasted User Details of the Parent Scribble
                                                        $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['blast_id'], $loggedUserID);
                                                }
                                                $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                        } else {
                                                $parentId = $parentScrib['scribble_id'];
                                                $parentScrib['notifications_type'] = "scribble";
                                                $blastedScribbleInfo = $scribbleObj->getScribleInfo($parentId);
                                                //$allScribbles[$key] = $blastedScribbleInfo;
                                                $allScribbles[$key] = $parentScrib;
                                                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                                $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                                //  check if the Parent scribble is Blasted
                                                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                                $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                                $blastedUsrAry = array();
                                                if($blastedUsrCnt > 0) {
                                                        //to Get Blasted User Details of the Parent Scribble
                                                        $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                                }
                                                $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                                if($blastedScribbleInfo['from_user_id'] == $loggedUserID) {
                                                        $allScribbles[$key]['isUpDownScribble']="2";
                                                        $allScribbles[$key]['UpDownAction']="2";
                                                        $allScribbles[$key]['isBlastedScribble']="2";
                                                }
                                                if($blastedScribbleInfo['from_user_id'] != $loggedUserID) {
                                                        $updownAry = array();
                                                        $updownAry = $scribbleObj->getUpDownActionByUserId($parentId, $loggedUserID);
                                                        if(empty($updownAry)) {
                                                                $allScribbles[$key]['isUpDownScribble']="0";
                                                                $allScribbles[$key]['UpDownAction']="0";
                                                        }
                                                        if(!empty($updownAry)) {
                                                                $allScribbles[$key]['isUpDownScribble']= $updownAry[0]['isUpDownScribble'];
                                                                $allScribbles[$key]['UpDownAction']= $updownAry[0]['UpDownAction'];
                                                        }
                                                        $blastAry = array();
                                                        $blastAry = $scribbleObj->getBlastActionByUserId($parentId, $loggedUserID);
                                                        if(empty($blastAry)) {
                                                                $allScribbles[$key]['isBlastedScribble']="0";
                                                        }
                                                        if(!empty($blastAry)) {
                                                                $allScribbles[$key]['isBlastedScribble']=$blastAry[0]['isBlastedScribble'];
                                                        }
                                                }
                                        }
                                }
                                $scribArray = $allScribbles;
                        }
                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($scribArray) > 0) {
                        $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                        foreach($scribArray as $key=>$scrib) {
                                if($scrib['isBlastedScribble'] == '')
                                    $scribArray[$key]['isBlastedScribble'] = "0";
                                if(!empty($scrib['created_date']))
                                    $scribArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                                $scribArray[$key]['notifications_type'] = "scribble";
                                $scrib['notifications_type'] = "scribble";
                                if(!empty($scrib['username']))
                                    $scribArray[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                        }
                }
                if(!empty($scribArray)) {
                    $resultAry = array('service_status'=>'success',"content" => $scribArray, "responsecode" => '500');
                } else {
                    $resultAry = array('service_status'=>'error',"error_message" => "No Scribbles Found", "responsecode" => '500');
                }
                echo $myjson->customEncode($resultAry);
                return true;
        }

        public function getmoreuserinfoscribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $req = $this->getRequest();
                $uid=$req->getParam('user_id');
                if($uid == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!', "userinfoscribblesCode"=>"1000"));
                        return;
                }
                if(!is_numeric($uid)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!', "userinfoscribblesCode"=>"1000"));
                        return;
                }
                $loggedUserID = $req->getParam('logged_user_id');
                if($loggedUserID == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!', "userinfoscribblesCode"=>"1000"));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!', "userinfoscribblesCode"=>"1000"));
                        return;
                }
                if($req->getParam('limit') != '')
                        $limit=$req->getParam('limit');
                else
                        $limit = 30;
                $scribble_id = $req->getParam('scribble_id');
                if($scribble_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Scribble Id can not be Empty!!!', "userinfoscribblesCode"=>"1000"));
                        return;
                }
                //$old_scribbleId = $scribble_id;
                $type = $req->getParam('type');
                if($type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Type can not be Empty!!!', "userinfoscribblesCode"=>"1000"));
                        return;
                }
                $userObj = new User_Model_User();
                $usrLvlObj = new User_Model_Userlevels();
                // check whether the given user exist or not
                $usrArray = array();
                $usrArray = $userObj->getUserDetails($uid);
                if(count($usrArray)  != 1) {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist', "userinfoscribblesCode"=>"1000");
                     $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                     echo $myjson->customEncode($resultAry);
                    return false;
                }
                if($loggedUserID == $uid) {
                        // Fetch LOgged USer Level
                        $usrLevelObj = new User_Model_Userlevels();
                        $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
                        $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];
                        // TO get User Scribbles
                        $parentScribbles =array();
                        $allScribbles =array();
                        $parentScribbles = $scribbleObj->getUserParentScribbles($uid,$loggedUserID,"U", $loggedUserLevel, $limit, $scribble_id, $type);
                        foreach($parentScribbles as $key =>$parentScrib) {
                                $parentId = $parentScrib['scribble_id'];
                                $parentScrib['notifications_type'] = "scribble";
                                $allScribbles[$key] = $parentScrib;
                                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                                $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                //  check if the Parent scribble is Blasted
                                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                $blastedUsrAry = array();
                                if($blastedUsrCnt > 0) {
                                    //to Get Blasted User Details of the Parent Scribble
                                    $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                }
                                $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                // As USer can not Updown/ Blast his own scribbles
                                $allScribbles[$key]['isUpDownScribble'] ='2';
                                $allScribbles[$key]['UpDownAction'] ='2';
                                $allScribbles[$key]['isBlastedScribble'] ='2';
                        }
                        $scribArray = $allScribbles;
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                // Get Scribbles only If Logged User is following Other User
                                // get logged User Level
                                $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                                $logged_user_level = $usrlevel[0]['user_level'];
                                $parentScribbles =array();
                                $allScribbles =array();
                                $parentScribbles = $scribbleObj->getUserParentScribbles($uid,$loggedUserID,"U", $logged_user_level, $limit, $scribble_id, $type);
                                foreach($parentScribbles as $key =>$parentScrib) {
//                                        $parentId = $parentScrib['scribble_id'];
//                                        $parentScrib['notifications_type'] = "scribble";
//                                        $allScribbles[$key] = $parentScrib;
//                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
//                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
//                                        //  check if the Parent scribble is Blasted
//                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
//                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
//                                        $blastedUsrAry = array();
//                                        if($blastedUsrCnt > 0) {
//                                            //to Get Blasted User Details of the Parent Scribble
//                                            $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
//                                        }
//                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                        if($parentScrib['blast_id'] != 0){
                                            $blastedScribbleInfo = array();
                                            $parentId = $parentScrib['blast_id'];
                                            $parentScrib['notifications_type'] = "scribble";
                                            $blastedScribbleInfo = $scribbleObj->getScribleInfo($parentId);
                                            $allScribbles[$key] = $blastedScribbleInfo;

                                            // TO check if Logged user BLASTED or Up or Down the parent Scribble
                                            if($blastedScribbleInfo['from_user_id'] == $loggedUserID){
                                                $allScribbles[$key]['isUpDownScribble']="2";
                                                $allScribbles[$key]['UpDownAction']="2";
                                                $allScribbles[$key]['isBlastedScribble']="2";
                                            }
                                            if($blastedScribbleInfo['from_user_id'] != $loggedUserID){
                                                //echo $blastedScribbleInfo[0]['from_user_id']."--".$loggedUserID."--".$parentId;print_r($blastedScribbleInfo);exit;
                                                $updownAry = array();
                                                $updownAry = $scribbleObj->getUpDownActionByUserId($parentId, $loggedUserID);
                                                if(empty($updownAry)){
                                                    $allScribbles[$key]['isUpDownScribble']="0";
                                                    $allScribbles[$key]['UpDownAction']="0";
                                                }
                                                if(!empty($updownAry)){
                                                    $allScribbles[$key]['isUpDownScribble']= $updownAry[0]['isUpDownScribble'];
                                                    $allScribbles[$key]['UpDownAction']= $updownAry[0]['UpDownAction'];
                                                }

                                                $blastAry = array();
                                                $blastAry = $scribbleObj->getBlastActionByUserId($parentId, $loggedUserID);
                                                if(empty($blastAry)){
                                                    $allScribbles[$key]['isBlastedScribble']="0";
                                                }
                                                if(!empty($blastAry)){
                                                    $allScribbles[$key]['isBlastedScribble']=$blastAry[0]['isBlastedScribble'];
                                                }

                                            }

                                            $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                            $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                            //  check if the Parent scribble is Blasted
                                            $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['blast_id']);
                                            $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                            $blastedUsrAry = array();
                                            if($blastedUsrCnt > 0) {
                                                //to Get Blasted User Details of the Parent Scribble
                                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['blast_id'], $loggedUserID);
                                            }
                                            $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                         } else {
                                            $parentId = $parentScrib['scribble_id'];
                                            $parentScrib['notifications_type'] = "scribble";
                                            $allScribbles[$key] = $parentScrib;
                                            $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                            $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                            //  check if the Parent scribble is Blasted
                                            $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                            $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                            $blastedUsrAry = array();
                                            if($blastedUsrCnt > 0) {
                                                //to Get Blasted User Details of the Parent Scribble
                                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                            }
                                            $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;

                                            // TO check if Logged user BLASTED or Up or Down the parent Scribble
                                            if($parentScrib['from_user_id'] == $loggedUserID){
                                                $allScribbles[$key]['isUpDownScribble']="2";
                                                $allScribbles[$key]['UpDownAction']="2";
                                                $allScribbles[$key]['isBlastedScribble']="2";
                                            }
                                            if($parentScrib['from_user_id'] != $loggedUserID){
                                                //echo $blastedScribbleInfo[0]['from_user_id']."--".$loggedUserID."--".$parentId;print_r($blastedScribbleInfo);exit;
                                                $updownAry = array();
                                                $updownAry = $scribbleObj->getUpDownActionByUserId($parentId, $loggedUserID);
                                                if(empty($updownAry)){
                                                    $allScribbles[$key]['isUpDownScribble']="0";
                                                    $allScribbles[$key]['UpDownAction']="0";
                                                }
                                                if(!empty($updownAry)){
                                                    $allScribbles[$key]['isUpDownScribble']= $updownAry[0]['isUpDownScribble'];
                                                    $allScribbles[$key]['UpDownAction']= $updownAry[0]['UpDownAction'];
                                                }

                                                $blastAry = array();
                                                $blastAry = $scribbleObj->getBlastActionByUserId($parentId, $loggedUserID);
                                                if(empty($blastAry)){
                                                    $allScribbles[$key]['isBlastedScribble']="0";
                                                }
                                                if(!empty($blastAry)){
                                                    $allScribbles[$key]['isBlastedScribble']=$blastAry[0]['isBlastedScribble'];
                                                }

                                            }

                                         }

                                }
                                $scribArray = $allScribbles;
                        }
                }
//                // IF Blasted Scribble, Get the Original Owner Details
//                foreach($scribArray as $key=>$scrib) {
//                        if($scrib['blast_id'] != 0) {
//                                $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
//                                $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
//                                $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
//                                $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
//                                $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
//                                if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
//                                        $scribArray[$key]['isBlastedScribble'] = 2;
//                                }
//                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
//                                        $scribArray[$key]['isBlastedScribble'] = 1;
//                                }
//                        }
//                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($scribArray) > 0) {
                        $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                        foreach($scribArray as $key=>$scrib) {
                                $scribArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);




                                $scribArray[$key]['notifications_type'] = "scribble";
                                $scrib['notifications_type'] = "scribble";
                                // if($scrib['notifications_type'] != 'scribble') continue;
                                $scribArray[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($loggedUserID, "U", '1');
                $resultAry = array('service_status'=>'success',"content" => $scribArray, "userinfoscribblesCode"=>"1000", "notificationcnt" => $notificationcnt['notificationcnt']);
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                 echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getuserpostedscribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $req = $this->getRequest();
                $uid=$req->getParam('user_id');
                if($uid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
                }
                $loggedUserID = $req->getParam('logged_user_id');
                if($loggedUserID == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!'));
                        return;
                }
                $limit = (($req->getParam('limit') != '') ? $req->getParam('limit'):30);
                $userDetAry = array();
                $userDetAry = $userObj->getUserDetails($uid);
                if(count($userDetAry) != 1) {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist');
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                $userDetAry = $userDetAry->toArray();
                // to Get User Level Name
                $usrLvlObj = new User_Model_Userlevels();
                $usrlevelName = $usrLvlObj->getUserLevels($userDetAry['user_level']);
                $usrlevelName = $usrlevelName->toArray();
                $userDetAry['user_level_name'] = $usrlevelName['level_name'];
                if($loggedUserID == $uid) {
                        // Fetch LOgged USer Level
                        $usrLevelObj = new User_Model_Userlevels();
                        $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
                        $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];
                        // TO get User Posted Scribbles
                        $parentScribbles =array();
                        $allScribbles =array();
                        $parentScribbles = $scribbleObj->getUserPostedParentScribbles($uid,$loggedUserID,"U", $loggedUserLevel, $limit);
                        foreach($parentScribbles as $key =>$parentScrib) {
                                $parentId = $parentScrib['scribble_id'];
                                $parentScrib['notifications_type'] = "scribble";
                                $allScribbles[$key] = $parentScrib;
                                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                                $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                //  check if the Parent scribble is Blasted
                                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                $blastedUsrAry = array();
                                if($blastedUsrCnt > 0) {
                                        //to Get Blasted User Details of the Parent Scribble
                                        $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                }
                                $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                // As User can not Updown/ Blast his own scribbles
                                $allScribbles[$key]['isUpDownScribble'] ='';
                                $allScribbles[$key]['UpDownAction'] ='';
                                $allScribbles[$key]['isBlastedScribble'] ='';
                        }
                        $scribArray = $allScribbles;
                        $userDetAry['isFollowing'] = "2";
                        $userDetAry['isOtherUserFollowing'] = "2";
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isFollowing'] = "1";
                                // Get Scribbles only If Logged User is following Other User
                                // get logged User Level
                                $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                                $logged_user_level = $usrlevel[0]['user_level'];
                                $parentScribbles =array();
                                $allScribbles =array();
                                $parentScribbles = $scribbleObj->getUserPostedParentScribbles($uid,$loggedUserID,"U", $logged_user_level, $limit);
                                foreach($parentScribbles as $key =>$parentScrib) {
                                        $parentId = $parentScrib['scribble_id'];
                                        $allScribbles[$key] = $parentScrib;
                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                        //  check if the Parent scribble is Blasted
                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                        $blastedUsrAry = array();
                                        if($blastedUsrCnt > 0) {
                                                //to Get Blasted User Details of the Parent Scribble
                                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                        }
                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                }
                                $scribArray = $allScribbles;
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isFollowing'] = "0";
                                $scribArray = array();
                        }
                        // To check if other user following logged User or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($loggedUserID,  'U', $uid, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isOtherUserFollowing'] = "1";
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isOtherUserFollowing'] = "0";
                        }
                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                // IF Blasted Scribble, Get the Original Owner Details
                foreach($scribArray as $key=>$scrib) {
                        if($scrib['blast_id'] != 0) {
                                $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                //echo "<pre>";print_r($ownerArray);exit;
                                $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 2;
                                }
                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 1;
                                }
                        }
                        $scribArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                        $scribArray[$key]['notifications_type'] = "scribble";
                        $scrib['notifications_type'] = "scribble";
                        // if($scrib['notifications_type'] != 'scribble') continue;
                        $scribArray[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                }
                //$userDetAry['scribble_content'] = $scribArray;
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($loggedUserID, "U", '1');
                $resultAry = array('service_status'=>'success',"content" => $scribArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getmoreuserpostedscribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $req = $this->getRequest();
                $uid=$req->getParam('user_id');
                if($uid == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
                }
                $loggedUserID = $req->getParam('logged_user_id');
                if($loggedUserID == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!'));
                        return;
                }
                if($req->getParam('limit') != '')
                        $limit=$req->getParam('limit');
                else
                        $limit = 30;
                $scribble_id = $req->getParam('scribble_id');
                if($scribble_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Scribble Id can not be Empty!!!'));
                        return;
                }
                $type = $req->getParam('type');
                if($type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Type can not be Empty!!!'));
                        return;
                }
                $userDetAry =array();
                $userDetAry = $userObj->getUserDetails($uid);
                if(count($userDetAry) != 1) {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }
                $userDetAry = $userDetAry->toArray();
                // to Get User Level Name
                $usrLvlObj = new User_Model_Userlevels();
                $usrlevelName = $usrLvlObj->getUserLevels($userDetAry['user_level']);
                $usrlevelName = $usrlevelName->toArray();
                $userDetAry['user_level_name'] = $usrlevelName['level_name'];
                if($loggedUserID == $uid) {
                    // Fetch LOgged USer Level
                    $usrLevelObj = new User_Model_Userlevels();
                    $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
                    $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];
                    // TO get User Posted Scribbles
                    $parentScribbles =array();
                    $allScribbles =array();
                    $parentScribbles = $scribbleObj->getUserPostedParentScribbles($uid,$loggedUserID,"U", $loggedUserLevel, $limit, $scribble_id, $type);
                    foreach($parentScribbles as $key =>$parentScrib) {
                            $parentId = $parentScrib['scribble_id'];
                            $parentScrib['notifications_type'] = "scribble";
                            $allScribbles[$key] = $parentScrib;
                            $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                            $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                            //  check if the Parent scribble is Blasted
                            $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                            $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                            $blastedUsrAry = array();
                            if($blastedUsrCnt > 0) {
                                //to Get Blasted User Details of the Parent Scribble
                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                            }
                            $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                            // As User can not Updown/ Blast his own scribbles
                            $allScribbles[$key]['isUpDownScribble'] ='';
                            $allScribbles[$key]['UpDownAction'] ='';
                            $allScribbles[$key]['isBlastedScribble'] ='';

                    }
                    $scribArray = $allScribbles;
                    $userDetAry['isFollowing'] = "2";
                    $userDetAry['isOtherUserFollowing'] = "2";
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isFollowing'] = "1";
                                // Get Scribbles only If Logged User is following Other User
                                // get logged User Level
                                $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                                $logged_user_level = $usrlevel[0]['user_level'];
                                $parentScribbles =array();
                                $allScribbles =array();
                                $parentScribbles = $scribbleObj->getUserPostedParentScribbles($uid,$loggedUserID,"U", $logged_user_level, $limit, $scribble_id, $type);
                                foreach($parentScribbles as $key =>$parentScrib) {
                                        $parentId = $parentScrib['scribble_id'];
                                        $allScribbles[$key] = $parentScrib;
                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                                        //  check if the Parent scribble is Blasted
                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                        $blastedUsrAry = array();
                                        if($blastedUsrCnt > 0) {
                                            //to Get Blasted User Details of the Parent Scribble
                                            $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                        }
                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                }
                                $scribArray = $allScribbles;
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isFollowing'] = "0";
                                $scribArray = array();
                        }
                        // To check if other user following logged User or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($loggedUserID,  'U', $uid, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isOtherUserFollowing'] = "1";
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isOtherUserFollowing'] = "0";
                        }
                }
                // IF Blasted Scribble, Get the Original Owner Details
                foreach($scribArray as $key=>$scrib) {
                        if($scrib['blast_id'] != 0) {
                                $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                //echo "<pre>";print_r($ownerArray);exit;
                                $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 2;
                                }
                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 1;
                                }
                        }
                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($scribArray) > 0) {
                    $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                    foreach($scribArray as $key=>$scrib) {
                        $scribArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                        $scribArray[$key]['notifications_type'] = "scribble";
                        $scrib['notifications_type'] = "scribble";
                        // if($scrib['notifications_type'] != 'scribble') continue;
                        $scribArray[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                    }
                }
                $userDetAry['scribble_content'] = $scribArray;
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($loggedUserID, "U", '1');
                $resultAry = array('service_status'=>'success',"content" => $scribArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function signupAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                
                $insAry = array();
                $userObj = new User_Model_User();
                //check email existancy
                $req = $this->getRequest();
                $lat = 0;
                $lng = 0;
                $lat = $req->getParam('lat');
                $lng = $req->getParam('lng');
                $zipcode = 0;
                $zip = $req->getParam('zipcode');
                if(!empty($zip)) {
                        $zipcode = $zip;
                }
                $country = 0;
                $country_code = 0;
                $state = 0;
                $city = 0;
                if(($zipcode != '') || ($lat != '' && $lng != '')) {
                        if(($country =='' || $state == '' || $city == '' || $zipcode == '') && ($lat != '' && $lng != '')) {
                                $URL = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lng."&sensor=false";
                                $googledata = $this->getgoogledata($URL);
                                $country = $googledata['country'];
                                $country_code = $googledata['country_code'];
                                $state = $googledata['state'];
                                $city = $googledata['city'];
                                $zipcode = $googledata['zipcode'];
                        }
                        if($lat == '' && $lng == '' && $zipcode != '') {
                                $URL = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
                                $googledata = $this->getgoogledata($URL);
                                $country = $googledata['country'];
                                $country_code = $googledata['country_code'];
                                $state = $googledata['state'];
                                $city = $googledata['city'];
                                $zipcode = $googledata['zipcode'];
                        }
                        if($country !='' && $state != '' && $city != '' && $zipcode != '') {
                                $CountryObj = new User_Model_Countries();
                                $countryexists = $CountryObj->getCountryByName($country_code);
                                if(!empty($countryexists)) {
                                        $countryid = $countryexists->id;
                                } else {
                                        $countrydata['country_code'] = $country_code;
                                        $countrydata['country_name'] = $country;
                                        $countryid = $CountryObj->insert($countrydata);
                                }
                                $StateObj = new User_Model_States();
                                $stateexists = $StateObj->getStateByName($state);
                                if(!empty($stateexists)) {
                                        $stateid = $stateexists->id;
                                } else {
                                        $statedata['state_name'] = $state;
                                        $statedata['country_id'] = $countryid;
                                        $stateid = $StateObj->insert($statedata);
                                }
                                $CityObj = new User_Model_Cities();
                                $cityexists = $CityObj->getCityByName($city);
                                if(!empty($cityexists)) {
                                        $cityid = $cityexists->id;
                                } else {
                                        $citydata['city_name'] = $city;
                                        $citydata['state_id'] = $stateid;
                                        $cityid = $CityObj->insert($citydata);
                                }
                                $zipcodeexists = $CityObj->getZipcodeByZip($zipcode);
                                //echo '<pre>';print_r($zipcodeexists);exit;
                                if(!empty($zipcodeexists)) {
                                        $zipid = $zipcodeexists['id'];
                                } else {
                                        $zipdata['zipcode'] = $zipcode;
                                        $zipdata['city_id'] = $cityid;
                                        $zipid = $CityObj->insertzipcode($zipdata);
                                }
                                $country = $countryid;
                                $state = $stateid;
                                $city = $cityid;
                                $zipcode = $zipid;
                        } else {
                            $country = 0;
                            $state = 0;
                            $city = 0;
                            $zipcode = 0;
                            $lat = 0;
                            $lng = 0;
                        }
                        //$URL1 = "http://maps.google.co.uk/maps/geo?q=" . urlencode($zipcide.",USA") . "&output=csv";
                        /*$URL1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false&output=csv";
                        $data1 = file_get_contents($URL1);
                        $data1 = json_decode($data1);
                        $lat = $data1->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
                        $lng = $data1->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};*/
                }
                //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo 'GD1:<pre>';print_r($req->getParams());exit; }
                $userEmail = $req->getParam('email');
                if(!preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $userEmail)) {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"417","content" => 'Invalid Email Format', "error_message" => 'Invalid Email Format');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                if(!$userObj->isEmailAvail($userEmail)) {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"417","content" => 'Email not available' ,"error_message" => 'Email not available');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                //check username existancy
                $userName = $req->getParam('username');
                if(!$userObj->isUsernameAvail($userName)) {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"404","content" => 'Username already exist',"error_message" => 'Username already exist');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                if(strlen($userName) < '6' || strlen($userName) > '15') {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"404","content" => 'Username should be between 6 to 15 characters',"error_message" => 'Username should be between 6 to 15 characters');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                $password = $req->getParam('password');
                if(strlen($password) < '6') {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"404","content" => 'Password should be minimum 6 characters',"error_message" => 'Password should be minimum 6 characters');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                /*if(strlen($userName) > '15') {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"404","content" => 'Username should be maximum 15 characters');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }*/
                if(!preg_match('/^[a-zA-Z]{1}[a-zA-Z0-9]+$/', $userName)) {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"404","content" => 'Username Allows Alphanumeric or only-Alpabets and first letter should be alphabet',"error_message" => 'Username Allows Alphanumeric or only-Alpabets and first letter should be alphabet');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') {
                    $request = $this->getRequest()->getParams();
                } else {
                    $request = $this->getRequest()->getPost();
                }


                //echo '<pre>';print_r($request);exit;
                //$insAry = $request;
                //insert in database
                foreach($request as $key => $val) {
                    if($val != '' && $val != '(null)' && $val != 'mkey' && $key != 'mkey' && $val != 'lat' && $key != 'lat' && $val != 'lng' && $key != 'lng' && $val != 'zipcode' && $key != 'zipcode' && $val != 'osType' && $key != 'osType' && $val != 'upmeVersion' && $key != 'upmeVersion'&& $val != 'module' && $key != 'module'&& $val != 'controller' && $key != 'controller'&& $val != 'action' && $key != 'action') {
                        $insAry[$key] = $val;
                        if($key == 'password')
                            $insAry[$key] = md5($val.SECURITY_SALT);
                    }
                }

                $is_celeb = 0;
                $insCeleb = 0;
                $insAry['register_from'] = 'M';
                $insAry['country'] = $country;
                $insAry['state'] = $state;
                $insAry['city'] = $city;
                $insAry['zipcode'] = $zipcode;
                if($lat != '' && $lng != '') {
                    $insAry['lat'] = $lat;
                    $insAry['lng'] = $lng;
                }
                $actCode = md5($userEmail);
                $insAry['activationcode'] = $actCode;
                if($insAry['user_type'] == 'C') {
                    $insAry['user_type'] = 'J';
                    $is_celeb = 1;
                }

                $ins = $userObj->insertNewUser($insAry);
                // insert in Celeb Temp Table for Admin Approval
                if($ins && $is_celeb == 1) {
                    // insert in Celeb Temp Table for Admin Approval
                    $insCeleb = $userObj->insertCeleb($ins);
                }

                // FOR PUSH NOTIFICATIONS
                $mkey = $this->getRequest()->getParam('mkey');
                if($mkey != '') {
                    $os_type = '';
                    $os_type = $this->getRequest()->getParam('osType');
                    if($os_type == 'android') {
                        $push_unique_id = '';
                        $push_unique_id = $this->getRequest()->getParam('push_unique_token');
                        /*if($push_unique_id == '') {
                            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Push notification Id can not be Empty!!!'));
                            return;
                        }*/
                        // Check if any active user with the same push notification Id
                        $activeUserByPushId = array();
                        $userObj = new User_Model_User();
                        $activeUserByPushId = $userObj->getUserByPushNotificationId($push_unique_id);
                        $cnt = count($activeUserByPushId);
                        if($cnt > 0){
                            $tempAry = array();
                            $tempAry['push_unique_token'] = '';
                            $tempAry['os_type'] = '';
                            $ids = '';
                            foreach($activeUserByPushId as $key=>$tmpUsr) {
                                if($key < $cnt-1)
                                    $ids .= "'".$tmpUsr['user_id']."',";
                                if($key == $cnt-1)
                                    $ids .= "'".$tmpUsr['user_id']."'";
                            }
                            $upd = $userObj->updatePushIdByUserId($ids, $tempAry);
                        }
                        $updatePushArr = array();
                        $updatePushArr['push_unique_token'] = $push_unique_id;
                        $updatePushArr['os_type'] = $os_type;
                        $upd = $userObj->updatePushIdByUserId($ins, $updatePushArr);
                    }
                }

                if($_SERVER['SERVER_NAME'] != '192.168.1.57') {
                    if($insCeleb && $is_celeb == 1) {
                        // send mail seeking approval, to Admin regarding New celebrity registration.
                        $adminMailId = 'varaprasad@rizecorp.net';
                        //$adminMailId = 'narayana@rizecorp.net';

                        $m = new UpmeSocial_HtmlMailer();
                        $m->setSubject("UPMEsocial - New Celebrity Registered");
                        $m->addTo($adminMailId)
                            ->setViewParam('user_id',$ins)
                            ->setViewParam('firstname',$req->getParam('firstname'))
                            ->setViewParam('lastname',$req->getParam('lastname'))
                            ->setViewParam('username',$req->getParam('username'))
                            ->setViewParam('password',$password)//$postAry['password']
                            ->setViewParam('email',$userEmail);
                        $m->sendHtmlTemplate("celebrity_registration.phtml");
                    }
                }
                // Updating Coolpoints
                $coolid = '';
                $cool['user_id'] = $ins;
                $cool['user_type'] = 'U';
                $cool['cool_points'] = '100';
                $coolid = $userObj->insertcoolpoints($cool);
                /***** Mail Functionality For Registration Without Payment *****/
                if($_SERVER['SERVER_NAME'] != '192.168.1.57') {
                        $m = new UpmeSocial_HtmlMailer();
                        $m->setSubject("UPMEsocial - Login Credentials");
                        $m->addTo($userEmail)
                            ->setViewParam('user_id',$ins)
                            ->setViewParam('firstname',$req->getParam('firstname'))
                            ->setViewParam('lastname',$req->getParam('lastname'))
                            ->setViewParam('username',$req->getParam('username'))
                            ->setViewParam('password',$password)//$postAry['password']
                            ->setViewParam('email',$userEmail)
                            ->setViewParam('actlink',$actCode);
                        $m->sendHtmlTemplate("logindetails.phtml");
                }
                if($ins) {
                    $resultAry = array('service_status'=>'success',"content" => $ins,"user_level" => "1");
                } else {
                    $resultAry = array('service_status'=>'error',"content" => 'Error Processing request',"error_message" => 'Error Processing request');
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
        }

        function getgoogledata($URL) {
                $country = 0;
                $state = 0;
                $city = 0;
                $zipcode = 0;
                $country_code = 0;
                $geodata = array();
                $data = file_get_contents($URL);
                $data =json_decode($data, true);
                foreach ($data['results'] as $result) {
                        foreach ($result["address_components"] as $address) {
                                // Repeat the following for each desired type
                                if($city == '') {
                                        // Repeat the following for each desired type
                                        if (in_array("locality", $address["types"])) {
                                                $city= $address["long_name"];
                                        }
                                } else if($state == '') {
                                        // Repeat the following for each desired type
                                        if (in_array("administrative_area_level_1", $address["types"])) {
                                                $state = $address["long_name"];
                                        }
                                } else if($country == '') {
                                        if (in_array("country", $address["types"])) {
                                                $country_code = $address["short_name"];
                                                $country = $address["long_name"];
                                        }
                                } else if($zipcode == '') {
                                        // Repeat the following for each desired type
                                        if (in_array("postal_code", $address["types"])) {
                                                $zipcode = $address["long_name"];
                                        }
                                }
                        }
                }
                $geodata['country'] = $country;
                $geodata['country_code'] = $country_code;
                $geodata['state'] = $state;
                $geodata['city'] = $city;
                $geodata['zipcode'] = $zipcode;
                return $geodata;
        }

        public function fileuploadAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                //$photo_type = $this->getRequest()->getParam('image_category');
                $userId = $this->getRequest()->getParam('user_id');
                if($userId == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                $w = $this->getRequest()->getParam('width');
                $h = $this->getRequest()->getParam('height');
                $aw = $this->getRequest()->getParam('android_width');
                $ah = $this->getRequest()->getParam('android_height');
                $width = 0;
                $height = 0;
                $android_width = 0;
                $android_height = 0;
                if(isset($w) && $w != '')
                    $width = $w;
                if(isset($h) && $h != '')
                    $height = $h;
                if(isset($aw) && $aw != '')
                    $android_width = $aw;
                if(isset($ah) && $ah != '')
                    $android_height = $ah;

                if(!empty($_FILES['image']['name'])) {
                        $dir_upload = UPLOAD_PATH.'images/original/';
                        $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                        $medium_path = UPLOAD_PATH.'images/medium/';
                        $mobile_path = UPLOAD_PATH.'images/mobile/';
                        $android_path = UPLOAD_PATH.'images/android/';
                        $fileName = $_FILES['image']['name'];
                        $image = new UpmeSocial_Controller_Action_Helper_Image();
                        $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
                        
                        // Upload images at Amazone S3 server
                        $uploadfiles = new Mobile_Model_Uploadfiles();
                        $resp = $uploadfiles->uploadImagesAtS3Server($arrFileName['image'],$userId);

                        //Saving data based on phototype
                        $userObj = new User_Model_User();
                        $updateAry['profile_pic_path'] = IMAGES_PATH.$userId."/".$arrFileName['image'];
                        //Assigning album data
                        $album['user_id'] = $userId;
                        $album['user_type'] = 'U';
                        $album['album_name'] = 'Profile Photos';
                        $album['is_default'] = '1';
                        $mobileobj = new Mobile_Model_Albums();
                        $aid = $mobileobj->albumnameexistsrnot($album);
                        $album['created_date'] = date('Y-m-d H:i:s');
                        if(!empty($aid['id'])) {
                            $albumid = $mobileobj->update($album, $aid['id']);
                            $albumid = $aid['id'];
                        } else
                            $albumid = $mobileobj->insert($album);
                        $photoObj = new Mobile_Model_Photos();
                        $coverphoto = $photoObj->albumidexistsrnot($albumid,$userId,'U');
                        //Assigning photo data
                        $data['album_id'] = $albumid;
                        $data['photo_name'] = 'Profile Photo';
                        $data['photo_original_name'] = IMAGES_PATH.$userId."/".$fileName;
                        $data['photo_modified_name'] = IMAGES_PATH.$userId."/".$arrFileName['image'];
                        $data['width'] = $arrFileName['width'];
                        $data['height'] = $arrFileName['height'];
                        $data['android_width'] = $arrFileName['android_width'];
                        $data['android_height'] = $arrFileName['android_height'];
                        $data['user_id'] = $userId;
                        $data['user_type'] = 'U';
                        if(!empty($coverphoto['photo_id']))
                                $data['cover_photo'] = '0';
                        else
                                $data['cover_photo'] = '1';
                        $data['description'] = '';
                        $data['created_date'] = date('Y-m-d H:i:s');
                        $data['blasted_date'] = $data['created_date'];
                        $photo_id = $photoObj->insert($data);
                        $id = $userObj->updateUser($updateAry, $userId);
                        if (!empty($arrFileName)) {
                                $resultAry = array('service_status'=>'success',"content" => 'File Uploaded Successfully',"profile_pic_path"=>IMAGES_PATH.$userId."/".$arrFileName['image']);//$arrFileName
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => '');
                        }
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                         echo $myjson->customEncode($resultAry);
                        return false;
                }
        }

        public function videouploadAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $userId = $this->getRequest()->getParam('user_id');
                if($userId == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!empty($_FILES['video']['name'])) {
                        $dir_upload = UPLOAD_PATH.'videos/';
                        if(isset($_FILES['video'])) {
                                $original_name = $_FILES['video']['name'];
                                $ext = pathinfo($_FILES['video']['name']);
                                $fileId = time();
                                $modifiedName = $userId.'_'.time().'.mp4';//.$ext['extension'];
                                $_FILES['video']['name'] = $userId.'_'.time().'.mp4';//.$ext['extension'];
                                $_FILES['video']['posted_by'] = $userId;
                        }

                        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/logs/app.log');
                        $logger = new Zend_Log($writer);
                        $logger->info('--------------------------- Profile video upload ------------------------');


                        $upload = new Zend_File_Transfer_Adapter_Http();
                        $upload->setDestination($dir_upload);
                        $config = array(
                                        'uploadPath'		=> UPLOAD_PATH.'videos/',
                                        'outputPath'		=> UPLOAD_PATH.'videos/',
                                        'thumbPath'		=> UPLOAD_PATH.'videos/',
                                        'conversionLog'		=> '/var/www/convertvideo/flvfiles/conversionLog.log',
                                        'errorLog'		=> '/var/www/convertvideo/flvfiles/errorLog.log',
                                        'conversionScript' 	=> '/var/www/convertvideo/processVideo.php',
                                        'outputFormat'		=> 'mp4', // either 'mp4' or 'flv'
                                        'bitRate'		=> 32000,
                                        'sampleRate'		=> 22050,
                                        'videoMaxWidth'		=> 320,
                                        'videoMaxHeight' 	=> 240,
                                        'thumbMaxWidth' 	=> 320,
                                        'thumbMaxHeight'	=> 240,
                                        'minDuration'		=> 1,
                                        'videoThumbDepth'	=> 25, // % into video to get thumbnail
                                );
                        $file = isset($_FILES['video']) ? $_FILES['video'] : null;
                        $logger->info('processVideo  ....!!!');

                        $converter = new UpmeSocial_Controller_Action_Helper_VideoConverter($file, $config);
                        $details = $converter->getDetails();
                        $converter->processVideo();

                        $logger->info('============== profile upload at s3 ================');
                        //upload videos at s3 server
                        $uploadfilesObj = new Mobile_Model_Uploadfiles();
                        $res1 = $uploadfilesObj->uploadVideosAtS3Server($modifiedName,$details['thumbnail'],$userId);

                        $logger->info('=============== Profile encode videos ================');
                        $jobId = $uploadfilesObj->videoEncode($fileId,$userId);
                        $logger->info('jobId............'.$jobId);

                        $outputFile = $userId."/".$userId.$fileId."/".$userId."_".$fileId.".m3u8";
                        $logger->info($fileId.$userId.".mp4");
                        $logger->info('================ success =============================');

                        if($jobId !=''){
                        $userObj = new User_Model_User();
                        $updateAry['profile_video_path'] = VIDEOS_PATH."".$outputFile; 
                        $updateAry['profile_video_thumbnail_path'] = VIDEOS_THUMBNAIL_PATH.$userId."/".$details['thumbnail'];

                        $videoObj = new Videos_Model_Videos();
                        $videoAry['video_user_name'] = 'Profile';
                        $videoAry['description'] = 'Profile video';
                        $videoAry['zencoder_job_id'] = $jobId;
                        $videoAry['video_name'] = VIDEOS_PATH.$original_name;
                        $videoAry['video_path'] = VIDEOS_PATH."".$outputFile;
                        $videoAry['thumbnail_path'] = VIDEOS_THUMBNAIL_PATH.$userId."/".$details['thumbnail'];
                        $videoAry['user_id'] = $userId;
                        $videoAry['created_date'] = date('Y-m-d H:i:s');
                        $videoAry['blasted_date'] = $videoAry['created_date'];
                        $video_id = $videoObj->insertVideo($videoAry);

                        $id = $userObj->updateUser($updateAry, $userId);
                        if(!empty($id)) {
                                $resultAry = array('service_status'=>'success',"content" => $id);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => '');
                        }
                        }

                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();               
                         echo $myjson->customEncode($resultAry);
                        return false;
                }
        }

        public function facebookemailcheckAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $data['email'] = $this->getRequest()->getParam('email');
                $data['unique_id'] = $this->getRequest()->getParam('unique_id');
                $userObj = new User_Model_User();
                $checkdata = $userObj->checkemailexistsrnot($data);
                if(!empty($checkdata)) {
                        $resultAry = array('service_status'=>'success',"content" => '415',"userid" => $checkdata->user_id, "user_level" => $checkdata->user_level);//415--->Email already Exists
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => '416');//416-->Email Available
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getscribblesAction() {
                if($_SERVER['REMOTE_ADDR'] != '192.168.1.57') {
                    $this->_helper->layout->disableLayout();
                }
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $scribArray = array();
                $request = $this->getRequest();
                $user_id = $request->getParam('user_id');
                if($user_id == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                    return;
                }
                $loggedUserID = $request->getParam('logged_user_Id');
                if($loggedUserID == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                    return;
                }
                $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):10);
                $userObj = new User_Model_User();
                $scrObj = new Scribbles_Model_Scribbles();
                $usrLvlObj = new User_Model_Userlevels();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                // check whether the given user exist or not
                $usrArray = array();
                $usrArray = $userObj->getUserDetails($user_id);
                if(count($usrArray) != 1) {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist',"responsecode"=>"1000");
                    echo $myjson->customEncode($resultAry);
                    return false;
                }
                $parentScribbles =array();
                $allScribbles =array();
                $newScribAry = array();
                if($loggedUserID == '' || $user_id == $loggedUserID) {  // IF logged user id viewing his own profile
                        $owner_id = $user_id;
                        $owner_level = $usrArray->user_level;
                } else {
                        $owner_id = $loggedUserID;
                        // get logged User Level
                        $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                        $owner_level = $usrlevel[0]['user_level'];
                }

                /////// TO GET PARENT SCRIBBLES
                $parentScribbles = $scrObj->getParentScribbles($user_id,$owner_id,$usrArray->user_type, $owner_level, $limit);
                $scribArray = $parentScribbles;
                // IF Blasted Scribble, Get the Original Owner Details
                foreach($scribArray as $key=>$scrib) {
                        $parentId1 = $scrib['scribble_id'];
                        if($scrib['blast_id'] != 0) {
                                // To Get Owner Details of the Blasted Scribble
                                $ownerArray = $scrObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                $newScribAry = $ownerArray['0'];
                                $followObj = new User_Model_Followers();
                                $userfollowing = $followObj->isLoggedUserFollowingOtherUser($ownerArray['0']['user_id'],  'U', $loggedUserID, "U");
                                if(count($userfollowing) > 0) {
                                        $newScribAry['isUpDownScribble'] = $scrib['isUpDownScribble'];
                                        $newScribAry['UpDownAction'] = $scrib['UpDownAction'];
                                        $newScribAry['isBlastedScribble'] = $scrib['isBlastedScribble'];
                                } else {
                                        $newScribAry['isUpDownScribble'] = $scrib['isUpDownScribble'];
                                        $newScribAry['UpDownAction'] = $scrib['UpDownAction'];
                                        $newScribAry['isBlastedScribble'] = "2";
                                }
//                                $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
//                                $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
//                                $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
//                                $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
//                                if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
//                                        $scribArray[$key]['isBlastedScribble'] = 2;
//                                }
//                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
//                                        $scribArray[$key]['isBlastedScribble'] = 1;
//                                }
                                $newScribAry['owner_user_id'] = $ownerArray[0]['user_id'];
                                $newScribAry['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                $newScribAry['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                $newScribAry['owner_user_name'] = $ownerArray[0]['username'];
//                                if($newScribAry['owner_user_id'] == $loggedUserID && $newScribAry['owner_user_type'] == "U") {
//                                        $newScribAry['isBlastedScribble'] = 2;
//                                }
//                                if($newScribAry['owner_user_id'] != $loggedUserID && $newScribAry['owner_user_type'] == "U") {
//                                       $newScribAry['isBlastedScribble'] = 1;
//                                }
                                $scribArray[$key] = $newScribAry;
                                $parentId1 = $scrib['blast_id'];
                        }
                        // check if any child scribbles
                        $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId1, $loggedUserID,$loggedUserID,"U", $owner_level);
                        $scribArray[$key]['childScribbles'] = $childScribblesCnt;
                        $scribArray[$key]['notifications_type'] = "scribble";
                        //  check if the Parent scribble is Blasted
                        $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentId1);
                        $scribArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                        $blastedUsrAry = array();
                        if($blastedUsrCnt > 0) {
                                //to Get Blasted User Details of the Parent Scribble
                                $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentId1, $loggedUserID);
                        }
                        $scribArray[$key]['BlastedUsers'] = $blastedUsrAry;
                }

                // for Other Notifications
                $mixedAry = array();
                $mixedAry = $scribArray;
                $scribbleCnt = count($scribArray);
                if($scribbleCnt > 0) {
                        $lastScribbleDate = $scribArray[0]['created_date'];
                        $lastScribbleDate1 = $scribArray[$scribbleCnt-1]['created_date'];
                        // get Logged User Following Users-list FOR getting Notifications
                        $followObj = new User_Model_Followers();
                        $loggedUserFollowing = $followObj->getFollowingIdsByUserID($loggedUserID, "U");
                        if(count($loggedUserFollowing) > 0) {
                                $followIdsArr = array();
                                foreach($loggedUserFollowing as $following) {
                                        $followIdsArr[] = $following['follower_id'];
                                }
                                $followerIds = implode(",", $followIdsArr);
                        } else $followerIds = "";
                        $limit2 = 10;
                        $cnt = count($scribArray);
                        // TO Get Notifications for Purchase Coupons
                        $notificationAry = array();
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationAry= $notificationObj->getFollowerCouponNotifications($user_id, $loggedUserID,$usrArray->user_type, $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2);
                        if(count($notificationAry) > 0) {
                                $mixedAry = $scribArray;
                                foreach($notificationAry as $notification) {
                                        $i = $cnt++;
                                        $mixedAry[$i] = $notification;
                                        $mixedAry[$i]['created_date'] = $notification['action_date'];
                                }
                        }
                        if($user_id == $loggedUserID) {
                                // TO Get all Huddle Targets of the Logged in User
                                $huddleArray = array();
                                $huddleArray = $notificationObj->getHuddleNotifications($loggedUserID,$usrArray->user_type, $lastScribbleDate, $lastScribbleDate1, $limit2);
                                if(count($huddleArray) > 0) {
                                        foreach($huddleArray as $huddle) {
                                                $i = $cnt++;
                                                $mixedAry[$i] = $huddle;
                                                $mixedAry[$i]['created_date'] = $huddle['action_date'];
                                        }
                                }
                        }
                        // if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                        //echo "<pre>";print_r($mixedAry);exit;
                        // to get Photo Uploaded or Blasted Notifications
                        if($request->getParam('upmeVersion') != '') {
                            $photosObj = new Mobile_Model_Photos();
                            $uType = $usrArray->user_type;
                            if($uType != "U")
                                $uType = "U";
                            $photoArrDetails = $photosObj->getPhotoUploadBlasts($user_id, $loggedUserID, "U", $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2);
                            //echo '<pre>';print_r($photoArrDetails);exit;
                            if(count($photoArrDetails) > 0) {
                                foreach($photoArrDetails as $photo) {
                                        $photo_user_type = $photo['user_type'];
                                        if($photo['user_type'] != 'U')
                                                $photo_user_type = "U";
                                        if($loggedUserID == $photo['user_id'] && $photo_user_type == "U") {
                                                $photo['isBlasted'] = "2";
                                        }
                                        //echo "<pre>";print_r($photo);exit;
                                        $i = $cnt++;
                                        $mixedAry[$i] = $photo;
                                        $mixedAry[$i]['id'] = $photo['photo_id'];
                                        // to get Blasted Users Count
                                        $photoBlastedUserCnt = $photosObj->getBlastedUserCountBasedOnPhotoId($photo['photo_id']);
                                        $blastedUserDetails = array();
                                        //echo $photoBlastedUserCnt;exit;
                                        if($photoBlastedUserCnt > 0) {
                                            $limit = 2; // get 2 blasted users
                                            $blastedUserDetails = $photosObj->getBlastedUserDetailsBasedOnPhotoId($photo['photo_id'], $loggedUserID, $limit);
                                            $photoCount = 0;
                                            foreach($blastedUserDetails as $blastUser)
                                            {
                                                $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                                                $blastedUserDetails[$photoCount] = $blastUser;
                                                $photoCount++; 
                                            }
                                            //echo "<pre>";print_r($blastedUserDetails);exit;
                                        }

                                        $photoCommentsArr = $photosObj->getPhotoComment($mixedAry[$i],$photo);

                                        if(count($photoCommentsArr) > 0) {
                                               $j =1;
                                            foreach($photoCommentsArr as $comment) {
                                                $cmntIndex = 'comment'.$j;
                                                $name = 'commenter_name'.$j;
                                                $id = 'commenter_id'.$j;
                                                $useType = 'commenter_type'.$j;
                                                $pic_path = 'profile_pic_path'.$j;
                                                $date = 'commenter_date'.$j;
                                                $cmtGender = 'commenter_gender'.$j;
                                                if($comment['comments'] != '') {
                                                    $mixedAry[$i][$cmntIndex]=$comment['comments'];
                                                    $mixedAry[$i][$cmtGender]=$comment['gender'];
                                                    if($comment['usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['user_id'];
                                                        $mixedAry[$i][$useType]='U';
                                                        $mixedAry[$i][$pic_path]=$comment['profile_pic_path'];
                                                        $mixedAry[$i][$name]=$comment['firstname']." ".$comment['lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                    if($comment['B_usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['business_id'];
                                                        $mixedAry[$i][$useType]="B";
                                                        $mixedAry[$i][$pic_path]=$comment['image_path'];
                                                        $mixedAry[$i][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                }
                                                $j++;
                                            } // foreach
                                        } // commentArr Cnt
                                            
                                        $mixedAry[$i]['cmntCnt'] = $photosObj->getPhotoCommentCnt($photo);
                                        $mixedAry[$i]['created_date'] = $photo['blasted_date'];
                                        $mixedAry[$i]['date'] = $timeObj->humaneDate($photo['blasted_date']);
                                        $mixedAry[$i]['notifications_type'] = "photo";
                                        $mixedAry[$i]['mediatype'] = "photo";
                                        $mixedAry[$i]['photoblastedUserCnt'] = $photoBlastedUserCnt;
                                        $mixedAry[$i]['photoblastedUsers'] = $blastedUserDetails;
                                }
                            }
                        }



                        if($request->getParam('upmeVersion') != '') {
                            $videosObj = new Mobile_Model_Videos();
                            $uType = $usrArray->user_type;
                            if($uType != "U")
                                $uType = "U";
                            $videoArrDetails = $videosObj->getVideoUploadBlasts($user_id, $loggedUserID, "U", $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2);
                            //echo '<pre>';print_r($photoArrDetails);exit;
                            if(count($videoArrDetails) > 0) {
                                foreach($videoArrDetails as $video) {
                                        $video_user_type = $video['user_type'];
                                        if($video['user_type'] != 'U')
                                                $video_user_type = "U";
                                        if($loggedUserID == $video['user_id'] && $video_user_type == "U") {
                                                $video['isBlasted'] = "2";
                                        }
                                        

                                        $videoBlastedUserCnt = $videosObj->getBlastedUserCountBasedOnVideoId($video['video_id']);

                                        if($loggedUserID == $video['user_id'] && $videoBlastedUserCnt == 0)
                                            {}else{
                                        $i = $cnt++;
                                        $mixedAry[$i] = $video;
                                        $mixedAry[$i]['id'] = $video['video_id'];
                                        // to get Blasted Users Count
                                        
                                        $blastedUserDetails = array();
                                        //echo $videoBlastedUserCnt;exit;
                                        if($videoBlastedUserCnt > 0) {
                                            $limit = 2; // get 2 blasted users
                                            $blastedUserDetails = $videosObj->getBlastedUserDetailsBasedOnVideoId($video['video_id']);
                                            $videoCount = 0;
                                            foreach($blastedUserDetails as $blastUser)
                                            {
                                                $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                                                $blastedUserDetails[$videoCount] = $blastUser;
                                                $videoCount++; 
                                            }
                                        }


                                        $videoCommentsArr = $videosObj->getVideoComment($mixedAry[$i],$video);

                                            if(count($videoCommentsArr) > 0) {
                                               $j =1;
                                            foreach($videoCommentsArr as $comment) {
                                                $cmntIndex = 'comment'.$j;
                                                $name = 'commenter_name'.$j;
                                                $id = 'commenter_id'.$j;
                                                $useType = 'commenter_type'.$j;
                                                $pic_path = 'profile_pic_path'.$j;
                                                $date = 'commenter_date'.$j;
                                                $cmtGender = 'commenter_gender'.$j;
                                                if($comment['comments'] != '') {
                                                    $mixedAry[$i][$cmntIndex]=$comment['comments'];
                                                    $mixedAry[$i][$cmtGender]=$comment['gender'];
                                                    if($comment['usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['user_id'];
                                                        $mixedAry[$i][$useType]='U';
                                                        $mixedAry[$i][$pic_path]=$comment['profile_pic_path'];
                                                        $mixedAry[$i][$name]=$comment['firstname']." ".$comment['lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                    if($comment['B_usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['business_id'];
                                                        $mixedAry[$i][$useType]="B";
                                                        $mixedAry[$i][$pic_path]=$comment['image_path'];
                                                        $mixedAry[$i][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                }
                                                $j++;
                                            } // foreach
                                        } // commentArr Cnt

                                        $mixedAry[$i]['cmntCnt'] = $videosObj->getvideoCommentCnt($video);
                                        $mixedAry[$i]['created_date'] = $video['blasted_date'];
                                        $mixedAry[$i]['date'] = $timeObj->humaneDate($video['blasted_date']);
                                        $mixedAry[$i]['notifications_type'] = "video";
                                        $mixedAry[$i]['mediatype'] = "video";
                                        $mixedAry[$i]['videoblastedUserCnt'] = $videoBlastedUserCnt;
                                        $mixedAry[$i]['videoblastedUsers'] = $blastedUserDetails;


                                }
                            }
                            }
                        }



                        //}
                        //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo '<pre>';print_r($mixedAry); }
                        $orderByDate = array();
                        foreach ($mixedAry as $key => $row) {
                                $orderByDate[$key]  = strtotime($row['created_date']);
                        }
                        array_multisort($orderByDate, SORT_DESC, $mixedAry);
                        //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53' || $_SERVER['REMOTE_ADDR'] == '192.168.1.21' ) { echo '<pre>';print_r($mixedAry);exit; }
                }
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                    //echo "<pre>";print_r($mixedAry);exit;
                }
                // for changing TIME format of CREATED DATE
                $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                foreach($mixedAry as $key=>$scrib) {
                        $mixedAry[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                        $mixedAry[$key]['date'] = $mixedAry[$key]['created_date'];
                        if($scrib['notifications_type'] != 'scribble' && $scrib['notifications_type'] != 'photo' && $scrib['notifications_type'] != 'video') continue;
                                $mixedAry[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                }
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                    //echo "<pre>";print_r($mixedAry);exit;
                }
                //if($_SERVER['REMOTE_ADDR'] == '192.168.1.21') { echo '<pre>';print_r($mixedAry);exit; }
                // if($_SERVER['REMOTE_ADDR'] == '192.168.1.53' || $_SERVER['REMOTE_ADDR'] == '192.168.1.21' || $_SERVER['REMOTE_ADDR'] == '192.168.1.88' ) { echo '<pre>';print_r($mixedAry);exit; }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($loggedUserID, "U", '1');
                //Level Message CheckUp
                $leveldata = $userObj->isuserinleveldetails($loggedUserID);
                if($leveldata['level_status'] == 'Up') {
                    $popup_message = 'Your Level is Upgraded';
                } elseif($leveldata['level_status'] == 'Down') {
                    $popup_message = 'Your Level is Degraded';
                } else {
                    $popup_message = '';
                }
                $id = $userObj->deleteleveldetail($leveldata['id']);
                //Send Response to mobile success r not
                if (!empty($mixedAry)) {
                        if(!empty($popup_message)) {
                            $resultAry = array('service_status'=>'success',"content" => $mixedAry, "notificationcnt" => $notificationcnt['notificationcnt'],"popup_message" => $popup_message,"responsecode"=>"1000");
                        } else {
                            $resultAry = array('service_status'=>'success',"content" => $mixedAry, "notificationcnt" => $notificationcnt['notificationcnt'],"responsecode"=>"1000");
                        }
                } else {
                        $resultAry = array('service_status'=>'success',"content" => array(),"responsecode"=>"1000");
                }
            	echo $myjson->customEncode($resultAry);
            	return false;
        }

        public function addalbumAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $req = $this->getRequest();

                $uid=$req->getParam('user_id');
                if($uid == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                    return;
                }
                if(!is_numeric($uid)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                    return false;
                }
                $user_type = $req->getParam('user_type');
                if($user_type == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User type can not be Empty!!!'));
                    return false;
                }

                $album_name = $req->getParam('album_name');
                if($album_name == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Album name can not be Empty!!!'));
                    return false;
                }

                $data['user_id'] = $uid;
                $data['user_type'] = $user_type;
                $data['album_name'] = $album_name;
                $mobileobj = new Mobile_Model_Albums();
                $val = $mobileobj->albumnameexistsrnot($data);
                $data['created_date'] = date('Y-m-d H:i:s');
                if(!empty($val))
                        $albumArr = '';
                else
                        $albumArr = $mobileobj->insert($data);
                if (!empty($albumArr)) {
                        $resultAry = array('service_status'=>'success',"content" => $albumArr);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'Album Already Exists');
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function addphotostoalbumAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();

                $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/logs/app.log');
                $logger = new Zend_Log($writer);
                $logger->info('<br>----------------- zencodercallbackAction -----------------------------------');
                //echo "<pre>";print_r($this->getRequest()->getParams());exit;
                $album_id = $this->getRequest()->getParam('album_id');
                if($album_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Album Id can not be Empty!!!'));
                        return;
                }
                $user_id = $this->getRequest()->getParam('user_id');
                if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                $user_type = $this->getRequest()->getParam('user_type');
                if($user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                $photo_name = $this->getRequest()->getParam('photo_name');
                if($photo_name == '')
                        $photo_name = '';
                $description = $this->getRequest()->getParam('description');
                if($description == '')
                        $description = '';
                $w = $this->getRequest()->getParam('width');
                $h = $this->getRequest()->getParam('height');
                $aw = $this->getRequest()->getParam('android_width');
                $ah = $this->getRequest()->getParam('android_height');
                $randomCode = $this->getRequest()->getParam('random_code');
                if($randomCode == '')
                        $randomCode = '';
                $gps_location = $this->getRequest()->getParam('gps_location');
                if($gps_location == '')
                        $gps_location = '';
                $gps_lat = $this->getRequest()->getParam('gps_lat');
                if($gps_lat == '')
                        $gps_lat = '';
                $gps_lng = $this->getRequest()->getParam('gps_lng');
                if($gps_lng == '')
                        $gps_lng = '';
                $width = 0;
                $height = 0;
                $android_width = 0;
                $android_height = 0;
                if(!empty($w))
                        $width = $w;
                if(!empty($h))
                        $height = $h;
                if(!empty($aw))
                        $android_width = $aw;
                if(!empty($ah))
                        $android_height = $ah;
                $userObj = new Mobile_Model_Photos();
                if(!empty($_FILES['image']['name'])) {
                        $dir_upload = UPLOAD_PATH.'images/original/';
                        $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                        $medium_path = UPLOAD_PATH.'images/medium/';
                        $mobile_path = UPLOAD_PATH.'images/mobile/';
                        $android_path = UPLOAD_PATH.'images/android/';
                        $huddle_path = UPLOAD_PATH.'images/huddle/';
                        $fileName = $_FILES['image']['name'];
                        //$fullPathNameFile = $dir_upload.$fileName;
                        $image = new UpmeSocial_Controller_Action_Helper_Image();
                        $rotateAngle = $this->getRequest()->getParam('angle');
                        $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path,'',$rotateAngle);
                        $coverphoto = $userObj->albumidexistsrnot($album_id,$user_id,$user_type);
                        
                        // Upload images at Amazone S3 server
                        $uploadfiles = new Mobile_Model_Uploadfiles();
                        $resp = $uploadfiles->uploadImagesAtS3Server($arrFileName['image'],$user_id);

                        //Saving data based on phototype
                        $data['album_id'] = $album_id;
                        $data['photo_name'] = $photo_name;
                        $data['photo_original_name'] = IMAGES_PATH.$user_id."/".$fileName;
                        $data['photo_modified_name'] = IMAGES_PATH.$user_id."/".$arrFileName['image'];
                        $data['width'] = $arrFileName['width'];
                        $data['height'] = $arrFileName['height'];
                        $data['android_width'] = $arrFileName['android_width'];
                        $data['android_height'] = $arrFileName['android_height'];
                        $data['user_id'] = $user_id;
                        $data['user_type'] = $user_type;
                        if(!empty($coverphoto['photo_id']))
                                $data['cover_photo'] = '0';
                        else
                                $data['cover_photo'] = '1';
                        $data['description'] = $description;
                        $data['created_date'] = date('Y-m-d H:i:s');
                        $data['blasted_date'] = $data['created_date'];
                        $data['random_code'] = $randomCode;
                        $data['gps_location'] = $gps_location;
                        $data['gps_lat'] = $gps_lat;
                        $data['gps_lng'] = $gps_lng;
                        $photo_id = $userObj->insert($data);

                        //Photo notification 
                        $resAry['reference_id'] = $photo_id;
                        $resAry['user_id'] = $user_id;
                        $resAry['user_type'] = $user_type;
                        $resAry['notifications_type'] = 'photo_uploads';
                        $resAry['action_date'] = date('Y-m-d H:i:s');
                        $notificationObj = new Notifications_Model_Notifications();
                        $notification_id = $notificationObj->insertNewRecord($resAry);

                        $logger->info('<br>-------------photo is...'.$photo_id." description...........".$description);
                        //Enter description as first comment 
                        if(!empty($photo_id) && !empty($description))
                        {
                            $dataArr['user_id'] = $user_id;
                            $dataArr['user_type'] = $user_type;
                            $dataArr['photo_id'] = $photo_id;
                            $dataArr['comments'] = $description;
                            $dataArr['commented_date'] = date('Y-m-d H:i:s');
                            $photocmntObj = new Photos_Model_Photocomments();
                            $id = $photocmntObj->savephotocomment($dataArr);
                        }
                        $logger->info('<br>-------------photo is...'.$photo_id." description...........".$description);

                        //Send Response to mobile success r not
                        if (!empty($photo_id)) {
                                $resultAry = array('service_status'=>'success',"content" => $photo_id);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => 'Some error Occured');
                        }


                        echo $myjson->customEncode($resultAry);
                        return false;
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'Upload Images');
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
        }

        public function updatephotostoalbumAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $album_id = $this->getRequest()->getParam('album_id');
                if($album_id == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Album Id can not be Empty!!!'));
                    return;
                }
                $user_id = $this->getRequest()->getParam('user_id');
                if($user_id == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                    return;
                }
                $user_type = $this->getRequest()->getParam('user_type');
                if($user_type == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Type can not be Empty!!!'));
                    return;
                }
                if($user_type != '' && $user_type != 'B')
                    $user_type = "U";

                $photo_name = $this->getRequest()->getParam('photo_name');
                $description = $this->getRequest()->getParam('description');
                if($photo_name == '' && $description == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Enter either Photo Name or Description!!!'));
                    return;
                }
                $randomCode = $this->getRequest()->getParam('random_code');
                if($randomCode == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Random code can not be Empty!!!'));
                    return;
                }
                $photoObj = new Mobile_Model_Photos();
                // check if Photo records Exist or not by Random Code
                $photoArray = $photoObj->getPhotoDetailsByRandomCode($album_id, $user_id, $user_type, $randomCode);
                //echo "<pre>";print_r($photoArray);exit;
                if(empty($photoArray)){
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Some error occured. Please try again!!!'));
                    return false;
                }

                //Updating data based on album Id, user Id and Random Code
                $data['photo_name'] = $photo_name;
                $data['description'] = $description;
                $data['random_code'] = '';
                $updStatus = $photoObj->updatePhotoDetailsByRandomCode($data, $randomCode);
                if ($updStatus) {
                    echo $myjson->customEncode(array('service_status'=>'success'));
                    return false;
                } else {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Some error occured. Please try again!!!'));
                    return false;
                }
        }

        public function displayalbumsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $user_id = $this->getRequest()->getParam('user_id');
                if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                $user_type = $this->getRequest()->getParam('user_type');
                if($user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                $mobileobj = new Mobile_Model_Albums();
                $albums = $mobileobj->getAlbumsByUserId($user_id,$user_type);
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($user_id, $user_type, '1');
                if (!empty($albums)) {
                        $resultAry = array('service_status'=>'success',"content" => $albums, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Albums To View');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function displayphotosAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                //$user_id = $this->getRequest()->getParam('user_id');
                //$user_type = $this->getRequest()->getParam('user_type');
                $album_id = $this->getRequest()->getParam('album_id');
                if($album_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Album Id can not be Empty!!!'));
                        return;
                }
                $logged_user_id = $this->getRequest()->getParam('logged_user_id');
                if($logged_user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                $userObj = new Mobile_Model_Photos();
                $photos = $userObj->getalbumphotos($album_id, $logged_user_id,"U");//,$user_id,$user_type
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($photos as $key=>$photo) { //echo '<pre>';print_R($photo);exit;
                        if($logged_user_id == $photo['user_id']) {
                                $photos[$key]['isBlasted'] = '2';
                        }
                        $photos[$key]['created_date'] = $timeObj->humaneDate($photo['created_date']);
                        $photos[$key]['notifications_type'] = "photo";
                        $photos[$key]['mediatype'] = "photo";
                        $photos[$key]['name'] = $photos[$key]['photo_modified_name'];

                        if(!empty($photo['commented_date1']) && !empty($photo['commented_date2'])) {
                                $photos[$key]['commented_date1'] = $timeObj->humaneDate($photo['commented_date1']);
                                $photos[$key]['commented_date2'] = $timeObj->humaneDate($photo['commented_date2']);
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_id, "U", '1');
                if (!empty($photos)) {
                        $resultAry = array('service_status'=>'success',"content" => $photos, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Photos to View');
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function displayphotowithcommentsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $user_type = $this->getRequest()->getParam('logged_user_type');
                if($user_type == '')
                        $user_type = "U";
                $user_id = $this->getRequest()->getParam('logged_user_id');
                if($user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($user_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Logged User  Id!!!'));
                        return;
                }
                $userObj = new User_Model_User();
                $available = $userObj->isUserAvail($user_id);
                if(!empty($available)) {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not available');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }
                $photo_id = $this->getRequest()->getParam('photo_id');
                $photoObj = new Mobile_Model_Photos();
                // to getch Photo details along with user and blast details
                $photoArr = $photoObj->getPhotoDetailsByPhotoId($user_id, $user_type, $photo_id);
                $photosDetails = $photoArr->toArray();
                // get Photo COmments
                $photoCommentsArr = $photoObj->getphotocomments($photo_id);
                $photoComments = $photoCommentsArr->toArray();
                $Comments = array();
                $CommentsNewArray = array();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($photoComments as $key=>$cmnts) {
                        if($cmnts['user_type'] == 'U') {
                                $Comments['user_posted'] =$cmnts['user_posted'];
                                $Comments['comments'] =$cmnts['comments'];
                                $Comments['commented_date'] =$timeObj->humaneDate($cmnts['commented_date']);
                                $Comments['user_type'] =$cmnts['user_type'];
                                $Comments['gender'] =$cmnts['gender'];
                                $Comments['profile_pic_path'] =$cmnts['profile_pic_path'];
                                $Comments['username'] =$cmnts['firstname']." ".$cmnts['lastname'];
                                $Comments['firstname'] =$cmnts['firstname'];
                                $Comments['realname'] =$cmnts['realname'];
                                $Comments['lastname'] =$cmnts['lastname'];
                        } elseif($cmnts['user_type'] == 'B') {
                                $Comments['user_posted'] =$cmnts['user_posted'];
                                $Comments['comments'] =$cmnts['comments'];
                                $Comments['commented_date'] =$timeObj->humaneDate($cmnts['commented_date']);
                                $Comments['user_type'] =$cmnts['user_type'];
                                $Comments['profile_pic_path'] =$cmnts['image_path'];
                                $Comments['username'] =$cmnts['business_name'];
                                $Comments['gender'] =$cmnts['gender'];
                                $Comments['realname'] =$cmnts['realname'];
                                $Comments['firstname'] =$cmnts['business_firstname'];
                                $Comments['lastname'] =$cmnts['business_lastname'];
                        }
                        $CommentsNewArray[] = $Comments;
                }
                if (!empty($photosDetails)) {
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($user_id, $user_type, '1');
                        if(!empty($photoComments)) {
                                $resultAry = array('service_status'=>'success',"photo_content" => $photosDetails, "content"=>$CommentsNewArray, "commentsCount"=>count($photoComments), "notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"403");
                        } else {
                                $resultAry = array('service_status'=>'success',"photo_content" => $photosDetails, "content"=>array(), "commentsCount"=>count($photoComments), "notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"403");
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Photos to View',"usermediacode"=>"403");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function deletealbumAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                if($_SERVER["REMOTE_ADDR"] == '192.168.1.57'){
                    //echo "<pre>";print_r($this->getRequest()->getParams());exit;
                }
                $album_id = $this->getRequest()->getParam('album_id');
                if($album_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Album Id can not be Empty!!!'));
                        return;
                }
                $mobileobj = new Mobile_Model_Albums();
                $data = $mobileobj->albumexistsrnot($album_id);
                if(!empty($data)) {
                        $albums = $mobileobj->deleteAlbum($album_id);
                        $resultAry = array('service_status'=>'success',"content" => 'Album Successfully Deleted');
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Album Doesn't exists");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function deletephotoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $photo_id = $this->getRequest()->getParam('photo_id');
                if($photo_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Photo Id can not be Empty!!!'));
                        return;
                }
                $userObj = new Mobile_Model_Photos();
                $data = $userObj->getphotobyphotoid($photo_id);
                if(!empty($data)) {
                        $photos = $userObj->deletePhoto($photo_id);
                        $resultAry = array('service_status'=>'success',"content" => 'Photo Successfully Deleted');
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Photo Doesn't exists");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function addphotocommentAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $uid=$request->getParam('user_id');
                if($uid == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
                }
                $user_type = $request->getParam('user_type');
                if($user_type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Type can not be Empty!!!'));
                        return;
                }
                $photo_id=$request->getParam('photo_id');
                if($photo_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Photo Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($photo_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Photo Id!!!'));
                        return;
                }
                $comment = $request->getParam('comments');
                if($comment == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Comment can not be Empty!!!'));
                        return;
                }
                if(strlen($comment) > 150) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Comment limit of 150 characters exceded'));
                        return false;
                }
                $data['user_id'] = $uid;
                $data['user_type'] = $user_type;
                $data['photo_id'] = $photo_id;
                $data['comments'] = $comment;
                $data['commented_date'] = date('Y-m-d H:i:s');
                $photocmntObj = new Photos_Model_Photocomments();
                $photoObj = new Mobile_Model_Photos();
                $id = $photocmntObj->savephotocomment($data);
                $photocomments = $photoObj->getphotocomments($photo_id);
                $photocomments = $photocomments->toArray();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($photocomments as $key=>$comment) {
                        $photocomments[$key]['commented_date'] = $timeObj->humaneDate($comment['commented_date']);
                }
                if(!empty($id)) {
                        $resultAry = array('service_status'=>'success',"content" => 'Comment Posted Successfully, comment_id= '.$id, "Comments" => $photocomments);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Some Error Occured");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();               echo $myjson->customEncode($resultAry);
                return false;
        }

        public function addvideosAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $userId = $this->getRequest()->getParam('user_id');

                if($userId == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                $userType = $this->getRequest()->getParam('user_type');
                if($userType == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }

                $videoName = $this->getRequest()->getParam('video_name');
                if($videoName == '')
                        $videoName = '';

                $description = $this->getRequest()->getParam('description');
                if($description == '')
                        $description = '';

                $randomCode = $this->getRequest()->getParam('random_code');
                if($randomCode == '')
                        $randomCode = '';

                $gpsLocation = $this->getRequest()->getParam('gps_location');
                if($gpsLocation == '')
                        $gpsLocation = '';

                $gpsLat = $this->getRequest()->getParam('gps_lat');
                if($gpsLat == '')
                        $gpsLat = '';

                $gpsLng = $this->getRequest()->getParam('gps_lng');
                if($gpsLng == '')
                        $gpsLng = '';
                
                if(empty($_FILES['video']['name'])) {
                        $resultAry = array('service_status'=>'error',"content" => 'Upload Video');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }
                $dir_upload = UPLOAD_PATH.'videos/';
                if(isset($_FILES['video'])) {
                        $original_name = $_FILES['video']['name'];
                        $ext = pathinfo($_FILES['video']['name']);
                        $fileId = time();
                        $modifiedName = $userId.'_'.time().'.'.$ext['extension'];
                        $_FILES['video']['name'] = $userId.'_'.time().'.'.$ext['extension'];
                        $_FILES['video']['posted_by'] = $userId;

                }

                $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/logs/app.log');
                $logger = new Zend_Log($writer);
                $logger->info(' --------------------------- video upload ------------------------------');


                $upload = new Zend_File_Transfer_Adapter_Http();
                $upload->setDestination($dir_upload);
                $config = array(
                                'uploadPath'		=> UPLOAD_PATH.'videos/',
                                'outputPath'		=> UPLOAD_PATH.'videos/',
                                'thumbPath'		=> UPLOAD_PATH.'videos/',
                                'conversionLog'		=> '/var/www/convertvideo/flvfiles/conversionLog.log',
                                'errorLog'		=> '/var/www/convertvideo/flvfiles/errorLog.log',
                                'conversionScript' 	=> '/var/www/convertvideo/processVideo.php',
                                'outputFormat'		=> 'mp4', // either 'mp4' or 'flv'
                                'bitRate'		=> 32000,
                                'sampleRate'		=> 22050,
                                'videoMaxWidth'		=> 320,
                                'videoMaxHeight' 	=> 240,
                                'thumbMaxWidth' 	=> 320,
                                'thumbMaxHeight'	=> 240,
                                'minDuration'		=> 1,
                                'videoThumbDepth'	=> 25, // % into video to get thumbnail
                        );

                $file = isset($_FILES['video']) ? $_FILES['video'] : null;

                $converter = new UpmeSocial_Controller_Action_Helper_VideoConverter($file, $config);
                $details = $converter->getDetails();
                $converter->processVideo();

                $logger->info('---------------------- upload at s3 ---------------------');
                //upload videos at s3 server
                $video_id = '';
                $uploadfilesObj = new Mobile_Model_Uploadfiles();
                $res1 = $uploadfilesObj->uploadVideosAtS3Server($modifiedName,$details['thumbnail'],$userId);

                $logger->info('----------------------  encode videos   -------------------');
                $jobId = $uploadfilesObj->videoEncode($fileId,$userId);
                $logger->info('jobId............'.$jobId);

                $outputFile = $userId."/".$userId.$fileId."/".$userId."_".$fileId.".m3u8";
                $logger->info($fileId.$userId.".mp4");
                $logger->info('success  ....!!!');

                //Saving data based on phototype
                 $videoObj = new Videos_Model_Videos();
                $updateAry['video_user_name'] = $videoName;
                $updateAry['video_name'] = VIDEOS_PATH.$original_name;
                $updateAry['video_path'] = VIDEOS_PATH."".$outputFile;//$details['title'].'.'.$ext['extension'];
                $updateAry['thumbnail_path'] = VIDEOS_THUMBNAIL_PATH.$userId."/".$details['thumbnail'];
                $updateAry['user_id'] = $userId;
                $updateAry['user_type'] =$userType;
                $updateAry['description'] = $description;
                $updateAry['random_code'] = $randomCode;
                $updateAry['gps_location'] = $gpsLocation;
                $updateAry['gps_lat'] = $gpsLat;
                $updateAry['gps_lng'] = $gpsLng;
                $updateAry['zencoder_job_id'] = $jobId;
                $updateAry['created_date'] = date('Y-m-d H:i:s');
                $updateAry['blasted_date'] = $updateAry['created_date'];
                $video_id = $videoObj->insertVideo($updateAry);

                //inser description as first comment
                if(!empty($video_id) && !empty($description))
                {
                $dataArr['user_id'] = $userId;
                $dataArr['user_type'] = $userType;
                $dataArr['video_id'] = $video_id;
                $dataArr['comments'] = $description;
                $dataArr['commented_date'] = date('Y-m-d H:i:s');
                $videoCmntObj = new Videos_Model_Videocomments();
                $id = $videoCmntObj->savevideocomment($dataArr);
                }


                //Notification data For video
                
                // $resAry['reference_id'] = $video_id;
                // $resAry['user_id'] = $userId;
                // $resAry['user_type'] = 'U';
                // $resAry['notifications_type'] = 'video_uploads';
                // $resAry['action_date'] = date('Y-m-d H:i:s');
                // $notificationObj = new Notifications_Model_Notifications();
                // $video_id = $notificationObj->insertNewRecord($resAry);
                if(empty($video_id)) {
                        $resultAry = array('service_status'=>'error',"error_msg" => '');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                
                         echo $myjson->customEncode($resultAry);
                        return false;
                }
                //convert uploaded video
               	$resultAry = array('service_status'=>'success',"content" => $video_id);
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                
                 echo $myjson->customEncode($resultAry);
                return false;
                /* Command to convert from .mov to flv
                    ffmpeg -i "/var/www/convertvideo/movies/1364445061.mov" -ar 44100 -ab 96 -f flv "/var/www/convertvideo/flvfiles/1364445061.flv"
                    Command to convert from .mov to .mp4
                    avconv -i "/var/www/upme/public/uploads/gallery/videos/1_1364551161.mp4" -f mp4 -vcodec copy -acodec aac -strict experimental "/var/www/upme/public/uploads/gallery/videos/1_1364551161.mp4"
                */
        }

        public function displayvideosAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $user_id = $this->getRequest()->getParam('user_id');
                $logged_user_id = $this->getRequest()->getParam('logged_user_id');
                $videoObj = new Mobile_Model_Videos();
                $videosArr = $videoObj->getvideosByUserID($user_id,$logged_user_id,"U");
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($videosArr as $key=>$video) {
                        $videosArr[$key]['created_date'] = $timeObj->humaneDate($video['created_date']);
                        if(!empty($video['commented_date1']) && !empty($video['commented_date2'])) {
                                $videosArr[$key]['commented_date1'] = $timeObj->humaneDate($video['commented_date1']);
                                $videosArr[$key]['commented_date2'] = $timeObj->humaneDate($video['commented_date2']);
                        }

                        $videosArr[$key]['notifications_type'] = "video";
                        $videosArr[$key]['mediatype'] = "video";
                        $videosArr[$key]['name'] = $videosArr[$key]['video_path'];

                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_id, "U", '1');
                if (!empty($videosArr)) {
                        $resultAry = array('service_status'=>'success',"content" => $videosArr,"notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Videos to View');
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function addvideocommentAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $uid=$request->getParam('user_id');
                if($uid == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
                }
                $user_type = $request->getParam('user_type');
                if($user_type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Type can not be Empty!!!'));
                        return;
                }
                $video_id=$request->getParam('video_id');
                if($video_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Video Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($video_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Video Id!!!'));
                        return;
                }
                $comment = $request->getParam('comments');
                if($comment == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Comment can not be Empty!!!'));
                        return;
                }
                if(strlen($comment) > 150) {
                   $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Comment limit of 150 characters exceded'));
                   return false;
                }
                $data['user_id'] = $uid;
                $data['user_type'] = $user_type;
                $data['video_id'] = $video_id;
                $data['comments'] = $comment;
                $data['commented_date'] = date('Y-m-d H:i:s');
                $videoObj = new Mobile_Model_Videos();
                $videoCmntObj = new Videos_Model_Videocomments();
                $id = $videoCmntObj->savevideocomment($data);
                $videoCommentsArr = $videoObj->getvideocomments($video_id);
                $videoComments = $videoCommentsArr->toArray();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($videoComments as $key=>$comment) {
                        $videoComments[$key]['commented_date'] = $timeObj->humaneDate($comment['commented_date']);
                } if(!empty($id)) {
                        $resultAry = array('service_status'=>'success',"content" => 'Comment Posted Successfully, comment_id= '.$id, "Comments" => $videoComments);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Some Error Occured");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function displayvideowithcommentsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $video_id = $request->getParam('video_id');
                $videoObj = new Mobile_Model_Videos();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }
                // to getch Photo details along with user and blast details
                $videoArr = $videoObj->getVideoDetailsByVideoId($logged_user_Id, $logged_user_type, $video_id);
                $videoDetails = $videoArr->toArray();
                // get Photo COmments
                $videoCommentsArr = $videoObj->getvideocomments($video_id);
                $videoComments = $videoCommentsArr->toArray();
                $Comments = array();
                $CommentsNewArray = array();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($videoComments as $key=>$cmnts) {
                        if($cmnts['user_type'] == 'U') {
                                $Comments['user_posted'] = $cmnts['user_posted'];
                                $Comments['comments'] = $cmnts['comments'];
                                $Comments['commented_date'] = $timeObj->humaneDate($cmnts['commented_date']);
                                $Comments['user_type'] = $cmnts['user_type'];
                                $Comments['profile_pic_path'] = $cmnts['profile_pic_path'];
                                $Comments['gender'] = $cmnts['gender'];
                                $Comments['username'] = $cmnts['firstname']." ".$cmnts['lastname'];
                                $Comments['firstname'] = $cmnts['firstname'];
                                $Comments['lastname'] = $cmnts['lastname'];
                                $Comments['realname'] =$cmnts['realname'];
                        } elseif($cmnts['user_type'] == 'B') {
                                $Comments['user_posted'] = $cmnts['user_posted'];
                                $Comments['comments'] = $cmnts['comments'];
                                $Comments['commented_date'] = $timeObj->humaneDate($cmnts['commented_date']);
                                $Comments['user_type'] = $cmnts['user_type'];
                                $Comments['profile_pic_path'] = $cmnts['image_path'];
                                $Comments['gender'] = $cmnts['gender'];
                                $Comments['username'] = $cmnts['business_name'];
                                $Comments['firstname'] = $cmnts['business_firstname'];
                                $Comments['lastname'] = $cmnts['business_lastname'];
                                $Comments['realname'] =$cmnts['realname'];
                        }
                        $CommentsNewArray[] = $Comments;
                }
                if (!empty($videoDetails)) {
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                        if(!empty($videoComments)) {
                                $resultAry = array('service_status'=>'success',"photo_content" => $videoDetails, "content"=>$CommentsNewArray, "commentsCount"=>count($videoComments),"notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"403");
                        } else {
                                $resultAry = array('service_status'=>'success',"photo_content" => $videoDetails, "content"=>array(), "commentsCount"=>count($videoComments),"usermediacode"=>"403");
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Video Details to View',"usermediacode"=>"403");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function playvideoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $videothumbnail = SITE_URL.'images/you_tube.jpg';
                $resultAry = array('service_status'=>'success',"content" => $videothumbnail);
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function deletevideoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $video_id = $this->getRequest()->getParam('video_id');
                $videoObj = new Videos_Model_Videos();
                $videos = $videoObj->getvideosbyvideoid($video_id);
                if(!empty($videos)) {
                        $videodata = $videoObj->deleteVideo($videos);
                        $resultAry = array('service_status'=>'success',"content" => 'Video Successfully Deleted');
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Video Doesn't exists");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getusercoolpointsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $user_id = $request->getParam('user_id');
                if($user_id == '') {
                        $resultAry = array('service_status'=>'error',"error_msg" => "User Id can not be Empty!!!");
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }
                if(is_null($user_id)) {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Invalid User Id!!!");
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }
                $uType = "U";
                // Fetching User Cool POints
                $userObj = new User_Model_User();
                $coolPoints = $userObj->getUserCoolPoints($user_id, $uType);
                $resultAry = array('service_status'=>'success',"content" => array('coolPoints'=>$coolPoints));
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function followuserAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $UserId = $request->getParam('user_id');
                if($UserId == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User ID can not be Empty!!!'));
                        return;
                }
                $UserType = $request->getParam('user_type');
                if($UserType == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                $FollowerType = $request->getParam('follower_type');
                if($FollowerType == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Follower Type can not be Empty!!!'));
                        return;
                }
                if($FollowerType != 'B') {
                    $FollowerType = "U";
                }

                $loggedUserId = $request->getParam('logged_user_id');
                if($loggedUserId == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User ID can not be Empty!!!'));
                        return;
                }
                if($loggedUserId == $UserId && $UserType == $FollowerType) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User can not follow Self!!!'));
                        return;
                }
                $insAry = array();
                $insAry['follower_id'] = $UserId;
                $insAry['follower_type'] = $FollowerType;
                $insAry['user_id'] = $loggedUserId;
                $insAry['user_type'] = $UserType;
                //echo "<pre>";print_r($insAry);exit;

                $followObj = new User_Model_Followers();
                // Check if alredy user id Following
                $followObj1 = new Business_Model_Followers();
                $duplicationArray = $followObj1->checkDuplicationFollow($insAry['follower_id'], $insAry['follower_type'], $insAry['user_id'], $insAry['user_type']);
                if(count($duplicationArray) == 0) {
                    // INsert New Follow Record
                    $ins = $followObj->insertNewRecord($insAry);
                    // Insert Notification for Following
                    $resAry['reference_id'] = $ins;
                    $resAry['user_id'] = $UserId;
                    $resAry['user_type'] = $FollowerType;
                    $resAry['notifications_type'] = 'following';
                    $resAry['action_date'] = date('Y-m-d H:i:s');
                    $notificationObj = new Notifications_Model_Notifications();
                    $id = $notificationObj->insertNewRecord($resAry);

                    $resAry['reference_id'] = $ins;
                    $resAry['user_id'] = $loggedUserId;
                    $resAry['user_type'] = $UserType;
                    $resAry['notifications_type'] = 'Followed';
                    $resAry['action_date'] = date('Y-m-d H:i:s');
                    $id = $notificationObj->insertNewRecord($resAry);

                    if($ins) {
                            if($FollowerType != 'B' && $UserType != 'B') {
                                    /****  Upgrade User Level if cool points & Follower/ following criteria reaches Limits ****/
                                    $usrLevelObj = new User_Model_Userlevels();
                                    $scrObj = new Scribbles_Model_Scribbles();

                                    //Get User Level
                                    $userLevel = $usrLevelObj->getUserLevelByUserId($insAry['user_id'], $insAry['user_type']);
                                    //Get All Levels details FROM DB
                                    $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                                    $followerObj = new User_Model_Followers();
                                    // to get Followers Count of logged User
                                    $followersCnt = $followerObj->getFollowersCnt($insAry['user_id'], $insAry['user_type']);

                                    // to Get Following Count of the Logged User
                                    $followingCnt = $followerObj->getFollowingCnt($insAry['user_id'], $insAry['user_type']);
                                    //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                                    foreach($allUserLevelsArray as $level) {
                                        if((($userLevel[0]['cool_points'] >= $level['minimum_coolpoints']  && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']))) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == ($level['level_no']-1)){
                                                $upgradeLevel = $level['level_no'];
                                                //Upgrade User Level
                                                $usrObj = new User_Model_User();
                                                $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                                // Update user level in Downlined scribbles also.
                                                $downlineUserLevel = $upgradeLevel;
                                                $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                                $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                                $lvlAry['level'] = $upgradeLevel;
                                                $lvlAry['level_status'] = 'Up';
                                                $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                                if(!empty($id)) {
                                                    $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                                } else {
                                                    $lvlid = $usrObj->insertleveldetails($lvlAry);
                                                }

                                                //Mail to be sent to the user
                    //                            $m = new UpmeSocial_HtmlMailer();
                    //                            $m->setSubject("UPMEsocial User Level Degrade");
                    //                            $m->addTo($userLevel[0]['email'])
                    //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
                    //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
                    //                                ->setViewParam('new_level',$upgradeLevel)
                    //                                ->setViewParam('email',$userLevel[0]['email'])
                    //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
                    //                            $m->sendHtmlTemplate("level_degrade.phtml");
                                        }
                                    }

                                    //Upgrade User Level if cool points & Follower/ following criteria reaches Limits
                                    $usrLevelObj = new User_Model_Userlevels();
                                    $scrObj = new Scribbles_Model_Scribbles();

                                    //Get User Level
                                    $userLevel = $usrLevelObj->getUserLevelByUserId($insAry['follower_id'], $insAry['follower_type']);
                                    //Get All Levels details FROM DB
                                    $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                                    $followerObj = new User_Model_Followers();
                                    // to get Followers Count of logged User
                                    $followersCnt = $followerObj->getFollowersCnt($insAry['follower_id'], $insAry['follower_type']);

                                    // to Get Following Count of the Logged User
                                    $followingCnt = $followerObj->getFollowingCnt($insAry['follower_id'], $insAry['follower_type']);
                                    //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                                    foreach($allUserLevelsArray as $level) {
                                        if((($userLevel[0]['cool_points'] >= $level['minimum_coolpoints']  && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']))) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == ($level['level_no']-1)){
                                                $upgradeLevel = $level['level_no'];
                                                //Upgrade User Level
                                                $usrObj = new User_Model_User();
                                                $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                                // Update user level in Downlined scribbles also.
                                                $downlineUserLevel = $upgradeLevel;
                                                $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                                $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                                $lvlAry['level'] = $upgradeLevel;
                                                $lvlAry['level_status'] = 'Down';
                                                $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                                if(!empty($id)) {
                                                    $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                                } else {
                                                    $lvlid = $usrObj->insertleveldetails($lvlAry);
                                                }

                                                //Mail to be sent to the user
                    //                            $m = new UpmeSocial_HtmlMailer();
                    //                            $m->setSubject("UPMEsocial User Level Degrade");
                    //                            $m->addTo($userLevel[0]['email'])
                    //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
                    //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
                    //                                ->setViewParam('new_level',$upgradeLevel)
                    //                                ->setViewParam('email',$userLevel[0]['email'])
                    //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
                    //                            $m->sendHtmlTemplate("level_degrade.phtml");
                                        }
                                    }
                                }
                                $notificationcnt = $notificationObj->notificationcnt($loggedUserId, "U", '1');
                                echo $myjson->customEncode(array('service_status'=>'success',"content" =>$ins,'user_id'=>$UserId,"notificationcnt" => $notificationcnt['notificationcnt']));
                                return;
                        } else {
                                echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Error while processing!!!. Please try again.'));
                                return;
                        }
                } else {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" =>"already Following"));
                        return;
                }
        }

        public function unfollowuserAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $user_id = $request->getParam('user_id');
                if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User ID can not be Empty!!!'));
                        return;
                }
                $user_type = $request->getParam('user_type');
                if($user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                $follower_id = $request->getParam('follower_id');
                if($follower_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Follower ID can not be Empty!!!'));
                        return;
                }
                $follower_type = $request->getParam('follower_type');
                if($follower_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Follower Type can not be Empty!!!'));
                        return;
                }
                $followObj = new User_Model_Followers();
                $val = $followObj->deletefollower($user_id,$user_type,$follower_id,$follower_type);
                if($val == '1') {
                    if($follower_type != 'B' && $user_type != 'B') {
                        /****  Upgrade User Level if cool points & Follower/ following criteria reaches Limits ****/
                        $usrLevelObj = new User_Model_Userlevels();
                        $scrObj = new Scribbles_Model_Scribbles();
                        //Get User Level
                        $userLevel = $usrLevelObj->getUserLevelByUserId($user_id, $user_type);
                        //Get All Levels details FROM DB
                        $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                        $followerObj = new User_Model_Followers();
                        // to get Followers Count of logged User
                        $followersCnt = $followerObj->getFollowersCnt($user_id, $user_type);

                        // to Get Following Count of the Logged User
                        $followingCnt = $followerObj->getFollowingCnt($user_id, $user_type);
                        //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                        foreach($allUserLevelsArray as $level) {
                            if((($userLevel[0]['cool_points'] < $level['minimum_coolpoints'] || $followersCnt < $level['minimum_followers']) || ($followingCnt < $level['minimum_followings'])) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == $level['level_no']){
                                    $upgradeLevel = $userLevel[0]['user_level']-1;
                                    //Upgrade User Level
                                    $usrObj = new User_Model_User();
                                    $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                    // Update user level in Downlined scribbles also.
                                    $downlineUserLevel = $upgradeLevel;
                                    $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                    $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                    $lvlAry['level'] = $upgradeLevel;
                                    $lvlAry['level_status'] = 'Down';
                                    $lvlid = $usrObj->insertleveldetails($lvlAry,$lvlAry['user_id']);

                                    //Mail to be sent to the user
        //                            $m = new UpmeSocial_HtmlMailer();
        //                            $m->setSubject("UPMEsocial User Level Degrade");
        //                            $m->addTo($userLevel[0]['email'])
        //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
        //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
        //                                ->setViewParam('new_level',$upgradeLevel)
        //                                ->setViewParam('email',$userLevel[0]['email'])
        //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
        //                            $m->sendHtmlTemplate("level_degrade.phtml");
                            }
                        }

                        /****  Upgrade User Level if cool points & Follower/ following criteria reaches Limits ****/
                        //Get User Level
                        $userLevel = $usrLevelObj->getUserLevelByUserId($follower_id, $follower_type);
                        //Get All Levels details FROM DB
                        $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                        // to get Followers Count of logged User
                        $followersCnt = $followerObj->getFollowersCnt($follower_id, $follower_type);

                        // to Get Following Count of the Logged User
                        $followingCnt = $followerObj->getFollowingCnt($follower_id, $follower_type);
                        //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                        foreach($allUserLevelsArray as $level) {
                            if((($userLevel[0]['cool_points'] < $level['minimum_coolpoints'] || $followersCnt < $level['minimum_followers']) || ($followingCnt < $level['minimum_followings'])) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == $level['level_no']){
                                    $upgradeLevel = $userLevel[0]['user_level']-1;
                                    //Upgrade User Level
                                    $usrObj = new User_Model_User();
                                    $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                    // Update user level in Downlined scribbles also.
                                    $downlineUserLevel = $upgradeLevel;
                                    $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                    $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                    $lvlAry['level'] = $upgradeLevel;
                                    $lvlAry['level_status'] = 'Down';
                                    $lvlid = $usrObj->insertleveldetails($lvlAry,$lvlAry['user_id']);
                                    //Mail to be sent to the user
        //                            $m = new UpmeSocial_HtmlMailer();
        //                            $m->setSubject("UPMEsocial User Level Degrade");
        //                            $m->addTo($userLevel[0]['email'])
        //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
        //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
        //                                ->setViewParam('new_level',$upgradeLevel)
        //                                ->setViewParam('email',$userLevel[0]['email'])
        //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
        //                            $m->sendHtmlTemplate("level_degrade.phtml");
                            }
                        }
                    }
                    $notificationObj = new Notifications_Model_Notifications();
                    $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                    echo $myjson->customEncode(array('service_status'=>'success',"content" =>"You have successfully unfollowed",'notificationcnt' => $notificationcnt['notificationcnt']));
                    return;
                } else {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Error while processing!!!. Please try again.'));
                        return;
                }
        }

        public function createmessageAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $to_user_id = $request->getParam('to_user_id');
                if($to_user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'To User ID can not be Empty!!!',"responsecode"=>"300"));
                        return;
                }
                $from_user_id = $request->getParam('from_user_id');
                if($from_user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'From User ID can not be Empty!!!',"responsecode"=>"300"));
                        return;
                }
                $message = $request->getParam('message');
                if($message == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Message can not be Empty!!!',"responsecode"=>"300"));
                        return;
                }
                $ids = explode(',', $to_user_id);
                $notificationObj = new Notifications_Model_Notifications();
                // array_pop($ids);  //modified by laxman
                foreach($ids as $id) {
                        if($id != '' && strlen($id) > 0) {
                                $data['from_user_id'] = $from_user_id;
                                $data['to_user_id'] = $id;
                                $data['message'] = $message;
                                $data['datetime'] = date('Y-m-d H:i:s');
                                $messageObj = new User_Model_Message();
                                $mid = $messageObj->insert($data);
                                $resAry['reference_id'] = $mid;
                                $resAry['user_id'] = $id;
                                $resAry['user_type'] = 'U';
                                $resAry['notifications_type'] = 'message';
                                $resAry['action_date'] = date('Y-m-d H:i:s');
                                $upgrade = $notificationObj->insertNewRecord($resAry);
                        }
                }
                $notificationcnt = $notificationObj->notificationcnt($from_user_id, "U", '1');
                if(!empty($mid)) {
                        $resultAry = array('service_status'=>'success',"content" => 'Message Sent Successfully', "message_id" => $mid,"notificationcnt" => $notificationcnt['notificationcnt'],"responsecode"=>"300");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Some Error Occured","responsecode"=>"300");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function replymessageAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $data = array();
                $data['parent_id'] = $this->getRequest()->getParam('parent_id');
                if($data['parent_id'] == '') {
                    $resultAry = array('service_status'=>'error',"error_msg" => "Parent Id Can't be Empty","responsecode"=>"301");
                    echo $myjson->customEncode($resultAry);
                    return false;
                }
                $data['from_user_id'] = $this->getRequest()->getParam('from_user_id');
                $data['to_user_id'] = $this->getRequest()->getParam('to_user_id');
                $data['message'] = $this->getRequest()->getParam('message');
                $data['datetime'] = date('Y-m-d H:i:s');
                $messageObj = new User_Model_Message();
                $id = $messageObj->insert($data);
                $resAry['reference_id'] = $id;
                $resAry['user_id'] = $data['to_user_id'];
                $resAry['user_type'] = 'U';
                $resAry['notifications_type'] = 'message';
                $resAry['action_date'] = date('Y-m-d H:i:s');
                $notificationObj = new Notifications_Model_Notifications();
                $upgrade = $notificationObj->insertNewRecord($resAry);
                $notificationcnt = $notificationObj->notificationcnt($data['from_user_id'], "U", '1');
                if(!empty($id)) {
                        $resultAry = array('service_status'=>'success',"content" => 'Message Replied Successfully', 'message_id' => $id,"notificationcnt" => $notificationcnt['notificationcnt'],"responsecode"=>"301");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Some Error Occured","responsecode"=>"301");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function messagestatusAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $msg_id = $this->getRequest()->getParam('message_id');
                $data['receiver_read_status'] = $this->getRequest()->getParam('status');
                $messageObj = new User_Model_Message();
                $id = $messageObj->updateMessagesByMsgid($data, $msg_id);
                if(!empty($id)) {
                        $resultAry = array('service_status'=>'success',"content" => 'Message Status Updated Successfully',"responsecode"=>"302");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Some Error Occured","responsecode"=>"302");
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function inboxmessagesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $user_id = $this->getRequest()->getParam('user_id');
                $pagenum = $this->getRequest()->pagenum;
                if($pagenum == '' || ($pagenum <= 0))
                    $pagenum = 0;
                $limit = $this->getRequest()->getParam('limit');
                if($limit!= '') { $limit = $limit; } else { $limit = 20; }
                $messageObj = new User_Model_Message();
                $messagedata = $messageObj->getInboxMessages($user_id,$limit,$pagenum);
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($messagedata as $key=>$message) {
                        $messagedata[$key]['datetime'] = $timeObj->humaneDate($message['datetime']);
                }
                //echo '<pre>';print_r($messagedata);exit;
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                if(!empty($messagedata)) {
                        $resultAry = array('service_status'=>'success',"content" => $messagedata,"notificationcnt" => $notificationcnt['notificationcnt'],"responsecode"=>"303");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Messages In Inbox To Display","responsecode"=>"303");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function sentmessagesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $user_id = $this->getRequest()->getParam('user_id');
                $messageObj = new User_Model_Message();
                $messagedata = $messageObj->getSentMessages($user_id);
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($messagedata as $key=>$message) {
                        $messagedata[$key]['datetime'] = $timeObj->humaneDate($message['datetime']);
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                if(!empty($messagedata)) {
                        $resultAry = array('service_status'=>'success',"content" => $messagedata,"notificationcnt" => $notificationcnt['notificationcnt'],"responsecode"=>"304");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Messages In SentBox To Display","responsecode"=>"304");
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getmessagesbasedonparentidAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $user_id = $this->getRequest()->getParam('user_id');
                if($user_id == '') {
                        $resultAry = array('service_status'=>'error',"error_msg" => "User Id Can't be Empty","responsecode"=>"400");
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                //$message_id = $this->getRequest()->getParam('message_id');
                $parent_id = $this->getRequest()->getParam('parent_id');
                if($parent_id == '') {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Parent Id Can't be Empty","responsecode"=>"400");
                        echo $myjson->customEncode($resultAry);
                        return false;
                }
                $pagenum = $this->getRequest()->pagenum;
                if($pagenum == '' || ($pagenum <= 0))
                        $pagenum = 0;
                $limit = $this->getRequest()->getParam('limit');
                if($limit!= '') { $limit = $limit; } else { $limit = 10; }
                $messageObj = new User_Model_Message();
                $messagedata = $messageObj->getmessagesbyparentid($parent_id,$limit,$pagenum);
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($messagedata as $key=>$message) {
                        $messagedata[$key]['datetime'] = $timeObj->humaneDate($message['datetime']);
                }
                if(!empty($messagedata)) {
                        $resultAry = array('service_status'=>'success',"content" => $messagedata,"notificationcnt" => $notificationcnt['notificationcnt'],"responsecode"=>"400");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Reply Messages To Display","responsecode"=>"400");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function deletemessageAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $message_id = $this->getRequest()->getParam('message_id');
                $type = $this->getRequest()->getParam('type');
                $messageObj = new User_Model_Message();
                if($type == 'inbox')
                        $updAry['receiver_delete'] = '1';
                if($type == 'sent')
                        $updAry['sender_delete'] = '1';
                $id = $messageObj->updateMessagesByMsgid($updAry, $message_id);
                $notificationObj = new Notifications_Model_Notifications();
                $notificationObj->deleteNotificationByReference($message_id);
                if(!empty($id)) {
                        $resultAry = array('service_status'=>'success',"content" => "Message has been deleted from ".ucfirst($type),"responsecode"=>"305");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Some Error Occured","responsecode"=>"305");
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function displayfriendslistAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $user_id = $this->getRequest()->getParam('user_id');
                $user_type = $this->getRequest()->getParam('user_type');
                $searchword = $this->getRequest()->getParam('searchword');
                $followObj = new User_Model_Followers();
                $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                $cityObj = new User_Model_Cities();
                $friendslistdata = $followObj->getFriendsList($user_id,$user_type,$searchword);
                $friendslist = '';
                $cityid = '';
                $cityname = '';
                $cityArr = array();
                //echo '<pre>';print_R($friendslistdata);exit;
                foreach($friendslistdata as $key => $friend):
                    if($key != 0 && $cityid == $friend['city']) {
                            $city = $cityname;
                    } elseif(!empty($friend['city']) && $friend['city'] != 0) {
                            $cityArr = $cityObj->getCityNameByCityID($friend['city']);
                            $cityArr = $cityArr->toArray();
                            if(!empty($cityArr))
                                    $city = $cityArr[0]['city_name'];
                            else
                                    $city = '';
                            $cityid = $friend['city'];
                            $cityname = $city;
                    } else {
                            $city = '';
                    }
                    $frienddata['realname'] = $friend['realname'];
                    $frienddata['username'] = $friend['username'];
                    $frienddata['user_level_name'] = '';
                    $friendslist[$key]['scribbleusername'] = $scribObj->scribbleUname($frienddata,30);
                    $friendslist[$key]['user_id'] = $friend['user_id'];
                    $friendslist[$key]['user_type'] = $friend['user_type'];
                    $friendslist[$key]['firstname'] = $friend['firstname'];
                    $friendslist[$key]['lastname'] = $friend['lastname'];
                    $friendslist[$key]['username'] = $friend['username'];
                    $friendslist[$key]['gender'] = $friend['gender'];
                    $friendslist[$key]['city'] = $city;
                    $friendslist[$key]['image'] = $friend['image'];
                endforeach;
                //echo '<pre>';print_R($friendslist);exit;
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($user_id, $user_type, '1');
                if(!empty($friendslist)) {
                        $resultAry = array('service_status'=>'success', "content" => $friendslist, "notificationcnt" => $notificationcnt['notificationcnt'], "responsecode"=>"306");
                } else {
                        $resultAry = array('service_status'=>'error', "error_msg" => "No Friends With Keyword To View", "responsecode"=>"306");
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function toppopularpeopleAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                if($logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }
                $search_key = $request->keyword;
                $pagenum = $request->pagenum;
                if($pagenum == '' || ($pagenum <= 0))
                        $pagenum = 0;
                $limit = $request->limit;
                if($limit == '')
                        $limit = 30;
                // Get Top Popular People based on Cool Points
                $searchObj = new Search_Model_Search();
                $topPopularPeopleArray = $searchObj->getTopPopularPeople($limit, $pagenum, $search_key);
                // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                $cityObj = new User_Model_Cities();
                $cityid = '';
                $cityname = '';
                $cityArr = array();
                if(count($topPopularPeopleArray) > 0) {
                        foreach($topPopularPeopleArray as $key => $popularUsr) {
                                if($key != 0 && $cityid == $popularUsr['city']) {
                                        $city = $cityname;
                                } elseif(!empty($popularUsr['city']) && $popularUsr['city'] != 0) {
                                        $cityArr = $cityObj->getCityNameByCityID($popularUsr['city']);
                                        $cityArr = $cityArr->toArray();
                                        if(!empty($cityArr))
                                                $city = $cityArr[0]['city_name'];
                                        else
                                                $city = '';
                                        $cityid = $popularUsr['city'];
                                        $cityname = $city;
                                } else {
                                        $city = '';
                                }
                                $topPopularPeopleArray[$key]['city'] = $city;
                                $popularUsrType = $popularUsr['user_type'];
                                if($popularUsr['user_type'] != 'B') {
                                        $popularUsrType = 'U';
                                }
                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                        $topPopularPeopleArray[$key]['isFollowing'] = "2";
                                                } else {
                                                        if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                $topPopularPeopleArray[$key]['isFollowing'] = "1";
                                                                break;
                                                        } else {
                                                                $topPopularPeopleArray[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                } else {
                                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                $topPopularPeopleArray[$key]['isFollowing'] = "2";
                                        } else {
                                                $topPopularPeopleArray[$key]['isFollowing'] = "0";
                                        }
                                }
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($topPopularPeopleArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $topPopularPeopleArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                } elseif($pagenum == 0 && count($topPopularPeopleArray) == 0) {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Top Popular Users Found");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function localpeopleAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                // New Modifications
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                if($logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }
                $search_key = $request->keyword;

                $pagenum = $request->pagenum;
                if($pagenum == '' || ($pagenum <= 0))
                        $pagenum = 0;
                $limit = $request->limit;
                if($limit == '')
                        $limit = 30;

                //get User Zipcode Based On Usr Id and Type
                $searchObj = new Search_Model_Search();
                //$localPeopleArray =  $searchObj->getLocalPeople($logged_user_city, $logged_user_state, $logged_user_country);
                $userDetailsArray = $searchObj->getUserZipCodeByID($logged_user_Id);
                //echo "<pre>";print_r($userDetailsArray);exit;
                // GEt Local  People based on Zipcode of Logged User
                $localPeopleArray =  $searchObj->getLocalPeopleByZipcode($userDetailsArray[0]['zipcode'], $limit,  $pagenum, $search_key);
                // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                $cityObj = new User_Model_Cities();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $cityname = '';
                $cityid = '';
                $cityArr = array();
                if(count($localPeopleArray) > 0) {
                        foreach($localPeopleArray as $key => $popularUsr) {
                                if($key != 0 && $cityid == $popularUsr['city']) {
                                        $city = $cityname;
                                } elseif(!empty($popularUsr['city']) && $popularUsr['city'] != 0) {
                                        $cityArr = $cityObj->getCityNameByCityID($popularUsr['city']);
                                        $cityArr = $cityArr->toArray();
                                        if(!empty($cityArr))
                                                $city = $cityArr[0]['city_name'];
                                        else
                                                $city = '';
                                        $cityid = $popularUsr['city'];
                                        $cityname = $city;
                                } else {
                                        $city = '';
                                }
                                $localPeopleArray[$key]['city'] = $city;
                                $popularUsrType = $popularUsr['user_type'];
                                if($popularUsr['user_type'] != 'B') {
                                        $popularUsrType = 'U';
                                }
                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                        $localPeopleArray[$key]['isFollowing'] = "2";
                                                } else {
                                                        if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                $localPeopleArray[$key]['isFollowing'] = "1";
                                                                break;
                                                        } else {
                                                                $localPeopleArray[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                } else {
                                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                $localPeopleArray[$key]['isFollowing'] = "2";
                                        } else {
                                                $localPeopleArray[$key]['isFollowing'] = "0";
                                        }
                                }
                                $localPeopleArray[$key]['action_date'] = $localPeopleArray[$key]['created_Date'];
                                $localPeopleArray[$key]['created_Date'] = $timeObj->humaneDate($localPeopleArray[$key]['created_Date']);
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($localPeopleArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $localPeopleArray, "notificationcnt"=>$notificationcnt['notificationcnt']);
                } else if($pagenum == 0 && count($localPeopleArray) == 0) {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Local Users Found");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function lamepeopleAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                if($logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }
                $search_key = $request->keyword;

                // For Pull to Refresh
                $pagenum = $request->pagenum;
                if($pagenum == '' || ($pagenum <= 0))
                    $pagenum = 0;
                $limit = $request->limit;
                if($limit == '')
                    $limit = 30;

                // GEt LAME People based on Cool Points
                $searchObj = new Search_Model_Search();
                $lamePeopleArray =  $searchObj->getLamePeople($limit, $pagenum, $search_key);
                // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                $cityObj = new User_Model_Cities();
                $cityid = '';
                $cityname = '';
                $cityArr = array();
                if(count($lamePeopleArray) > 0) {
                        foreach($lamePeopleArray as $key => $popularUsr) {
                                if($key != 0 && $cityid == $popularUsr['city']) {
                                        $city = $cityname;
                                } elseif(!empty($popularUsr['city']) && $popularUsr['city'] != 0) {
                                        $cityArr = $cityObj->getCityNameByCityID($popularUsr['city']);
                                        $cityArr = $cityArr->toArray();
                                        if(!empty($cityArr))
                                                $city = $cityArr[0]['city_name'];
                                        else
                                                $city = '';
                                        $cityid = $popularUsr['city'];
                                        $cityname = $city;
                                } else {
                                        $city = '';
                                }
                                $lamePeopleArray[$key]['city'] = $city;
                                $popularUsrType = $popularUsr['user_type'];
                                if($popularUsr['user_type'] != 'B'){
                                        $popularUsrType = 'U';
                                }
                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $Usrfollows) {
                                                if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                        $lamePeopleArray[$key]['isFollowing'] = "2";
                                                } else {
                                                        if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                $lamePeopleArray[$key]['isFollowing'] = "1";
                                                                break;
                                                        }  else {
                                                                $lamePeopleArray[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                } else {
                                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                $lamePeopleArray[$key]['isFollowing'] = "2";
                                        } else {
                                                $lamePeopleArray[$key]['isFollowing'] = "0";
                                        }
                                }
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($lamePeopleArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $lamePeopleArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else if($pagenum == 0 && count($lamePeopleArray) == 0) {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Lame Users Found");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getuserphotovideodetailsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                //To get  Followers Album Photos OR Video Thumbnails Details
                $limit= 30;
                $photosObj = new Mobile_Model_Photos();
                $followersgallery = $photosObj->getTimelineClickedImagesVideosDetails($loggedUserId,$loggedUserType, $limit);
                //echo "<pre>";print_r($followersgallery);exit;
                //echo count($followersgallery);exit;
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($followersgallery)) {
                        $resultAry = array('service_status'=>'success',"content" => $followersgallery, "notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"429");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Album or Video Details found',"usermediacode"=>"429");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getdownlinescribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_level = $request->user_level;
                if($logged_user_level == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Level can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_level)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):30);
                // FOR Fetchng Logged User  DOWNLINE Scribbbles
                $scrObj = new Scribbles_Model_Scribbles();
                $downlineArray = array();
                $downlineArray = $scrObj->getDownlineScribbles($logged_user_Id,$logged_user_level,$limit);
                foreach($downlineArray as $key =>$parentScrib) {
                        $parentId = $parentScrib['scribble_id'];
                        $downlineArray[$key] = $parentScrib;
                        $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $logged_user_Id,$logged_user_Id,"U", $logged_user_level,"downline");
                        $downlineArray[$key]['childScribbles'] = $childScribblesCnt;

                         //  check if the Parent scribble is Blasted
                        $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                        $downlineArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                        $blastedUsrAry = array();
                        if($blastedUsrCnt > 0) {
                            //to Get Blasted User Details of the Parent Scribble
                            $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $logged_user_Id);
                        }
                        $downlineArray[$key]['BlastedUsers'] = $blastedUsrAry;
                }
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($downlineArray) > 0) {
                        $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                        foreach($downlineArray as $key=>$scrib) {
                                $downlineArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                                $downlineArray[$key]['notifications_type'] = "scribble";
                                $scrib['notifications_type'] = "scribble";
                                if($scrib['notifications_type'] != 'scribble') continue;
                                $downlineArray[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                if(!empty($downlineArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $downlineArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Scribbles Found');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getatusernamescribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $logged_user_Id = $request->user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                // to check user Exist or not
                $userObj = new User_Model_User();
                $usrArray = array();
                $usrArray = $userObj->getUserDetails($logged_user_Id);
                if(count($usrArray) > 0) {
                        $limit = $request->limit;
                        // For fetching @username Scribbles
                        $scrObj = new Scribbles_Model_Scribbles();
                        $atUsernameArray = array();
                        $atUsernameArray = $scrObj->getAtUserNameScribbles($logged_user_Id,$usrArray->user_level, $limit);
                        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                        if(count($atUsernameArray) > 0) {
		                foreach($atUsernameArray as $key=>$scrib) {
		                        $atUsernameArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
		                }
                        }
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                        if(!empty($atUsernameArray)) {
                                $resultAry = array('service_status'=>'success',"content" => $atUsernameArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => 'No Scribbles Found');
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No User Available with Id');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getalltimelinephotosdetailsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == ''){
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Type Can not be Empty!!!'));
                        return;
                        $logged_user_Id = 'U';
                }
                $refresh_user_id = $request->getParam('refresh_user_id');
                $refresh_user_date = urldecode($request->getParam('refresh_user_date'));
                $type = $request->getParam('type');
                $limit = $request->getParam('limit');
                if($limit!= ''){ $limit = $limit; } else { $limit = '';}
                //$album_id = $this->getRequest()->getParam('album_id');
                $photosObj = new Mobile_Model_Photos();
                $photos = $photosObj->getTimelineClickedImagesVideosDetails($logged_user_Id,$logged_user_type, $limit, $refresh_user_id, $refresh_user_date, $type);

                $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($photos) > 0) {
                        foreach($photos as $key=>$photo) {
                                $photos[$key]['created_date'] = $timeObj->humaneDate($photo['created_date']);
                                $photos[$key]['date'] = $timeObj->humaneDate($photo['date']);
                                $photos[$key]['scribbleusername'] = $scribObj->scribbleUname($photo,26);
                                if(isset($photo['commenter_date1']) && $photo['commenter_date1'] != '') {
                                        $photos[$key]['commenter_date1'] = $timeObj->humaneDate($photo['commenter_date1']);
                                }
                                if(isset($photo['commenter_date2']) && $photo['commenter_date2'] != '') {
                                        $photos[$key]['commenter_date2'] = $timeObj->humaneDate($photo['commenter_date2']);
                                }
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                //$photos = array();
                if (!empty($photos)) {
                        $resultAry = array('service_status'=>'success',"content" => $photos, "notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"429");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Photos to View',"notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"429");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function blastphotovideoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $photo_id = $request->getParam('photo_id');
                if($photo_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Photo Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($photo_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Photo Id!!!'));
                        return;
                }
                $type = $request->getParam('type');
                if($type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Share Type can not be Empty!!!'));
                        return;
                }
                $logged_User_Id = $request->getParam('logged_user_id');
                if($logged_User_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_User_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!'));
                        return;
                }
                $logged_User_Type = $request->getParam('logged_user_type');
                if($logged_User_Type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Type can not be Empty!!!'));
                        return;
                }
                $notificationObj = new Notifications_Model_Notifications();
                if($type == 'photo') {
                        // Get Photo Details By Photo ID
                        $followerObj = new User_Model_Followers();
                        $photoObj = new Photos_Model_Photos();
                        //Check User balst this Photo r not
                        $availrnot = $photoObj->isAvailable($logged_User_Id,$logged_User_Type,$photo_id);
                        //echo '<pre>';print_R($availrnot);exit;
                        if(!empty($availrnot)) {
                                echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have already Share this Photo'));
                                return;
                        }
                        //Checking For Limit Of the day
                        $daycount = $photoObj->getdaycount($logged_User_Id,$logged_User_Type);
                        //echo '<pre>';print_r($daycount);exit;

                        $blastLimit = 50;
                        if($_SERVER['SERVER_NAME'] == '192.168.1.57'){
                            $blastLimit = PHOTO_BLAST_LIMIT;
                        }

                        if($daycount['count'] >= $blastLimit) {
                                echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have reached day limit for Share Photo'));
                                return;
                        }
                        $photoDetails = $photoObj->getPhotoDetailsByPhotoId($photo_id);
                        $photoDetails = $photoDetails ->toArray();
                        $photo_owner_id = $photoDetails[0]['user_id'];
                        $photo_owner_type = $photoDetails[0]['user_type'];
                        $isFollowingArr = $followerObj->isLoggedUserFollowingOtherUser($photo_owner_id, $photo_owner_type, $logged_User_Id, $logged_User_Type);
                        if(empty($isFollowingArr)) {
                                echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You need to follow the user to share this photo'));
                                return;
                        }
                        $insArray = array();
                        $insArray['album_id'] = $photoDetails[0]['album_id'];
                        $insArray['photo_original_name'] = $photoDetails[0]['photo_original_name'];
                        $insArray['photo_modified_name'] = $photoDetails[0]['photo_modified_name'];
                        $insArray['user_id'] = $logged_User_Id;
                        $insArray['user_type'] = $logged_User_Type;
                        $insArray['cover_photo'] = 0;
                        $insArray['blast_id'] = $photoDetails[0]['photo_id'];
                        $insArray['width'] = $photoDetails[0]['width'];
                        $insArray['height'] = $photoDetails[0]['height'];
                        $insArray['android_width'] = $photoDetails[0]['android_width'];
                        $insArray['android_height'] = $photoDetails[0]['android_height'];
                        $insArray['created_date'] = date('Y-m-d H:i:s');
                        $insArray['blasted_date'] = $insArray['created_date'];
                        $photoObj = new Photos_Model_Photos();
                        $ins = $photoObj->blastPhoto($insArray);

                        // Update Blasted Date of Original Photo with latest Blasted Date
                        $updAry = array();
                        $updAry['blasted_date'] = $insArray['blasted_date'];
                        $updDate = $photoObj->updateParentPhotoBlastedDate($updAry, $photoDetails[0]['photo_id']);

                        $resAry['reference_id'] = $ins;
                        $resAry['user_id'] = $photo_owner_id;
                        $resAry['user_type'] = $photo_owner_type;
                        $resAry['notifications_type'] = 'photo_blasted';
                        $resAry['action_date'] = date('Y-m-d H:i:s');
                        $ins = $notificationObj->insertNewRecord($resAry);

                        $scrObj = new Scribbles_Model_Scribbles();
                        if($ins) {
                                if($photo_owner_type != 'B') {
                                        $upd   = $scrObj->updateCoolPoints($photo_owner_id, $photo_owner_type,"photoblast");
                                        /**** Upgrade User Level if cool points decreases Level Limits ****/
                                        $usrLevelObj = new User_Model_Userlevels();
                                        //Get User Level
                                        $userLevel = $usrLevelObj->getUserLevelByUserId($photo_owner_id, $photo_owner_type);
                                        //Get All Levels details
                                        $allUserLevelsArray   = $usrLevelObj->getAllTypesOfUserLevels();

                                        // to get Followers Count of logged User
                                        $followersCnt = $followerObj->getFollowersCnt($photo_owner_id, $photo_owner_type);

                                        // to Get Following Count of the Logged User
                                        $followingCnt = $followerObj->getFollowingCnt($photo_owner_id,  $photo_owner_type);

                                        $levelArray = array();
                                        foreach ($allUserLevelsArray as $level) {
                                                if(($userLevel[0]['cool_points'] >= $level['minimum_coolpoints'] && $level['minimum_coolpoints']+1 >= $userLevel[0]['cool_points']) && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']) && $userLevel[0]['user_level']!= $level['level_no']){
                                                        $upgradeLevel = $level['level_no'];
                                                        // Upgrade User Level
                                                        $usrObj = new User_Model_User();
                                                        $upgrade = $usrObj->upgradeLevel($photo_owner_id,$upgradeLevel);
                                                        // Update user level in Downlined scribbles also.
                                                        $downlineUserLevel = $upgradeLevel;
                                                        $UpdArr   = $scrObj->updateDownlinedScribblesLevel($photo_owner_id, $downlineUserLevel);
                                                        $resAry['reference_id'] = $upgrade;
                                                        $resAry['user_id'] = $photo_owner_id;
                                                        $resAry['user_type'] = $photo_owner_type;
                                                        $resAry['notifications_type'] = 'level_upgrade';
                                                        $resAry['action_date'] = date('Y-m-d H:i:s');
                                                        $upgrade = $notificationObj->insertNewRecord($resAry);
                                                        $lvlAry['user_id'] = $photo_owner_id;
                                                        $lvlAry['level'] = $upgradeLevel;
                                                        $lvlAry['level_status'] = 'Up';
                                                        $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                                        if(!empty($id)) {
                                                            $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                                        } else {
                                                            $lvlid = $usrObj->insertleveldetails($lvlAry);
                                                        }

                                                        // Mail to be sent to the user
                            //                            $m = new UpmeSocial_HtmlMailer();
                            //                            $m->setSubject("UPMEsocial User Level Upgrade");
                            //                            $m->addTo($userLevel[0]['email'])
                            //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
                            //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
                            //                                ->setViewParam('email',$userLevel[0]['email'])
                            //                                ->setViewParam('new_level',$upgradeLevel)
                            //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
                            //                            $m->sendHtmlTemplate("level_upgrade.phtml");
                                                }
                                        }
                                }
                                // Get ALl Photo Blasted User Count
                                $mobilePhotoObj = new Mobile_Model_Photos();
                                $blastedUserCnt = $mobilePhotoObj->getBlastedUserCountBasedOnPhotoId($photo_id);

                                $limit = 2; // get 2 blasted users
                                $blastedUserDetails = array();
                                $blastedUserDetails = $mobilePhotoObj->getBlastedUserDetailsBasedOnPhotoId($photo_id, $logged_User_Id, $limit);
                                $photoCount = 0;
                                foreach($blastedUserDetails as $blastUser)
                                {
                                    $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                                    $blastedUserDetails[$photoCount] = $blastUser;
                                    $photoCount++; 
                                }
                                $notificationcnt = $notificationObj->notificationcnt($logged_User_Id, $logged_User_Type, '1');
                                $resultAry = array('service_status'=>'success',"content" => $ins, "blastedUserCnt"=>$blastedUserCnt, "blastedUsers"=>$blastedUserDetails, "notificationcnt" => $notificationcnt['notificationcnt']);
                        } else {
                                $resultAry = array('service_status'=>'error_message',"content" =>'Error processing request');
                        }
                        echo $myjson->customEncode($resultAry);
                }
                if($type == 'video') {
                        // Get Video Details By Video ID
                        $videoObj = new Videos_Model_Videos();
                        //Check User balst this Video r not
                        $availrnot = $videoObj->isAvailable($logged_User_Id,$logged_User_Type,$photo_id);
                        //echo '<pre>';print_R($availrnot);exit;
                        if(!empty($availrnot)) {
                                echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have already share this Video'));
                                return;
                        }
                        //Checking For Limit Of the day
                        $daycount = $videoObj->getdaycount($logged_User_Id,$logged_User_Type);
                        //echo '<pre>';print_r($daycount);exit;
                        if($daycount['count'] >= 50) {
                                echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have reached day limit for share Video'));
                                return;
                        }
                        $videoDetails = $videoObj->getvideosbyvideoid($photo_id);
                        $video_owner_id = $videoDetails['user_id'];
                        $video_owner_type = $videoDetails['user_type'];

                        $insArray = array();
                        $insArray['video_name'] = $videoDetails['video_name'];
                        $insArray['video_path'] = $videoDetails['video_path'];
                        $insArray['thumbnail_path'] = $videoDetails['thumbnail_path'];
                        $insArray['zencoder_job_id'] = $videoDetails['zencoder_job_id'];
                        $insArray['status'] = $videoDetails['status'];
                        $insArray['user_id'] = $logged_User_Id;
                        $insArray['user_type'] = $logged_User_Type;
                        $insArray['blast_id'] = $videoDetails['video_id'];
                        $insArray['created_date'] = date('Y-m-d H:i:s');

                        $videoObj = new Videos_Model_Videos();
                        $ins = $videoObj->blastVideo($insArray);
                        $resAry['reference_id'] = $ins;
                        $resAry['user_id'] = $video_owner_id;
                        $resAry['user_type'] = $video_owner_type;
                        $resAry['notifications_type'] = 'video_blasted';
                        $resAry['action_date'] = date('Y-m-d H:i:s');
                        $ins = $notificationObj->insertNewRecord($resAry);
                        $scrObj = new Scribbles_Model_Scribbles();
                        if($ins) {
                                if($video_owner_type != 'B') {
                                        $upd   = $scrObj->updateCoolPoints($video_owner_id, $video_owner_type,"videoblast");
                                        /**** Upgrade User Level if cool points decreases Level Limits ****/
                                        $usrLevelObj = new User_Model_Userlevels();
                                        //Get User Level
                                        $userLevel = $usrLevelObj->getUserLevelByUserId($video_owner_id, $video_owner_type);
                                        //Get All Levels details
                                        $allUserLevelsArray   = $usrLevelObj->getAllTypesOfUserLevels();

                                        $followerObj = new User_Model_Followers();
                                        // to get Followers Count of logged User
                                        $followersCnt = $followerObj->getFollowersCnt($video_owner_id, $video_owner_type);

                                        // to Get Following Count of the Logged User
                                        $followingCnt = $followerObj->getFollowingCnt($video_owner_id,  $video_owner_type);

                                        $levelArray = array();
                                        foreach ($allUserLevelsArray as $level) {
                                                if(($userLevel[0]['cool_points'] >= $level['minimum_coolpoints'] && $level['minimum_coolpoints']+1 == $userLevel[0]['cool_points']) && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']) && $userLevel[0]['user_level']!= $level['level_no']) {
                                                        $upgradeLevel = $level['level_no'];
                                                        // Upgrade User Level
                                                        $usrObj = new User_Model_User();
                                                        $upgrade = $usrObj->upgradeLevel($video_owner_id,$upgradeLevel);
                                                        // Update user level in Downlined scribbles also.
                                                        $downlineUserLevel = $upgradeLevel;
                                                        $UpdArr   = $scrObj->updateDownlinedScribblesLevel($video_owner_id, $downlineUserLevel);
                                                        $resAry['reference_id'] = $upgrade;
                                                        $resAry['user_id'] = $video_owner_id;
                                                        $resAry['user_type'] = $video_owner_type;
                                                        $resAry['notifications_type'] = 'level_upgrade';
                                                        $resAry['action_date'] = date('Y-m-d H:i:s');
                                                        $upgrade = $notificationObj->insertNewRecord($resAry);
                                                        $lvlAry['user_id'] = $video_owner_id;
                                                        $lvlAry['level'] = $upgradeLevel;
                                                        $lvlAry['level_status'] = 'Up';
                                                        $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                                        if(!empty($id)) {
                                                            $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                                        } else {
                                                            $lvlid = $usrObj->insertleveldetails($lvlAry);
                                                        }

                                                        // Mail to be sent to the user
                            //                            $m = new UpmeSocial_HtmlMailer();
                            //                            $m->setSubject("UPMEsocial User Level Upgrade");
                            //                            $m->addTo($userLevel[0]['email'])
                            //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
                            //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
                            //                                ->setViewParam('new_level',$upgradeLevel)
                            //                                ->setViewParam('email',$userLevel[0]['email'])
                            //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
                            //                            $m->sendHtmlTemplate("level_upgrade.phtml");
                                                }
                                        }
                                }

                                 // Get ALl Videos Blasted User Count
                                $mobileVideosObj = new Mobile_Model_Videos();
                                $blastedUserCnt = $mobileVideosObj->getBlastedUserCountBasedOnVideoId($photo_id);

                                $limit = 2; // get 2 blasted users
                                $blastedUserDetails = array();
                                $blastedUserDetails = $mobileVideosObj->getBlastedUserDetailsBasedOnVideoId($photo_id);
                                $videoCount = 0;
                                foreach($blastedUserDetails as $blastUser)
                                {
                                    $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                                    $blastedUserDetails[$videoCount] = $blastUser;
                                    $videoCount++; 
                                }

                                $notificationcnt = $notificationObj->notificationcnt($logged_User_Id, $logged_User_Type, '1');
                                $resultAry = array('service_status'=>'success',"content" => $ins, "blastedUserCnt"=>$blastedUserCnt, "blastedUsers"=>$blastedUserDetails, "notificationcnt" => $notificationcnt['notificationcnt']);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_message" =>'Error processing request');
                        }
                        echo $myjson->customEncode($resultAry);
                        return;
                }
        }

        public function getbusinessuserinfoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $businessuserObj = new Business_Model_Business();
                $followObj = new Business_Model_Followers();
                $userFollowObj = new User_Model_Followers();
                $req = $this->getRequest();
                $bid=$req->getParam('business_id');
                if($bid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Business Id can not be Empty!!!', "userinfocode" => "400"));
                        return;
                }
                if(!is_numeric($bid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Business Id!!!', "userinfocode" => "400"));
                        return;
                }
                if($req->getParam('limit') != '')
                        $limit=$req->getParam('limit');
                else
                        $limit = 10;
                $businessexists = $businessuserObj->getBusinessUserexistOrNot($bid);
                if($businessexists == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Business Id!!!', "userinfocode" => "400"));
                        return;
                }
                $businessuserDetAry =array();
                $businessuserDetAry = $businessuserObj->getBusinessUserDetails($bid);
                $businessuserDetAry = $businessuserDetAry->toArray();
                if (!empty($businessuserDetAry)) {
                        $loggedUserID = $req->getParam('logged_user_id');
                        if($loggedUserID == '') {
                                echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!', "userinfocode" => "400"));
                                return;
                        }
                        if(!is_numeric($loggedUserID)) {
                                echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!', "userinfocode" => "400"));
                                return;
                        }
                        // If Loged User Seeing his Profile
                        /*if($loggedUserID == $bid) {
                                $businessuserDetAry['isFollowing'] = "2";
                        } else {*/
                                // TO check if following other profile user or not
                                $businessuserfollowing = $userFollowObj->isLoggedUserFollowingOtherUser($bid, 'B', $loggedUserID, "U");
                                if(count($businessuserfollowing) > 0) {
                                        $businessuserDetAry['isFollowing'] = "1";
                                }
                                if(count($businessuserfollowing) == 0) {
                                        $businessuserDetAry['isFollowing'] = "0";
                                }
                        //}
                        $reviewObj = new Business_Model_Reviews();
                        $reviewArray = $reviewObj->getBusinessReviewsById($bid, $bid, "B", $limit);

                        // for changing TIME format of CREATED DATE
                        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                        foreach($reviewArray as $key=>$review) {
                                $reviewArray[$key]['created_date'] = $timeObj->humaneDate($review['created_date']);
                        }
                        $couponObj = new Business_Model_Coupons();
                        $businessactivecouponscnt = $couponObj->businessactivecouponscount($bid);
                        $businessuserDetAry['nooffollowers'] = $followObj->getFollowersCnt($bid);
                        $businessuserDetAry['nooffollowings'] = $followObj->getFollowingCnt($bid);
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($loggedUserID, "U", '1');
                        $resultAry = array('service_status'=>'success',"content" => $businessuserDetAry, "reviews" => $reviewArray,"notificationcnt" => $notificationcnt['notificationcnt'], "businessactivecouponscnt" => $businessactivecouponscnt, "userinfocode" => "400");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => '', "userinfocode" => "400");
                }
                echo $myjson->customEncode($resultAry);
        }

        public function getbusinessuseruppersinfoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $businesscouponsObj = new Business_Model_Coupons();
                $req = $this->getRequest();
                $bid=$req->getParam('business_id');
                if($bid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Business Id can not be Empty!!!',"searchcode"=>'433'));
                        return;
                }
                if(!is_numeric($bid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Business Id!!!',"searchcode"=>'433'));
                        return;
                }
                $limit = 10;
                $pagenum = 0;

                if($req->getParam('pagenum') != '')
                        $pagenum=$req->getParam('pagenum');

                if($req->getParam('limit') != '')
                        $limit=$req->getParam('limit');

                $busiObj = new Business_Model_Business();
                $isBusinessAvailable = $busiObj->getBusinessUserexistOrNot($bid);
                $cnt = count($isBusinessAvailable);
                if($cnt > 0) {
                        $couponsDetAry =array();
                        $couponsDetAry = $businesscouponsObj->getcouponsbybusinessid($bid, $pagenum, $limit);
                        if (!empty($couponsDetAry)) {
                            $purchasecouponsObj = new Business_Model_Purchasecoupons();
                                foreach($couponsDetAry as $key => $coupons) {
                                        if(strtotime($coupons['expiration_date']) > strtotime(date("Y-m-d"))) {
                                                $couponsDetAry[$key]['active'] = '1';
                                        } else {
                                                $couponsDetAry[$key]['active'] = '0';
                                        }
                                        $couponsDetAry[$key]['expiration_date'] = date('d, M Y', strtotime($coupons['expiration_date']));
                                        $cnt = $purchasecouponsObj->purchasedcouponcount($coupons['coupon_id']);
                                        $couponsDetAry[$key]['couponcnt'] = $cnt['couponscnt'];
                                }
                                $resultAry = array('service_status'=>'success',"content" => $couponsDetAry,"searchcode"=>'433');
                        } else {
                                $resultAry = array('service_status'=>'error',"error_message" => 'No Uppers Found',"searchcode"=>'433');
                        }
                }
                if($cnt == 0) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" =>"Business Id doen't exist!!!","searchcode"=>'433'));
                        return;
                }
                echo $myjson->customEncode($resultAry);
        }

        public function getuserpurchaseduppersinfoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $businesspurchasecouponsObj = new Business_Model_Purchasecoupons();
                $req = $this->getRequest();
                $uid = $req->getParam('logged_user_id');
                if($uid == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                    return;
                }
                if(!is_numeric($uid)) {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!'));
                    return;
                }
                $pagenum = 0;
                if($req->getParam('pagenum') != '')
                    $pagenum=$req->getParam('pagenum');

                $limit = 10;
                if($req->getParam('limit') != '')
                    $limit=$req->getParam('limit');

                $purchasedcouponsDetAry =array();
                $purchasedcouponsDetAry = $businesspurchasecouponsObj->getpurchasecouponsinfo($uid, $pagenum, $limit);
                if (!empty($purchasedcouponsDetAry)) {
                    $purchasecouponsObj = new Business_Model_Purchasecoupons();
                        foreach($purchasedcouponsDetAry as $key => $coupons) {
                                if(strtotime($coupons['expiration_date']) > strtotime(date("Y-m-d"))) {
                                        $purchasedcouponsDetAry[$key]['active'] = '1';
                                } else {
                                        $purchasedcouponsDetAry[$key]['active'] = '0';
                                }
                                $purchasedcouponsDetAry[$key]['expiration_date'] = date('d, M Y', strtotime($coupons['expiration_date']));
                                $cnt = $purchasecouponsObj->purchasedcouponcount($coupons['coupon_id']);
                                $purchasedcouponsDetAry[$key]['couponcnt'] = $cnt['couponscnt'];
                        }
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($uid, "U", '1');
                        $resultAry = array('service_status'=>'success',"content" => $purchasedcouponsDetAry,"notificationcnt" => $notificationcnt['notificationcnt'],"searchcode" => '433',"responsecode"=>'402');
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Purchased Uppers Found',"searchcode" => '433',"responsecode"=>'403');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function myuppersAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $req = $this->getRequest();
                $user_id = $req->getParam('logged_user_id');
                if($user_id == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                    return;
                }
                if(!is_numeric($user_id)) {
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User Id!!!'));
                    return;
                }
                if($req->getParam('pagenum') != '')
                    $pagenum = $req->getParam('pagenum');
                else
                    $pagenum = 0;

                if($req->getParam('limit') != '')
                    $limit = $req->getParam('limit');
                else
                    $limit = 10;

                if($req->getParam('keyword') != '' && $req->getParam('keyword') != '(null)')
                    $keyword = $req->getParam('keyword');
                else
                    $keyword = '';

                /*if($uid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!'));
                        return;
                }*/
                /*if(!empty($user_id)) {
                        $purchasecouponsObj = new Business_Model_Purchasecoupons;
                        $purchasecoupons = $purchasecouponsObj->getactivepurchasecouponsinfo($user_id);
                        foreach($purchasecoupons as $key => $coupons) {
                                $purchasecoupons[$key]['purchase_date'] =  date('d, M Y', strtotime($coupons['purchase_date']));
                                $purchasecoupons[$key]['expiration_date'] = date('d, M Y', strtotime($coupons['expiration_date']));
                                $date = date('d, M Y');
                                if($purchasecoupons[$key]['expiration_date'] > $date) {
                                        $purchasecoupons[$key]['active'] = '1';
                                } else {
                                        $purchasecoupons[$key]['active'] = '0';
                                }
                        }
                } else {*/
                        $couponsObj = new Business_Model_Coupons();
                        //$coupons = $couponsObj->getallcouponsinfo($pagenum, $limit);
                        $coupons = $couponsObj->getUppersByUserId($user_id, $pagenum, $limit,$keyword);
                        //$purchasecouponsObj = new Business_Model_Purchasecoupons();
                        foreach($coupons as $key => $coupon) {
                                //echo "<pre>";print_r($coupon);exit;
                                if(strtotime($coupon['expiration_date']) > strtotime(date("Y-m-d"))) {
                                        $coupons[$key]['active'] = '1';
                                } else {
                                        $coupons[$key]['active'] = '0';
                                }
                                $coupons[$key]['expiration_date'] = date('d, M Y', strtotime($coupon['expiration_date']));
                                //$cnt = $purchasecouponsObj->purchasedcouponcount($coupon['coupon_id']);
                                //$coupons[$key]['couponcnt'] = $cnt['couponscnt'];
                        }
                //}
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                if(!empty($coupons)) {
                        $resultAry = array('service_status'=>'success',"content" => $coupons,"notificationcnt" => $notificationcnt['notificationcnt'],"searchcode"=>'433');
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Uppers Found',"searchcode"=>'433');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function purchaseupperAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $purchasecouponsObj = new Business_Model_Purchasecoupons();
                $couponsObj = new Business_Model_Coupons();
                $req = $this->getRequest();
                $user_id = $req->getParam('logged_user_id');
                if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($user_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!'));
                        return;
                }
                $coupon_id = $req->getParam('coupon_id');
                if($coupon_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Upper Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($coupon_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Upper Id!!!'));
                        return;
                }
                $couponavailbility = $couponsObj->getcouponbyid($coupon_id);
                // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($user_id, 'U');
                $isFollowing = 0;
                //if(in_array($couponavailbility['business_id'],$loggedUserFollowing))
                $followingCnt = count($loggedUserFollowing);
                if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $Usrfollows) {
                                if($couponavailbility['business_id'] == $Usrfollows['follower_id'] && $Usrfollows['follower_type'] == 'B') {
                                        $isFollowing = 1;
                                }
                        }
                } else {
                        $isFollowing = 0;
                }
                if($isFollowing == 0) {
                        $updateAry = array();
                        $updateAry['user_id'] = $user_id;
                        $updateAry['user_type'] = 'U';
                        $updateAry['follower_id'] = $couponavailbility['business_id'];
                        $updateAry['follower_type'] = 'B';
                        $fid = $followObj->insertNewRecord($updateAry);
                        if($updateAry['follower_type'] == 'B') {
                                $resAry['reference_id'] = $fid;
                                $resAry['user_id'] = $updateAry['follower_id'];
                                $resAry['user_type'] = $updateAry['follower_type'];
                                $resAry['notifications_type'] = 'following';
                                $resAry['action_date'] = date('Y-m-d H:i:s');
                                $notificationObj = new Notifications_Model_Notifications();
                                $id = $notificationObj->insertNewRecord($resAry);
                                $resAry['reference_id'] = $fid;
                                $resAry['user_id'] = $updateAry['user_id'];
                                $resAry['user_type'] = $updateAry['user_type'];
                                $resAry['notifications_type'] = 'Followed';
                                $resAry['action_date'] = date('Y-m-d H:i:s');
                                $notificationObj = new Notifications_Model_Notifications();
                                $id = $notificationObj->insertNewRecord($resAry);
                                // Upgrade User Level if cool points & Follower/ following criteria reaches Limits
                                $usrLevelObj = new User_Model_Userlevels();
                                $scrObj = new Scribbles_Model_Scribbles();

                                //Get User Level
                                $userLevel = $usrLevelObj->getUserLevelByUserId($updateAry['user_id'], $updateAry['user_type']);
                                //Get All Levels details FROM DB
                                $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                                $followerObj = new User_Model_Followers();
                                // to get Followers Count of logged User
                                $followersCnt = $followerObj->getFollowersCnt($updateAry['user_id'], $updateAry['user_type']);

                                // to Get Following Count of the Logged User
                                $followingCnt = $followerObj->getFollowingCnt($updateAry['user_id'], $updateAry['user_type']);
                                //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                                foreach($allUserLevelsArray as $level) {
                                        if(isset($userLevel[0]) && !empty($userLevel[0]) && (($userLevel[0]['cool_points'] >= $level['minimum_coolpoints']  && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']))) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == ($level['level_no']-1)){
                                                $upgradeLevel = $level['level_no'];
                                                //Upgrade User Level
                                                $usrObj = new User_Model_User();
                                                $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                                // Update user level in Downlined scribbles also.
                                                $downlineUserLevel = $upgradeLevel;
                                                $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                                $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                                $lvlAry['level'] = $upgradeLevel;
                                                $lvlAry['level_status'] = 'Up';
                                                $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                                if(!empty($id)) {
                                                    $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                                } else {
                                                    $lvlid = $usrObj->insertleveldetails($lvlAry);
                                                }
                                        }
                                }

                                // Upgrade User Level if cool points & Follower/ following criteria reaches Limits
                                $usrLevelObj = new User_Model_Userlevels();
                                $scrObj = new Scribbles_Model_Scribbles();

                                //Get User Level
                                $userLevel = $usrLevelObj->getUserLevelByUserId($updateAry['follower_id'], $updateAry['follower_type']);
                                //Get All Levels details FROM DB
                                $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                                $followerObj = new User_Model_Followers();
                                // to get Followers Count of logged User
                                $followersCnt = $followerObj->getFollowersCnt($updateAry['follower_id'], $updateAry['follower_type']);

                                // to Get Following Count of the Logged User
                                $followingCnt = $followerObj->getFollowingCnt($updateAry['follower_id'], $updateAry['follower_type']);
                                foreach($allUserLevelsArray as $level) {
                                        if(isset($userLevel[0]) && !empty($userLevel[0]) && (($userLevel[0]['cool_points'] >= $level['minimum_coolpoints']  && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']))) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == ($level['level_no']-1)){
                                                $upgradeLevel = $level['level_no'];
                                                //Upgrade User Level
                                                $usrObj = new User_Model_User();
                                                $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                                // Update user level in Downlined scribbles also.
                                                $downlineUserLevel = $upgradeLevel;
                                                $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                                $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                                $lvlAry['level'] = $upgradeLevel;
                                                $lvlAry['level_status'] = 'Up';
                                                $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                                if(!empty($id)) {
                                                    $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                                } else {
                                                    $lvlid = $usrObj->insertleveldetails($lvlAry);
                                                }
                                        }
                                }
                        }
                }
                if(!empty($couponavailbility)) {
                        $date = date('Y-m-d');
                        $expirydate = $couponavailbility['expiration_date'];
                        if($expirydate > $date) {
                                $data['user_id'] = $user_id;
                                $data['coupon_id'] = $coupon_id;
                                $data['purchase_date'] = date('Y-m-d H:i:s');
                                $stat = $purchasecouponsObj->insert($data);
                                // For INserting into Notifications table
                                $resAry['reference_id'] = $stat;
                                $resAry['user_id'] = $user_id;
                                $resAry['user_type'] = 'U';
                                $resAry['notifications_type'] = 'purchasecoupon';
                                $resAry['action_date'] = date('Y-m-d H:i:s');
                                $notificationObj = new Notifications_Model_Notifications();
                                $stat = $notificationObj->insertNewRecord($resAry);
                                $cnt = $purchasecouponsObj->purchasedcouponcount($coupon_id);
                                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
                                    //echo "<pre>";print_r($cnt['couponscnt']);exit;
                                }
                                $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                                if (!empty($stat)) {
                                        $resultAry = array('service_status'=>'success',"content" => "Upper Purchased Successfully", "PurchasedCouponsCount" => $cnt['couponscnt'], "notificationcnt"=> $notificationcnt['notificationcnt']);
                                }
                        } else {
                                $resultAry = array('service_status'=>'error',"error_message" => 'Upper Expired');
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => "Upper Id Doesn't exists");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function upuppersAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $user_id = $request->getParam('logged_user_id');
                if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($user_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!'));
                        return;
                }
                $pagenum = $request->pagenum;
                if($pagenum == '')
                    $pagenum = 0;
                $limit = $request->limit;
                if($limit == '')
                    $limit = 10;
                $couponsObj = new Business_Model_Coupons();
                $couponsinfo = $couponsObj->getcouponbydate($user_id, $pagenum, $limit);
                $purchasecouponsObj = new Business_Model_Purchasecoupons();
                foreach($couponsinfo as $key => $coupons) {
                    if(strtotime($coupons['expiration_date']) > strtotime(date("Y-m-d"))) {
                            $couponsinfo[$key]['active'] = '1';
                    } else {
                            $couponsinfo[$key]['active'] = '0';
                    }
                    $couponsinfo[$key]['expiration_date'] = date('d, M Y', strtotime($coupons['expiration_date']));
                    $cnt = $purchasecouponsObj->purchasedcouponcount($coupons['coupon_id']);
                    $couponsinfo[$key]['couponcnt'] = $cnt['couponscnt'];
                }
                if(!empty($couponsinfo)) {
                        $resultAry = array('service_status'=>'success',"content" => $couponsinfo,"responsecode"=>'400');
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Purchased Uppers Found',"responsecode"=>'401');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function uppersinfoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $coupon_id = $this->getRequest()->getParam('coupon_id');
                if($coupon_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Upper Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($coupon_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Upper Id!!!'));
                        return;
                }
                $couponsObj = new Business_Model_Coupons();
                $couponsinfo = $couponsObj->getcouponbyid($coupon_id);
                if(!empty($couponsinfo)) {
                        //echo '<pre>';print_r($couponsinfo);exit;
                        $data['viewcount'] = $couponsinfo['viewcount']+1;
                        $id = $couponsObj->updatecoupondata($data,$coupon_id);
                        $couponsinfo['viewcount'] = $data['viewcount'];
                        if($couponsinfo['image_path'] != '') {
                            $couponsinfo['coupon_image'] = $couponsinfo['image_path'];
                        }
                        //foreach($couponsinfo as $key => $coupons) {
                                if(strtotime($couponsinfo['expiration_date']) > strtotime(date("Y-m-d"))) {
                                        $couponsinfo['active'] = '1';
                                } else {
                                        $couponsinfo['active'] = '0';
                                }
                        //}
                        $couponsinfo['expiration_date'] = date('d, M Y', strtotime($couponsinfo['expiration_date']));
                        $resultAry = array('service_status'=>'success',"content" => $couponsinfo,"searchcode"=>'433');
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Uppers Found',"searchcode"=>'433');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function allactiveuppersAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $pagenum = $request->pagenum;
                if($pagenum == '')
                    $pagenum = 0;
                $limit = $request->limit;
                if($limit == '')
                    $limit = 10;
                $couponsObj = new Business_Model_Coupons();
                $couponsinfo = $couponsObj->getcouponsbydate($pagenum, $limit);
                $purchasecouponsObj = new Business_Model_Purchasecoupons();
                foreach($couponsinfo as $key => $coupons) {
                    if(strtotime($coupons['expiration_date']) > strtotime(date("Y-m-d"))) {
                            $couponsinfo[$key]['active'] = '1';
                    } else {
                            $couponsinfo[$key]['active'] = '0';
                    }
                    $couponsinfo[$key]['expiration_date'] = date('d, M Y', strtotime($coupons['expiration_date']));
                    $cnt = $purchasecouponsObj->purchasedcouponcount($coupons['coupon_id']);
                    $couponsinfo[$key]['couponcnt'] = $cnt['couponscnt'];
                }
                if(!empty($couponsinfo)) {
                        $resultAry = array('service_status'=>'success',"content" => $couponsinfo,"searchcode"=>'433');
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Uppers Found',"searchcode"=>'433');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function updateuppersviewcountAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $upper_id = $request->getParam('upper_id');
                if($upper_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Upper Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($upper_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Upper Id!!!'));
                        return;
                }
                $couponsObj = new Business_Model_Coupons();
                $couponsinfo = $couponsObj->getcouponbyid($upper_id);
                $data['viewcount'] = $couponsinfo['viewcount']+1;
                $id = $couponsObj->updatecoupondata($data,$upper_id);
                if($id == 1) {
                        $resultAry = array('service_status'=>'success',"viewcount" => $data['viewcount'],"searchcode"=>'433');
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Uppers Found',"searchcode"=>'433');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getreviewsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $reviewArray = array();
                $request = $this->getRequest();
                // Business User Id CHeck
                $business_id = $request->getParam('business_id');
                if($business_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Business Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($business_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Business Id!!!'));
                        return;
                }
                // Logged User Id CHeck
                $loggedUserID = $request->getParam('logged_user_id');
                if($loggedUserID == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User Id!!!'));
                        return;
                }
                // Logged User Type CHeck
                $loggedUserType = $request->getParam('logged_user_type');
                if($loggedUserType == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Type can not be Empty!!!'));
                        return;
                }
                if($request->getParam('limit') != '')
                        $limit = $request->getParam('limit');
                else
                        $limit = 10;
                $businessObj = new Business_Model_Business();
                $reviewObj = new Business_Model_Reviews();
                // check whether the given user exist or not
                $businessArray   = array();
                $businessArray   = $businessObj->getBusinessUserexistOrNot($business_id);
                if(count($businessArray) == 1) {
                        if($loggedUserID == $business_id) {
                                $reviewArray = $reviewObj->getBusinessReviewsById($business_id, $business_id, "B", $limit);
                        } else {
                                $reviewArray = $reviewObj->getBusinessReviewsById($business_id, $loggedUserID, $loggedUserType, $limit);
                        }
                        // for changing TIME format of CREATED DATE
                        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                        foreach($reviewArray as $key=>$review) {
                                $reviewArray[$key]['created_date'] = $timeObj->humaneDate($review['created_date']);
                        }
                        //Send Response to mobile success r not
                        if (!empty($reviewArray)) {
                                $resultAry = array('service_status'=>'success',"content" => $reviewArray);
                        } else {
                                $resultAry = array('service_status'=>'success',"content" => array());
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'Business Id not Exist');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getlnglatbybusinessidAction() {
                // FOR MAP DISPLAY
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                // Business User Id CHeck
                $business_id = $request->getParam('business_id');
                if($business_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Business  Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($business_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Business Id!!!'));
                        return;
                }
                $businessObj = new Business_Model_Business();
                // check whether the given user exist or not
                $businessArray   = array();
                $businessArray   = $businessObj->getBusinessUserexistOrNot($business_id);
                if(count($businessArray) == 1) {
                        $lnglatArray = $businessObj->getLngLatByBusinessId($business_id);
                        $lnglatArray = $lnglatArray->toArray();
                        //Send Response to mobile success r not
                        if (!empty($lnglatArray)) {
                                $resultAry = array('service_status'=>'success',"content" => $lnglatArray);
                        } else {
                                $resultAry = array('service_status'=>'success',"content" => array());
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'Business Id not Exist');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function postreviewAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                //get all arguments
                $request = $this->getRequest();
                $fromUserId = $request->getParam('fromId');
                if($fromUserId == '' || $fromUserId == 0) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid FromId'));
                        return;
                }
                $fromUserType = $request->getParam('fromType');
                if($fromUserType == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Fromtype'));
                        return;
                }
                $toUserId = $request->getParam('toId');
                $scribble = $request->getParam('review');
                if($scribble == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid review'));
                        return;
                }
                if(strlen($scribble) > 150) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Review limit exceeded'));
                        return;
                }
                if($request->getParam('parentId') != '')
                        $parent_Id = $request->getParam('parentId');
                else
                        $parent_Id = 0;

                $insAry = array(
                                'parent_id' => $parent_Id,
                                'from_user_id' => $fromUserId,
                                'from_user_type' => strtoupper($fromUserType),
                                'to_user_id' => $toUserId,
                                'to_user_type'=>"B",
                                'review_message' => $scribble,
                                'created_date' => date('Y-m-d H:i:s')
                        );
                $reviewObj = new Business_Model_Reviews();
                $ins = $reviewObj->insertReview($insAry);
                if($ins){
                        //$reviewInfo = $reviewObj->getReviewInfo($ins);
                        echo $myjson->customEncode(array('service_status'=>'success',"content" => $ins));
                        return;
                } else {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'error while posting review'));
                        return;
                }
        }

        public function getcelebritiesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                if($logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }
                $pagenum = $request->pagenum;
                if($pagenum == '')
                    $pagenum = 0;
                $limit = $request->limit;
                if($limit == '')
                    $limit = 10;
                $search_key = $request->keyword;
                $searchObj = new Search_Model_Search();
                $celebritydata = $searchObj->getcelebrities($pagenum, $limit, $search_key);
                // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                $cityObj = new User_Model_Cities();
                $cityArr = array();
                if(count($celebritydata) > 0) {
                        foreach($celebritydata as $key => $popularUsr) {
                                if($key != 0 && $cityid == $popularUsr['city']) {
                                        $city = $cityname;
                                } elseif(!empty($popularUsr['city']) && $popularUsr['city'] != 0) {
                                        $cityArr = $cityObj->getCityNameByCityID($popularUsr['city']);
                                        $cityArr = $cityArr->toArray();
                                        if(!empty($cityArr))
                                                $city = $cityArr[0]['city_name'];
                                        else
                                                $city = '';
                                        $cityid = $popularUsr['city'];
                                        $cityname = $city;
                                } else {
                                        $city = '';
                                }
                                $celebritydata[$key]['city'] = $city;
                                $popularUsrType = $popularUsr['user_type'];
                                if($popularUsr['user_type'] != 'B'){
                                        $popularUsrType = 'U';
                                }
                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $Usrfollows) {
                                                if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                        $celebritydata[$key]['isFollowing'] = "2";
                                                } else {
                                                        if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                $celebritydata[$key]['isFollowing'] = "1";
                                                                break;
                                                        }  else {
                                                                $celebritydata[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                } else {
                                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                $celebritydata[$key]['isFollowing'] = "2";
                                        } else {
                                                $celebritydata[$key]['isFollowing'] = "0";
                                        }
                                }
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($celebritydata)) {
                        $resultAry = array('service_status'=>'success',"content" => $celebritydata, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Celebrities Found');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getcountriesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $countryObj = new User_Model_Countries();
                $countries = $countryObj->get_countries();
                $countries = $countries->toArray();
                if(!empty($countries)) {
                        $resultAry = array('service_status'=>'success',"content" => $countries, "responsecode" => '500');
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => 'No Countries Found', "responsecode" => '500');
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function businesspeopleAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;

                $logged_user_type = $request->getParam('logged_user_type');
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)){
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Login User Id!!!'));
                        return;
                }
                $userObj = new User_Model_User();
                $userdata = $userObj->isUserAvail($logged_user_Id);
                if($userdata == '') {
                        if($logged_user_type == '')
                            $logged_user_type = 'U';
                        if(!empty($request->keyword))
                                $search_key = $request->keyword;
                        else
                                $search_key = '';
                        if(!empty($request->lat))
                                $lat = $request->lat;
                        else
                                $lat = '';
                        if(!empty($request->lng))
                                $lng = $request->lng;
                        else
                                $lng = '';

                        $pagenum = $request->pagenum;
                        if($pagenum != '' && !is_numeric($pagenum)) {
                                echo $resultAry = json_encode(array('service_status'=>'error',"error_message" => 'Invalid Page Number', "responsecode" => '500'));
                                return;
                        }
                        if($pagenum == '')
                            $pagenum = 0;
                        $limit = $request->limit;
                        if($limit != '' && !is_numeric($limit)) {
                                echo $resultAry = json_encode(array('service_status'=>'error',"error_message" => 'Invalid Limit Format', "responsecode" => '500'));
                                return;
                        }
                        if($limit == '')
                            $limit = 30;
                        $searchObj = new Search_Model_Search();
                        // GEt Business Details based on nearest distance
                        $businessArray =  $searchObj->getNearestBusiness($lat, $lng, $pagenum, $limit, $search_key);
                        //echo '<pre>';print_r($businessArray);//exit;
                        // get Logged User Following Users-list
                        $followObj = new User_Model_Followers();
                        $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                        //echo '<pre>';print_r($loggedUserFollowing);exit;
                        $followingCnt = count($loggedUserFollowing);
                        $cityObj = new User_Model_Cities();
                        $cityid = '';
                        $cityname = '';
                        $cityArr = array();
                        if(count($businessArray) > 0) {
                                foreach($businessArray as $key => $popularUsr) {
                                        if($key != 0 && $cityid == $popularUsr['city']) {
                                                $city = $cityname;
                                        } elseif(!empty($popularUsr['city']) && $popularUsr['city'] != 0) {
                                                $cityArr = $cityObj->getCityNameByCityID($popularUsr['city']);
                                                $cityArr = $cityArr->toArray();
                                                if(!empty($cityArr))
                                                        $city = $cityArr[0]['city_name'];
                                                else
                                                        $city = '';
                                                $cityid = $popularUsr['city'];
                                                $cityname = $city;
                                        } else {
                                                $city = '';
                                        }
                                        $businessArray[$key]['city'] = $city;
                                        $popularUsrType = $popularUsr['user_type'];
                                        if($popularUsr['user_type'] != 'B'){
                                                $popularUsrType = 'U';
                                        }
                                        if($followingCnt > 0) {
                                                foreach($loggedUserFollowing as $Usrfollows) {
                                                        if($popularUsr['business_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                                $businessArray[$key]['isFollowing'] = "2";
                                                        } else {
                                                                if(($popularUsr['business_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                        $businessArray[$key]['isFollowing'] = "1";
                                                                        break;
                                                                } else {
                                                                        $businessArray[$key]['isFollowing'] = "0";
                                                                }
                                                        }
                                                }
                                        } else {
                                                if($popularUsr['business_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                        $businessArray[$key]['isFollowing'] = "2";
                                                } else {
                                                        $businessArray[$key]['isFollowing'] = "0";
                                                }
                                        }
                                }
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "User id Doesn't Exists", "userinfocode" => '419');
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($businessArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $businessArray,"notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_message" => "No Records Found");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function searchfriendsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Login User Id!!!'));
                        return;
                }
                //$logged_user_type = 'U';
                $search_key = $request->keyword;
                $limit = $request->limit;
                if($limit == '') {
                        $limit = 20;
                }
                $searchObj = new Search_Model_Search();
                $businessArray =  $searchObj->getFriends($limit, $search_key);
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                if(!empty($businessArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $businessArray,"notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Businesses Found");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function userdataforeditAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();//upmeVersion=1.0.3
                $user_id = $this->getRequest()->getParam('user_id');
                $upmeVersion = $this->getRequest()->getParam('upmeVersion');
                $versionary = array('1.0.3','1.3.0');
                $versionpass = @in_array($upmeVersion, $versionary);
                $osType = $this->getRequest()->getParam('osType');
                if(!empty($user_id)) {
                        $userObj = new User_Model_User();
                        $exists = $userObj->isUserAvail($user_id);
                        if($exists == '') {
                                $userdata = $userObj->getUserDetails($user_id);
                                $userdata = $userdata->toArray();
                                //echo '<pre>';print_r($userdata);exit;
                                if(!empty($userdata)) {
                                        //Fetching Countries
                                        $countriesObj = new User_Model_Countries();
                                        if($versionpass && $osType!='iphone') {
                                            $countries = $countriesObj->getCountryNameByCountryID($userdata['country']);
                                            $countries = $countries->toArray();
                                        } else {
                                            $countries = $countriesObj->get_countries();
                                            $countries = $countries->toArray();
                                        }
                                        //Fetching States Based On CountryId
                                        $statesObj = new User_Model_States();
                                        if($versionpass && $osType!='iphone') {
                                            $states = $statesObj->getStateNameByStateID($userdata['state']);
                                            $states = $states->toArray();
                                        } else {
                                            $states = $statesObj->get_states($userdata['country'],50);
                                            $states = $states->toArray();
                                        }
                                        //Fetching Cities Based On StateId
                                        $cityObj = new User_Model_Cities();
                                        if($versionpass && $osType!='iphone') {
                                            $cities = $cityObj->getCityNameByCityID($userdata['city']);
                                            $cities = $cities->toArray();
                                        } else {
                                            $cities = $cityObj->get_cities($userdata['state'],50);
                                            $cities = $cities->toArray();
                                        }
                                        //Fetching Zipcode Basedon CityId
                                        if($versionpass && $osType!='iphone') {
                                            $zipcodes = $cityObj->getZipcodeByZipID($userdata['zipcode']);
                                        } else {
                                            $zipcodes = $cityObj->getZipcodesByCity($userdata['city'],50);
                                        }
                                        if($versionpass && $osType!='iphone') {
                                            $schools[] = $userObj->getschooldata($userdata['high_school_info']);
                                        } else {
                                            $schools = $userObj->getSchools();
                                        }
                                        if($versionpass && $osType!='iphone') {
                                            $colleges[] = $userObj->getcollegedata($userdata['college_info']);
                                        } else {
                                            $colleges = $userObj->getColleges();
                                        }
                                        if($versionpass && $osType!='iphone') {
                                            $workplaces[] = $userObj->getWorkPlacedata($userdata['employer_info']);
                                        } else {
                                            $workplaces = $userObj->getWorkPlaces();
                                        }
                                        $notificationObj = new Notifications_Model_Notifications();
                                        $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                                        $resultAry = array('service_status'=>'success',"content" => $userdata, "countries" => $countries, "states" => $states, "cities" => $cities, "zipcodes" => $zipcodes, 'schools' => $schools, "colleges" => $colleges, "workplaces" => $workplaces,"notificationcnt"=>$notificationcnt['notificationcnt'], "userinfocode" => '418');
                                } else {
                                        $resultAry = array('service_status'=>'error',"error_msg" => "User id Doesn't Exists", "userinfocode" => '419');
                                }
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => "Invalid User Id", "userinfocode" => '419');
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "User id can't be Empty", "userinfocode" => '419');
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
                return false;
        }
        public function getusersettingsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $user_id = $this->getRequest()->getParam('user_id');
                if(!empty($user_id)) {
                        $userObj = new User_Model_User();
                        $exists = $userObj->isUserAvail($user_id);
                        if($exists == '') {
                                $settingsdata = $userObj->getusersettings($user_id);
                                if(!empty($settingsdata)) {
                                        //Fetching Languages
                                        $lanObj = new User_Model_Languages();
                                        $languages = $lanObj->get_languages();
                                        $languages = $languages->toArray();
                                        //Fetching Languages
                                        $timeObj = new User_Model_Timezones();
                                        $timezones = $timeObj->get_timezones();
                                        $timezones = $timezones->toArray();
                                        $notificationObj = new Notifications_Model_Notifications();
                                        $notificationcnt = $notificationObj->notificationcnt($user_id, "U", '1');
                                        $resultAry = array('service_status'=>'success',"content" => $settingsdata, "languages" => $languages, "timezones" => $timezones, "notificationcnt"=>$notificationcnt['notificationcnt'], "userinfocode" => '418');
                                }
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => "Invalid User Id", "userinfocode" => '419');
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "User id can't be Empty", "userinfocode" => '419');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function updateusersettingsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $user_id = $this->getRequest()->getParam('user_id');
                if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "User id can't be Empty", "userinfocode" => '419'));
                        return false;
                }
                $user_language = $this->getRequest()->getParam('user_language');
                $user_timezone = $this->getRequest()->getParam('user_timezone');
                $privacy = $this->getRequest()->getParam('privacy');
                $location = $this->getRequest()->getParam('location');
                $userObj = new User_Model_User();
                $exists = $userObj->isUserAvail($user_id);
                if($exists == '') {
                        $data['user_language'] = $user_language;
                        $data['user_timezone'] = $user_timezone;
                        $data['privacy'] = $privacy;
                        $data['location'] = $location;
                        $id = $userObj->updateUser($data, $user_id);
                        if(!empty($id)) {
                                $resultAry = array('service_status'=>'success',"content" => 'Successfully Updated');
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => '');
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Invalid User Id", "userinfocode" => '419');
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function userprofilephotoeditAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $user_id = $this->getRequest()->getParam('user_id');
                $w = $this->getRequest()->getParam('width');
                $h = $this->getRequest()->getParam('height');
                $aw = $this->getRequest()->getParam('android_width');
                $ah = $this->getRequest()->getParam('android_height');
                $width = 0;
                $height = 0;
                $android_width = 0;
                $android_height = 0;
                if(!empty($w))
                    $width = $w;
                if(!empty($h))
                    $height = $h;
                if(!empty($aw))
                    $android_width = $aw;
                if(!empty($ah))
                    $android_height = $ah;
                $userObj = new User_Model_User();
                if(!empty($user_id)) {
                        $userexists = $userObj->isUserAvail($user_id);
                        if($userexists == '') {
                                if($this->getRequest()->isPost()) {
                                        if(!empty($_FILES['image']['name'])) {
                                                $dir_upload = UPLOAD_PATH.'images/original/';
                                                $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                                                $medium_path = UPLOAD_PATH.'images/medium/';
                                                $mobile_path = UPLOAD_PATH.'images/mobile/';
                                                $android_path = UPLOAD_PATH.'images/android/';
                                                $fileName = $_FILES['image']['name'];
                                                $image = new UpmeSocial_Controller_Action_Helper_Image();
                                                $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);

                                                 // Upload images at Amazone S3 server
                                                $uploadfiles = new Mobile_Model_Uploadfiles();
                                                $resp1 = $uploadfiles->uploadImagesAtS3Server($arrFileName['image'],$user_id);

                                                //Saving imagesedit to user
                                                $data['profile_pic_path'] = IMAGES_PATH.$user_id."/".$arrFileName['image'];
                                                $album['user_id'] = $user_id;
                                                $album['user_type'] = 'U';
                                                $album['album_name'] = 'Profile Photos';
                                                //echo '<pre>';print_r($album);exit;
                                                $mobileobj = new Mobile_Model_Albums();
                                                $val = $mobileobj->albumnameexistsrnot($album);
                                                //echo $val.'<pre>';print_r($val);exit;
                                                $album['created_date'] = date('Y-m-d H:i:s');
                                                if(!empty($val)) {
                                                    $albumdet = $mobileobj->getAlbumsByUserId($user_id, 'U');
                                                    $id = $mobileobj->update($album,$albumdet['0']['id']);
                                                    $albumid = $albumdet['0']['id'];
                                                } else {
                                                    $album['is_default'] = '1';
                                                    $albumid = $mobileobj->insert($album);
                                                }
                                                $photoObj = new Mobile_Model_Photos();
                                                $coverphoto = $photoObj->albumidexistsrnot($albumid,$user_id,'U');
                                                $photodata['album_id'] = $albumid;
                                                $photodata['photo_name'] = 'Profile Photo';
                                                $photodata['photo_original_name'] = IMAGES_PATH.$user_id."/".$fileName;
                                                $photodata['photo_modified_name'] = $data['profile_pic_path'];
                                                $photodata['width'] = $arrFileName['width'];
                                                $photodata['height'] = $arrFileName['height'];
                                                $photodata['android_width'] = $arrFileName['android_width'];
                                                $photodata['android_height'] = $arrFileName['android_height'];
                                                $photodata['user_id'] = $user_id;
                                                $photodata['user_type'] = 'U';
                                                if(!empty($coverphoto['photo_id']))
                                                        $photodata['cover_photo'] = '0';
                                                else
                                                        $photodata['cover_photo'] = '1';
                                                $photodata['created_date'] = date('Y-m-d H:i:s');
                                                $photodata['blasted_date'] = $photodata['created_date'];
                                                $photo_id = $photoObj->insert($photodata);
                                                $id = $userObj->updateUser($data, $user_id);
                                                if(!empty($id))
                                                        $resultAry = array('service_status'=>'success',"content" => $arrFileName['image'], "userinfocode" => '418');
                                        } else {
                                                $resultAry = array('service_status'=>'error',"error_msg" => "File data can't be Empty", "userinfocode" => '419');
                                        }
                                } else {
                                        $resultAry = array('service_status'=>'error',"error_msg" => "Some Error Occured in data posting", "userinfocode" => '419');
                                }
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => "Invalid User Id", "userinfocode" => '419');
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "User Id can't be Empty", "userinfocode" => '419');
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function userbusinessAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $logged_user_id = $this->getRequest()->getParam('logged_user_id');
                $logged_user_type = $this->getRequest()->getParam('logged_user_type');
                if($logged_user_id == ''){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "Logged User id can not be Empty!!!"));
                    return false;
                }
                if(!is_numeric($logged_user_id)){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "Invalid Logged User id!!!"));
                    return false;
                }

                $pagenum = $this->getRequest()->getParam('pagenum');
                if($pagenum != '' && !is_numeric($pagenum)){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "Invalid Page Number Format!!!"));
                    return false;
                }
                $limit = $this->getRequest()->getParam('limit');
                if($limit != '' && !is_numeric($limit)){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "Invalid Limit Format!!!"));
                    return false;
                }

                if($pagenum == '') $pagenum = 0;
                if($limit == '') $limit = 30;
                if($logged_user_type == '')
                    $logged_user_type = "U";
                if(!empty($logged_user_id)) {
                        $userObj = new User_Model_User();
                        $userdata = $userObj->isUserAvail($logged_user_id);
                        if($userdata == '') {
                                $follwersObj = new User_Model_Followers();
                                $businessdata = $follwersObj->getFollowingbusiness($logged_user_id, $logged_user_type, $pagenum, $limit);
                                $followObj = new User_Model_Followers();
                                $loggedUserFollowing = $followObj->getFollowing($logged_user_id, $logged_user_type);
                                $followingCnt = count($loggedUserFollowing);
                                if(count($businessdata) > 0) {
                                        foreach($businessdata as $key => $popularUsr) {
                                                $popularUsrType = 'B';
                                                if($followingCnt > 0) {
                                                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                                if($popularUsr['business_id'] == $logged_user_id && $popularUsrType == $logged_user_type) {
                                                                        $businessdata[$key]['isFollowing'] = "2";
                                                                } else {
                                                                        if(($popularUsr['business_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                                $businessdata[$key]['isFollowing'] = "1";
                                                                                break;
                                                                        } else {
                                                                                $businessdata[$key]['isFollowing'] = "0";
                                                                        }
                                                                }
                                                        }
                                                } else {
                                                        if($popularUsr['business_id'] == $logged_user_id && $popularUsrType == $logged_user_type) {
                                                                $businessdata[$key]['isFollowing'] = "2";
                                                        } else {
                                                                $businessdata[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                    $resultAry = array('service_status'=>'success',"content" => $businessdata);
                                } else {
                                    $resultAry = array('service_status'=>'error',"error_msg" => "No Records Found");
                                }
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => "User id Doesn't Exists");
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "User id can't be Empty");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getlatestnotificationsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $followObj = new User_Model_Followers();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                $notificationObj = new Notifications_Model_Notifications();
                $result = $notificationObj->getUserUnreadNotifications($logged_user_Id,$logged_user_type,'10');
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                $followerscnt = $followObj->getFollowersCnt($logged_user_Id);
                $followingscnt = $followObj->getFollowingCnt($logged_user_Id);
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($result) > 0) {
                    foreach($result as $key=>$notification) {
                        if(isset($notification['action_date']) && $notification['action_date'] != '') {
                            $result[$key]['action_date'] = $timeObj->humaneDate($notification['action_date']);
                        }
                        if(isset($notification['upper_created_date']) && $notification['upper_created_date'] != '') {
                            $result[$key]['upper_created_date'] = $timeObj->humaneDate($notification['upper_created_date']);
                        }
                        if(isset($notification['purchased_date']) && $notification['purchased_date'] != '') {
                            $result[$key]['purchased_date'] = $timeObj->humaneDate($notification['purchased_date']);
                        }
                    }
                }
                $tempResult = $result;
                $result = array();
                if(count($tempResult) > 0){
                    foreach($tempResult as $key)
                    if(count($key) > 0)
                        $result[] = $key;
                }
                //echo '<pre>';print_r($result);exit;
                $notificationscnt = count($result);
                for($i=0;$i<$notificationscnt;$i++) {
                        if(!empty($result[$i]['notification_id']))
                                $notificationIds[] =  $result[$i]['notification_id'];
                }
                $ids = @implode(",", $notificationIds);
                if(!empty($ids)) {
                        $data['read_status'] = '0';
                        $id = $notificationObj->updatereadstatus($data, @implode(",", $notificationIds));
                }
                //echo '<pre>';print_r($result);exit;
                if(!empty($result)) {
                        $resultAry = array('service_status'=>'success', "content" => $result, "notificationcnt" => $notificationcnt['notificationcnt'],"followerscnt" => $followerscnt, "followingscnt" => $followingscnt);
                } else {
                        $resultAry = array('service_status'=>'error', "error_msg" => "No Notifications to View","followerscnt" => $followerscnt, "followingscnt" => $followingscnt);
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getallnotificationsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                //$action_date = $request->action_date;
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                //if($_SERVER['REMOTE_ADDR'] == '182.72.66.214') { echo "<pre>"; print_r($request->getParams()); exit(); }
                $notification_id = $request->getParam('notification_id');
                $type = $request->getParam('type');
                $limit = $request->limit;
                if($limit == '')
                        $limit = 30;
                $userObj = new User_Model_User();
                $userdata = $userObj->isUserAvail($logged_user_Id);
                if($userdata == '') {
                        $notificationObj = new Notifications_Model_Notifications();
                        $result = $notificationObj->getUserNotifications($logged_user_Id,$logged_user_type,$limit,$notification_id,$type);
                        // for changing TIME format of CREATED DATE
                        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                        if(count($result) > 0) {
                                foreach($result as $key=>$notification) {
                                        if(isset($notification['action_date']) && $notification['action_date'] != ''){
                                                $result[$key]['action_date'] = $timeObj->humaneDate($notification['action_date']);
                                        }
                                        if(isset($notification['upper_created_date']) && $notification['upper_created_date'] != ''){
                                                $result[$key]['upper_created_date'] = $timeObj->humaneDate($notification['upper_created_date']);
                                        }
                                        if(isset($notification['purchased_date']) && $notification['purchased_date'] != ''){
                                                $result[$key]['purchased_date'] = $timeObj->humaneDate($notification['purchased_date']);
                                        }
                                }
                        }
                        $tempResult = $result;
                        $result = array();
                        if(count($tempResult) > 0){
                                foreach($tempResult as $key)
                                        if(count($key) > 0)
                                                $result[] = $key;
                        }
                        $notificationscnt = count($result);
                        for($i=0;$i<$notificationscnt;$i++) {
                                if(!empty($result[$i]['notification_id']))
                                        $notificationIds[] =  $result[$i]['notification_id'];
                        }
                        $ids = @implode(",", $notificationIds);
                        if(!empty($ids)) {
                                $data['read_status'] = '0';
                                $id = $notificationObj->updatereadstatus($data, @implode(",", $notificationIds));
                        }
                        if(!empty($result)) {
                                $upd = $notificationObj->updateNotificationReadStatus($logged_user_Id,$logged_user_type);
                                $resultAry = array('service_status'=>'success',"content" => $result, "notificationcnt" => $notificationscnt,"type"=>$type);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => "No Notifications Found","type"=>$type);
                        }
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Invalid User ID");
                }
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                    //echo "<pre>";print_r($resultAry);exit;
                }
                $cusomEncodeHelper = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $cusomEncodeHelper->customEncode($resultAry);
                return false;
        }

        public function getpopularbusinessAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                if($logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }

                $pagenum = $request->pagenum;
                if($pagenum != '' && !is_numeric($pagenum)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Page Number Format!!!'));
                        return;
                }
                if($pagenum == '')
                    $pagenum = '0';

                $limit = $request->limit;
                if($limit != '' && !is_numeric($limit)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Limit Format!!!'));
                        return;
                }
                if($limit == '')
                    $limit = 30;
                $businessObj = new Business_Model_Business();
                $business = $businessObj->getpopularbusiness($pagenum, $limit);
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                if(count($business) > 0) {
                        foreach($business as $key => $popularUsr) {
                                $popularUsrType = $popularUsr['user_type'];
                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                if($popularUsr['business_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                        $business[$key]['isFollowing'] = "2";
                                                } else {
                                                        if(($popularUsr['business_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                $business[$key]['isFollowing'] = "1";
                                                                break;
                                                        } else {
                                                                $business[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                } else {
                                        $business[$key]['isFollowing'] = "0";
                                }
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($business)) {
                        $resultAry = array('service_status'=>'success',"content" => $business, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Businesses Available');
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getpopularschoolsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $logged_user_id = $this->getRequest()->getParam('logged_user_id');
                if($logged_user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $userObj = new User_Model_User();
                $userdata = $userObj->isUserAvail($logged_user_id);
                if($userdata == '') {
                        $schoolsdata = $userObj->getpopularschools();
                        $followObj = new User_Model_Followers();
                        $loggedUserFollowing = $followObj->getAllFollowing($logged_user_id);
                        $followingCnt = count($loggedUserFollowing);
                        if(count($schoolsdata) > 0) {
                                foreach($schoolsdata as $key => $popularUsr) {
                                        $popularUsrType = 'S';
                                        if($followingCnt > 0) {
                                                foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                        if($popularUsr['school_id'] == $logged_user_id && $popularUsrType == 'U') {
                                                                $schoolsdata[$key]['isFollowing'] = "2";
                                                        } else {
                                                                if(($popularUsr['school_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                        $schoolsdata[$key]['isFollowing'] = "1";
                                                                        break;
                                                                }  else {
                                                                        $schoolsdata[$key]['isFollowing'] = "0";
                                                                }
                                                        }
                                                }
                                        } else {
                                                if($popularUsr['school_id'] == $logged_user_id && $popularUsrType == 'U') {
                                                        $schoolsdata[$key]['isFollowing'] = "2";
                                                } else {
                                                        $schoolsdata[$key]['isFollowing'] = "0";
                                                }
                                        }
                                }
                        }
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($logged_user_id, "U", '1');
                        if(!empty($schoolsdata)) {
                                $resultAry = array('service_status'=>'success',"content" => $schoolsdata,"notificationcnt" => $notificationcnt['notificationcnt']);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => 'No Popular Schools Available');
                        }
                } else {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getpopularcollegesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $logged_user_id = $this->getRequest()->getParam('logged_user_id');
                if($logged_user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $userObj = new User_Model_User();
                $userdata = $userObj->isUserAvail($logged_user_id);
                if($userdata == '') {
                        $collegesdata = $userObj->getpopularcolleges();
                        $followObj = new User_Model_Followers();
                        $loggedUserFollowing = $followObj->getAllFollowing($logged_user_id);
                        $followingCnt = count($loggedUserFollowing);
                        if(count($collegesdata) > 0) {
                                foreach($collegesdata as $key => $popularUsr) {
                                        $popularUsrType = 'C';
                                        if($followingCnt > 0) {
                                                foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                        if($popularUsr['college_id'] == $logged_user_id && $popularUsrType == 'U') {
                                                                $collegesdata[$key]['isFollowing'] = "2";
                                                        } else {
                                                                if(($popularUsr['college_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                        $collegesdata[$key]['isFollowing'] = "1";
                                                                        break;
                                                                }  else {
                                                                        $collegesdata[$key]['isFollowing'] = "0";
                                                                }
                                                        }
                                                }
                                        } else {
                                                if($popularUsr['college_id'] == $logged_user_id && $popularUsrType == 'U') {
                                                        $collegesdata[$key]['isFollowing'] = "2";
                                                } else {
                                                        $collegesdata[$key]['isFollowing'] = "0";
                                                }
                                        }
                                }
                        }
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($logged_user_id, "U", '1');
                        if(!empty($collegesdata)) {
                                $resultAry = array('service_status'=>'success',"content" => $collegesdata,"notificationcnt" => $notificationcnt['notificationcnt']);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => 'No Popular Schools Available');
                        }
                } else {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getpopularworkplacesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $logged_user_id = $this->getRequest()->getParam('logged_user_id');
                if($logged_user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $userObj = new User_Model_User();
                $userdata = $userObj->isUserAvail($logged_user_id);
                if($userdata == '') {
                        $workplacedata = $userObj->getpopularworkplaces();
                        $followObj = new User_Model_Followers();
                        $loggedUserFollowing = $followObj->getAllFollowing($logged_user_id);
                        $followingCnt = count($loggedUserFollowing);
                        if(count($workplacedata) > 0) {
                                foreach($workplacedata as $key => $popularUsr) {
                                        $popularUsrType = 'W';
                                        if($followingCnt > 0) {
                                                foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                        if($popularUsr['work_place_id'] == $logged_user_id && $popularUsrType == 'U') {
                                                                $workplacedata[$key]['isFollowing'] = "2";
                                                        } else {
                                                                if(($popularUsr['work_place_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                        $workplacedata[$key]['isFollowing'] = "1";
                                                                        break;
                                                                }  else {
                                                                        $workplacedata[$key]['isFollowing'] = "0";
                                                                }
                                                        }
                                                }
                                        } else {
                                                if($popularUsr['work_place_id'] == $logged_user_id && $popularUsrType == 'U') {
                                                        $workplacedata[$key]['isFollowing'] = "2";
                                                } else {
                                                        $workplacedata[$key]['isFollowing'] = "0";
                                                }
                                        }
                                }
                        }
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($logged_user_id, "U", '1');
                        if(!empty($workplacedata)) {
                                $resultAry = array('service_status'=>'success',"content" => $workplacedata,"notificationcnt" => $notificationcnt['notificationcnt']);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => 'No Popular Work Places Available');
                        }
                } else {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function uppopularacademyAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                //echo '<pre>';print_r($this->getRequest()->getParams());exit;
                $logged_user_Id = $this->getRequest()->getParam('logged_user_id');
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $this->getRequest()->getParam('logged_user_type');
                if($logged_user_type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                $follower_type = $this->getRequest()->getParam('follower_type');
                if($follower_type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Follower Type can not be Empty!!!'));
                        return;
                }
                $follower_id = $this->getRequest()->getParam('follower_id');
                if($follower_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Follower Id can not be Empty!!!'));
                        return;
                }
                $userObj = new User_Model_User();
                $user = $userObj->isUserAvail($logged_user_Id);
                if($user != '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id Doesnot exists'));
                        return;
                }
                if($follower_type == 'S' && $follower_id != '') {
                        $follower = $userObj->isSchoolAvail($follower_id);
                        if(!empty($follower)) {
                                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "School Does not exists"));
                                return;
                        }
                        $followedrnot = $userObj->isUserFollowingAcademy($follower_id,$follower_type,$logged_user_Id,$logged_user_type);
                        if($followedrnot == '') {
                                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "You have already followed this School"));
                                return;
                        }
                }
                if($follower_type == 'C' && $follower_id != '') {
                        $follower = $userObj->isCollegeAvail($follower_id);
                        if(!empty($follower)) {
                                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "College Does not exists"));
                                return;
                        }
                        $followedrnot = $userObj->isUserFollowingAcademy($follower_id,$follower_type,$logged_user_Id,$logged_user_type);
                        if($followedrnot == '') {
                                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "You have already followed this College"));
                                return;
                        }
                }
                if($follower_type == 'W' && $follower_id != '') {
                        $follower = $userObj->isWorkPlaceAvail($follower_id);
                        if(!empty($follower)) {
                                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "Work Place Does not exists"));
                                return;
                        }
                        $followedrnot = $userObj->isUserFollowingAcademy($follower_id,$follower_type,$logged_user_Id,$logged_user_type);
                        if($followedrnot == '') {
                                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "You have already followed this Work Place"));
                                return;
                        }
                }
                if($user == '' && $follower == '') {
                        $followerObj = new User_Model_Followers();
                        $data['user_id'] = $logged_user_Id;
                        $data['user_type'] = $logged_user_type;
                        $data['follower_id'] = $follower_id;
                        $data['follower_type'] = $follower_type;
                        $data['created_date'] = date('Y-m-d H:i:s');
                        $id = $followerObj->insertNewRecord($data);
                        if(!empty($id) && $follower_type == 'S') {
                                $schooldata = $userObj->getschooldata($follower_id);
                                //echo '<pre>';print_r($schooldata);
                                $school['popular_order'] = $schooldata['followerscnt']+1;
                                $upid = $userObj->updateschool($school, $follower_id);
                                //$followercnt = $userObj->getSchoolFollowersCnt($follower_id);
                                if($upid == '1') {
                                        $resultAry = array('service_status'=>'success',"content" => "School Successfully Followed","followercnt" => $schooldata['followerscnt']);
                                } else {
                                        $resultAry = array('service_status'=>'error',"error_msg" => "Some error occured");
                                }
                        }
                        if(!empty($id) && $follower_type == 'C') {
                                $collegedata = $userObj->getcollegedata($follower_id);
                                $college['popular_order'] = $collegedata['followerscnt']+1;
                                $upid = $userObj->updatecollege($college, $follower_id);
                                //$followercnt = $userObj->getCollegeFollowersCnt($follower_id);
                                if($upid == '1') {
                                        $resultAry = array('service_status'=>'success',"content" => "College Successfully Followed","followercnt" => $collegedata['followerscnt']);
                                } else {
                                        $resultAry = array('service_status'=>'error',"error_msg" => "Some error occured");
                                }
                        }
                        if(!empty($id) && $follower_type == 'W') {
                                $workplacedata = $userObj->getworkplacedata($follower_id);
                                $workplace['popular_order'] = $workplacedata['followerscnt']+1;
                                $upid = $userObj->updateworkplace($workplace, $follower_id);
                                //$followercnt = $userObj->getWorkPlaceFollowersCnt($follower_id);
                                if($upid == '1') {
                                        $resultAry = array('service_status'=>'success',"content" => "Work Place Successfully Followed","followercnt" => $workplacedata['followerscnt']);
                                } else {
                                        $resultAry = array('service_status'=>'error',"error_msg" => "Some error occured");
                                }
                        }
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function searchallusersAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Login User Id!!!'));
                        return;
                }
                $logged_user_type = 'U';
                $search_key = $request->keyword;
                if($search_key == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'keyword can not be Empty!!!'));
                        return;
                }
                if(is_numeric($search_key)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid keyword!!!'));
                        return;
                }
                // For Pull to Refresh
               $pagenum = $request->pagenum;
                if($pagenum == '' || ($pagenum <= 0))
                    $pagenum = 0;
                $limit = $request->limit;
                if($limit == '')
                    $limit = 30;
                $searchObj = new Search_Model_Search();
                $userArray =  $searchObj->getAllUsers($logged_user_Id, $limit, $pagenum, $search_key);
                // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                $cityObj = new User_Model_Cities();
                $cityid = '';
                $cityname = '';
                $cityArr = array();
                if(count($userArray) > 0) {
                        foreach($userArray as $key => $popularUsr) {
                                if($key != 0 && $cityid == $popularUsr['city']) {
                                        $city = $cityname;
                                } elseif(!empty($popularUsr['city']) && $popularUsr['city'] != 0) {
                                        $cityArr = $cityObj->getCityNameByCityID($popularUsr['city']);
                                        $cityArr = $cityArr->toArray();
                                        if(!empty($cityArr))
                                                $city = $cityArr[0]['city_name'];
                                        else
                                                $city = '';
                                        $cityid = $popularUsr['city'];
                                        $cityname = $city;
                                } else {
                                        $city = '';
                                }
                                $userArray[$key]['city'] = $city;
                                $popularUsrType = $popularUsr['user_type'];
                                if($popularUsr['user_type'] != 'B') {
                                        $popularUsrType = 'U';
                                }
                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $Usrfollows) {
                                                if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                        $userArray[$key]['isFollowing'] = "2";
                                                } else {
                                                        if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                $userArray[$key]['isFollowing'] = "1";
                                                                break;
                                                        } else {
                                                                $userArray[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                } else {
                                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                $userArray[$key]['isFollowing'] = "2";
                                        } else {
                                                $userArray[$key]['isFollowing'] = "0";
                                        }
                                }
                        }
                }
                //echo "<pre>";print_r($userArray);exit;
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($userArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $userArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Users Found");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getchildscribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                //echo '<pre>';print_r($request->getParams());exit;
                $user_Id = $request->user_id;
                if($user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }

                if(!is_numeric($user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }

                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)){
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Login User Id!!!'));
                        return;
                }

                $parent_Id = $request->parent_id;
                if($parent_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Parent Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($parent_Id)){
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Parent Id!!!'));
                        return;
                }
                $scribbleObj = new Scribbles_Model_Scribbles();
                $exists = $scribbleObj->getScribleInfo($parent_Id);
                if($exists == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Parent Id!!!'));
                        return;
                }

                $scribble_owner_Id = $request->scribble_owner_id;
                if($scribble_owner_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Scribble Owner Id can not be Empty!!!'));
                        return;
                }

                $logged_user_type = 'U';
                $limit = $request->limit;
                if($limit == '') {
                        $limit = 20;
                }

//                $followObj = new User_Model_Followers();
//                $userfollowing = $followObj->isLoggedUserFollowingOtherUser($scribble_owner_Id, 'U', $logged_user_Id, "U");
//                //echo "<pre>";print_r($userfollowing);exit;
//                if(count($userfollowing) == 0 && $scribble_owner_Id != $logged_user_Id) {
//                        $resultAry = array('service_status'=>'error',"error_msg" => "You need to Follow the user to view the scribbles", "isFollowing"=>0);
//                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
//                         echo $myjson->customEncode($resultAry);
//                        return false;
//                }

                $usrLevelObj = new User_Model_Userlevels();
                $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($logged_user_Id, "U");
                $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];

                $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parent_Id, $user_Id,$logged_user_Id,$logged_user_type, $loggedUserLevel);
                $childScribbles = $scribbleObj->getChildScribblesByParentId($parent_Id, $user_Id,$logged_user_Id,$logged_user_type, $loggedUserLevel);
                //echo '<pre>';print_r($childScribbles);exit;

                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                // IF Blasted Scribble, Get the Original Owner Details
                foreach($childScribbles as $key=>$scrib) {
                        if($scrib['blast_id'] != 0) {
                                // To Get Owner Details of the Blasted Scribble
                                $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                //echo "<pre>";print_r($ownerArray);exit;
                                $childScribbles[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                $childScribbles[$key]['owner_gender'] = $ownerArray[0]['gender'];
                                $childScribbles[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                $childScribbles[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                $childScribbles[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                if($scribArray[$key]['owner_user_id'] == $loggedUserID && $childScribbles[$key]['owner_user_type'] == "U") {
                                        $childScribbles[$key]['isBlastedScribble'] = 2;
                                }
                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $childScribbles[$key]['owner_user_type'] == "U") {
                                        $childScribbles[$key]['isBlastedScribble'] = 1;
                                }
                        }
                        //  check if the Parent scribble is Blasted
                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($scrib['scribble_id']);
                        $childScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                        $blastedUsrAry = array();
                        if($blastedUsrCnt > 0) {
                                //to Get Blasted User Details of the Parent Scribble
                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($scrib['scribble_id'], $logged_user_Id);
                        }
                        $childScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                        if($scrib['blast_id'] == 0) {
                                foreach($blastedUsrAry as $blastedby) {
                                        if($blastedby['user_id'] == $logged_user_Id) {
                                                $childScribbles[$key]['isBlastedScribble'] = 1;
                                        }
                                }
                        }
                        //// for changing TIME format of CREATED DATE
                        $childScribbles[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                        //$childScribbles[$key]['follow_created_date'] = $timeObj->humaneDate($scrib['follow_created_date']);
                        $childScribbles[$key]['notifications_type'] = "scribble";
                        $scrib['notifications_type'] = "scribble";
                        if($scrib['notifications_type'] != 'scribble') continue;
                        $childScribbles[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,28);
                }

                // TO get Parent Scribble Info ( as requested by Mobile team )
                $parentScribbleAry = array();
                $parentScribbleAry = $scribbleObj->getScribleInfo($parent_Id);
                $parentScribbleAry['replyCount'] = $childScribblesCnt;
                $parentScribbleAry['created_date'] = $timeObj->humaneDate($parentScribbleAry['created_date']);
                $parentScribbleAry['scribbleusername'] = $scribObj->scribbleUname($parentScribbleAry,28);

                //  check if the Parent scribble is Blasted
                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parent_Id);
                $parentScribbleAry['BlastedUsersCnt'] = $blastedUsrCnt;
                $blastedUsrAry = array();
                if($blastedUsrCnt > 0) {
                        //to Get Blasted User Details of the Parent Scribble
                        $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parent_Id, $user_Id);
                }
                $parentScribbleAry['BlastedUsers'] = $blastedUsrAry;
                if($parentScribbleAry['user_id'] == $logged_user_Id) {
                        $parentScribbleAry['isUpDownScribble'] = "2";
                        $parentScribbleAry['UpDownAction'] = "UP";
                        $parentScribbleAry['isBlastedScribble'] = "2";
                }
                //echo '<pre>';print_r($parentScribbleAry);exit;

                // check if Other user performed Up / down / Blast Actions
                if($parentScribbleAry['user_id'] != $logged_user_Id) {
//                    $isUpDownBlastAry = array();
//                    $isUpDownBlastAry = $scribbleObj->getUpDownAndBlastByScribbleAndUserID($parentScribbleAry[0]['scribble_id'],$user_Id);
//                    //echo "<pre>";print_r($parentScribbleAry);print_r($isUpDownBlastAry);exit;
//                    $parentScribbleAry[0]['isUpDownScribble'] = $isUpDownBlastAry['isUpDownScribble'];
//                    $parentScribbleAry[0]['UpDownAction'] = $isUpDownBlastAry['UpDownAction'];
//                    $parentScribbleAry[0]['isBlastedScribble'] = $isUpDownBlastAry['isBlastedScribble'];

                    $updownAry = array();
                    $updownAry = $scribbleObj->getUpDownActionByUserId($parentScribbleAry['scribble_id'], $logged_user_Id);
                    if(empty($updownAry)){
                        $parentScribbleAry['isUpDownScribble']="";
                        $parentScribbleAry['UpDownAction']="0";
                    }
                    if(!empty($updownAry)){
                        $parentScribbleAry['isUpDownScribble']= $updownAry[0]['isUpDownScribble'];
                        $parentScribbleAry['UpDownAction']= $updownAry[0]['UpDownAction'];
                    }

                    $blastAry = array();
                    $blastAry = $scribbleObj->getBlastActionByUserId($parentScribbleAry['scribble_id'], $logged_user_Id);
                    if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') {
                        //echo "<pre>";print_r($blastAry);exit;
                    }
                    if(empty($blastAry)) {
                        $parentScribbleAry['isBlastedScribble']="0";
                    }
                    if(!empty($blastAry)) {
                        $parentScribbleAry['isBlastedScribble']=$blastAry[0]['isBlastedScribble'];
                    }
                }

                //$childScribbles['ParentScribbleInfo'] = $parentScribbleAry;
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');

                if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') {
                    //echo "<pre>";print_r($childScribbles);exit;
                }

                if(!empty($childScribbles)) {
                        $resultAry = array('service_status'=>'success',"content" => $childScribbles, "notificationcnt" => $notificationcnt['notificationcnt'], "ParentScribbleInfo" => array($parentScribbleAry));
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Scribbles Found", "ParentScribbleInfo" => array($parentScribbleAry));
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public  function businesshotspotsAction(){
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)){
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Login User Id!!!'));
                        return;
                }
                $userObj = new User_Model_User();
                $userdata = $userObj->isUserAvail($logged_user_Id);
                if($userdata == '') {
                    $logged_user_type = 'U';
                    $lat = $request->lat;
                     if($lat == '') {
                            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Latitude can not be Empty!!!'));
                            return;
                    }
                    $lng = $request->lng;
                     if($lng == '') {
                            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Longitude can not be Empty!!!'));
                            return;
                    }

                    $limit = $request->limit;

                    $businessObj = new Business_Model_Business();
                     // GEt Business Details based on nearest distance
                    $resultAry =  $businessObj->getHotSpots($lat, $lng, $limit);
                    //echo "<pre>";print_r($resultAry);exit;
                    // get Logged User Following Users-list
                    $followObj = new User_Model_Followers();
                    $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                    //echo "<pre>";print_r($loggedUserFollowing);exit;
                    $followingCnt = count($loggedUserFollowing);
                    if(count($resultAry) > 0) {
                            foreach($resultAry as $key => $popularUsr) {
                                    $popularUsrType = $popularUsr['user_type'];
                                    if($popularUsr['user_type'] != 'B'){
                                            $popularUsrType = 'U';
                                    }
                                    if($followingCnt > 0) {
                                            foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                    if($popularUsr['business_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                            $businessArray[$key]['isFollowing'] = "2";
                                                    } else {
                                                            if(($popularUsr['business_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                                    $resultAry[$key]['isFollowing'] = "1";
                                                                    break;
                                                            } else {
                                                                    $resultAry[$key]['isFollowing'] = "0";
                                                            }
                                                    }
                                            }
                                    } else {
                                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                $resultAry[$key]['isFollowing'] = "2";
                                        } else {
                                                $resultAry[$key]['isFollowing'] = "0";
                                        }
                                    }
                            }
                    }
                } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => "User id Doesn't Exists");
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($resultAry)) {
                        $resultAry1 = array('service_status'=>'success',"content" => $resultAry, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry1 = array('service_status'=>'error',"error_msg" => "No Business hotspots Found!!!");
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry1);
                return false;
        }

        public function notificationscountAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $logged_user_id = $this->getRequest()->getParam('logged_user_id');
                $logged_user_type = $this->getRequest()->getParam('logged_user_type');
                if($logged_user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_id)){
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Login User Id!!!'));
                        return;
                }
                if($logged_user_type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Type can not be Empty!!!'));
                        return;
                }
                $notificationObj = new Notifications_Model_Notifications();
                $cnt = $notificationObj->notificationcnt($logged_user_id,$logged_user_type,'1');
                if(!empty($cnt)) {
                        $resultAry = array('service_status'=>'success',"content" => $cnt['notificationcnt'],"responsecode" => '700');
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No Records to View","responsecode" => '700');
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getmorescribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $scribArray = array();
                $request = $this->getRequest();
                $user_id = $request->getParam('user_id');
                $scrObj = new Scribbles_Model_Scribbles();
                if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($user_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_Id = $request->getParam('logged_user_id');
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)){
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Login User Id!!!'));
                        return;
                }
                $scribble_id = $request->getParam('scribble_id');
                if($scribble_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Scribble Id can not be Empty!!!'));
                        return;
                }
                $type = $request->getParam('type');
                if($type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Type can not be Empty!!!'));
                        return;
                }
                $latesttype = 'scribbleId';
                $ntype = $request->getParam('ntype');
                if($ntype == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Notification Type can not be Empty!!!'));
                        return;
                }

                // check whether the given user exist or not
                $usrArray = array();
                $userObj = new User_Model_User();
                $usrArray = $userObj->getUserDetails($user_id);
                if(count($usrArray) != 1) {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist');
                        echo $myjson->customEncode($resultAry);
                        return false;
                }

                if($ntype == 'purchasecoupon' || $ntype == 'huddle_target') {
                    // Get Purchase_Coupon/Huddle_Target Created Date by Coupon Id
                    $notificationObj = new Notifications_Model_Notifications();
                    $couponDetails = $notificationObj->getNotoficationDetailsByNotificationID($scribble_id);
                    $latesttype = 'datetime';
                    $scribble_id = $couponDetails['action_date'];
                }
                if($ntype == 'photo') {
                    // Get Photo Details By Photo ID
                    $photosObj = new Mobile_Model_Photos();
                    $photoDetails = $photosObj->getphotobyphotoid($scribble_id);
                    //echo "<pre>";print_r($photoDetails);exit;
                    $latesttype = 'datetime';
                    $scribble_id = $photoDetails['created_date'];
                }
//                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
//                    if($ntype == "photo"){
//                        // Get Photo Blasted Date BY Photo Id
//                        $photoDet = array();
//                        $photosObj = new Mobile_Model_Photos();
//                        $photoDet = $photosObj->getphotobyphotoid($scribble_id);
//                        //echo "<pre>";print_r($photoDet);exit;
//                        $latesttype = 'datetime';
//                        $scribble_id = $photoDet['blasted_date'];
//                    }
//                }

                $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):10);
                $userObj = new User_Model_User();
                $usrLvlObj = new User_Model_Userlevels();


                $parentScribbles =array();
                $allScribbles =array();
                if($logged_user_Id == '' || $user_id == $logged_user_Id) {
                        $owner_id = $user_id;
                        $owner_level = $usrArray->user_level;
                } else {
                        $owner_id = $logged_user_Id;
                        // get logged User Level
                        $usrlevel = $usrLvlObj->getUserLevelByUserId($logged_user_Id, 'U');
                        $owner_level = $usrlevel[0]['user_level'];
                }
                $parentScribbles = $scrObj->getMoreParentScribbles($user_id,$owner_id,$usrArray->user_type, $owner_level, $scribble_id, $type,$latesttype, $limit);
                foreach($parentScribbles as $key =>$parentScrib){
                        $parentId = $parentScrib['scribble_id'];
                        $allScribbles[$key] = $parentScrib;
                        $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $user_id,$user_id,$usrArray->user_type, $usrArray->user_level);
                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                        $allScribbles[$key]['notifications_type'] = "scribble";
                }
                $scribArray = $allScribbles;

                 // IF Blasted Scribble, Get the Original Owner Details
                foreach($scribArray as $key=>$scrib) {
                        if($scrib['blast_id'] != 0) {
                                // To Get Owner Details of the Blasted Scribble
                                $ownerArray = $scrObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                //echo "<pre>";print_r($ownerArray);exit;
                                $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                $scribArray[$key]['gender'] = $ownerArray[0]['gender'];
                                $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                if($scribArray[$key]['owner_user_id'] == $logged_user_Id && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 2;
                                }
                                if($scribArray[$key]['owner_user_id'] != $logged_user_Id && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 1;
                                }
                        }

                        //  check if the Parent scribble is Blasted
                        $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($scrib['scribble_id']);
                        $scribArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                        $blastedUsrAry = array();
                        if($blastedUsrCnt > 0) {
                            //to Get Blasted User Details of the Parent Scribble
                            $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($scrib['scribble_id'], $logged_user_Id);
                        }
                        $scribArray[$key]['BlastedUsers'] = $blastedUsrAry;
                }

                $mixedAry = array();
                $mixedAry = $scribArray;

                $scribbleCnt = count($mixedAry);
                if($scribbleCnt > 0) {
                    if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                        //echo "<pre>";print_r($mixedAry);exit;
                    }
                    $lastScribbleDate = $scribArray[0]['created_date'];
                    $lastScribbleDate1 = $scribArray[$scribbleCnt-1]['created_date'];
                    // get Logged User Following Users-list
                    $followObj = new User_Model_Followers();
                    $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, "U");
                    if(count($loggedUserFollowing) > 0) {
                        $followIdsArr = array();
                        foreach($loggedUserFollowing as $following) {
                            $followIdsArr[] = $following['follower_id'];
                        }
                        $followerIds = implode(",", $followIdsArr);
                        $limit2 = 10;

                        // TO Get Notifications for Purchase Coupons
                        $notificationAry = array();
                        $notificationObj = new Notifications_Model_Notifications();
                        $mixedAry = $scribArray;
                        $cnt = count($scribArray);
                        $notificationAry= $notificationObj->getFollowerCouponNotifications($user_id, $logged_user_Id,$usrArray->user_type, $lastScribbleDate,$lastScribbleDate1, $followerIds, $limit2, $type);
                        foreach($notificationAry as $notification) {
                            $i = $cnt++;
                            $mixedAry[$i] = $notification;
                            $mixedAry[$i]['created_date'] = $notification['action_date'];
                        }
                        if($user_id == $logged_user_Id) {

                            // TO Get all Huddle Targets of the Logged in User
                            $huddleArray = array();
                            $huddleArray = $notificationObj->getHuddleNotifications($logged_user_Id,$usrArray->user_type, $lastScribbleDate, $lastScribbleDate1, $limit2, $type);
                             foreach($huddleArray as $huddle){
                                $i = $cnt++;
                                $mixedAry[$i] = $huddle;
                                $mixedAry[$i]['created_date'] = $huddle['action_date'];
                            }
                        }

                       //if($_SERVER['REMOTE_ADDR'] == '192.168.1.57')
                       //{
                                //echo "<pre>";print_r($mixedAry);exit;
                              // to get Photo Uploaded or Blasted Notifications
                        if($request->getParam('upmeVersion') != '') {
                             $photosObj = new Mobile_Model_Photos();
                             $uType = $usrArray->user_type;
                             if($uType != "U")
                                 $uType = "U";
                             $photoArrDetails = $photosObj->getPhotoUploadBlasts($user_id, $logged_user_Id,$uType, $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2, $type);
                             if(count($photoArrDetails) > 0){
                                 foreach($photoArrDetails as $photo) {
                                         $i = $cnt++;
                                        $mixedAry[$i] = $photo;
                                        $mixedAry[$i]['id'] = $photo['photo_id'];
                                        // to get Blasted Users Count
                                        $photoBlastedUserCnt = $photosObj->getBlastedUserCountBasedOnPhotoId($photo['photo_id']);
                                        $blastedUserDetails = array();
                                        //echo $photoBlastedUserCnt;exit;
                                        if($photoBlastedUserCnt > 0) {
                                            $limit = 2; // get 2 blasted users
                                            $blastedUserDetails = $photosObj->getBlastedUserDetailsBasedOnPhotoId($photo['photo_id'], $logged_user_Id, $limit);
                                            $photoCount = 0;
                                            foreach($blastedUserDetails as $blastUser)
                                            {
                                                $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                                                $blastedUserDetails[$photoCount] = $blastUser;
                                                $photoCount++; 
                                            }
                                            //echo "<pre>";print_r($blastedUserDetails);exit;
                                        }

                                        $photoCommentsArr = $photosObj->getPhotoComment($mixedAry[$i],$photo);

                                        if(count($photoCommentsArr) > 0) {
                                               $j =1;
                                            foreach($photoCommentsArr as $comment) {
                                                $cmntIndex = 'comment'.$j;
                                                $name = 'commenter_name'.$j;
                                                $id = 'commenter_id'.$j;
                                                $useType = 'commenter_type'.$j;
                                                $pic_path = 'profile_pic_path'.$j;
                                                $date = 'commenter_date'.$j;
                                                if($comment['comments'] != '') {
                                                    $mixedAry[$i][$cmntIndex]=$comment['comments'];
                                                    if($comment['usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['user_id'];
                                                        $mixedAry[$i][$useType]='U';
                                                        $mixedAry[$i][$pic_path]=$comment['profile_pic_path'];
                                                        $mixedAry[$i][$name]=$comment['firstname']." ".$comment['lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                    if($comment['B_usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['business_id'];
                                                        $mixedAry[$i][$useType]="B";
                                                        $mixedAry[$i][$pic_path]=$comment['image_path'];
                                                        $mixedAry[$i][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                }
                                                $j++;
                                            } // foreach
                                        } // commentArr Cnt
                                            
                                        $mixedAry[$i]['cmntCnt'] = $photosObj->getPhotoCommentCnt($photo);


                                        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                                        $mixedAry[$i]['created_date'] = $photo['blasted_date'];
                                        $mixedAry[$i]['date'] = $timeObj->humaneDate($photo['blasted_date']);
                                        $mixedAry[$i]['notifications_type'] = "photo";
                                        $mixedAry[$i]['mediatype'] = "photo";
                                        $mixedAry[$i]['photoblastedUserCnt'] = $photoBlastedUserCnt;
                                        $mixedAry[$i]['photoblastedUsers'] = $blastedUserDetails;
                                 }
                             }
                         }


                         if($request->getParam('upmeVersion') != '') {
                            $videosObj = new Mobile_Model_Videos();
                            $uType = $usrArray->user_type;
                            if($uType != "U")
                                $uType = "U";
                            $videoArrDetails = $videosObj->getVideoUploadBlasts($user_id, $logged_user_Id, "U", $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2);
                            //echo '<pre>';print_r($photoArrDetails);exit;
                            if(count($videoArrDetails) > 0) {
                                foreach($videoArrDetails as $video) {
                                        $video_user_type = $video['user_type'];
                                        if($video['user_type'] != 'U')
                                                $video_user_type = "U";
                                        if($logged_user_Id == $video['user_id'] && $video_user_type == "U") {
                                                $video['isBlasted'] = "2";
                                        }
                                        
                                        $i = $cnt++;
                                        $mixedAry[$i] = $video;
                                        $mixedAry[$i]['id'] = $video['video_id'];
                                        // to get Blasted Users Count
                                        $videoBlastedUserCnt = $videosObj->getBlastedUserCountBasedOnVideoId($video['video_id']);
                                        $blastedUserDetails = array();
                                        //echo $videoBlastedUserCnt;exit;
                                        if($videoBlastedUserCnt > 0) {
                                            $limit = 2; // get 2 blasted users
                                            $blastedUserDetails = $videosObj->getBlastedUserDetailsBasedOnVideoId($video['video_id'], $logged_user_Id, $limit);
                                            $videoCount = 0;
                                            foreach($blastedUserDetails as $blastUser)
                                            {
                                                $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                                                $blastedUserDetails[$videoCount] = $blastUser;
                                                $videoCount++; 
                                            }
                                            //echo "<pre>";print_r($blastedUserDetails);exit;
                                        }

                                         $videoCommentsArr = $videosObj->getVideoComment($mixedAry[$i],$video);

                                            if(count($videoCommentsArr) > 0) {
                                               $j =1;
                                            foreach($videoCommentsArr as $comment) {
                                                $cmntIndex = 'comment'.$j;
                                                $name = 'commenter_name'.$j;
                                                $id = 'commenter_id'.$j;
                                                $useType = 'commenter_type'.$j;
                                                $pic_path = 'profile_pic_path'.$j;
                                                $date = 'commenter_date'.$j;
                                                if($comment['comments'] != '') {
                                                    $mixedAry[$i][$cmntIndex]=$comment['comments'];
                                                    if($comment['usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['user_id'];
                                                        $mixedAry[$i][$useType]='U';
                                                        $mixedAry[$i][$pic_path]=$comment['profile_pic_path'];
                                                        $mixedAry[$i][$name]=$comment['firstname']." ".$comment['lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                    if($comment['B_usertype'] != '') {
                                                        $mixedAry[$i][$id]=$comment['business_id'];
                                                        $mixedAry[$i][$useType]="B";
                                                        $mixedAry[$i][$pic_path]=$comment['image_path'];
                                                        $mixedAry[$i][$name]=$comment['business_firstname']." ".$comment['business_lastname'];
                                                        $mixedAry[$i][$date]=$comment['commented_date'];
                                                    }
                                                }
                                                $j++;
                                            } // foreach
                                        } // commentArr Cnt

                                        $mixedAry[$i]['cmntCnt'] = $videosObj->getvideoCommentCnt($video);

                                        $mixedAry[$i]['created_date'] = $video['blasted_date'];
                                        $mixedAry[$i]['date'] = $timeObj->humaneDate($video['blasted_date']);
                                        $mixedAry[$i]['notifications_type'] = "video";
                                        $mixedAry[$i]['mediatype'] = "video";
                                        $mixedAry[$i]['videoblastedUserCnt'] = $videoBlastedUserCnt;
                                        $mixedAry[$i]['videoblastedUsers'] = $blastedUserDetails;
                                }
                            }
                        }


                        $orderByDate = array();
                        foreach ($mixedAry as $key => $row) {
                            $orderByDate[$key]  = strtotime($row['created_date']);
                        }
                        array_multisort($orderByDate, SORT_DESC, $mixedAry);
                    }
                }
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                    //echo "<pre>";print_r($mixedAry);exit;
                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $scribObj = new UpmeSocial_View_Helper_Scribbleusername();

                foreach($mixedAry as $key => $scrib) {
                    $mixedAry[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                    $mixedAry[$key]['date'] =  $mixedAry[$key]['created_date'];
                    //display realname+usernmae+userlevel. Limit 30chars.
                    if($scrib['notifications_type'] != 'scribble' && $scrib['notifications_type'] != 'photo' && $scrib['notifications_type'] != 'video') continue;
                    $mixedAry[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                //Send Response to mobile success r not
                if (!empty($mixedAry)) {
                        $resultAry = array('service_status'=>'success',"content" => $mixedAry,"notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'success',"content" => array());
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getmoredownlinescribblesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $logged_user_Id = $request->user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_level = $request->user_level;
                if($logged_user_level == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Level can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_level)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $scribble_id = $request->getParam('scribble_id');
                if($scribble_id == '') {
                       $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Scribble Id can not be Empty!!!'));
                       return;
                }
                $type = $request->getParam('type');
                if($type == '') {
                       $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Type can not be Empty!!!'));
                       return;
                }
                $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):30);
                // FOR Fetchng Logged User's More  DOWNLINE Scribbbles
                $scrObj = new Scribbles_Model_Scribbles();
                $downlineArray = array();
                $downlineArray = $scrObj->getMoreDownlineScribbles($logged_user_Id,$logged_user_level, $scribble_id, $type, $limit);
                foreach($downlineArray as $key =>$parentScrib) {
                        $parentId = $parentScrib['scribble_id'];
                        $downlineArray[$key] = $parentScrib;
                        $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $logged_user_Id,$logged_user_Id,"U", $logged_user_level,"downline");
                        $downlineArray[$key]['childScribbles'] = $childScribblesCnt;

                         //  check if the Parent scribble is Blasted
                        $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                        $downlineArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                        $blastedUsrAry = array();
                        if($blastedUsrCnt > 0) {
                            //to Get Blasted User Details of the Parent Scribble
                            $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $logged_user_Id);
                        }
                        $downlineArray[$key]['BlastedUsers'] = $blastedUsrAry;
                }
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($downlineArray) > 0) {

                    $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                    foreach($downlineArray as $key=>$scrib) {
                        $downlineArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                        $downlineArray[$key]['notifications_type'] = "scribble";
                        $scrib['notifications_type'] = "scribble";
                       // if($scrib['notifications_type'] != 'scribble') continue;
                        $downlineArray[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                    }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                if(!empty($downlineArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $downlineArray, "notificationcnt" => $notificationcnt['notificationcnt']);
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Scribbles Found');
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getalluserphotosAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $logged_user_Id = $request->user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!',"userinfocode"=>"1000"));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!',"userinfocode"=>"1000"));
                        return;
                }
                $logged_user_type = "U";
                $photosObj = new Mobile_Model_Photos();
                $photos = $photosObj->getphotosbyuserid($logged_user_Id,$logged_user_type);
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                if(count($photos) > 0) {
                        foreach($photos as $key=>$scrib) {
                                $photos[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($photos)) {
                        $resultAry = array('service_status'=>'success',"content" => $photos, "notificationcnt" => $notificationcnt['notificationcnt'], 'userinfocode'=>"1000");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Photos Found',"userinfocode"=>"1000");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function makeprofilepictureAction(){
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $logged_user_Id = $request->user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $photo_Id = $request->photo_id;
                if($photo_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Photo Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($photo_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Photo Id!!!'));
                        return;
                }
                //$logged_user_type = "U";
                $photosObj = new Mobile_Model_Photos();
                $updateAction = $photosObj->makeProfilePicture($logged_user_Id, $photo_Id);
                if($updateAction == 1) {
                        $resultAry = array('service_status'=>'success',"content" => "Updated Successfully");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'Error Updating Profile Picture!!!');
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getalltimelineblastedusersAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();

                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }

                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!',"usermediacode"=>"450"));
                        return;
                }

                $Id = $request->id;
                if($Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Id can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }

                if(!is_numeric($Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Id!!!',"usermediacode"=>"450"));
                        return;

                }

                $type = $request->type;
                if($type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Type can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }

                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '' || $logged_user_type != 'B'){
                        $logged_user_type = 'U';
                }
                $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):30);
                $photosObj = new Mobile_Model_Photos();
                $followObj = new User_Model_Followers();
                $blastedUsers = $photosObj->getAllTimelineBlastedUsers($logged_user_Id,$logged_user_type, $Id, $type, $limit);
                //echo "<pre>";print_r($blastedUsers);exit;
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                //echo "<pre>";print_r($loggedUserFollowing);exit;
                $followingCnt = count($loggedUserFollowing);
                if(count($blastedUsers) > 0) {
                        foreach($blastedUsers as $key => $followingUsr) {
                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                            if($followingUsr['user_type'] != 'B')
                                                $followUser_type = "U";
                                            else
                                                $followUser_type = "U";
                                            if($followingUsr['user_id'] == $logged_user_Id && strtolower($followUser_type) == strtolower($logged_user_type)) {
                                                        $blastedUsers[$key]['isFollowing'] = "2";
                                            } else {
                                                    if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && (strtolower($followUser_type) == strtolower($Usrfollows['follower_type']))) {
                                                            $blastedUsers[$key]['isFollowing'] = "1";
                                                            break;
                                                    } else {
                                                            $blastedUsers[$key]['isFollowing'] = "0";
                                                    }
                                            }
                                        }
                                } else {
                                    $blastedUsers[$key]['isFollowing'] = "0";
                                }
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if (!empty($blastedUsers)) {
                        $resultAry = array('service_status'=>'success',"content" => $blastedUsers, "notificationcnt" => $notificationcnt['notificationcnt'], "usermediacode"=>"450");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Users to View',"usermediacode"=>"450");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function businessgalleryforuserAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $business_id = $request->getParam('business_id');
                $gallery_type = $request->getParam('gallery_type');
                $businessObj = new Business_Model_Business();
                $mobileobj = new Mobile_Model_Albums();
                $videoObj = new Videos_Model_Videos();
                if($business_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Business Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($business_id)){
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Business Id!!!'));
                        return;
                }
                if($gallery_type == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Gallery Type can not be Empty!!!'));
                        return;
                }
                $businessexists = $businessObj->getBusinessUserexistOrNot($business_id);
                //echo '<pre>';print_r($businessexists);exit;
                if(!empty($businessexists)) {
                        if($gallery_type == 'image') {
                                $gallerycontent = $mobileobj->getBusinessAlbumsByUserId($business_id, 'B');
                                $errormsg = "No Photos To View";
                        }
                        if($gallery_type == 'video') {
                                $gallerycontent = $videoObj->getBusinessVideosByUserId($business_id, 'B');
                                $errormsg = "No Videos To View";
                        }
                        if(!empty($gallerycontent)) {
                                $resultAry = array('service_status'=>'success',"content" => $gallerycontent);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => $errormsg);
                        }
                } else {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Business Id!!!'));
                        return;
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getallscribbleblastedusersAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();

                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }

                if(!is_numeric($logged_user_Id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!',"usermediacode"=>"450"));
                        return;
                }

                $scribbleId = $request->scribble_id;
                if($scribbleId == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Scribble Id can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }

                if(!is_numeric($scribbleId)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Scribble Id!!!',"usermediacode"=>"450"));
                        return;
                }

                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '' || $logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }
                $scribbleObj = new Scribbles_Model_Scribbles();
                $followObj = new User_Model_Followers();
                $blastedUsers = $scribbleObj->getAllScribbleBlastedUsers($scribbleId);
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                if(count($blastedUsers) > 0) {
                        foreach($blastedUsers as $key => $followingUsr) {

                                if($followingCnt > 0) {
                                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                                if($followingUsr['user_type'] != 'B')
                                                        $followUser_type = "U";
                                                else
                                                        $followUser_type = "U";
                                                if($followingUsr['user_id'] == $logged_user_Id && strtolower($followUser_type) == strtolower($logged_user_type)) {
                                                        $blastedUsers[$key]['isFollowing'] = "2";
                                                } else {
                                                        if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && (strtolower($followUser_type) == strtolower($Usrfollows['follower_type']))) {
                                                                $blastedUsers[$key]['isFollowing'] = "1";
                                                                break;
                                                        } else {
                                                                $blastedUsers[$key]['isFollowing'] = "0";
                                                        }
                                                }
                                        }
                                } else {
                                        $blastedUsers[$key]['isFollowing'] = "0";
                                }
                        }
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if (!empty($blastedUsers)) {
                        $resultAry = array('service_status'=>'success',"content" => $blastedUsers,"notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"450");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Users to View',"usermediacode"=>"450");
                }
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function getbusinessesAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $logged_user_Id = $request->logged_user_id;
                $keyword = $request->keyword;
                if($keyword == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                        return;
                }
                $businesscatObj = new Business_Model_Businesscategories();
                $followObj = new User_Model_Followers();
                $businesskeys = explode(',', $keyword);
                $cnt = COUNT($businesskeys);
                $business_id = array();
                //echo 'val<pre>';print_r($businesskeys);exit;
                foreach($businesskeys as $i => $businesskey) {
                        if(empty($businesskey)) {
                                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'No Keywords To View'));
                                return false;
                        }
                        $match = array();
                        preg_match_all('/\((.*?)\)/', $businesskey, $match);
                        if(!empty($match[1][0])) {
                                if(!empty($match[1][0])) {
                                        $fetchdata = $businesscatObj->getbusinessid($match[1][0]);
                                        if(!empty($fetchdata)) {
                                                $business_id[] = $fetchdata;
                                        } else {
                                                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'No Business Exists'));
                                                return false;
                                        }
                                } else {
                                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'No Business Exists'));
                                        return false;
                                }
                        } else {
                                foreach($business_id as $bid):
                                        $bids[] = $bid['business_id'];
                                endforeach;
                                if(!empty($bids))
                                    $ids = implode(',', $bids);
                                else
                                    $ids = '';

                                $keyword = $businesskeys[$cnt-1];
                                $businesskeyword = array();
                                $keyworddata = $businesscatObj->getbusinessbykeyword($keyword,$ids);
                                //echo 'val<pre>';print_r($keyworddata);exit;
                                if(!empty($keyworddata)) {
                                        $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, 'U');
                                        $followingCnt = count($loggedUserFollowing);
                                        //echo '<pre>';print_r($loggedUserFollowing);exit;
                                        foreach($keyworddata as $key =>$keydata):
                                                $businesskeyword[$key]['business_id'] = $keydata['business_id'];
                                                $businesskeyword[$key]['business_name'] = $keydata['business_name'];
                                                $businesskeyword[$key]['keyword'] = $keydata['keyword'].'('.$keydata['business_name'].')';
                                                if($followingCnt > 0) {
                                                        foreach($loggedUserFollowing as $Usrfollows):
                                                                if($keydata['business_id'] == $logged_user_Id && $keydata['user_type'] == 'U') {
                                                                        $businesskeyword[$key]['is_following'] = "2";
                                                                } else {
                                                                        if(($keydata['business_id'] == $Usrfollows['follower_id']) && ($keydata['user_type'] == $Usrfollows['follower_type'])) {
                                                                                $businesskeyword[$key]['is_following'] = "1";
                                                                                break;
                                                                        } else {
                                                                                $businesskeyword[$key]['is_following'] = "0";
                                                                        }
                                                                }
                                                        endforeach;
                                                } else {
                                                        if($keydata['business_id'] == $logged_user_Id && $keydata['user_type'] == 'U') {
                                                                $businesskeyword[$key]['is_following'] = "2";
                                                        } else {
                                                                $businesskeyword[$key]['is_following'] = "0";
                                                        }
                                                }
                                        endforeach;
                                        $notificationObj = new Notifications_Model_Notifications();
                                        $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                                        if (!empty($businesskeyword)) {
                                                $resultAry = array('service_status'=>'success',"content" => $businesskeyword, "notificationcnt" => $notificationcnt['notificationcnt']);
                                        } else {
                                                $resultAry = array('service_status'=>'error',"error_msg" => 'No Businesses To View');
                                        }
                                } else {
                                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Businesses To View');
                                }
                                echo $myjson->customEncode($resultAry);
                                return false;
                        }
                }
                /*} else {
                        $businesskeyword = array();
                        $keyworddata = $businesscatObj->getbusinessbykeyword($keyword);
                        $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, 'U');
                        $follower_ids = array();
                        foreach($loggedUserFollowing as $key => $val) {
                                $follower_ids[] = $val['follower_id'];
                        }
                        foreach($keyworddata as $key =>$keydata):
                                $businesskeyword[$key]['business_id'] = $keydata['business_id'];
                                $businesskeyword[$key]['keyword'] = $keydata['keyword'].'('.$keydata['business_name'].')';
                                if(in_array($keydata['business_id'],$follower_ids)) {
                                    $businesskeyword[$key]['is_following'] = '1';
                                } else {
                                    $businesskeyword[$key]['is_following'] = '0';
                                }
                        endforeach;
                        if (!empty($businesskeyword)) {
                                $resultAry = array('service_status'=>'success',"content" => $businesskeyword);
                        } else {
                                $resultAry = array('service_status'=>'error',"error_msg" => 'No Businesses To View');
                        }
                        echo $myjson->customEncode($resultAry);
                        return false;
                }*/
        }

        public function getunreadnotificationcntAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $userObj = new User_Model_User();
                $user_id = $request->getParam('user_id');
                if($user_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'User Id should not be empty',"responsecode" => '700');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return;
                }
                if(!is_numeric($user_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!',"responsecode" => '700'));
                        return;
                }
                if($userObj->isUserAvail($user_id) != '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => "User Doesn't Exists!!!","responsecode" => '700'));
                        return;
                }
                $user_type = $request->getParam('user_type');
                if($user_type == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'User Type should not be empty',"responsecode" => '700');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return;
                }
                $notificationObj = new Notifications_Model_Notifications();
                if(!empty($user_id) && !empty($user_type)) {
                    $notificationcnt = $notificationObj->notificationcnt($user_id, $user_type, '1');
                    $resultAry = array('service_status'=>'success',"content" => $notificationcnt['notificationcnt'],"responsecode" => '700');
		}
                 $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function checkemailsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $request = $this->getRequest();
                $logged_user_id = $request->getParam('logged_user_id');
                if($logged_user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User Id!!!'));
                        return;
                }

                $logged_user_type = $request->getParam('logged_user_type');
                if(!ctype_alpha($logged_user_type)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User Type!!!'));
                        return;
                }

                if($logged_user_type == '')
                        $logged_user_type = "U";
                $email = $request->getParam('email');
                //$email = "varaprasad@rizecorp.net,kiran@rizecorp.net,narayana@gmail.co.in,laxman@gmail.com,satya123@gmail.com,krishna@gmail.com,harsha@gmail.com,lakshmi@gmail.com,durga123@gmail.com,satyan@gmail.com,hari@gmail.com";
                $emailAry = array();
                $usrAry = array();
                $emailAry = explode(',', $email);
                $emails = '';
                $cnt = count($emailAry);
                $i = 1;
                foreach($emailAry as $mail) {
                        if($i < ($cnt-1)) {
                                $emails .= "'".$mail."',";
                        } else
                                $emails .= "'".$mail."'";
                        $i++;
                }
                $userDetailsArray = $userObj->getUserDetailsByEmails($emails);
                //echo "<pre>";print_r($userDetailsArray);exit;
                $loggedUserFollowing = $followObj->getFollowingUsersByLoggedUser($logged_user_id, 0, 0, $logged_user_type);
                //echo "<pre>";print_r($loggedUserFollowing);exit;

                $followingCnt = count($loggedUserFollowing);
                if(count($userDetailsArray) > 0) {
                        foreach($userDetailsArray as $key => $followingUsr) {
                                if($followingUsr['user_type'] != 'U')
                                        $followingUsr['user_type'] = "U";
                                if($followingCnt > 0) {
                                    foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                            if($followingUsr['user_id'] == $logged_user_id && $followingUsr['user_type'] == $logged_user_type) {
                                                    $userDetailsArray[$key]['isFollowing'] = "2";
                                            } else {
                                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                                        $userDetailsArray[$key]['isFollowing'] = "1";
                                                        break;
                                                } else {
                                                        $userDetailsArray[$key]['isFollowing'] = "0";
                                                }
                                            }
                                    }
                                } else {
                                        $userDetailsArray[$key]['isFollowing'] = "0";
                                }
                        }
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($logged_user_id, $logged_user_type, '1');
                        //echo "<pre>";print_r($userDetailsArray);exit;
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content" => $userDetailsArray, "notificationcnt" => $notificationcnt['notificationcnt']));
                        return;
                } else {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'No Users Available with the Emails Specified'));
                        return;
                }
        }

        public function inviteusersbyemailAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $userObj = new User_Model_User();
                //$followObj = new User_Model_Followers();
                $request = $this->getRequest();
                $logged_user_id = $request->getParam('logged_user_id');
                if($logged_user_id == '') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_id)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User Id!!!'));
                        return;
                }

                $logged_user_type = $request->getParam('logged_user_type');
                if(!ctype_alpha($logged_user_type)) {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User Type!!!'));
                        return;
                }

                if($logged_user_type == '')
                        $logged_user_type = "U";

                $userexists = $userObj->isUserAvail($logged_user_id);
                $loggeduserDetails = $userObj->getUserDetails($logged_user_id);
                //echo "<pre>";print_r($loggeduserDetails['firstname']);exit;
                if($userexists == '') {
                        $email = $request->getParam('email');
                        $emailAry = array();
                        //$usrAry = array();
                        $emailAry = explode(',', $email);
                        $emailAry = array_filter($emailAry);
                        $emails = '';
                        $cnt = count($emailAry);
                        $i = 1;
                        //echo '<pre>';print_r($emailAry);exit;
                        foreach($emailAry as $mail) {
                                if($mail != '') {
                                        // Validate Email
                                        if (preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $mail)) {
                                                if($i < ($cnt)) {
                                                        $emails .= "'".$mail."',";
                                                } else {
                                                        $emails .= "'".$mail."'";
                                                }
                                        }
                                }
                                $i++;
                        }
                        $userEmailsArray = $userObj->getUserEmailDetiailsByEmails($emails);
                        $newArr = array();
                        $i = 1;
                        foreach($userEmailsArray as $new){
                                $newArr[$i] = $new['email'];
                                $i++;
                        }
                        //$tempMailsArray = array();
                        $newUserMailArr = array();
                        foreach($emailAry as $mail1){
                                $key = array_search($mail1, $newArr);
                                //echo "<br>".$mail1."---".$key;
                                if($key=='' && $mail1 != '' && $mail1 != ',' && preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $mail1))
                                        $newUserMailArr[] = $mail1;
                        }
                        //echo "<pre>";print_r($newUserMailArr);exit;
                        // Insert Inviting User Emails in DB
                        $insEmails = $userObj->insertInvitingUserEmails($newUserMailArr, $logged_user_id);
                        //echo "<pre>";print_r($insEmails);exit;
                        foreach($insEmails as $newUserMail){
                                // TO Send Invite Mail as User with the Email Does Not Exist in UpmeSocial
                                if($_SERVER['SERVER_NAME'] != '192.168.1.57' && $newUserMail != '' && $newUserMail != ','){
                                    $m = new UpmeSocial_HtmlMailer();
                                    $m->setSubject("Invitation to join me on upmesocial");
                                    $m->addTo($newUserMail)
                                        ->setViewParam('user_id',$logged_user_id)
                                        ->setViewParam('name',$loggeduserDetails['firstname']." ".$loggeduserDetails['lastname'])
                                        ->setViewParam('email',$newUserMail);
                                    $m->sendHtmlTemplate("inviteuserbyemail.phtml");
                                }
                        }
                        $notificationObj = new Notifications_Model_Notifications();
                        $notificationcnt = $notificationObj->notificationcnt($logged_user_id, $logged_user_type, '1');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'success',"content" => 'You have invited friends successfully', 'MailsList'=>$newUserMailArr, "notificationcnt" => $notificationcnt['notificationcnt']));
                        return;
                } else {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'No User available with the Id specified'));
                        return;
                }
        }

        public function suggestedfriendsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                // New Modifications
                $logged_user_Id = $request->logged_user_id;
                if($logged_user_Id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($logged_user_Id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                        return;
                }
                $logged_user_type = $request->logged_user_type;
                if($logged_user_type == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                        return;
                }
                if($logged_user_type != 'B') {
                        $logged_user_type = 'U';
                }
                $search_key = $request->keyword;

                $pagenum = $request->pagenum;
                if($pagenum == '' || ($pagenum <= 0))
                        $pagenum = 0;

                $limit = $request->limit;
                if($limit == '')
                        $limit = 30;

                // CHeck if User Exist for the user id Specified
                $userObj = new User_Model_User();
                $userexists = $userObj->isUserAvail($logged_user_Id);
                if($userexists != '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "User Id doesn't Exists!!!"));
                        return false;
                }
                //get User Zipcode Based On Usr Id and Type
                $searchObj = new Search_Model_Search();
                $userDetailsArray = $searchObj->getUserZipCodeByID($logged_user_Id);
                //echo "<pre>";print_r($userDetailsArray);exit;

                // GEt Local  People based on Zipcode of Logged User
                $localPeopleArray =  $searchObj->getSuggestedFriendsByZipcode($logged_user_Id, $logged_user_type, $userDetailsArray[0]['zipcode'], $limit,  $pagenum, $search_key);
                //echo "<pre>";print_r($localPeopleArray);exit;

                // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                foreach($localPeopleArray as $key => $popularUsr) {
                        $localPeopleArray[$key]['isFollowing'] = "0";
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
                if(!empty($localPeopleArray)) {
                        $resultAry = array('service_status'=>'success',"content" => $localPeopleArray, "notificationcnt"=>$notificationcnt['notificationcnt']);
                } else {
                    if($pagenum == 0) {
                        $resultAry = array('service_status'=>'error',"error_msg" => "Currently you dont have any suggested friends!!!");
                    } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => "No more suggested friends!!!");
                    }
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }

        public function scancontactspreliminarycheckAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                $userObj = new User_Model_User();
                $i = 1;
                $emails = '';
                $emailAry = $request->getParam('user');
                if(empty($emailAry)){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error','error_message'=>'Emails can not be Empty',"useridnfo"=>"300"));
                    return;
                }
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
                    //echo "<pre>";print_r($emailAry);exit;
                }
                $cnt = count($emailAry);
                foreach($emailAry as $contact){
                    $contact['username'] = str_replace('(null)','',$contact['username']);
                    $contact['email'] = str_replace('(null)','',$contact['email']);

                    // Validate Email
                    if (preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $contact['email'])) {
                        if($i < ($cnt)) {
                            $emails .= "'".$contact['email']."',";
                        } else
                                $emails .= "'".$contact['email']."'";
                    }

                    $i++;
                }

                //echo $emails;
                if($emails == ''){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error','error_message'=>'No valid emails addresses in list',"userinfo"=>"300"));
                    return;
                }
                $userEmailsArray = $userObj->getUserEmailDetiailsByEmails($emails);
                $newArr = array();
                $j = 1;
                foreach($userEmailsArray as $new){
                    $newArr[$j] = $new['email'];
                    $j++;
                }
                $newContactArray = array();
                $i=0;
                foreach($emailAry as $email){
                    $email['username'] = str_replace('(null)','',$email['username']);
                    $email['email'] = str_replace('(null)','',$email['email']);
                    $key = array_search($email['email'], $newArr);
                    if($key==''){
                            $newContactArray[$i]['username'] = $email['username'];
                            $newContactArray[$i]['email'] = $email['email'];
                            $i++;
                    }
                }
                if(empty($newContactArray)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error','error_message'=>'All the users with the Email-Ids Specified already Registered',"userinfo"=>"300"));
                    return;
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success','MailsList'=>$newContactArray,"userinfo"=>"300"));
                return;
        }

        public function forgotpasswordAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $email = $this->getRequest()->getParam('email');
                $mkey = $this->getRequest()->getParam('mkey');
                $userObj = new User_Model_User();
                if(!preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $email)) {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"417","content" => 'Invalid Email Format');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }
                $available = $userObj->isEmailAvail($email);
                if(!empty($available)) {
                        $resultAry = array('service_status'=>'error',"signupcode"=>"417","content" => 'Email not available');
                         $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                        return false;
                }
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $data['password'] = substr(str_shuffle($chars),0,6);
                $uid = $userObj->updatePasswordByEmail($data, $email);
                $userdata = $userObj->getuserdatabyemail($email);
//                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
//                    echo $data['password'];
//                }
                if($_SERVER['SERVER_NAME'] != '192.168.1.57') {
                        $m = new UpmeSocial_HtmlMailer();
                        $m->setSubject("UPMEsocial - Updateded Login Credentials");
                        $m->addTo($email)
                            ->setViewParam('user_id',$userdata['user_id'])
                            ->setViewParam('firstname',$userdata['firstname'])
                            ->setViewParam('lastname',$userdata['lastname'])
                            ->setViewParam('username',$userdata['username'])
                            ->setViewParam('password',$data['password'])
                            ->setViewParam('email',$email);
                        $m->sendHtmlTemplate("forgotpassworddetails.phtml");
                }
                if(!empty($mkey)) {
                    if($uid) {
                            $resultAry = array('service_status'=>'success',"content" => "Your password has been send to Registered Email");
                    } else {
                            $resultAry = array('service_status'=>'error',"error_msg" => '');
                    }
                     $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();                echo $myjson->customEncode($resultAry);
                }

        }

        public function isuserfollowingbusinessAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->getParam('logged_user_id');
            if($logged_user_Id == '') {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                return;
            }
            if(!is_numeric($logged_user_Id)) {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Logged User Id!!!'));
                return;
            }
            $logged_user_type = $request->logged_user_type;
            if($logged_user_type == '') {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Type can not be Empty!!!'));
                return;
            }

            if($logged_user_type =="B" || !ctype_alpha($logged_user_type)) {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User Type!!!'));
                return;
            }


            if($logged_user_type !="U"){
                $logged_user_type = "U";
            }

            $business_Id = $request->getParam('business_id');
            if($business_Id == '') {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Business Id can not be Empty!!!'));
                return;
            }
            if(!is_numeric($business_Id)) {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Business Id!!!'));
                return;
            }

            // check if User Exist or Not
            $userObj = new User_Model_User();
            $isUserExist = $userObj->isUserAvail($logged_user_Id);
            //echo "<pre>";print_r($isUserExist);exit;
            if($isUserExist != '') {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => "Logged User Id doesn't Exists!!!"));
                return false;
            }

            // check if Business Exist or Not
            $businessuserObj = new Business_Model_Business();
            $businessexists = $businessuserObj->getBusinessUserexistOrNot($business_Id);
            if($businessexists == '') {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => "Business Id doesn't Exist!!!"));
                return;
            }

            $followObj = new User_Model_Followers();
            $isFollowing = $followObj->isLoggedUserFollowingOtherUser($business_Id, "B", $logged_user_Id, $logged_user_type);

            if(count($isFollowing) ==0){
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content" => "NO"));
                return;
            }
            if(count($isFollowing) > 0){
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content" => "YES"));
                return;
            }
        }

        public function searchuppersAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $keyword = $request->getParam('keyword');
            /*if($keyword == '') {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Keyword can not be Empty!!!'));
                return;
            }*/
            $pagenum = 0;
            if($request->pagenum != '')
                $pagenum = $request->pagenum;

            $limit = 10;
            if($request->limit != '')
                $limit = $request->limit;

            $couponsObj = new Business_Model_Coupons();
            $couponsArr = $couponsObj->getUppersByKeyword($keyword, $pagenum, $limit);
            //echo "<pre>";print_r($couponsArr);exit;
            foreach($couponsArr as $key => $coupon) {
                    $couponsArr[$key]['active'] = '1';
                    $couponsArr[$key]['expiration_date'] = date('d, M Y', strtotime($coupon['expiration_date']));
            }
            if (!empty($couponsArr)) {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                echo $myjson->customEncode(array('service_status'=>'success',"content" => $couponsArr, "searchcode"=>"433"));
                return;
            } else {
                if($pagenum != 0 && empty($couponsArr)){
                   $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                   echo $myjson->customEncode(array('service_status'=>'error',"error_message" =>"", "searchcode"=>"433"));
                   return;
                } else {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"error_message" =>"No Uppers Found", "searchcode"=>"433"));
                    return;
                }
            }
        }

        public function getbusinessbycategoryidAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->logged_user_id;
            if($logged_user_Id == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                    return;
            }
            if(!is_numeric($logged_user_Id)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                    return;
            }
            $logged_user_type = $request->logged_user_type;
            if($logged_user_type == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                    return;
            }
            if($logged_user_type != 'B') {
                    $logged_user_type = 'U';
            }

            $pagenum = $request->pagenum;
            if($pagenum != '' && !is_numeric($pagenum)){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Page Number Format!!!'));
                    return;
            }
            if($pagenum == '')
                $pagenum = '0';

            $limit = $request->limit;
            if($limit != '' && !is_numeric($limit)){
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Limit Format!!!'));
                    return;
            }
            if($limit == '')
                $limit = 30;
            $businessObj = new Business_Model_Businesscategories();
            $business = $businessObj->getBusinessByCategoryId($pagenum, $limit);
            //echo '<pre>';print_r($businessdata);exit;
            $followObj = new User_Model_Followers();
            $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
            $followingCnt = count($loggedUserFollowing);
            if(count($business) > 0) {
                    foreach($business as $key => $popularUsr) {
                            $popularUsrType = $popularUsr['user_type'];
                            if($followingCnt > 0) {
                                    foreach($loggedUserFollowing as $Usrfollows) {
                                            if($popularUsr['business_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                                                    $business[$key]['isFollowing'] = "2";
                                            } else {
                                                    if(($popularUsr['business_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                                            $business[$key]['isFollowing'] = "1";
                                                            break;
                                                    } else {
                                                            $business[$key]['isFollowing'] = "0";
                                                    }
                                            }
                                    }
                            } else {
                                    $business[$key]['isFollowing'] = "0";
                            }
                    }
            }
            $notificationObj = new Notifications_Model_Notifications();
            $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, $logged_user_type, '1');
            if(!empty($business)) {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content" => $business, "notificationcnt" => $notificationcnt['notificationcnt']));
                return;
            } else {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_message" =>"No Businesses Found"));
                return;
            }
        }

        public function completesearchAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
            $request = $this->getRequest();
            $searchObj  = new Search_Model_Search();
            if($request->getParam('keyword') != '') { $val = $request->getParam('keyword'); } else { $val = ''; }
            $category  = $request->getParam('category');
            if($category == '') {
                $category = "People";
            }
            $logged_user_Id = $request->getParam('logged_user_id');
            if($logged_user_Id == '') {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                return;
            }
            $logged_user_type = $request->getParam('logged_user_type');
            if($logged_user_type == '') {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                return;
            }
            $pagenum = $request->pagenum;
            if($pagenum == '')
                $pagenum = 0;
            $limit = $request->limit;
            if($limit == '')
                $limit = 10;
            $searchArray = array();
            $searchDetails = array();
            $cityObj = new User_Model_Cities();
            $cityid = '';
            $cityname = '';
            /************ FOR LEFT SIDE BAR SEARCH *************/
            // SEARCH IN TYPE PEOPLE
            if($request->getParam('category') == "People" && $val != '') {
                $searchDetails = $searchObj->getPeopleDetails($val, $logged_user_Id, $logged_user_type);
                if(count($searchDetails) > 0) {
                    foreach($searchDetails as $key =>$searchU) {
                        $searchArray[$key]['user_id'] = $searchU['user_id'];
                        $searchArray[$key]['username'] = $searchU['username'];
                        $searchArray[$key]['uname'] = $searchU['uname'];
                        $searchArray[$key]['firstname'] = $searchU['firstname'];
                        $searchArray[$key]['image_path'] = $searchU['profile_pic_path'];
                        $searchArray[$key]['user_type'] = 'U';
                    }
                }
                $searchDetails = $searchArray;
            }

            // SEARCH IN TYPE BUSINESS
            if($request->getParam('category') == "Business"  && $val != '') {
                $searchDetails = $searchObj->getBusinessDetails($val, $logged_user_Id, $logged_user_type);
                if(count($searchDetails) > 0) {
                    foreach($searchDetails as $key =>$searchU) {
                        $searchArray[$key]['user_id'] = $searchU['business_id'];
                        $searchArray[$key]['username'] = $searchU['username'];
                        $searchArray[$key]['uname'] = $searchU['business_name'];
                        $searchArray[$key]['firstname'] = $searchU['firstname'];
                        $searchArray[$key]['image_path'] = $searchU['image_path'];
                        $searchArray[$key]['user_type'] = 'B';
                    }
                }
                $searchDetails = $searchArray;
            }

            // SEARCH IN TYPE COLLEGE/UNIVERSITY
            if($request->getParam('category') == "College") {
                $searchDetails = $searchObj->getCollegeDetails($val);
                if(count($searchDetails) > 0) {
                    foreach($searchDetails as $key =>$searchU){
                        $searchArray[$key]['id'] = $searchU['college_id'];
                        $searchArray[$key]['name'] = $searchU['college_name'];
                        $searchArray[$key]['city'] = $searchU['college_city'];
                        $searchArray[$key]['followerscnt'] = $searchU['followerscnt'];
                        $searchArray[$key]['type'] = "C";
                    }
                }
                $searchDetails = $searchArray;
            }

            // SEARCH IN TYPE SCHOOLS
            if($request->getParam('category') == "School") {
                $searchDetails = $searchObj->getSchoolDetails($val);
                if(count($searchDetails) > 0) {
                    foreach($searchDetails as $key =>$searchU) {
                        $searchArray[$key]['id'] = $searchU['school_id'];
                        $searchArray[$key]['name'] = $searchU['school_name'];
                        $searchArray[$key]['city'] = $searchU['school_city'];
                        $searchArray[$key]['followerscnt'] = $searchU['followerscnt'];
                        $searchArray[$key]['type'] = "S";
                    }
                }
                $searchDetails = $searchArray;
            }

            // SEARCH IN TYPE WORK PLACE
            if($request->getParam('category') == "Workplace") {
                $searchDetails = $searchObj->getWorkPlaceDetails($val);
                if(count($searchDetails) > 0) {
                    foreach($searchDetails as $key =>$searchU) {
                        $searchArray[$key]['id'] = $searchU['work_place_id'];
                        $searchArray[$key]['name'] = $searchU['work_place_name'];
                        $searchArray[$key]['city'] = $searchU['work_place_city'];
                        $searchArray[$key]['followerscnt'] = $searchU['followerscnt'];
                        $searchArray[$key]['type'] = "W";
                    }
                }
                $searchDetails = $searchArray;
            }

            $filterArray['category'] = $request->getParam('category');
            $filterArray['keyword'] = $request->getParam('keyword');
            $filterArray['gender'] = $request->getParam('gender');
            $filterArray['agefrom'] = $request->getParam('agefrom');
            $filterArray['ageto'] = $request->getParam('ageto');
            $filterArray['email'] = $request->getParam('email');
            $filterArray['country'] = $request->getParam('country');
            $filterArray['state'] = $request->getParam('state');
            $filterArray['city'] = $request->getParam('city');
            $filterArray['school'] = $request->getParam('school');
            $filterArray['college'] = $request->getParam('college');
            $filterArray['workplace'] = $request->getParam('workplace');
            $filterArray['mkey'] = $request->getParam('mkey');
            $searchDetails = $searchObj->getCompleteSearchDetails($filterArray,$logged_user_Id,$pagenum,$limit);
            if(count($searchDetails) > 0) {
            // get Logged User Following Users-list
                $followObj = new User_Model_Followers();
                $loggedUserFollowing = $followObj->getAllFollowing($logged_user_Id, $logged_user_type);
                $followingCnt = count($loggedUserFollowing);
                if($category == 'People' || $category == 'Business') {
                    foreach($searchDetails as $key => $search) {
                        if($key != 0 && $cityid == $search['city']) {
                                $city = $cityname;
                        } elseif(!empty($search['city']) && $search['city'] != 0) {
                                $cityArr = $cityObj->getCityNameByCityID($search['city']);
                                $cityArr = $cityArr->toArray();
                                if(!empty($cityArr))
                                        $city = $cityArr[0]['city_name'];
                                else
                                        $city = '';
                                $cityid = $search['city'];
                                $cityname = $city;
                        } else {
                                $city = '';
                        }
                        $searchDetails[$key]['city'] = $city;
                        $searchUserType = $search['user_type'];
                        $user_id = $search['user_id'];
                        if($search['user_type'] != 'B' && $category == 'People') {
                            $searchUserType = 'U';
                        }
                        if($category == 'Businsess') {
                            $searchUserType = 'B';
                        }
                        if($logged_user_type == "B" && $category == 'People') {
                            $searchDetails[$key]['isFollowing'] = "2";
                        } else {
                            if($followingCnt > 0) {
                                foreach($loggedUserFollowing as $Usrfollows) {
                                    if($user_id == $logged_user_Id && $searchUserType == $logged_user_type) {
                                        $searchDetails[$key]['isFollowing'] = "2";
                                    } else {
                                        if(($user_id == $Usrfollows['follower_id']) && ($searchUserType == $Usrfollows['follower_type'])) {
                                        $searchDetails[$key]['isFollowing'] = "1";
                                        break;
                                        }  else {
                                        $searchDetails[$key]['isFollowing'] = "0";
                                        }
                                    }
                                }
                            } else {
                                if($user_id == $logged_user_Id && $searchUserType == $logged_user_type) {
                                    $searchDetails[$key]['isFollowing'] = "2";
                                } else {
                                    $searchDetails[$key]['isFollowing'] = "0";
                                }
                            }
                        }
                    }
                }
                if($request->getParam('category') == 'College' || $request->getParam('category') == 'School' || $request->getParam('category') == 'Workplace') {
                    if(count($searchDetails) > 0) {
                        foreach($searchDetails as $key => $popularUsr) {
                            if($request->getParam('category') == 'College')
                                $popularUsrType = 'C';
                            if($request->getParam('category') == 'School')
                                $popularUsrType = 'S';
                            if($request->getParam('category') == 'Workplace')
                                $popularUsrType = 'W';
                            if($followingCnt > 0) {
                                foreach($loggedUserFollowing as $Usrfollows) {
                                    if($popularUsr['id'] == $logged_user_Id && $popularUsrType == $popularUsr['type']) {
                                        $searchDetails[$key]['isFollowing'] = "2";
                                    } else {
                                        if(($popularUsr['id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                            $searchDetails[$key]['isFollowing'] = "1";
                                            break;
                                        }  else {
                                            $searchDetails[$key]['isFollowing'] = "0";
                                        }
                                    }
                                }
                            } else {
                                if($popularUsr['college_id'] == $logged_user_Id && $popularUsrType == 'U') {
                                    $searchDetails[$key]['isFollowing'] = "2";
                                } else {
                                    $searchDetails[$key]['isFollowing'] = "0";
                                }
                            }
                        }
                    }
                }
            }
            if(!empty($searchDetails)) {
                $resultAry = array('service_status'=>'success',"content" => $searchDetails);
                echo $myjson->customEncode($resultAry);
                return false;
            } else {
                $resultAry = array('service_status'=>'error',"error_msg" => "No Record Found");
                echo $myjson->customEncode($resultAry);
                return false;
            }
        }

        public function getquoteAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $msgObj = new User_Model_Message();
                $data = $msgObj->getquote();
                if(!empty($data)) {
                        echo $myjson->customEncode(array('service_status'=>'success',"content" => array($data),"responsecode" => '5000'));
                        return;
                } else {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'No Quote For The Day!!!',"responsecode" => '5000'));
                        return;
                }
        }

        public function getphotodetailedinfoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $photoObj = new Photos_Model_Photos();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $logged_userId = $this->getRequest()->getParam('logged_user_id');
                if($logged_userId == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }
                if(!is_numeric($logged_userId)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Logged User Id!!!',"usermediacode"=>"450"));
                        return;
                }
                $userId = $this->getRequest()->getParam('user_id');
                if($userId == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }
                if(!is_numeric($userId)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!',"usermediacode"=>"450"));
                        return;
                }
                $photoId = $this->getRequest()->getParam('photo_id');
                if($photoId == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Photo Id can not be Empty!!!',"usermediacode"=>"450"));
                        return;
                }
                if(!is_numeric($photoId)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Photo Id!!!',"usermediacode"=>"450"));
                        return;
                }

                $photodata = $photoObj->getphotodetailedinfo($logged_userId,$userId,$photoId);
                $mobphotoObj = new Mobile_Model_Photos();
                $photoBlastUserCnt = $mobphotoObj->getBlastedUserCountBasedOnPhotoId($photoId);
                $photodata['blastedUserCnt'] = $photoBlastUserCnt;
                $blastedUserDetails = $mobphotoObj->getBlastedUserDetailsBasedOnPhotoId($photoId, $userId);

                $photoCount = 0;
                foreach($blastedUserDetails as $blastUser)
                {
                    $blastUser['created_date'] = $timeObj->humaneDate($blastUser['created_date']);
                    $blastedUserDetails[$photoCount] = $blastUser;
                    $photoCount++; 
                }

                $photodata['blastedUsers'] = $blastedUserDetails;
                $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $photodata['date'] = $timeObj->humaneDate($photodata['date']);
                $photodata['scribbleusername'] = $scribObj->scribbleUname($photodata,26);
                if(isset($photodata['commenter_date1']) && $photodata['commenter_date1'] != '') {
                        $photodata['commenter_date1'] = $timeObj->humaneDate($photodata['commenter_date1']);
                }
                if(isset($photodata['commenter_date2']) && $photodata['commenter_date2'] != '') {
                        $photodata['commenter_date2'] = $timeObj->humaneDate($photodata['commenter_date2']);
                }
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($logged_userId, 'U', '1');
                if(!empty($photodata)) {
                        $resultAry = array('service_status'=>'success',"content" => array($photodata), "notificationcnt" => $notificationcnt['notificationcnt'],"usermediacode"=>"429");
                } else {
                        $resultAry = array('service_status'=>'error',"error_msg" => 'No Photo Details Found',"usermediacode"=>"429");
                }
                echo $myjson->customEncode($resultAry);
                return false;
        }
        
        //Novasys Code 
        public function getfollowersfortaggingAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
        $request = $this->getRequest();
        $userid = $request->getParam('user_id');
        if($userid == '') {
                echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                return;
        }
        if(!is_numeric($userid)) {
                echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!'));
                return;
        }
        
        $loggedUserType = $request->getParam('logged_user_type');
        if($loggedUserType == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Type can not be Empty!!!'));
                        return;
        }
        
        $loggedUserType = 'U';
        
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):30);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);

        $followObj = new User_Model_Followers();
        
        $userfollowers = $followObj->getMyFollowers($userid,$userid, $limit , $pagenum ,$loggedUserType);
        //echo "<pre>";print_r($userfollowers);exit;
        if(count($userfollowers) > 0) {
                $resAry = array('service_status' => 'success','content'=>$userfollowers);
                echo $myjson->customEncode($resAry);
        }
        elseif(count($userfollowers) == 0) {
                $resAry = array('service_status' => 'success','content'=>array());
                echo $myjson->customEncode($resAry);
        } else {
                $resAry = array('service_status' => 'error','error-message'=>'Some Error occured');
                echo $myjson->customEncode($resAry);
        }
        
        return false;
        }
        
        
        //Novasys Code get followers using tag value
        public function getfollowersfortagAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
        $request = $this->getRequest();
        $userid = $request->getParam('user_id');
        if($userid == '') {
                echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                return;
        }
        if(!is_numeric($userid)) {
                echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User Id!!!'));
                return;
        }
        
        $loggedUserType = $request->getParam('logged_user_type');
        if($loggedUserType == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Type can not be Empty!!!'));
                        return;
        }
        
        $loggedUserType = 'U';
        
        $searchKey = $request->getParam('search_key');
        
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):30);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);

        $followObj = new User_Model_Followers();
        
        $userfollowers = $followObj->getAllMyFollowersUsingUserId($userid, $limit , $pagenum ,$loggedUserType,$searchKey);
        //echo "<pre>";print_r($userfollowers);exit;
        if(count($userfollowers) > 0) {
                $resAry = array('service_status' => 'success','content'=>$userfollowers);
                echo $myjson->customEncode($resAry);
        }
        elseif(count($userfollowers) == 0) {
                $resAry = array('service_status' => 'success','content'=>array());
                echo $myjson->customEncode($resAry);
        } else {
                $resAry = array('service_status' => 'error','error-message'=>'Some Error occured');
                echo $myjson->customEncode($resAry);
        }
        
        return false;
        }

        public function getlocationdetailsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();

        $request = $this->getRequest();
        $country_id = $request->getParam('country_id');
        $state_id = $request->getParam('state_id');
        $city_id = $request->getParam('city_id');
        $zipcode_id = $request->getParam('zipcode_id');
        $category = $request->getParam('category');
        $keyword = $request->getParam('keyword');
        $error_message='';

        $countryObj = new User_Model_Countries();
        $stateObj = new User_Model_States();
        $cityObj = new User_Model_Cities();

        if($keyword == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => ' Keyword can not be Empty!!!'));
                        return;
        }

        if($category == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => ' Category can not be Empty!!!'));
                        return;
        }

        if($category == "country")
        {
            $resultArr = $countryObj->getcountiresbykeyword(30,$keyword);
        }


        if($category == "state")
        {
            if($country_id != ''){
            $resultArr = $stateObj->getstatesbykeyword(30,$keyword,$country_id);
            }
            else
            {
                $error_message = "Country Id can not be Empty!!!";
            }
        }

        if($category == "city")
        {
            if($state_id != '')
            {
            $resultArr = $cityObj->getcitiesbykeyword(30,$keyword,$state_id);
            }
            else
            {
                $error_message = "State Id can not be Empty!!!";
            }
        }

        if($category == "zipcode")
        {
            if($city_id != '')
            {
            $resultArr = $cityObj->getzipcodebykeyword(30,$keyword,$city_id);
            }
            else
            {
                $error_message = "City Id can not be Empty!!!";
            }
        }

        if(count($resultArr) > 0) {
                $resAry = array('service_status' => 'success','content'=>$resultArr);
                echo $myjson->customEncode($resAry);
        }
        elseif(count($resultArr) == 0) {
                if($error_message == ''){
                $resAry = array('service_status' => 'success','message'=>'No record found.');
                echo $myjson->customEncode($resAry);
               }else
               {
                $resAry = array('service_status' => 'success','message'=>$error_message);
                echo $myjson->customEncode($resAry);
               }
        } else {
                $resAry = array('service_status' => 'error','error-message'=>'Some Error occured');
                echo $myjson->customEncode($resAry);
        }
        return false;
       
        }


      public function getprofessiondetailsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();

        $request = $this->getRequest();
        $category = $request->getParam('category');
        $keyword = $request->getParam('keyword');

        $userObj = new User_Model_User();

        if($keyword == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => ' Keyword can not be Empty!!!'));
                        return;
        }

        if($category == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => ' Category can not be Empty!!!'));
                        return;
        }

        if($category == "schools")
        {
            $resultArr = $userObj->getschoolsbykeyword(30,$keyword);
        }


        if($category == "colleges")
        {
            $resultArr = $userObj->getcollegesbykeyword(30,$keyword);
        }

        if($category == "employers")
        {
           
            $resultArr = $userObj->getworkplacesbykeyword(30,$keyword);
        }

        if(count($resultArr) > 0) {
                $resAry = array('service_status' => 'success','content'=>$resultArr);
                echo $myjson->customEncode($resAry);
        }
        elseif(count($resultArr) == 0) {
                $resAry = array('service_status' => 'success','message'=>'No record found.');
                echo $myjson->customEncode($resAry);
        } else {
                $resAry = array('service_status' => 'error','error-message'=>'Some Error occured');
                echo $myjson->customEncode($resAry);
        }
        return false;
       
        }

        //API for stats 
         public function getuserstatsdataAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $req = $this->getRequest();
                $uid=$req->getParam('user_id');
                if($uid == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($uid)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
                }
                $loggedUserID = $uid;//$req->getParam('logged_user_id');
                if($loggedUserID == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Logged User Id can not be Empty!!!'));
                        return;
                }
                if(!is_numeric($loggedUserID)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid Logged User  Id!!!'));
                        return;
                }
                $userDetAry =array();
                $userDetAry = $userObj->getUserDetailsForStats($uid);
                if(count($userDetAry) !=1){
                        $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist');
                        echo $myjson->customEncode($resultAry);
                        return true;
                }
                $loggeduserDetAry = $userObj->getUserDetailsForStats($loggedUserID);
                $userDetAry = $userDetAry->toArray();

                // to Get User Level Name
                $usrLvlObj = new User_Model_Userlevels();
                $usrlevelName = $usrLvlObj->getUserLevels($userDetAry['user_level']);
                $usrlevelName = $usrlevelName->toArray();

                //User overall Rank
                $usrOverAllRank = $usrLvlObj->getUserOverAllRank($userDetAry['cool_points']);

                //User Rank in level
                $usrLevelRank = $usrLvlObj->getUserRankInLevel($usrlevelName,$userDetAry['cool_points']);

                //User business
                $userBusinessArr = $userObj->getBussinessForState($uid);
                
                
                //user colleges 
                $userEployersArr = $userObj->getEmployersForState($uid,$userDetAry['employer_info']);

                $updatedDate = $userDetAry['updated_date'];

                //Get user all sch0ols info from profiles and Top line 
                $userSchoolsArr = $userObj->getSchoolsFromProfileForState($userDetAry['high_school_info'],$updatedDate,$uid);

                //Get user all colleges  info from profiles and Top line 
                $userCollegesArr = $userObj->getcollegsFromProfileForState($userDetAry['college_info'],$updatedDate,$uid);

                //Get user all employers  info from profiles and Top line 
                $userEployersArr = $userObj->getEmployersFromProfileForState($userDetAry['employer_info'],$updatedDate,$uid);

                
                $userDetAry['user_level_name'] = $usrlevelName['level_name'];
                $userDetAry['user_overall_rank'] = $usrOverAllRank['rank'];
                $userDetAry['user_level_rank'] = $usrLevelRank['rank'];
                $userDetAry['nooffollowers'] = $followObj->getFollowersCnt($uid);
                $userDetAry['nooffollowings'] = $followObj->getFollowingCnt($uid);
                $userDetAry['schools'] = $userSchoolsArr;
                $userDetAry['colleges'] = $userCollegesArr;
                $userDetAry['employers'] = $userEployersArr;
                $userDetAry['business'] = $userBusinessArr;


                $resultAry = array('service_status'=>'success',"content" => $userDetAry );
                echo $myjson->customEncode($resultAry);
                return true;
        }


        public function zencodercallbackAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();

                $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/logs/app.log');
                $logger = new Zend_Log($writer);
                $logger->info('<br>----------------- zencodercallbackAction -----------------------------------');

                $request = $this->getRequest()->getRawBody();
                $logger->info('<br> request post data .....'.$request);

                $dataObj = json_decode($this->getRequest()->getRawBody());

                $logger->info('<br> dataObj job id  .....'.$dataObj->job->id);
                $logger->info('<br> dataObj job state  .....'.$dataObj->job->state);


                $jobid = $dataObj->job->id;
                $jobStatus = $dataObj->job->state;
                
                if($jobid)
                {
                    $videoObj = new Videos_Model_Videos();
                    $updateAry['status'] = $jobStatus;
                    
                    $video_id = $videoObj->updateVideoStatus($updateAry,$jobid);
                    if($video_id)
                        echo "successfully Updated....!!!";
                    else
                        echo "Failed to update.......!!!";
                }
                return true;
        }

        public function shownotificationcountAction()
        {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
            $notificationObj = new Notifications_Model_Notifications();

            $req = $this->getRequest();
                
            $user_id=$req->getParam('user_id');
            if($user_id == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'User Id can not be Empty!!!'));
                        return;
            }
            if(!is_numeric($user_id)) {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_message" => 'Invalid User  Id!!!'));
                        return;
            }


            $notificationcntObj = $notificationObj->notificationcnt($user_id,'U','1');
            $messagenotificationcntObj = $notificationObj->messegenotificationcnt($user_id,'U','1');

            $medianotificationcntObj = $notificationObj->medianotificationcnt($user_id,'U','1');

            $sribblenotificationcntObj = $notificationObj->sribblenotificationcnt($user_id,'U','1');

            $huddlenotificationcntObj = $notificationObj->huddlenotificationcnt($user_id,'U','1');

            
            $resultAry = array('service_status'=>'success',"content" => $notificationcntObj['notificationcnt'],"messegenotificationcnt" => $messagenotificationcntObj['messegenotificationcnt'],"medianotificationcnt" => $medianotificationcntObj['medianotificationcnt'],"sribblenotificationcnt" => $sribblenotificationcntObj['sribblenotificationcnt'],"huddlenotificationcnt" => $huddlenotificationcntObj['huddlenotificationcnt'],"responsecode" => "700");

           
            echo $myjson->customEncode($resultAry);
            return false;
        }



}