<?php
class Notifications_IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $user_account_status = $logged_user_data->user_account_status;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $user_account_status = $logged_user_data->status;
            $loggedUserType = 'B';
        }
        if(!empty($logged_user_data)) {
            $limit= 30;
            $photosObj = new Mobile_Model_Photos();
            $followersgallery = $photosObj->getTimelineClickedImagesVideosDetails($loggedUserId,$loggedUserType, $limit);
            //echo "<pre>";print_r($followersgallery);exit;
            //echo count($followersgallery);exit;
            $this->view->assign('followersgallery', $followersgallery);
        }
    }

    public function indexAction() {
        // action body
    }

    public function getnotificationswithidAction() {
        $this->_helper->layout->disableLayout();
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $loggedUserType = 'B';
        }
        if($loggedUserId == '') {
            $this->_redirect('user');
        }
        $notificationObj = new Notifications_Model_Notifications();
        //$result = $notificationObj->getnotificationswithId($loggedUserId,$loggedUserType);
        //$result = $notificationObj->getUserNotifications($loggedUserId,$loggedUserType,'30','','');
        $result = $notificationObj->getUserUnreadNotifications($loggedUserId,$loggedUserType,'10');
        $notificationscnt = count($result);
        for($i=0;$i<$notificationscnt;$i++) {
            if(!empty($result[$i]['notification_id']))
                    $notificationIds[] =  $result[$i]['notification_id'];
        }
        $ids = @implode(",", $notificationIds);
        if(!empty($ids)) {
            $data['read_status'] = '0';
            //$id = $notificationObj->updatereadstatus($data, @implode(",", $notificationIds));
            $upd = $notificationObj->updateNotificationReadStatus($loggedUserId,$loggedUserType);
        }
        $this->view->notification = $result;
    }

    public function getallnotificationsAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $loggedUserType = 'B';
        }
        if($loggedUserId == '') {
            $this->_redirect('user');
        }
        $notificationObj = new Notifications_Model_Notifications();
        $cnt = $notificationObj->notificationcnt($loggedUserId,$loggedUserType,'0');
        //echo $loggedUserId.'-->'.$loggedUserType;echo $cnt['notificationcnt'];//exit;
        $result = $notificationObj->getUserNotifications($loggedUserId,$loggedUserType,'30','','');
        //echo '<pre>';print_r($result);exit;
        $this->view->notification = $result;
        $this->view->notificationcnt = $cnt['notificationcnt'];
    }

    public function morenotificationsAction() {
        $this->_helper->layout->disableLayout();
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $loggedUserType = 'B';
        }
        if($loggedUserId == '') {
            $this->_redirect('user');
        }
        $lastnotification_id = $this->getRequest()->getParam('notification_id');
        $notificationObj = new Notifications_Model_Notifications();
        $cnt = $notificationObj->notificationcnt($loggedUserId,$loggedUserType,'0');
        $result = $notificationObj->getUserNotifications($loggedUserId,$loggedUserType,'30',$lastnotification_id,'previous');
        $this->view->notification = $result;
        $this->view->notificationcnt = $cnt['notificationcnt'];
    }

    public function deletenotificationAction() {
        $this->_helper->layout->disableLayout();
        $notification_id = $this->getRequest()->getParam('notification_id');
        $notificationObj = new Notifications_Model_Notifications();
        $id = $notificationObj->deleteNotification($notification_id);
        echo $id;exit;
    }

}