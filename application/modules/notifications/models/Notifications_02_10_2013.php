<?php
class Notifications_Model_Notifications {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Notifications_Model_DbTable_Notifications');
                }
                return $this->_dbTable;
        }
        
        public function insertNewRecord($data){
                return $this->getDbTable()->insert($data);
        }

        public function getUserNotifications($userId,$userType,$limit,$notificationid='',$type='') {
                $notifications = $this->getNotificationsByUserId($userId,$userType,$limit,$notificationid,$type);
                $result = array();
                foreach($notifications as $val):
                        $notifications = $this->getNotificationDetails($val);
                        if($notifications != '')
                                $result[] = $notifications;
                endforeach;
                return $result;
        }

        public function getUserUnreadNotifications($userId,$userType,$limit) {
                $unread = $this->getUnreadNotifications($userId,$userType);
                $result = array();
                if(count($unread) > 0) {
                        foreach($unread as $key => $val):
                                $notifications = $this->getNotificationDetails($val);
                                if($notifications != '')
                                        $result[] = $notifications;
                        endforeach;
                }
                if(count($result) < $limit) {
                        $remainCnt = $limit - count($result);
                        $read = $this->getReadNotifications($userId,$userType,$remainCnt);
                        if(count($read) > 0) {
                                foreach($read as $key => $val):
                                        $notifications = $this->getNotificationDetails($val);
                                        if($notifications != '')
                                                $result[] = $notifications;
                                endforeach;
                        }
                }
                return $result;
        }

        public function getallUserNotifications($userId,$userType) {
                $read = $this->getReadNotifications($userId,$userType,'30');
                if(count($read) > 0) {
                    foreach($read as $val):
                            $notifications = $this->getNotificationDetails($val);
                            if($notifications != '')
                                    $result[] = $notifications;
                    endforeach;
                }
                return $result;
        }

        //get all notifications by Userid
        public function getNotificationsByUserId($userId,$userType,$limit,$notificationid='',$orderType='') {
                $select = $this->getDbTable()->select()
                                            ->where('user_id = ?',$userId)
                                            ->where('user_type =?',$userType)
                                            ->where('notifications_type != ?','photo_uploads');
                if($notificationid != '' && $orderType != '') {
                        if($orderType == "previous") {
                                $select->where("id < '".$notificationid."'");
                        }
                        if($orderType == "next") {
                                $select->where("id > '".$notificationid."'");
                        }
                }
                $select->group('id')
                        ->order('action_date DESC')
                        ->limit($limit);
                if($_SERVER['REMOTE_ADDR']=='182.72.66.214'){
                    //echo $select;exit;
                    //echo '<Pre>';print_r($notifications);exit;
                }
                $notifications = $this->getDbTable()->fetchAll($select);
                
                return $notifications;
        }

        //get all unread notifications
        public function getUnreadNotifications($userId,$userType) {
                $select = $this->getDbTable()->select()
                                            ->where('user_id = ?',$userId)
                                            ->where('user_type = ?',$userType)
                                            ->where('notifications_type != ?','photo_uploads')
                                            ->where('notifications_type != ?','Followed')
                                            ->where('read_status = ?', 1)
                                            ->order('id DESC');
                $unread = $this->getDbTable()->fetchAll($select);
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.53')
                    //echo '<pre>';print_r($unread);exit;
                return $unread;
        }

        //get all unread notifications
        public function getReadNotifications($userId,$userType,$remainCnt) {
                $select = $this->getDbTable()->select()
                                            ->where('user_id = ?',$userId)
                                            ->where('user_type =?',$userType)
                                            ->where('notifications_type != ?','photo_uploads')
                                            ->where('read_status = ?', 0)
                                            ->order('id DESC')
                                            ->limit($remainCnt);
                $read = $this->getDbTable()->fetchAll($select);
                return $read;
        }

        public function getNotificationDetails($notDet) {
                $info = '';
                switch($notDet['notifications_type']) {
                        case 'message': $info = $this->getMsgNotification($notDet);
                                break;
                        /*case 'Followed': $info = $this->getFollowedNotifications($notDet);
                        	break;*/
                        case 'following': $info = $this->getfollowingNotifications($notDet);
                        	break;
                        case 'purchasecoupon': $info = $this->getPurchaseupperNotifications($notDet);
                        	break;
                        case 'new_upper': $info = $this->getNewupperNotifications($notDet);
                        	break;
                        case 'level_upgrade': $info = $this->getLevelupgradeNotifications($notDet);
                        	break;
                        case 'scribble_blasted': $info = $this->getScribbleblastedNotifications($notDet);
                        	break;
                        case 'photo_blasted': $info = $this->getPhotoblastedNotifications($notDet);
                        	break;
                        case 'video_blasted': $info = $this->getVideoblastedNotifications($notDet);
                        	break;
                        case 'tomorrow_expired': $info = $this->getTomorrowexpiredNotifications($notDet);
                        	break;
                        case 'expired': $info = $this->getExpiredNotifications($notDet);
                        	break;
                        /*case 'photo_uploads': $info = $this->getPhotouploadsNotifications($notDet);
                        	break;*/
                        case 'video_uploads': $info = $this->getVideouploadsNotifications($notDet);
                        	break;
                        case 'huddle_target': $info = $this->getHuddleTargetNotifications($notDet);
                        	break;
                        case 'business_target' : $info = $this->getBusinessTargetNotifications($notDet);
                        default:;
                }
                return $info;
        }

        public function getMsgNotification($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_user_messages')
                        ->where('message_id = ?',$notDet['reference_id'])
                        ->where('receiver_delete = ?', 0);
                $msgResult = $db->fetchRow($select);
                if($msgResult['from_user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$msgResult['from_user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.$userResult['firstname'].'</b> has sent you a message';
                        $content['action_date'] = $msgResult['datetime'];
                        $content['message_id'] = $msgResult['message_id'];
                }
                return $content;
        }
        
        public function getfollowingNotifications($notDet) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $content = array();
                $select = $db->select()
                        ->from('tbl_followers')
                        ->where('followers_id = '.$notDet['reference_id']);
                $followingResult = $db->fetchRow($select);
                if($followingResult['user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$followingResult['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '@<b>'.ucwords($userResult['firstname']).'</b> is now following you';
                        $content['action_date'] = $followingResult['created_date'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$followingResult['user_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '@<b>'.ucwords($businessResult['firstname']).'</b> is now following you';
                        $content['action_date'] = $followingResult['created_date'];
                }
                return $content;
        }

        public function getFollowedNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_followers')
                        ->where('followers_id = ?',$notDet['reference_id']);
                $followedResult = $db->fetchRow($select);
                //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo '<Pre>';print_r($followedResult);exit; }
                if($followedResult['user_type'] == 'U' && $followedResult['follower_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$followedResult['follower_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        //$content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> is followed by you';
                        $content['message'] = 'You are now following @<b>'.ucwords($userResult['firstname']).'</b>';
                        $content['action_date'] = $followedResult['created_date'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$followedResult['follower_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = 'You are now following @<b>'.ucwords($businessResult['firstname']).'</b>';
                        $content['action_date'] = $followedResult['created_date'];
                }
                //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo '<Pre>';print_r($content);exit; }
                return $content;
        }
        
        public function getPurchaseupperNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_purchase_coupons')
                        ->where('purchase_id = ?',$notDet['reference_id']);
                $purchaseResult = $db->fetchRow($select);
                $select = $db->select()
                        ->from('tbl_coupons')
                        ->where('coupon_id = ?',$purchaseResult['coupon_id']);
                $purchasecouponResult = $db->fetchRow($select);
                if($purchaseResult['user_id'] != '') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$purchaseResult['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = 'You have purchased a '.$purchasecouponResult['title'].' Upper';//'<b>'.ucwords($userResult['firstname']).
                        $content['action_date'] = $purchaseResult['purchase_date']; 
                        $content['coupon_id'] = $purchaseResult['coupon_id'];
                        $content['total_coupon'] = $purchaseResult['total_coupon'];
                        $content['business_id'] = $purchasecouponResult['business_id'];
                }
                return $content;
        }

        public function getNewupperNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_coupons')
                        ->where('coupon_id = ?',$notDet['reference_id']);
                $NewUpperResult = $db->fetchRow($select);
                if($NewUpperResult['business_id'] != '') {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$NewUpperResult['business_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '<b>'.ucwords($businessResult['business_name']).'</b> has created a '.$NewUpperResult['title'].' Upper';
                        $content['action_date'] = $NewUpperResult['created_date'];
                        $content['coupon_id'] = $NewUpperResult['coupon_id'];
                        $content['business_id'] = $NewUpperResult['business_id'];
                }
                return $content;
        }

        public function getLevelupgradeNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                if($notDet['notifications_type'] == 'level_upgrade') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$notDet['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> your level is upgraded';
                        $content['action_date'] = $notDet['action_date'];
                }
                return $content;
        }

        public function getScribbleblastedNotifications($notDet) {
                //echo '<pre>';print_r($notDet);exit;
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_blasts')
                        ->where('scribble_id = ?',$notDet['reference_id']);
                $scribbleResult = $db->fetchRow($select);
                //echo '<pre>';print_r($scribbleResult);exit;
                if($scribbleResult['user_id'] != '') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$scribbleResult['user_id']);
                        $userResult = $db->fetchRow($select);
                        //echo '<pre>';print_r($userResult);exit;
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> has blasted your Scribble';
                        $content['action_date'] = $notDet['action_date'];
                        $content['blast_id'] = $scribbleResult['blast_id'];
                        $content['scribble_id'] = $scribbleResult['scribble_id'];
                        $content['from_user_id'] = $notDet['user_id'];
                }//echo '<pre>';print_R($content);exit;
                return $content;
        }
		
        public function getPhotoblastedNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_photos')
                        ->where('photo_id = ?',$notDet['reference_id']);
                $blastedphotoResult = $db->fetchRow($select);
                if($blastedphotoResult['user_id'] == '') return;
                if($blastedphotoResult['user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$blastedphotoResult['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> has blasted your photo';
                        $content['action_date'] = $blastedphotoResult['created_date'];
                        $content['album_id'] = $blastedphotoResult['album_id'];
                        $content['photo_id'] = $blastedphotoResult['photo_id'];
                        $content['blast_id'] = $blastedphotoResult['blast_id'];
                        if($blastedphotoResult['blast_id'] != '') {
                            $select = $db->select()
                                            ->from('tbl_photos','user_id')
                                            ->where('photo_id = ?',$blastedphotoResult['blast_id']);
                            $blastedphotoResult1 = $db->fetchRow($select);
                            $content['owner_id'] = $blastedphotoResult1['user_id'];
                        } else
                            $content['owner_id'] = $blastedphotoResult['user_id'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$blastedphotoResult['user_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '<b>'.ucwords($businessResult['firstname']).'</b> has blasted your photo';
                        $content['action_date'] = $blastedphotoResult['created_date'];
                        $content['album_id'] = $blastedphotoResult['album_id'];
                        $content['photo_id'] = $blastedphotoResult['photo_id'];
                        $content['blast_id'] = $blastedphotoResult['blast_id'];
                        if($blastedphotoResult['blast_id'] != '') {
                            $select = $db->select()
                                            ->from('tbl_photos','user_id')
                                            ->where('photo_id = ?',$blastedphotoResult['blast_id']);
                            $blastedphotoResult1 = $db->fetchRow($select);
                            $content['owner_id'] = $blastedphotoResult1['user_id'];
                        } else
                            $content['owner_id'] = $blastedphotoResult['user_id'];
                }
                return $content;
        }

        public function getVideoblastedNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_videos')
                        ->where('video_id = ?',$notDet['reference_id']);
                $blastedvideoResult = $db->fetchRow($select);
                if($blastedvideoResult['user_id'] == '') return;
                if($blastedvideoResult['user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$blastedvideoResult['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> has blasted your Video';
                        $content['action_date'] = $blastedvideoResult['created_date'];
                        $content['video_id'] = $blastedvideoResult['video_id'];
                        $content['owner_id'] = $blastedvideoResult['user_id'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$blastedvideoResult['user_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '<b>'.ucwords($businessResult['firstname']).'</b> has blasted your Video';
                        $content['action_date'] = $blastedvideoResult['created_date'];
                        $content['video_id'] = $blastedvideoResult['video_id'];
                        $content['owner_id'] = $blastedvideoResult['user_id'];
                }
                return $content;
        }

        public function getTomorrowexpiredNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_coupons')
                        ->where('coupon_id = ?',$notDet['reference_id']);
                $tomexpResult = $db->fetchRow($select);
                if($notDet['user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$notDet['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> your purchased '.$tomexpResult['title'].' upper will expire within 1 Day.';
                        $content['action_date'] = $notDet['action_date'];
                        $content['coupon_id'] = $tomexpResult['coupon_id'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$notDet['user_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '<b>'.ucwords($businessResult['firstname']).'</b> your '.$tomexpResult['title'].' upper will expire within 1 Day.';
                        $content['action_date'] = $notDet['action_date'];
                        $content['coupon_id'] = $tomexpResult['coupon_id'];
                }
                return $content;
        }

        public function getExpiredNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_coupons')
                        ->where('coupon_id = ?',$notDet['reference_id']);
                $expResult = $db->fetchRow($select);
                if($notDet['user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$notDet['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> your purchased '.$expResult['title'].' upper had expired.';
                        $content['action_date'] = $notDet['action_date'];
                        $content['coupon_id'] = $expResult['coupon_id'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$notDet['user_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '<b>'.ucwords($businessResult['firstname']).' your '.$expResult['title'].' upper had expired.';
                        $content['action_date'] = $notDet['action_date'];
                        $content['coupon_id'] = $expResult['coupon_id'];
                }
                return $content;
        }

        public function getPhotouploadsNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_photos')
                        ->where('photo_id = ?',$notDet['reference_id']);
                $photouploadResult = $db->fetchRow($select);
                //echo '<Pre>';print_r($photouploadResult);exit;
                if($photouploadResult['user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$photouploadResult['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> had uploaded a photo.';
                        $content['action_date'] = $photouploadResult['created_date'];
                        $content['album_id'] = $photouploadResult['album_id'];
                        $content['photo_id'] = $photouploadResult['photo_id'];
                        $content['owner_id'] = $photouploadResult['user_id'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$photouploadResult['user_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '<b>'.ucwords($businessResult['firstname']).'</b> had uploaded a photo.';
                        $content['action_date'] = $photouploadResult['created_date'];
                        $content['album_id'] = $photouploadResult['album_id'];
                        $content['photo_id'] = $photouploadResult['photo_id'];
                        $content['owner_id'] = $photouploadResult['user_id'];
                }
                return $content;
        }
        
        public function getVideouploadsNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('tbl_videos')
                        ->where('video_id = ?',$notDet['reference_id']);
                $videouploadResult = $db->fetchRow($select);
                if($videouploadResult['user_type'] == 'U') {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$videouploadResult['user_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = '<b>'.ucwords($userResult['firstname']).'</b> had uploaded a video.';
                        $content['action_date'] = $videouploadResult['created_date'];
                        $content['video_id'] = $videouploadResult['video_id'];
                        $content['owner_id'] = $videouploadResult['user_id'];
                } else {
                        $select = $db->select()
                                ->from('tbl_business_users')
                                ->where('business_id = ?',$videouploadResult['user_id']);
                        $businessResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $businessResult['business_id'];
                        $content['user_type'] = 'B';
                        $content['firstname'] = $businessResult['firstname'];
                        $content['user_image'] = $businessResult['image_path'];
                        $content['message'] = '<b>'.ucwords($businessResult['firstname']).'</b> had uploaded a video.';
                        $content['action_date'] = $videouploadResult['created_date'];
                        $content['video_id'] = $videouploadResult['video_id'];
                        $content['owner_id'] = $videouploadResult['user_id'];
                }
                return $content;
        }

        public function getHuddleTargetNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('huddle_user_chat')
                        ->where('room_id  = ?',$notDet['reference_id']);
                $huddleResult = $db->fetchRow($select);
                if($huddleResult['owner_id'] == $notDet->user_id) {
                        $select = $db->select()
                                ->from('tbl_users')
                                ->where('user_id = ?',$huddleResult['owner_id']);
                        $userResult = $db->fetchRow($select);
                        $content['read_status'] = $notDet['read_status'];
                        $content['notification_id'] = $notDet['id'];
                        $content['notification_type'] = $notDet['notifications_type'];
                        $content['user_id'] = $userResult['user_id'];
                        $content['user_type'] = 'U';
                        $content['firstname'] = $userResult['firstname'];
                        $content['user_image'] = $userResult['profile_pic_path'];
                        $content['message'] = 'You has Created Chat Room';//'<b>'.ucwords($userResult['firstname']).
                        $content['action_date'] = $notDet['action_date'];
                        $content['room_id'] = $huddleResult['room_id'];
                        $content['owner_id'] = $huddleResult['owner_id'];
                } else {
                        $tids = explode(',', $huddleResult['target_id']);
                        if(in_array($notDet->user_id, $tids)) {
                                $select = $db->select()
                                        ->from('tbl_users')
                                        ->where('user_id = ?',$notDet->user_id);
                                $userResult = $db->fetchRow($select);
                                $content['read_status'] = $notDet['read_status'];
                                $content['notification_id'] = $notDet['id'];
                                $content['notification_type'] = $notDet['notifications_type'];
                                $content['user_id'] = $userResult['user_id'];
                                $content['user_type'] = 'U';
                                $content['firstname'] = $userResult['firstname'];
                                $content['user_image'] = $userResult['profile_pic_path'];
                                $content['message'] = 'You have been Targeted in Chat Room';//'<b>'.ucwords($userResult['firstname']).
                                $content['action_date'] = $notDet['action_date'];
                                $content['room_id'] = $huddleResult['room_id'];
                                $content['owner_id'] = $huddleResult['owner_id'];
                        }
                        $mids = explode(',', $huddleResult['members_id']);
                        if(in_array($notDet->user_id, $mids)) {
                                $select = $db->select()
                                        ->from('tbl_users')
                                        ->where('user_id = ?',$notDet->user_id);
                                $userResult = $db->fetchRow($select);
                                $content['read_status'] = $notDet['read_status'];
                                $content['notification_id'] = $notDet['id'];
                                $content['notification_type'] = $notDet['notifications_type'];
                                $content['user_id'] = $userResult['user_id'];
                                $content['user_type'] = 'U';
                                $content['firstname'] = $userResult['firstname'];
                                $content['user_image'] = $userResult['profile_pic_path'];
                                $content['message'] = 'You have been invited to Chat Room';//'<b>'.ucwords($userResult['firstname']).
                                $content['action_date'] = $notDet['action_date'];
                                $content['room_id'] = $huddleResult['room_id'];
                                $content['owner_id'] = $huddleResult['owner_id'];
                        }
                }
                return $content;
        }

        public function getBusinessTargetNotifications($notDet) {
                $content = array();
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from('huddles_business_target')
                        ->where('id  = ?',$notDet['reference_id']);
                $huddleResult = $db->fetchRow($select);
                //echo '<pre>';print_r($huddleResult);exit;
                $select = $db->select()
                            ->from('tbl_users')
                            ->where('user_id = ?',$huddleResult['user_id']);
                $userResult = $db->fetchRow($select);
                $content['read_status'] = $notDet['read_status'];
                $content['notification_id'] = $notDet['id'];
                $content['notification_type'] = $notDet['notifications_type'];
                $content['user_id'] = $userResult['user_id'];
                $content['user_type'] = 'U';
                $content['realname'] = $userResult['firstname'].' '.$userResult['lastname'];
                $content['username'] = $userResult['username'];
                $content['user_image'] = $userResult['profile_pic_path'];
                $content['message'] = ucwords($content['realname']).' @'.$content['username'].' has started a huddle session and we think they are talking about your product: '.$huddleResult['keyword'];
                $content['action_date'] = $notDet['action_date'];
                return $content;
        }

        public function updatereadstatus($data, $id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $res = $db->update('tbl_notifications',$data, array('id IN (?)' => $id));
                return $res;
        }
        
        public function updateNotificationReadStatus($userId,$userType) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $res = $db->update('tbl_notifications',array("read_status"=>"0"), array('user_id IN (?)' => $userId,'user_type IN (?)'=>$userType));
                return $res;
        }

        public function notificationcnt($loggedUserId,$loggedUserType,$status) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('N' => 'tbl_notifications'), array("COUNT('N.id') as notificationcnt"))
                            ->where('N.read_status = ?',$status)
                            ->where('N.notifications_type != ?',"photo_uploads")
                            ->where('N.notifications_type != ?',"Followed")
                            ->where('N.user_id = ?',$loggedUserId)
                            ->where('N.user_type = ?',$loggedUserType);
                $cnt = $db->fetchrow($select);
                return $cnt;
        }

        public function valueexistsrnot($loggedUserId,$loggedUserType) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('T' => 'tbl_notifications_time_status'),array('id','last_seen_time'))
                            ->where('T.user_id = ?',$loggedUserId)
                            ->where('T.user_type = ?',$loggedUserType);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function deleteNotification($notificationid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE N.* FROM tbl_notifications N WHERE N.id = ".$notificationid);
                $id = $select->execute();
                return $id;
        }

        public function deleteNotificationByReference($refid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE N.* FROM tbl_notifications N WHERE N.reference_id = ".$refid);
                $id = $select->execute();
                return $id;
        }
        
        public function getScribbleNotifications($userId,$userType) {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($userType != "B")
                        $userType = "U";
                //get all notifications by Userid
                $select = $this->getDbTable()->select()
                                            ->from(array('N'=>'tbl_notifications'))
                                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userId."' AND F.user_type = '".$userType."'", array(''))
                                            ->joinLeft(array('U' => 'tbl_users')," N.user_id = U.user_id AND U.user_type = U.user_type AND U.user_id = '".$userId."'", array('profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                                            ->where('N.user_id = ?',$userId)
                                            ->where('N.user_type =?',$userType)
                                            ->where("notifications_type IN('huddle_target', 'purchasecoupon')")
                                            ->setIntegrityCheck(false);
                $notifications = $this->getDbTable()->fetchAll($select);
                $notifications = $notifications->toArray();
                if(count($notifications) > 0) {
                    foreach($notifications as $key => $notif){
                        if($notif['notifications_type'] == 'huddle_target') { 
                            $select = $db->select()
                                                    ->from(array('HC' => 'huddle_user_chat'),array('*'))
                                                    ->where('HC.room_id = ?',$notif['reference_id']);
                            $targetSelect = $db->fetchRow($select);
                            $notifications[$key]['huddleDetails'] = $targetSelect;
                        }
                        if($notif['notifications_type'] == 'purchasecoupon') {
                            $select = $db->select()
                                                    ->from(array('C' => 'tbl_coupons'),array('*'))
                                                    ->joinLeft(array('PC' => 'tbl_purchase_coupons')," PC.coupon_id = C.coupon_id AND PC.purchase_id = '".$notif['reference_id']."'", array(''))
                                                    ->where('PC.purchase_id = ?',$notif['reference_id']);
                            $couponAry = $db->fetchRow($select);
                            $notifications[$key]['couponDetails'] = $couponAry;
                        } 
                    }
                }
                return $notifications;
        }
        
        public function getFollowerCouponNotifications($user_id, $loggedUserID,$userType, $lastScribbleDate,$lastScribbleDate1, $followerIds, $limit2, $type=''){
                $db = Zend_Db_Table::getDefaultAdapter();
                if($userType != "B")
                        $userType = "U";
                //get all  Purchase Coupons Notifications by Userid 
                $select = $this->getDbTable()->select()
                                        ->from(array('N'=>'tbl_notifications'))
                                        ->where('FIND_IN_SET(N.user_id,"'.$followerIds.'")')
                                        ->where('N.user_type =?',$userType)
                                        ->where("notifications_type = 'purchasecoupon'");
                if($lastScribbleDate != ''){                    
                    if($type == 'previous'){
                        $select = $select ->where('N.action_date BETWEEN "'.$lastScribbleDate1.'" AND "'.$lastScribbleDate.'"');
                    } else {
                        $select = $select ->where('N.action_date >"'.$lastScribbleDate.'"');
                    }
                }
                $select = $select->limit($limit2)
                                             ->setIntegrityCheck(false);
				//echo $select;exit;
                $notifications = $this->getDbTable()->fetchAll($select);
                $notifications = $notifications->toArray();
                //echo "<pre>";print_r($notifications);exit;
                if(count($notifications) > 0) {
                    foreach($notifications as $key => $notif){
                        if($notif['notifications_type'] == 'purchasecoupon') {
                            $select = $db->select()
                                        ->from(array('C' => 'tbl_coupons'),array('C.coupon_id', 'C.title', 'C.code', 'C.description', 'C.original_price', 'C.discounted_price', 'C.coupon_price', 'C.expiration_date', 'C.image_path', 'C.status', 'C.business_id'))
                                        ->joinLeft(array('PC' => 'tbl_purchase_coupons')," PC.coupon_id = C.coupon_id AND PC.purchase_id = '".$notif['reference_id']."'", array(''))
                                        ->joinLeft(array('U' => 'tbl_users'),"PC.user_id = U.user_id", array('profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)','gender', 'user_level'))
                                        ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','zipcode'))
                                        ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                                        ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                                        ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                                        ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                                        ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                                        ->where('PC.purchase_id = ?',$notif['reference_id'])
                                        ->where("C.status='1'");
                            $couponAry = $db->fetchRow($select);
                            $notifications[$key]['couponDetails'] = $couponAry;
                            $notifications[$key]['couponDetails']['expire_date'] = $couponAry['expiration_date'];
                            $notifications[$key]['couponDetails']['expiration_date'] = date('d, M Y', strtotime($couponAry['expiration_date']));
                        } 
                    }
                }
                return $notifications;
        }
        
        public function getHuddleNotifications($loggedUserID,$userType, $lastScribbleDate, $lastScribbleDate1, $limit2, $type =''){
                $db = Zend_Db_Table::getDefaultAdapter();
                if($userType != "B")
                        $userType = "U";
                //get Huddlel notifications by Userid
                $select = $this->getDbTable()->select()
                                            ->from(array('N'=>'tbl_notifications'))
                                            ->joinLeft(array('U' => 'tbl_users')," N.user_id = U.user_id AND U.user_type = U.user_type AND U.user_id = '".$loggedUserID."'", array('profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                                            ->where('N.user_type =?',$userType)
                                            ->where('N.user_id =?',$loggedUserID)
                                            ->where("notifications_type = 'huddle_target'");
                if($lastScribbleDate != ''){
                    if($type == 'previous'){
                        $select = $select ->where('N.action_date BETWEEN "'.$lastScribbleDate1.'" AND "'.$lastScribbleDate.'"');
                    } else {
                        $select = $select ->where('N.action_date > "'.$lastScribbleDate.'"');
                    }
                }
                $select = $select->limit($limit2)
                                ->setIntegrityCheck(false);
                //echo $select;exit;
                $notifications = $this->getDbTable()->fetchAll($select);
                $notifications = $notifications->toArray();
                if(count($notifications) > 0) {
                    $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                    foreach($notifications as $key => $notif){
                        if($notif['notifications_type'] == 'huddle_target') { 
                            $select = $db->select()
                                                    ->from(array('HC' => 'huddle_user_chat'),array('*'))
                                                    ->where('HC.room_id = ?',$notif['reference_id']);
                            $targetSelect = $db->fetchRow($select);
                            $notifications[$key]['huddleDetails'] = $targetSelect;
                            $notifications[$key]['huddleDetails']['created_date'] = $timeObj->humaneDate($targetSelect['created_date']);
                        } 
                    }
                }
                return $notifications;
        }
        
        public function getNotoficationDetailsByNotificationID($notifucation_id) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $this->getDbTable()->select()
                                            ->where('id = ?',$notifucation_id);
                $notifications = $this->getDbTable()->fetchAll($select);
                //echo "<pre>";print_r($notifications);exit;
                return $notifications[0];
        }

        public function getunsendpushnotifications() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from('tbl_notifications as N')
                                ->where('N.send_push_notification = ?', 1)
                                ->where('N.user_id != ?', 0)
                                ->group('N.user_id');
                //echo $select;exit;
                $result = $db->fetchAll($select);
                return $result;
        }

        public function updatePushNotifications($userId,$userType) {
                $updAry['send_push_notification'] = 0;
                $res = $this->getDbTable()->update($updAry, array('user_id = ?' => $userId,'user_type =?' =>$userType));
                return $res;
        }

}