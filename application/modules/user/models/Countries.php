<?php
class User_Model_Countries {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_Countries');
                }
                return $this->_dbTable;
        }

        public function get_countries() {
                $select = $this->getDbTable()->select()
                                            ->where('status = "1"')
                                            ->order ('country_name ASC')
                                            ->limit(50);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getlocation($countryId,$stateId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C' => 'tbl_country'), array('id','country_name'))
                                ->joinLeft(array('S'=>'tbl_state'), "S.country_id = C.id", array('sid'=>'id', 'state_name'))
                                ->joinLeft(array('Ct'=>'tbl_city'), "Ct.state_id = S.id", array('cid'=>'id', 'city_name'))
                                ->where('C.status = "1"')
                                ->where('S.country_id = ?',$countryId)
                                ->where('Ct.state_id = ?',$stateId)
                                ->order('C.country_name ASC');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getCountryNameByCountryID($country_id) {
            $select = $this->getDbTable()->select()
                                        ->where('id = "'.$country_id.'" AND status = "1"');
            $resultSet = $this->getDbTable()->fetchAll($select);
            return $resultSet;
        }

        public function getCountryByName($country) {
            $select = $this->getDbTable()->select()
                                        ->where('country_code = "'.$country.'" AND status = "1"');
            $resultSet = $this->getDbTable()->fetchRow($select);
            return $resultSet;
        }

        public function insert($data) {
            $insId = $this->getDbTable()->insert($data);
            return $insId;
        }

        public function getcountiresbykeyword($limit, $keyword) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('C'=>'tbl_country'), array('country_name','id'))
                            ->where('C.country_name LIKE "'.addslashes($keyword).'%" AND C.status = "1"')
                            ->limit($limit);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

}