<?php
class User_Model_Message {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_Message');
                }
                return $this->_dbTable;
        }

        public function insert($data) {
                return $this->getDbTable()->insert($data);
        }

        public function updateMessages($updAry, $uid) {
                if(!empty($uid) && $uid != '0') {
                        try{
                                $where = "to_user_id = '".$uid."' OR from_user_id = '".$uid."'";
                                $res = $this->getDbTable()->update($updAry, $where);
                                if (false === $res) {
                                        return 0;  // bool false returned, query failed
                                } else {
                                        return 1;
                                }
                        } catch (Zend_Exception $zex){} 
                }
        }

        public function updateMessagesByMsgid($updAry, $msgid) {
                if(!empty($msgid) && $msgid != '0') {
                        try{
                                $res = $this->getDbTable()->update($updAry, array('message_id= ?' => $msgid));
                                if (false === $res) {
                                        return 0;  // bool false returned, query failed
                                } else {
                                        return 1;
                                }
                        } catch (Zend_Exception $zex){} 
                }
        }

        public function getInboxMessages($user_id, $limit='', $pagenum='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select    = $db->select()
                                ->from(array('M'=>'tbl_user_messages'))
                                ->joinLeft(array('U' => 'tbl_users')," M.from_user_id = U.user_id ", array('gender','firstname','lastname','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)','profile_pic_path'))
                                ->joinLeft(array('L'=>'tbl_user_levels'),"L.level_no = U.user_level",array('level_name'))
                                ->where("M.to_user_id =".$user_id)
                                ->where("M.receiver_delete = 0")
                                ->where("U.user_account_status !='D' AND U.user_account_status !='I'")
                                ->order("M.message_id DESC");
                if(!empty($limit) || !empty($pagenum)) {
                    $offset = $pagenum * $limit;
                    $select->limit($limit,$offset);
                }
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getSentMessages($user_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select    = $db->select()
                                ->from(array('M'=>'tbl_user_messages'))
                                ->joinLeft(array('U' => 'tbl_users')," M.to_user_id = U.user_id ", array('firstname','lastname','username','realname' => 'CONCAT(U.firstname, " ", U.lastname)','profile_pic_path'))
                                ->joinLeft(array('L'=>'tbl_user_levels'),"L.level_no = U.user_level",array('level_name'))
                                ->where("M.from_user_id =".$user_id)
                                ->where("M.sender_delete = 0")
                                ->where("U.user_account_status !='D' AND U.user_account_status !='I'")
                                ->order("M.message_id DESC");
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getmessagesbyparentid($parent_id,$limit,$pagenum) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $db->select()
                            ->from(array('M'=>'tbl_user_messages'))
                            ->joinLeft(array('U' => 'tbl_users')," M.from_user_id = U.user_id ", array('firstname','lastname','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)','profile_pic_path'))
                            ->joinLeft(array('L'=>'tbl_user_levels'),"L.level_no = U.user_level",array('level_name'))
                            ->where("U.user_account_status !='D' AND U.user_account_status !='I'")
                            ->where("M.message_id = ?",$parent_id)
                            ->orWhere("M.parent_id = ?",$parent_id)
                            ->order("M.message_id ASC");
            if(!empty($limit) || !empty($pagenum)) {
                $offset = $pagenum * $limit;
                $select->limit($limit,$offset);
            }
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function deleteMessage($msgid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE M.*, N.* FROM tbl_user_messages M
                                    LEFT JOIN tbl_notifications N ON (N.reference_id = M.message_id AND (N.notifications_type = 'message' OR N.notifications_type = 'reply_message'))
                                    WHERE M.message_id = ".$msgid);
                $id = $select->execute();
                return $id;
        }

        public function deletemsgnotification($msgid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE N.* FROM tbl_notifications N WHERE N.reference_id = ".$msgid." AND N.notifications_type = 'message'");
                $id = $select->execute();
                return $id;
        }

        public function getquote() {
            $date = date('Y-m-d');
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('Q' => 'tbl_quotes'), array('id','quote'))
                        ->where('Q.created_date ="'.$date.'" AND Q.status = "1"');
            $resultSet = $db->fetchRow($select);
            return $resultSet;
        }

}