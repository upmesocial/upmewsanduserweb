<?php
class User_Model_Languages {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_Languages');
                }
                return $this->_dbTable;
        }

        public function get_languages() {
                $select = $this->getDbTable()->select()
                                            ->where('status = "1"');
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

}