<?php
class User_Model_Userlevels {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_Userlevels');
                }
                return $this->_dbTable;
        }

        public function getUserLevels($level) {
                $select = $this->getDbTable()->select()
                                    ->from(array('tbl_user_levels'),array('level_name','minimum_coolpoints','level_no'))
                                    ->where("level_no  =?",$level);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getEquivalentOrLowerLevelsBasedOnUserLevel($level) {
                $select = $this->getDbTable()->select()
                                    ->from(array('tbl_user_levels'),array('level_name','level_no'))
                                    ->where("level_no  <=?",$level)
                                    ->order('level_no DESC');
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getAllTypesOfUserLevels() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select1 = $db->select()
                                ->from(array('UL' => 'tbl_user_levels'), array('UL.*'));        
                $userLevelsArray = $db->fetchAll($select1);
                return $userLevelsArray;
        }

        public function getUserLevelByUserId($userId, $userType) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select1 = $db->select()
                                ->from(array('U' => 'tbl_users'), array('U.user_id','U.email','U.firstname','U.lastname','U.user_level'))
                                ->joinLeft(array('CP'=>'tbl_user_coolpoints'),'U.user_id = CP.user_id AND U.user_id = "'.$userId.'"', array('cool_points'))
                                ->where('U.user_id = "'.$userId.'"');        
                $userLevelArray = $db->fetchAll($select1);
                return $userLevelArray;
        }

        public function getUserOverAllRank($coolPoint)
        {
             $db = Zend_Db_Table::getDefaultAdapter();
             $select = $db->select()
                            ->from(array('tbl_user_coolpoints'),array('COUNT(*)+1 As rank'))
                            ->where("cool_points  > ?",$coolPoint);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function getUserRankInLevel($userLevelInfo,$userCoolPoints)
        {
            // SELECT Count(*)+1 as rank FROM tbl_user_coolpoints WHERE (cool_points BETWEEN 0 AND 140) And cool_points >29
            // SELECT Count(*)+1 as rank FROM tbl_user_coolpoints WHERE cool_points >350  And cool_points >400

            $startCoolPoints = $userLevelInfo['minimum_coolpoints'];
            if($userLevelInfo['level_no'] == 6)
            {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('tbl_user_coolpoints'),array('COUNT(*)+1 As rank'))
                            ->where("cool_points > ".$startCoolPoints." AND cool_points > ".$userCoolPoints);
                $resultSet = $db->fetchRow($select);
            }
            else
            {
                $nextlevelName = $this->getUserLevels($userLevelInfo['level_no']+1);
                $nextlevelName = $nextlevelName->toArray();
                $endCoolPoints = $nextlevelName["minimum_coolpoints"];

                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('tbl_user_coolpoints'),array('COUNT(*)+1 As rank'))
                            ->where("(cool_points BETWEEN ".$startCoolPoints." AND ".$endCoolPoints." ) And cool_points > ".$userCoolPoints);
                $resultSet = $db->fetchRow($select);
            
            }
            return $resultSet;
        }

}