<?php
class User_Model_Followers {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                       throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_Followers');
                }
                return $this->_dbTable;
        }
        
        public function getFollowedDateByID($followers_id){
             $db = Zend_Db_Table::getDefaultAdapter();
              $select = $db->select()
                                        ->from(array('F' => 'tbl_followers'), array('followers_id', 'created_date'))
                                        ->where('F.followers_id = ?',$followers_id);
              $resultSet = $db->fetchAll($select);
              return $resultSet;
        }

        //function to get all users who are following
        public function getFollowers($userId, $loggedUserId, $limit=0, $pagenum=0, $userType = 'U') {
            $db = Zend_Db_Table::getDefaultAdapter();
            if($loggedUserId == $userId) {
                $tempUserId = $loggedUserId;
            } else {
                $tempUserId = $userId;
            }
            $select = $db->select()
                    ->from(array('F' => 'tbl_followers'), array('followers_id','user_id','user_type','followers_id','follower_id', 'created_date'))
                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' AND U.user_account_status != 'D' ", array('user_id','firstname','lastname','profile_pic_path','user_level','username'))
                    ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('level_name'))
                    ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND CP.user_type = 'U'", array('cool_points'))
                    ->where('F.follower_id = ?',$tempUserId)
                    ->where('F.follower_type = ?',$userType);
            //$select->order('F.created_date DESC');
            $select->order (array('U.firstname','F.created_date DESC'));
            
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset);
            $resultSet = $db->fetchAll($select);
            $result = array();
            for($k = 0; $k < count($resultSet);$k++) { 
                        $ary['followers_id'] = $resultSet[$k]['followers_id'];
                        $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                        $ary['firstname'] = $resultSet[$k]['firstname'];
                        $ary['lastname'] = $resultSet[$k]['lastname'];
                        $ary['username'] = $resultSet[$k]['username'];
                        $ary['user_id'] = $resultSet[$k]['user_id'];
                        $ary['follower_id'] = $resultSet[$k]['follower_id'];
                        $ary['created_date'] = $resultSet[$k]['created_date'];
                        $ary['user_type'] = $resultSet[$k]['user_type'];
                        $ary['user_level'] = $resultSet[$k]['user_level'];
                        $ary['user_level_name'] = $resultSet[$k]['level_name'];
                        $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                        $ary['cool_points'] = $resultSet[$k]['cool_points'];
                        $result[] = $ary;
            }
            return $result;
        }

        //function to get all users who are following
        function getFollowing($userId,$limit=0, $pagenum=0, $userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($limit != '') {
                        $limit = $limit;
                }
                $select = $db->select()
                                ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id', 'created_date'))
                                ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','image_path','business_user_name'=>'username'))
                                ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type != 'B' AND U.user_account_status != 'D'", array('user_id','firstname','lastname','profile_pic_path', 'user_level','username'))
                                ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('level_name'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                                ->where('F.user_id = ?',$userId)
                                ->where('F.user_type = ?',$userType)
                                ->where('F.follower_type IN("U", "B")');
                $select->order (array('U.firstname','F.created_date DESC'));
                $offset = $pagenum * $limit;
                $select->limit($limit,$offset);
                //echo $select;exit;

                $resultSet = $db->fetchAll($select);
                //echo "<pre>";print_r($resultSet);exit;
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        $followerTYpe= "B";
                        if($resultSet[$k]['follower_type'] != "B") {
                                $followerTYpe= "U";
                        }
                        if($resultSet[$k]['follower_type']  == 'B') {
                                $ary['followers_id'] = $resultSet[$k]['followers_id'];
                                $ary['user_id'] = $resultSet[$k]['business_id'];  //added by laxman
                                $ary['realname'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['username'] = $resultSet[$k]['business_user_name'];
                                $ary['follower_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['created_date'] = $resultSet[$k]['created_date'];
                                $ary['user_level'] = $resultSet[$k]['user_level'];
                                $ary['user_level_name'] = $resultSet[$k]['level_name'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type']  == 'U') {
                                $ary['followers_id'] = $resultSet[$k]['followers_id'];
                                $ary['user_id'] = $resultSet[$k]['user_id'];//added by laxman
                                $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['username'] = $resultSet[$k]['username'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['created_date'] = $resultSet[$k]['created_date'];
                                $ary['user_level'] = $resultSet[$k]['user_level'];
                                $ary['user_level_name'] = $resultSet[$k]['level_name'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $ary['cool_points'] = $resultSet[$k]['cool_points'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        //function to get all users who are following
        function getAllFollowing($userId,$userType = 'U', $limit = '') {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($limit != '') {
                    $limit = $limit;
                }
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type = 'U' AND U.user_account_status != 'D'", array('user_id','firstname','lastname','profile_pic_path', 'user_level'))
                        ->joinLeft(array('S' => 'tbl_schools'), "S.school_id = F.follower_id AND F.follower_type = 'S'", array('school_id', 'school_name'))
                        ->joinLeft(array('C' => 'tbl_colleges'), "C.college_id = F.follower_id AND F.follower_type = 'C'", array('college_id', 'college_name'))
                        ->joinLeft(array('W' => 'tbl_work_place'), "W.work_place_id = F.follower_id AND F.follower_type = 'W'", array('work_place_id', 'work_place_name'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                        ->where('F.user_id = ?',$userId)
                        ->where('F.user_type = ?',$userType)
                        ->order ('F.created_date DESC')
                        ->limit($limit);               
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['follower_type']  == 'B') {
                                $ary['name'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['follower_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                /*$ary['user_level'] = $resultSet[$k]['user_level'];
                                $ary['level_name'] = $resultSet[$k]['level_name'];*/
                                if(isset($resultSet[$k]['lat']) && $resultSet[$k]['lat'] != '' && isset($resultSet[$k]['lng']) && $resultSet[$k]['lng'] != ''){
                                        $ary['blat'] = $resultSet[$k]['lat'];
                                        $ary['blng'] = $resultSet[$k]['lng'];
                                }
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type']  == 'U') {
                                $ary['name'] = $resultSet[$k]['firstname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['user_level'] = $resultSet[$k]['user_level'];
                                $ary['level_name'] = $resultSet[$k]['level_name'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $ary['cool_points'] = $resultSet[$k]['cool_points'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type'] == 'S') {
                                $ary['name'] = $resultSet[$k]['school_name'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type'] == 'C') {
                                $ary['name'] = $resultSet[$k]['college_name'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type'] == 'W') {
                                $ary['name'] = $resultSet[$k]['work_place_name'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        //function to get count all users who are following
        function getFollowersCnt($userId,$userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('followerCnt'=>'count(F.user_id)'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array(''))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' AND U.user_account_status != 'D' ", array(''))
                        ->where('F.follower_id = ?',$userId)
                        ->where('F.follower_type = ?',$userType)
                        ->order ('F.created_date DESC');
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                //echo "<pre>";print_r($resultSet);exit;
                return $resultSet['0']['followerCnt'];
        }

        //function to get count of all users who are following
        function getFollowingCnt($userId,$userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('followingCnt'=>'count(F.user_id)'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array(''))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type = 'U' AND U.user_account_status != 'D' ", array(''))
                        ->where('F.user_id = ?',$userId)
                        ->where('F.user_type = ?',$userType)
                        ->where('F.follower_type = "U" OR F.follower_type = "B"')
                        ->order ('F.created_date DESC');
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                return $resultSet['0']['followingCnt'];
        }

        public function getFollowerIds($userId,$userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' AND U.user_account_status != 'D' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.follower_id = ?',$userId)
                        ->where('F.follower_type = ?',$userType)
                        ->order ('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        public function getFollowingIds($userId,$userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type = 'U' AND U.user_account_status != 'D' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.user_id = ?',$userId)
                        ->where('F.user_type = ?',$userType)
                        ->order ('F.created_date DESC');
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['follower_type']  == 'B') {
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['user_type'] = $resultSet[$k]['follower_type'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type']  == 'U') {
                                $ary['user_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['follower_type'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        public function getFollowersAlbumImages($userId,$userType = 'U') {
                /*
                SELECT ID, date, name, type, userID, userType
                    FROM (SELECT   photo_id ID, created_date date, photo_modified_name name, 'photo' type, user_id userID, user_type userType
                    FROM tbl_photos WHERE photo_modified_name != ''
                    UNION
                    SELECT   video_id ID, created_date date, thumbnail_path name, 'video' type, user_id userID, 'U' userType
                    FROM tbl_videos WHERE thumbnail_path != ''
                    ORDER BY date DESC) AS A
                    LEFT JOIN tbl_followers F ON F.follower_id = "1" AND F.follower_type = "U" AND F.user_id = A.userID AND F.user_type = A.userType
                    WHERE F.follower_id = "1" AND F.follower_type = "U" AND F.user_id = A.userID AND F.user_type = A.userType
                    ORDER BY date DESC
                    LIMIT 30
                */
                $date = date('Y-m-d H:i:s');
                $db = Zend_Db_Table::getDefaultAdapter();
                // if($_SERVER['REMOTE_ADDR'] == '192.168.1.63' || $_SERVER['REMOTE_ADDR'] == '192.168.1.58'){
                $select = "SELECT   ID, date, name, type, userID, userType, U.username, U.profile_pic_path
                                FROM (SELECT   photo_id ID, created_date date, photo_modified_name name, 'photo' type, user_id userID, user_type userType
                                FROM tbl_photos WHERE photo_modified_name != ''
                                UNION
                                SELECT   video_id ID, created_date date, thumbnail_path name, 'video' type, user_id userID, 'U' userType
                                FROM tbl_videos WHERE thumbnail_path != ''
                                ORDER BY date DESC) AS A
                                LEFT JOIN tbl_followers F ON F.user_id = '".$userId."' AND F.user_type = '".$userType."' AND F.follower_id = A.userID AND F.follower_type = A.userType
                                LEFT JOIN tbl_users U ON U.user_id = userID
                                WHERE ((F.user_id = '".$userId."' AND F.user_type = '".$userType."'  AND A.userType != 'B' AND F.follower_id = A.userID AND F.follower_type = A.userType) OR ('".$userId."' = A.userID AND '".$userType."' = A.userType)) AND date >= '".$date."' - INTERVAL 1 DAY
                                ORDER BY date DESC
                                LIMIT 30";
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        //Insert New Record to Database
        public function insertNewRecord($data){
                return $this->getDbTable()->insert($data);
        }

        public function deletefollower($user_id,$user_type,$follower_id,$follower_type) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $where = "F.user_id = '".$user_id."' AND F.user_type = '".$user_type."' AND F.follower_id = '".$follower_id."' AND F.follower_type = '".$follower_type."'";
                //$id = $db->delete("tbl_followers", $where);
                $select = $db->query("DELETE F.*, N.* FROM tbl_followers F
                                    LEFT JOIN tbl_notifications N ON (N.reference_id = F.followers_id AND (N.notifications_type = 'Followed' OR N.notifications_type = 'following'))
                                    WHERE ".$where);
                $id = $select->execute();
                return $id;
        }
        
        public function isLoggedUserFollowingOtherUser($uid,  $userType, $loggedUserId, $loggedUserType){ 
            if($userType != 'B'){
                $userType = "U";
            }
            if($loggedUserType != 'B'){
                $loggedUserType = "U";
            }
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('F' => 'tbl_followers'), array('user_id','follower_id','user_type','follower_type', 'created_date'))
                            ->where('F.follower_id= "'.$uid.'" AND F.follower_type = "'.$userType.'" AND F.user_id  ="'.$loggedUserId.'" AND F.user_type ="'.$loggedUserType.'"');
            //echo $select;exit;
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getFriendsList($user_id,$user_type,$searchword) {
                //echo $user_id."--".$user_type."==".$searchword;exit;
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','user_type'))
                        ->joinLeft(array('U' => 'tbl_users')," (U.user_id = F.user_id AND F.user_type = 'U') OR (U.user_id = F.follower_id AND F.follower_type != 'B') AND U.user_account_status != 'D' ", array('user_id','gender','firstname','lastname','username','profile_pic_path','city'))
                        //->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','bfname'=>'firstname','blname'=>'lastname','image_path'))
                        ->where('(F.user_id = '.$user_id.' AND F.user_type = "'.$user_type.'") OR (F.follower_id = '.$user_id.' AND F.follower_type = "'.$user_type.'")')
                        //->where('F.follower_type IN("U", "B")')
                        ->where('F.follower_type = ?',"U")
                        ->where('(U.user_id != '.$user_id.')')
                        //->where('B.firstname LIKE "'.$searchword.'%" OR B.lastname LIKE "'.$searchword.'%" OR B.business_name LIKE "'.$searchword.'%" OR U.firstname LIKE "'.$searchword.'%" OR U.lastname LIKE "'.$searchword.'%"')
                        ->where('U.firstname LIKE "'.$searchword.'%" OR U.lastname LIKE "'.$searchword.'%"')
                        ->group('U.user_id')
                        ->order(array('U.firstname ASC','F.created_date DESC'))
                        ;
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                //echo '<pre>';print_R($resultSet);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['gender'] = $resultSet[$k]['gender'];
                                $ary['firstname'] = $resultSet[$k]['bfname'];
                                $ary['lastname'] = $resultSet[$k]['blname'];
                                $ary['image'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['gender'] = $resultSet[$k]['gender'];
                                $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                                $ary['username'] = $resultSet[$k]['username'];
                                $ary['city'] = $resultSet[$k]['city'];
                                $ary['image'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        function getFollowingbusiness($businessId,$businessType = 'B', $pagenum = '', $limit = '') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ")//, array('business_id','business_name','image_path','firstname','lastname')
                        ->joinLeft(array('C' => 'tbl_business_categories'), "C.id = B.category_id", array('category_name'))
                        ->where('F.user_id = ?',$businessId)
                        ->where('F.user_type = ?',$businessType)
                        ->where('F.follower_type = "B"')
                        ->order (array('B.business_name','F.created_date DESC'));
                if($limit != '' && $pagenum != ''){
                    $offset = $pagenum * $limit;
                    $select->limit($limit,$offset);
                }
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                /*$result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['follower_type']  == 'B') {
                                $ary['businessname'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['follower_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        }
                }*/
                return $resultSet;
        }
        
        public function getFollowersByUserID($userId, $userType,$limit, $notInFollowers='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.58'){
                    $select = $db->select()
                                ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                                ->where("F.user_id = '".$userId."' AND F.user_type = 'U' AND F.follower_type='U'")
                                ->orwhere("F.follower_id = '".$userId."' AND F.follower_type = 'U' AND F.user_type = 'U'");
                                if($notInFollowers != '') {
                                    $select->where('F.follower_id NOT IN('.$notInFollowers.')');
                                }
                                $select->order ('F.created_date DESC');
                                //->limit($limit);
                } else {
                     $select = $db->select()
                                ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                                //->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.user_type = 'B' ", array('business_id','business_name','image_path', 'username'))
                                ->joinLeft(array('U' => 'tbl_users')," (U.user_id = F.follower_id  AND F.user_type = 'U' AND F.follower_type = 'U')", array('user_id','firstname','lastname','gender','profile_pic_path', 'username'))
                                ->where("F.user_id = '".$userId."' AND F.user_type = 'U' AND F.follower_type='U'")
                                ->where('U.user_account_status != "D"');
                                //->orwhere("F.follower_id = '".$userId."' AND F.follower_type = 'U'");
                                if($notInFollowers != '') {
                                    $select->where('F.follower_id NOT IN('.$notInFollowers.')');
                                }
                                $select->order ('U.firstname ASC')
                                    ->limit($limit);
                }
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                $result = array();
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.58'){
                    //echo "<pre>";print_r($resultSet);exit;
                    foreach($resultSet as $key=>$usr){
                        if(!in_array($usr['user_id'], $result)){
                            //$result[]
                        }
                    }
                }else {
                    for($k = 0; $k < count($resultSet);$k++) { //echo '<pre>';print_r($resultSet);exit;
                            if($resultSet[$k]['user_type']  == 'B') {
                                    $ary['name'] = $resultSet[$k]['business_name'];
                                    $ary['firstname'] = $resultSet[$k]['business_name'];
                                    $ary['lastname'] = '';
                                    $ary['username'] = $resultSet[$k]['username'];
                                    $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                                    $ary['user_id'] = $resultSet[$k]['business_id'];
                                    $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                    $ary['user_type'] = $resultSet[$k]['user_type'];
                                    $ary['gender'] = $resultSet[$k]['gender'];
                                    $ary['image_name'] = $resultSet[$k]['image_path'];
                                    $result[] = $ary;
                            } else if($resultSet[$k]['user_type']  == 'U') {
                                    $ary['name'] = $resultSet[$k]['firstname'];
                                    $ary['username'] = $resultSet[$k]['username'];
                                    $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                                    $ary['firstname'] = $resultSet[$k]['firstname'];
                                    $ary['lastname'] = $resultSet[$k]['lastname'];
                                    $ary['user_id'] = $resultSet[$k]['user_id'];
                                     $ary['gender'] = $resultSet[$k]['gender'];
                                    $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                    $ary['user_type'] = $resultSet[$k]['user_type'];
                                    $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                    $result[] = $ary;
                            }
                    }
                }
                return $result;
        }

        function getFollowersByLevel($userId, $userlevel, $userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' AND U.user_account_status != 'D' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.follower_id = ?',$userId)
                        ->where('F.follower_type = ?',$userType)
                        ->where('U.user_level = ?',$userlevel)
                        ->order ('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) { //echo '<pre>';print_r($resultSet);exit;
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['name'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['name'] = $resultSet[$k]['firstname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        function getFollowersByDownline($userId, $userlevel, $userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' AND U.user_account_status != 'D' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.follower_id = ?',$userId)
                        ->where('F.follower_type = ?',$userType)
                        ->where('U.user_level < ?',$userlevel)
                        ->order ('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) { //echo '<pre>';print_r($resultSet);exit;
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['name'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['name'] = $resultSet[$k]['firstname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        // GEt user Following Ids
        function getFollowingIdsByUserID($userId,$limit=0, $pagenum=0, $userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($limit != ''){
                        $limit = $limit;
                }
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array(''))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type != 'B'", array(''))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array(''))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array(''))
                        ->where('F.user_id = ?',$userId)
                        ->where('F.user_type = ?',$userType)
                        ->where('U.user_account_status != "D"')
                        ->where("F.follower_type IN('U')");
            $select->order (array('U.firstname','F.created_date DESC'));
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset);
            
            //echo $select;exit;
            $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        //function to get all users who are following
        public function getMyFollowers($userId, $loggedUserId, $limit, $pagenum, $userType = 'U') {
            $db = Zend_Db_Table::getDefaultAdapter();
            if($loggedUserId == $userId) {
                $tempUserId = $loggedUserId;
            } else {
                $tempUserId = $userId;
            }

            $select = $db->select()
                    ->from(array('F' => 'tbl_followers'), array('followers_id','user_id','user_type','followers_id','follower_id', 'created_date'))
                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' AND U.user_account_status != 'D'", array('gender','user_id','firstname','lastname','profile_pic_path','user_level','username','city'))
                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path', 'name' => 'IF(F.follower_type="B", B.business_name, U.firstname)', 'business_user_name'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname','business_city'=>'city'))
                    ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('level_name'))
                    ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                    ->joinLeft(array('CT' => 'tbl_city'),"CT.id = U.city",array('city_name'))
                    ->where('F.follower_id = ?',$tempUserId)
                    ->where('F.follower_type = ?',$userType);
            $select->order(array('U.firstname','F.created_date DESC'));

            $offset = $pagenum * $limit;
            $select->limit($limit,$offset);
            //echo $select;exit;
            $resultSet = $db->fetchAll($select);
            $result = array();
            for($k = 0; $k < count($resultSet);$k++) { 
                if($resultSet[$k]['user_type']  == 'B') {
                        $ary['name'] = $resultSet[$k]['business_name'];
                        $ary['followers_id'] = $resultSet[$k]['followers_id'];
                        $ary['realname'] = $resultSet[$k]['business_name'];
                        $ary['username'] = $resultSet[$k]['business_user_name'];
                        $ary['firstname'] = $resultSet[$k]['business_firstname'];
                        $ary['lastname'] = $resultSet[$k]['business_lastname'];
                        $ary['user_id'] = $resultSet[$k]['business_id'];
                        $ary['gender'] = $resultSet[$k]['gender'];
                        $ary['follower_id'] = $resultSet[$k]['follower_id'];
                        $ary['created_date'] = $resultSet[$k]['created_date'];
                        $ary['user_type'] = $resultSet[$k]['user_type'];
                        $ary['user_level'] = " ";
                        $ary['user_level_name'] = " ";
                        $ary['city'] = $resultSet[$k]['business_city'];
                        $ary['city_name'] = $resultSet[$k]['city_name'];
                        $ary['image_name'] = $resultSet[$k]['image_path'];                                
                        $result[] = $ary;

                } elseif($resultSet[$k]['user_type']  == 'U') {
                    //echo "<PRE>";print_r($resultSet[$k]);exit;
                        $ary['followers_id'] = $resultSet[$k]['followers_id'];
                        $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                        $ary['firstname'] = $resultSet[$k]['firstname'];
                        $ary['lastname'] = $resultSet[$k]['lastname'];
                        $ary['username'] = $resultSet[$k]['username'];
                        $ary['user_id'] = $resultSet[$k]['user_id'];
                        $ary['gender'] = $resultSet[$k]['gender'];
                        $ary['follower_id'] = $resultSet[$k]['follower_id'];
                        $ary['created_date'] = $resultSet[$k]['created_date'];
                        $ary['user_type'] = $resultSet[$k]['user_type'];
                        $ary['user_level'] = $resultSet[$k]['user_level'];
                        $ary['user_level_name'] = $resultSet[$k]['level_name'];
                        $ary['city'] = $resultSet[$k]['city'];
                        $ary['city_name'] = $resultSet[$k]['city_name'];
                        $ary['image_name'] = $resultSet[$k]['profile_pic_path'  ];
                        $ary['cool_points'] = $resultSet[$k]['cool_points'];
                        $result[] = $ary;
                }          
            }
            //echo "<PRE>";print_r($result);exit;
            return $result;
        }

        //function to get all users who are following
        function getMyFollowing($userId,$limit, $pagenum, $userType = 'U') {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id', 'created_date'))
                        ->joinLeft(array('B' => 'tbl_business_users'),"B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','business_email'=>'email', 'name' => 'IF(F.follower_type="B", B.business_name, U.firstname)','image_path','business_user_name'=>'username','business_city'=>'city'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type != 'B' AND U.user_account_status != 'D'", array('gender','user_id','firstname','lastname','email','profile_pic_path', 'user_level','username','city'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                        ->where('F.user_id = ?',$userId)
                        ->where('F.user_type = ?',$userType)
                        ->where('F.follower_type IN("U", "B")');
            $select->order(array('name ASC','U.firstname ASC','B.business_name ASC','F.created_date DESC'));
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset);
            //echo $select;exit;

            $resultSet = $db->fetchAll($select);
            //echo "<pre>";print_r($resultSet);exit;
            $result = array();
            for($k = 0; $k < count($resultSet);$k++) {
                $followerTYpe= "B";
                 if($resultSet[$k]['follower_type'] != "B"){
                     $followerTYpe= "U";
                 }
                 if($resultSet[$k]['follower_type']  == 'U') {
                         $ary['followers_id'] = $resultSet[$k]['followers_id'];
                         $ary['user_id'] = $resultSet[$k]['user_id'];
                         $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                         $ary['firstname'] = $resultSet[$k]['firstname'];
                         $ary['lastname'] = $resultSet[$k]['lastname'];
                         $ary['email'] = $resultSet[$k]['email'];
                         $ary['username'] = $resultSet[$k]['username'];
                         $ary['follower_id'] = $resultSet[$k]['follower_id'];
                         $ary['gender'] = $resultSet[$k]['gender'];
                         $ary['follower_type'] = $resultSet[$k]['follower_type'];
                         $ary['created_date'] = $resultSet[$k]['created_date'];
                         $ary['user_level'] = $resultSet[$k]['user_level'];
                         $ary['user_level_name'] = $resultSet[$k]['level_name'];
                         $ary['city'] = $resultSet[$k]['city'];
                         $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                         $ary['cool_points'] = $resultSet[$k]['cool_points'];
                         $result[] = $ary;
                 }
                 if($resultSet[$k]['follower_type']  == 'B') {
                         $ary['followers_id'] = $resultSet[$k]['followers_id'];
                         $ary['user_id'] = $resultSet[$k]['business_id'];
                         $ary['realname'] = $resultSet[$k]['business_name'];
                         $ary['firstname'] = $resultSet[$k]['business_name'];
                         $ary['lastname'] = '';
                         $ary['email'] = $resultSet[$k]['business_email'];
                         $ary['username'] = $resultSet[$k]['business_user_name'];
                         $ary['follower_id'] = $resultSet[$k]['business_id'];
                         $ary['gender'] = $resultSet[$k]['gender'];
                         $ary['follower_type'] = $resultSet[$k]['follower_type'];
                         $ary['created_date'] = $resultSet[$k]['created_date'];
                         $ary['user_level'] = $resultSet[$k]['user_level'];
                         $ary['user_level_name'] = $resultSet[$k]['level_name'];
                         $ary['city'] = $resultSet[$k]['business_city'];
                         $ary['image_name'] = $resultSet[$k]['image_path'];
                         $result[] = $ary;
                 }
             }
            // For Sorting Array which is mixed with business and User details by realname Asc ORDER
            $b = array();
            $c = array();
             foreach($result as $k=>$v) {
                $b[$k] = strtolower($v['realname']);
            }
            asort($b);
            foreach($b as $key=>$val) {
                $c[] = $result[$key];
            }

            return $c;
        }

        //function to get all users who are following
        function getFollowingUsersByLoggedUser($userId,$limit='10', $pagenum='0', $userType = 'U') {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id', 'created_date'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type != 'B' AND U.user_account_status != 'D'", array('user_id','firstname','lastname','email','profile_pic_path', 'user_level','username'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                        ->where('F.user_id = ?',$userId)
                        ->where('F.user_type = ?',$userType)
                        ->where('F.follower_type IN("U", "B")');
            $select->order(array('U.firstname ASC','F.created_date DESC'));

            $resultSet = $db->fetchAll($select);
            //echo "<pre>";print_r($resultSet);exit;
            $result = array();
            for($k = 0; $k < count($resultSet);$k++) {
                $followerTYpe= "B";
                if($resultSet[$k]['follower_type']  == 'U') {
                         $ary['followers_id'] = $resultSet[$k]['followers_id'];
                         $ary['user_id'] = $resultSet[$k]['user_id'];
                         $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                         $ary['firstname'] = $resultSet[$k]['firstname'];
                         $ary['lastname'] = $resultSet[$k]['lastname'];
                         $ary['email'] = $resultSet[$k]['email'];
                         $ary['username'] = $resultSet[$k]['username'];
                         $ary['follower_id'] = $resultSet[$k]['follower_id'];
                         $ary['follower_type'] = $resultSet[$k]['follower_type'];
                         $ary['created_date'] = $resultSet[$k]['created_date'];
                         $ary['user_level'] = $resultSet[$k]['user_level'];
                         $ary['user_level_name'] = $resultSet[$k]['level_name'];
                         $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                         $ary['cool_points'] = $resultSet[$k]['cool_points'];
                         $result[] = $ary;
                 }
             }
            // FOr Sorting Array which is mixed with business and User details by realname Asc ORDER
            $b = array();
            $c = array();
             foreach($result as $k=>$v) {
                $b[$k] = strtolower($v['realname']);
            }
            asort($b);
            foreach($b as $key=>$val) {
                $c[] = $result[$key];
            }

            return $c;
        }
        
        
        
        //Novasys Code 
        
        //function to get all users who are following
        public function getAllMyFollowersUsingUserId($userId, $limit, $pagenum, $userType = 'U', $searchKey) {
            $db = Zend_Db_Table::getDefaultAdapter();

                  $select = $db->select()
                    ->from(array('F' => 'tbl_followers'), array('followers_id','user_id','user_type','followers_id','follower_id', 'created_date'))
                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' AND U.user_account_status != 'D' AND (U.username like '".$searchKey."%'  OR U.firstname like '".$searchKey."%' OR U.lastname like '".$searchKey."%')", array('user_id','firstname','lastname','profile_pic_path','user_level','username','city'))
                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path', 'name' => 'IF(F.follower_type="B", B.business_name, U.firstname)', 'business_user_name'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname','business_city'=>'city'))
                    ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('level_name'))
                    ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                    ->joinLeft(array('CT' => 'tbl_city'),"CT.id = U.city",array('city_name'))
                    ->where('F.follower_id = ?',$userId)
                    ->where('F.follower_type = ?',$userType);
            $select->order(array('U.firstname','F.created_date DESC'));

            // $offset = $pagenum * $limit;
            // $select->limit($limit);
            //echo $select;exit;
            $resultSet = $db->fetchAll($select);
            $result = array();                 
                
                for($k = 0; $k < count($resultSet);$k++) { 
                $realname = $resultSet[$k]['firstname']."".$resultSet[$k]['lastname'];
                if($realname != '')
                {
                
                if($resultSet[$k]['user_type']  == 'B') {
                        $ary['name'] = $resultSet[$k]['business_name'];
                        $ary['followers_id'] = $resultSet[$k]['followers_id'];
                        $ary['realname'] = $resultSet[$k]['business_name'];
                        $ary['username'] = $resultSet[$k]['business_user_name'];
                        $ary['firstname'] = $resultSet[$k]['business_firstname'];
                        $ary['lastname'] = $resultSet[$k]['business_lastname'];
                        $ary['user_id'] = $resultSet[$k]['business_id'];
                        $ary['follower_id'] = $resultSet[$k]['follower_id'];
                        $ary['created_date'] = $resultSet[$k]['created_date'];
                        $ary['user_type'] = $resultSet[$k]['user_type'];
                        $ary['user_level'] = " ";
                        $ary['user_level_name'] = " ";
                        $ary['city'] = $resultSet[$k]['business_city'];
                        $ary['city_name'] = $resultSet[$k]['city_name'];
                        $ary['image_name'] = $resultSet[$k]['image_path'];                                
                        $result[] = $ary;

                } elseif($resultSet[$k]['user_type']  == 'U') {
                    //echo "<PRE>";print_r($resultSet[$k]);exit;
                        $ary['followers_id'] = $resultSet[$k]['followers_id'];
                        $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                        $ary['firstname'] = $resultSet[$k]['firstname'];
                        $ary['lastname'] = $resultSet[$k]['lastname'];
                        $ary['username'] = $resultSet[$k]['username'];
                        $ary['user_id'] = $resultSet[$k]['user_id'];
                        $ary['follower_id'] = $resultSet[$k]['follower_id'];
                        $ary['created_date'] = $resultSet[$k]['created_date'];
                        $ary['user_type'] = $resultSet[$k]['user_type'];
                        $ary['user_level'] = $resultSet[$k]['user_level'];
                        $ary['user_level_name'] = $resultSet[$k]['level_name'];
                        $ary['city'] = $resultSet[$k]['city'];
                        $ary['city_name'] = $resultSet[$k]['city_name'];
                        $ary['image_name'] = $resultSet[$k]['profile_pic_path'  ];
                        $ary['cool_points'] = $resultSet[$k]['cool_points'];
                        $result[] = $ary;
                    }
                }       
            }
            //echo "<PRE>";print_r($result);exit;
            return $result;
                
            }   

}