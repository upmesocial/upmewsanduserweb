<?php
class User_Model_States {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_States');
                }
                return $this->_dbTable;
        }

        public function get_states($id, $limit='') {
                $select = $this->getDbTable()->select()
                                            ->where('country_id ="'.$id.'" AND status = "1"');
                if($limit != '')
                    $select->limit($limit);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }
        
        public function getStateNameByStateID($state_id) {
            $select = $this->getDbTable()->select()
                                        ->where('id = "'.$state_id.'" AND status = "1"');
            $resultSet = $this->getDbTable()->fetchAll($select);
            return $resultSet;
        }

        public function getStateByName($state) {
            $select = $this->getDbTable()->select()
                                        ->where('state_name = "'.$state.'"  AND status = "1"');
            $resultSet = $this->getDbTable()->fetchRow($select);
            return $resultSet;
        }

        public function insert($data) {
            $insId = $this->getDbTable()->insert($data);
            return $insId;
        }

        public function getstatesbykeyword($limit,$keyword,$countryId) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('S'=>'tbl_state'), array('state_name','id'))
                            ->where('S.state_name LIKE "'.addslashes($keyword).'%" AND S.country_id = "'.$countryId.'" AND S.status="1"')
                            ->limit($limit);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

}