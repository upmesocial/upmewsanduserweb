<?php class User_Model_User {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {  
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_Users');
                }
                return $this->_dbTable;
        }

        //function to get all user details
        public function getUserDetails($userId) {
                $select = $this->getDbTable()->select()
                                        ->from(array('U'=>'tbl_users'), array('U.*'))
                                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'))
                                        ->where('U.user_id = ?',$userId)
                                        ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getAllUserDetails() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                        ->from($this->getDbTable(), array('user_id','user_type','profile_pic_path'));
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getAllUserEmails() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                        ->from($this->getDbTable(), array('firstname','email'));
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        //function to get all user details by Username
        public function getUserDetailsByname($uName) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('U'=>'tbl_users'))
                                ->joinLeft(array('UL' => 'tbl_user_levels'), "UL.id = U.user_level", array('user_level_name' => 'level_name'))
                                ->where('U.username = ?',$uName);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        //function to get Facebook user details
        public function getFbUserDetails($fbUniqueId, $email) {
                $select = $this->getDbTable()->select()
                            ->where('facebook_unique_id = "'.$fbUniqueId.'" AND email="'.$email.'"');
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        //function to insert user details
        public function insertNewUser($data) {//echo 'ins<pre>';print_r($data);exit;
                $insId = $this->getDbTable()->insert($data);
                //insert details in openfire database
                $db2 = Zend_Registry::get('db2');
                $opdata = array('username' => $insId,'encryptedPassword' => OF_PWD, 'name' => $data['username'], 'email' => $data['email'],'creationDate'=> '001356021370133', 'modificationDate' => '001356021370133' );
                $db2->insert('ofUser',$opdata);
                return $insId;
        }

        //function to check user existancy
        public function isUserAvail($userId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from($this->getDbTable(), 'COUNT(user_id)')
                                        ->where('user_id = ?',$userId);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        //function to check username existancy
        public function isUsernameAvail($username,$userId='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from($this->getDbTable(), 'COUNT(user_id)')
                                        ->where('username = ?',$username);
                if($userId != '')
                        $select->where('user_id != ?',$userId);	

                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        //function to check email existancy
        public function isEmailAvail($email,$userId = '') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                    ->from($this->getDbTable(), 'COUNT(user_id)')
                                    ->where('email = ?',$email);
                if($userId != '')
                        $select->where('user_id != ?',$userId);	

                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        public function updateUser($updAry, $uid) {
                if(!empty($uid) && $uid != '0') {
                    try{
                            $res = $this->getDbTable()->update($updAry, array('user_id= ?' => $uid));
                            if (false === $res) {
                                return 0;  // bool false returned, query failed
                            } else {
                                return 1;
                            }
                    } catch (Zend_Exception $zex){}
                }
        }

        public function checkemailexistsrnot($data) {
                $select = $this->getDbTable()->select()
                                ->where('email = ?',$data['email'])
                                ->where('facebook_unique_id = ?',$data['unique_id']);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function duplicatePasswordCheck($id, $password) {
                $password   = md5($password.SECURITY_SALT);
                $select     = $this->getDbTable()->select()
                                    ->from(array('tbl_users'),array('count(user_id) as Cnt'))
                                    ->where("user_id  =?",$id)
                                    ->where("password =?",$password);
                $resultSet  = $this->getDbTable()->fetchRow($select);
                return $resultSet['Cnt'];
        }

        public function insertcoolpoints($data) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $ins = $db->insert('tbl_user_coolpoints',$data);
                return $ins;
        }

        public function getUserCoolPoints($user_id, $userType) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('tbl_user_coolpoints'), array('cool_points'))
                                ->where('user_id ="'.$user_id.'" AND user_type = "'.$userType.'"');
                $coolPoints = $db->fetchOne($select);
                if($coolPoints == '')
                    $coolPoints = 0;
                return $coolPoints;
        }

        public function upgradeLevel($uid, $uLevel) {
                if(!empty($uid) && $uid != '0') {
                        $updAry['user_level'] = $uLevel;
                        try {
                                $res = $this->getDbTable()->update($updAry, array('user_id= ?' => $uid));
                                if (false === $res) {
                                        return 0;  // bool false returned, query failed
                                } else {
                                        return $res;
                                }
                        } catch (Zend_Exception $zex){}
                }
        }

        public function getfirstname($user_id, $userType='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('tbl_users'), array('firstname'))
                            ->where('user_id ="'.$user_id.'"');
                $fname = $db->fetchOne($select);
                return $fname;
        }

        public function getPopularUser() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $where = "(U.user_account_status !='D' AND U.user_account_status !='I')";
                $select = $db->select()
                                ->from(array('U'=>'tbl_users'), array('user_id','firstname','lastname','profile_pic_path','user_type','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'user_level', 'city'))
                                ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'));
                $select->where($where);
                $select->order(array('CP.cool_points DESC', 'U.firstname ASC'));
                $popularArray = $db->fetchRow($select);
                return $popularArray;
        }

        public function getPopularBusiness() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('B'=>'tbl_business_users'), array('business_id','business_name','firstname','lastname','image_path','user_type','username', 'realname' => 'CONCAT(B.firstname, " ", B.lastname)'))
                                ->joinLeft(array('F' => 'tbl_followers'), "B.business_id = F.follower_id AND F.follower_type = 'B'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->group('B.business_id')
                                ->order('followerscnt DESC')
                                ->order('B.business_name ASC');
                $popularArray = $db->fetchRow($select);
                return $popularArray;
        }

        public function isSchoolAvail($schoolId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('S' => 'tbl_schools'), 'COUNT(S.school_id)')
                                ->where('S.school_id = ?',$schoolId);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        public function getSchools($order='', $limit='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('S'=>'tbl_schools'))
                                        ->where('S.status = 1');
                if($order != ''){
                    $select->order(array($order));
                }
                if($limit != '')
                    $select->limit($limit);
                else
                    $select->limit(50);
                $select->setIntegrityCheck(false);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getSchoolFollowersCnt($schoolid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('S'=>'tbl_schools'),array('S.popular_order'))
                                        ->where('S.status = 1')
                                        ->where('S.school_id = ?', $schoolid)
                                        ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function getschooldata($schoolid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('S'=>'tbl_schools'))
                                        ->joinLeft(array('F' => 'tbl_followers'), "S.school_id = F.follower_id AND F.follower_type = 'S'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                        ->where('S.status = 1')
                                        ->where('S.school_id = ?', $schoolid)
                                        ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function updateschool($schoolAry, $schoolid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                if(!empty($schoolid) && $schoolid != '0') {
                        try {
                                $res = $db->update('tbl_schools',$schoolAry, array('school_id= ?' => $schoolid));
                                if (false === $res) {
                                        return 0;
                                } else {
                                        return $res;
                                }
                        } catch (Zend_Exception $zex){} 
                }
        }

        public function popularschool() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('S' => 'tbl_schools'),array('school_id','school_name','popular_order'))
                                ->joinLeft(array('F' => 'tbl_followers'), "S.school_id = F.follower_id AND F.follower_type = 'S'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->group('S.school_id')
                                ->order('followerscnt DESC')
                                ->order('S.school_name ASC');
                $resultset = $db->fetchRow($select);
                return $resultset;
        }

        public function getpopularschools() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('S' => 'tbl_schools'),array('school_id','school_name','popular_order'))
                                ->joinLeft(array('F' => 'tbl_followers'), "S.school_id = F.follower_id AND F.follower_type = 'S'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->group('S.school_id')
                                ->order('followerscnt DESC')
                                ->order('S.school_name ASC');
                $resultset = $db->fetchAll($select);
                return $resultset;
        }

        public function isUserFollowingAcademy($follower_id,$follower_type,$user_id,$user_type) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('F' => 'tbl_followers'), 'COUNT(F.followers_id)')
                                ->where('F.user_id = ?',$user_id)
                                ->where('F.user_type = ?',$user_type)
                                ->where('F.follower_id = ?',$follower_id)
                                ->where('F.follower_type = ?',$follower_type);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        public function isCollegeAvail($collegeId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C' => 'tbl_colleges'), 'COUNT(C.college_id)')
                                ->where('C.college_id = ?',$collegeId);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        public function getColleges($order='C.popular_order DESC', $limit='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('C'=>'tbl_colleges'))
                                        ->where('C.status = 1');
                if($order != '') {
                    $select->order(array($order));
                }
                if($limit != '')
                    $select->limit($limit);
                else
                    $select->limit(50);
                 $select->setIntegrityCheck(false);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getCollegeFollowersCnt($collegeid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('C'=>'tbl_colleges'),array('C.popular_order'))
                                        ->where('C.status = 1')
                                        ->where('C.college_id = ?', $collegeid)
                                        ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function getcollegedata($collegeid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('C'=>'tbl_colleges'))
                                        ->joinLeft(array('F' => 'tbl_followers'), "C.college_id = F.follower_id AND F.follower_type = 'C'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                        ->where('C.status = 1')
                                        ->where('C.college_id = ?', $collegeid)
                                        ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function updatecollege($collegeAry, $collegeid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                if(!empty($collegeid) && $collegeid != '0') {
                        try {
                                $res = $db->update('tbl_colleges',$collegeAry, array('college_id= ?' => $collegeid));
                                if (false === $res) {
                                        return 0;
                                } else {
                                        return $res;
                                }
                        } catch (Zend_Exception $zex){} 
                }
        }

        public function popularcollege() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C' => 'tbl_colleges'),array('college_id','college_name','popular_order'))
                                ->joinLeft(array('F' => 'tbl_followers'), "C.college_id = F.follower_id AND F.follower_type = 'C'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->group('C.college_id')
                                ->order('followerscnt DESC')
                                ->order('C.college_name ASC');
                $resultset = $db->fetchRow($select);
                return $resultset;
        }

        public function getpopularcolleges() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C' => 'tbl_colleges'),array('college_id','college_name','popular_order'))
                                ->joinLeft(array('F' => 'tbl_followers'), "C.college_id = F.follower_id AND F.follower_type = 'C'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->group('C.college_id')
                                ->order('followerscnt DESC')
                                ->order('C.college_name ASC');
                $resultset = $db->fetchAll($select);
                return $resultset;
        }

        public function isWorkPlaceAvail($workplaceId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('W' => 'tbl_work_place'), 'COUNT(W.work_place_id)')
                                ->where('W.work_place_id = ?',$workplaceId);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        public function getWorkPlaces($limit='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('W'=>'tbl_work_place'))
                                        ->where('W.status = 1')
                                        ->setIntegrityCheck(false);
                if($limit != '')
                    $select->limit($limit);
                else
                    $select->limit(50);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getWorkPlaceFollowersCnt($workplaceid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('W'=>'tbl_work_place'),array('W.popular_order'))
                                        ->where('W.status = 1')
                                        ->where('W.work_place_id = ?', $workplaceid)
                                        ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function getWorkPlacedata($workplaceid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('W'=>'tbl_work_place'))
                                        ->joinLeft(array('F' => 'tbl_followers'), "W.work_place_id = F.follower_id AND F.follower_type = 'W'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                        ->where('W.status = 1')
                                        ->where('W.work_place_id = ?', $workplaceid)
                                        ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function updateworkplace($workplaceAry, $workplaceid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                if(!empty($workplaceid) && $workplaceid != '0') {
                        try {
                                $res = $db->update('tbl_work_place',$workplaceAry, array('work_place_id= ?' => $workplaceid));
                                if (false === $res) {
                                        return 0;
                                } else {
                                        return $res;
                                }
                        } catch (Zend_Exception $zex){} 
                }
        }

        public function popularworkplace() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('W' => 'tbl_work_place'),array('work_place_id','work_place_name','popular_order'))
                                ->joinLeft(array('F' => 'tbl_followers'), "W.work_place_id = F.follower_id AND F.follower_type = 'W'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->group('W.work_place_id')
                                ->order('followerscnt DESC')
                                ->order('W.work_place_name ASC');
                $resultset = $db->fetchRow($select);
                return $resultset;
        }

        public function getpopularworkplaces() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('W' => 'tbl_work_place'),array('work_place_id','work_place_name','popular_order'))
                                ->joinLeft(array('F' => 'tbl_followers'), "W.work_place_id = F.follower_id AND F.follower_type = 'W'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->group('W.work_place_id')
                                ->order('followerscnt DESC')
                                ->order('W.work_place_name ASC');
                $resultset = $db->fetchAll($select);
                return $resultset;
        }

        public function getUserDetailsByEmails($emails) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('U' => 'tbl_users'),array('user_id','firstname','lastname','profile_pic_path','user_type','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'user_level', 'email'))
                                ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'))
                                ->where('email IN ('.$emails.')');
                $resultset = $db->fetchAll($select);
                return $resultset;
        }

        public function getUserEmailDetiailsByEmails($emails) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('U' => 'tbl_users'),array('email'))
                                ->where('email IN ('.$emails.')');
                $resultset = $db->fetchAll($select);
                return $resultset;
        }

        public function insertInvitingUserEmails($emailsArray, $loggd_user_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                foreach($emailsArray as $email) {
                        //  check if the user with Email is already invited
                        $isAlreadyInvited = $this->checkIfAlreadyInvited($email);
                        if($isAlreadyInvited == '0') {
                                $data = array();
                                $data['user_id'] = $loggd_user_id;
                                $data['email'] = $email;
                                $data['invited_on'] = date('Y-m-d');
                                $ins = $db->insert('tbl_invite_friends',$data);
                        }
                        $resultset[] = $email;
                }
                return $resultset;
        }

        public function checkIfAlreadyInvited($email){
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('UI'=>'tbl_invite_friends'), array('email'))
                            ->where("email='".$email."'");
                $resultset = $db->fetchAll($select);
                if(!empty($resultset))
                        return "1";
                return "0";
        }

        public function unsubscribe($data){
                $db = Zend_Db_Table::getDefaultAdapter();
                //  check if the user with Email is already Unsubscribed
                $isAlreadyunsubscribed = $this->checkIfAlreadyUnsubscribed($data['email']);
                $ins = 0;
                if($isAlreadyunsubscribed == '0') {
                        $usrdata = array();
                        $usrdata['user_id'] = $data['user_id'];
                        $usrdata['email'] = $data['email'];
                        $usrdata['action_date'] = date('Y-m-d');
                        $ins = $db->insert('tbl_unsubscribed_users',$usrdata);
                }
                return $ins;
        }

        public function checkIfAlreadyUnsubscribed($email){
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('US'=>'tbl_unsubscribed_users'), array('email'))
                            ->where("email='".$email."'");
                $resultset = $db->fetchAll($select);
                if(!empty($resultset))
                        return "1";
                return "0";
        }

        public function activateuser($ml) {
                $updAry = array('user_account_status' => 'A');
                if(!empty($ml) && $ml != '0') {
                        try{
                                $res = $this->getDbTable()->update($updAry, array('activationcode= ?' => $ml));
                                if (false === $res) {
                                        return 0;  // bool false returned, query failed
                                } else {
                                        return 1;
                                }
                        } catch (Zend_Exception $zex){} 
                }
        }

        public function updatePasswordByEmail($updAry, $email) {
                $updAry['password'] = md5($updAry['password'].SECURITY_SALT);
                if(!empty($email)) {
                    try{
                            $res = $this->getDbTable()->update($updAry, array('email= ?' => $email));
                            if (false === $res) {
                                return 0;  // bool false returned, query failed
                            } else {
                                return 1;
                            }
                        } catch (Zend_Exception $zex){} 
                }
        }

        public function getuserdatabyemail($email) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from('tbl_users',array('user_id','firstname','lastname','username'))
                                ->where('email = ?',$email);
                $resultset = $db->fetchRow($select);
                return $resultset;
        }

        public function getOpenfireUserRecords(){
                $db2 = Zend_Registry::get('db2');
                $select = $db2->select()
                                ->from('ofUser',array('name','email','username'))
                                ->limit(1);
                $resultset = $db2->fetchRow($select);
                return $resultset;
        }

        public function InsertNewCollogeData($data){
                $db = Zend_Db_Table::getDefaultAdapter();
                foreach($data as $arr){
                    $db->insert('tbl_college_imported_data',$arr);
                }
                return true;
        }

        public function InsertNewSchoolData($data){
                $db = Zend_Db_Table::getDefaultAdapter();
                foreach($data as $arr){
                    $db->insert('tbl_school_imported_data',$arr);
                }
                return true;
        }
        
        public function importSchoolDataFromImportedToMainTable(){
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from('tbl_school_imported_data',array('PSS_INST','PSS_CITY'));
                $resultset = $db->fetchAll($select);
                if(!empty($resultset)){
                    foreach($resultset as $arr){
                        //echo "<pre>";print_r($arr);exit;
                        $tempArr = array();
                        $tempArr['school_name'] = $arr['PSS_INST'];
                        $tempArr['school_city'] = $arr['PSS_CITY'];
                        $db->insert('tbl_schools',$tempArr);
                    }
                    return true;
                }
                return false;
                
        }
        
        public function importCollegeDataFromImportedToMainTable(){
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from('tbl_college_imported_data',array('name'));
                $resultset = $db->fetchAll($select);
                if(!empty($resultset)){
                    foreach($resultset as $arr){
                        //echo "<pre>";print_r($arr);exit;
                        $tempArr = array();
                        $tempArr['college_name'] = $arr['name'];
                        $db->insert('tbl_colleges',$tempArr);
                    }
                    return true;
                }
                return false;
                
        }

        public function getschoolsbykeyword($limit, $keyword) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('S'=>'tbl_schools'), array('school_name','school_id'))
                            ->where('S.school_name LIKE "'.addslashes($keyword).'%"')
                            ->limit($limit);
            //echo $select;exit;
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getcollegesbykeyword($limit, $keyword) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('C'=>'tbl_colleges'), array('college_name','college_id'))
                            ->where('C.college_name LIKE "'.addslashes($keyword).'%"')
                            ->limit($limit);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getworkplacesbykeyword($limit, $keyword) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('W'=>'tbl_work_place'), array('work_place_name','work_place_id'))
                            ->where('W.work_place_name LIKE "'.addslashes($keyword).'%"')
                            ->limit($limit);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }
        
        //function to get all user details
        public function getUserDataByUserId($userId) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('U'=>'tbl_users'), array('U.user_id', 'U.user_level'))
                        ->where('U.user_id IN ('.$userId.')');
            $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getUserDataByUserIds($userId) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('U'=>'tbl_users'), array('U.*'))
                        ->joinInner(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'))
                        ->joinInner(array('UL' => 'tbl_user_levels')," U.user_level = UL.level_no", array('level_name'))
                        ->where('U.user_id IN ('.$userId.')');
            $resultSet = $db->fetchAll($select);
                return $resultSet;
        }
        
        public function getBlastedUserDataByUserIds($userIds) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('U'=>'tbl_users'), array('U.user_id', 'U.username'))
                        ->where('U.user_id IN ('.$userIds.')');
            $resultSet = $db->fetchAll($select);
                return $resultSet;
        }
        
        public function insertCeleb($user_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $data['user_id'] = $user_id;
            $ins = $db->insert('tbl_user_celeb_temp',$data);
            return $ins;
        }
        
        public function deleteCelebTemp($user_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $where = "user_id='".$user_id."'";
            $db->delete('tbl_user_celeb_temp',$where);
            return true;
        }
        
        public function getUserByPushNotificationId($push_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('U'=>'tbl_users'), array('U.user_id'))
                        ->where("U.push_unique_token = '".$push_id."'");
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }
        
        public function updatePushIdByUserId($uids, $updAry){
            $where = "user_id IN (".$uids.")";
            $db = Zend_Db_Table::getDefaultAdapter();
            try{
                    $res = $db->update('tbl_users', $updAry, $where);
                    return $res;
            } catch (Zend_Exception $zex){} 
        }

        public function getuniquecode($userid) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('U'=>'tbl_users'), array('U.push_unique_token','U.os_type'))
                        ->where('U.user_id =?',$userid);
            $resultSet = $db->fetchRow($select);
            return $resultSet;
        }

        public function get_schools() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('S'=>'tbl_schools'))
                                ->where('S.status = 1');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function get_colleges() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C'=>'tbl_colleges'))
                                ->where('C.status = 1');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function get_workplaces() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('W'=>'tbl_work_place'))
                                ->where('W.status = 1');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getusersettings($userId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from(array('U'=>'tbl_users'), array('U.user_id','U.user_language','U.user_timezone','U.user_account_status','U.privacy','U.location'))
                                        ->where('U.user_id = ?',$userId)
                                        ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function insertleveldetails($data) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $res = $db->insert('tbl_level_details',$data);
                return $res;
        }

        public function updateleveldetails($data,$user_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $res = $db->update('tbl_level_details', $data, array('user_id= ?' => $user_id));
                return $res;
        }

        public function isuserinleveldetails($user_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                        ->from('tbl_level_details',array('id','level_status'))
                                        ->where('user_id = ?',$user_id);
                $data = $db->fetchRow($select);
                return $data;
        }

        public function deleteleveldetail($id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $where = "id='".$id."'";
                $db->delete('tbl_level_details',$where);
                return true;
        }

         //function to get all user details
        public function getUserDetailsForStats($userId) {
            // ->joinLeft(array('SC' => 'tbl_schools'),"U.high_school_info = SC.school_id",array('school_name','school_id','U.updated_date'))
                $select = $this->getDbTable()->select()
                                        ->from(array('U'=>'tbl_users'), array('U.user_id','U.username','U.firstname','U.lastname','U.high_school_info','U.college_info','U.employer_info','U.user_level','U.created_date','U.updated_date'))
                                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'))
                                        ->joinLeft(array('CT' => 'tbl_city'),"U.city = CT.id",array('city_name'))
                                        ->joinLeft(array('ST' => 'tbl_state'),"U.state = ST.id",array('state_name'))
                                        ->joinLeft(array('CN' => 'tbl_country'),"U.country = CN.id",array('country_name'))
                                        ->where('U.user_id = ?',$userId)
                                        ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getSchoolsForState($userId,$schoolsId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F'=>'tbl_followers'), array('created_date'))
                        ->joinLeft(array('S' => 'tbl_schools')," F.follower_id  = S.school_id ", array('school_name','school_id'))
                        ->where('F.user_id = '.$userId." AND F.follower_type = 'S' ")
                        ->where('F.follower_id NOT IN ('.$schoolsId.')')
                        ->order('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getCollegesForState($userId,$collegeID) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F'=>'tbl_followers'), array('created_date'))
                        ->joinLeft(array('C' => 'tbl_colleges')," F.follower_id  = C.college_id  ", array('college_id','college_name'))
                        ->where('F.user_id = '.$userId." AND F.follower_type = 'C' ")
                        ->where('F.follower_id NOT IN ('.$collegeID.')')
                        ->order('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getEmployersForState($userId,$employerID) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F'=>'tbl_followers'), array('created_date'))
                        ->joinLeft(array('W' => 'tbl_work_place')," F.follower_id  = W.work_place_id  ", array('work_place_id','work_place_name'))
                        ->where('F.user_id = '.$userId." AND F.follower_type = 'W' ")
                        ->where('F.follower_id NOT IN ('.$employerID.')')
                        ->order('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getBussinessForState($userId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F'=>'tbl_followers'), array('created_date'))
                        ->joinLeft(array('BS' => 'tbl_business_users')," F.follower_id  = BS.business_id  ", array('business_id','business_name'))
                        ->where('F.user_id = '.$userId." AND F.follower_type = 'B' ")
                        ->order('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getSchoolsFromProfileForState($schools_id,$created_date,$uid) {
                $db = Zend_Db_Table::getDefaultAdapter();

                $userSchoolsArr = $this->getSchoolsForState($uid,$schools_id);

                //get schools info
                if(trim($schools_id) != ' ' &&trim($schools_id) != '' && $schools_id != 0 && $schools_id != null){
                        $schoolExplode = explode(',',$schools_id);
                        $schoolExplode = array_filter($schoolExplode);
                        $schoolIds = implode(",",$schoolExplode);
                        //get schhol info
                        $select = $db->select()
                                        ->from('tbl_schools',array('school_name','school_id'))
                                        ->where('school_id IN ('.$schoolIds.')');
                        $result = $db->fetchAll($select);

                        $schoolsCount = count($userSchoolsArr);
                        for($k = 0; $k < count($result); $k++) {
                                $res = $result[$k];
                                $res['created_date'] = $created_date;
                                $userSchoolsArr[$schoolsCount] = $res;
                                $schoolsCount++;
                        }
                }

                return $this->sortArrayByDate($userSchoolsArr);
        }

        public function getcollegsFromProfileForState($collegeID,$created_date,$uid) {
                $db = Zend_Db_Table::getDefaultAdapter();

                $userCollegesArr = $this->getCollegesForState($uid,$collegeID);

                //get colleges  info
                if(trim($collegeID) != ' ' &&trim($collegeID) != '' && $collegeID != 0 && $collegeID != null){
                        $collegeExplode = explode(',',$collegeID);
                        $collegeExplode = array_filter($collegeExplode);
                        $collegeIds = implode(",",$collegeExplode);
                        //get schhol info
                        $select = $db->select()
                                        ->from('tbl_colleges',array('college_name','college_id'))
                                        ->where('college_id IN ('.$collegeIds.')');
                        $result = $db->fetchAll($select);

                        $collegeCount = count($userCollegesArr);
                        for($k = 0; $k < count($result); $k++) {
                                $res = $result[$k];
                                $res['created_date'] = $created_date;
                                $userCollegesArr[$collegeCount] = $res;
                                $collegeCount++;
                        }
                }
                return $this->sortArrayByDate($userCollegesArr);
        }

        public function getEmployersFromProfileForState($employerID,$created_date,$uid) {
                $db = Zend_Db_Table::getDefaultAdapter();

                $userEmployersArr = $this->getEmployersForState($uid,$employerID);

                //get schools info
                if(trim($employerID) != ' ' &&trim($employerID) != '' && $employerID != 0 && $employerID != null){
                        $employerExplode = explode(',',$employerID);
                        $employerExplode = array_filter($employerExplode);
                        $employerIds = implode(",",$employerExplode);
                        //get Employers info
                        $select = $db->select()
                                        ->from('tbl_work_place',array('work_place_name','work_place_id'))
                                        ->where('work_place_id IN ('.$employerIds.')');
                        $result = $db->fetchAll($select);

                        $employersCount = count($userEmployersArr);
                        for($k = 0; $k < count($result); $k++) {
                                $res = $result[$k];
                                $res['created_date'] = $created_date;
                                $userEmployersArr[$employersCount] = $res;
                                $employersCount++;
                        }
                }
                return $this->sortArrayByDate($userEmployersArr);
        }

        //Sort array using created date.
        public function sortArrayByDate($mixedArr)
        {
             $orderByDate = array();
                foreach ($mixedArr as $key => $row) {
                        $orderByDate[$key]  = strtotime($row['created_date']);
                }
                array_multisort($orderByDate, SORT_DESC, $mixedArr);
                return $mixedArr;
        }



}