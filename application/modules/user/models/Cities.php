<?php class User_Model_Cities {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('User_Model_DbTable_Cities');
                }
                return $this->_dbTable;
        }

        public function get_cities($id, $limit='') {
                $select = $this->getDbTable()->select()
                                            ->where('state_id ="'.$id.'" AND status = "1"');
                if($limit != '')
                    $select->limit($limit);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getZipcodesByCity($cityId, $limit='') {
            $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('Z' => 'tbl_zipcode'), array('id','zipcode'))
                        ->where('Z.city_id ="'.$cityId.'" AND Z.status = "1"');
                if($limit != '')
                    $select->limit($limit);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getCityNameByCityID($city_id) {
            $select = $this->getDbTable()->select()
                                        ->where('id = "'.$city_id.'" AND status = "1"');
                                        //echo $select;exit;
            $resultSet = $this->getDbTable()->fetchAll($select);
            return $resultSet;
        }

        public function getZipcodeByZipID($zip_id) {
            $db = Zend_Db_Table::getDefaultAdapter();
             $select = $db->select()
                        ->from(array('Z' => 'tbl_zipcode'))//, array('zipcode')
                        ->where('Z.id ="'.$zip_id.'" AND Z.status = "1"');
            $resultSet = $db->fetchAll($select);
            if(count($resultSet) > 0)
                return $resultSet;
            else
                return 0;
        }

        public function getCityByName($city) {
            $select = $this->getDbTable()->select()
                                        ->where('city_name = "'.$city.'"');
            $resultSet = $this->getDbTable()->fetchRow($select);
            return $resultSet;
        }

        public function insert($data) {
            $insId = $this->getDbTable()->insert($data);
            return $insId;
        }

        public function getZipcodeByZip($zipcode) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('Z' => 'tbl_zipcode'), array('id','zipcode'))
                        ->where('Z.zipcode ="'.$zipcode.'" AND Z.status = "1"');
            $resultSet = $db->fetchRow($select);
            return $resultSet;
        }

        public function insertzipcode($data) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $ins = $db->insert('tbl_zipcode',$data);
            return $ins;
        }

        public function getcitiesbykeyword($limit,$keyword,$stateId) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('C'=>'tbl_city'), array('city_name','id'))
                            ->where('C.city_name LIKE "'.addslashes($keyword).'%" AND C.state_id = "'.$stateId.'" AND C.status = "1"')
                            ->limit($limit);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getzipcodebykeyword($limit,$keyword,$cityId) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('Z'=>'tbl_zipcode'), array('zipcode','id'))
                            ->where('Z.zipcode LIKE "'.addslashes($keyword).'%" AND Z.city_id = "'.$cityId.'" AND Z.status = "1"')
                            ->limit($limit);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

}
