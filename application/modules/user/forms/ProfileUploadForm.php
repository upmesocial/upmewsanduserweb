<?php
class User_Form_ProfileUploadForm extends Zend_Form {
    public function init() {
            $this->addPrefixPath('ZC_Form_Decorator','ZC/Form/Decorator','decorator');
            $this->setMethod('post');
            $this->setAttrib('id','profileUploadForm');
            $maxuploadsize = 2* 1024 * 1024; //2MB
            $photo = new Zend_Form_Element_File(array('name' => 'photo', 'id' => 'photo','class'=>'file required','size'=>'15','isArray' => false,'multiple'=>false));
            $photo->setLabel($this->getView()->translate('Upload Photo').':')//to display label
                        ->setRequired(true)
                        ->setMaxFileSize($maxuploadsize)
                        ->addValidator('Size', false, $maxuploadsize)// limit to 10 meg
                        ->addValidator('Extension', false, 'jpg,jpeg,png,gif')// only JPEG, PNG, and GIFs
                        ->addValidator('ImageSize', false,array('maxwidth' => MAX_IMAGE_WIDTH,'maxheight' => MAX_IMAGE_HEIGHT));
            $photo->setDestination("uploads/images/original/");
            $photo->setValueDisabled(true);
            $this->addElements(
                    array($photo)
            );
    }

    public function isValid( $data ) {
            $txterror = 0;
            $isValid = parent::isValid( $data );
            if( !$isValid ) {
                    $arrErrors = parent::getErrors();	
                    if( is_array( $arrErrors ) && count( $arrErrors ) > 0 ) {
                            foreach( $arrErrors as $key => $value ) {
                                    if( is_array( $value ) && count( $value ) > 0 ) {
                                            $txterror = 1;
                                            $objElement = parent::getElement( $key );
                                            if( $objElement )
                                                $objElement->setAttrib('class',$objElement->getAttrib('class') . ' invalid errorclass');
                                    }
                            }
                    }
                    if($txterror == 1)
                            return false;
            }
            return true;
    }
}