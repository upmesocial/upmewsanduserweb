<?php
class User_Form_ProfileVideoForm extends Zend_Form {
    public function init() {
            $this->addPrefixPath('ZC_Form_Decorator','ZC/Form/Decorator','decorator');
            $this->setMethod('post');
            $this->setAttrib('id','profilevideoForm');
            
            //$maxuploadsize = 2* 1024 * 1024; //2MB
            
            $photo = new Zend_Form_Element_File(array('name' => 'video', 'id' => 'video','class'=>'file','size'=>'15'));
            $photo->setLabel($this->getView()->translate('Upload Video').':')//to display label
                        //->setRequired(true)
                        ->addValidator('Count', false, 1)
                        ->addValidator('Extension', false, 'avi,mov,wmv,mpeg,mp4,flv,mpg')// only JPEG, PNG, and GIFs
                        ->addValidator('Size', false,array('min' => '10KB','max' => '100MB'));
            $photo->setDestination("uploads/videos/");
            
            $photo->setValueDisabled(true);
            
            
            $this->addElements(
                    array($photo)
            );
    }
    
    public function isValid( $data ) {
            $txterror = 0;
            $isValid = parent::isValid( $data );
            if( !$isValid ) {
                    $arrErrors = parent::getErrors();	
                    if( is_array( $arrErrors ) && count( $arrErrors ) > 0 ) {
                            foreach( $arrErrors as $key => $value ) {
                                    if( is_array( $value ) && count( $value ) > 0 ) {
                                            $txterror = 1;
                                            $objElement = parent::getElement( $key );
                                            if( $objElement )
                                                $objElement->setAttrib('class',$objElement->getAttrib('class') . ' invalid errorclass');
                                    }
                            }
                    }
                    if($txterror == 1)
                            return false;
            }
            return true;
    }
}