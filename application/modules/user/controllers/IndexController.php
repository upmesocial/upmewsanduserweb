<?php
class User_IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
        $mkey = $this->getRequest()->getParam('mkey');
        if(!isset($mkey) && $mkey == '') {
            if(Zend_Registry::isRegistered('userdata')) {
                $logged_user_data = Zend_Registry::get('userdata');
                $loggedUserId = $logged_user_data->user_id;
                $user_account_status = $logged_user_data->user_account_status;
                $loggedUserType = 'U';
            }
            if(Zend_Registry::isRegistered('businessdata')) {
                $logged_user_data = Zend_Registry::get('businessdata');
                $loggedUserId = $logged_user_data->business_id;
                $user_account_status = $logged_user_data->status;
                $loggedUserType = 'B';
            }
        }
        if(!empty($logged_user_data)) {
            $limit= 30;
            $photosObj = new Mobile_Model_Photos();
            $followersgallery = $photosObj->getTimelineClickedImagesVideosDetails($loggedUserId,$loggedUserType, $limit);
            //echo "<pre>";print_r($followersgallery);exit;
            //echo count($followersgallery);exit;
            $this->view->assign('followersgallery', $followersgallery);
        }
        $user_id = $this->getRequest()->getParam('user_id');
        if(!empty($user_id)) {
            $loggedUserId = $user_id;
            $loggedUserType = 'U';
        }
    }

    public function indexAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        //echo "<pre>";print_r($logged_user_data);exit;
        if(!empty($loggedUserId) && $logged_user_data->user_type != "B") {
            $this->_redirect('user/userprofile');
            //$this->_helper->redirector('','dashboard','user');
        }
        if(!empty($loggedUserId) && $logged_user_data->user_type == "B") {
            $this->_redirect('business/businessprofile');
            //$this->_helper->redirector('','dashboard','user');
        }

    }

    public function validatesignupAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $uName = $request->getParam('uname');
        $uPwd = $request->getParam('pwd');
        $cPwd = $request->getParam('cpwd');
        //if values are not passed correctly
        if($uName == "" || $uPwd == "" || $cPwd == "") {
            echo "Required Fields Can Not Be Empty";
            return false;
        }
        if(strlen($uPwd) < '6') {
            echo "Password Should be Minimum of 6 Characters";
            return false;
        }
        if(strlen($cPwd) < '6') {
            echo "Confirm Password Should be Minimum of 6 Characters";
            return false;
        }
        if($cPwd != $uPwd) {
            echo "Password Mismatch";
            return false;
        }
        if(strlen($uName) < '6') {
            echo "UserName Should be Minimum of 6 Characters";
            return false;
        }
        if(strlen($uName) > '15') {
            echo "UserName Should be Maximum of 15 Characters";
            return false;
        }
        if(!preg_match('/^[a-zA-Z]{1}[a-zA-Z0-9]+$/', $uName)) {
            echo 'Username Allows Alphanumeric or only-Alpabets and first letter should be alphabet';
            return false;
        }
        $userObj = new User_Model_User();
        if(!$userObj->isUsernameAvail($uName)) {
            echo "Username not available";
            return false;
        }
        echo 'success';
    }

    public function loginAction() {
        /*$request = $this->getRequest();
        $isAjaxReq = $this->getRequest()->isXmlHttpRequest();
        $mobileKey = $this->getRequest()->getParam('mkey');*/
        $uName = $this->getRequest()->getParam('uname');
        $uPwd = $this->getRequest()->getParam('pwd');
        if($uName == '' || $uPwd == '')
            $this->_helper->redirector('index','index','user');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $userdetails = $userObj->getUserDetailsByname($uName);
        //echo "password =".md5($uPwd.'ce8d96d579d389e783f95b3772785783ea1a9854');
        //echo "<br />";
        //echo "username = ".$uName;
        //exit();
        $adapter = new Zend_Auth_Adapter_DbTable(
                        $this->_getParam('db'),
                        'tbl_users',
                        'username',
                        'password',
                        'MD5(CONCAT(?, "'.SECURITY_SALT.'"))'
                );
        // We're authenticated! Redirect to the home page
        $adapter->setIdentity($uName);
        $adapter->setCredential($uPwd);
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        //echo "<pre>";print_r($auth);//exit;
        $result = $auth->authenticate($adapter);
        //echo "<pre>";print_r($result);exit;

        if ($result->isValid()) {
            $user = $adapter->getResultRowObject();
            //echo '<pre>';print_r($user);print_r($userdetails);exit;

            // FOR PUSH NOTIFICATIONS
		    if($_SERVER['REMOTE_ADDR'] == '192.168.1.57' || $_SERVER['REMOTE_ADDR'] =='10.211.173.8' || $_SERVER['REMOTE_ADDR'] =='182.72.66.214'){
				$mkey = $this->getRequest()->getParam('mkey');
				if($mkey != ''){
				    $os_type = $this->getRequest()->getParam('osType');
				    /*if($os_type == '') {
				        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
				        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'OS Type can not be Empty!!!'));
				        return;
				    }*/
				    if($os_type == 'android' || $os_type == 'iphone') {

				        $push_unique_id = $this->getRequest()->getParam('push_unique_token');
				       /* if($push_unique_id == '') {
				            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
				            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Push notification Id can not be Empty!!!'));
				            return;
				        } */
				        // Check if any active user with the same push notification Id
				        $activeUserByPushId = array();
				        $userObj = new User_Model_User();
				        $activeUserByPushId = $userObj->getUserByPushNotificationId($push_unique_id);
				        $cnt = count($activeUserByPushId);
				        if($cnt > 0){
				            $tempAry = array();
				            $tempAry['push_unique_token'] = '';
				            $tempAry['os_type'] = '';
				            $ids = '';
				            foreach($activeUserByPushId as $key=>$tmpUsr) {
				                if($key < $cnt-1)
				                    $ids .= "'".$tmpUsr['user_id']."',";
				                if($key == $cnt-1)
				                    $ids .= "'".$tmpUsr['user_id']."'";
				            }
				            $upd = $userObj->updatePushIdByUserId($ids, $tempAry);
				        }
				        $updatePushArr = array();
				        $updatePushArr['push_unique_token'] = $push_unique_id;
				        $updatePushArr['os_type'] = $os_type;
				        $upd = $userObj->updatePushIdByUserId($userdetails['user_id'], $updatePushArr);
				    }
				}
		    }

            $auth->getStorage()->write($user);
            if($userdetails['user_account_status'] == 'D') {
                $resultAry = array('service_status'=>'deactive',"content" => $userdetails);
            } /*elseif($userdetails['user_account_status'] == 'I') {
                $this->_helper->redirector('index','requesttoadmin','user');
            }*/
            else {
                $resultAry = array('service_status'=>'success',"content" => $userdetails);
            }
            //$resultAry = array('service_status'=>'success',"content" => $user);
        } else {
            $resultAry = array('service_status'=>'error',"error_msg" => 'Invalid Login Details');
        }
        echo json_encode($resultAry);
    }

    public function requesttoadminAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $userdata = $auth->getIdentity();
        $this->view->assign('userdata', $userdata);
        $request = $this->getRequest();
        if(!empty($request)) {
            //Code for sending mail to admin to request for account activation.
        }
    }

    public function logoutAction() {
        $this->_helper->viewRenderer->setNoRender();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if($auth->hasIdentity()) {
            $auth->clearIdentity();
            setcookie('luname', '', time() -3600, '/');
            setcookie('lupwd', '', time() -3600, '/');
            Zend_Session::namespaceUnset('userdata');
            Zend_Registry::_unsetInstance();
            $this->_helper->redirector('index','index','user');
            //Zend_Session::destroy(true, true);
        } else {
            $this->_helper->redirector('index','index','user');
        }
    }

    public function facebookAction() {
        $token = $this->getRequest()->getParam('token',false);
        if($token == false) {
            $this->_helper->redirector('index');
        }
        Zend_Session::start();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $fbAdapter = new FB_Facebook($token);
        $result = $auth->authenticate($fbAdapter);
        if($result->isValid()) {
            //$user = $fbAdapter->getUser();
            $user   = $result->getIdentity();
            $auth->getStorage()->write($user);
            $userdata = new Zend_Session_Namespace('userdata');
            $userdata->email = $user['email'];
            $userdata->facebook_unique_id = $user['facebook_unique_id'];
            $this->_helper->redirector('index','userprofile','user');
        }
        $this->_helper->redirector('index');
    }

    public function userprofileAction() {
        $request = $this->getRequest();
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $user_account_status = $logged_user_data->user_account_status;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $user_account_status = $logged_user_data->status;
            $loggedUserType = 'B';
        }



        $userObj = new User_Model_User();
        $followObj = new User_Model_Followers();

        // check if the session exist, else redirect to Home page
        if($loggedUserId == '' || $user_account_status == 'I') {
            $this->_helper->redirector('index','index','user');
        }
        if($request->getParam('user_id') == '') {
            if(!empty($logged_user_data->facebook_unique_id)) {
                $uniqueId= $logged_user_data->facebook_unique_id;
                $email = $logged_user_data->email;
                $userdata = $userObj->getFbUserDetails($uniqueId, $email);
                $uid = $userdata->user_id;
                $uType = $userdata->user_type;
                $ufname = $userdata->firstname;
            } else {
                $userdata = $logged_user_data;
                $uid = $userdata->user_id; //echo '1'.$uid;exit;
                $uType = $userdata->user_type;
                $ufname = $userdata->firstname;
            }
        } else {
            $uid = $request->getParam('user_id');//echo '2'.$uid;exit;
            $userdata = $userObj->getUserDetails($uid);
            $uType = $userdata->user_type;
            $ufname = $userObj->getfirstname($uid,$uType);
        }
        $this->view->assign('ufname', $ufname);
        if($uType != 'B')
            $uType = "U";
        // Fetching User Cool POints
        $coolPoints = $userObj->getUserCoolPoints($uid, $uType);
        $this->view->coolPoints = $coolPoints;

        $userfollowing  = array();
        if($request->getParam('user_id') != '') {
            if(Zend_Registry::isRegistered('businessdata')) {
                $businessdata = Zend_Registry::get('businessdata');
            }
            if(!empty($businessdata)) {
                $this->view->businessdata = $businessdata;
            }
            // TO check if following other profile user or not
            $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid, 'U', $loggedUserId, $loggedUserType);
            //echo $uid.'-->'."<pre>".$loggedUserId."-->".$loggedUserType;print_r($userfollowing);exit;
            if(!empty($userfollowing)) {
                if($userfollowing[0]['user_id'] == $loggedUserId && $userfollowing[0]['user_type'] == $loggedUserType) {
                    $userfollowing[0]['isFollowing'] = "1";
                } else {
                    $userfollowing[0]['isFollowing'] = "0";
                }
            } elseif($loggedUserId == $uid && $loggedUserType=='U') {
                $userfollowing[0]['isFollowing'] = "2";
            } else {
                $userfollowing[0]['isFollowing'] = "0";
            }
        } else {
            $userfollowing[0]['isFollowing'] = "2";
        }
        //echo "<pre>";print_r($userdata);print_r($userfollowing);exit;
        $this->view->userfollowing = $userfollowing[0];
        $this->view->loggedUserId = $loggedUserId;
        $this->view->loggedUserType = $loggedUserType;

        if(empty($userdata)) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->redirector('login');
            return;
        }
        $userlevelObj = new User_Model_Userlevels();
        $scribbleObj = new Scribbles_Model_Scribbles();
        $limit = ($request->getParam('limit') != '')?$request->getParam('limit'):10;
        $userDetAry = array();
        $userDetAry = $userObj->getUserDetails($uid);
        $level = $userlevelObj->getUserLevelByUserId($userdata->user_id, $userdata->user_type);
        //echo '<pre>';print_R($userDetAry->user_level);exit;
        $level= $userDetAry->user_level;
        $userlevelAry = $userlevelObj->getUserLevels($level);

        // to Get User Level equivalent Or Lower Levels
        $userLowerLevelsAry = $userlevelObj->getEquivalentOrLowerLevelsBasedOnUserLevel($level);
        //echo "<pre>";print_r($userLowerLevelsAry->toArray());exit;
        $userDetAry = $userDetAry->toArray();
        $userDetAry['nooffollowers'] = $followObj->getFollowersCnt($uid);
        $userDetAry['nooffollowings'] = $followObj->getFollowingCnt($uid);
        //$userDetAry['scribblecount'] = $scribbleObj->getScribbleCnt($uid,$userDetAry['user_level']);
        $userDetAry['scribblecount'] = $scribbleObj->getUserPostedScribbleCnt($uid, $userDetAry['user_level']);
        //$scribArray = $scribbleObj->getUserScriblesById($uid,$limit);
        $this->view->assign('userDetAry', $userDetAry);
        $this->view->assign('userlevelAry', $userlevelAry);
        //$this->view->assign('scribArray', $scribArray);

        //For UnknowUserProfile Page
        if($userfollowing[0]['isFollowing'] == '0' || $loggedUserType == 'B') {
            if($loggedUserType == "B")
                $tempLevelType = $userdata->user_level;
            else
                $tempLevelType = $logged_user_data->user_level;
            $level = $followObj->getFollowersByLevel($userDetAry['user_id'], $tempLevelType);
            $downline = $followObj->getFollowersByDownline($userDetAry['user_id'], $tempLevelType);
            //echo '<pre>';print_r($logged_user_data);print_r($userDetAry);exit;
            //$loggedUserlevel= $logged_user_data->user_level;

            $userDetAry['scribblecount'] = $scribbleObj->getUserPostedScribbleCnt($uid, $tempLevelType);
            $this->view->assign('userDetAry', $userDetAry);
            $this->view->assign('level', $level);
            $this->view->assign('downline', $downline);
            $this->view->assign('loggedUserlevel',$tempLevelType);
            $this->render('unknownprofile');
        }
        //To Display Media Time Line
//        $followersgallery = $followObj->getFollowersAlbumImages($loggedUserId, $loggedUserType);
//        //if($_SERVER['REMOTE_ADDR'] == '182.72.88.157') {echo "<pre>";print_r($followersgallery);exit;}
//        $this->view->assign('followersgallery', $followersgallery);
        if(!empty($userdata)) {
            $userdata = $userObj->getUserDetails($uid);
            $this->view->userdata = $userdata;
            $this->view->userLowerLevelsAry = $userLowerLevelsAry->toArray();
        } else {
            $this->_helper->redirector('index','index','user');
        }
    }

    public function mediaAction() {
        $request = $this->getRequest();
        //echo '<pre>';print_r($request->getParams());exit;
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if(!$auth->hasIdentity()) {
            $this->_helper->redirector('index','index','user');
        }
        if($request->getParam('user_id') =='') {
            $user_id = $auth->getIdentity()->user_id;
        } else {
            $user_id = $request->getParam('user_id');
        }
        if($request->getParam('type') == 'videos') {
            $type = 'videos';
        } else {
            $type = 'photos';
        }
        $mobileobj = new Mobile_Model_Albums();
        $albums = $mobileobj->getAlbumsByUserId($user_id,'U');
        $this->view->assign('albums',$albums);
        $videoObj = new Videos_Model_Videos();
        $videos = $videoObj->getvideos($user_id);
        $this->view->assign('videos',$videos);
        $this->view->assign('type',$type);
        $this->view->assign('user_id',$user_id);
    }

    public function updateuserdescriptionAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $description = $request->getParam('description');
         if($description == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Description can not be Empty!!!'));
            return;
        }

        $user_id = $request->getParam('user_id');
        if($user_id == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }

        if(!is_numeric($user_id)) {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
            return;
        }
        $updateAry['description'] = $description;
        $userObj = new User_Model_User();
        $stat = $userObj->updateUser($updateAry, $user_id);
        if($stat) {
            echo json_encode(array('service_status'=>'success',"content" =>$stat));
            return;
        } else {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
            return;
        }
    }

    public function updateuserdetailsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        /*** To update user Account Details ***/
        $request = $this->getRequest();
        //echo '<pre>';print_r($request->getParams());exit;
        $userObj = new User_Model_User();
        $mobileKey = $this->getRequest()->getParam('mkey');
        $user_id = $request->getParam('user_id');
        if($user_id == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        $country = $request->getParam('country');
        $state = $request->getParam('state');
        $city = $request->getParam('city');
        $high_school_info = $request->getParam('high_school_info');
        $college_info = $request->getParam('college_info');
        $employer_info = $request->getParam('employer_info');
        $mobile_no = $request->getParam('mobile_no');
        if($mobileKey == '') {
            if($country == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Country can not be Empty!!!'));
                return;
            }
            if($state == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'State can not be Empty!!!'));
                return;
            }
            if($city == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'City can not be Empty!!!'));
                return;
            }
            if($high_school_info == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'High School Info can not be Empty!!!'));
                return;
            }
            if($college_info == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'College Info can not be Empty!!!'));
                return;
            }
            if($employer_info == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Employer Info can not be Empty!!!'));
                return;
            }
            if($mobile_no == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Mobile No can not be Empty!!!'));
                return;
            }
        }
        $firstname = $request->getParam('firstname');
        if($firstname == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Firstname can not be Empty!!!'));
            return;
        }
        $lastname = $request->getParam('lastname');
        /*if($mobileKey == '') {
            if($lastname == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Lastname can not be Empty!!!'));
                return;
            }
        */
        $email = $request->getParam('email');
        $zipcode = $request->getParam('zipcode');
        $description = $request->getParam('description');
        $dob = $request->getParam('dob');
        $gender = $request->getParam('gender');
        if($email == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Email can not be Empty!!!'));
            return;
        }
        if(!preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $email)) {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Invalid Email Format!!!'));
            return;
        }
        if($email != '') {
            if(!$userObj->isEmailAvail($email,$user_id)) {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Email Already Exists!!!'));
                return;
            }
        }
        if($mobileKey == '') {
            if($gender == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Gender can not be Empty!!!'));
                return;
            }
            if($zipcode == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Zipcode can not be Empty!!!'));
                return;
            }
            if($description == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Description can not be Empty!!!'));
                return;
            }
            if($dob == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Date Of Birth can not be Empty!!!'));
                return;
            }
        }
        if($request->getParam('address_line_1') != '') {
            $address_line_1 = $request->getParam('address_line_1');
        }
        if($request->getParam('address_line_1') == '') {
            $address_line_1 = '';
        }
        if($request->getParam('address_line_2') != '') {
            $address_line_2 = $request->getParam('address_line_2');
        }
        if($request->getParam('address_line_2') == '') {
            $address_line_2 = '';
        }
        $updateAry['firstname'] = $firstname;
        $updateAry['lastname'] = $lastname;
        $updateAry['email'] = $email;
        $updateAry['gender'] = $gender;
        $updateAry['dob'] = $dob;
        $updateAry['mobile_no'] = $mobile_no;
        $updateAry['description'] = $description;
        $updateAry['address_line_1'] = $address_line_1;
        $updateAry['address_line_2'] = $address_line_2;
        $updateAry['country'] = $country;
        $updateAry['state'] = $state;
        $updateAry['city'] = $city;
        $updateAry['zipcode'] = $zipcode;
        $updateAry['high_school_info'] = $high_school_info;
        $updateAry['college_info'] = $college_info;
        $updateAry['employer_info'] = $employer_info;
        $updateAry['updated_date'] = date('Y-m-d H:i:s');
        //echo '<pre>';print_r($updateAry);exit;
        $stat = $userObj->updateUser($updateAry, $user_id);
        /** Setting Updated Images data to sessions **/
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $user = $auth->getIdentity();
        $user->firstname = $updateAry['firstname'];
        $auth->getStorage()->write($user);
        if($stat) {
            echo json_encode(array('service_status'=>'success',"content" =>$stat, 'responsecode' => '1001'));
            return;
        } else {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.', 'responsecode' => '1001'));
            return;
        }
    }

    public function statusdisableAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $request = $this->getRequest();
        $user_id = $request->getParam('user_id');
        if($user_id == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        $user_account_status = $request->getParam('user_account_status');
        if($user_account_status == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Account Status can not be Empty!!!'));
            return;
        }
        $updateAry['user_account_status']   = $user_account_status;
        $stat = $userObj->updateUser($updateAry, $user_id);
        if($stat) {
            echo json_encode(array('service_status'=>'success',"content" =>"Your Account has been successfully Deactivated"));
            return;
        } else {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
            return;
        }
    }

    public function updateuserlocationAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $request = $this->getRequest();
        $user_id = $request->getParam('user_id');
        if($user_id == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        $location = $request->getParam('location');
        if($location == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Location can not be Empty!!!'));
            return;
        }
        if($location != '0') {
            $updateAry['location'] = $location;
            $stat = $userObj->updateUser($updateAry, $user_id);
            if($stat) {
                echo json_encode(array('service_status'=>'success',"content" =>$stat));
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
            }
        } elseif($location=='0') {
            $updateAry['location'] = $location;
            $stat = $userObj->updateUser($updateAry, $user_id);
            if($stat) {
                echo json_encode(array('service_status'=>'success',"content" =>$stat));
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
            }
        } else {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
            return;
        }
    }

    public function updateuserprivacyAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $request = $this->getRequest();
        $user_id = $request->getParam('user_id');
        if($user_id == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        $privacy = $request->getParam('privacy');
        if($privacy == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Privacy can not be Empty!!!'));
            return;
        }
        if($privacy != '0') {
            $updateAry['privacy'] = $privacy;
            $stat = $userObj->updateUser($updateAry, $user_id);
            if($stat){
                echo json_encode(array('service_status'=>'success',"content" =>$stat));
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
            }
        } elseif($privacy=='0') {
            $updateAry['privacy'] = $privacy;
            $stat = $userObj->updateUser($updateAry, $user_id);
            if($stat){
                echo json_encode(array('service_status'=>'success',"content" =>$stat));
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
            }
        } else {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
        }
    }

    public function updateuserpasswordAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $request = $this->getRequest();
        $user_id = $request->getParam('user_id');
        if($user_id == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        $oldpwd = $request->getParam('oldpwd');
        if($oldpwd == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Old Password can not be Empty!!!'));
            return;
        }
        $newpwd = $request->getParam('newpwd');
        if($newpwd == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'New Password can not be Empty!!!'));
            return;
        }
        if(strlen($newpwd)<6) {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'New Password should be 6 and more characters!!!'));
            return;
        }
        $confirmpwd = $request->getParam('confirmpwd');
        if($confirmpwd == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Confirm Password can not be Empty!!!'));
            return;
        }
        if(strlen($confirmpwd)<6) {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Confirm Password should be 6 and more characters!!!'));
            return;
        }
        if($newpwd!=$confirmpwd ) {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'New Password and Confirm Password should be same!!!'));
            return;
        }
        $checkvalid = $userObj->duplicatepasswordcheck($user_id,$oldpwd);
        if($checkvalid != '0') {
            $updateAry['password'] = md5($newpwd.SECURITY_SALT);
            $stat = $userObj->updateUser($updateAry, $user_id);
            if($stat) {
                echo json_encode(array('service_status'=>'success',"content" =>$stat));
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
            }
        } else {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Incorrect Old Password.'));
            return;
        }
    }

    public function findfriendsAction($action="findFriends") {
        $request = $this->getRequest();
        $userdata = new Zend_Session_Namespace('userdata');
        //echo $uid.'<Pre>';print_r($request->getParams());print_R($userdata->password);exit;
        if($request->getParam('username') != '') {
            $uname = $request->getParam('username');
        } else {
            $uname = $userdata->username;
        }
        if($request->getParam('password') != '') {
            $pwd = $request->getParam('password');
        } else {
            $pwd = $userdata->password;
        }
        Zend_Session::start();
        $userdata = new Zend_Session_Namespace('userdata');
        $userdata->username = $uname;
        $userdata->password = $pwd;
        $this->view->assign('username', $uname);
        $this->view->assign('password', $pwd);
        $this->view->assign('action', $action);
    }

    public function signupAction($action="signup") {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        if(!empty($loggedUserId)) {
            $this->_redirect('user/dashboard');
        }
        $resultAry = array();
        $request = $this->getRequest();
        //echo "<pre>";print_r($request->getParams());//exit;
        $userName = ($request->uname != '' ? $request->uname : '');
        $password = ($request->pwd != '' ? base64_decode($request->pwd): '');
        $userObj = new User_Model_User();
        $postAry = $this->getRequest()->getParam('SignUp');
        //echo '<pre>';print_r($postAry);exit;
        $userdata = new Zend_Session_Namespace('userdata');
        $uid= $userdata->Id;
        //echo $uid;exit;
        //echo "<pre>".$uid;print_r($postAry);exit;
        if(!empty($uid) && $postAry == '') {
            $userdet = $userObj->getUserDetails($uid);
            $userdet['password'] = $userdata->password;
            //echo '<pre>';print_r($userdet);exit;
            $stateObj = new User_Model_States();
            $cityObj = new User_Model_Cities();
            $countryId = $userdet['country'];
            $stateId = $userdet['state'];
            $cityId = $userdet['city'];
            $states = $stateObj->get_states($countryId);
            $cities = $cityObj->get_cities($stateId);
            $zipcodes = $cityObj->getZipcodesByCity($cityId);
            $this->view->assign('postdata', $userdet);
            $this->view->assign('states', $states);
            $this->view->assign('cities', $cities);
            $this->view->assign('zipcode', $zipcodes);
        }
        if(!empty($uid))
            $userid= $uid;
        else
            $userid = '';
        if(!empty($postAry)) {
            $userEmail = $postAry['email'];
            if(!preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $userEmail)) {
                $resultAry = 'Invalid Email Format';
            }
            if($postAry['email'] != '') {
                if(!$userObj->isEmailAvail($userEmail,$userid)) {
                    $resultAry = 'Email Already Exists';
                }
            }
            // TO check for  Valid USer Name
            $userName = $postAry['username'];
            if(!preg_match('/^[a-zA-Z]{1}[a-zA-Z0-9]+$/', $userName)) {
                $resultAry = 'Username Allows Alphanumeric or only-Alpabets and first character should be alphabet';
            }

            if(strlen($userName) < '6' || strlen($userName) > '15') {
                    $resultAry = 'Username should be between 6 to 15 characters';
            }

            if($postAry['username'] != '') {
                //$userName = $postAry['username'];
                if(!$userObj->isUsernameAvail($userName,$userid)) {
                    $resultAry = 'Username Already Exists';
                }
            }
            $password = $postAry['password'];
            if(strlen($password) < '6') {
                    $resultAry = 'Password should be minimum 6 characters';
            }

            if(empty($resultAry)) {
                Zend_Session::start();
                $userdata = new Zend_Session_Namespace('userdata');
                $userdata->username = $postAry['username'];
                $userdata->password = $postAry['password'];
                //insert in database
                $postAry['password'] = md5($postAry['password'].SECURITY_SALT);
                $actCode = md5($userEmail);
                $postAry['activationcode'] = $actCode;
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.53'){
                    //echo $uid."<pre>".$uid;print_r($postAry);exit;
                }
                if(!empty($uid)) {
                    $id = $userObj->updateUser($postAry, $uid);
                } else {
                    $id = $userObj->insertNewUser($postAry);
                }
                if($uid == '') {
                    Zend_Session::start();
                    $userdata = new Zend_Session_Namespace('userdata');
                    $userdata->Id = $id;
                    $cool['user_id'] = $id;
                    $cool['user_type'] = 'U';
                    $cool['cool_points'] = '100';
                    $coolid = $userObj->insertcoolpoints($cool);
                    /***** Mail Functionality For Registration Without Payment *****/
                    if($_SERVER['SERVER_NAME'] == '192.168.1.57')
                    $m = new UpmeSocial_HtmlMailer();
                    $m->setSubject("Login Credentials");
                    $m->addTo($userEmail)
                        ->setViewParam('user_id',$uid)
                        ->setViewParam('firstname',$postAry['firstname'])
                        ->setViewParam('lastname',$postAry['lastname'])
                        ->setViewParam('username',$postAry['username'])
                        ->setViewParam('password',$userdata->password)
                        ->setViewParam('email',$userEmail)//$postAry['password']
                        ->setViewParam('actlink',$actCode);//$postAry['password']
                    $m->sendHtmlTemplate("logindetails.phtml");
                }
                if($id != '') {
                    return $this->_redirect('user/profilepicture');
                }
            } else {
                $stateObj = new User_Model_States();
                $cityObj = new User_Model_Cities();
                $countryId = $postAry['country'];
                $stateId = $postAry['state'];
                $cityId = $postAry['city'];
                $states = $stateObj->get_states($countryId);
                $cities = $cityObj->get_cities($stateId);
                $zipcodes = $cityObj->getZipcodesByCity($cityId);
                $schools = $userObj->getSchools();
                $colleges = $userObj->getColleges();
                $workplaces = $userObj->getWorkPlaces();
                $this->view->assign('errormsg', $resultAry);
                $this->view->assign('postdata', $postAry);
                $this->view->assign('states', $states);
                $this->view->assign('cities', $cities);
                $this->view->assign('zipcode', $zipcodes);
                $this->view->assign('schools', $schools);
                $this->view->assign('colleges', $colleges);
                $this->view->assign('workplaces', $workplaces);
            }
        }
        // For Fetching Country Details
        $countriesObj = new User_Model_Countries();
        $countries = $countriesObj->get_countries();
        $schools = $userObj->getSchools();
        $colleges = $userObj->getColleges();
        $workplaces = $userObj->getWorkPlaces();
        $this->view->assign('action', $action);
        //$this->view->SignUp =  $request['SignUp'];
        // For forwarding user name and password to Insert into DB
        $this->view->assign('username', $userName);
        $this->view->assign('password', $password);
        $this->view->assign("countries",$countries);
        $this->view->assign('schools', $schools);
        $this->view->assign('colleges', $colleges);
        $this->view->assign('workplaces', $workplaces);
    }

    public function profilepictureAction($action="profilepicture") {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        if(!empty($loggedUserId)) {
            $this->_redirect('user/dashboard');
        }
        $form = new User_Form_ProfileUploadForm();
        $request = $this->getRequest();
        $userdata = new Zend_Session_Namespace('userdata');
        $uid= $userdata->Id;
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $arrFileName = array();
                $album = '';
                if(!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == '0') {
                    $dir_upload = UPLOAD_PATH.'images/original/';
                    $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                    $medium_path = UPLOAD_PATH.'images/medium/';
                    $mobile_path = UPLOAD_PATH.'images/mobile/';
                    $android_path = UPLOAD_PATH.'images/android/';
                    $fileName = $_FILES['photo']['name'];
                    //$fullPathNameFile = $dir_upload.$fileName;
                    $image = new UpmeSocial_Controller_Action_Helper_Image();
                    $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
                    $userObj = new User_Model_User();
                    $updateAry['profile_pic_path'] = $arrFileName['photo'];
                    $album['user_id'] = $uid;
                    $album['user_type'] = 'U';
                    $album['album_name'] = 'Profile Photos';
                    $album['is_default'] = '1';
                    $albumobj = new Mobile_Model_Albums();
                    $aid = $albumobj->albumnameexistsrnot($album);
                    //echo '<pre>';print_r($aid);print_r($_FILES);exit;
                    $album['created_date'] = date('Y-m-d H:i:s');
                    if(!empty($aid['id'])) {
                        $albumid = $albumobj->update($album, $aid['id']);
                        $albumid = $aid['id'];
                    } else
                        $albumid = $albumobj->insert($album);
                    $photoObj = new Mobile_Model_Photos();
                    $coverphoto = $photoObj->albumidexistsrnot($albumid,$uid,'U');
                    //echo '<pre>';print_r($aid);print_r($coverphoto);exit;
                    $data['album_id'] = $albumid;
                    $data['photo_name'] = 'Profile Photo';
                    $data['photo_original_name'] = $fileName;
                    $data['photo_modified_name'] = $arrFileName['photo'];
                    $photodata['width'] = $arrFileName['width'];
                    $photodata['height'] = $arrFileName['height'];
                    $photodata['android_width'] = $arrFileName['android_width'];
                    $photodata['android_height'] = $arrFileName['android_height'];
                    $data['user_id'] = $uid;
                    $data['user_type'] = 'U';
                    $data['cover_photo'] = '1';
                    $data['created_date'] = date('Y-m-d H:i:s');
                    $data['blasted_date'] = $data['created_date'];
                    if($coverphoto['photo_id'] == '')
                        $photo_id = $photoObj->insert($data);
                    else
                        $photo_id = $photoObj->update($data,$aid['id']);
                    $id = $userObj->updateUser($updateAry, $uid);
                    //$this->_helper->redirector('profilevideo');
                    $this->_redirect('user/profilevideo');
                } else {
                    $resultAry = array('service_status'=>'error',"content" => '');
                }
            }
        }
        $this->view->form = $form;
        $this->view->assign('action', $action);
    }

    public function profilevideoAction($action="profilevideo") {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        if(!empty($loggedUserId)) {
            $this->_redirect('user/dashboard');
        }
        $form = new User_Form_ProfileVideoForm();
        $request = $this->getRequest();
        $userdata = new Zend_Session_Namespace('userdata');
        $uid = $userdata->Id;
        $uname = $userdata->username;
        $pwd = $userdata->password;
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                if(!empty($_FILES['video']['name'])) {
                    $dir_upload = UPLOAD_PATH.'videos/';
                    if(isset($_FILES['video']))	{
                        $ext = pathinfo($_FILES['video']['name']);
                        $_FILES['video']['name'] = $uid.'_'.time().'.'.$ext['extension'];
                        $_FILES['video']['posted_by'] = $uid;
                    }
                    $upload = new Zend_File_Transfer_Adapter_Http();
                    $upload->setDestination($dir_upload);
                    $config = array(
                                    'uploadPath' => UPLOAD_PATH.'videos/',
                                    'outputPath' => UPLOAD_PATH.'videos/',
                                    'thumbPath' => UPLOAD_PATH.'videos/',
                                    'conversionLog' => '/var/www/convertvideo/flvfiles/conversionLog.log',
                                    'errorLog' => '/var/www/convertvideo/flvfiles/errorLog.log',
                                    'conversionScript' => '/var/www/convertvideo/processVideo.php',
                                    'outputFormat' => 'mp4', // either 'mp4' or 'flv'
                                    'bitRate' => 32000,
                                    'sampleRate' => 22050,
                                    'videoMaxWidth' => 320,
                                    'videoMaxHeight' => 240,
                                    'thumbMaxWidth' => 320,
                                    'thumbMaxHeight' => 240,
                                    'minDuration' => 1,
                                    'videoThumbDepth' => 25, // % into video to get thumbnail
                            );
                    $file = isset($_FILES['video']) ? $_FILES['video'] : null;
                    $converter = new UpmeSocial_Controller_Action_Helper_VideoConverter($file, $config);
                    $details = $converter->getDetails();
                    $converter->processVideo();
                    $userObj = new User_Model_User();
                    $updateAry['profile_video_path'] = $details['title'].'.'.$details['format'];
                    $updateAry['profile_video_thumbnail_path'] = $details['thumbnail'];
                    $videoObj = new Videos_Model_Videos();
                    $videoAry['video_name'] = $details['title'].'.'.$details['format'];
                    $videoAry['video_path'] = $details['title'].'.'.$details['format'];
                    $videoAry['thumbnail_path'] = $details['thumbnail'];
                    $videoAry['user_id'] = $uid;
                    $videoAry['created_date'] = date('Y-m-d H:i:s');
                    $videoAry['blasted_date'] = $videoAry['created_date'];
                    $video_id = $videoObj->insertVideo($videoAry);
                    $userObj->updateUser($updateAry, $uid);
                }
            }
            $uName = $uname;
            $uPwd = $pwd;
            if($uName == '' || $uPwd == '')
                $this->_helper->redirector('index','index','user');
            $adapter = new Zend_Auth_Adapter_DbTable(
                            $this->_getParam('db'),
                            'tbl_users',
                            'username',
                            'password',
                            'MD5(CONCAT(?, "'.SECURITY_SALT.'"))'
                    );
            // We're authenticated! Redirect to the home page
            $adapter->setIdentity($uName);
            $adapter->setCredential($uPwd);
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $result = $auth->authenticate($adapter);
            if ($result->isValid()) {
                $user = $adapter->getResultRowObject();
                $auth->getStorage()->write($user);
                //$resultAry = array('service_status'=>'success',"content" => $user);
            }/* else {
                $resultAry = array('service_status'=>'error',"content" => '');
            }
            echo json_encode($resultAry);*/
            //$this->_helper->redirector('userprofile');
            $this->_redirect('user/userprofile');
        }
        $this->view->form = $form;
        $this->view->assign('action', $action);
    }

    public function editAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
        }
        //$request = $this->getRequest();
        $this->_helper->layout->disableLayout();
    }

    public function accountsettingsAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
            $user_id = $userdata->user_id;
        }
        $userObj = new User_Model_User();
        $form = new User_Form_ProfileUploadForm();
        if($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                if(!empty($_FILES['photo']['name'])) {
                    $dir_upload = UPLOAD_PATH.'images/original/';
                    $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                    $medium_path = UPLOAD_PATH.'images/medium/';
                    $mobile_path = UPLOAD_PATH.'images/mobile/';
                    $android_path = UPLOAD_PATH.'images/android/';
                    $fileName = $_FILES['photo']['name'];
                    //$fullPathNameFile = $dir_upload.$fileName;
                    $image = new UpmeSocial_Controller_Action_Helper_Image();
                    $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
                    //Saving imagesedit to business_user
                    $data['profile_pic_path'] = $arrFileName['photo'];
                    $data['updated_date'] = date('Y-m-d H:i:s');
                    $album['user_id'] = $user_id;
                    $album['user_type'] = 'U';
                    $album['album_name'] = 'Profile Photos';
                    $mobileobj = new Mobile_Model_Albums();
                    //$mobileobj1 = new Mobile_Model_Photos();
                    $val = $mobileobj->albumnameexistsrnot($album);
                    $album['created_date'] = date('Y-m-d H:i:s');
                    $album['blasted_date'] = $album['created_date'];
                    if(!empty($val)) {
                        $albumdet = $mobileobj->getAlbumsByUserId($user_id, 'U');
                        $id = $mobileobj->update($album,$albumdet['0']['id']);
                        $albumid = $albumdet['0']['id'];
                    } else {
                        $album['is_default'] = '1';
                        $albumid = $mobileobj->insert($album);
                    }
                    $photoObj = new Mobile_Model_Photos();
                    $coverphoto = $photoObj->albumidexistsrnot($albumid,$user_id,'U');
                    $photodata['album_id'] = $albumid;
                    $photodata['photo_name'] = 'Profile Photo';
                    $photodata['photo_original_name'] = $fileName;
                    $photodata['photo_modified_name'] = $data['profile_pic_path'];
                    $photodata['width'] = $arrFileName['width'];
                    $photodata['height'] = $arrFileName['height'];
                    $photodata['android_width'] = $arrFileName['android_width'];
                    $photodata['android_height'] = $arrFileName['android_height'];
                    $photodata['user_id'] = $user_id;
                    $photodata['user_type'] = 'U';
                    if(!empty($coverphoto['photo_id']))
                        $photodata['cover_photo'] = '0';
                    else
                        $photodata['cover_photo'] = '1';
                    $photodata['created_date'] = date('Y-m-d H:i:s');
                    $photoObj->insert($photodata);
                    $id = $userObj->updateUser($data, $user_id);
                    /** Setting Updated Images data to sessions **/
                    $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
                    $user = $auth->getIdentity();
                    $user->profile_pic_path = $data['profile_pic_path'];
                    $auth->getStorage()->write($user);
                    if($id == '1')
                        echo 'success';exit;
                }
            }
        }
        $userdata = $userObj->getUserDetails($user_id);
        //Fetching Countries
        $countriesObj = new User_Model_Countries();
        $countries = $countriesObj->get_countries();
        $countries = $countries->toArray();
        //Fetching States Based On CountryId
        $statesObj = new User_Model_States();
        $states = $statesObj->get_states($userdata->country);
        //Fetching Cities Based On StateId
        $cityObj = new User_Model_Cities();
        $cities = $cityObj->get_cities($userdata->state);
        //Fetching Zipcode Basedon CityId
        $zipcodes = $cityObj->getZipcodesByCity($userdata->city);
        $schoolOrder = "school_name ASC";
        $schools = $userObj->getSchools($schoolOrder);
        $collegeOrder = "college_name ASC";
        $colleges = $userObj->getColleges($collegeOrder);
        $workplaces = $userObj->getWorkPlaces();
        /**** Fetching Languages ****/
        $languagesObj = new User_Model_Languages();
        $languages = $languagesObj->get_languages();
        /**** Fetching Timezones ****/
        $timezonesObj = new User_Model_Timezones();
        $timezones = $timezonesObj->get_timezones();
        $this->_helper->layout->disableLayout();
        $this->view->assign('form',$form);
        $this->view->assign('countries',$countries);
        $this->view->assign('states',$states);
        $this->view->assign('cities',$cities);
        $this->view->assign('zipcode',$zipcodes);
        $this->view->assign('languages',$languages);
        $this->view->assign('timezones',$timezones);
        $this->view->assign('schools', $schools);
        $this->view->assign('colleges', $colleges);
        $this->view->assign('workplaces', $workplaces);
        //echo '<pre>';print_r($userdata);exit;
        $this->view->assign('userdata',$userdata);
    }

    public function myuppersAction() {
        $this->_helper->layout->disableLayout();
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
        }
        $purchasecouponsObj = new Business_Model_Purchasecoupons;
        $puchasecoupons = $purchasecouponsObj->getpurchasecouponsinfo($userdata->user_id);
        $this->view->assign('purchasecoupons',$puchasecoupons);
        $this->view->assign('userdata',$userdata);
    }

    public function deactiveAction() {//echo 'hi<pre>';print_R($_SESSION);
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
        }
        $this->view->assign('userdata', $userdata);
    }

    public function useractiveAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
        $user_id = $this->getRequest()->getParam('user_id');
        if($user_id == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        $user_account_status = $this->getRequest()->getParam('user_account_status');
        if($user_account_status == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Account Status can not be Empty!!!'));
            return;
        }
        $mkey = $this->getRequest()->getParam('mkey');
        /***** To update User Status ****/
        $updateAry = array();
        if($user_account_status == 'A') {
            $updateAry['user_account_status'] = $user_account_status;
            $userObj = new User_Model_User();
            $userObj->updateUser($updateAry, $user_id);
            /** Setting Updated Images data to sessions **/
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $user = $auth->getIdentity();
            $user->user_account_status = $user_account_status;
            $auth->getStorage()->write($user);
            if(!empty($mkey)) {
                echo $myjson->customEncode(array('service_status'=>'success',"content" =>'Successfully Activated..!'));
                return;
            } else {
                echo "success";exit;
            }
        } else {
            if(!empty($mkey)) {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
            } else {
                echo "error";exit;
            }
        }
    }

    public function viewfollowersAction() {
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if($auth->hasIdentity()) {
            $userdata = $auth->getIdentity();
            if($request->getParam('user_id') == '') {
                $userid = $auth->getIdentity()->user_id;
            } else {
                $userid = $request->getParam('user_id');
            }
        } else {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
            $businessdata = $auth->getIdentity();
            if($request->getParam('user_id') == '') {
                $userid = $auth->getIdentity()->business_id;
            } else {
                $userid = $request->getParam('user_id');
            }
        }
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):5);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);

        // FOr Other USer Calculation
        if(!empty($userdata)) {
            $loggedUserId   = $auth->getIdentity()->user_id;
            if($auth->getIdentity()->user_type == "B")
                $loggedUserType = "B";
            else
                $loggedUserType = "U";
        } elseif(!empty($businessdata)) {
            $loggedUserId = $businessdata->business_id;
            $loggedUserType = $businessdata->user_type;
        } else {
            $loggedUserId = $request->getParam('user_id');
            $loggedUserType = "U";
        }
        $followObj = new User_Model_Followers();
        //echo $loggedUserId.'<pre>'.$userid;exit;
        if($loggedUserId == $userid) { // if logged user seeing his followers
            $userfollowers = $followObj->getMyFollowers($userid, $loggedUserId, $limit, $pagenum, "U");
            $userfollowing = $followObj->getMyFollowing($userid, $limit, $pagenum,"U");
            $loggedUserFollowing = $followObj->getMyFollowing($loggedUserId, 0, 0, "U");
            $followingCnt = count($loggedUserFollowing);
            if(count($userfollowers) > 0){
                foreach($userfollowers as $key => $followingUsr) {
                    if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                            if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                $userfollowers[$key]['isFollowing'] = "1";
                                break;
                            }  else {
                                $userfollowers[$key]['isFollowing'] = "0";
                            }
                        }
                    } else {
                        $userfollowers[$key]['isFollowing'] = "0";
                    }
                }
            }
        } else {
            // if Logged user is viewing other user's followers, CHecking following permissions for logged user with other user followers
            if($loggedUserType == 'B') {
                $loggedUserType = 'U';
            }
            $userfollowers = $followObj->getMyFollowers($userid, $loggedUserId, $limit, $pagenum, $loggedUserType);
            $loggedUserFollowing = $followObj->getMyFollowing($loggedUserId, 0, 0, "U");
            $followingCnt = count($loggedUserFollowing);
            if(count($userfollowers) > 0) {
                foreach($userfollowers as $key => $followingUsr) {
                    if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                            //echo $followingUsr['user_id']."--".$loggedUserId."----".$followingUsr['user_type']."-->".$loggedUserType;exit;
                            if($followingUsr['user_id'] == $loggedUserId && $followingUsr['user_type'] == $loggedUserType) {
                                $userfollowers[$key]['isFollowing'] = "2";
                            } else {
                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                   $userfollowers[$key]['isFollowing'] = "1";
                                   break;
                                } else {
                                   $userfollowers[$key]['isFollowing'] = "0";
                                }
                            }
                        }
                    } else {
                        $userfollowers[$key]['isFollowing'] = "0";
                    }
                }
            }
        }
        $this->view->limit = $limit;
        $this->view->pagenum = $pagenum;
        $this->_helper->layout->disableLayout();
        $this->view->assign('userfollowers', $userfollowers);
    }

    public function viewfollowingAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $request = $this->getRequest();
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):5);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);
        if($auth->hasIdentity()) {
            $userdata = $auth->getIdentity();
            if($request->getParam('user_id') == '') {
                $userid = $auth->getIdentity()->user_id;
                $user_type = 'U';
            } else {
                $userid = $request->getParam('user_id');
                $user_type = 'U';
            }
        } else {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
            $businessdata = $auth->getIdentity();
            if($request->getParam('user_id') == '') {
                $userid = $businessdata->business_id;
                $user_type = $businessdata->user_type;
            } else {
                $userid = $request->getParam('user_id');
                $user_type = 'U';
            }
        }
        // FOr Other USer Calculation
        if(!empty($userdata)) {
            $loggedUserId   = $auth->getIdentity()->user_id;
            if($auth->getIdentity()->user_type == "B")
                $loggedUserType = "B";
            else
                $loggedUserType = "U";
        } elseif(!empty($businessdata)) {
            $loggedUserId = $businessdata->business_id;
            $loggedUserType = $businessdata->user_type;
        } else {
            $loggedUserId = $request->getParam('user_id');
            $loggedUserType = 'U';
        }
        $followObj = new User_Model_Followers();
        if($loggedUserId == $userid) { // if logged user seeing whom he follow
            $userfollowing = $followObj->getMyFollowing($userid, $limit, $pagenum, 'U');
            if(count($userfollowing) > 0) {
                foreach($userfollowing as $key3=>$usrfollow) {
                    $userfollowing[$key3]['isFollowing'] = "1";
                }
            }
        } else {
            $userfollowing = $followObj->getMyFollowing($userid, $limit, $pagenum, 'U');
            if($_SERVER['REMOTE_ADDR']=='192.168.1.57') {
                //echo '2<pre>';print_r($userfollowing);exit;
            }
            $loggedUserFollowing = $followObj->getMyFollowing($loggedUserId, 0, 0, $loggedUserType);
            $loggedUserFollowingCnt = count($loggedUserFollowing);
            if(count($userfollowing) > 0) {
                foreach($userfollowing as $key => $viewUsrFollowing) {
                    if($loggedUserFollowingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $loggedUsrFollowing) {
                            if($viewUsrFollowing['follower_id'] == $loggedUserId && $viewUsrFollowing['follower_type'] == $loggedUserType) {
                                $userfollowing[$key]['isFollowing'] = "2";
                            } else {
                                if(($viewUsrFollowing['follower_id'] == $loggedUsrFollowing['follower_id']) && ($viewUsrFollowing['follower_type'] == $loggedUsrFollowing['follower_type'])) {
                                   $userfollowing[$key]['isFollowing'] = "1";
                                   break;
                                } else {
                                   $userfollowing[$key]['isFollowing'] = "0";
                                }
                            }
                        }
                    } else { //if logged  user is not following anyone
                        foreach($userfollowing as $key => $viewUsrFollowing) {
                            if($viewUsrFollowing['follower_id'] == $loggedUserId && $viewUsrFollowing['follower_type'] == $loggedUserType) {
                                $userfollowing[$key]['isFollowing'] = "2";
                            } else {
                                $userfollowing[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                }
            }
        }
        $this->view->limit = $limit;
        $this->view->pagenum = $pagenum;
        $this->_helper->layout->disableLayout();
        $this->view->assign('userfollowing', $userfollowing);
    }

    public function makefriendsAction() {
        /***** To update User Status ****/
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $userdata = $auth->getIdentity();
        $updateAry = array();
        $resAry = array();
        if($request->getParam('user_id') != '') {
            $updateAry['user_id'] = $request->getParam('user_id');
            $updateAry['user_type'] = $request->getParam('user_type');
        } else {
            $updateAry['user_id'] = $userdata->user_id;
            $updateAry['user_type'] = 'U';
        }
        if(isset($request)) {
            $updateAry['follower_id'] = $request->getParam('follower_id');
            $updateAry['follower_type'] = $request->getParam('follower_type');
            if($updateAry['user_type'] == 'B' && $request->getParam('follower_type') == 'U') {
                echo json_encode(array('service_status'=>'error',"error-message" => 'Business User can not follow Normal User!!!'));
                return;
            } else {
                $followObj = new User_Model_Followers();

                 // CHech if alredy user id FOlowing
                $followObj1 = new Business_Model_Followers();
                $duplicationArray = $followObj1->checkDuplicationFollow($updateAry['follower_id'], $updateAry['follower_type'], $updateAry['user_id'], $updateAry['user_type']);
                if(count($duplicationArray) == 0) {
                    $fid = $followObj->insertNewRecord($updateAry);
                    if($updateAry['follower_type'] == 'U' || $updateAry['follower_type'] == 'B') {
                        $resAry['reference_id'] = $fid;
                        $resAry['user_id'] = $request->getParam('follower_id');
                        $resAry['user_type'] = $request->getParam('follower_type');
                        $resAry['notifications_type'] = 'following';
                        $resAry['action_date'] = date('Y-m-d H:i:s');
                        $notificationObj = new Notifications_Model_Notifications();
                        $id = $notificationObj->insertNewRecord($resAry);
                        $resAry['reference_id'] = $fid;
                        $resAry['user_id'] = $updateAry['user_id'];
                        $resAry['user_type'] = $updateAry['user_type'];
                        $resAry['notifications_type'] = 'Followed';
                        $resAry['action_date'] = date('Y-m-d H:i:s');
                        $notificationObj = new Notifications_Model_Notifications();
                        $id = $notificationObj->insertNewRecord($resAry);

                        /****  Upgrade User Level if cool points & Follower/ following criteria reaches Limits ****/
                        $usrLevelObj = new User_Model_Userlevels();
                        $scrObj = new Scribbles_Model_Scribbles();

                        //Get User Level
                        $userLevel = $usrLevelObj->getUserLevelByUserId($updateAry['user_id'], $updateAry['user_type']);
                        //Get All Levels details FROM DB
                        $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                        $followerObj = new User_Model_Followers();
                        // to get Followers Count of logged User
                        $followersCnt = $followerObj->getFollowersCnt($updateAry['user_id'], $updateAry['user_type']);

                        // to Get Following Count of the Logged User
                        $followingCnt = $followerObj->getFollowingCnt($updateAry['user_id'], $updateAry['user_type']);
                        //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                        foreach($allUserLevelsArray as $level) {
                            if((($userLevel[0]['cool_points'] >= $level['minimum_coolpoints']  && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']))) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == ($level['level_no']-1)){
                                    $upgradeLevel = $level['level_no'];
                                    //Upgrade User Level
                                    $usrObj = new User_Model_User();
                                    $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                    // Update user level in Downlined scribbles also.
                                    $downlineUserLevel = $upgradeLevel;
                                    $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                    $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                    $lvlAry['level'] = $upgradeLevel;
                                    $lvlAry['level_status'] = 'Up';
                                    $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                    if(!empty($id)) {
                                        $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                    } else {
                                        $lvlid = $usrObj->insertleveldetails($lvlAry);
                                    }

                                    //Mail to be sent to the user
        //                            $m = new UpmeSocial_HtmlMailer();
        //                            $m->setSubject("UPMEsocial User Level Degrade");
        //                            $m->addTo($userLevel[0]['email'])
        //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
        //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
        //                                ->setViewParam('new_level',$upgradeLevel)
        //                                ->setViewParam('email',$userLevel[0]['email'])
        //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
        //                            $m->sendHtmlTemplate("level_degrade.phtml");
                            }
                        }

                         /****  Upgrade User Level if cool points & Follower/ following criteria reaches Limits ****/
                        $usrLevelObj = new User_Model_Userlevels();
                        $scrObj = new Scribbles_Model_Scribbles();

                        //Get User Level
                        $userLevel = $usrLevelObj->getUserLevelByUserId($updateAry['follower_id'], $updateAry['follower_type']);
                        //Get All Levels details FROM DB
                        $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                        $followerObj = new User_Model_Followers();
                        // to get Followers Count of logged User
                        $followersCnt = $followerObj->getFollowersCnt($updateAry['follower_id'], $updateAry['follower_type']);

                        // to Get Following Count of the Logged User
                        $followingCnt = $followerObj->getFollowingCnt($updateAry['follower_id'], $updateAry['follower_type']);
                        //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                        foreach($allUserLevelsArray as $level) {
                            if((($userLevel[0]['cool_points'] >= $level['minimum_coolpoints']  && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']))) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == ($level['level_no']-1)){
                                    $upgradeLevel = $level['level_no'];
                                    //Upgrade User Level
                                    $usrObj = new User_Model_User();
                                    $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                    // Update user level in Downlined scribbles also.
                                    $downlineUserLevel = $upgradeLevel;
                                    $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                    $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                    $lvlAry['level'] = $upgradeLevel;
                                    $lvlAry['level_status'] = 'Up';
                                    $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                    if(!empty($id)) {
                                        $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                    } else {
                                        $lvlid = $usrObj->insertleveldetails($lvlAry);
                                    }

                                    //Mail to be sent to the user
        //                            $m = new UpmeSocial_HtmlMailer();
        //                            $m->setSubject("UPMEsocial User Level Degrade");
        //                            $m->addTo($userLevel[0]['email'])
        //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
        //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
        //                                ->setViewParam('new_level',$upgradeLevel)
        //                                ->setViewParam('email',$userLevel[0]['email'])
        //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
        //                            $m->sendHtmlTemplate("level_degrade.phtml");
                            }
                        }

                    }
                    echo json_encode(array('service_status'=>'success',"message" => 'Followed Successfully!!!'));
                    return;
                }
            }
        } else {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Error in following process!!!'));
            return;
        }
        exit(0);
    }

    public function makeunfriendsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $userdata = $auth->getIdentity();
        $updateAry = array();
        if($request->getParam('user_id') != '') {
            $user_id = $request->getParam('user_id');
            $user_type = $request->getParam('user_type');
        } else {
            $user_id = $userdata->user_id;
            $user_type = 'U';
        }
        if(isset($request)) {
            $follower_id = $request->getParam('follower_id');
            $follower_type = $request->getParam('follower_type');
            if($user_type == 'B' && $follower_type == 'U') {
                echo "error";exit;
            } else {
                $followObj = new User_Model_Followers();
                $val = $followObj->deletefollower($user_id,$user_type,$follower_id,$follower_type);
                if($val == '1') {

                    /****  Upgrade User Level if cool points & Follower/ following criteria reaches Limits ****/
                    $usrLevelObj = new User_Model_Userlevels();
                    $scrObj = new Scribbles_Model_Scribbles();
                    //Get User Level
                    $userLevel = $usrLevelObj->getUserLevelByUserId($user_id, $user_type);
                    //Get All Levels details FROM DB
                    $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                    $followerObj = new User_Model_Followers();
                    // to get Followers Count of logged User
                    $followersCnt = $followerObj->getFollowersCnt($user_id, $user_type);

                    // to Get Following Count of the Logged User
                    $followingCnt = $followerObj->getFollowingCnt($user_id, $user_type);
                    //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                    foreach($allUserLevelsArray as $level) {
                        if((($userLevel[0]['cool_points'] < $level['minimum_coolpoints'] || $followersCnt < $level['minimum_followers']) || ($followingCnt < $level['minimum_followings'])) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == $level['level_no']){
                                $upgradeLevel = $userLevel[0]['user_level']-1;
                                //Upgrade User Level
                                $usrObj = new User_Model_User();
                                $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                // Update user level in Downlined scribbles also.
                                $downlineUserLevel = $upgradeLevel;
                                $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                $lvlAry['level'] = $upgradeLevel;
                                $lvlAry['level_status'] = 'Down';
                                $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                if(!empty($id)) {
                                    $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                } else {
                                    $lvlid = $usrObj->insertleveldetails($lvlAry);
                                }

                                //Mail to be sent to the user
    //                            $m = new UpmeSocial_HtmlMailer();
    //                            $m->setSubject("UPMEsocial User Level Degrade");
    //                            $m->addTo($userLevel[0]['email'])
    //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
    //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
    //                                ->setViewParam('new_level',$upgradeLevel)
    //                                ->setViewParam('email',$userLevel[0]['email'])
    //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
    //                            $m->sendHtmlTemplate("level_degrade.phtml");
                        }
                    }

                    /****  Upgrade User Level if cool points & Follower/ following criteria reaches Limits ****/
                    //Get User Level
                    $userLevel = $usrLevelObj->getUserLevelByUserId($follower_id, $follower_type);
                    //Get All Levels details FROM DB
                    $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();

                    // to get Followers Count of logged User
                    $followersCnt = $followerObj->getFollowersCnt($follower_id, $follower_type);

                    // to Get Following Count of the Logged User
                    $followingCnt = $followerObj->getFollowingCnt($follower_id, $follower_type);
                    //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
                    foreach($allUserLevelsArray as $level) {
                        if((($userLevel[0]['cool_points'] < $level['minimum_coolpoints'] || $followersCnt < $level['minimum_followers']) || ($followingCnt < $level['minimum_followings'])) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == $level['level_no']){
                                $upgradeLevel = $userLevel[0]['user_level']-1;
                                //Upgrade User Level
                                $usrObj = new User_Model_User();
                                $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                                // Update user level in Downlined scribbles also.
                                $downlineUserLevel = $upgradeLevel;
                                $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                                $lvlAry['user_id'] = $userLevel[0]['user_id'];
                                $lvlAry['level'] = $upgradeLevel;
                                $lvlAry['level_status'] = 'Down';
                                $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                                if(!empty($id)) {
                                    $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                                } else {
                                    $lvlid = $usrObj->insertleveldetails($lvlAry);
                                }

                                //Mail to be sent to the user
    //                            $m = new UpmeSocial_HtmlMailer();
    //                            $m->setSubject("UPMEsocial User Level Degrade");
    //                            $m->addTo($userLevel[0]['email'])
    //                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
    //                                ->setViewParam('old_level',$userLevel[0]['user_level'])
    //                                ->setViewParam('new_level',$upgradeLevel)
    //                                ->setViewParam('email',$userLevel[0]['email'])
    //                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
    //                            $m->sendHtmlTemplate("level_degrade.phtml");
                        }
                    }

                    echo "success";exit;
                } else {
                    echo "error";exit;
                }
            }
        } else {
            echo "error";exit;
        }
    }

    public function viewalbumsAction() {
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if($auth->hasIdentity()) {
            if($request->getParam('user_id') == '') {
                $userid = $auth->getIdentity()->user_id;
            } else {
                $userid = $request->getParam('user_id');
            }
            if($auth->getIdentity()->user_type == "B")
                $user_type = "B";
            else
                $user_type = "U";
            $mobileobj = new Mobile_Model_Albums();
            $albums = $mobileobj->getAlbumsByUserId($userid,$user_type);
            $this->view->assign('albums',$albums);
            $userObj = new User_Model_User();
            $firstname = $userObj->getfirstname($userid, $user_type);
            $this->view->assign('firstname',$firstname);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->_helper->layout->disableLayout();
        } else {
            $userid = $request->getParam('user_id');
            $user_type = 'U';
            $mobileobj = new Mobile_Model_Albums();
            $albums = $mobileobj->getAlbumsByUserId($userid,$user_type);
            $this->view->assign('albums',$albums);
            $userObj = new User_Model_User();
            $firstname = $userObj->getfirstname($userid, $user_type);
            $this->view->assign('firstname',$firstname);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->_helper->layout->disableLayout();
        }
    }

    public function viewalbumphotosAction() {
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if($auth->hasIdentity()) {
            if($request->getParam('user_id') == '') {
                $userid = $auth->getIdentity()->user_id;
            } else {
                $userid = $request->getParam('user_id');
            }
            if($auth->getIdentity()->user_type == "B")
                $user_type = "B";
            else
                $user_type = "U";
            $album_id = $this->getRequest()->getParam('album_id');
            $mobileobj = new Mobile_Model_Albums();
            $photos = $mobileobj->getalbumphotos($album_id, $userid, $user_type);
            $this->view->assign('photos',$photos);
            $userObj = new User_Model_User();
            $firstname = $userObj->getfirstname($userid, $user_type);
            $this->view->assign('firstname',$firstname);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->view->assign('album_id',$album_id);
            $this->_helper->layout->disableLayout();
        } else {
            $userid = $request->getParam('user_id');
            $user_type = 'U';
            $album_id = $this->getRequest()->getParam('album_id');
            $mobileobj = new Mobile_Model_Albums();
            $photos = $mobileobj->getalbumphotos($album_id, $userid, $user_type);
            $this->view->assign('photos',$photos);
            $userObj = new User_Model_User();
            $firstname = $userObj->getfirstname($userid, $user_type);
            $this->view->assign('firstname',$firstname);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->view->assign('album_id',$album_id);
            $this->_helper->layout->disableLayout();
        }
    }

    public function viewalbumphotowithcommentsAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if($auth->hasIdentity()) {
            if($this->getRequest()->getParam('user_id') == '') {
                $userid = $auth->getIdentity()->user_id;
            } else {
                $userid = $this->getRequest()->getParam('user_id');
            }
            if($auth->getIdentity()->user_type == "B")
                $user_type = "B";
            else
                $user_type = "U";
            $logged_user_id = $auth->getIdentity()->user_id;
            $album_id = $this->getRequest()->getParam('album_id');
            $photo_id = $this->getRequest()->getParam('photo_id');
            $limit = 5;
            $photoObj = new Photos_Model_Photos();
            $photo = $photoObj->getphotowithcomments($logged_user_id, $user_type, $photo_id, $limit);
            $commentscnt = $photoObj->getPhotoCommentCnt($photo_id);
            $photo = $photo->toArray();
            $this->view->assign('photo',$photo);
            $this->view->assign('limit',$limit);
            $this->view->assign('commentcnt',$commentscnt['COUNT']);
            $this->view->assign('album_id',$album_id);
            $this->view->assign('photo_id',$photo_id);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->_helper->layout->disableLayout();
        } else {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
            $businessdata = $auth->getIdentity();
            $userid = $businessdata->business_id;
            $user_type = $businessdata->user_type;
            $album_id = $this->getRequest()->getParam('album_id');
            $photo_id = $this->getRequest()->getParam('photo_id');
            $limit = 5;
            $photoObj = new Photos_Model_Photos();
            $photo = $photoObj->getphotowithcomments($userid, $user_type, $photo_id, $limit);
            $commentscnt = $photoObj->getPhotoCommentCnt($photo_id);
            $photo = $photo->toArray();
            $this->view->assign('photo',$photo);
            $this->view->assign('limit',$limit);
            $this->view->assign('commentcnt',$commentscnt['COUNT']);
            $this->view->assign('album_id',$album_id);
            $this->view->assign('photo_id',$photo_id);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->_helper->layout->disableLayout();
        }
    }

    public function addphotocommentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $data['user_id'] = $this->getRequest()->getParam('user_id');
        $data['user_type'] = $this->getRequest()->getParam('user_type');
        $data['photo_id'] = $this->getRequest()->getParam('photo_id');
        $data['comments'] = $this->getRequest()->getParam('comments');
        $data['commented_date'] = date('Y:m:d H:i:s');
        $photoObj = new Photos_Model_Photocomments();
        $id = $photoObj->savephotocomment($data);
        if(!empty($id)) {
                echo 'success';
        } else {
                echo 'fail';
        }
    }

    public function viewcommentsAction() {
        $photo_id = $this->getRequest()->getParam('photo_id');
        $user_id = $this->getRequest()->getParam('user_id');
        $user_type = $this->getRequest()->getParam('user_type');
        $limit = $this->getRequest()->getParam('limit');
        $photoObj = new Photos_Model_Photos();
        $photo = $photoObj->getphotowithcomments($user_id, $user_type, $photo_id,$limit);
        $commentscnt = $photoObj->getPhotoCommentCnt($photo_id);
        $photo = $photo->toArray();
        $this->view->assign('photo',$photo);
        $this->view->assign('limit',$limit);
        $this->view->assign('commentcnt',$commentscnt['COUNT']);
        $this->_helper->layout->disableLayout();
    }

    public function viewvideosAction() {
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if(!$auth->hasIdentity()) {
            $this->_helper->redirector('index','index','user');
        }
        if($request->getParam('user_id') =='') {
            $user_id = $auth->getIdentity()->user_id;
        } else {
            $user_id = $request->getParam('user_id');
        }
        $videoObj = new Videos_Model_Videos();
        $videos = $videoObj->getvideos($user_id);
        $this->view->assign('videos',$videos);
        $this->view->assign('user_id',$user_id);
        //$this->view->assign('user_type',$user_type);
        $this->_helper->layout->disableLayout();
    }

    public function viewvideowithcommentsAction() {
        $request = $this->getRequest();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if($auth->hasIdentity()) {
            if($request->getParam('user_id') == '') {
                $userid = $auth->getIdentity()->user_id;
            } else {
                $userid = $request->getParam('user_id');
            }
            if($auth->getIdentity()->user_type == "B")
                $user_type = "B";
            else
                $user_type = "U";
            $limit = 5;
            $logged_user_id = $auth->getIdentity()->user_id;
            $video_id = $this->getRequest()->getParam('video_id');
            $videoObj = new Videos_Model_Videos();
            $video = $videoObj->getvideowithcomments($video_id,$limit,$userid,$user_type);
            $commentscnt = $videoObj->getVideoCommentCnt($video_id);
            $videos = $video->toArray();
            $this->view->assign('video',$videos);
            $this->view->assign('limit',$limit);
            $this->view->assign('commentcnt',$commentscnt['COUNT']);
            $this->view->assign('video_id',$video_id);
            $this->view->assign('logged_user_id',$logged_user_id);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->_helper->layout->disableLayout();
        } else {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
            $businessdata = $auth->getIdentity();
            $userid = $businessdata->business_id;
            $user_type = $businessdata->user_type;
            $limit = 5;
            $video_id = $this->getRequest()->getParam('video_id');
            $videoObj = new Videos_Model_Videos();
            $video = $videoObj->getvideowithcomments($video_id,$limit,$userid,$user_type);
            $commentscnt = $videoObj->getVideoCommentCnt($video_id);
            $videos = $video->toArray();
            $this->view->assign('video',$videos);
            $this->view->assign('limit',$limit);
            $this->view->assign('commentcnt',$commentscnt['COUNT']);
            $this->view->assign('video_id',$video_id);
            $this->view->assign('user_id',$userid);
            $this->view->assign('user_type',$user_type);
            $this->_helper->layout->disableLayout();
        }
    }

    public function addvideocommentAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $data['user_id'] = $this->getRequest()->getParam('user_id');
        $data['user_type'] = $this->getRequest()->getParam('user_type');
        $data['video_id'] = $this->getRequest()->getParam('video_id');
        $data['comments'] = $this->getRequest()->getParam('comments');
        $data['commented_date'] = date('Y-m-d H:i:s');
        $photoObj = new Videos_Model_Videocomments();
        $id = $photoObj->savevideocomment($data);
        if(!empty($id)) {
            echo 'success';
        } else {
            echo 'fail';
        }
    }

    public function viewvideocommentsAction() {
        //echo '<pre>';print_r($this->getRequest()->getParams());exit;
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $loggedUserType = "U";
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $loggedUserType = "B";
        }
        $video_id = $this->getRequest()->getParam('video_id');
        $limit = $this->getRequest()->getParam('limit');
        $videoObj = new Videos_Model_Videos();
        $video = $videoObj->getvideowithcomments($video_id,$limit,$loggedUserId,$loggedUserType);
        $commentscnt = $videoObj->getVideoCommentCnt($video_id);
        $video = $video->toArray();
        $this->view->assign('video',$video);
        $this->view->assign('limit',$limit);
        $this->view->assign('commentcnt',$commentscnt['COUNT']);
        $this->_helper->layout->disableLayout();
    }

    public function deletealbumAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $album_id = $this->getRequest()->getParam('album_id');
        $mobileobj = new Mobile_Model_Albums();
        $data = $mobileobj->albumexistsrnot($album_id);
        if(!empty($data)) {
            $albums = $mobileobj->deleteAlbum($album_id);
            echo '1';
        } else {
            echo '0';
        }
    }

    public function deletephotoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $photo_id = $this->getRequest()->getParam('photo_id');
        $userObj = new Mobile_Model_Photos();
        $data = $userObj->getphotobyphotoid($photo_id);
        if(!empty($data)) {
            $photos = $userObj->deletePhoto($photo_id);
            echo '1';
        } else {
            echo '0';
        }
    }

    public function deletevideoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $video_id = $this->getRequest()->getParam('video_id');
        $videoObj = new Videos_Model_Videos();
        $videos = $videoObj->getvideosbyvideoid($video_id);
        if(!empty($videos)) {
            $videodata = $videoObj->deleteVideo($videos);
            echo '1';
        } else {
            echo '0';
        }
    }

    public function dashboardAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $loggedUserType = "U";
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $loggedUserType = "B";
        }
        //echo "<pre>";print_r($logged_user_data);exit;
        // check if the session exist, else rerirect to Home page
        if($loggedUserId == '' && $loggedUserType == "U"){
            $this->_helper->redirector('index','index','user');
        }

        if($loggedUserId != '' && $loggedUserType == "B"){
            $this->_redirect('business/businessprofile');
        }

        $request = $this->getRequest();
        $userObj = new User_Model_User();
        if($request->getParam('user_id') ==''){
            if(!empty($logged_user_data->facebook_unique_id)) {
                $uniqueId= $logged_user_data->facebook_unique_id;
                $email =$logged_user_data->email;
                $userdata = $userObj->getFbUserDetails($uniqueId, $email);
                $uid = $userdata->user_id;
                $uType = $userdata->user_type;
            } else {
                $userdata = $logged_user_data;
                $uid = $userdata->user_id;
                $uType = $userdata->user_type;
            }
        } else {
            $uid = $request->getParam('user_id');
            $userdata = $userObj->getUserDetails($uid);
            $uType = $userdata->user_type;
        }

        if($uType != 'B')
            $uType = "U";

        // Fetching User Cool POints
        $coolPoints = $userObj->getUserCoolPoints($uid, $uType);
        $this->view->coolPoints = $coolPoints;
        $this->view->loggedUserId = $loggedUserId;
        $this->view->loggedUserType = $loggedUserType;
        if(empty($userdata)) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->redirector('login');
            return;
        }
        $userlevelObj = new User_Model_Userlevels();
        $followObj = new User_Model_Followers();
        $scribbleObj = new Scribbles_Model_Scribbles();
        $limit = ($request->getParam('limit') != '')?$request->getParam('limit'):10;

        //To Get User Details
        $userDetAry = array();
        $userDetAry = $userObj->getUserDetails($uid);

        //To get User Level
        $level= $userDetAry->user_level;
        $userlevelAry = $userlevelObj->getUserLevels($level);

        //To Get User Level equivalent Or Lower Levels
        $userLowerLevelsAry = $userlevelObj->getEquivalentOrLowerLevelsBasedOnUserLevel($level);
        $userDetAry = $userDetAry->toArray();
        /*$userDetAry['nooffollowers'] = $followObj->getFollowersCnt($uid);
        $userDetAry['nooffollowings'] = $followObj->getFollowingCnt($uid);
        $userDetAry['scribblecount'] = $scribbleObj->getScribbleCnt($uid,$userDetAry['user_level']);*/
        $this->view->assign('userDetAry', $userDetAry);
        $this->view->assign('userlevelAry', $userlevelAry);

        //To Display Followers Album Photos at Top
//        $followersgallery = $followObj->getFollowersAlbumImages($loggedUserId, 'U');
//        $this->view->assign('followersgallery', $followersgallery);
        $school = $userObj->popularschool();
        $college = $userObj->popularcollege();
        $workplace = $userObj->popularworkplace();
        $this->view->assign('school',$school);
        $this->view->assign('college',$college);
        $this->view->assign('workplace',$workplace);
        if(!empty($userdata)) {
            $this->view->userdata = $userdata;
            $this->view->userlevelAry = $userlevelAry;
            $this->view->userLowerLevelsAry = $userLowerLevelsAry;
        } else {
            $this->_helper->redirector('index','index','user');
        }
    }

    public function blastphotoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $photo_id = $request->getParam('photo_id');
        if($photo_id == '') {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Photo Id can not be Empty!!!'));
            return;
        }

        if(!is_numeric($photo_id)) {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Invalid Photo Id!!!'));
            return;
        }

        $logged_User_Id = $request->getParam('logged_user_id');
        if($logged_User_Id == '') {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Logged User Id can not be Empty!!!'));
            return;
        }

        if(!is_numeric($logged_User_Id)) {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Invalid Logged User  Id!!!'));
            return;
        }

        $logged_User_Type = $request->getParam('logged_user_type');
        if($logged_User_Type == '') {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Logged User Type can not be Empty!!!'));
            return;
        }

        $photoObj = new Photos_Model_Photos();
        $followerObj = new User_Model_Followers();
        //Check User balst this Photo r not
        $availrnot = $photoObj->isAvailable($logged_User_Id,$logged_User_Type,$photo_id);
        if(!empty($availrnot)) {
                echo json_encode(array('service_status'=>'error', 'error_msg' => 'You have already Blast this Photo'));
                return;
        }
        //Checking For Limit Of the day
        $daycount = $photoObj->getdaycount($logged_User_Id,$logged_User_Type);

        $blastLimit = 50;
        if($_SERVER['SERVER_NAME'] == '192.168.1.57'){
            $blastLimit = PHOTO_BLAST_LIMIT;
        }

        if($daycount['count'] >= $blastLimit) {
                echo json_encode(array('service_status'=>'error', 'error_msg' => 'You have reached day limit for Blast Photo'));
                return;
        }

        // Get Photo Details By Photo ID
        $photoDetails = $photoObj->getPhotoDetailsByPhotoId($photo_id);
        $photoDetails = $photoDetails ->toArray();
        $photo_owner_id = $photoDetails[0]['user_id'];
        $photo_owner_type = $photoDetails[0]['user_type'];
        //Check Following Photo Owner r Not
        $isFollowingArr = $followerObj->isLoggedUserFollowingOtherUser($photo_owner_id, $photo_owner_type, $logged_User_Id, $logged_User_Type);
        if(empty($isFollowingArr)) {
                echo json_encode(array('service_status'=>'error', 'error_msg' => 'You need to follow the user to blast this photo'));
                return;
        }
        $insArray = array();
        $insArray['album_id'] = $photoDetails[0]['album_id'];
        $insArray['photo_original_name'] = $photoDetails[0]['photo_original_name'];
        $insArray['photo_modified_name'] = $photoDetails[0]['photo_modified_name'];
        $insArray['user_id'] = $logged_User_Id;
        $insArray['user_type'] = $logged_User_Type;
        $insArray['cover_photo'] = 0;
        $insArray['blast_id'] = $photoDetails[0]['photo_id'];
        $insArray['width'] = $photoDetails[0]['width'];
        $insArray['height'] = $photoDetails[0]['height'];
        $insArray['android_width'] = $photoDetails[0]['android_width'];
        $insArray['android_height'] = $photoDetails[0]['android_height'];
        $insArray['created_date'] = date('Y-m-d H:i:s');
        $insArray['blasted_date'] = $insArray['created_date'];
        $ins = $photoObj->blastPhoto($insArray);

        // Update Blasted Date of Original Photo with latest Blasted Date
        $updAry = array();
        $updAry['blasted_date'] = $insArray['blasted_date'];
        $updDate = $photoObj->updateParentPhotoBlastedDate($updAry, $photoDetails[0]['photo_id']);

        $resAry['reference_id'] = $ins;
        $resAry['user_id'] = $photo_owner_id;
        $resAry['user_type'] = $photo_owner_type;
        $resAry['notifications_type'] = 'photo_blasted';
        $resAry['action_date'] = date('Y-m-d H:i:s');
        $notificationObj = new Notifications_Model_Notifications();
        $ins1 = $notificationObj->insertNewRecord($resAry);
        $scrObj = new Scribbles_Model_Scribbles();
        if($ins) {
            $upd = $scrObj->updateCoolPoints($photo_owner_id, $photo_owner_type,"photoblast");

            /**** Upgrade User Level if cool points decreases Level Limits ****/
            $usrLevelObj = new User_Model_Userlevels();
            //Get User Level
            $userLevel = $usrLevelObj->getUserLevelByUserId($photo_owner_id, $photo_owner_type);
            //Get All Levels details
            $allUserLevelsArray   = $usrLevelObj->getAllTypesOfUserLevels();

            // to get Followers Count of logged User
            $followersCnt = $followerObj->getFollowersCnt($photo_owner_id, "U");

            // to Get Following Count of the Logged User
            $followingCnt = $followerObj->getFollowingCnt($photo_owner_id, "U");

            $levelArray = array();
            foreach ($allUserLevelsArray as $level) {
                    if(($userLevel[0]['cool_points'] >= $level['minimum_coolpoints'] && $level['minimum_coolpoints']+1 >= $userLevel[0]['cool_points']) && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']) && $userLevel[0]['user_level']!= $level['level_no']) {
                            $upgradeLevel = $level['level_no'];
                            // Upgrade User Level
                            $usrObj = new User_Model_User();
                            $upgrade = $usrObj->upgradeLevel($photo_owner_id,$upgradeLevel);
                            // Update user level in Downlined scribbles also.
                            $downlineUserLevel = $upgradeLevel;
                            $UpdArr   = $scrObj->updateDownlinedScribblesLevel($photo_owner_id, $downlineUserLevel);
                            $resAry['reference_id'] = $upgrade;
                            $resAry['user_id'] = $photo_owner_id;
                            $resAry['user_type'] = $photo_owner_type;
                            $resAry['notifications_type'] = 'level_upgrade';
                            $resAry['action_date'] = date('Y-m-d H:i:s');
                            $notificationObj = new Notifications_Model_Notifications();
                            $upgrade = $notificationObj->insertNewRecord($resAry);
                            $lvlAry['user_id'] = $photo_owner_id;
                            $lvlAry['level'] = $upgradeLevel;
                            $lvlAry['level_status'] = 'Up';
                            $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                            if(!empty($id)) {
                                $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                            } else {
                                $lvlid = $usrObj->insertleveldetails($lvlAry);
                            }

                            // Mail to be sent to the user
//                            $m = new UpmeSocial_HtmlMailer();
//                            $m->setSubject("UPMEsocial User Level Upgrade");
//                            $m->addTo($userLevel[0]['email'])
//                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
//                                ->setViewParam('old_level',$userLevel[0]['user_level'])
//                                ->setViewParam('new_level',$upgradeLevel)
//                                ->setViewParam('email',$userLevel[0]['email'])
//                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
//                            $m->sendHtmlTemplate("level_upgrade.phtml");
                    }
            }

            // get all Blasted Users Details


            $resultAry = array('service_status'=>'success',"content" => $ins);
        } else {
            $resultAry = array('service_status'=>'error-message',"content" =>'Error processing request');
        }
        echo json_encode($resultAry);
    }

    /*public function blastvideoAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $video_id = $request->getParam('video_id');
        if($video_id == '') {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Video Id can not be Empty!!!'));
            return;
        }

        if(!is_numeric($video_id)) {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Invalid Video Id!!!'));
            return;
        }

        $logged_User_Id = $request->getParam('logged_user_id');
        if($logged_User_Id == '') {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Logged User Id can not be Empty!!!'));
            return;
        }

        if(!is_numeric($logged_User_Id)) {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Invalid Logged User  Id!!!'));
            return;
        }

        $logged_User_Type = $request->getParam('logged_user_type');
        if($logged_User_Type == '') {
            echo json_encode(array('service_status'=>'error',"error-message" => 'Logged User Type can not be Empty!!!'));
            return;
        }

        $videoObj = new Videos_Model_Videos();
        //Check User balst this Video r not
        $availrnot = $videoObj->isAvailable($logged_User_Id,$logged_User_Type,$photo_id);
        //echo '<pre>';print_R($availrnot);exit;
        if(!empty($availrnot)) {
                echo json_encode(array('service_status'=>'error', 'error_msg' => 'you have already Blast this Video'));
                return;
        }
        //Checking For Limit Of the day
        $daycount = $videoObj->getdaycount($logged_User_Id,$logged_User_Type);
        //echo '<pre>';print_r($daycount);exit;
        if($daycount['count'] >= 10) {
                echo json_encode(array('service_status'=>'error', 'error_msg' => 'you have reached day limit for Blast Video'));
                return;
        }
        $videoDetails = $videoObj->getvideosbyvideoid($photo_id);
        $video_owner_id = $videoDetails['user_id'];
        $video_owner_type = $videoDetails['user_type'];
        $insArray = array();
        $insArray['video_name'] = $videoDetails['video_name'];
        $insArray['video_path'] = $videoDetails['video_path'];
        $insArray['thumbnail_path'] = $videoDetails['thumbnail_path'];
        $insArray['user_id'] = $logged_User_Id;
        $insArray['user_type'] = $logged_User_Type;
        $insArray['blast_id'] = $videoDetails['video_id'];
        $insArray['created_date'] = date('Y-m-d H:i:s');
        $videoObj = new Videos_Model_Videos();
        $ins = $videoObj->blastVideo($insArray);

        // Update Blasted Date of Original Photo with latest Blasted Date
        $updAry = array();
        $updAry['blasted_date'] = $insArray['blasted_date'];
        $updDate = $photoObj->updateParentPhotoBlastedDate($updAry, $videoDetails[0]['video_id']);

        $resAry['reference_id'] = $ins;
        $resAry['user_id'] = $video_owner_id;
        $resAry['user_type'] = $video_owner_type;
        $resAry['notifications_type'] = 'video_blasted';
        $resAry['action_date'] = date('Y-m-d H:i:s');
        $notificationObj = new Notifications_Model_Notifications();
        $ins1 = $notificationObj->insertNewRecord($resAry);
        $scrObj = new Scribbles_Model_Scribbles();
        if($ins) {
            $upd = $scrObj->updateCoolPoints($video_owner_id, $video_owner_type,"videoblast");

            /**** Upgrade User Level if cool points decreases Level Limits ****/
            /*$usrLevelObj = new User_Model_Userlevels();
            //Get User Level
            $userLevel = $usrLevelObj->getUserLevelByUserId($video_owner_id, $video_owner_type);
            //Get All Levels details
            $allUserLevelsArray   = $usrLevelObj->getAllTypesOfUserLevels();

            $followerObj = new User_Model_Followers();
            // to get Followers Count of logged User
            $followersCnt = $followerObj->getFollowersCnt($logged_User_Id, "U");

            // to Get Following Count of the Logged User
            $followingCnt = $followerObj->getFollowingCnt($logged_User_Id, "U");

            $levelArray = array();
            foreach ($allUserLevelsArray as $level) {
                    if(($level['minimum_coolpoints'] == $userLevel[0]['cool_points'] && $level['minimum_coolpoints']+1 == $userLevel[0]['cool_points']) && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings'])) {
                            $upgradeLevel = $userLevel[0]['user_level']+1;
                            // Upgrade User Level
                            $usrObj = new User_Model_User();
                            $upgrade = $usrObj->upgradeLevel($video_owner_id,$upgradeLevel);
                            // Update user level in Downlined scribbles also.
                            $downlineUserLevel = $upgradeLevel;
                            $UpdArr   = $scrObj->updateDownlinedScribblesLevel($video_owner_id, $downlineUserLevel);
                            $resAry['reference_id'] = $upgrade;
                            $resAry['user_id'] = $video_owner_id;
                            $resAry['user_type'] = $video_owner_type;
                            $resAry['notifications_type'] = 'level_upgrade';
                            $resAry['action_date'] = date('Y-m-d H:i:s');
                            $notificationObj = new Notifications_Model_Notifications();
                            $upgrade = $notificationObj->insertNewRecord($resAry);

                            // Mail to be sent to the user
//                            $m = new UpmeSocial_HtmlMailer();
//                            $m->setSubject("UPMEsocial User Level Upgrade");
//                            $m->addTo($userLevel[0]['email'])
//                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
//                                ->setViewParam('old_level',$userLevel[0]['user_level'])
//                                ->setViewParam('new_level',$upgradeLevel)
                                  ->setViewParam('email',$userLevel[0]['email'])
//                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
//                            $m->sendHtmlTemplate("level_upgrade.phtml");
                    /*}
            }

            // get all Blasted Users Details


            $resultAry = array('service_status'=>'success',"content" => $ins);
        } else {
            $resultAry = array('service_status'=>'error-message',"content" =>'Error processing request');
        }
        echo json_encode($resultAry);
    }*/

    public function messagesAction() {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
            $user_id = $userdata->user_id;
        }
        $messageObj = new User_Model_Message();
        $updAry['receiver_read_status'] = '1';
        $id = $messageObj->updateMessages($updAry, $user_id);
        $inboxdata = $messageObj->getInboxMessages($user_id);
        $sentdata = $messageObj->getSentMessages($user_id);
        $this->view->assign('user_id',$user_id);
        $this->view->assign('inboxdata',$inboxdata);
        $this->view->assign('sentdata',$sentdata);
    }

    public function displayfriendslistAction() {
        $keyword = $this->getRequest()->getParam('q');
        if($keyword !='') {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $followObj = new User_Model_Followers();
            if(Zend_Registry::isRegistered('userdata')) {
                $userdata = Zend_Registry::get('userdata');
                $user_id = $userdata->user_id;
                if($userdata->user_type != 'B')
                    $user_type = 'U';
                else
                    $user_type = 'B';
            }
            $result = $followObj->getFriendsList($user_id,$user_type,$keyword);
            $i=0;
            $data ='';
            foreach($result as $user) {
                $name = stripslashes($user['firstname'].' '.$user['lastname']);
                $json[$i]['value'] = $user['user_id'];
                $json[$i]['name'] = $name;
                $data = $json;
                $i++;
            }
            header("Content-type: application/json");
            echo json_encode($data);
        }
    }

    public function createmessageAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $ids = explode(',', $this->getRequest()->getParam('to_user_id'));
        array_pop($ids);
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
            $user_id = $userdata->user_id;
        }
        foreach($ids as $id) {
            $data['from_user_id'] = $user_id;
            $data['to_user_id'] = $id;
            $data['message'] = $this->getRequest()->getParam('message');
            $data['datetime'] = date('Y-m-d H:i:s');
            $messageObj = new User_Model_Message();
            $mid = $messageObj->insert($data);
            $resAry['reference_id'] = $mid;
            $resAry['user_id'] = $data['to_user_id'];
            $resAry['user_type'] = 'U';
            $resAry['notifications_type'] = 'message';
            $resAry['action_date'] = date('Y-m-d H:i:s');
            $notificationObj = new Notifications_Model_Notifications();
            $upgrade = $notificationObj->insertNewRecord($resAry);
        }

        if(!empty($upgrade))
            echo 'success';exit;
    }

    public function replymessageAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
            $user_id = $userdata->user_id;
        }
        $data['parent_id'] = $this->getRequest()->getParam('parent_id');
        $data['from_user_id'] = $user_id;
        $data['to_user_id'] = $this->getRequest()->getParam('to_user_id');
        $data['message'] = $this->getRequest()->getParam('message');
        $data['datetime'] = date('Y-m-d H:i:s');
        $messageObj = new User_Model_Message();
        $id = $messageObj->insert($data);
        $resAry['reference_id'] = $id;
        $resAry['user_id'] = $data['to_user_id'];
        $resAry['user_type'] = 'U';
        $resAry['notifications_type'] = 'message';
        $resAry['action_date'] = date('Y-m-d H:i:s');
        $notificationObj = new Notifications_Model_Notifications();
        $upgrade = $notificationObj->insertNewRecord($resAry);
        if(!empty($upgrade))
            echo 'success';exit;
    }

    public function deletemessageAction() {
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();
        $message_id = $this->getRequest()->getParam('message_id');
        $type = $this->getRequest()->getParam('type');
        $messageObj = new User_Model_Message();
        if($type == 'inbox')
            $updAry['receiver_delete'] = '1';
        if($type == 'sent')
            $updAry['sender_delete'] = '1';
        $id = $messageObj->updateMessagesByMsgid($updAry, $message_id);
        $messageObj->deletemsgnotification($message_id);
        echo $id;exit;
    }

    public function profilerightbarAction() {
        $request = $this->getRequest();
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $user_account_status = $logged_user_data->user_account_status;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $user_account_status = $logged_user_data->status;
            $loggedUserType = 'B';
        }
        // check if the session exist, else redirect to Home page
        if($loggedUserId == '' || $user_account_status == 'I') {
            $this->_helper->redirector('index','index','user');
        }

        $userObj = new User_Model_User();
        if($request->getParam('user_id') == '') {
            if(!empty($logged_user_data->facebook_unique_id)) {
                $uniqueId= $logged_user_data->facebook_unique_id;
                $email = $logged_user_data->email;
                $userdata = $userObj->getFbUserDetails($uniqueId, $email);
                $uid = $userdata->user_id;
                $uType = $userdata->user_type;
                $ufname = $userdata->firstname;
            } else {
                $userdata = $logged_user_data;
                $uid = $userdata->user_id; //echo '1'.$uid;exit;
                $uType = $userdata->user_type;
                $ufname = '';
            }
        } else {
            $uid = $request->getParam('user_id');//echo '2'.$uid;exit;
            $userdata = $userObj->getUserDetails($uid);
            $uType = $userdata->user_type;
            $ufname = $userObj->getfirstname($uid,$uType);
        }
        if($uType != 'B')
            $uType = "U";
        $this->view->assign('ufname', $ufname);
        $this->view->assign('user_id', $uid);

        // Fetching User Cool POints
        $coolPoints = $userObj->getUserCoolPoints($uid, $uType);
        $this->view->coolPoints = $coolPoints;

        $userfollowing  = array();
        if($request->getParam('user_id') !='') {
            if(Zend_Registry::isRegistered('businessdata')) {
                $businessdata = Zend_Registry::get('businessdata');
            }
            if(!empty($businessdata)) {
                $this->view->businessdata = $businessdata;
            }
            // TO check if following other profile user or not
            $followObj = new User_Model_Followers();
            $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserId, $loggedUserType);
            if(!empty($userfollowing)) {
                if($userfollowing[0]['user_id'] == $loggedUserId && $userfollowing[0]['user_type'] == $loggedUserType) {
                    $userfollowing[0]['isFollowing'] = "1";
                } else {
                    $userfollowing[0]['isFollowing'] = "0";
                }
            } elseif($loggedUserId == $uid && $loggedUserType=='U') {
                $userfollowing[0]['isFollowing'] = "2";
            } else {
                $userfollowing[0]['isFollowing'] = "0";
            }
        } else {
            $userfollowing[0]['isFollowing'] = "2";
        }
        //echo "<pre>";print_r($userfollowing);exit;
        $this->view->userfollowing = $userfollowing[0];
        $this->view->loggedUserId = $loggedUserId;
        $this->view->loggedUserType = $loggedUserType;
        if(empty($userdata)) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->_helper->redirector('login');
            return;
        }

        // to get Business HotSpots
        if($logged_user_data->zipcode != '') {
            //echo "<pre>";print_r($logged_user_data);exit;
            $zip_id = $logged_user_data->zipcode;

            $cityObj = new User_Model_Cities();
            $zip = $cityObj->getZipcodeByZipID($zip_id);
            //echo "<pre>";print_r($zip);exit;
            $URL1 = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zip."&sensor=false";
            $json = file_get_contents($URL1);
            $json = json_decode($json);
            //echo "<pre>";print_r($json->{'results'}[0]->{'geometry'}->{'location'});exit;
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $lng = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            //echo $lat." -- ".$lng;exit;
            if($lat != '' && $lng != '') {
                // FOr Top Business HotSpots
                $limit = 3;
                $businessObj = new Business_Model_Business();
                $businessHotSpotArray =  $businessObj->getTopBusinessHotSpots($lat, $lng, $limit);
                foreach($businessHotSpotArray as $key=>$business) {
                    // getFollowersCount and logged user Relation with the business     getFollowersCnt
                    $businessFollowerObj = new Business_Model_Followers();
                    $followerCnt = $businessFollowerObj->getFollowersCnt($business['business_id']);
                    $businessHotSpotArray[$key]['followerCnt'] = $followerCnt;

                    // is logged user folowing
                    $followingAry = $businessFollowerObj->isLoggedUserFollowingOtherUser($business['business_id'], "B");
                    if(count($followingAry) == 0) { $isFollowing = 0; }
                    if(count($followingAry) > 0) { $isFollowing = 1; }
                    $businessHotSpotArray[$key]['isfollowing'] = $isFollowing;
                }
            } else {
                    $businessHotSpotArray = array();
            }
        } else {
                    $businessHotSpotArray = array();
        }
        $this->view->businessHotSpots = $businessHotSpotArray;
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
            //echo "<pre>";print_r($businessHotSpotArray);exit;
        }
        $userlevelObj = new User_Model_Userlevels();
        $followObj = new User_Model_Followers();
        $scribbleObj = new Scribbles_Model_Scribbles();
        $limit = ($request->getParam('limit') != '')?$request->getParam('limit'):10;
        $userDetAry = array();
        $userDetAry = $userObj->getUserDetails($uid);
        $level= $userDetAry->user_level;
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
            //echo "data<pre>";print_r($userDetAry->user_level);print_r($userdata);exit;
        }
        $userlevelAry = $userlevelObj->getUserLevels($level);
        if($request->getParam('user_id') == '' || $request->getParam('user_id') == $loggedUserId) {
            $usrLvl = $userDetAry['user_level'];
        } else{
            $usrLvl = $logged_user_data->user_level;
        }
        // to Get User Level equivalent Or Lower Levels
        $userLowerLevelsAry = $userlevelObj->getEquivalentOrLowerLevelsBasedOnUserLevel($level);

        $userDetAry = $userDetAry->toArray();
        $userDetAry['nooffollowers'] = $followObj->getFollowersCnt($uid);
        $userDetAry['nooffollowings'] = $followObj->getFollowingCnt($uid);
        //$userDetAry['scribblecount'] = $scribbleObj->getScribbleCnt($uid, $userDetAry['user_level']);
        $userDetAry['scribblecount'] = $scribbleObj->getUserPostedScribbleCnt($uid,$usrLvl);
        //$scribArray = $scribbleObj->getUserScriblesById($uid,$limit);
        $this->view->assign('userDetAry', $userDetAry);
        $this->view->assign('userlevelAry', $userlevelAry);
        //$this->view->assign('scribArray', $scribArray);

        if(!empty($userdata)) {
            $this->view->userdata = $userdata;
            $this->view->userLowerLevelsAry = $userLowerLevelsAry->toArray();
        } else {
            $this->_helper->redirector('index','index','user');
        }
    }

    public function createalbumsforallusersAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $data = $userObj->getAllUserDetails();
        foreach ($data as $user) {
            $user_id = $user['user_id'];
            $user_type = 'U';
            $user_image = $user['profile_pic_path'];
            $albumObj = new Mobile_Model_Albums();
            $albuminfo = $albumObj->albumexistsrnotwithisdefault($user_id, $user_type);
            if(empty($albuminfo)) {
                $album['user_id'] = $user_id;
                $album['user_type'] = $user_type;
                $album['album_name'] = 'Profile Photos';
                $album['is_default'] = '1';
                $album['created_date'] = date('Y-m-d H:i:s');
                $albumid = $albumObj->insert($album);
                $photoObj = new Mobile_Model_Photos();
                $photo['album_id'] = $albumid;
                $photo['photo_original_name'] = '';
                $photo['photo_modified_name'] = $user_image;
                $photo['user_id'] = $user_id;
                $photo['user_type'] = $user_type;
                $photo['cover_photo'] = '1';
                $photo['created_date'] = date('Y-m-d H:i:s');
                $photo_id = $photoObj->insert($photo);
            }
        }
        echo 'successfully done';
    }

    public function popularschoolsAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if($loggedUserId == '') {
            $this->_redirect('user');
        }
        $userObj = new User_Model_User();
        $schoolsdata = $userObj->getpopularschools();
        $followObj = new User_Model_Followers();
        $loggedUserFollowing = $followObj->getAllFollowing($loggedUserId);
        $followingCnt = count($loggedUserFollowing);
        if(count($schoolsdata) > 0) {
            foreach($schoolsdata as $key => $popularUsr) {
                $popularUsrType = 'S';
                if($followingCnt > 0) {
                    foreach($loggedUserFollowing as $Usrfollows) {
                        if($popularUsr['school_id'] == $loggedUserId && $popularUsrType == 'S') {
                            $schoolsdata[$key]['isFollowing'] = "2";
                        } else {
                            if(($popularUsr['school_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                $schoolsdata[$key]['isFollowing'] = "1";
                                break;
                            }  else {
                                $schoolsdata[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                } else {
                    if($popularUsr['school_id'] == $loggedUserId && $popularUsrType == 'U') {
                        $schoolsdata[$key]['isFollowing'] = "2";
                    } else {
                        $schoolsdata[$key]['isFollowing'] = "0";
                    }
                }
            }
        }
        $this->view->assign('user_id', $loggedUserId);
        $this->view->assign('schoolsdata', $schoolsdata);
    }

    public function popularcollegesAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if($loggedUserId == '') {
            $this->_redirect('user');
        }
        $userObj = new User_Model_User();
        $collegesdata = $userObj->getpopularcolleges();
        $followObj = new User_Model_Followers();
        $loggedUserFollowing = $followObj->getAllFollowing($loggedUserId);
        $followingCnt = count($loggedUserFollowing);
        if(count($collegesdata) > 0) {
            foreach($collegesdata as $key => $popularUsr) {
                $popularUsrType = 'C';
                if($followingCnt > 0) {
                    foreach($loggedUserFollowing as $Usrfollows) {
                        if($popularUsr['college_id'] == $loggedUserId && $popularUsrType == 'U') {
                            $collegesdata[$key]['isFollowing'] = "2";
                        } else {
                            if(($popularUsr['college_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                $collegesdata[$key]['isFollowing'] = "1";
                                break;
                            }  else {
                                $collegesdata[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                } else {
                    if($popularUsr['college_id'] == $loggedUserId && $popularUsrType == 'U') {
                        $collegesdata[$key]['isFollowing'] = "2";
                    } else {
                        $collegesdata[$key]['isFollowing'] = "0";
                    }
                }
            }
        }
        $this->view->assign('user_id', $loggedUserId);
        $this->view->assign('collegesdata', $collegesdata);
    }

    public function popularworkplacesAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if($loggedUserId == '') {
            $this->_redirect('user');
        }
        $userObj = new User_Model_User();
        $workplacedata = $userObj->getpopularworkplaces();
        $followObj = new User_Model_Followers();
        $loggedUserFollowing = $followObj->getAllFollowing($loggedUserId);
        $followingCnt = count($loggedUserFollowing);
        if(count($workplacedata) > 0) {
            foreach($workplacedata as $key => $popularUsr) {
                $popularUsrType = 'W';
                if($followingCnt > 0) {
                    foreach($loggedUserFollowing as $Usrfollows) {
                        if($popularUsr['work_place_id'] == $loggedUserId && $popularUsrType == 'U') {
                            $workplacedata[$key]['isFollowing'] = "2";
                        } else {
                            if(($popularUsr['work_place_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                $workplacedata[$key]['isFollowing'] = "1";
                                break;
                            }  else {
                                $workplacedata[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                } else {
                    if($popularUsr['work_place_id'] == $loggedUserId && $popularUsrType == 'U') {
                        $workplacedata[$key]['isFollowing'] = "2";
                    } else {
                        $workplacedata[$key]['isFollowing'] = "0";
                    }
                }
            }
        }
        $this->view->assign('user_id', $loggedUserId);
        $this->view->assign('workplacesdata', $workplacedata);
    }

    public function unsubscribeAction(){
        $request = $this->getRequest();
        //echo "<pre>";print_r($request->getParams());//exit;

        $userArray = array();
        $err = 0;
        $uid = $request->getParam('u');
        $user_id = 0;
        if($uid != '' && is_numeric(base64_decode($uid)))
            $user_id = base64_decode($uid);
        if($uid != '' && is_numeric(base64_decode($uid)))
            $user_id = base64_decode($uid);
        $ml = $request->getParam('ml');
        if($ml == ''){
            $err = 1;
        } else
        if($ml!= '') {
            $email = base64_decode($ml);
            $userArray['user_id'] = $user_id;
            $userArray['email'] = $email;
            //echo "<pre>";print_r($userArray);
            $userObj = new User_Model_User();
            $unsubscribe = $userObj->unsubscribe($userArray);
        }
        $this->view->error = $err;
    }

    /***** FOR INTERNAL TESTING PURPOSE ********/
    public function updatephotosvideosblasteddateAction(){
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57' || $_SERVER['REMOTE_ADDR'] == '182.72.66.214') {
            // For Updating blasted_date for photos and Videos tables
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                    ->from(array('tbl_photos'), array('photo_id', 'blast_id', 'created_date'));
                $photos = $db->fetchAll($select);
                //echo "<pre>";print_r($photos);exit;

                $updArray = array();
                foreach($photos as $usr){
                    if($usr['blast_id'] != 0) {
                        // TO update Blasted Date for Blasted Row
                        $updArray = array();
                        $updArray['blasted_date'] = $usr['created_date'];
                        $db->update('tbl_photos', $updArray, ' photo_id="'.$usr['photo_id'].'"');

                        $select = $db->select()
                                            ->from(array('tbl_photos'), array('photo_id', 'blast_id', 'created_date'))
                                            ->where(' photo_id="'.$usr['blast_id'].'"');
                        //echo $select;exit;
                        $updArray1 = array();
                        $parentPhotos = $db->fetchAll($select);
                        echo "<pre>";print_r($parentPhotos);print_r($usr);
                        //exit;
                        $updArray1['blasted_date'] = $usr['created_date'];
                        $db->update('tbl_photos', $updArray1, ' photo_id="'.$parentPhotos[0]['photo_id'].'"');
                    } else {
                        $updArray = array();
                        $updArray['blasted_date'] = $usr['created_date'];
                        echo "<pre>";print_r($usr);
                        $db->update('tbl_photos', $updArray, ' photo_id="'.$usr['photo_id'].'"');
                    }
                }
                echo " Photo Done";

                $select = $db->select()
                                    ->from(array('tbl_videos'), array('video_id', 'blast_id', 'created_date'));
                $videos = $db->fetchAll($select);
                //echo "<pre>";print_r($photos);exit;

                foreach($videos as $usr){
                    if($usr['blast_id'] != 0) {
                        // TO update Blasted Date for Blasted Row
                        $updArray = array();
                        $updArray['blasted_date'] = $usr['created_date'];
                        $db->update('tbl_videos', $updArray, ' video_id="'.$usr['video_id'].'"');

                        // TO update Blasted Date for Parent Row
                        $select = $db->select()
                                            ->from(array('tbl_videos'), array('video_id', 'blast_id', 'created_date'))
                                            ->where(' video_id="'.$usr['blast_id'].'"');
                        //echo $select;exit;
                        $parentVideos = $db->fetchAll($select);
                        echo "<pre>";print_r($parentVideos);print_r($usr);
                        //exit;
                        $updArray1 = array();
                        $updArray1['blasted_date'] = $usr['created_date'];
                        $db->update('tbl_videos', $updArray1, ' video_id="'.$parentVideos[0]['video_id'].'"');
                    } else {
                        $updArray = array();
                        $updArray['blasted_date'] = $usr['created_date'];
                        echo "<pre>";print_r($usr);
                        $db->update('tbl_videos', $updArray, ' video_id="'.$usr['video_id'].'"');
                    }
                }
                echo " Videos Done";exit;
        }
    }

    public function activateuserAction(){
        $request = $this->getRequest();
        $ml = $request->getParam('l');
        if($ml == ''){
            $err = 1;
        } else if($ml != '') {
            $userObj = new User_Model_User();
            $err = $userObj->activateuser($ml);
			if($err == 1) $err = 0;
        }
		else
			$err = 1;
        $this->view->error = $err;
    }

    public function forgotpasswordAction(){
        $this->_helper->layout->disableLayout();
    }

    /***** FOR INTERNAL TESTING PURPOSE ********/
    public function testmailAction(){
        /***** Mail Functionality For Registration Without Payment *****/
//        $m = new UpmeSocial_HtmlMailer();
//        $m->setSubject("Login Credentials");
//        //$m->addTo("rize.programmer@gmail.com")
//        $m->addTo("varaprasad@rizecorp.net")
//            ->setViewParam('user_id',"1")
//            ->setViewParam('firstname',"Krishna")
//            ->setViewParam('lastname',"K")
//            ->setViewParam('username',"krishna")
//            ->setViewParam('password',"123456");
//        $m->sendHtmlTemplate("logindetails.phtml");

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $followObj = new User_Model_Followers();
        $request = $this->getRequest();
        $logged_user_id = $request->getParam('logged_user_id');
        if($logged_user_id == '') {
                echo json_encode(array('service_status'=>'error',"error-message" => 'Logged User Id can not be Empty!!!'));
                return;
        }
        if(!is_numeric($logged_user_id)) {
                echo json_encode(array('service_status'=>'error',"error-message" => 'Invalid Logged User Id!!!'));
                return;
        }

        $logged_user_type = $request->getParam('logged_user_type');
        if(!ctype_alpha($logged_user_type)) {
                echo json_encode(array('service_status'=>'error',"error-message" => 'Invalid Logged User Type!!!'));
                return;
        }


        if($logged_user_type == '')
            $logged_user_type = "U";
        //$email = $request->getParam('email');
        $email = "varaprasad@rizecorp.net,kiran@rizecorp.net,narayana@gmail.co.in,laxman@gmail.com,satya123@gmail.com,krishna@gmail.com,harsha@gmail.com,lakshmi@gmail.com,durga123@gmail.com,satyan@gmail.com,hari@gmail.com";
        $emailAry = array();
        $usrAry = array();
        $emailAry = explode(',', $email);
        //echo "<pre>";print_r($emailAry);exit;
        $loggedUserFollowing = $followObj->getFollowingUsersByLoggedUser($logged_user_id, 0, 0, $logged_user_type);
        //echo "<pre>";print_r($loggedUserFollowing);exit;

        $followingCnt = count($loggedUserFollowing);
        if(count($emailAry) > 0) {
            foreach($emailAry as $key => $mail) {
                if($followingCnt > 0) {
                    foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                        //echo $mail."--".$Usrfollows['email']."--".$Usrfollows['user_id']."--".$logged_user_id."----".$Usrfollows['follower_type']."-->".$logged_user_type;exit;
                        if($mail == $Usrfollows['email'] && $Usrfollows['user_id'] == $logged_user_id && $Usrfollows['follower_type'] == $logged_user_type) {
                            $usrAry[$key]['email'] = $mail;
                            $usrAry[$key]['isFollowing'] = "2";
                        } else {
                            if($mail == $Usrfollows['email']) {
                               $usrAry[$key]['email'] = $mail;
                                $usrAry[$key]['isFollowing'] = "1";
                               break;
                            } else {
                                $usrAry[$key]['email'] = $mail;
                                $usrAry[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                } else {
                    $usrAry[$key]['email'] = $mail;
                    $usrAry[$key]['isFollowing'] = "0";
                }
            }
        }
        echo "<pre>";print_r($usrAry);exit;
    }

    public function testdb2connectionAction(){
        $userObj = new User_Model_User();
        $openfireUserRecords =  $userObj->getOpenfireUserRecords();
        echo "<pre>";print_r($openfireUserRecords);exit;
    }

    public function cropimageAction(){
        ini_set('max_execution_time' , 500);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

         if(!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == '0') {
                $dir_upload = UPLOAD_PATH.'images/original/';
                $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                $medium_path = UPLOAD_PATH.'images/medium/';
                $mobile_path = UPLOAD_PATH.'images/mobile/';
                $android_path = UPLOAD_PATH.'images/android/';
                $fileName = $_FILES['photo']['name'];
                //$fullPathNameFile = $dir_upload.$fileName;
                $image = new UpmeSocial_Controller_Action_Helper_Image();
                $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
         }


    }

}
