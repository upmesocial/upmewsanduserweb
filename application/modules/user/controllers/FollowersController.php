<?php
class User_FollowersController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
       
    }
	
    public function followersAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
        $request = $this->getRequest();
        $userid = $request->getParam('userid');
        $userType = $request->getParam('usertype');
        $loggedUserId = $request->getParam('logged_user_Id');
        if($userid == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        if(!is_numeric($userid)) {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
            return;
        }
        if($userType == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
            return;
        }
        if($loggedUserId == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
            return;
        }
        if(!is_numeric($loggedUserId)) {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Logged User Id!!!'));
            return;
        }
        $cityObj = new User_Model_Cities();
        $cityid = '';
        $cityname = '';
        $cityArr = array();
        $followObj = new User_Model_Followers();
        $pagenum = $request->pagenum;
        if($pagenum == '' || ($pagenum <= 0))
            $pagenum = 0;
        $limit = $request->getParam('limit');
        if($limit!= '') { $limit = $limit; } else { $limit = 30; }
        if($loggedUserId == $userid) { // if logged user seeing his followers
            $userfollowers = $followObj->getMyFollowers($userid, $loggedUserId,$limit,$pagenum,$userType);
            $userfollowing = $followObj->getMyFollowing($userid, $userType, $limit);
            $followingCnt = count($userfollowing);
            if(count($userfollowers) > 0) {
                foreach($userfollowers as $key => $followingUsr) {
                    if($key != 0 && $cityid == $followingUsr['city']) {
                        $city = $cityname;
                    } elseif(!empty($followingUsr['city']) && $followingUsr['city'] != 0) {
                        $cityArr = $cityObj->getCityNameByCityID($followingUsr['city']);
                        $cityArr = $cityArr->toArray();
                        if(!empty($cityArr))
                                $city = $cityArr[0]['city_name'];
                        else
                                $city = '';
                        $cityid = $followingUsr['city'];
                        $cityname = $city;
                    } else {
                        $city = '';
                    }
                    $userfollowers[$key]['city'] = $city;
                    if($followingCnt > 0) {
                        foreach($userfollowing as $key1 => $Usrfollows) {
                            if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                $userfollowers[$key]['isFollowing'] = "1";
                                break;
                            } else {
                                $userfollowers[$key]['isFollowing'] = "0";
                            }
                        }
                    } else {
                        $userfollowers[$key]['isFollowing'] = "0";
                    }
                }
            }
        } else {
            // if Logged user is viewing other user's followers, CHecking following permissions for logged user with toher user followers
            $userfollowers = $followObj->getMyFollowers($userid,$loggedUserId,$limit,$pagenum,$userType);
            $loggedUserFollowing = $followObj->getMyFollowing($loggedUserId, 0, 0, "U");
            //echo '<pre>';print_r($userfollowers);exit;
            $followingCnt = count($loggedUserFollowing);
            if(count($userfollowers) > 0) {
                foreach($userfollowers as $key => $followingUsr) {
                    if($key != 0 && $cityid == $followingUsr['city']) {
                        $city = $cityname;
                    } elseif(!empty($followingUsr['city']) && $followingUsr['city'] != 0) {
                        $cityArr = $cityObj->getCityNameByCityID($followingUsr['city']);
                        $cityArr = $cityArr->toArray();
                        if(!empty($cityArr))
                                $city = $cityArr[0]['city_name'];
                        else
                                $city = '';
                        $cityid = $followingUsr['city'];
                        $cityname = $city;
                    } else {
                        $city = '';
                    }
                    $userfollowers[$key]['city'] = $city;
                    if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                            //echo "<pre>".$loggedUserId."-->".$userType;print_r($followingUsr);
                            if($followingUsr['user_id'] == $loggedUserId && strtolower($followingUsr['user_type']) == strtolower('U')) {
                                $userfollowers[$key]['isFollowing'] = "2";
                            } else {
                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && (strtolower($followingUsr['user_type']) == strtolower($Usrfollows['follower_type']))) {
                                    $userfollowers[$key]['isFollowing'] = "1";
                                    break;
                                } else {
                                    $userfollowers[$key]['isFollowing'] = "0"; 
                                }
                            }
                        }
                    } else {
                        if($followingUsr['user_id'] == $loggedUserId && strtolower($followingUsr['user_type']) == strtolower('U')) {
                            $userfollowers[$key]['isFollowing'] = "2";
                        } else {
                            $userfollowers[$key]['isFollowing'] = "0";
                        }
                    }
                }
            }
        }
        $resAry = array('status' => 'success','content'=>$userfollowers);
        echo $myjson->customEncode($resAry);
        return false;
    }
	
    public function followingAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $userid = $this->getRequest()->getParam('userid');
        $loggedUserId = $this->getRequest()->getParam('logged_user_Id');
        $userType = $this->getRequest()->getParam('usertype');
        $limit = $this->getRequest()->getParam('limit');
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
        if($userid == '') {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
                return;
        }
        if(!is_numeric($userid)) {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
                return;
        }
        if($loggedUserId == '') {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Logged User Id can not be Empty!!!'));
                return;
        }
        if(!is_numeric($loggedUserId)) {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid Logged User Id!!!'));
                return;
        }
         if($userType == '') {
                echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
                return;
        }
        $cityObj = new User_Model_Cities();
        $followObj = new User_Model_Followers();
        $cityid = '';
        $cityname = '';
        $cityArr = array();
        $pagenum = $request->pagenum;
        if($pagenum == '' || ($pagenum <= 0))
            $pagenum = 0;
        $limit = $request->getParam('limit');
        if($limit!= '') { $limit = $limit; } else { $limit = 30;}
        
        if($loggedUserId == $userid){ // if logged user seeing whom he follow
            $userfollowing = $followObj->getMyFollowing($userid, $limit, $pagenum, $userType);
            if(count($userfollowing) > 0){
                foreach($userfollowing as $key3=>$usrfollow) {
                    if($key3 != 0 && $cityid == $usrfollow['city']) {
                        $city = $cityname;
                    } elseif(!empty($usrfollow['city']) && $usrfollow['city'] != 0) {
                        $cityArr = $cityObj->getCityNameByCityID($usrfollow['city']);
                        $cityArr = $cityArr->toArray();
                        if(!empty($cityArr))
                                $city = $cityArr[0]['city_name'];
                        else
                                $city = '';
                        $cityid = $usrfollow['city'];
                        $cityname = $city;
                    } else {
                        $city = '';
                    }
                    $userfollowing[$key3]['city'] = $city;
                    $userfollowing[$key3]['isFollowing'] = "1";
                }
            }
        } else {
            $userfollowing = $followObj->getMyFollowing($userid, $limit, $pagenum, $userType);
            $loggedUserFollowing = $followObj->getMyFollowing($loggedUserId, 0, 0, "U");
            $loggedUserFollowingCnt = count($loggedUserFollowing);
            if(count($userfollowing) > 0) {
                foreach($userfollowing as $key => $viewUsrFollowing) {
                    if($key != 0 && $cityid == $viewUsrFollowing['city']) {
                        $city = $cityname;
                    } elseif(!empty($viewUsrFollowing['city']) && $viewUsrFollowing['city'] != 0) {
                        $cityArr = $cityObj->getCityNameByCityID($viewUsrFollowing['city']);
                        $cityArr = $cityArr->toArray();
                        if(!empty($cityArr))
                                $city = $cityArr[0]['city_name'];
                        else
                                $city = '';
                        $cityid = $viewUsrFollowing['city'];
                        $cityname = $city;
                    } else {
                        $city = '';
                    }
                    $userfollowing[$key]['city'] = $city;
                    if($loggedUserFollowingCnt > 0) {
                        foreach($loggedUserFollowing as $loggedUsrFollowing) {
                            if($viewUsrFollowing['follower_id'] == $loggedUserId && strtolower($viewUsrFollowing['follower_type']) == strtolower($userType)) {
                                $userfollowing[$key]['isFollowing'] = "2";
                            } else {
                                if(($viewUsrFollowing['follower_id'] == $loggedUsrFollowing['follower_id']) && (strtolower($viewUsrFollowing['follower_type']) == strtolower($loggedUsrFollowing['follower_type']))) {
                                   $userfollowing[$key]['isFollowing'] = "1";
                                   break;
                                }  else {
                                   $userfollowing[$key]['isFollowing'] = "0"; 
                                }
                            }
                        }
                    } else { //if logged  user is not following anyone
                        foreach($userfollowing as $viewUsrFollowing) {
                            if($viewUsrFollowing['follower_id'] == $loggedUserId && $viewUsrFollowing['follower_type'] == $userType) {
                                $userfollowing[$key]['isFollowing'] = "2";
                            } else {
                                $userfollowing[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                }
            }
        }
        // FOr Sorting Array which is mixed with business and User details by realname Asc ORDER
        $b = array();
        $c = array();
        foreach($userfollowing as $k=>$v) {
            $b[$k] = strtolower($v['realname']);
        }
        asort($b);
        foreach($b as $key=>$val) {
            $c[] = $userfollowing[$key];
        }
        $resAry = array('status' => 'success','content'=>$c);
        echo $myjson->customEncode($resAry);
    }

    public function getfollowersbyuseridAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
        $request = $this->getRequest();
        $userid = $request->getParam('user_id');
        if($userid == '') {
                echo $myjson->customEncode(array('status'=>'error',"error_msg" => 'User Id can not be Empty!!!',"userinfocode"=>"420"));
                return;
        }
        if(!is_numeric($userid)) {
                echo $myjson->customEncode(array('status'=>'error',"error_msg" => 'Invalid User Id!!!',"userinfocode"=>"420"));
                return;
        }
        $notInFollowers = $request->getParam('not_in_user_ids');
        $userType = "U";
        $limit = $request->getParam('limit');
        if($limit == '') {
                $limit = 300;
        };
        $followObj = new User_Model_Followers();
        $userfollowers = $followObj->getFollowersByUserID($userid, $userType, $limit, $notInFollowers);
        //echo "<pre>";print_r($userfollowers);exit;
        if(count($userfollowers) > 0) {
                $resAry = array('status' => 'success','content'=>$userfollowers,'userinfocode'=>'420');
                echo $myjson->customEncode($resAry);
        }
        elseif(count($userfollowers) == 0) {
                $resAry = array('status' => 'success','content'=>array(),'userinfocode'=>'420');
                echo $myjson->customEncode($resAry);
        } else {
                $resAry = array('status' => 'error','error-message'=>'Some Error occured','userinfocode'=>'420');
                echo $myjson->customEncode($resAry);
        }
    }

    // for testing purpose only
//    public function updatecoolpointsindbto100Action(){
//        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender();
//        $usrObj = new User_Model_User();
//        $usrAry = array();
//        $usrAry = $usrObj->getAllUserDetails();
//        //echo "<pre>";print_r($usrAry);exit;
//        foreach($usrAry as $key=>$user){
//            $coolPoints = $usrObj->getUserCoolPoints($user['user_id'], "U");
//            //echo "<pre>";print_r($coolPointsAry);exit;
//            if($coolPoints == '' || $coolPoints == 0) {
//                $coolpointsArray = array();
//                $coolpointsArray['user_id'] =$user['user_id'] ;
//                $coolpointsArray['user_type'] = "U";
//                $coolpointsArray['cool_points'] = 100;
//                $usrObj->insertcoolpoints($coolpointsArray);
//            }
//        }
//        echo "DONE";exit;
//    }

    public function getscribblesAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $scribArray = array();
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
            $request = $this->getRequest();
            $user_id = $request->getParam('user_id');
            $loggedUserID = $request->getParam('logged_user_Id');
            $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):10);
            $userObj = new User_Model_User();
            $scrObj = new Scribbles_Model_Scribbles();
            $usrLvlObj = new User_Model_Userlevels();
            // check whether the given user exist or not
            $usrArray = array();
            $usrArray = $userObj->getUserDetails($user_id);
            //echo "<pre>";print_r($usrArray);exit;
            if(count($usrArray) != 1) {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not Exist',"responsecode"=>"1000");
                    echo $myjson->customEncode($resultAry);
                    return false;
            }
            $parentScribbles =array();
            $allScribbles =array();
            if($loggedUserID == '' || $user_id = $loggedUserID) {  // IF logged user id viewing his own profile
                    $owner_id = $user_id;
                    $owner_level = $usrArray->user_level;
            } else {
                    $owner_id = $loggedUserID;
                    // get logged User Level
                    $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                    $owner_level = $usrlevel[0]['user_level'];
            }
            $parentScribbles = $scrObj->getParentScribbles($user_id,$owner_id,$usrArray->user_type, $owner_level, $limit);
            foreach($parentScribbles as $key =>$parentScrib) {
                    $parentId = $parentScrib['scribble_id'];
                    $allScribbles[$key] = $parentScrib;
                    $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $user_id,$owner_id,$usrArray->user_type, $owner_level);
                    $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
                    $allScribbles[$key]['notifications_type'] = "scribble";
            }
            $scribArray = $allScribbles;
            $i = 0;
            // IF Blasted Scribble, Get the Original Owner Details
            foreach($scribArray as $key=>$scrib) {
                    if($scrib['blast_id'] != 0) {
                            // To Get Owner Details of the Blasted Scribble
                            $ownerArray = $scrObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                            //echo "<pre>";print_r($ownerArray);exit;
                            $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                            $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                            $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                            $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                            if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                    $scribArray[$key]['isBlastedScribble'] = 2;
                            }
                            if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                    $scribArray[$key]['isBlastedScribble'] = 1;
                            }
                    }
                    //  check if the Parent scribble is Blasted
                    $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($scrib['scribble_id']);
                    $scribArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                    if($blastedUsrCnt > 0) {
                            //to Get Blasted User Details of the Parent Scribble
                            $blastedUsrAry = array();
                            $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($scrib['scribble_id']);
                            $scribArray[$key]['BlastedUsers'] = $blastedUsrAry;
                            $i++;
                    }
            }
            $mixedAry = array();
            $mixedAry = $scribArray;
            $scribbleCnt = count($mixedAry);
            if($scribbleCnt >0) {
                    $lastScribbleDate = $scribArray[$scribbleCnt-1]['created_date'];
                    // get Logged User Following Users-list
                    $followObj = new User_Model_Followers();
                    $loggedUserFollowing = $followObj->getFollowingIdsByUserID($loggedUserID, "U");
                    if(count($loggedUserFollowing) > 0) {
                            $followIdsArr = array();
                            foreach($loggedUserFollowing as $following) {
                                    $followIdsArr[] = $following['follower_id'];
                            }
                            $followerIds = implode(",", $followIdsArr);
                            $limit2 = 10;
                            // TO Get Notifications for Purchase Coupons
                            $notificationAry = array();
                            $notificationObj = new Notifications_Model_Notifications();
                            $notificationAry= $notificationObj->getFollowerCouponNotifications($user_id, $loggedUserID,$usrArray->user_type, $lastScribbleDate, $followerIds, $limit2);
                            $cnt = count($scribArray);
                            if(count($notificationAry) > 0) {
                                    $mixedAry = $scribArray;
                                    foreach($notificationAry as $notification) {
                                            $i = $cnt++;
                                            $mixedAry[$i] = $notification;
                                            $mixedAry[$i]['created_date'] = $notification['action_date'];
                                    }
                            }
                            if($user_id == $loggedUserID) {
                                    // TO Get all Huddle Targets of the Logged in User
                                    $huddleArray = array();
                                    $huddleArray = $notificationObj->getHuddleNotifications($loggedUserID,$usrArray->user_type, $lastScribbleDate, $limit2);
                                    if(count($huddleArray) > 0){
                                            foreach($huddleArray as $huddle) {
                                                    $i = $cnt++;
                                                    $mixedAry[$i] = $huddle;
                                                    $mixedAry[$i]['created_date'] = $huddle['action_date'];
                                            }
                                    }
                            }
                            $orderByDate = array();
                            foreach ($mixedAry as $key => $row) {
                                    $orderByDate[$key]  = strtotime($row['created_date']);
                            }
                            array_multisort($orderByDate, SORT_DESC, $mixedAry);
                    }
                    // for changing TIME format of CREATED DATE
                    $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                    $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                    foreach($mixedAry as $key=>$scrib) {
                            $mixedAry[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                            if($scrib['notifications_type'] != 'scribble') continue;
                            $mixedAry[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
                    }
            }
            //Send Response to mobile success r not
            if (!empty($mixedAry)) {
                    $resultAry = array('service_status'=>'success',"content" => $mixedAry,"responsecode"=>"1000");
            } else {
                    $resultAry = array('service_status'=>'success',"content" => array(),"responsecode"=>"1000");
            }        
            echo $myjson->customEncode($resultAry);
            return false;
    }

    public function sendfbinvitesAction(){
        
    }

    public function testimagecropAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
       /* 
        $source_image_path = UPLOAD_PATH.'images/original/1373644972.jpg';
        $thumbnail_image_path = UPLOAD_PATH.'images/test/1373644972.jpg';
        
        define('THUMBNAIL_IMAGE_MAX_WIDTH', 310);
        define('THUMBNAIL_IMAGE_MAX_HEIGHT', 310);
        
        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
        switch ($source_image_type) {
            case IMAGETYPE_GIF:
                $source_gd_image = imagecreatefromgif($source_image_path);
                break;
            case IMAGETYPE_JPEG:
                $source_gd_image = imagecreatefromjpeg($source_image_path);
                break;
            case IMAGETYPE_PNG:
                $source_gd_image = imagecreatefrompng($source_image_path);
                break;
        }
        if ($source_gd_image === false) {
            return false;
        }
        // FOr RESIZING THE IMAGE AS PER THE ASPECT RATIO
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = THUMBNAIL_IMAGE_MAX_WIDTH / THUMBNAIL_IMAGE_MAX_HEIGHT;
        if ($source_image_width <= THUMBNAIL_IMAGE_MAX_WIDTH && $source_image_height <= THUMBNAIL_IMAGE_MAX_HEIGHT) {
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
//            $thumbnail_image_width = (int) (THUMBNAIL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
//            $thumbnail_image_height = THUMBNAIL_IMAGE_MAX_HEIGHT;
            $thumbnail_image_width = THUMBNAIL_IMAGE_MAX_WIDTH;
            $thumbnail_image_height = (int) (THUMBNAIL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
        } else {
//            $thumbnail_image_width = THUMBNAIL_IMAGE_MAX_WIDTH;
//            $thumbnail_image_height = (int) (THUMBNAIL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
                $thumbnail_image_width = (int) (THUMBNAIL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
                $thumbnail_image_height = THUMBNAIL_IMAGE_MAX_HEIGHT;
        }
        
        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 90);
        
        
        // FOR CROPPING THE IMAGE FROM THE THUMB IMAGE
        $img = imagecreatetruecolor(THUMBNAIL_IMAGE_MAX_WIDTH,THUMBNAIL_IMAGE_MAX_HEIGHT);
        $org_img = imagecreatefromjpeg($thumbnail_image_path);
        imagecopy($img,$org_img, 0, 0, 0, 0, THUMBNAIL_IMAGE_MAX_WIDTH, THUMBNAIL_IMAGE_MAX_HEIGHT);
        imagejpeg($img,$thumbnail_image_path,90);
        
        imagedestroy($img);
        imagedestroy($source_gd_image);
        imagedestroy($thumbnail_gd_image);
        
        return true;
        */
        
        
    }

}