<?php
class User_AjaxController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body
    }

    public function getstatesAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $res = new User_Model_States();
        $mkey = $this->getRequest()->getParam('mkey');
        $countryId = $this->getRequest()->getParam('country_id');
        if($countryId == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Country Id can not be Empty!!!'));
            return;
        }
        $limit = $this->getRequest()->getParam('limit');
        if($limit == '')
            $limit = 10;
        $states = $res->get_states($countryId, $limit);
        $states = $states->toArray();
        if($mkey == '') {
            echo json_encode($states, $limit);
            return;
        }
        if(!empty($states)) {
                $resultAry = array('service_status'=>'success',"content" => $states, "responsecode" => '502');
        } else {
                $resultAry = array('service_status'=>'error',"error_message" => 'No States Found', "responsecode" => '502');
        }
        echo json_encode($resultAry);
        return false;
    }

    public function getcitiesAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $res = new User_Model_Cities();
        $mkey = $this->getRequest()->getParam('mkey');
        $stateId= $this->getRequest()->getParam('state_id');
        if($stateId == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'State Id can not be Empty!!!'));
            return;
        }
        $limit = $this->getRequest()->getParam('limit');
        if($limit == '')
            $limit = 10;
        $cities = $res->get_cities($stateId, $limit);
        $cities = $cities->toArray();
        if($mkey == ''){
            echo json_encode($cities);
            return;
        }
        if(!empty($cities)) {
                $resultAry = array('service_status'=>'success',"content" => $cities, "responsecode" => '504');
        } else {
                $resultAry = array('service_status'=>'error',"error_message" => 'No Cities Found', "responsecode" => '504');
        }
        echo json_encode($resultAry);
        return false;
    }

    public function getzipcodesAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $mkey = $this->getRequest()->getParam('mkey');
        $cityId= $this->getRequest()->getParam('cityId');
        if($cityId == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'City Id can not be Empty!!!'));
            return;
        }
        $limit = $this->getRequest()->getParam('limit');
        if($limit == '')
            $limit = 10;
        $cityObj = new User_Model_Cities();
        $zipcodes = $cityObj->getZipcodesByCity($cityId, $limit);
        if($mkey == ''){
            echo json_encode($zipcodes);
            return;
        }
        if(!empty($zipcodes)) {
                $resultAry = array('service_status'=>'success',"content" => $zipcodes, "userinfocode" => '1000');
        } else {
                $resultAry = array('service_status'=>'error',"error_message" => 'No ZipCodes Found', "userinfocode" => '1000');
        }
        echo json_encode($resultAry);
        return false;
    }

    public function getschoolsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $mkey = $this->getRequest()->getParam('mkey');
        $limit = $this->getRequest()->getParam('limit');
        if($limit == '')
            $limit = 10;
        $userObj = new User_Model_User();
        $schools = $userObj->getSchools('',$limit);
        if($mkey == ''){
            echo json_encode($schools);
            return;
        }
        if(!empty($schools)) {
                $resultAry = array('service_status'=>'success',"content" => $schools, "responsecode" => '550');
        } else {
                $resultAry = array('service_status'=>'error',"error_message" => 'No Schools Found', "responsecode" => '550');
        }
        echo json_encode($resultAry);
        return false;
    }

    public function getcollegesAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $mkey = $this->getRequest()->getParam('mkey');
        $limit = $this->getRequest()->getParam('limit');
        if($limit == '')
            $limit = 10;
        $userObj = new User_Model_User();
        $colleges = $userObj->getColleges('', $limit);
        if($mkey == ''){
            echo json_encode($colleges);
            return;
        }
        if(!empty($colleges)) {
                $resultAry = array('service_status'=>'success',"content" => $colleges, "responsecode" => '552');
        } else {
                $resultAry = array('service_status'=>'error',"error_message" => 'No Colleges Found', "responsecode" => '552');
        }
        echo json_encode($resultAry);
        return false;
    }

    public function getworkplacesAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $mkey = $this->getRequest()->getParam('mkey');
        $limit = $this->getRequest()->getParam('limit');
        if($limit == '')
            $limit = 10;
        $userObj = new User_Model_User();
        $workplaces = $userObj->getWorkPlaces($limit);
        if($mkey == '') {
            echo json_encode($workplaces);
            return;
        }
        if(!empty($workplaces)) {
                $resultAry = array('service_status'=>'success',"content" => $workplaces, "responsecode" => '554');
        } else {
                $resultAry = array('service_status'=>'error',"error_message" => 'No Work Places Found', "responsecode" => '554');
        }
        echo json_encode($resultAry);
        return false;
    }

    public function detailedanalyticsAction() {
        $request = $this->getRequest();
        if($request->getParam('isFollowerRequest') == "YES") {
            $user_id = $request->getParam('user_id');
            $myFollowerData = array();
            // TO GET Business User FOllowers data Statistics
            $businessObj = new Business_Model_Business();
            $myFollowerData = $businessObj->getMyFollowerAnalyticsData($user_id);
            echo "<pre>";print_r($myFollowerData);exit;
        }
    }

    public function changepasswordAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $request = $this->getRequest();
        $user_id = $request->getParam('user_id');
        if($user_id == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        $userObj = new User_Model_User();
        $available = $userObj->isUserAvail($user_id);
        if(!empty($available)) {
                $resultAry = array('service_status'=>'error',"error_msg" => 'User Id not available');
                echo json_encode($resultAry);
                return false;
        }
        $user_type = $request->getParam('user_type');
        if($user_type == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
            return;
        }
        $oldpwd = $request->getParam('oldpwd');
        if($oldpwd == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Old Password can not be Empty!!!'));
            return;
        }
        if(strlen($oldpwd) < 6) {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Old Password should be 6 and more characters!!!'));
            return;
        }
        $newpwd = $request->getParam('newpwd');
        if($newpwd == '') {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'New Password can not be Empty!!!'));
            return;
        }
        if(strlen($newpwd) < 6) {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'New Password should be 6 and more characters!!!'));
            return;
        }
        $checkvalid = $userObj->duplicatepasswordcheck($user_id,$oldpwd);
        if($checkvalid != '0') {
            $updateAry['password'] = md5($newpwd.SECURITY_SALT);
            $stat = $userObj->updateUser($updateAry, $user_id);
            if($stat) {
                echo json_encode(array('service_status'=>'success',"content" =>"Password Updated Successfully"));
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
                return;
            }
        } else {
            echo json_encode(array('service_status'=>'error',"error_msg" => 'Incorrect Old Password.'));
            return;
        }
    }

    public function sendupdatemailsAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new User_Model_User();
        $useremails = $userObj->getAllUserEmails();
        foreach($useremails as $useremail):
            $m = new UpmeSocial_HtmlMailer();
            $m->setSubject("UPMEsocial - important upgrade");
            $m->addTo($useremail['email'])
                ->setViewParam('firstname',$useremail['firstname']);
            $m->sendHtmlTemplate("updatemails.phtml");
        endforeach;
    }

    public function sendversionupdatemailAction() {
        ini_set('max_execution_time' , 0);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        //$userObj = new User_Model_User();
        //$useremails = $userObj->getAllUserEmails();
        //foreach($useremails as $useremail):
            $useremail['firstname'] = 'Kiran';
            $useremail['email'] = 'kiranprasad@rizecorp.net';
            $m = new UpmeSocial_HtmlMailer();
            $m->setSubject("Application update available notification");
            $m->addTo($useremail['email'])
                ->setViewParam('firstname',$useremail['firstname']);
            $m->sendHtmlTemplate("versionupdatemail.phtml");
            echo $useremail['email']."<br />";
            sleep(1);
        //endforeach;
        echo 'completed'; exit();
    }

    public function importcollegestodbfromfileAction(){
        ini_set('max_execution_time' , 500);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        //echo "<pre>";print_r($_SERVER);
        //print_r(dirname(getcwd()));
//        exit;


        // Getting data from File and saving it to DB
        $file_path = $_SERVER['DOCUMENT_ROOT']."/upme/docs/schoolcollegedata/";

        $row = 1;
        $collegeArray = array();
        if (($handle = fopen($file_path."CollegeNavigator_Search_college_lists.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                //echo "<p> $num fields in line $row: <br /></p>\n";
                if($row != 1){
                    for ($c=0; $c < $num; $c++) {
                        //echo $data[$c] . "<br />\n";
                        $collegeArray[$row-1][$c] = $data[$c];
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        $dataAry = array();
        //echo "<pre>";
        //print_r($collegeArray);
        //exit;
        foreach($collegeArray as $key=>$college){
            $dataAry[$key]['name'] = $college[0];
            $dataAry[$key]['address'] = $college[1];
            $dataAry[$key]['general_information'] = $college[2];
            $dataAry[$key]['website'] = $college[3];
            $dataAry[$key]['type'] = $college[4];
            $dataAry[$key]['awards_offered'] = $college[5];
            $dataAry[$key]['campus_setting'] = $college[6];
            $dataAry[$key]['campus_housing'] = $college[7];
            $dataAry[$key]['student_population'] = $college[8];
            $dataAry[$key]['undergraduate_students'] = $college[9];
            $dataAry[$key]['student_to_faculty_ratio'] = $college[10];
            $dataAry[$key]['IPEDS_ID'] = $college[11];
            $dataAry[$key]['OPE_ID'] = $college[12];
        }
        echo "<pre>";print_r($dataAry);
        print_r($collegeArray);
        exit;

        $userObj = new User_Model_User();
        $ins = $userObj->InsertNewCollogeData($dataAry);
        if($ins){
            echo "Data imported Successfully";
        }

    }

    public function importcollegesfromimportedtomaintableAction(){
        ini_set('max_execution_time' , 500);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $userObj = new User_Model_User();
        $ins = $userObj->importCollegeDataFromImportedToMainTable();
        if($ins){
            echo "Data imported Successfully";
        }
    }

    public function importschoolsfromimportedtomaintableAction(){
        ini_set('max_execution_time' , 500);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $userObj = new User_Model_User();
        $schoolData = array();
        $ins = $userObj->importSchoolDataFromImportedToMainTable();
        //echo "<pre>";print_r($schoolData);exit;
        if($ins){
            echo "Data imported Successfully";
        }
    }

    public function importschoolstodbfromfileAction(){
        ini_set('max_execution_time' , 500);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        //echo "<pre>";print_r($_SERVER);print_r(dirname(getcwd()));exit;


        // Getting data from File and saving it to DB
        $file_path = $_SERVER['DOCUMENT_ROOT']."/upme/docs/schoolcollegedata/";


        $row = 1;
        $schoolArray = array();
        if (($handle = fopen($file_path."ncesdata_347C260_private schools1.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $num = count($data);
                //echo "<p> $num fields in line $row: <br /></p>\n";
                if($row >5){
                    for ($c=0; $c < $num; $c++) {
                        //echo $data[$c] . "<br />\n";
                        $schoolArray[$row-1][$c] = $data[$c];
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        $dataAry = array();
        //echo "<pre>";
        //print_r($schoolArray);
        //exit;
        foreach($schoolArray as $key=>$school){
            $dataAry[$key]['PSS_SCHOOL_ID'] = $school[0];
            $dataAry[$key]['PSS_INST'] = $school[1];
            $dataAry[$key]['LoGrade'] = $school[2];
            $dataAry[$key]['HiGrade'] = $school[3];
            $dataAry[$key]['PSS_ADDRESS'] = $school[4];
            $dataAry[$key]['PSS_CITY'] = $school[5];
            $dataAry[$key]['PSS_COUNTY_NO'] = $school[6];
            $dataAry[$key]['PSS_COUNTY_FIPS'] = $school[7];
            $dataAry[$key]['PSS_STABB'] = $school[8];
            $dataAry[$key]['PSS_FIPS'] = $school[9];
            $dataAry[$key]['PSS_ZIP5'] = $school[10];
            $dataAry[$key]['PSS_PHONE'] = $school[11];
            $dataAry[$key]['PSS_SCH_DAYS'] = $school[12];
            $dataAry[$key]['PSS_STU_DAY_HRS'] = $school[13];
            $dataAry[$key]['PSS_LIBRARY'] = $school[14];
            $dataAry[$key]['PSS_ENROLL_UG'] = $school[15];
            $dataAry[$key]['PSS_ENROLL_PK'] = $school[16];
            $dataAry[$key]['PSS_ENROLL_K'] = $school[17];
            $dataAry[$key]['PSS_ENROLL_1'] = $school[18];
            $dataAry[$key]['PSS_ENROLL_2'] = $school[19];
            $dataAry[$key]['PSS_ENROLL_3'] = $school[20];
            $dataAry[$key]['PSS_ENROLL_4'] = $school[21];
            $dataAry[$key]['PSS_ENROLL_5'] = $school[22];
            $dataAry[$key]['PSS_ENROLL_6'] = $school[23];
            $dataAry[$key]['PSS_ENROLL_7'] = $school[24];
            $dataAry[$key]['PSS_ENROLL_8'] = $school[25];
            $dataAry[$key]['PSS_ENROLL_9'] = $school[26];
            $dataAry[$key]['PSS_ENROLL_10'] = $school[27];
            $dataAry[$key]['PSS_ENROLL_11'] = $school[28];
            $dataAry[$key]['PSS_ENROLL_12'] = $school[29];

            $dataAry[$key]['PSS_ENROLL_T'] = $school[30];
            $dataAry[$key]['PSS_ENROLL_TK12'] = $school[31];
            $dataAry[$key]['PSS_RACE_AI'] = $school[32];
            $dataAry[$key]['PSS_RACE_AS'] = $school[33];
            $dataAry[$key]['PSS_RACE_H'] = $school[34];
            $dataAry[$key]['PSS_RACE_B'] = $school[35];
            $dataAry[$key]['PSS_RACE_W'] = $school[36];
            $dataAry[$key]['PSS_RACE_P'] = $school[37];
            $dataAry[$key]['PSS_RACE_2'] = $school[38];
            $dataAry[$key]['PSS_FTE_TEACH'] = $school[39];
            $dataAry[$key]['PSS_LOCALE'] = $school[40];
            $dataAry[$key]['PSS_COED'] = $school[41];
            $dataAry[$key]['PSS_TYPE'] = $school[42];
            $dataAry[$key]['PSS_LEVEL'] = $school[43];
            $dataAry[$key]['PSS_RELIG'] = $school[44];
            $dataAry[$key]['PSS_COMM_TYPE'] = $school[45];
            $dataAry[$key]['PSS_INDIAN_PCT'] = $school[46];
            $dataAry[$key]['PSS_ASIAN_PCT'] = $school[47];
            $dataAry[$key]['PSS_HISP_PCT'] = $school[48];
            $dataAry[$key]['PSS_BLACK_PCT'] = $school[49];
            $dataAry[$key]['PSS_WHITE_PCT'] = $school[50];
            $dataAry[$key]['P_PACISL_PCT'] = $school[51];
            $dataAry[$key]['P_TWOMORE_PCT'] = $school[52];
            $dataAry[$key]['PSS_STDTCH_RT'] = $school[53];
            $dataAry[$key]['PSS_ORIENT'] = $school[54];
            $dataAry[$key]['PSS_COUNTY_NAME'] = $school[55];
            $dataAry[$key]['PSS_ASSOC_1'] = $school[56];
            $dataAry[$key]['PSS_ASSOC_2'] = $school[57];
            $dataAry[$key]['PSS_ASSOC_3'] = $school[58];
            $dataAry[$key]['PSS_ASSOC_4'] = $school[59];
            $dataAry[$key]['PSS_ASSOC_5'] = $school[60];
            $dataAry[$key]['PSS_ASSOC_6'] = $school[61];
            $dataAry[$key]['PSS_ASSOC_7'] = $school[62];

        }
        //echo "<pre>";print_r($dataAry);
        //print_r($schoolArray);
        //exit;

        $userObj = new User_Model_User();
        $ins = $userObj->InsertNewSchoolData($dataAry);
        if($ins){
            echo "Data imported Successfully";
        }

    }

    public function imagecropfromoriginalAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
//        $image = new UpmeSocial_Controller_Action_Helper_Image();
//        //$arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
//        $dir_path = UPLOAD_PATH.'images/original/';
//        $photo = '1362723119.jpg';//'1362662168.jpg';
//        $target_path = UPLOAD_PATH.'images/test/';
//        $arrFileName = $image->cropFromOriginalTheDimentionsSpecified($photo, $dir_path, $target_path, '310', '310');
//        echo 'OUTPUT<pre>';print_r($arrFileName);exit;
        $image = UPLOAD_PATH.'images/original/1362657826.jpg'; //1362657826.jpg,1363159510.jpg'; // the image to crop
        $destination_image = UPLOAD_PATH.'images/test/1362657826.jpg'; // make sure the directory is writeable
        $display_image = SITE_URL.'uploads/images/test/1362657826.jpg';

        $w = '310';
        $h = '310';
        list($rwidth, $rheight) = getimagesize($image);
        echo 'RWidth:'.$rwidth.'<br>RHeight:'.$rheight;
        $r = $rwidth / $rheight;
        echo '<br>R:'.$r.'<br>';
        if($rwidth < $w && $rheight < $h) {
            $newwidth = $rwidth;
            $newheight = $rheight;
        } else if($rwidth < $w && $rheight > $h) {
            $newwidth = $w;
            $newheight = $rheight;
        } else if($rwidth > $w && $rheight < $h) {
            $newwidth = $rwidth;
            $newheight = $h;
        } else if($rwidth > $w && $rheight > $h) {
            $newwidth = $w;
            $newheight = $h;
        }
        //echo 'NewHeight'.$newheight.'<br>NewWidth'.$newwidth.'<br>';exit;
        $img = imagecreatetruecolor($newwidth,$newheight);//imagecreatetruecolor('310','310');
        $org_img = imagecreatefromjpeg($image);
        imagecopy($img,$org_img, 0, 0, 20, 20, $newwidth, $newheight);
        imagejpeg($img,$destination_image,90);
        imagedestroy($img);
        echo '<img src="'.$display_image.'" ><p>';
    }

    public function getschoolsbykeywordAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->logged_user_id;
            $keyword = $request->keyword;
            $limit = $request->limit;
            if($limit == '')
                $limit = 10;
            if($keyword == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                    return;
            }
            $userObj = new User_Model_User();
            $followObj = new User_Model_Followers();
            $schoolsdata = $userObj->getschoolsbykeyword($limit, $keyword);
            if(!empty($schoolsdata)) {
                    $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, 'U');
                    $follower_ids = array();
                    foreach($loggedUserFollowing as $key => $val) {
                            $follower_ids[] = $val['follower_id'];
                    }
                    $schooldata = array();
                    foreach($schoolsdata as $key =>$school):
                            $schooldata[$key]['school_id'] = $school['school_id'];
                            $schooldata[$key]['school_name'] = $school['school_name'];
                            if(in_array($school['school_id'],$follower_ids)) {
                                $schooldata[$key]['is_following'] = '1';
                            } else {
                                $schooldata[$key]['is_following'] = '0';
                            }
                    endforeach;
                    $notificationObj = new Notifications_Model_Notifications();
                    $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                    if (!empty($schooldata)) {
                            $resultAry = array('service_status'=>'success',"content" => $schooldata, "notificationcnt" => $notificationcnt['notificationcnt']);
                    } else {
                            $resultAry = array('service_status'=>'error',"error_msg" => 'No Schools To View');
                    }
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No Schools To View');
            }
            echo json_encode($resultAry);
            return false;
    }

    public function getcollegesbykeywordAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->logged_user_id;
            $keyword = $request->keyword;
            $limit = $request->limit;
            if($limit == '')
                $limit = 10;
            if($keyword == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                    return;
            }
            $userObj = new User_Model_User();
            $followObj = new User_Model_Followers();
            $collegesdata = $userObj->getcollegesbykeyword($limit, $keyword);
            if(!empty($collegesdata)) {
                    $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, 'U');
                    $follower_ids = array();
                    foreach($loggedUserFollowing as $key => $val) {
                            $follower_ids[] = $val['follower_id'];
                    }
                    $collegedata = array();
                    foreach($collegesdata as $key =>$college):
                            $collegedata[$key]['college_id'] = $college['college_id'];
                            $collegedata[$key]['college_name'] = $college['college_name'];
                            if(in_array($college['college_id'],$follower_ids)) {
                                $collegedata[$key]['is_following'] = '1';
                            } else {
                                $collegedata[$key]['is_following'] = '0';
                            }
                    endforeach;
                    $notificationObj = new Notifications_Model_Notifications();
                    $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                    if (!empty($collegedata)) {
                            $resultAry = array('service_status'=>'success',"content" => $collegedata, "notificationcnt" => $notificationcnt['notificationcnt']);
                    } else {
                            $resultAry = array('service_status'=>'error',"error_msg" => 'No Colleges To View');
                    }
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No Colleges To View');
            }
            echo json_encode($resultAry);
            return false;
    }

    public function getworkplacesbykeywordAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->logged_user_id;
            $keyword = $request->keyword;
            $limit = $request->limit;
            if($limit == '')
                $limit = 10;
            if($keyword == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                    return;
            }
            $userObj = new User_Model_User();
            $followObj = new User_Model_Followers();
            $workplacesdata = $userObj->getworkplacesbykeyword($limit, $keyword);
            if(!empty($workplacesdata)) {
                    $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, 'U');
                    $follower_ids = array();
                    foreach($loggedUserFollowing as $key => $val) {
                            $follower_ids[] = $val['follower_id'];
                    }
                    $workplacedata = array();
                    foreach($workplacesdata as $key =>$workplace):
                            $workplacedata[$key]['work_place_id'] = $workplace['work_place_id'];
                            $workplacedata[$key]['work_place_name'] = $workplace['work_place_name'];
                            if(in_array($workplace['work_place_id'],$follower_ids)) {
                                $workplacedata[$key]['is_following'] = '1';
                            } else {
                                $workplacedata[$key]['is_following'] = '0';
                            }
                    endforeach;
                    $notificationObj = new Notifications_Model_Notifications();
                    $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                    if (!empty($workplacedata)) {
                            $resultAry = array('service_status'=>'success',"content" => $workplacedata, "notificationcnt" => $notificationcnt['notificationcnt']);
                    } else {
                            $resultAry = array('service_status'=>'error',"error_msg" => 'No Work Places To View');
                    }
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No Work Places To View');
            }
            echo json_encode($resultAry);
            return false;
    }

    public function getcountriesbykeywordAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->logged_user_id;
            $limit = $request->limit;
            if($limit == '')
                $limit = 10;
            $keyword = $request->keyword;
            if($keyword == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                    return;
            }
            $countryObj = new User_Model_Countries();
            $countriesdata = $countryObj->getcountiresbykeyword($limit, $keyword);
            if(!empty($countriesdata)) {
                    $countrydata = array();
                    foreach($countriesdata as $key =>$country):
                            $countrydata[$key]['id'] = $country['id'];
                            $countrydata[$key]['country_name'] = $country['country_name'];
                    endforeach;
                    $notificationObj = new Notifications_Model_Notifications();
                    $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                    if (!empty($countrydata)) {
                            $resultAry = array('service_status'=>'success',"content" => $countrydata, "notificationcnt" => $notificationcnt['notificationcnt']);
                    } else {
                            $resultAry = array('service_status'=>'error',"error_msg" => 'No Countries To View');
                    }
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No Countries To View');
            }
            echo json_encode($resultAry);
            return false;
    }

    public function getstatesbykeywordAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->logged_user_id;
            $limit = $request->limit;
            if($limit == '')
                $limit = 10;
            $countryId = $request->country_id;
            $keyword = $request->keyword;
            if($keyword == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                    return;
            }
            if($countryId == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "Country Id can't be Empty"));
                    return;
            }
            $stateObj = new User_Model_States();
            $statesdata = $stateObj->getstatesbykeyword($limit, $keyword,$countryId);
            if(!empty($statesdata)) {
                    $statedata = array();
                    foreach($statesdata as $key =>$city):
                            $statedata[$key]['id'] = $city['id'];
                            $statedata[$key]['state_name'] = $city['state_name'];
                    endforeach;
                    $notificationObj = new Notifications_Model_Notifications();
                    $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                    if (!empty($statedata)) {
                            $resultAry = array('service_status'=>'success',"content" => $statedata, "notificationcnt" => $notificationcnt['notificationcnt']);
                    } else {
                            $resultAry = array('service_status'=>'error',"error_msg" => 'No States To View');
                    }
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No States To View');
            }
            echo json_encode($resultAry);
            return false;
    }

    public function getcitiesbykeywordAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $logged_user_Id = $request->logged_user_id;
            $limit = $request->limit;
            if($limit == '')
                $limit = 10;
            $stateId = $request->state_id;
            $keyword = $request->keyword;
            if($keyword == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                    return;
            }
            if($stateId == '') {
                    echo json_encode(array('service_status'=>'error',"error_msg" =>  "StateId can't be Empty"));
                    return;
            }
            $cityObj = new User_Model_Cities();
            $citiesdata = $cityObj->getcitiesbykeyword($limit,$keyword,$stateId);
            if(!empty($citiesdata)) {
                    $citydata = array();
                    foreach($citiesdata as $key =>$city):
                            $citydata[$key]['id'] = $city['id'];
                            $citydata[$key]['city_name'] = $city['city_name'];
                    endforeach;
                    $notificationObj = new Notifications_Model_Notifications();
                    $notificationcnt = $notificationObj->notificationcnt($logged_user_Id, "U", '1');
                    if (!empty($citydata)) {
                            $resultAry = array('service_status'=>'success',"content" => $citydata, "notificationcnt" => $notificationcnt['notificationcnt']);
                    } else {
                            $resultAry = array('service_status'=>'error',"error_msg" => 'No Cities To View');
                    }
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No Cities To View');
            }
            echo json_encode($resultAry);
            return false;
    }

    public function updateallphotosblasteddateAction(){
        // INTERNAL MODIFICATIONS PURPOSE
        // TO Update All photos Blasted dates That are not Updated Properly.
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $userPhotosObj = new Photos_Model_Photos();
        $mobilePhotoObj = new Mobile_Model_Photos();
        $photosArray = $userPhotosObj->getAllOriginalPhotos();

        foreach($photosArray as $key=>$photo){

            // check If any Blasted Photos are there
            $blastedPhotos = $mobilePhotoObj->getBlastedPhotosByPhotoId($photo['photo_id']);
            $photosArray[$key]['blasted_photos'] = $blastedPhotos;
            if(!empty($blastedPhotos)){
                //echo "<pre>";print_r($blastedPhotos);exit;
                $cnt = count($blastedPhotos);
                $blastedIds = '';
                for($i=0;$i<=$cnt-1;$i++){
                    if($i<$cnt-1)
                        $blastedIds .= "'".$blastedPhotos[$i]."',";
                    if($i == $cnt-1)
                        $blastedIds .= "'".$blastedPhotos[$i]."'";
                }

                // UPDATE BLASTED DATE for the BLASTED PHOTOS
                $userPhotosObj->updatePhotoBlastedDateByBlastedIds($photo['photo_id'], $photo['blasted_date'], $blastedIds);
            }
        }
        echo "Updated";
        //echo "<pre>";print_r($photosArray);exit;
    }

    // to export all countries to a CSV file for Web-service.
    public function getcountriestocsvAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        // Get Countries from DB
        $countryObj = new User_Model_Countries();
        $countriesArray = $countryObj->get_countries();
        //echo "<pre>";print_r($countriesArray);exit;

        $i = 0;
        $stringData = '';
        // Create a Csv file by inserting the fetched data to Csv file.
        foreach($countriesArray as $res)
        {
                $row='';
                $first_row='';
                foreach($res as $key=>$v)
                {
                    if($i==0)
                    {
                        if($first_row==''){
                                $first_row .=$key;
                        } else {
                                $first_row .=','.$key;
                        }
                    }
                    if($row==''){
                            $row .='"'.$v.'"';
                    } else {
                            $row .=',"'.strip_tags($v).'"';
                    }
                }
                if($i++==0)
                        $stringData .= $first_row."\n";
                $stringData .= $row."\n";
            }
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=countries.csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $stringData;die;
    }

    public function getstatestocsvAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $country_id = $this->getRequest()->getParam('country_id');
        if($country_id == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" =>  "Country Id can't be Empty"));
                return false;
        }
        // Get Countries from DB
        $stateObj = new User_Model_States();
        $statesArray = $stateObj->get_states($country_id);
        //echo "<pre>";print_r($countriesArray);exit;

        $i = 0;
        $stringData = '';
        // Create a Csv file by inserting the fetched data to Csv file.
        foreach($statesArray as $res)
        {
                $row='';
                $first_row='';
                foreach($res as $key=>$v)
                {
                    if($i==0)
                    {
                        if($first_row==''){
                                $first_row .='"'.$key.'"';
                        } else {
                                $first_row .=',"'.$key.'"';
                        }
                    }
                    if($row==''){
                            $row .='"'.$v.'"';
                    } else {
                            $row .=',"'.strip_tags($v).'"';
                    }
                }
                if($i++==0)
                        $stringData .= $first_row."\n";
                $stringData .= $row."\n";
            }
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=states.csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $stringData;die;
    }

    public function getcitiestocsvAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $country_id = $this->getRequest()->getParam('country_id');
        if($country_id == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" =>  "Country Id can't be Empty"));
                return false;
        }

        $state_id = $this->getRequest()->getParam('state_id');
        if($state_id == '') {
                echo json_encode(array('service_status'=>'error',"error_msg" =>  "State Id can't be Empty"));
                return false;
        }
        // Get Countries from DB
        $cityObj = new User_Model_Cities();
        $citiesArray = $cityObj->get_cities($state_id);
        //echo "<pre>";print_r($countriesArray);exit;

        $i = 0;
        $stringData = '';
        // Create a Csv file by inserting the fetched data to Csv file.
        foreach($citiesArray as $res)
        {
                $row='';
                $first_row='';
                foreach($res as $key=>$v)
                {
                    if($i==0)
                    {
                        if($first_row==''){
                                $first_row .='"'.$key.'"';
                        } else {
                                $first_row .=',"'.$key.'"';
                        }
                    }
                    if($row==''){
                            $row .='"'.$v.'"';
                    } else {
                            $row .=',"'.strip_tags($v).'"';
                    }
                }
                if($i++==0)
                        $stringData .= $first_row."\n";
                $stringData .= $row."\n";
            }
            header("Content-type: application/x-msdownload");
            header("Content-Disposition: attachment; filename=states.csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $stringData;die;
    }

    /// FOR INTERNAL TESTING PURPOSE
    public function missingimageuploadbyoriginalAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $file_name = '1374775038.png';
        $source_path = UPLOAD_PATH.'images/original/';
        $fullPathNameFile = $source_path.$file_name;
        $source = $source_path;
        $thumb_path = UPLOAD_PATH.'images/thumbnails/';
        $medium_path = UPLOAD_PATH.'images/medium/';
        $mobile_path = UPLOAD_PATH.'images/mobile/';
        $android_path = UPLOAD_PATH.'images/android/';
        $image = new UpmeSocial_Controller_Action_Helper_Image();
        $request = $this->getRequest();
        if($request->getParam('isOldModel') == ''){
            // for resize medium images
            $image->cropFromOriginalTheDimentionsSpecified($medium_path.$file_name,$source_path,391,391);


            $this->cropImage($huddle_path,$file_name,$thmb_medium_width,200,$source);

            // for resize small images
            $image->cropFromOriginalTheDimentionsSpecified($thumb_path.$file_name,$source_path,100,100);

            // for resize mobile images
            $image->cropFromOriginalTheDimentionsSpecified($mobile_path.$file_name,$source_path,310,310);

            //for reesize android images
            $image->cropFromOriginalTheDimentionsSpecified($android_path.$file_name,$source_path,480,480);
        } else {
            $setthumb_height = 100;
            $setthumb_width = 100;

            $thmb_medium_width = 391;
            $thmb_medium_height = 391;

            $original_img_width = 800;
            $original_img_height = 600;

            $mobile_img_width = 310;
            $mobile_img_height = 310;
            $mobile_img_height_max_limit = 310;

            $huddle_img_width = 200;
            $huddle_img_height = 200;
            $huddle_img_height_max_limit = 200;

            $android_img_width = 480;
            $android_img_height = 480;
            $android_img_height_max_limit = 480;

            // for Medium Images
            list($thmb_medium_width,$thmb_medium_height) = $image->getNewDimensions($fullPathNameFile,$thmb_medium_width,$thmb_medium_height);
            $image->cropImage($medium_path,$file_name,$thmb_medium_width,$thmb_medium_height,$source);



            // for resize small images
            list($setthumb_width,$setthumb_height) = $image->getNewDimensions($fullPathNameFile,$setthumb_width,$setthumb_height);
            $image->cropImage($thumb_path,$file_name,$setthumb_width,$setthumb_height,$source);



            // for resize mobile images
            list($mobile_img_width,$mobile_img_height) = $image->getMobileDimensions($fullPathNameFile,$mobile_img_width,$mobile_img_height);
            // if height is greater than the Limit Height, Restrict it to Limit.
            if($mobile_img_height > $mobile_img_height_max_limit)
                $mobile_img_height = $mobile_img_height_max_limit;

            $image->cropImage($mobile_path,$file_name,$mobile_img_width,$mobile_img_height,$source);

            //for reesize android images
            list($android_img_width,$android_img_height) = $image->getMobileDimensions($fullPathNameFile,$android_img_width,$android_img_height);

            // if height is greater than the Limit Height, Restrict it to Limit.
            if($android_img_height > $android_img_height_max_limit)
                $android_img_height = $android_img_height_max_limit;
            $image->cropImage($android_path,$file_name,$android_img_width,$android_img_height,$source);


        }

        echo 'Successfully Uploaded..!';exit;
    }
//For croping images to set profile images in circle shape in android
    public function imageuploadbyoriginalAction() {
        ini_set('max_execution_time', 99999);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $huddle_img_width = 200;
        $huddle_img_height = 200;

        $dir_upload = UPLOAD_PATH.'images/original/';
        $images = glob($dir_upload . "*.*");
        $huddle_path = UPLOAD_PATH.'images/huddle/';
        $newFilearray = array();
        $image = new UpmeSocial_Controller_Action_Helper_Image();
        foreach($images as $imageinfo):
            $info = explode('/',$imageinfo);
            //echo '<pre>';print_r($info);exit;
            $file_name = $info[9];
            preg_match("/\.([^\.]+)$/", $file_name, $matches);
            $newFilearray[] = $file_name;
            $fullPathNameFile =  $dir_upload.$file_name;
            $rotateAngle = '';
            if($rotateAngle != '' && $rotateAngle != 0) {
                    $rotateAngle = 360 - $rotateAngle;
                    $source = imagecreatefromjpeg($fullPathNameFile); // Load
                    $rotate = imagerotate($source, $rotateAngle, 0);// Rotate
                    imagejpeg($rotate,$fullPathNameFile);// Output
            }
            $source = $dir_upload;
            $source_path = $dir_upload.$file_name;
            // for resize coupon images
            $image->cropFromOriginalTheDimentionsSpecified($huddle_path.$file_name,$source_path,$huddle_img_width,$huddle_img_height);
        endforeach;
        echo 'Successfully Uploaded..!';exit;
    }

	//send mail when zapier action calls
	public function zapiertriggerAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $useremail['firstname'] = 'Kiran';
        $useremail['email'] = 'ramakrishna@rizecorp.net';
            $m = new UpmeSocial_HtmlMailer();
            $m->setSubject("Zapier Trigger - ".time());
            $m->addTo($useremail['email'])
                ->setViewParam('firstname',$useremail['firstname']);
            $m->sendHtmlTemplate("versionupdatemail.phtml");
            echo $useremail['email']."<br />";
            //sleep(1);
        //endforeach;
        echo 'completed'; exit();
    }

}
