<?php
class Scribbles_IndexController extends Zend_Controller_Action {

    public function init() {
            /* Initialize action controller here */
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $user_account_status = $logged_user_data->user_account_status;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $user_account_status = $logged_user_data->status;
            $loggedUserType = 'B';
        }
        if(!empty($logged_user_data)) {
            $followObj = new User_Model_Followers();
            //echo '<pre>';print_r($loggedUserId);exit;
            $followersgallery = $followObj->getFollowersAlbumImages($loggedUserId, $loggedUserType);
            /*$limit= 30;
            $photosObj = new Mobile_Model_Photos();
            $followersgallery = $photosObj->getTimelineClickedImagesVideosDetails($loggedUserId,$loggedUserType, $limit);*/
            //echo "<pre>";print_r($followersgallery);exit;
            //echo count($followersgallery);exit;
            $this->view->assign('followersgallery', $followersgallery);
        }
    }
    
    public function indexAction() {
            //action body
            $userObj = new User_Model_User();
            $scribArray = array();
            $newScribArray = array();
            if(Zend_Registry::isRegistered('userdata')) {
                    $logged_user_data = Zend_Registry::get('userdata');
                    $loggedUserID = $logged_user_data->user_id;
                    $logged_user_data = $userObj->getUserDetails($loggedUserID);
                    //$user_account_status = $logged_user_data->user_account_status;
                    //$loggedUserType = 'U';
            }
            // Fetch LOgged USer Level
            $usrLevelObj = new User_Model_Userlevels();
            $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
            $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];

            $request = $this->getRequest();
            if($request->getParam('user_id') != '') {
                    $user_id = $request->getParam('user_id');
                    $userdata = $userObj->getUserDetails($user_id);
                    $user_type = $userdata->user_type;
                    $user_level = $userdata->user_level;
            } else {
                    $user_id = $logged_user_data->user_id;
                    $user_type = $logged_user_data->user_type;
                    $user_level = $logged_user_data->user_level;
                    $userdata = $logged_user_data;
            }
            $scrObj = new Scribbles_Model_Scribbles();
            //echo '<pre>';print_r($request->getParams());exit;
            // FOR Fetching User LELEV SCRIBBLES Info
            $offset = 0;
            $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):20);
            if($user_id == $loggedUserID && $logged_user_data->user_type != 'B') {
                    $scribArray = $scrObj->getUserPostedAndBlastedParentScribbles($user_id,$logged_user_data->user_id,$logged_user_data->user_type, $logged_user_data->user_level, $limit);
                    //echo "<pre>";print_r($scribArray);exit;
                    $purchasecouponsObj = new Business_Model_Purchasecoupons;
                    $purchasecoupons = $purchasecouponsObj->getpurchasecouponsinfo($user_id);
                    //$couponscnt = $purchasecouponsObj->getcouponscntbyid($user_id,$purchasecoupons->coupon_id);
            } else {
                    // TO check if following other profile user or not
                    $followObj = new User_Model_Followers();
                    $userfollowing = $followObj->isLoggedUserFollowingOtherUser($user_id, 'U', $loggedUserID, "U");
                    if(count($userfollowing) > 0) {
                            $scribArray = $scrObj->getUserPostedAndBlastedParentScribbles($user_id,$logged_user_data->user_id,$user_type, $logged_user_data->user_level, $limit);
                    }
                    if(count($userfollowing) == 0) {
                            $scribArray = array();
                    }
                    $purchasecouponsObj = new Business_Model_Purchasecoupons;
                    $purchasecoupons = $purchasecouponsObj->getpurchasecouponsinfo($user_id);
            }
            //$scribblecnt = $scrObj->getScribbleCnt($user_id,$user_level, 'U');
            //$scribblecnt = $scrObj->getUserScriblesCntById($user_id,$user_id,$user_type,$user_level,'','');
            //}
            if($_SERVER['REMOTE_ADDR'] == '192.168.1.57' || $_SERVER['REMOTE_ADDR'] == '182.72.66.214'){
                    //echo "<pre>";print_r($scribArray);exit;
            }
            foreach($scribArray as $key=>$scrib) {
                    $isValidScribble=0;
                    if($scrib['blast_id'] != 0) {
                            $ownerArray = $scrObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                            //echo "<Pre>";print_r($scrib);print_r($ownerArray);exit;
                            if($scrib['user_level'] < $ownerArray[0]['user_level']){
                                    $isValidScribble = 1;
                            }
                            if($isValidScribble == 0) {
                                    $scribArray[$key] = $ownerArray[0];
                                    $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                    $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                    $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                    $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                    $scribArray[$key]['owner_profile_pic_path'] = $ownerArray[0]['profile_pic_path'];
                                    if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                            $scribArray[$key]['isBlastedScribble'] = 2;
                                    }
                                    $scribArray[$key]['isBlastedScribble'] = 1;
                                    $scribArray[$key]['isUpDownScribble'] = "";
    //                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
    //                                        $scribArray[$key]['isBlastedScribble'] = 1;
    //                                }
                                    $newScribArray[$key] = $scribArray[$key];
                            }
                    }
                    if($isValidScribble == 0) {
                            $parentId = $scrib['scribble_id'];
                            if($scrib['blast_id'] != 0){
                                    $parentId = $scrib['blast_id'];
                            }
                            // to get childscribbleCnt
                            $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $user_id,$loggedUserID,"U", $loggedUserLevel);
                            $scribArray[$key]['childScribbles'] = $childScribblesCnt;

                            // check if the Parent scribble is Blasted
                            $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentId);
                            $scribArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                            $blastedUsrAry = array();
                            if($blastedUsrCnt > 0) {
                                    //to Get Blasted User Details of the Parent Scribble
                                    $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentId, $loggedUserID);
                            }
                            $scribArray[$key]['BlastedUsers'] = $blastedUsrAry;
                            if($scrib['blast_id'] == 0) {
                                    foreach($blastedUsrAry as $blastedby){
                                            if($blastedby['user_id'] == $loggedUserID){
                                                    $scribArray[$key]['isBlastedScribble'] = 1;
                                            }
                                    }
                            }
                            $newScribArray[$key] = $scribArray[$key];
                    }
            }
            $this->view->purchasecoupons = $purchasecoupons;
            $this->view->scribArray = $newScribArray;
            //$this->view->scribblecnt = $scribblecnt;
            $this->view->scribblecnt = count($scribArray);

            // For Downline Scribbles
            $downlineArray = array();
            $downlineArray = $scrObj->getDownlineScribbles($loggedUserID,$loggedUserLevel,$limit);
            foreach($downlineArray as $key =>$parentScrib) {
                $parentId = $parentScrib['scribble_id'];
                $downlineArray[$key] = $parentScrib;
                $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $loggedUserID,$loggedUserID,"U", $loggedUserLevel,"downline");
                $downlineArray[$key]['childScribbles'] = $childScribblesCnt;

                //  check if the Parent scribble is Blasted
                $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                $downlineArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                $blastedUsrAry = array();
                if($blastedUsrCnt > 0) {
                    //to Get Blasted User Details of the Parent Scribble
                    $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                }
                $downlineArray[$key]['BlastedUsers'] = $blastedUsrAry;
            }
            $this->view->downlineArray = $downlineArray;

            // For fetching @username Scribbles
            $atUsernameArray = array();
            $atUsernameArray = $scrObj->getAtUserNameScribbles($loggedUserID,$loggedUserLevel,$limit);
            $this->view->atUsernameArray = $atUsernameArray;

            $pagenum = 1;
            $this->view->pagenum = $pagenum;
            $this->view->limit = $limit;
            $this->view->user_id = $user_id;
            $this->view->userdata = $userdata;
    }

    public function morelevelscribblesAction() {
            $this->_helper->layout->disableLayout();
            //$this->_helper->viewRenderer->setNoRender();
            if(Zend_Registry::isRegistered('userdata')) {
                    $logged_user_data = Zend_Registry::get('userdata');
                    $loggedUserID = $logged_user_data->user_id;
            }
            $request = $this->getRequest();
            if($request->getParam('user_id') != '') {
                    $user_id = $request->getParam('user_id');
                    $userObj = new User_Model_User();
                    $userdata = $userObj->getUserDetails($user_id);
                    $user_type = $userdata->user_type;
                    $user_level = $userdata->user_level;
            } else {
                    $user_id = $loggedUserID;
                    $userdata = $logged_user_data;
            }
            if($loggedUserID == '') {
                    $this->_redirect('user');
            }
            //$lastscribble_id = $this->getRequest()->getParam('scribblesid');
            /*$user_id = $this->getRequest()->getParam('user_id');
            if($user_id == '')
                    $user_id = $loggedUserId;*/
            $scribbleObj = new Scribbles_Model_Scribbles();
            $scribblecnt = $scribbleObj->getUserScriblesCntById($user_id,$user_id,$user_type,$user_level,'','');
            $offset = $this->getRequest()->getParam('offset')+40;
            $limit = $this->getRequest()->getParam('limit')+40;
            $scribArray = $scribbleObj->getUserScriblesById($user_id,$user_id,$user_type, $user_level, $offset, $limit);
            //echo '<pre>';print_r($scribArray);exit;
            $this->view->scribArray = $scribArray;
            $this->view->scribblecnt = $scribblecnt;
            $this->view->offset = $offset;
            $this->view->limit = $limit;
            $this->view->user_id = $user_id;
            $this->view->userdata = $userdata;
    }

    public function postscribblewebAction() {
            $this->_helper->layout->disableLayout();
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $logged_user_id = $auth->getIdentity()->user_id;
            $logged_user_type = $auth->getIdentity()->user_type;
            $request = $this->getRequest();
            $fromUserId = $request->getParam('fromId');
            if($fromUserId == '' || $fromUserId == 0) {
                    echo 'error';
                    return;
            }
            $fromUserType = $request->getParam('fromType');
            if($fromUserType == '') {
                    echo 'error';
                    return;
            }
            $toUserId = $request->getParam('toId');
            $scribble = $request->getParam('scribble');
            if($scribble == '') {
                    echo "error";
                    return;
            }
            if(strlen($scribble) > 150) {
                    echo "error";
                    return;
            }
            $tabType = $request->getParam('tabType');
            
            if($request->getParam('parentId') != '')
                    $parent_Id = $request->getParam('parentId');
            else
                    $parent_Id = 0;
            
            $user_level = $request->getParam('user_level');
            //if($user_level == 0) {
            //  //Get USer level
            //  $usrLvlObj = new User_Model_Userlevels();
            //  $usrlevel = $usrLvlObj->getUserLevelByUserId($fromUserId, $fromUserType);
            //  $user_level = $usrlevel[0]['user_level'];
            //}
            
            $scrObj = new Scribbles_Model_Scribbles();
            
            $insAry = array(
                                'parent_id' => $parent_Id,
                                'from_user_id' => $fromUserId,
                                'from_user_type' => strtoupper($fromUserType),
                                'to_user_id' => $toUserId,
                                'scribble_message' => $scribble,
                                'user_level'=>$user_level,
                                'created_date' => date('Y-m-d H:i:s')
                            );
                //echo "<pre>";print_r($insAry);exit;
                $ins = $scrObj->insertScribble($insAry);
                
                if($ins!= '' && $ins!= 0) {
                    // if the scribble is already having Child Scribbles
                    if($parent_Id != 0) {
                        if(Zend_Registry::isRegistered('userdata')) {
                            $logged_user_data = Zend_Registry::get('userdata');
                            $loggedUserID = $logged_user_data->user_id;
                            $loggedUserType = $logged_user_data->user_id;
                            $loggedUserLevel = $logged_user_data->user_level;
                         }
                         $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):40);
                         $scribbleObj = new Scribbles_Model_Scribbles();
                         $childScribbles = $scribbleObj->getChildScribblesByParentId($parent_Id, $fromUserId,$loggedUserID,$loggedUserType, $loggedUserLevel);
                         //echo "<pre>";print_r($childScribbles);exit

                         $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                         $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
                         // IF Blasted Scribble, Get the Original Owner Details
                         foreach($childScribbles as $key=>$scrib) {
                                 if($scrib['blast_id'] != 0) {
                                         // To Get Owner Details of the Blasted Scribble
                                         $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                         //echo "<pre>";print_r($ownerArray);exit;
                                         $childScribbles[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                         $childScribbles[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                         $childScribbles[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                         $childScribbles[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                         if($scribArray[$key]['owner_user_id'] == $loggedUserID && $childScribbles[$key]['owner_user_type'] == "U") {
                                                 $childScribbles[$key]['isBlastedScribble'] = 2;
                                         }
                                         if($scribArray[$key]['owner_user_id'] != $loggedUserID && $childScribbles[$key]['owner_user_type'] == "U") {
                                                 $childScribbles[$key]['isBlastedScribble'] = 1;
                                         }
                                 }
                                 //  check if the Parent scribble is Blasted
                                 $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($scrib['scribble_id']);
                                 $childScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                 $blastedUsrAry = array();
                                 if($blastedUsrCnt > 0) {
                                     //to Get Blasted User Details of the Parent Scribble
                                     $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($scrib['scribble_id'], $loggedUserID);
                                 }
                                 $childScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                         }
                         $this->view->scribArray = $childScribbles;
                    }
                    
                    //  if the scribble is going to have first child scribbles
                    if($parent_Id == 0) {
                        $scribbleInfo = $scrObj->getScribleInfo($ins);
                        $scribbleInfo['BlastedUsersCnt'] = 0;
                        $scribbleInfo['BlastedUsers'] = array();
                        $this->view->scribArray = $scribbleInfo;
                    }
                    $this->view->parent_id = $parent_Id;
                    $this->view->tabType = $tabType;
//                    $scribbleInfo = $scrObj->getScribleInfo($ins);
//                    if($scribbleInfo[0]['from_user_type'] == 'U') {
//                            if($scribbleInfo['0']['profile_pic_path'] != '' && file_exists(UPLOAD_PATH."images/thumbnails/".$scribbleInfo['0']['profile_pic_path']))
//                                    $imgPath = SITE_URL.'/uploads/images/thumbnails/'.$scribbleInfo['0']['profile_pic_path'];
//                            else
//                                    $imgPath = SITE_URL.'/images/no-user.gif';
//                            $name = ucfirst($scribbleInfo['0']['firstname'])." ".ucfirst($scribbleInfo['0']['lastname']);
//                            $usrName = $scribbleInfo['0']['username'];
//                    }
//                    if($scribbleInfo[0]['from_user_type'] == 'B') {
//                            $imgPath = SITE_URL.'/uploads/images/thumbnails/'.$scribbleInfo['0']['image_path'];
//                            $name = ucfirst($scribbleInfo['0']['business_name']);
//                            $usrName = $scribbleInfo['0']['business_username'];
//                    }
//                    // for Emotions
//                    $strAry = array(':0',':x',':-D',':-)',':-p',':(',':D',':^0',';|','?:|',';-)');
//                    $replaceAry = array('<img src="'.SITE_URL.'images/emoticons/angel.png">','<img src="'.SITE_URL.'images/emoticons/kiss.png">','<img src="'.SITE_URL.'images/emoticons/laugh.png">','<img src="'.SITE_URL.'images/emoticons/plain.png">','<img src="'.SITE_URL.'images/emoticons/raspberry.png">','<img src="'.SITE_URL.'images/emoticons/sad.png">','<img src="'.SITE_URL.'images/emoticons/smile.png">','<img src="'.SITE_URL.'images/emoticons/smile-big.png">','<img src="'.SITE_URL.'images/emoticons/surprise.png">','<img src="'.SITE_URL.'images/emoticons/uncertain.png">','<img src="'.SITE_URL.'images/emoticons/wink.png">');
//                    $reply_html = '<a id="reply'.$scribbleInfo['0']['scribble_id'].'" class="various2 fancybox.ajax pointer" onclick="replyScribble('.$scribbleInfo['0']['scribble_id'].','.$logged_user_id.',\''.$logged_user_type.'\','.$scribbleInfo['0']['from_user_id'].')"><img class="tab_icon2" src="'.SITE_URL.'images/reply.png" ></a>';
//                    $output = str_replace($strAry,$replaceAry,$scribbleInfo[0]['scribble_message']);
//                    $timeObj = new UpmeSocial_View_Helper_Datetimediff();
//                    $html='';
//                    $html = '<div class="tab_row">
//                                    <img width="66" height="66" src="'.$imgPath.'">
//                                    <div class="tab_row_cnt_otr">
//                                            <h1>'.$this->view->translate($name).'</h1><h2>'.$this->view->translate($usrName).'</h2><br>
//                                            <h2  class="scrb-text">'.$this->view->translate(stripslashes(urldecode($output))).'</h2><br>
//                                            <div class="clear"></div>
//                                            <span class="time">'.$this->view->translate($timeObj->humaneDate($scribbleInfo[0]['created_date'])).'</span>
//                                            '.$reply_html.'
//                                    </div>
//                            </div>';
//                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content" => $html));                
//                    return;
            } else {
                        $this->_helper->viewRenderer->setNoRender();
                        //$myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Error in posting scribble!!!. Please try again.'));
                        echo "error";
                        return;
                }
    }

    public function postscribbleAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            //get all arguments
            $request = $this->getRequest();
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
            $fromUserId = $request->getParam('fromId');
            if($fromUserId == '' || $fromUserId == 0) {
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Invalid FromId'));
                    return;
            }
            $fromUserType = $request->getParam('fromType');
            if($fromUserType == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Invalid Fromtype'));
                    return;
            }
            $toUserId = $request->getParam('toId');
            $scribble = $request->getParam('scribble');
            if($scribble == '') {
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Please scribble something cool'));
                    return;
            }
            if(strlen($scribble) > 540) {
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'scribble limit exceeded'));
                    return;
            }
            $user_level = $request->getParam('level');
            //if($user_level == 0) {
            //  //Get USer level
            //  $usrLvlObj = new User_Model_Userlevels();
            //  $usrlevel = $usrLvlObj->getUserLevelByUserId($fromUserId, $fromUserType);
            //  $user_level = $usrlevel[0]['user_level'];
            //}
            if($request->getParam('parentId') != '')
                    $parent_Id = $request->getParam('parentId');
            else
                    $parent_Id = 0;
            if($request->getParam('gps_location') != '')
                $gps_location = $request->getParam('gps_location');
            else
                $gps_location = '';
            if($request->getParam('gps_lat') != '')
                $gps_lat = $request->getParam('gps_lat');
            else
                $gps_lat = '';
            if($request->getParam('gps_lng') != '')
                $gps_lng = $request->getParam('gps_lng');
            else
                $gps_lng = '';
            $insAry = array(
                            'parent_id' => $parent_Id,
                            'from_user_id' => $fromUserId,
                            'from_user_type' => strtoupper($fromUserType),
                            'to_user_id' => $toUserId,
                            'user_level'=>$user_level,
                            'scribble_message' => $scribble,
                            'gps_location' => $gps_location,
                            'gps_lat' => $gps_lat,
                            'gps_lng' => $gps_lng,
                            'created_date' => date('Y-m-d H:i:s')
                    );
            $scrObj = new Scribbles_Model_Scribbles();
            $ins = $scrObj->insertScribble($insAry);
            if($ins) {
                    //$scribbleInfo = $scrObj->getScribleInfo($ins);
                    echo $myjson->customEncode(array('service_status'=>'success',"content" => $ins));
                    return;
            } else {
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'error while posting scribble'));
                    return;
            }
    }

    public function replyscribbleAction() {
            $this->_helper->layout->disableLayout();
            $request = $this->getRequest();
            $parentId = $request->getParam('parentId');
            //echo $parentId;exit;
            $frmUserId = $request->getParam('frmUserId');
            $frmUsrType = $request->getParam('frmUsrType');
            $toUserId = $request->getParam('toUserId');
            $user_level = $request->getParam('user_level');
            $tabType = $request->getParam('tabType');
            $this->view->parentId = $parentId;
            $this->view->frmUserId = $frmUserId;
            $this->view->frmUsrType = $frmUsrType;
            $this->view->toUserId = $toUserId;
            $this->view->user_level = $user_level;
            $this->view->tabType = $tabType;
    }

    public function upscribbleAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            //get all arguments
            $request = $this->getRequest();
            $uId = $request->getParam('uId');
            $loggedUserId = $request->getParam('logged_user_Id');
            $uType = $request->getParam('uType');
            $actionType = $request->getParam('actionType');
            $scribbleId = $request->getParam('scribbleId');
            $typeObj = $request->getParam('type');
            $scrObj = new Scribbles_Model_Scribbles();
            $usrLevelObj = new User_Model_Userlevels();
            
            // check if logged User Following Scribble Owner
            $followObj = new User_Model_Followers();
            $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uId,  'U', $loggedUserId, $uType);
            if(count($userfollowing) == 0) {
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You need to follow to Up Scribble'));
                    return;
            }
            
            //Check User Up this Scribbles r not
            $availrnot = $scrObj->Actioncheck($loggedUserId, $uType, $actionType, $scribbleId, $typeObj);
            //echo '<pre>';print_R($availrnot);exit;
            if(!empty($availrnot)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have already Up this Scribble'));
                    return;
            }
            //Checking For Limit Of the day
            $daycount = $scrObj->getdaycount($loggedUserId, $actionType);
            //echo '<pre>';print_r($daycount);exit;
            $updownLimit = 50;
            if($_SERVER['SERVER_NAME'] == '192.168.1.57'){
                $updownLimit = UP_DOWN_LIMIT;
            }
            if($daycount['count'] >= $updownLimit) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have reached day limit for Up Scribble'));
                    return;
            }
            //for inserting UP in table
            $ins = $scrObj->upScribble($loggedUserId, $uType, $actionType, $scribbleId, $typeObj);
            //for Updating cool points
            $upd = $scrObj->updateCoolPoints($uId, $uType, "up");
            $coolPoints = $upd;
            /****  Upgrade User Level if cool points reaches Level Limits ****/
            //Get User Level
            $userLevel = $usrLevelObj->getUserLevelByUserId($uId, $uType);
            
            //Get All Levels details FROM DB
            $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();
            
            $followerObj = new User_Model_Followers();
            // to get Followers Count of logged User
            $followersCnt = $followerObj->getFollowersCnt($uId, $uType);

            // to Get Following Count of the Logged User
            $followingCnt = $followerObj->getFollowingCnt($uId, $uType);
            //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
            
            // CHeck if user crosses any level
            $levelArray = array();
            foreach ($allUserLevelsArray as $level) {
                //echo $level['minimum_coolpoints']."===".$userLevel[0]['cool_points']."---".$followersCnt."---.".$level['minimum_followers']."====".$followingCnt."====".$level['minimum_followings'];exit;
                    if(($userLevel[0]['cool_points'] >=$level['minimum_coolpoints']) && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings']) && $userLevel[0]['cool_points'] > 0){
                            $upgradeLevel = $level['level_no'];
                            // Upgrade User Level
                            $usrObj = new User_Model_User();
                            $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                            // Update user level in Downlined scribbles also.
                            $downlineUserLevel = $upgradeLevel;
                            $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                            $lvlAry['user_id'] = $userLevel[0]['user_id'];
                            $lvlAry['level'] = $upgradeLevel;
                            $lvlAry['level_status'] = 'Up';
                            $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                            if(!empty($id['id'])) {
                                $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                            } else {
                                $lvlid = $usrObj->insertleveldetails($lvlAry);
                            }

                            // Mail to be sent to the user
//                            $m = new UpmeSocial_HtmlMailer();
//                            $m->setSubject("UPMEsocial User Level Upgrade");
//                            $m->addTo($userLevel[0]['email'])
//                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
//                                ->setViewParam('old_level',$userLevel[0]['user_level'])
//                                ->setViewParam('new_level',$upgradeLevel)
//                                ->setViewParam('email',$userLevel[0]['email'])
//                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
//                            $m->sendHtmlTemplate("level_upgrade.phtml");
                    }
            }
            $userDetArr['user_id'] = $uId;
            $userDetArr['cool_points'] = $coolPoints;
            if($ins && $upd!= '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content"=>'up', 'user_details'=>$userDetArr));
                    return;
            } else {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error'));
                    return;
            }
    }

    public function downscribbleAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            //get all arguments
            $request = $this->getRequest();
            $uId = $request->getParam('uId');
            $loggedUserId = $request->getParam('logged_user_Id');
            $uType = $request->getParam('uType');
            $actionType = $request->getParam('actionType');
            $scribbleId = $request->getParam('scribbleId');
            $typeObj = $request->getParam('type');
            $scrObj = new Scribbles_Model_Scribbles();
            $usrLevelObj = new User_Model_Userlevels();
            
            // check if logged User Following Scribble Owner
            $followObj = new User_Model_Followers();
            $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uId,  'U', $loggedUserId, $uType);
            if(count($userfollowing) == 0) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You need to follow to Down Scribble'));
                    return;
            }
            
            //Check User Down this Scribbles r not
            $availrnot = $scrObj->Actioncheck($loggedUserId, $uType, $actionType, $scribbleId, $typeObj);
            //echo '<pre>';print_R($availrnot);exit;
            if(!empty($availrnot)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have already Down this Scribble'));
                    return;
            }
            //Checking For Limit Of the day
            $daycount = $scrObj->getdaycount($loggedUserId, $actionType);
            //echo '<pre>';print_r($daycount);exit;
            $updownLimit = 50;
            if($_SERVER['SERVER_NAME'] == '192.168.1.57'){
                $updownLimit = UP_DOWN_LIMIT;
            }
            if($daycount['count'] >= $updownLimit) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have reached day limit for Down Scribble'));
                    return;
            }
            
            
            /**** Degrade User Level if cool points decreases Level Limits ****/        
            //for inserting Down in table
            $ins = $scrObj->downScribble($loggedUserId, $uType, $actionType, $scribbleId,$typeObj);
            //for Updating cool points
            $upd = $scrObj->updateCoolPoints($uId, $uType,"down");
            $coolPoints = $upd;
            
            //Get All Levels details
            $allUserLevelsArray = $usrLevelObj->getAllTypesOfUserLevels();
            // Get User Level
            $userLevel = $usrLevelObj->getUserLevelByUserId($uId, $uType);
            
            $followerObj = new User_Model_Followers();
            // to get Followers Count of logged User
            $followersCnt = $followerObj->getFollowersCnt($uId, $uType);

            // to Get Following Count of the Logged User
            $followingCnt = $followerObj->getFollowingCnt($uId, $uType);
            //echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit;
            
            $levelArray = array();
            foreach($allUserLevelsArray as $level) {
                    if(((($level['minimum_coolpoints']-1) == $userLevel[0]['cool_points']) || ($followersCnt < $level['minimum_followers'] || $followingCnt < $level['minimum_followings'])) && $userLevel[0]['cool_points'] > 0 && $userLevel[0]['user_level'] == $level['level_no']){
                            $upgradeLevel = $level['level_no']-1;
                            //Upgrade User Level
                            $usrObj = new User_Model_User();
                            $upgrade = $usrObj->upgradeLevel($userLevel[0]['user_id'],$upgradeLevel);
                            // Update user level in Downlined scribbles also.
                            $downlineUserLevel = $upgradeLevel;
                            $UpdArr   = $scrObj->updateDownlinedScribblesLevel($userLevel[0]['user_id'], $downlineUserLevel);
                            $lvlAry['user_id'] = $userLevel[0]['user_id'];
                            $lvlAry['level'] = $upgradeLevel;
                            $lvlAry['level_status'] = 'Down';
                            $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                            if(!empty($id)) {
                                $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                            } else {
                                $lvlid = $usrObj->insertleveldetails($lvlAry);
                            }

                            //Mail to be sent to the user
//                            $m = new UpmeSocial_HtmlMailer();
//                            $m->setSubject("UPMEsocial User Level Degrade");
//                            $m->addTo($userLevel[0]['email'])
//                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
//                                ->setViewParam('old_level',$userLevel[0]['user_level'])
//                                ->setViewParam('new_level',$upgradeLevel)
//                                ->setViewParam('email',$userLevel[0]['email'])
//                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
//                            $m->sendHtmlTemplate("level_degrade.phtml");
                    }
            }
            $userDetArr['user_id'] = $uId;
            $userDetArr['cool_points'] = $coolPoints;
            if($ins && $upd!= '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'success',"content"=>'down', 'user_details'=>$userDetArr));
                    return;
            } else {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error'));
                    return;
            }
    }

    public function blastscribbleAction() {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();
            $fromUserId = $request->getParam('frmUserId');
            if($fromUserId == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'User ID can not be Empty!!!'));
                    return;
            }
            if(!is_numeric($fromUserId)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Invalid From User Id!!!'));
                    return;
            }
            $fromUserType = $request->getParam('frmUsrType');
            if($fromUserType == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'User Type can not be Empty!!!'));
                    return;
            }
            $blastId = $request->getParam('blastId');
            if($blastId == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'share Id can not be Empty!!!'));
                    return;
            }
            if(!is_numeric($blastId)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Invalid share Id!!!'));
                    return;
            }
            $logged_user_Id = $request->getParam('logged_user_Id');
            if($logged_user_Id == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Logged User Id can not be Empty!!!'));
                    return;
            }
            if(!is_numeric($logged_user_Id)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Invalid Logged User Id!!!'));
                    return;
            }
            $scribbleId = $request->getParam('scribbleId');
            if($scribbleId == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Scribble Id can not be Empty!!!'));
                    return;
            }
            if(!is_numeric($scribbleId)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Invalid Scribble Id!!!'));
                    return;
            }
            $blastContent = $request->getParam('blastContent');
            if($blastContent == '') {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Share Content can not be Empty!!!'));
                    return;
            }
            
            // check if logged User Following Scribble Owner
            $followObj = new User_Model_Followers();
            $userfollowing = $followObj->isLoggedUserFollowingOtherUser($fromUserId, $fromUserType, $logged_user_Id, "U");
        
            if(count($userfollowing) == 0) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You need to follow to Share Scribble'));
                    return;
            }
            
            if($blastId == 0) {
                    $scribble_Owner = $fromUserId;
                    $blastId = $scribbleId;
            }
            // Find Original Ower of Scribble that is Blasted
            $scrObj = new Scribbles_Model_Scribbles();
            if($blastId != 0) {
                    $ownerArray = $scrObj->getScribleInfo($blastId);
                    $scribble_Owner = $ownerArray['from_user_id'];
            }
            // Check User Down this Scribbles r not
            $availrnot = $scrObj->blastcheck($logged_user_Id, $blastId, $scribbleId);
            //echo '<pre>';print_R($availrnot);exit;
            if(!empty($availrnot)) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have already Share this Scribble'));
                    return;
            }
        
            // Checking For Limit Of the day
            $daycount = $scrObj->getblastdaycount($logged_user_Id);
       // echo "<pre>";print_r($request->getParams());exit;
            //echo '<pre>';print_r($daycount);exit;
            $blastLimit = 50;
            if($_SERVER['SERVER_NAME'] == '192.168.1.57') {
                $blastLimit = SCRIBBLE_BLAST_LIMIT;
            }
        
            if($daycount['count'] >= $blastLimit) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                    echo $myjson->customEncode(array('service_status'=>'error', 'error_msg' => 'You have reached day limit for Share Scribble'));
                    return;
            }
        
            // for inserting New blasted scribble
            $insAry = array(
                            'from_user_id' => $logged_user_Id,
                            'from_user_type' => strtoupper($fromUserType),
                            'blast_id' => $blastId,
                            'scribble_message' =>$blastContent,
                            'user_level' => $ownerArray['user_level'],
                            'created_date' => date('Y-m-d H:i:s')
                    );
            $ins = $scrObj->blastScribble($insAry);
            // for inserting into Blasts table
            $blastAry = array(
                                'user_id' => $logged_user_Id,
                                'blast_id' => $blastId,
                                'scribble_id' => $ins
                        );
            $insBlast = $scrObj->insertBlast($blastAry);
            $resAry['reference_id'] = $ins;
            $resAry['user_id'] = $fromUserId;
            $resAry['user_type'] = $fromUserType;
            $resAry['notifications_type'] = 'scribble_blasted';
            $resAry['action_date'] = date('Y-m-d H:i:s');
            $notificationObj = new Notifications_Model_Notifications();
            $upgrade = $notificationObj->insertNewRecord($resAry);
            //for Updating cool points
            $upd = $scrObj->updateCoolPoints($scribble_Owner, $fromUserType,"blast");
            $coolPoints = $upd;
            /**** Upgrade User Level if cool points decreases Level Limits ****/        
            $usrLevelObj = new User_Model_Userlevels();
            //Get User Level
            $userLevel = $usrLevelObj->getUserLevelByUserId($scribble_Owner, $fromUserType);
            //Get All Levels details
            $allUserLevelsArray   = $usrLevelObj->getAllTypesOfUserLevels();
            
            $followerObj = new User_Model_Followers();
            // to get Followers Count of logged User
            $followersCnt = $followerObj->getFollowersCnt($scribble_Owner, "U");

            // to Get Following Count of the Logged User
            $followingCnt = $followerObj->getFollowingCnt($scribble_Owner, "U");
            //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo "<pre>".$followersCnt."--".$followingCnt;print_r($allUserLevelsArray);print_r($userLevel);exit; }
            $levelArray = array();
            foreach ($allUserLevelsArray as $level) {
                    if(($userLevel[0]['cool_points'] >= $level['minimum_coolpoints'] && $level['minimum_coolpoints']+1 >= $userLevel[0]['cool_points']) && ($followersCnt >= $level['minimum_followers'] && $followingCnt >= $level['minimum_followings'])) {
                            $upgradeLevel = $level['level_no'];

                            // Upgrade User Level
                            $usrObj = new User_Model_User();
                            $upgrade = $usrObj->upgradeLevel($scribble_Owner,$upgradeLevel);
                            // Update user level in Downlined scribbles also.
                            $downlineUserLevel = $upgradeLevel;
                            $UpdArr   = $scrObj->updateDownlinedScribblesLevel($scribble_Owner, $downlineUserLevel);
                            $resAry['reference_id'] = $upgrade;
                            $resAry['user_id'] = $fromUserId;
                            $resAry['user_type'] = $fromUserType;
                            $resAry['notifications_type'] = 'level_upgrade';
                            $resAry['action_date'] = date('Y-m-d H:i:s');
                            $notificationObj = new Notifications_Model_Notifications();
                            $upgrade = $notificationObj->insertNewRecord($resAry);
                            $lvlAry['user_id'] = $fromUserId;
                            $lvlAry['level'] = $upgradeLevel;
                            $lvlAry['level_status'] = 'Up';
                            $id = $usrObj->isuserinleveldetails($lvlAry['user_id']);
                            if(!empty($id)) {
                                $lvlid = $usrObj->updateleveldetails($lvlAry,$lvlAry['user_id']);
                            } else {
                                $lvlid = $usrObj->insertleveldetails($lvlAry);
                            }
                            // Mail to be sent to the user
//                            $m = new UpmeSocial_HtmlMailer();
//                            $m->setSubject("UPMEsocial User Level Upgrade");
//                            $m->addTo($userLevel[0]['email'])
//                                ->setViewParam('name',  ucfirst($userLevel[0]['firstname'])." ".$userLevel[0]['lastname'])
//                                ->setViewParam('old_level',$userLevel[0]['user_level'])
//                                ->setViewParam('new_level',$upgradeLevel)
//                                ->setViewParam('email',$userLevel[0]['email'])
//                                ->setViewParam('coolpoints',$userLevel[0]['cool_points']);
//                            $m->sendHtmlTemplate("level_upgrade.phtml");
                    }
            }
            $userDetArr['user_id'] = $scribble_Owner;
            $userDetArr['cool_points'] = $coolPoints;

            // to get blasted user info
            $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($blastId);
            $blastedUsrAry = array();
            if($blastedUsrCnt > 0) {
                //to Get Blasted User Details of the Parent Scribble
                $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($blastId, $logged_user_Id);
            }

            if($ins && $insBlast) {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content" =>$ins,"type"=>"blast", 'user_details'=>$userDetArr, 'BlastedUsersCnt'=>$blastedUsrCnt,  'BlastedUsers'=>$blastedUsrAry));
                    return;
            } else {
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"content" => 'Error in sharing scribble!!!. Please try again.'));
                    return;
            }
    }

    public function viewscribblesAction() {
            $this->_helper->layout->disableLayout();
            $scribArray = array();
            if(Zend_Registry::isRegistered('userdata')) {
                    $logged_user_data = Zend_Registry::get('userdata');
                    $loggedUserID = $logged_user_data->user_id;
                    $loggedUserType = 'U';
                    $loggedUserLevel = $logged_user_data->user_level;
            }
            if(Zend_Registry::isRegistered('businessdata')) {
                    $logged_user_data = Zend_Registry::get('businessdata');
                    $loggedUserID = $logged_user_data->business_id;
                    $loggedUserType = 'B';
            }
            if(empty($loggedUserID)) {
                    $this->helper->redirector('user');
            }
            $request = $this->getRequest();
            if($request->getParam('user_id') != '' && $request->getParam('user_id') != 'user_id') {
                    $user_id = $request->getParam('user_id');
                    $uid = $request->getParam('user_id');
                    $userObj = new User_Model_User();
                    $userdata = $userObj->getUserDetails($user_id);
                    $user_type = $userdata->user_type;
                    $user_level = $userdata->user_level;
            } else {
                    $user_id = $loggedUserID;
                    $uid = $loggedUserID;
                    $user_type = $loggedUserType;
                    $user_level = $loggedUserLevel;
            }
            //if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):20);

                $userDetAry =array();
                $userDetAry = $userObj->getUserDetails($uid);
                $userDetAry = $userDetAry->toArray();
                
                // to Get User Level Name
                $usrLvlObj = new User_Model_Userlevels();
                $usrlevelName = $usrLvlObj->getUserLevels($userDetAry['user_level']);
                $usrlevelName = $usrlevelName->toArray();
                $userDetAry['user_level_name'] = $usrlevelName['level_name'];
                if($loggedUserID == $uid) {    
                    // Fetch LOgged USer Level
                    $usrLevelObj = new User_Model_Userlevels();
                    $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
                    $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];

                    // TO get User Posted Scribbles
                    $parentScribbles =array();
                    $allScribbles =array();
                    $parentScribbles = $scribbleObj->getUserPostedParentScribbles($uid,$loggedUserID,"U", $loggedUserLevel, $limit);
                    foreach($parentScribbles as $key =>$parentScrib) {

                            $parentId = $parentScrib['scribble_id'];
                            $parentScrib['notifications_type'] = "scribble";
                            $allScribbles[$key] = $parentScrib;
                            $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                            $allScribbles[$key]['childScribbles'] = $childScribblesCnt;


                            //  check if the Parent scribble is Blasted
                            $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                            $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                            $blastedUsrAry = array();
                            if($blastedUsrCnt > 0) {
                                //to Get Blasted User Details of the Parent Scribble
                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                            }
                            $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;

                            // As User can not Updown/ Blast his own scribbles
                            $allScribbles[$key]['isUpDownScribble'] ='';
                            $allScribbles[$key]['UpDownAction'] ='';
                            $allScribbles[$key]['isBlastedScribble'] ='';

                    }
                    $scribArray = $allScribbles;
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isFollowing'] = "1";
                                // Get Scribbles only If Logged User is following Other User
                                // get logged User Level
                                $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                                $logged_user_level = $usrlevel[0]['user_level'];
                                $parentScribbles =array();
                                $allScribbles =array();
                                $parentScribbles = $scribbleObj->getUserPostedParentScribbles($uid,$loggedUserID,"U", $logged_user_level, $limit);
                                foreach($parentScribbles as $key =>$parentScrib) {
                                        $parentId = $parentScrib['scribble_id'];
                                        $allScribbles[$key] = $parentScrib;
                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;

                                        //  check if the Parent scribble is Blasted
                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                        $blastedUsrAry = array();
                                        if($blastedUsrCnt > 0) {
                                            //to Get Blasted User Details of the Parent Scribble
                                            $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                        }
                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                }
                                $scribArray = $allScribbles;
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isFollowing'] = "0";
                                $scribArray = array();
                        }
                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                // IF Blasted Scribble, Get the Original Owner Details
                foreach($scribArray as $key=>$scrib) {

                        if($scrib['blast_id'] != 0) {
                                $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                //echo "<pre>";print_r($ownerArray);exit;
                                $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 2;
                                }
                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 1;
                                }
                        }
                    $scribArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                }
//            } else {
//                $scribleObj = new Scribbles_Model_Scribbles();
//                $scribArray = $scribleObj->getUserScribbles($user_id, $user_level, $user_type, $loggedUserID);
//            }
            $pagenum = 0;
            $this->view->pagenum = $pagenum;
            $this->view->limit = $limit;
            $this->view->assign('scribArray',$scribArray);
    }

    public function getmoreuserpostedscribbleswebAction() {
            $this->_helper->layout->disableLayout();
            $scribArray = array();
            if(Zend_Registry::isRegistered('userdata')) {
                    $logged_user_data = Zend_Registry::get('userdata');
                    $loggedUserID = $logged_user_data->user_id;
                    $loggedUserType = 'U';
                    $loggedUserLevel = $logged_user_data->user_level;
            }
            if(Zend_Registry::isRegistered('businessdata')) {
                    $logged_user_data = Zend_Registry::get('businessdata');
                    $loggedUserID = $logged_user_data->business_id;
                    $loggedUserType = 'B';
            }
            if(empty($loggedUserID)) {
                    $this->helper->redirector('user');
            }
            $request = $this->getRequest();
            if($request->getParam('user_id') != '' && $request->getParam('user_id') != 'user_id') {
                    $user_id = $request->getParam('user_id');
                    $uid = $request->getParam('user_id');
                    $userObj = new User_Model_User();
                    $userdata = $userObj->getUserDetails($user_id);
                    $user_type = $userdata->user_type;
                    $user_level = $userdata->user_level;
            } else {
                    $user_id = $loggedUserID;
                    $uid = $loggedUserID;
                    $user_type = $loggedUserType;
                    $user_level = $loggedUserLevel;
            }
            //if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                $userObj = new User_Model_User();
                $followObj = new User_Model_Followers();
                $scribbleObj = new Scribbles_Model_Scribbles();
                $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):20);
                $pagenum = $request->getParam('pagenum');
                $userDetAry =array();
                $userDetAry = $userObj->getUserDetails($uid);
                $userDetAry = $userDetAry->toArray();
                
                // to Get User Level Name
                $usrLvlObj = new User_Model_Userlevels();
                $usrlevelName = $usrLvlObj->getUserLevels($userDetAry['user_level']);
                $usrlevelName = $usrlevelName->toArray();
                $userDetAry['user_level_name'] = $usrlevelName['level_name'];
                if($loggedUserID == $uid) {    
                    // Fetch LOgged USer Level
                    $usrLevelObj = new User_Model_Userlevels();
                    $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
                    $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];

                    // TO get User Posted Scribbles
                    $parentScribbles =array();
                    $allScribbles =array();
                    $parentScribbles = $scribbleObj->getMoreUserPostedParentScribblesWeb($uid,$loggedUserID,"U", $loggedUserLevel, $limit, $pagenum);
                    foreach($parentScribbles as $key =>$parentScrib) {

                            $parentId = $parentScrib['scribble_id'];
                            $parentScrib['notifications_type'] = "scribble";
                            $allScribbles[$key] = $parentScrib;
                            $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $loggedUserLevel);
                            $allScribbles[$key]['childScribbles'] = $childScribblesCnt;


                            //  check if the Parent scribble is Blasted
                            $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                            $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                            $blastedUsrAry = array();
                            if($blastedUsrCnt > 0) {
                                //to Get Blasted User Details of the Parent Scribble
                                $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                            }
                            $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;

                            // As User can not Updown/ Blast his own scribbles
                            $allScribbles[$key]['isUpDownScribble'] ='';
                            $allScribbles[$key]['UpDownAction'] ='';
                            $allScribbles[$key]['isBlastedScribble'] ='';

                    }
                    $scribArray = $allScribbles;
                } else {
                        // TO check if logged user following other profile user or not
                        $userfollowing = $followObj->isLoggedUserFollowingOtherUser($uid,  'U', $loggedUserID, "U");
                        if(count($userfollowing) > 0) {
                                $userDetAry['isFollowing'] = "1";
                                // Get Scribbles only If Logged User is following Other User
                                // get logged User Level
                                $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
                                $logged_user_level = $usrlevel[0]['user_level'];
                                $parentScribbles =array();
                                $allScribbles =array();
                                $parentScribbles = $scribbleObj->getMoreUserPostedParentScribblesWeb($uid,$loggedUserID,"U", $logged_user_level, $limit, $pagenum);
                                foreach($parentScribbles as $key =>$parentScrib) {
                                        $parentId = $parentScrib['scribble_id'];
                                        $allScribbles[$key] = $parentScrib;
                                        $childScribblesCnt = $scribbleObj->getChildScribblesCntByParentId($parentId, $uid,$loggedUserID,"U", $logged_user_level);
                                        $allScribbles[$key]['childScribbles'] = $childScribblesCnt;

                                        //  check if the Parent scribble is Blasted
                                        $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
                                        $allScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                                        $blastedUsrAry = array();
                                        if($blastedUsrCnt > 0) {
                                            //to Get Blasted User Details of the Parent Scribble
                                            $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
                                        }
                                        $allScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                                }
                                $scribArray = $allScribbles;
                        }
                        if(count($userfollowing) == 0) {
                                $userDetAry['isFollowing'] = "0";
                                $scribArray = array();
                        }
                }
                // for changing TIME format of CREATED DATE
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                // IF Blasted Scribble, Get the Original Owner Details
                foreach($scribArray as $key=>$scrib) {

                        if($scrib['blast_id'] != 0) {
                                $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                                //echo "<pre>";print_r($ownerArray);exit;
                                $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                                $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                                $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                                $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                                if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 2;
                                }
                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                        $scribArray[$key]['isBlastedScribble'] = 1;
                                }
                        }
                    $scribArray[$key]['created_date'] = $timeObj->humaneDate($scrib['created_date']);
                }
//            } else {
//                $scribleObj = new Scribbles_Model_Scribbles();
//                $scribArray = $scribleObj->getUserScribbles($user_id, $user_level, $user_type, $loggedUserID);
//            }
            $this->view->pagenum = $pagenum;
            $this->view->limit = $limit;
            $this->view->assign('scribArray',$scribArray);
    }
    
    public function scribbledashboardAction(){
        $request = $this->getRequest();
        // IF CALLING FROM DASHBOARD
        if(Zend_Registry::isRegistered('userdata')) {
           $logged_user_data = Zend_Registry::get('userdata');
           $loggedUserID = $logged_user_data->user_id;
           $loggedUserType = $logged_user_data->user_id;
           $usrArray = $logged_user_data;
        }
        $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):20);
        $this->view->limit = $limit;

        $userObj = new User_Model_User();
        $scrObj = new Scribbles_Model_Scribbles();
        $usrLvlObj = new User_Model_Userlevels();
        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
        $parentScribbles =array();
        $allScribbles =array();
        $newScribAry = array();

        // get logged User Level
        $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
        $owner_level = $usrlevel[0]['user_level'];

        $parentScribbles = $scrObj->getParentScribbles($loggedUserID,$loggedUserID,$loggedUserType, $owner_level, $limit);
//        foreach($parentScribbles as $key =>$parentScrib) {
//               $parentId = $parentScrib['scribble_id'];
//               $allScribbles[$key] = $parentScrib;
//               $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $loggedUserID,$loggedUserID,$loggedUserType, $owner_level);
//               $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
//               $allScribbles[$key]['notifications_type'] = "scribble";
//        }
//        $scribArray = $allScribbles;
//      
        $scribArray = $parentScribbles;
        if($_SERVER['REMOTE_ADDR']=='192.168.1.57'){
            //echo "<pre>";print_r($scribArray);exit;
        }
        // IF Blasted Scribble, Get the Original Owner Details
        foreach($scribArray as $key=>$scrib) {
               if($scrib['blast_id'] != 0) {
                       // To Get Owner Details of the Blasted Scribble
                       $ownerArray = $scrObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                       //echo "<pre>";print_r($ownerArray);print_r($scribArray);exit;
                       $newScribAry = $ownerArray['0'];
                       $followObj = new User_Model_Followers();
                       $userfollowing = $followObj->isLoggedUserFollowingOtherUser($ownerArray['0']['user_id'],  'U', $loggedUserID, "U");
                       //echo "<pre>";print_r($userfollowing);exit;
                       if(count($userfollowing) > 0){
                        $newScribAry['isUpDownScribble'] = $scrib['isUpDownScribble'];
                        $newScribAry['UpDownAction'] = $scrib['UpDownAction'];
                        $newScribAry['isBlastedScribble'] = $scrib['isBlastedScribble'];
                       } else {
                            $newScribAry['isUpDownScribble'] = $scrib['isUpDownScribble'];
                            $newScribAry['UpDownAction'] = $scrib['UpDownAction'];
                            $newScribAry['isBlastedScribble'] = "2";
                       }
                       
//                       $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
//                       $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
//                       $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
//                       $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
//                       
//                       if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
//                               $scribArray[$key]['isBlastedScribble'] = 2;
//                       }
//                       if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
//                               $scribArray[$key]['isBlastedScribble'] = 1;
//                       }
                    $newScribAry['owner_user_id'] = $ownerArray[0]['user_id'];
                    $newScribAry['owner_user_type'] = $ownerArray[0]['from_user_type'];
                    $newScribAry['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                    $newScribAry['owner_user_name'] = $ownerArray[0]['username'];
                    
                    $scribArray[$key] = $newScribAry; 
               }
               
                $parentId1 = $scrib['scribble_id'];
                if($scrib['blast_id'] != 0){
                    $parentId1 = $scrib['blast_id'];
                }
                $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId1, $loggedUserID,$loggedUserID,"U", $owner_level);
                $scribArray[$key]['childScribbles'] = $childScribblesCnt;
                $scribArray[$key]['notifications_type'] = "scribble";
                //  check if the Parent scribble is Blasted
                $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentId1);
                $scribArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                //echo "<PRE>";print_r($scribArray);exit;
                $blastedUsrAry = array();
                if($blastedUsrCnt > 0) {
                    //to Get Blasted User Details of the Parent Scribble
                    $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentId1, $loggedUserID);
                }
                $scribArray[$key]['BlastedUsers'] = $blastedUsrAry;
        }
        
        // for Other Notifications
        $mixedAry = array();
        $mixedAry = $scribArray;
        $scribbleCnt = count($mixedAry);
        if($scribbleCnt >0) {
            $lastScribbleDate = $mixedAry[0]['created_date'];
            $lastScribbleDate1 = $mixedAry[$scribbleCnt-1]['created_date'];
            //echo $lastScribbleDate."-->".$lastScribbleDate1;exit;
            
            // get Logged User Following Users-list FOR getting Notifications
            $followObj = new User_Model_Followers();
            $loggedUserFollowing = $followObj->getFollowingIdsByUserID($loggedUserID, "U");
            if(count($loggedUserFollowing) > 0) {
                    $followIdsArr = array();
                    foreach($loggedUserFollowing as $following) {
                            $followIdsArr[] = $following['follower_id'];
                    }
                    $followerIds = implode(",", $followIdsArr);
            } else $followerIds = "";
            $limit2 = 10;
            $cnt = count($scribArray);

            // TO Get Notifications for Purchase Coupons
            $notificationAry = array();
            $notificationObj = new Notifications_Model_Notifications();
            $notificationAry= $notificationObj->getFollowerCouponNotifications($loggedUserID, $loggedUserID,$usrArray->user_type, $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2);
            if(count($notificationAry) > 0) {
                    //$mixedAry = $scribArray;
                    foreach($notificationAry as $notification) {
                            $i = $cnt++;
                            $mixedAry[$i] = $notification;
                            $mixedAry[$i]['created_date'] = $notification['action_date'];
                    }
            }
            
            $type = "previous";
            // TO Get all Huddle Targets of the Logged in User
            $huddleArray = array();
            $huddleArray = $notificationObj->getHuddleNotifications($loggedUserID,$usrArray->user_type, $lastScribbleDate, $lastScribbleDate1, $limit2, $type);
            if(count($huddleArray) > 0) {
                    foreach($huddleArray as $huddle) {
                            $i = $cnt++;
                            $mixedAry[$i] = $huddle;
                            $mixedAry[$i]['created_date'] = $huddle['action_date'];
                    }
            }
            // to get Photo Uploaded or Blasted Notifications
            $photosObj = new Mobile_Model_Photos();
            $uType = $usrArray->user_type;
            if($uType != "U")
                $uType = "U";
            $photoArrDetails = $photosObj->getPhotoUploadBlasts($loggedUserID, $loggedUserID, "U", $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2);
            if(count($photoArrDetails) > 0) {
                foreach($photoArrDetails as $photo) {
                        $photo_user_type = $photo['user_type'];
                        if($photo['user_type'] != 'U')
                                $photo_user_type = "U";
                        if($loggedUserID == $photo['user_id'] && $photo_user_type == "U") {
                                $photo['isBlasted'] = "2";
                        }
                        $i = $cnt++;
                        $mixedAry[$i] = $photo;

                        // to get Blasted Users Count
                        $photoBlastedUserCnt = $photosObj->getBlastedUserCountBasedOnPhotoId($photo['photo_id']);
                        $blastedUserDetails = array();
                        //echo $photoBlastedUserCnt;exit;
                        if($photoBlastedUserCnt > 0) {
                            $limit1 = 2; // get 2 blasted users
                            $blastedUserDetails = $photosObj->getBlastedUserDetailsBasedOnPhotoId($photo['photo_id'], $loggedUserID, $limit1);
                            //echo "<pre>";print_r($blastedUserDetails);exit;
                        }
                        $mixedAry[$i]['created_date'] = $photo['blasted_date'];
                        $mixedAry[$i]['date'] = $timeObj->humaneDate($photo['blasted_date']);
                        $mixedAry[$i]['notifications_type'] = "photo";
                        $mixedAry[$i]['mediatype'] = "photo";
                        $mixedAry[$i]['photoblastedUserCnt'] = $photoBlastedUserCnt;
                        $mixedAry[$i]['photoblastedUsers'] = $blastedUserDetails;
                }
            }
            //}
            $orderByDate = array();
            foreach ($mixedAry as $key => $row) {
                    $orderByDate[$key]  = strtotime($row['created_date']);
            }
            array_multisort($orderByDate, SORT_DESC, $mixedAry);
            if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') { 
                //echo '<pre>';print_r($mixedAry); exit;
            }

        }
        // for changing TIME format of CREATED DATE
        $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
        foreach($mixedAry as $key=>$scrib) {
                $mixedAry[$key]['date'] = $mixedAry[$key]['created_date'];
                if($scrib['notifications_type'] != 'scribble' && $scrib['notifications_type'] != 'photo') continue;
                        $mixedAry[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
        }
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
            //echo "<pre>";print_r($mixedAry);exit;
        }
        
//        $orderByDate = array();
//        foreach ($scribArray as $key => $row) {     
//                $orderByDate[$key]  = strtotime($row['created_date']);
//        }
//        array_multisort($orderByDate, SORT_DESC, $scribArray);
        //echo "<PRE>";print_r($scribArray);exit;
        $this->view->scribArray = $mixedAry;
        $this->view->scribblecnt = count($scribArray);
        
        if($_SERVER['REMOTE_ADDR'] =='192.168.1.57'){
           //echo "<pre>";print_r($mixedAry);exit;
        }

        // For Downline Scribbles
        $downlineArray = array();
        $downlineArray = $scrObj->getDownlineScribbles($loggedUserID,$owner_level,$limit);
        foreach($downlineArray as $key =>$parentScrib) {
           $parentId = $parentScrib['scribble_id'];
           $downlineArray[$key] = $parentScrib;
           $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $loggedUserID,$loggedUserID,"U", $owner_level,"downline");
           $downlineArray[$key]['childScribbles'] = $childScribblesCnt;

            //  check if the Parent scribble is Blasted
           $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
           $downlineArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
           $blastedUsrAry = array();
           if($blastedUsrCnt > 0) {
               //to Get Blasted User Details of the Parent Scribble
               $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
           }
           $downlineArray[$key]['BlastedUsers'] = $blastedUsrAry;
        }
        $this->view->downlineArray = $downlineArray;

        // For fetching @username Scribbles
        $atUsernameArray = array();
        $atUsernameArray = $scrObj->getAtUserNameScribbles($loggedUserID,$limit);
        $this->view->atUsernameArray = $atUsernameArray;
        
        $purchasecouponsObj = new Business_Model_Purchasecoupons;
        $purchasecoupons = $purchasecouponsObj->getpurchasecouponsinfo($loggedUserID);
        $this->view->purchasecoupons = $purchasecoupons;
        
        $pagenum = 0;
        $this->view->pagenum = $pagenum;
    }

    public function getchildscribblesAction(){
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        //echo "<pre>";print_r($request->getParams());exit;
        $parent_id = $request->getParam('parent_id');
        $user_Id = $request->getParam('from_user_id');
        $tabType = $request->getParam('tabType');
        if(Zend_Registry::isRegistered('userdata')) {
           $logged_user_data = Zend_Registry::get('userdata');
           $loggedUserID = $logged_user_data->user_id;
           $loggedUserType = $logged_user_data->user_id;
           $loggedUserLevel = $logged_user_data->user_level;
        }
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):40);
        $scribbleObj = new Scribbles_Model_Scribbles();
        $childScribbles = $scribbleObj->getChildScribblesByParentId($parent_id, $user_Id,$loggedUserID,$loggedUserType, $loggedUserLevel);
        //echo "<pre>";print_r($childScribbles);exit
        
        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
        $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
        // IF Blasted Scribble, Get the Original Owner Details
        foreach($childScribbles as $key=>$scrib) {
                if($scrib['blast_id'] != 0) {
                        // To Get Owner Details of the Blasted Scribble
                        $ownerArray = $scribbleObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                        if($_SERVER['REMOTE_SERVER'] == '192.168.1.57') {
                            //echo "<pre>";print_r($ownerArray);exit;
                        }
                        $childScribbles[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                        $childScribbles[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                        $childScribbles[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                        $childScribbles[$key]['owner_user_name'] = $ownerArray[0]['username'];
                        if($scribArray[$key]['owner_user_id'] == $loggedUserID && $childScribbles[$key]['owner_user_type'] == "U") {
                                $childScribbles[$key]['isBlastedScribble'] = 2;
                        }
                        if($scribArray[$key]['owner_user_id'] != $loggedUserID && $childScribbles[$key]['owner_user_type'] == "U") {
                                $childScribbles[$key]['isBlastedScribble'] = 1;
                        }
                }
                //  check if the Parent scribble is Blasted
                $blastedUsrCnt = $scribbleObj->getBlastedUsersCntByScribbleId($scrib['scribble_id']);
                $childScribbles[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                $blastedUsrAry = array();
                if($blastedUsrCnt > 0) {
                    //to Get Blasted User Details of the Parent Scribble
                    $blastedUsrAry = $scribbleObj->getBlastedUsersByScribbleId($scrib['scribble_id'], $loggedUserID);
                }
                $childScribbles[$key]['BlastedUsers'] = $blastedUsrAry;
                if($scrib['blast_id'] == 0) {
                    foreach($blastedUsrAry as $blastedby){
                        if($blastedby['user_id'] == $loggedUserID){
                            $childScribbles[$key]['isBlastedScribble'] = 1;
                        }
                    }
                }
        }
        $this->view->scribArray = $childScribbles;
        $this->view->tabType = $tabType;
    }

    public function getmorelevelscribbleswebAction(){
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        //echo "<pre>";print_r($request->getParams());exit;
        if(Zend_Registry::isRegistered('userdata')) {
           $logged_user_data = Zend_Registry::get('userdata');
           $loggedUserID = $logged_user_data->user_id;
           $loggedUserType = $logged_user_data->user_id;
           $usrArray = $logged_user_data;
        }
        $user_id = $request->getParam('user_id');
        $scribble_id = $request->getParam('scribble_id');
        $type = $request->getParam('type');
        $ntype = $request->getParam('ntype');
        $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):20);
        $pagenum = $request->getParam('pagenum');
        
        $this->view->pagenum = $pagenum;
        $this->view->limit = $limit;
        $userObj = new User_Model_User();
        $scrObj = new Scribbles_Model_Scribbles();
        $usrLvlObj = new User_Model_Userlevels();
        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
        $parentScribbles =array();
        $allScribbles =array();
        
        $latesttype = 'scribbleId';
        if($ntype == 'purchasecoupon' || $ntype == 'huddle_target') {
            
            // Get Purchase_Coupon/Huddle_Target Created Date by Coupon Id
            $notificationObj = new Notifications_Model_Notifications();
            $couponDetails = $notificationObj->getNotoficationDetailsByNotificationID($scribble_id);
            $latesttype = 'datetime';
            $scribble_id = $couponDetails['action_date'];
        }
        
        if($ntype == 'photo') {
            // Get Photo Details By Photo ID
            $photosObj = new Mobile_Model_Photos();
            $photoDetails = $photosObj->getphotobyphotoid($scribble_id);
            //echo "<pre>";print_r($photoDetails);exit;
            $latesttype = 'datetime';
            $scribble_id = $photoDetails['created_date'];
        }
        
        // get logged User Level
        $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
        $owner_level = $usrlevel[0]['user_level'];
//        $parentScribbles = $scrObj->getMoreParentScribblesWeb($loggedUserID,$loggedUserID,$loggedUserType, $owner_level, $limit, $pagenum);
        $owner_user_type = 'U';
        if($usrArray->user_type != 'U')
            $owner_user_type = 'U';
        $parentScribbles = $scrObj->getMoreParentScribbles($user_id,$loggedUserID,$owner_user_type, $owner_level, $scribble_id, $type,$latesttype, $limit);
        foreach($parentScribbles as $key =>$parentScrib) {
               $parentId = $parentScrib['scribble_id'];
               $allScribbles[$key] = $parentScrib;
               $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $loggedUserID,$loggedUserID,$loggedUserType, $owner_level);
               $allScribbles[$key]['childScribbles'] = $childScribblesCnt;
               $allScribbles[$key]['notifications_type'] = "scribble";
        }
        $scribArray = $allScribbles;


        // IF Blasted Scribble, Get the Original Owner Details
        foreach($scribArray as $key=>$scrib) {
               if($scrib['blast_id'] != 0) {
                       // To Get Owner Details of the Blasted Scribble
                       $ownerArray = $scrObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                       //echo "<pre>";print_r($ownerArray);exit;
                       $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                       $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                       $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                       $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                       if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                               $scribArray[$key]['isBlastedScribble'] = 2;
                       }
                       if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                               $scribArray[$key]['isBlastedScribble'] = 1;
                       }
               }

               //  check if the Parent scribble is Blasted
               $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($scrib['scribble_id']);
               $scribArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
               $blastedUsrAry = array();
               if($blastedUsrCnt > 0) {
                   //to Get Blasted User Details of the Parent Scribble
                   $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($scrib['scribble_id'], $loggedUserID);
               }
               $scribArray[$key]['BlastedUsers'] = $blastedUsrAry;
        }
        // for Other Notifications
        $mixedAry = array();
        $mixedAry = $scribArray;
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
            //echo "<pre>";print_r($mixedAry);//exit;
        }
        $scribbleCnt = count($mixedAry);
        if($scribbleCnt >0) {
            $lastScribbleDate = $mixedAry[0]['created_date'];
            $lastScribbleDate1 = $mixedAry[$scribbleCnt-1]['created_date'];
            //echo $lastScribbleDate."-->".$lastScribbleDate1;
            //echo "<pre>";print_r($mixedAry);
            //exit;
            // get Logged User Following Users-list FOR getting Notifications
            $followObj = new User_Model_Followers();
            $loggedUserFollowing = $followObj->getFollowingIdsByUserID($loggedUserID, "U");
            if(count($loggedUserFollowing) > 0) {
                    $followIdsArr = array();
                    foreach($loggedUserFollowing as $following) {
                            $followIdsArr[] = $following['follower_id'];
                    }
                    $followerIds = implode(",", $followIdsArr);
            } else $followerIds = "";
            $limit2 = 10;
            $cnt = count($scribArray);

            // TO Get Notifications for Purchase Coupons
            $notificationAry = array();
            $notificationObj = new Notifications_Model_Notifications();
            $notificationAry= $notificationObj->getFollowerCouponNotifications($loggedUserID, $loggedUserID,$usrArray->user_type, $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2, $type);
            if(count($notificationAry) > 0) {
                    //$mixedAry = $scribArray;
                    foreach($notificationAry as $notification) {
                            $i = $cnt++;
                            $mixedAry[$i] = $notification;
                            $mixedAry[$i]['created_date'] = $notification['action_date'];
                    }
            }
            // TO Get all Huddle Targets of the Logged in User
            $huddleArray = array();
            $huddleArray = $notificationObj->getHuddleNotifications($loggedUserID,$usrArray->user_type, $lastScribbleDate, $lastScribbleDate1, $limit2, $type);
            if(count($huddleArray) > 0) {
                    foreach($huddleArray as $huddle) {
                            $i = $cnt++;
                            $mixedAry[$i] = $huddle;
                            $mixedAry[$i]['created_date'] = $huddle['action_date'];
                    }
            }
            //echo "<pre>";print_r($mixedAry);exit;
            // to get Photo Uploaded or Blasted Notifications
            $photosObj = new Mobile_Model_Photos();
            $uType = $usrArray->user_type;
            if($uType != "U")
                $uType = "U";
            $photoArrDetails = array();
            $photoArrDetails = $photosObj->getPhotoUploadBlasts($loggedUserID, $loggedUserID, "U", $lastScribbleDate, $lastScribbleDate1, $followerIds, $limit2, $type);
            if(count($photoArrDetails) > 0) {
                foreach($photoArrDetails as $photo) {
                        $photo_user_type = $photo['user_type'];
                        if($photo['user_type'] != 'U')
                                $photo_user_type = "U";
                        if($loggedUserID == $photo['user_id'] && $photo_user_type == "U") {
                                $photo['isBlasted'] = "2";
                        }
                        $i = $cnt++;
                        $mixedAry[$i] = $photo;

                        // to get Blasted Users Count
                        $photoBlastedUserCnt = $photosObj->getBlastedUserCountBasedOnPhotoId($photo['photo_id']);
                        $blastedUserDetails = array();
                        //echo $photoBlastedUserCnt;exit;
                        if($photoBlastedUserCnt > 0) {
                            $limit = 2; // get 2 blasted users
                            $blastedUserDetails = $photosObj->getBlastedUserDetailsBasedOnPhotoId($photo['photo_id'], $loggedUserID, $limit);
                            //echo "<pre>";print_r($blastedUserDetails);exit;
                        }
                        $mixedAry[$i]['created_date'] = $photo['blasted_date'];
                        $mixedAry[$i]['date'] = $timeObj->humaneDate($photo['blasted_date']);
                        $mixedAry[$i]['notifications_type'] = "photo";
                        $mixedAry[$i]['mediatype'] = "photo";
                        $mixedAry[$i]['photoblastedUserCnt'] = $photoBlastedUserCnt;
                        $mixedAry[$i]['photoblastedUsers'] = $blastedUserDetails;
                        if($photo['photo_id'] == '1662'){
                            //echo '<pre>';print_r($mixedAry);exit; 
                        }
                }
            }
            //}
            $orderByDate = array();
            foreach ($mixedAry as $key => $row) {
                    $orderByDate[$key]  = strtotime($row['created_date']);
            }
            array_multisort($orderByDate, SORT_DESC, $mixedAry);
            if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') { 
                //echo '<pre>';print_r($mixedAry);exit; 
            }
        }
        // for changing TIME format of CREATED DATE
        $scribObj = new UpmeSocial_View_Helper_Scribbleusername();
        foreach($mixedAry as $key=>$scrib) {
                $mixedAry[$key]['date'] = $mixedAry[$key]['created_date'];
                if($scrib['notifications_type'] != 'scribble' && $scrib['notifications_type'] != 'photo') continue;
                        $mixedAry[$key]['scribbleusername'] = $scribObj->scribbleUname($scrib,30);
        }
        $this->view->scribArray = $mixedAry;
        $this->view->scribblecnt = count($scribArray);
    }

    public function getmoredownlinescribbleswebAction(){
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        
        if(Zend_Registry::isRegistered('userdata')) {
           $logged_user_data = Zend_Registry::get('userdata');
           $loggedUserID = $logged_user_data->user_id;
           $loggedUserType = $logged_user_data->user_id;
        }
        $limit = (($request->getParam('limit') != '') ? $request->getParam('limit'):20);
        $pagenum = $request->getParam('pagenum');
        //echo $pagenum;exit;
        $this->view->pagenum = $pagenum;
        $this->view->limit = $limit;
        $userObj = new User_Model_User();
        $scrObj = new Scribbles_Model_Scribbles();
        $usrLvlObj = new User_Model_Userlevels();

        $parentScribbles =array();
        $allScribbles =array();

        // get logged User Level
        $usrlevel = $usrLvlObj->getUserLevelByUserId($loggedUserID, 'U');
        $owner_level = $usrlevel[0]['user_level'];

        // For Downline Scribbles
        $downlineArray = array();
        $downlineArray = $scrObj->getMoreDownlineScribblesWeb($loggedUserID,$owner_level,$limit, $pagenum);
        foreach($downlineArray as $key =>$parentScrib) {
           $parentId = $parentScrib['scribble_id'];
           $downlineArray[$key] = $parentScrib;
           $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $loggedUserID,$loggedUserID,"U", $owner_level,"downline");
           $downlineArray[$key]['childScribbles'] = $childScribblesCnt;

            //  check if the Parent scribble is Blasted
           $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentScrib['scribble_id']);
           $downlineArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
           $blastedUsrAry = array();
           if($blastedUsrCnt > 0) {
               //to Get Blasted User Details of the Parent Scribble
               $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentScrib['scribble_id'], $loggedUserID);
           }
           $downlineArray[$key]['BlastedUsers'] = $blastedUsrAry;
        }
        $this->view->downlineArray = $downlineArray;
    }
    
    public function getmoreuserprofilelevelscribbleswebAction() {
        $this->_helper->layout->disableLayout();
        //action body
        $scribArray = array();
        $newScribArray = array();
        if(Zend_Registry::isRegistered('userdata')) {
                $logged_user_data = Zend_Registry::get('userdata');
                $loggedUserID = $logged_user_data->user_id;
                //$user_account_status = $logged_user_data->user_account_status;
                //$loggedUserType = 'U';
        }
        // Fetch LOgged USer Level
        $usrLevelObj = new User_Model_Userlevels();
        $loggedUserLevelArray = $usrLevelObj->getUserLevelByUserId($loggedUserID, "U");
        $loggedUserLevel = $loggedUserLevelArray['0']['user_level'];

        $request = $this->getRequest();
        if($request->getParam('user_id') != '') {
                $user_id = $request->getParam('user_id');
                $userObj = new User_Model_User();
                $userdata = $userObj->getUserDetails($user_id);
                $user_type = $userdata->user_type;
                $user_level = $userdata->user_level;
        } else {
                $user_id = $logged_user_data->user_id;
                $user_type = $logged_user_data->user_type;
                $user_level = $logged_user_data->user_level;
                $userdata = $logged_user_data;
        }
        $scrObj = new Scribbles_Model_Scribbles();
        //echo '<pre>';print_r($request->getParams());exit;
        // FOR Fetching User LELEV SCRIBBLES Info
        $pagenum = $request->getParam('pagenum');
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):20);
        if($user_id == $loggedUserID && $logged_user_data->user_type != 'B') {
                $scribArray = $scrObj->getMoreUserPostedAndBlastedParentScribblesWeb($user_id,$logged_user_data->user_id,$logged_user_data->user_type, $logged_user_data->user_level, $limit, $pagenum);
                $purchasecouponsObj = new Business_Model_Purchasecoupons;
                $purchasecoupons = $purchasecouponsObj->getpurchasecouponsinfo($user_id);
                //$couponscnt = $purchasecouponsObj->getcouponscntbyid($user_id,$purchasecoupons->coupon_id);
        } else {
                // TO check if following other profile user or not
                $followObj = new User_Model_Followers();
                $userfollowing = $followObj->isLoggedUserFollowingOtherUser($user_id,  'U', $loggedUserID, "U");
                if(count($userfollowing) > 0) {
                        $scribArray = $scrObj->getMoreUserPostedAndBlastedParentScribblesWeb($user_id,$logged_user_data->user_id,$user_type, $logged_user_data->user_level, $limit, $pagenum);
                }
                if(count($userfollowing) == 0) {
                        $scribArray = array();
                }
                $purchasecouponsObj = new Business_Model_Purchasecoupons;
                $purchasecoupons = $purchasecouponsObj->getpurchasecouponsinfo($user_id);
        }
        //}
        
        foreach($scribArray as $key=>$scrib) {
                $isViewableScribble = 0;                
                if($scrib['blast_id'] != 0) {
                        $ownerArray = $scrObj->getOriginalOwnerOfScribble($scrib['blast_id']);
                        if($scrib['user_level'] < $ownerArray['0']['user_level']){
                            $isViewableScribble = 1;
                        }
                        if($isViewableScribble == 0){
                            $scribArray[$key] = $ownerArray[0];
                            $scribArray[$key]['owner_user_id'] = $ownerArray[0]['user_id'];
                            $scribArray[$key]['owner_user_type'] = $ownerArray[0]['from_user_type'];
                            $scribArray[$key]['owner_name'] = $ownerArray[0]['firstname']." ".$ownerArray[0]['lastname'];
                            $scribArray[$key]['owner_user_name'] = $ownerArray[0]['username'];
                            if($scribArray[$key]['owner_user_id'] == $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
                                    $scribArray[$key]['isBlastedScribble'] = 2;
                            }
                            $scribArray[$key]['isBlastedScribble'] = 1;
                            $scribArray[$key]['isUpDownScribble'] = "";
//                                if($scribArray[$key]['owner_user_id'] != $loggedUserID && $scribArray[$key]['owner_user_type'] == "U") {
//                                        $scribArray[$key]['isBlastedScribble'] = 1;
//                                }
                            $newScribArray[$key] = $scribArray[$key];
                        }
                }
                
                
                
                if($isViewableScribble == 0){
                    $parentId = $scrib['scribble_id'];
                    if($scrib['blast_id'] != 0){
                        $parentId = $scrib['blast_id'];
                    }
                   
                    // to get childscribbleCnt
                    $childScribblesCnt = $scrObj->getChildScribblesCntByParentId($parentId, $user_id,$loggedUserID,"U", $loggedUserLevel);
                    $scribArray[$key]['childScribbles'] = $childScribblesCnt;

                    //  check if the Parent scribble is Blasted
                    $blastedUsrCnt = $scrObj->getBlastedUsersCntByScribbleId($parentId);
                    $scribArray[$key]['BlastedUsersCnt'] = $blastedUsrCnt;
                    $blastedUsrAry = array();
                    if($blastedUsrCnt > 0) {
                        //to Get Blasted User Details of the Parent Scribble
                        $blastedUsrAry = $scrObj->getBlastedUsersByScribbleId($parentId, $loggedUserID);
                    }
                    $scribArray[$key]['BlastedUsers'] = $blastedUsrAry;
                    if($scrib['blast_id'] == 0) {
                        foreach($blastedUsrAry as $blastedby){
                            if($blastedby['user_id'] == $loggedUserID){
                                $scribArray[$key]['isBlastedScribble'] = 1;
                            }
                        }
                    }
                    $newScribArray[$key] = $scribArray[$key];
                }
        }
        
        $this->view->scribArray = $newScribArray;
        $this->view->scribblecnt = count($scribArray);
        
        $this->view->pagenum = $pagenum;
        $this->view->limit = $limit;
        $this->view->user_id = $user_id;
        $this->view->userdata = $userdata;
    }
    
    public function getallscribbleblastedusersAction(){
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();

            $logged_user_Id = $request->logged_user_id;
            $scribbleId = $request->scribble_id;
            $logged_user_type = 'U';
            
            // to get all scribble blasted Users
            $scribbleObj = new Scribbles_Model_Scribbles();
            $blastedUsers = $scribbleObj->getAllScribbleBlastedUsersWeb($scribbleId);
            
            // to get logged user Followings
            $followObj = new User_Model_Followers();
            $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
            
            // check if any following users available in Blasted Users
            $followingCnt = count($loggedUserFollowing);
            if(count($blastedUsers) > 0) {
                    foreach($blastedUsers as $key => $followingUsr) {
                            if($followingCnt > 0) {
                                    foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                        $followUser_type = "U";
                                        if($followingUsr['user_type'] != 'U') 
                                            $followUser_type = "U";
                                            
                                        if($followingUsr['user_id'] == $logged_user_Id && strtolower($followUser_type) == strtolower($logged_user_type)) {
                                                    $blastedUsers[$key]['isFollowing'] = "2";
                                        } else {
                                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && (strtolower($followUser_type) == strtolower($Usrfollows['follower_type']))) {
                                                        $blastedUsers[$key]['isFollowing'] = "1";
                                                        break;
                                                } else {
                                                        $blastedUsers[$key]['isFollowing'] = "0";
                                                }
                                        }
                                    }
                            } else {
                                $blastedUsers[$key]['isFollowing'] = "0";
                            }
                    }
            }
            
            
            //echo "<pre>";print_r($blastedUsers);exit;
            $html = '<div style="position: absolute; background: none repeat scroll 0 0 #D1D0D0; border-radius:5px;" class="popupDiv">';
            foreach($blastedUsers as $usr){
                    $name = ucfirst($usr['firstname'])." ".$usr['lastname'];
                $html .='<div style="padding:5px;float:left; width:95%">
                                <a href="'.SITE_URL.'user/userprofile/'.$usr['user_id'].'">
                                    <h1>'.$name.'</h1>
                                    <h2>@'.$usr['username'].'</h2>
                                </a>
                            </div>
                        <div class="clear"></div>';
            }
            $html .='</div>';
            echo $html;exit;
            if (!empty($blastedUsers)) {
                    $resultAry = array('service_status'=>'success',"content" => $blastedUsers,"usermediacode"=>"450");
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No Users to View',"usermediacode"=>"450");
            }
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry);
            return false;
        }
        
    public function targetdetailsAction(){
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $userId = trim($request->getParam('user_id'));

        $huddleObj = new Huddles_Model_Huddles();

        //get rooms count that targeted the user
        $roomCnt = $huddleObj->getTagetedRoomsCnt($userId);
        if($roomCnt == 0)
                $msg = 'Currently No users are talking about you';

        //get users count that targeted the user
        $usersCnt = $huddleObj->getUsersTargetedCnt($userId);
        if($roomCnt == 1)
                $msg = $usersCnt.' people are talking about you in a Huddle room';
        if($roomCnt > 1)
                $msg = $usersCnt.' people are talking about you in '.$roomCnt.' Huddle rooms';
        $this->view->msg = $msg;
    }
    
    public function getallphotoblastedusersAction(){
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $request = $this->getRequest();

            $logged_user_Id = $request->logged_user_id;
            $photoId = $request->photo_id;
            $logged_user_type = 'U';
            $type = "photo";
            $limit='';
            //echo $logged_user_Id;exit;
            // to get all scribble blasted Users
            $photosObj = new Mobile_Model_Photos();
            $blastedUsers = $photosObj->getAllPhotoBlastedUsers($logged_user_Id,$logged_user_type, $photoId);
            if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
                //echo "<pre>";print_r($blastedUsers);exit;
            }
            // to get logged user Followings
            $followObj = new User_Model_Followers();
            
            $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
            
            // check if any following users available in Blasted Users
            $followingCnt = count($loggedUserFollowing);
            if(count($blastedUsers) > 0) {
                    foreach($blastedUsers as $key => $followingUsr) {
                            if($followingCnt > 0) {
                                    foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                        $followUser_type = "U";
                                        if($followingUsr['user_type'] != 'U') 
                                            $followUser_type = "U";
                                            
                                        if($followingUsr['user_id'] == $logged_user_Id && strtolower($followUser_type) == strtolower($logged_user_type)) {
                                                    $blastedUsers[$key]['isFollowing'] = "2";
                                        } else {
                                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && (strtolower($followUser_type) == strtolower($Usrfollows['follower_type']))) {
                                                        $blastedUsers[$key]['isFollowing'] = "1";
                                                        break;
                                                } else {
                                                        $blastedUsers[$key]['isFollowing'] = "0";
                                                }
                                        }
                                    }
                            } else {
                                $blastedUsers[$key]['isFollowing'] = "0";
                            }
                    }
            }
            
            
            //echo "<pre>";print_r($blastedUsers);exit;
            $html = '<div style="position: absolute; background: none repeat scroll 0 0 #D1D0D0; border-radius:5px;" class="popupDiv">';
            foreach($blastedUsers as $usr){
                    $name = ucfirst($usr['firstname'])." ".$usr['lastname'];
                $html .='<div style="padding:5px;float:left; width:95%">
                                <a href="'.SITE_URL.'user/userprofile/'.$usr['user_id'].'">
                                    <h1>'.$name.'</h1>
                                    <h2>@'.$usr['username'].'</h2>
                                </a>
                            </div>
                        <div class="clear"></div>';
            }
            $html .='</div>';
            echo $html;exit;
            if (!empty($blastedUsers)) {
                    $resultAry = array('service_status'=>'success',"content" => $blastedUsers,"usermediacode"=>"450");
            } else {
                    $resultAry = array('service_status'=>'error',"error_msg" => 'No Users to View',"usermediacode"=>"450");
            }
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry);
            return false;
        }

}