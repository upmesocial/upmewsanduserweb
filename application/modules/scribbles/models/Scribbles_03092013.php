<?php
class Scribbles_Model_Scribbles {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Scribbles_Model_DbTable_Scribbles');
        }
        return $this->_dbTable;
    }

    //post a scribble
    public function insertScribble($data) {
            return $this->getDbTable()->insert($data);
    }

    //function to get single scribble information
    public function getScribleInfo($scribeId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                    ->from(array('S' => 'tbl_scribbles'))
                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)','profile_pic_path','username'))
                    ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                    ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                    ->where('S.scribble_id = ?',$scribeId)
                    ->setIntegrityCheck(false);
        $resArray = $db->fetchAll($select);
        return $resArray;
    }

    public function getUserScriblesById($userID,$loggedUserId, $user_type, $user_level, $offset='', $limit = '') {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $db = Zend_Db_Table::getDefaultAdapter();
        if($offset == '')
            $offset = 0;
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserId) {
            $select = $db->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B'", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                            ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U'", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$userID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$userID."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('
                                    (
                                        (S.from_user_id = "'.$userID.'" AND S.from_user_type = "'.$user_type.'" AND S.blast_id =0)
                                        OR (S.to_user_id = "'.$userID.'" AND S.from_user_type = "'.$user_type.'")
                                        OR (S.from_user_id = F.follower_id)
                                    )
                                    AND S.user_level <=  "'.$user_level.'"')
                            ->group('S.scribble_id')
                            ->order(array("S.created_date DESC"));
        } else {
            $select = $db->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                            ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('(S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id) AND S.user_level <=  "'.$user_level.'"')
                            ->group('S.scribble_id')
                            ->order(array("S.created_date DESC"));
        }
        if($limit != '' && ($offset == '' || $offset == '40')) {
            $select->limit($limit, $offset);
        } else {
            $select->limit('30', $limit);
        }
//        echo $select;exit;
        $resArray = $db->fetchAll($select);
        //echo '<Pre>';print_r($resArray);exit;
        return $resArray;
    }

    public function getUserScriblesCntById($userID,$loggedUserId, $user_type, $user_level, $offset='', $limit = '') {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $db = Zend_Db_Table::getDefaultAdapter();
        if($offset == '')
            $offset = 0;
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserId) {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B'", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                            ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U'", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$userID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$userID."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('((S.from_user_id = "'.$userID.'" AND S.from_user_type = "'.$user_type.'" AND S.blast_id =0) OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id) AND S.user_level <=  "'.$user_level.'"')
                            ->group('S.scribble_id')
                            ->order(array("S.created_date DESC"))
                            ->setIntegrityCheck(false);
        } else {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                            ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('(S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id) AND S.user_level <=  "'.$user_level.'"')
                            ->group('S.scribble_id')
                            ->order("created_date DESC")
                            ->setIntegrityCheck(false);
        }
        if($limit != '') {
            $select->limit($limit,$offset);
        }
        $resArray = COUNT($db->fetchAll($select));
        return $resArray;
    }

    public function getUserScriblesWithReplyByScribbleId($userID,$loggedUserId, $user_type, $user_level, $limit = '') {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserId) {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                            ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$userID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$userID."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('((S.from_user_id = "'.$userID.'" AND S.blast_id =0) OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id) AND S.user_level <= "'.$user_level.'"')
                            ->group('S.scribble_id')
                            ->order(array("S.created_date DESC"))
                            ->setIntegrityCheck(false);
        } else {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                            ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('(S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id) AND S.user_level <= "'.$user_level.'"')
                            ->group('S.scribble_id')
                            ->order("created_date DESC")
                            ->setIntegrityCheck(false);
        }
        if($limit != '')
            $select->limit($limit,0);
        $resArray = $db->fetchAll($select);
        return $resArray;
    }

    public function getScribbleCnt($userID,$user_level, $user_type='U') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                    ->from(array('S' => 'tbl_scribbles'))
                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                    ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                    ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                    ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                    ->where('S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id')
                    ->where('S.user_level <=  "'.$user_level.'"')
                    ->group('S.scribble_id')
                    ->setIntegrityCheck(false);
        //if($_SERVER['REMOTE_ADDR'] == '192.168.1.58') { echo $select;exit; }
        $resultSet = count($db->fetchAll($select));
        return $resultSet;
    }

    public function getUserPostedScribbleCnt($userID, $user_level, $user_type='U') {
        $select = $this->getDbTable()->select()
                    ->from(array('S' => 'tbl_scribbles'), array('scribbleCnt'=>'COUNT(S.scribble_id)'))
                    ->where('S.from_user_id = "'.$userID.'" AND S.blast_id=0 AND S.parent_id=0')
                    ->where('S.user_level <=  "'.$user_level.'"');
//        $select = $this->getDbTable()->select()
//                            ->from(array('S' => 'tbl_scribbles'))
//                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username' => 'CONCAT(U.firstname, " ",U.lastname)'))
//                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
//                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
//                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
//                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
//                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"')
//                            ->group('S.scribble_id')
//                             ->setIntegrityCheck(false);
        //echo "<br>".$select;exit;
        $resultSet = $this->getDbTable()->fetchAll($select);
        //echo "<pre>";print_r(count($resultSet));exit;
        return $resultSet[0]['scribbleCnt'];
    }

    public function Actioncheck($loggedUserId, $uType, $actionType, $scribbleId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from('tbl_user_up_downs', array('id'))
                        ->where('user_id = ?',$loggedUserId)
                        ->where('user_type = ?',$uType)
                        ->where('action_type = ?',$actionType)
                        ->where('scribble_id = ?',$scribbleId);
        $resultSet = $db->fetchAll($select);
        return $resultSet;
    }

    public function getdaycount($loggedUserId, $actionType) {
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from('tbl_user_up_downs', array('COUNT(id) as count'))
                        ->where('user_id = ?',$loggedUserId)
                        ->where('action_type = ?',$actionType)
                        ->where('date(action_date) = ?',$date);
        //echo $select;exit;
        $resultSet = $db->fetchRow($select);
        return $resultSet;
    }

    public function upScribble($uId, $uType, $actionType, $scribbleId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        // for inserting Scribble UP
        $inserted = $db->insert('tbl_user_up_downs',array('user_id'=>$uId,'user_type'=>$uType,'action_type'=>$actionType,'scribble_id'=>$scribbleId));
        return $inserted;
    }

    public function downScribble($uId, $uType, $actionType, $scribbleId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $inserted = $db->insert('tbl_user_up_downs',array('user_id'=>$uId,'user_type'=>$uType,'action_type'=>$actionType,'scribble_id'=>$scribbleId));
        return $inserted;
    }

    public function updateCoolPoints($userID, $uType, $actType) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $coolpoints = $this->fetchcoolpoints($actType);
        $select = $db->select()
                    ->from(array('C' => 'tbl_user_coolpoints'))
                    ->where('C.user_id = "'.$userID.'" AND C.user_type = "'.$uType.'"');
        $pointsArray = $db->fetchAll($select);
        $resultCnt = count($pointsArray);
        if($resultCnt == 0) {
            $insArray['user_id'] = $userID;
            $insArray['user_type'] = $uType;
            if($actType == 'up') {
                $insArray['cool_points'] = $coolpoints['points'];
            }
            if($actType == 'down') {
                $insArray['cool_points'] = $coolpoints['points'];
            }
            if($actType == 'blast') {
                $insArray['cool_points'] = $coolpoints['points'];
            }
            if($actType == 'photoblast') {
                $insArray['cool_points'] = $coolpoints['points'];
            }
            if($actType == 'videoblast') {
                $insArray['cool_points'] = $coolpoints['points'];
            }
            $db->insert('tbl_user_coolpoints',$insArray);
                return true;
        }
        if($actType == 'up') {
            $updateArray['cool_points'] = $pointsArray['0']['cool_points']+$coolpoints['points'];
        }
        if($actType == 'down') {
            $updateArray['cool_points'] = $pointsArray['0']['cool_points']+$coolpoints['points'];
        }
        if($actType == 'blast') {
            $updateArray['cool_points'] = $pointsArray['0']['cool_points']+$coolpoints['points'];
        }
        if($actType == 'photoblast') {
            $updateArray['cool_points'] = $pointsArray['0']['cool_points']+$coolpoints['points'];
        }
        if($actType == 'videoblast') {
            $updateArray['cool_points'] = $pointsArray['0']['cool_points']+$coolpoints['points'];
        }
        $where = 'user_id = "'.$userID.'" AND user_type="'.$uType.'"';
        $db->update('tbl_user_coolpoints',$updateArray,$where);
        return $updateArray['cool_points'];
    }

    public function fetchcoolpoints($type) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from(array('tbl_coolpoints_settings'), array('name','points'))
                        ->where('name =?',$type);
        $coolpoints = $db->fetchRow($select);
        return $coolpoints;
    }

    public function blastcheck($loggedUserId, $blastId, $scribbleId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from('tbl_blasts', array('blast_id'))
                        ->where('user_id = ?',$loggedUserId)
                        ->where('blast_id = ?',$blastId)
                        ->where('scribble_id = ?',$scribbleId);
        $resultSet = $db->fetchAll($select);
        return $resultSet;
    }

    public function getblastdaycount($loggedUserId) {
                                    $loggedUserType = 'U';
        /* SELECT COUNT(S.scribble_id) AS `count`
            FROM `tbl_scribbles` AS `S`
            LEFT JOIN tbl_blasts B ON S.blast_id = B.scribble_id
            WHERE B.user_id = '8' AND S.from_user_id = '8' AND S.created_date LIKE '2013-06-04%'*/
        $date = date('Y-m-d');
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from('tbl_scribbles as S', array('COUNT(S.scribble_id) as count'))
                        ->joinLeft('tbl_blasts as B', 'S.blast_id = B.scribble_id', '')
                        ->where('B.user_id = ?',$loggedUserId)
                        ->where('S.from_user_id = ?',$loggedUserId)
                        ->where('date(S.created_date) = ?',$date);

        $resultSet = $db->fetchRow($select);
		$scribbleCnt = $resultSet['count'];

		$select = $db->select()
						->from('tbl_photos', array('COUNT(photo_id) as count'))
						->where('user_id = ?',$loggedUserId)
						->where('user_type = ?',$loggedUserType)
						->where('blast_id != 0')
						->where('date(blasted_date) = ?',$date);

		$resultSet = $db->fetchRow($select);
		$resultSet['count']+= $scribbleCnt;
        return $resultSet;
    }

    public function blastScribble($data) {
          return $this->getDbTable()->insert($data);
    }

    public function insertBlast($data) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $ins = $db->insert('tbl_blasts',$data);
        return $ins;
    }

    public function getDownlineScribbles($user_id, $user_level,$limit='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $user_type ="U";
        // FOr Fetching FOllowers of the User Specified
        $followObj = new User_Model_Followers();
        $usrFollowersArray = array();
        $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id', 'follower_type'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                        ->where('F.user_id = "'.$user_id.'" AND F.user_type ="U" AND F.follower_type = "U"');
        $usrFollowersArray = $db->fetchAll($select);
        $usrFollowersArray[] = array('follower_id' => $user_id,'follower_type' => 'U');
        if(count($usrFollowersArray) > 0) {
            $usrCnt = count($usrFollowersArray);
            $i = 0;

            foreach($usrFollowersArray as $followers) {
                if($i == 0)
                    $ids = "'".$followers['follower_id']."'";
                else
                    $ids = $ids.","."'".$followers['follower_id']."'";
                $i++;
            }
            $select1 = $db->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','business_name as username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$user_id."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$user_id."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            //->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$user_id."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('((S.from_user_id = "'.$user_id.'" AND S.from_user_type = "U" AND S.blast_id =0) OR S.to_user_id = "'.$user_id.'" OR S.from_user_id = F.follower_id) AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0" AND S.blast_id = 0')
                            ->where('U.user_id  IN ('.$ids.') OR U.user_id = "'.$user_id.'"')
                            ->where('S.user_level <= "'.$user_level.'"')
                            ->where('S.user_level != 0')
                            ->order ('S.created_date DESC')
                            ->group('S.scribble_id')
                            ->limit($limit);
            //echo $select1; exit();
        	$scribblesArray = $db->fetchAll($select1);
        	$scribbleIdAry = array();
            $res = array();
            $result = array();
            for($k=0;$k<count($scribblesArray);$k++) {
                    $scrId = $scribblesArray[$k]['scribble_id'];
                    $res[$scrId] = $scribblesArray[$k];
                    $qry = $this->getDbTable()->select()
                                    ->from(array('S' => 'tbl_scribbles'), array('scribble_id','blast_id','from_user_id'))
                                    ->where("S.blast_id = ?",$scrId)
                                    ->where("S.from_user_id = ?",$user_id)
                                    ->setIntegrityCheck(false);
                    $scrArray = $db->fetchRow($qry);
                    if(!empty($scrArray['scribble_id'])) {
                        $res[$scrId]['isBlastedScribble'] = $scrArray['scribble_id'];
                    } else {
                        $res[$scrId]['isBlastedScribble'] = '0';
                    }
                    $scribbleIdAry[] = $scribblesArray[$k]['scribble_id'];
            }
            if(count($scribbleIdAry) > 0) {
                    $scrStr = implode(",",$scribbleIdAry);
                    $qry = $this->getDbTable()->select()
                                    ->from(array('BL' => 'tbl_blasts'), array('scribble_id','blast_id'))
                                    ->where("BL.scribble_id IN (".$scrStr.")")
                                    ->where("BL.user_id = '".$user_id."'")
                                    ->setIntegrityCheck(false);
                    $resArray = $db->fetchAll($qry);
                    for($k =0;$k<count($resArray);$k++) {
                            $scrId = $resArray[$k]['scribble_id'];
                            $res[$scrId]['isBlastedScribble'] = $resArray[$k]['blast_id'];
                    }
            }
            if(count($res) > 0) {
                    foreach($res as $val)
                            $result[] = $val;
            }
        } else
            $result = array();
        return $result;
    }

    public function getAtUserNameScribbles($user_id, $user_level, $limit='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
                                    ->from(array('S' => 'tbl_scribbles'))
                                    ->joinLeft(array('U' => 'tbl_users'),"(U.user_id = S.from_user_id)", array('U.user_id','U.username','U.firstname','U.lastname','U.user_level','U.profile_pic_path'))
                                    ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$user_id."' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                                    //->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$user_id."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                                    ->where('S.to_user_id ="'.$user_id.'" AND S.from_user_id !="'.$user_id.'"')
                                    ->order ('S.created_date DESC')
                                    ->where('S.user_level <= "'.$user_level.'"')
                                    ->group('S.scribble_id')
                                    ->limit($limit);
        $scribblesArray = $db->fetchAll($select1);
        $scribbleIdAry = array();
        $res = array();
        $result = array();
        for($k=0;$k<count($scribblesArray);$k++) {
                $scrId = $scribblesArray[$k]['scribble_id'];
                $res[$scrId] = $scribblesArray[$k];
                $qry = $this->getDbTable()->select()
                                ->from(array('S' => 'tbl_scribbles'), array('scribble_id','blast_id','from_user_id'))
                                ->where("S.blast_id = ?",$scrId)
                                ->where("S.from_user_id = ?",$user_id)
                                ->setIntegrityCheck(false);
                $scrArray = $db->fetchRow($qry);
                if(!empty($scrArray['scribble_id'])) {
                    $res[$scrId]['isBlastedScribble'] = $scrArray['scribble_id'];
                } else {
                    $res[$scrId]['isBlastedScribble'] = '0';
                }
                $scribbleIdAry[] = $scribblesArray[$k]['scribble_id'];
        }
        if(count($scribbleIdAry) > 0) {
                $scrStr = implode(",",$scribbleIdAry);
                $qry = $this->getDbTable()->select()
                                ->from(array('BL' => 'tbl_blasts'), array('scribble_id','blast_id'))
                                ->where("BL.scribble_id IN (".$scrStr.")")
                                ->where("BL.user_id = '".$user_id."'")
                                ->setIntegrityCheck(false);
                $resArray = $db->fetchAll($qry);
                for($k =0;$k<count($resArray);$k++) {
                        $scrId = $resArray[$k]['scribble_id'];
                        $res[$scrId]['isBlastedScribble'] = $resArray[$k]['blast_id'];
                }
        }
        if(count($res) > 0) {
                foreach($res as $val)
                        $result[] = $val;
        }
        return $result;
    }

    public function getOriginalOwnerOfScribble($scribble_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
                        ->from(array('S' => 'tbl_scribbles'))
                        ->joinLeft(array('U' => 'tbl_users'),"(U.user_id = S.from_user_id)", array('U.user_id','U.username','U.firstname','U.lastname','user_user_level'=>'U.user_level','U.profile_pic_path', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                         ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                         ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                        ->where('S.scribble_id ="'.$scribble_id.'"')
                        ->order ('S.created_date DESC')
                        ->group('S.scribble_id');
        $scribblesArray = $db->fetchAll($select1);
        return $scribblesArray;
    }

    public function getParentScribbles($userID,$loggedUserId, $user_type, $user_level, $limit = '', $scribbleType='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";

        if($userID == $loggedUserId) {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)','user_level'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."' AND S.from_user_type = 'U' AND S.from_user_id = F.follower_id AND S.from_user_type = F.user_type", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$userID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            //->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$userID."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('(
                                        (S.from_user_id = "'.$userID.'" AND S.from_user_type = "U")
                                        OR S.to_user_id = "'.$userID.'"
                                        OR (S.from_user_id = F.follower_id AND S.from_user_type = F.follower_type)
                                      )
                                      AND S.user_level <= "'.$user_level.'"
                                      AND S.parent_id ="0" AND S.blast_id="0"');
            if($scribbleType == "downline"){
                $select->where('S.user_level != 0');
            }
            $select->group('S.scribble_id')
                            ->order(array("S.created_date DESC"))
                            ->setIntegrityCheck(false);
            $userid = $userID;
        } else {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)','user_level'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'  AND S.from_user_type = 'U' AND S.from_user_id = F.follower_id AND S.from_user_type = F.user_type", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            //->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('
                                        (
                                            S.from_user_id = "'.$userID.'"
                                            OR S.to_user_id = "'.$userID.'"
                                            OR (S.from_user_id = F.follower_id AND S.from_user_type = F.follower_type)
                                        )
                                        AND S.user_level <=  "'.$user_level.'"
                                        AND S.parent_id ="0" AND S.blast_id="0"')
                            ->group('S.scribble_id')
                            ->order("created_date DESC")
                            ->setIntegrityCheck(false);
                $userid = $loggedUserId;
        }
        if($limit != '')
            $select->limit($limit,0);
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57' || $_SERVER['REMOTE_ADDR'] =='10.211.173.8' || $_SERVER['REMOTE_ADDR'] =='182.72.66.214'){
            //echo $select;exit;
        }
        $scribblesArray = $db->fetchAll($select);

        if(count($scribblesArray) == 0) return array();

        $scribbleIdAry = array();
        $res = array();
        $result = array();
        for($k=0;$k<count($scribblesArray);$k++) {
                $scrId = $scribblesArray[$k]['scribble_id'];
                $res[$scrId] = $scribblesArray[$k];
                $qry = $this->getDbTable()->select()
                                ->from(array('S' => 'tbl_scribbles'), array('scribble_id','blast_id','from_user_id'))
                                ->where("S.blast_id = ?",$scrId)
                                ->where("S.from_user_id = ?",$userid)
                                ->setIntegrityCheck(false);
                $scrArray = $db->fetchRow($qry);
                if(!empty($scrArray['scribble_id'])) {
                    $res[$scrId]['isBlastedScribble'] = $scrArray['scribble_id'];
                } else {
                    $res[$scrId]['isBlastedScribble'] = '0';
                }
                $scribbleIdAry[] = $scribblesArray[$k]['scribble_id'];
        }
        if(count($scribbleIdAry) > 0) {
                $scrStr = implode(",",$scribbleIdAry);
                $qry = $this->getDbTable()->select()
                                ->from(array('BL' => 'tbl_blasts'), array('scribble_id','blast_id'))
                                ->where("BL.scribble_id IN (".$scrStr.")")
                                ->where("BL.user_id = '".$userid."'")
                                ->setIntegrityCheck(false);
                $resArray = $db->fetchAll($qry);
                for($k =0;$k<count($resArray);$k++) {
                        $scrId = $resArray[$k]['scribble_id'];
                        $res[$scrId]['isBlastedScribble'] = $resArray[$k]['blast_id'];
                }
        }
        if(count($res) > 0) {
                foreach($res as $val)
                        $result[] = $val;
        }
        return $result;
    }



    public function getUserParentScribbles($userID,$loggedUserId, $user_type, $user_level, $limit = '10', $scribble_id=0, $orderType='') {


        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";

        if($userID == $loggedUserId) { // If logged User Seeing his own Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');
            if($scribble_id != '0' && $orderType != '') {
                if($orderType == "previous") {
                    $select->where("S.scribble_id < '".$scribble_id."'");
                }
                if($orderType == "next") {
                    $select->where("S.scribble_id > '".$scribble_id."'");
                }
            }
            $select->group('S.scribble_id')
                            ->order(array("S.created_date DESC"))
                            ->setIntegrityCheck(false);
			if($orderType == "previous" || $orderType == "") {
				if($limit != '')
					$select->limit($limit,0);
			}
			$scribblesArray = $db->fetchAll($select);
			return $scribblesArray;

        }
		// If logged User Seeing Other User Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            //->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');
             if($scribble_id != '0' && $orderType != '') {
                if($orderType == "previous") {
                    $select->where("S.scribble_id < '".$scribble_id."'");
                }
                if($orderType == "next") {
                    $select->where("S.scribble_id > '".$scribble_id."'");
                }
            }
             $select->group('S.scribble_id')
                            ->order("created_date DESC")
                            ->setIntegrityCheck(false);

        if($orderType == "previous" || $orderType == "") {
            if($limit != '')
                $select->limit($limit,0);
        }
        //echo $select;exit;
        $scribblesArray = $db->fetchAll($select);
		if(count($scribblesArray) == 0) return array();


		$scribbleIdAry = array();
		$res = array();
		$result = array();
		for($k=0;$k<count($scribblesArray);$k++){

			$scrId = $scribblesArray[$k]['scribble_id'];
			$res[$scrId] = $scribblesArray[$k];
			$res[$scrId]['isBlastedScribble'] = '0';
			$scribbleIdAry[] = $scribblesArray[$k]['scribble_id'];
		}
		if(count($scribbleIdAry) > 0){
			$scrStr = implode(",",$scribbleIdAry);
			$qry = $this->getDbTable()->select()
                        ->from(array('BL' => 'tbl_blasts'), array('scribble_id','blast_id'))
                        ->where("BL.scribble_id IN (".$scrStr.")")
						->where("BL.user_id = '".$loggedUserId."'")
                        ->setIntegrityCheck(false);
        	$resArray = $db->fetchAll($qry);
			for($k =0;$k<count($resArray);$k++){
				$scrId = $resArray[$k]['scribble_id'];
				$res[$scrId]['isBlastedScribble'] = $resArray[$k]['blast_id'];
			}


		}
		if(count($res) > 0){
			foreach($res as $key => $val)
				$result[] = $val;
		}
		//if($_SERVER['REMOTE_ADDR'] == '49.207.201.184'){ echo "<pre>"; print_r($result); exit(); }
        return $result;
        //return $scribblesArray;
    }


    public function getUserPostedParentScribbles($userID,$loggedUserId, $user_type, $user_level, $limit = '', $scribble_id=0, $orderType='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";

        if($userID == $loggedUserId) {           // If logged User Seeing his own Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0" AND S.blast_id=0');
            if($scribble_id != '0' && $orderType != '') {
                if($orderType == "previous") {
                    $select->where("S.scribble_id < '".$scribble_id."'");
                }
                if($orderType == "next") {
                    $select->where("S.scribble_id > '".$scribble_id."'");
                }
            }
            $select->group('S.scribble_id')
                            ->order(array("S.created_date DESC"))
                            ->setIntegrityCheck(false);
        } else {            // If logged User Seeing Other User Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"  AND S.blast_id=0');
                                    
                                    

             if($scribble_id != '0' && $orderType != '') {
                if($orderType == "previous") {
                    $select->where("S.scribble_id < '".$scribble_id."'");
                }
                if($orderType == "next") {
                    $select->where("S.scribble_id > '".$scribble_id."'");
                }
            }
            $select->group('S.scribble_id')
                        ->order("created_date DESC")
                        ->setIntegrityCheck(false);
        }
        if($orderType == "previous" || $orderType == "" ) {
            if($limit != '')
                $select->limit($limit,0);
        }
                                   
        $scribblesArray = $db->fetchAll($select);
        return $scribblesArray;
    }

    public function getMoreUserPostedParentScribblesWeb($userID,$loggedUserId, $user_type, $user_level, $limit, $pagenum) {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";

        if($userID == $loggedUserId) {           // If logged User Seeing his own Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0" AND S.blast_id=0');
            $select->group('S.scribble_id')
                            ->order(array("S.created_date DESC"));
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset)
                        ->setIntegrityCheck(false);
        } else {            // If logged User Seeing Other User Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"  AND S.blast_id=0');
            $select->group('S.scribble_id')
                        ->order("created_date DESC");
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset)
                        ->setIntegrityCheck(false);
        }
        $scribblesArray = $db->fetchAll($select);
        return $scribblesArray;
    }

    public function getChildScribblesCntByParentId($parentId, $userID,$loggedUserId, $user_type, $user_level, $limit = '',$scribbleType ='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from(array('S' => 'tbl_scribbles'), array('S.scribble_id'))
                        ->where('S.parent_id ="'.$parentId.'"');
        if($scribbleType == "downline") {
            $select->where('S.user_level != 0');
        }
        if($limit != '')
            $select->limit($limit,0);
        $scribblesArray = $db->fetchAll($select);
        return count($scribblesArray);
    }

    public function getChildScribblesByParentId($parentId, $userID,$loggedUserId, $user_type, $user_level, $limit = '') {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";
            //get user followers. array name: $userFollowers
            //get all replies of the parent scribble including posted user details
            //get replies that were posted by my followers array name: $reliesIds
            //get all records of replyIds from updowns table using FIND_IN_SET
            //get all records of replyIds from blasts table using FIND_IN_SET
            //compare those ids with scribble action table and get status. If you found record, then hide links, if no action found , display links
            //check each reply posted userid with followers array and push replied users(those I am following) into another array name: repliedFollowers
            $loggedUserFollowers = array();
            $followerObj = new User_Model_Followers();
            $loggedUserFollowers = $followerObj->getFollowing($loggedUserId);
            $childScribbleArray = array();
            // Get Child Scribbles by Parent Id
             $select = $this->getDbTable()->select()
                                ->from(array('S' => 'tbl_scribbles'))
    //                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                                ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username','realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                                ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$loggedUserId."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                                ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                                ->where('S.parent_id ="'.$parentId.'" AND S.blast_id = "0"')
                                ->group('S.scribble_id')
                                ->order(array("S.created_date DESC"))
                                ->setIntegrityCheck(false);
            if($limit != '')
                $select->limit($limit,0);
            $childScribbleArray = $db->fetchAll($select);
            $tempScribbleAry = array();
            foreach($childScribbleArray as $key => $child) {
                $tempScribbleAry[] = $child['scribble_id'];
            }
            $childScribbleIds = implode(",", $tempScribbleAry);
            $updownAry = array();
            // Getting if Up/Down's available for the scribble by the logged user
            $upDownSelect = $db->select()
                                ->from(array('UD' => 'tbl_user_up_downs'), array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                                ->where("UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND FIND_IN_SET(UD.scribble_id,'".$childScribbleIds."')")
                                ->order("action_date DESC");
            $updownAry = $db->fetchAll($upDownSelect);
            $tempUpdownAry = array();
            foreach($updownAry as $updown) {
                $tempUpdownAry[$updown['isUpDownScribble']] = $updown['UpDownAction'];
            }
            $blastAry = array();
            // Getting if Blasts available for the scribble by the logged user
            $blastSelect = $db->select()
                                ->from(array('BL' => 'tbl_blasts'), array('blast_id as isBlastedScribble'))
                                ->where("BL.user_id = '".$loggedUserId."' AND BL.scribble_id AND FIND_IN_SET(BL.scribble_id,'".$childScribbleIds."')");
            $blastAry = $db->fetchAll($blastSelect);
            $tempBlastAry = array();
            foreach($blastAry as $blast) {
                $tempBlastAry[] = $blast['isBlastedScribble'];
            }
            $followUserIds = array();
            foreach($loggedUserFollowers as $blast) {
                $followUserIds[] = $blast['follower_id'];
            }
            if(count($childScribbleArray) > 0) {
                    foreach($childScribbleArray as $key => $val) {
                            $childScribbleArray[$key]['isUpDownScribble'] = '';
                            $childScribbleArray[$key]['isBlastedScribble'] = '0';
                            $childScribbleArray[$key]['UpDownAction'] = "";

                            if(in_array($val['from_user_id'],$followUserIds)) {
                                    if(array_key_exists($val['scribble_id'],$tempUpdownAry)) {
                                            if($tempUpdownAry[$val['scribble_id']] == 'down') {
                                                    $childScribbleArray[$key]['isUpDownScribble'] = $val['scribble_id'];
                                                    $childScribbleArray[$key]['isBlastedScribble'] = $val['scribble_id'];
                                                    $childScribbleArray[$key]['UpDownAction'] = "down";
                                            }
                                            if($tempUpdownAry[$val['scribble_id']] == 'up') {
                                                    $childScribbleArray[$key]['isUpDownScribble'] = $val['scribble_id'];
                                                    $childScribbleArray[$key]['UpDownAction'] = "up";
                                            }
                                    }
                                    if(in_array($val['scribble_id'],$tempBlastAry)) {
                                            $childScribbleArray[$key]['UpDownAction'] = "0";
                                            $childScribbleArray[$key]['isUpDownScribble'] = $val['scribble_id'];
                                            $childScribbleArray[$key]['isBlastedScribble'] = $val['scribble_id'];
                                    }
                            } else {
                                    $childScribbleArray[$key]['UpDownAction'] = "0";
                                    $childScribbleArray[$key]['isUpDownScribble'] = $val['scribble_id'];
                                    $childScribbleArray[$key]['isBlastedScribble'] = $val['scribble_id'];
                            }
                    }
            }
//            if($_SERVER['REMOTE_ADDR'] == '192.168.1.63' || $_SERVER['REMOTE_ADDR'] == '182.72.88.157') {
//                //echo $select;
//                //echo "<BR />".$upDownSelect;
//                //echo "<BR />".$blastSelect;
//                //echo "<pre>";print_r($childScribbleArray);exit;
//            }
            return $childScribbleArray;
    }

    public function getMoreDownlineScribbles($user_id, $user_level, $scribble_id, $orderType, $limit='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $user_type ="U";
        // FOr Fetching FOllowers of the User Specified
        $followObj = new User_Model_Followers();
        $usrFollowersArray = array();
        $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id', 'follower_type'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                        ->where('F.user_id = "'.$user_id.'" AND F.user_type ="U" AND F.follower_type = "U"');
        $usrFollowersArray = $db->fetchAll($select);
        $usrFollowersArray[] = array('follower_id' => $user_id,'follower_type' => 'U');
        if(count($usrFollowersArray) > 0) {
            $usrCnt = count($usrFollowersArray);
            $i = 0;
            foreach($usrFollowersArray as $followers) {
                if($i == 0)
                    $ids = "'".$followers['follower_id']."'";
                else
                    $ids = $ids.","."'".$followers['follower_id']."'";
                $i++;
            }
            $select1 = $db->select()
                        ->from(array('S' => 'tbl_scribbles'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','business_name as username'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                        ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$user_id."' AND S.from_user_type = '".$user_type."'", array(''))
                        ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$user_id."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                        ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                        //->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$user_id."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                        ->where('((S.from_user_id = "'.$user_id.'" AND S.from_user_type = "U" AND S.blast_id =0) OR S.to_user_id = "'.$user_id.'" OR S.from_user_id = F.follower_id) AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0" AND S.blast_id = "0"')
                        ->where('U.user_id  IN ('.$ids.')')
                        ->where('S.user_level <= "'.$user_level.'"')
                        ->where('S.user_level != 0');
            if($orderType == "previous") {
                $select1->where("S.scribble_id < '".$scribble_id."'");
            }
            if($orderType == "next") {
                $select1->where("S.scribble_id > '".$scribble_id."'");
            }
            $select1->order('S.created_date DESC')
                    ->group('S.scribble_id');
            if($orderType == "previous" || $orderType == "") {
                $select1->limit($limit);
            }
            //echo $select1;exit;
            $scribblesArray = $db->fetchAll($select1);
        	$scribbleIdAry = array();
            $res = array();
            $result = array();
            for($k=0;$k<count($scribblesArray);$k++) {
                    $scrId = $scribblesArray[$k]['scribble_id'];
                    $res[$scrId] = $scribblesArray[$k];
                    $qry = $this->getDbTable()->select()
                                    ->from(array('S' => 'tbl_scribbles'), array('scribble_id','blast_id','from_user_id'))
                                    ->where("S.blast_id = ?",$scrId)
                                    ->where("S.from_user_id = ?",$user_id)
                                    ->setIntegrityCheck(false);
                    $scrArray = $db->fetchRow($qry);
                    if(!empty($scrArray['scribble_id'])) {
                        $res[$scrId]['isBlastedScribble'] = $scrArray['scribble_id'];
                    } else {
                        $res[$scrId]['isBlastedScribble'] = '0';
                    }
                    $scribbleIdAry[] = $scribblesArray[$k]['scribble_id'];
            }
            if(count($scribbleIdAry) > 0) {
                    $scrStr = implode(",",$scribbleIdAry);
                    $qry = $this->getDbTable()->select()
                                    ->from(array('BL' => 'tbl_blasts'), array('scribble_id','blast_id'))
                                    ->where("BL.scribble_id IN (".$scrStr.")")
                                    ->where("BL.user_id = '".$user_id."'")
                                    ->setIntegrityCheck(false);
                    $resArray = $db->fetchAll($qry);
                    for($k =0;$k<count($resArray);$k++) {
                            $scrId = $resArray[$k]['scribble_id'];
                            $res[$scrId]['isBlastedScribble'] = $resArray[$k]['blast_id'];
                    }
            }
            if(count($res) > 0) {
                    foreach($res as $val)
                            $result[] = $val;
            }
        } else
            $result = array();
        return $result;
    }

    public function getMoreParentScribbles($userID,$loggedUserId, $user_type, $user_level, $scribble_id, $orderType, $latesttype = 'scribbleId',  $limit = '', $scribbleType='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserId) {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','business_name as username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$userID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            //->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$userID."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('
                                    (
                                        (S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.blast_id =0)
                                        OR S.to_user_id = "'.$userID.'"
                                        OR (S.from_user_id = F.follower_id AND F.user_type = "U" AND F.follower_type="U")
                                    )
                                    AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"  AND S.blast_id="0"');
            if($orderType == "previous") {
                if($latesttype == 'scribbleId')
                    $select->where("S.scribble_id < '".$scribble_id."'");
                else
                    $select->where("S.created_date <= '".$scribble_id."'");
            }
            if($orderType == "next") {
                 if($latesttype == 'scribbleId')
                    $select->where("S.scribble_id > '".$scribble_id."'");
                else
                    $select->where("S.created_date > '".$scribble_id."'");
            }
            if($scribbleType == "downline"){
                $select->where('S.user_level != 0');
            }
            $select->group('S.scribble_id')
                    ->order(array("S.created_date DESC"))
                    ->setIntegrityCheck(false);
            $userid = $userID;
        } else {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','business_name as username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            //->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('
                                    (S.from_user_id = "'.$userID.'"
                                        OR S.to_user_id = "'.$userID.'"
                                        OR (S.from_user_id = F.follower_id AND F.user_type = "U" AND F.follower_type="U")
                                    ) AND S.user_level <=  "'.$user_level.'" AND S.parent_id ="0" and S.blast_id="0"');
            if($orderType == "previous") {
                $select->where("S.scribble_id < '".$scribble_id."'");
            }
            if($orderType == "next") {
                $select->where("S.scribble_id > '".$scribble_id."'");
            }
            $select->group('S.scribble_id')
                    ->order("created_date DESC")
                    ->setIntegrityCheck(false);
            $userid = $userID;
        }
        if($orderType == "previous" || $orderType == "") {
            if($limit != '')
                $select->limit($limit);
        }
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57' || $_SERVER['REMOTE_ADDR'] =='10.211.173.8' || $_SERVER['REMOTE_ADDR'] =='182.72.66.214'){
            //echo $select;exit;
        }
        $scribblesArray = $db->fetchAll($select);

		if(count($scribblesArray) == 0) return array();

		$scribbleIdAry = array();
		$res = array();
		$result = array();
		for($k=0;$k<count($scribblesArray);$k++){

			$scrId = $scribblesArray[$k]['scribble_id'];
			$res[$scrId] = $scribblesArray[$k];
			$qry = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'), array('scribble_id','blast_id','from_user_id'))
                            ->where("S.blast_id = ?",$scrId)
                            ->where("S.from_user_id = ?",$userid)
                            ->setIntegrityCheck(false);
            $scrArray = $db->fetchRow($qry);
            if(!empty($scrArray['scribble_id'])) {
                $res[$scrId]['isBlastedScribble'] = $scrArray['scribble_id'];
            } else {
            	$res[$scrId]['isBlastedScribble'] = '0';
            }
			$scribbleIdAry[] = $scribblesArray[$k]['scribble_id'];
		}
		if(count($scribbleIdAry) > 0){
			$scrStr = implode(",",$scribbleIdAry);
			$qry = $this->getDbTable()->select()
                        ->from(array('BL' => 'tbl_blasts'), array('scribble_id','blast_id'))
                        ->where("BL.scribble_id IN (".$scrStr.")")
						->where("BL.user_id = '".$loggedUserId."'")
                        ->setIntegrityCheck(false);
        	$resArray = $db->fetchAll($qry);
			for($k =0;$k<count($resArray);$k++){
				$scrId = $resArray[$k]['scribble_id'];
				$res[$scrId]['isBlastedScribble'] = $resArray[$k]['blast_id'];
			}


		}
		if(count($res) > 0){
			foreach($res as $key => $val)
				$result[] = $val;
		}
		//if($_SERVER['REMOTE_ADDR'] == '49.207.201.184'){ echo "<pre>"; print_r($result); exit(); }
        return $result;



        //return $scribblesArray;
    }

    public function getUserScribbles($userID, $user_level, $user_type='U', $loggedUserID) {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserID) {
                $select = $this->getDbTable()->select()
                                ->from(array('S' => 'tbl_scribbles'))
                                ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                                ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                                ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                                ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                                ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                                ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$userID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                                ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                                ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$userID."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                                ->where('S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id')
                                ->where('S.user_level <=  "'.$user_level.'"')
                                ->group('S.scribble_id')
                                ->setIntegrityCheck(false);
        } else {
                $select = $this->getDbTable()->select()
                                ->from(array('S' => 'tbl_scribbles'))
                                ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username'))
                                ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                                ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                                ->joinLeft(array('FB' => 'tbl_business_users'),"FB.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id as follow_business_row_id','business_name as follow_business_name','image_path as follow_business_image_path','username as follow_business_username'))
                                ->joinLeft(array('FU' => 'tbl_users')," FU.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id as follow_user_row_id','firstname as follow_user_firstname','lastname as follow_user_lastname','profile_pic_path as follow_user_profile_pic_path','username as follow_user_username'))
                                ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                                ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                                ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserID."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                                ->where('S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id')
                                ->where('S.user_level <=  "'.$user_level.'"')
                                ->group('S.scribble_id')
                                ->setIntegrityCheck(false);
        }
        $resultSet = $db->fetchAll($select);
        return $resultSet;
    }

    public function updateDownlinedScribblesLevel($user_id, $user_level) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $updateArray['user_level'] = $user_level;
        $where = " from_user_id = '".$user_id."' AND from_user_type = 'U' AND user_level != 0 AND parent_id ='0'";
        $db->update('tbl_scribbles',$updateArray,$where);
    }

    public function getBlastedUsersByScribbleId($scribeId, $logged_user_id='') {
        $db = Zend_Db_Table::getDefaultAdapter();
//        if($_SERVER['REMOTE_ADDR'] != '192.168.1.63'){
//        $select = $this->getDbTable()->select()
//                    ->from(array('U' => 'tbl_users'), array('user_id','firstname','lastname','profile_pic_path','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)'))
//                    ->joinLeft(array('S' => 'tbl_scribbles')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array(''))
//                    ->where('S.blast_id = ?',$scribeId);
//        $select->order(array('S.created_date DESC'));
//        $select->limit(2)
//                    ->setIntegrityCheck(false);
//        $resArray = $db->fetchAll($select);
//        } else {
                $qry = "SELECT `U`.`user_id`, `U`.`firstname`, `U`.`lastname`, `U`.`profile_pic_path`, `U`.`username`, CONCAT(U.firstname, ' ', U.lastname) AS `realname`, if(U.user_id = '".$logged_user_id."', 1, 0) AS orderflag FROM `tbl_users` AS `U` LEFT JOIN `tbl_scribbles` AS `S` ON U.user_id = S.from_user_id AND S.from_user_type = 'U' WHERE (S.blast_id = '".$scribeId."') ORDER BY orderflag DESC, `S`.`created_date` DESC LIMIT 2";
                $select = $db->query($qry);
        if($_SERVER['REMOTE_ADDR'] == '192.168.1.63') {
            //echo $qry;exit;
        }
        $resArray = $select->fetchAll();
//        echo "<pre>";print_r($resArray);exit;
//        }
        return $resArray;
    }

    public function getBlastedUsersCntByScribbleId($scribeId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                        ->from(array('U' => 'tbl_users'), array('usrCnt'=>'COUNT(user_id)'))
                        ->joinLeft(array('S' => 'tbl_scribbles')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array(''))
                        ->where('S.blast_id = ?',$scribeId)
                        ->setIntegrityCheck(false);
        $resArray = $db->fetchAll($select);
        if(!empty($resArray)) {
            return $resArray[0]['usrCnt'];
        } else return 0;
    }

    public function getAllScribbleBlastedUsers($scribeId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                        ->from(array('U' => 'tbl_users'), array('user_id','firstname','lastname','profile_pic_path','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'user_type'))
                        ->joinLeft(array('S' => 'tbl_scribbles')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('S.scribble_id'))
                        ->where('S.blast_id = ?',$scribeId)
                        ->order('S.created_date DESC')
                        ->setIntegrityCheck(false);
        $resArray = $db->fetchAll($select);
        return $resArray;
    }

    //function to get single scribble information without User Info
    public function getScribleInformationByScribbleId($scribeId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                        ->from(array('S' => 'tbl_scribbles'))
                        ->where('S.scribble_id = ?',$scribeId)
                        ->setIntegrityCheck(false);
        $resArray = $db->fetchAll($select);
        return $resArray[0];
    }

    public function getMoreParentScribblesWeb($userID,$loggedUserId, $user_type, $user_level, $limit, $pagenum, $scribbleType='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserId) {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$userID."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$userID."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('
                                    (
                                        (S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" )
                                        OR S.to_user_id = "'.$userID.'"
                                        OR (S.from_user_id = F.follower_id  AND S.from_user_type = F.follower_type)
                                    )
                                    AND S.user_level <= "'.$user_level.'"
                                    AND S.parent_id ="0"
                                    AND S.blast_id = "0"');
            if($scribbleType == "downline") {
                $select->where('S.user_level != 0');
            }
            $select->group('S.scribble_id')
                            ->order(array("S.created_date DESC"))
                            ->setIntegrityCheck(false);
        } else {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('(S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'" OR S.from_user_id = F.follower_id) AND S.user_level <=  "'.$user_level.'" AND S.parent_id ="0"  AND S.blast_id = "0"')
                            ->group('S.scribble_id')
                            ->order("created_date DESC")
                            ->setIntegrityCheck(false);
        }
        $offset = $pagenum*$limit;
        $select->limit($limit, $offset);
        $scribblesArray = $db->fetchAll($select);
        return $scribblesArray;
    }

    public function getMoreDownlineScribblesWeb($user_id, $user_level,$limit, $pagenum) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $user_type ="U";
        // FOr Fetching FOllowers of the User Specified
        $followObj = new User_Model_Followers();
        $usrFollowersArray = array();
        $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id', 'follower_type'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                        ->where('F.user_id = "'.$user_id.'" AND F.user_type ="U" AND F.follower_type = "U"');
        $usrFollowersArray = $db->fetchAll($select);
        $usrFollowersArray[] = array('follower_id' => $user_id,'follower_type' => 'U');
        if(count($usrFollowersArray) > 0) {
            $usrCnt = count($usrFollowersArray);
            $i = 0;

            foreach($usrFollowersArray as $followers) {
                if($i == 0)
                    $ids = "'".$followers['follower_id']."'";
                else
                    $ids = $ids.","."'".$followers['follower_id']."'";
                $i++;
            }
            $select1 = $db->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','business_name as username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$user_id."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$user_id."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"(BL.user_id = '".$user_id."' AND BL.scribble_id = S.scribble_id)", array('blast_id as isBlastedScribble'))
                            ->where('((S.from_user_id = "'.$user_id.'" AND S.from_user_type = "U" AND S.blast_id =0) OR S.to_user_id = "'.$user_id.'" OR S.from_user_id = F.follower_id) AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0" AND S.blast_id = 0')
                            ->where('U.user_id  IN ('.$ids.') OR U.user_id = "'.$user_id.'"')
                            ->where('S.user_level <= "'.$user_level.'"')
                            ->where('S.user_level != 0')
                            ->order ('S.created_date DESC')
                            ->group('S.scribble_id');
            $offset = $pagenum * $limit;
            $select1->limit($limit,$offset);
            //echo $select1; exit();
             $scribblesArray = $db->fetchAll($select1);
        } else
            $scribblesArray = array();
        return $scribblesArray;
    }

    public function getMoreUserParentScribbles($userID,$loggedUserId, $user_type, $user_level, $limit = '', $pagenum) {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";

        if($userID == $loggedUserId) {           // If logged User Seeing his own Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');

            $select->group('S.scribble_id')
                        ->order(array("S.created_date DESC"));
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset)
                        ->setIntegrityCheck(false);
        } else {            // If logged User Seeing Other User Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');

            $select->group('S.scribble_id')
                            ->order("created_date DESC");
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset)
                        ->setIntegrityCheck(false);
        }
        $scribblesArray = $db->fetchAll($select);
        //echo "<pre>";print_r($scribblesArray);exit;
        return $scribblesArray;
    }

    public function getAllScribbleBlastedUsersWeb($scribeId){
        $db = Zend_Db_Table::getDefaultAdapter();
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserID = $logged_user_data->user_id;
        }else
            $loggedUserID = 0;
        $select = $this->getDbTable()->select()
                    ->from(array('U' => 'tbl_users'), array('user_id','firstname','lastname','profile_pic_path','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'user_type','orderflag'=>'if(U.user_id = "'.$loggedUserID.'", 1, 0)'))
                    ->joinLeft(array('S' => 'tbl_scribbles')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('S.scribble_id'))
                    ->where('S.blast_id = ?',$scribeId)
                    ->order(array('orderflag DESC','S.created_date DESC'))
                    ->setIntegrityCheck(false)
                    ->limit(18446744073709551615,2);

        /*
         * SELECT `U`.`user_id`, `U`.`firstname`, `U`.`lastname`, `U`.`profile_pic_path`, `U`.`username`, CONCAT(U.firstname, " ", U.lastname) AS `realname`, if(U.user_id = "1", 1, 0) AS `orderflag`, `S`.`scribble_id` FROM `tbl_users` AS `U` LEFT JOIN `tbl_scribbles` AS `S` ON U.user_id = S.from_user_id AND S.from_user_type = 'U' WHERE (S.blast_id = '730') ORDER BY `orderflag` DESC LIMIT 2147483647 OFFSET 2
         *

           SELECT `U`.`user_id`, `U`.`firstname`, `U`.`lastname`, `U`.`profile_pic_path`, `U`.`username`, CONCAT(U.firstname, ' ', U.lastname) AS `realname`, if(U.user_id = '1', 1, 0) AS orderflag FROM `tbl_users` AS `U` LEFT JOIN `tbl_scribbles` AS `S` ON U.user_id = S.from_user_id AND S.from_user_type = 'U' WHERE (S.blast_id = '730') ORDER BY orderflag DESC, `S`.`created_date` DESC LIMIT 2
         */

        /*
         * $qry = "SELECT `U`.`user_id`, `U`.`firstname`, `U`.`lastname`, `U`.`profile_pic_path`, `U`.`username`, CONCAT(U.firstname, ' ', U.lastname) AS `realname`, if(U.user_id = '".$logged_user_id."', 1, 0) AS orderflag FROM `tbl_users` AS `U` LEFT JOIN `tbl_scribbles` AS `S` ON U.user_id = S.from_user_id AND S.from_user_type = 'U' WHERE (S.blast_id = '".$scribeId."') ORDER BY orderflag DESC, `S`.`created_date` DESC LIMIT 2";
         *
         */
        //echo $select;exit;
        $resArray = $db->fetchAll($select);
        return $resArray;
    }

    public function getUserPostedAndBlastedParentScribbles($userID,$loggedUserId, $user_type, $user_level, $limit = '', $scribble_id=0, $orderType='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";

        if($userID == $loggedUserId) {           // If logged User Seeing his own Profile


            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');
            if($scribble_id != '0' && $orderType != '') {
                if($orderType == "previous") {
                    $select->where("S.scribble_id < '".$scribble_id."'");
                }
                if($orderType == "next") {
                    $select->where("S.scribble_id > '".$scribble_id."'");
                }
            }
            $select->group('S.scribble_id')
                            ->order(array("S.created_date DESC"))
                            ->setIntegrityCheck(false);


        } else {            // If logged User Seeing Other User Profile


            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');

             if($scribble_id != '0' && $orderType != '') {
                if($orderType == "previous") {
                    $select->where("S.scribble_id < '".$scribble_id."'");
                }
                if($orderType == "next") {
                    $select->where("S.scribble_id > '".$scribble_id."'");
                }
            }
            $select->group('S.scribble_id')
                        ->order("created_date DESC")
                        ->setIntegrityCheck(false);
        }
        if($orderType == "previous" || $orderType == "" ) {
            if($limit != '')
                $select->limit($limit,0);
        }
         if($_SERVER['REMOTE_ADDR'] == '192.168.1.57' || $_SERVER['REMOTE_ADDR'] == '182.72.66.214'){
            //echo $select;exit;
        }
        $scribblesArray = $db->fetchAll($select);
        return $scribblesArray;
    }

    public function getMoreUserPostedAndBlastedParentScribblesWeb($userID,$loggedUserId, $user_type, $user_level, $limit, $pagenum) {
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";

        if($userID == $loggedUserId) {           // If logged User Seeing his own Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');
            $select->group('S.scribble_id')
                            ->order(array("S.created_date DESC"));
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset)
                        ->setIntegrityCheck(false);
        } else {            // If logged User Seeing Other User Profile
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_scribbles'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path', 'username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array(''))
                            ->joinLeft(array('UD' => 'tbl_user_up_downs'),"UD.user_id = '".$loggedUserId."' AND UD.user_type = 'U' AND S.scribble_id = UD.scribble_id", array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints'),"S.from_user_id = CP.user_id", array('cool_points'))
                            ->joinLeft(array('BL' => 'tbl_blasts'),"BL.user_id = '".$loggedUserId."' AND BL.scribble_id = S.scribble_id", array('blast_id as isBlastedScribble'))
                            ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                            ->where('S.from_user_id = "'.$userID.'" AND S.from_user_type = "U" AND S.user_level <= "'.$user_level.'" AND S.parent_id ="0"');
            $select->group('S.scribble_id')
                        ->order("created_date DESC");
            $offset = $pagenum * $limit;
            $select->limit($limit,$offset)
                        ->setIntegrityCheck(false);
        }
        $scribblesArray = $db->fetchAll($select);

        if($_SERVER['REMOTE_ADDR'] == '192.168.1.57' || $_SERVER['REMOTE_ADDR'] == '182.72.66.214'){
            //echo "<br>".$select;//exit;
            //echo "<pre>";print_r($scribblesArray);//exit;
        }
        return $scribblesArray;
    }

    public function getUpDownActionByUserId($scribbleId,$usrId){
        //echo $scribbleId;
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from(array('UD' => 'tbl_user_up_downs'), array('scribble_id as isUpDownScribble','action_type as UpDownAction'))
                        ->where("UD.user_id = '".$usrId."' AND UD.user_type = 'U' AND UD.scribble_id = '".$scribbleId."'");
        //echo $select."<br>";
        $resArray = $db->fetchAll($select);
        //echo "<pre>";print_r($resArray);exit;
        return $resArray;
    }

    public function getBlastActionByUserId($scribble_id,$userID){
        //echo $scribbleId;
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from(array('BL' => 'tbl_blasts'), array('blast_id as isBlastedScribble'))
                        ->where("BL.user_id = '".$userID."' AND BL.blast_id = '".$scribble_id."'");
                        //->where("BL.user_id = '".$userID."' AND BL.scribble_id = '".$scribble_id."'");
        //echo $select."<br>";
        $resArray = $db->fetchAll($select);
        //echo "<pre>";print_r($resArray);exit;
        return $resArray;
    }
}
