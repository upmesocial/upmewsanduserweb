<?php
class Huddles_Model_Huddles {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Huddles_Model_DbTable_Huddles');
                }
                return $this->_dbTable;
        }

        public function createRoom($data) {
                return $this->getDbTable()->insert($data);
        }

        public function joinRoom($data, $roomId) {
                if(!empty($roomId)){
                        try{
                                $res = $this->getDbTable()->update($data, array('room_id = ?' => $roomId));
                                if (false === $res) {
                                    return 0;
                                } else {
                                    return 1;
                                }
                        } catch (Zend_Exception $zex){}
                }
        }

        public function getRoomInfo($rid) {
                $select = $this->getDbTable()->select()
                                ->where('room_id = ?',$rid)
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchRow($select);
                if(count($resultSet) == 0) {
                        return $resultSet;
                }
                $roomInfo = $resultSet->toArray();
                $db = Zend_Db_Table::getDefaultAdapter();
                $memberInfo = array();
                //get target info
                if(trim($resultSet['target_id']) != ' ' &&trim($resultSet['target_id']) != '' && $resultSet['target_id'] != 0 && $resultSet['target_id'] != null){
                        $targetExplode = explode(',',$resultSet['target_id']);
                        $targetExplode = array_filter($targetExplode);
                        $targetUsers = implode(",",$targetExplode);
                        //get target users info
                        $select = $db->select()
                                        ->from('tbl_users',array('user_id','username','gender', 'realname' => 'CONCAT(firstname, " ", lastname)','firstname','lastname','profile_pic_path'))
                                        ->where('user_id IN ('.$targetUsers.')');
                        $result = $db->fetchAll($select);
                        for($k = 0; $k < count($result); $k++) {
                                $res = $result[$k];
                                $res['type'] = 'Target';
                                $res['isKicked'] = '0';
                                $memberInfo[] = $res;
                        }
                }
                //get owner info
                $select = $db->select()
                                ->from('tbl_users as U',array('user_id','username', 'gender','realname' => 'CONCAT(firstname, " ", lastname)','firstname','lastname','profile_pic_path'))
                                ->joinLeft('tbl_user_levels as L','L.id=U.user_level',array('level_name'))
                                ->where('user_id = ? ',$resultSet['owner_id']);
		//echo $select;exit;
                $result = $db->fetchRow($select);
                $result['type'] = 'Owner';
                $memberInfo[] = $result;

                //get participants info
                if(trim($resultSet['members_id']) != ' ' &&trim($resultSet['members_id']) != '' && $resultSet['members_id'] != 0 && $resultSet['members_id'] != null){
                        $targetExplode = explode(',',$resultSet['members_id']);
                        $targetExplode = array_filter($targetExplode);
                        $targetUsers = implode(",",$targetExplode);
                        $select = $db->select()
                                        ->from('tbl_users',array('user_id','username','gender', 'realname' => 'CONCAT(firstname, " ", lastname)','firstname','lastname','profile_pic_path'))
                                        ->where('user_id IN ('.$targetUsers.')');
                        $result = $db->fetchAll($select);
                        for($k = 0; $k < count($result); $k++) {
                                $res = $result[$k];
                                $res['type'] = 'Member';
                                $res['isKicked'] = '0';
                                $memberInfo[] = $res;
                        }
                }
                //get participants info
                if(trim($resultSet['kickedusers_id']) != ' ' &&trim($resultSet['kickedusers_id']) != '' && $resultSet['kickedusers_id'] != 0 && $resultSet['kickedusers_id'] != null){
                        $targetExplode = explode(',',$resultSet['kickedusers_id']);
                        $targetExplode = array_filter($targetExplode);
                        $targetUsers = implode(",",$targetExplode);
                        $select = $db->select()
                                        ->from('tbl_users',array('user_id','username','gender', 'realname' => 'CONCAT(firstname, " ", lastname)','firstname','lastname','profile_pic_path'))
                                        ->where('user_id IN ('.$targetUsers.')');
                        $result = $db->fetchAll($select);
                        for($k = 0; $k < count($result); $k++) {
                                $res = $result[$k];
                                $res['type'] = 'Member';
                                $res['isKicked'] = '1';
                                $memberInfo[] = $res;
                        }
                }

                     //Added not member in group field 
                        $followObj = new User_Model_Followers();
                        $notInIds = "".$resultSet['members_id'].",".$resultSet['target_id'].",".$resultSet['kickedusers_id'];

                        $notInFollowerIds = explode(',',$notInIds);
                        $notInFollowerIds = array_filter($notInFollowerIds);
                        $notInFollowerIdsUsers = implode(",",$notInFollowerIds);
                        $limit = 300;
                        $getNotMemersIds = $followObj->getFollowersByUserID($resultSet['owner_id'],"U", $limit, $notInFollowerIdsUsers);

                        $ids = "";
                        foreach ($getNotMemersIds as $key => $value) {

                            if($ids == "")
                            $ids.="".$value['user_id'];
                            else
                            $ids.=",".$value['user_id'];
                        }
                        $roomInfo['notmembers_id'] = $ids;



                $roomInfo['user_detail'] = $memberInfo;
                //echo '<pre>';print_r($roomInfo);exit;
                return $roomInfo;
        }

        //function to get all rooms of a user
        public function getAllUserRooms($userId,$usertype='',$roomType='') {

                $limit = 300;
                $page = 0;
                $type = "U";
                $followObj = new User_Model_Followers();
                $getAllFollowingIds = $followObj->getMyFollowing($userId,$limit,$page,$type);

                        $ids = "";
                        foreach ($getAllFollowingIds as $key => $value) {

                            if($ids == "")
                            $ids.="".$value['follower_id'];
                            else
                            $ids.=",".$value['follower_id'];
                        }


                $db = Zend_Db_Table::getDefaultAdapter();
                if ($usertype == 'owner') {
                        $select = $this->getDbTable()->select()
                                                    ->where('owner_id = ?',$userId)
                                                    ->where('open_status =1')
                                                    ->order(array('created_date DESC'))
                                                    ->setIntegrityCheck(false);
                } else if ($usertype == 'member' && $roomType == 'private') {
                        $select = $this->getDbTable()->select()
                                                    ->where('(FIND_IN_SET('.$userId.',members_id) <> 0 ) OR (FIND_IN_SET('.$userId.',kickedusers_id) <> 0 )')
                                                    ->where('(FIND_IN_SET('.$userId.',target_id) = 0 )')
                                                    ->where('(FIND_IN_SET('.$userId.',decline_id) = 0 )')
                                                    ->where('room_type = ?',$roomType)
                                                    ->where('open_status =1')
                                                    ->order(array('created_date DESC'))
                                                    ->setIntegrityCheck(false);
                }else if ($usertype == 'member' && $roomType == 'public') {
                        $select = $this->getDbTable()->select()
                                                    ->where('room_type = ?',$roomType)
                                                    ->where('(FIND_IN_SET('.$userId.',target_id) = 0 )')
                                                    ->where('(FIND_IN_SET('.$userId.',decline_id) = 0 )')
                                                    ->where('owner_id IN ('.$ids.')')
                                                    ->where('open_status =1')
                                                    ->order(array('created_date DESC'))
                                                    ->setIntegrityCheck(false);
                }
                 else if ($usertype == 'target') {
                        $select = $this->getDbTable()->select()
                                                    ->where('(FIND_IN_SET('.$userId.',target_id) <> 0 )')
                                                    ->where('open_status =1')
                                                    ->order(array('created_date DESC'))
                                                    ->setIntegrityCheck(false);
                } else {
                        $select = $this->getDbTable()->select()
                                                    ->where('owner_id = '.$userId.' OR (FIND_IN_SET('.$userId.',members_id) <> 0 ) OR (FIND_IN_SET('.$userId.',target_id) <> 0)')
                                                    ->where('open_status =1')
                                                    ->order(array('created_date DESC'))
                                                    ->setIntegrityCheck(false);
                }
                $resultSet = $db->fetchAll($select);
                //echo '<pre>';print_r($resultSet);//exit;


                if(count($resultSet) == 0) {
                        return $resultSet;
                }
                $result = array();
                foreach($resultSet as $val) {
                        $result[] = $this->getRoomInfo($val['room_id']);
                }
                return $result;
        }

        //function to get all rooms of a user
        public function getAllUserRoomsOnRoomType($userId,$usertype='',$pagenum='0',$limit='20') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $offset = $pagenum * $limit;
                if ($usertype == 'owner') {
                        $select = $this->getDbTable()->select()
                                                    ->where('owner_id = ?',$userId)
                                                    ->where("open_status = '1'")
                                                    ->order(array('created_date DESC'))
                                                    ->limit($limit, $offset)
                                                    ->setIntegrityCheck(false);
                } else if ($usertype == 'member') {
                        $select = $this->getDbTable()->select()
                                                    ->where('(FIND_IN_SET('.$userId.',members_id) <> 0 )')
                                                    ->where("open_status = '1'")
                                                    ->order(array('created_date DESC'))
                                                    ->limit($limit, $offset)
                                                    ->setIntegrityCheck(false);
                } else if ($usertype == 'target') {
                        $select = $this->getDbTable()->select()
                                                    ->where('(FIND_IN_SET('.$userId.',target_id) <> 0 )')
                                                    ->where("open_status = '1'")
                                                    ->order(array('created_date DESC'))
                                                    ->limit($limit, $offset)
                                                    ->setIntegrityCheck(false);
                } else {
                        $select = $this->getDbTable()->select()
                                                    ->where('owner_id = '.$userId.' OR (FIND_IN_SET('.$userId.',members_id) <> 0 ) OR (FIND_IN_SET('.$userId.',target_id) <> 0)')
                                                    ->where("open_status = '1'")
                                                    ->order(array('created_date DESC'))
                                                    ->limit($limit, $offset)
                                                    ->setIntegrityCheck(false);
                }
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                $followObj = new User_Model_Followers();
                /*if(count($resultSet) == 0) {
                        return $resultSet;
                }*/
                $i=0;
                $resultAry = array();
                foreach($resultSet as $i => $val) {
                        $resultAry[$i] = $this->getRoomInfo($val['room_id']);
                }
                $count = count($resultAry);
                if($usertype == 'member') {
                        $followersIds = $followObj->getFollowerIds($userId);
                        foreach($followersIds as $followersId) {
                                $existFollowersAry[] = $followersId['user_id'];
                                $select = $db->select()
                                                ->from('huddle_user_chat',array('room_id','members_id','owner_id','target_id','kickedusers_id'))
                                                ->where("owner_id = ?",$followersId['user_id'])
                                                ->where("open_status = '1'")
                                                ->where("room_type = ?",'public')
                                                ->limit($limit, $offset);
                                $results = $db->fetchAll($select);
                                foreach($results as $j => $result):
                                    $existUsersAry = explode(',',$result['members_id']);
                                    $existKickedUsersAry = explode(',',$result['kickedusers_id']);
                                    $existTargetUsersAry = explode(',',$result['target_id']);
                                    if((!in_array($userId,$existUsersAry)) && ($userId != $result['owner_id']) && (!in_array($userId,$existKickedUsersAry)) && (!in_array($userId,$existTargetUsersAry))) {
                                        $resultAry[$i+$j] = $this->getRoomInfo($result['room_id']);
                                    }
                                endforeach;
                        }
                }
                return $resultAry;
        }

        //function to get all information of a room,users and chat log from openfire server
        public function getRoomChatLogOF($roomId) {
                if($roomId == 0 || $roomId == '') return;
                $roomInfo = $this->getRoomInfo($roomId);
                //get room chat log
                $db2 = Zend_Registry::get('db2');
                $select = $db2->select()
                                ->from('ofOffline',array('username','stanza'))
                                ->where(' stanza LIKE "%<subject>'.$roomId.'</subject>%"');
                                //->setIntegrityCheck(false);
                $history = $db2->fetchAll($select);
                $hisCnt = count($history);
                for($k = 0; $k < $hisCnt; $k++) {
                        preg_match_all('/<body.*>(.*)<\/body>/Uism',$history[$k]['stanza'],$match);
                        $history[$k]['stanza'] = $match[1][0];
                }
                $result = array(
                                'RoomInfo' => $roomInfo,
                                'Conversation' => $history
                        );
                return $result;
        }

        //function to get all rooms information of a user and chat log
        public function getRoomsChatLogOfUser($userId) {
                if($userId == 0 || $userId == '') return;
                $select = $this->getDbTable()->select()
                                            ->where('owner_id = '.$userId.' OR (FIND_IN_SET('.$userId.',members_id) <> 0 ) OR (FIND_IN_SET('.$userId.',target_id) <> 0)')
                                            ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                if(count($resultSet) == 0) {
                        return $resultSet;
                }
                $result = array();
                foreach($resultSet as $key => $val) {
                        $result[] = $this->getRoomChatLog($val['room_id']);
                }
                return $result;
        }

        //function to insert data into chat logs
        public function insertChatLog($data) {
                $db = Zend_Db_Table::getDefaultAdapter();
                //check user is there in member list or not
                $select = $db->select()
                ->from('huddle_user_chat',array('members_id','owner_id','target_id','kickedusers_id'))
                ->where("room_id = ?",$data['room_id']);
                $resultSet = $db->fetchRow($select);
                $existUsersAry = explode(',',$resultSet['members_id']);
                $existKickedUsersAry = explode(',',$resultSet['kickedusers_id']);
                $existTargetUsersAry = explode(',',$resultSet['target_id']);
                if(in_array($data['user_id'],$existUsersAry) || $data['user_id'] == $resultSet['owner_id']) {
                	$ins = $db->insert('huddle_user_chat_logs',$data);
                	return $ins;
                }
                if((!in_array($data['user_id'],$existUsersAry)) && ($data['user_id'] != $resultSet['owner_id']) && (!in_array($data['user_id'],$existKickedUsersAry)) && (!in_array($data['user_id'],$existTargetUsersAry))) {
                        $select = $db->select()
                                    ->from(array('U' => 'tbl_users'),array('user_id','firstname','lastname', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)','profile_pic_path','username'))
                                    ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('level_name as user_level_name'))
                                    ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'))
                                    ->where("U.user_id = ?",$data['user_id']);
                        $resultSet = $db->fetchRow($select);
                        if(!empty($existUsersAry)) {
                            $existUsersAry[] .= $data['user_id'];
                            $updAry['members_id'] = @implode(",",$existUsersAry);
                        } else {
                            $updAry['members_id'] = $data['user_id'];
                        }
                        $db->update('huddle_user_chat', $updAry, 'room_id = "'.$data['room_id'].'"');
                        return $resultSet;
                }
                return 'err';
        }

        //function to get all information of a room,users and chat log from upme database
        public function getRoomChatLog($roomId) {
                if($roomId == 0 || $roomId == '') return;
                $roomInfo = $this->getRoomInfo($roomId);
                //get room chat log
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from('huddle_user_chat_logs as H',array('user_id','message'))
                            ->joinLeft('tbl_users as U','U.user_id = H.user_id',array('realname' =>'CONCAT(firstname," ",lastname)','username','gender','profile_pic_path'))
                            ->where(' room_id = ?',$roomId);
                            //->setIntegrityCheck(false);
                $history = $db->fetchAll($select);
                $result = array(
                                'RoomInfo' => $roomInfo,
                                'Conversation' => $history
                        );
                return $result;
        }

        //function to close chat rooms after 24hrs
        public function closeChatRooms() {
                $currTime = date("Y-m-d H:i:s"); //exit();
                $updAry = array('open_status' => 0);
                $resultSet = $this->getDbTable()->update($updAry, 'created_date <= DATE_SUB("'.$currTime.'", INTERVAL 24 HOUR)');
        }

        //function to get single hudddle information By huddle Date
        public function getHuddleDetailsByHuddleID($date, $type) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select();
                if($type == 'previous') {
                        $select = $select->where('created_date >"'.$date.'"');
                }
                if($type == 'next') {
                        $select = $select->where('created_date<"'.$date.'"');
                }
                $select = $select->setIntegrityCheck(false);
                $resArray = $db->fetchOne($select);
                return $resArray;
        }

        //function to get targeted rooms count by userid
        public function getTagetedRoomsCnt($userId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $qry = 'SELECT COUNT(room_id) AS count 
                                FROM huddle_user_chat
                                WHERE FIND_IN_SET('.$userId.',target_id) <> 0 
                                AND open_status = 1
                                ';
                $cnt = $db->fetchOne( $qry );
                return $cnt;
        }

        //function to get users count who targeted
        public function getUsersTargetedCnt($userId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                //get rooms tageted this user
                //by repeating for loop, count users in members_id and owner_id
                $select = $this->getDbTable()->select()
                                ->where('FIND_IN_SET('.$userId.',target_id) <> 0')
                                ->where('open_status = ?', 1)
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                $cnt = 0;
                for($k = 0; $k < count($resultSet); $k++){
                        if($resultSet[$k]['owner_id'] != 0 && $resultSet[$k]['owner_id'] != '')
                                $cnt++;
                        if($resultSet[$k]['members_id'] != 0 && $resultSet[$k]['members_id'] != '') {
                                $memAry = explode(",",$resultSet[$k]['members_id']);
                                $memAry = array_filter($memAry);
                                $cnt+= count($memAry);
                        }
                }
                return $cnt;
        }

        public function getchatcnt($rid) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C' => 'huddle_user_chat_logs'), 'COUNT(C.log_id)')
                                ->where('C.room_id =?',$rid);
                $cnt = (int) $db->fetchOne($select);
                return $cnt;
        }

        public function getHuddleDetailsByRoomPhotoPath($room_photo_path) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('P'=>'huddle_user_chat'),array('COUNT(room_id) as count'))
                                ->where("P.room_photo_path =?",$room_photo_path);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function updatehuddlewidthnheight($data, $room_photo_path) {
                $db = Zend_Db_Table::getDefaultAdapter();
                return  $db->update('huddle_user_chat', $data, 'room_photo_path = "'.$room_photo_path.'"');
        }
		
        //function to insert new users to huddle room
        public function addUsersToRoom($userIds,$roomId) {
                $zex = '';
                $db = Zend_Db_Table::getDefaultAdapter();
                $userIdsAry = explode(",",$userIds);
                //select existing user
                $select = $db->select()
                                ->from('huddle_user_chat',array('members_id'))
                                ->where("room_id = ?",$roomId);
                $resultSet = $db->fetchRow($select);
                $existUsersAry = explode(',',$resultSet['members_id']);
                $newAry = array_unique(array_merge($existUsersAry,$userIdsAry));
                $updAry = array('members_id' => implode(',',$newAry));
                try {
                        $res = $this->getDbTable()->update($updAry, array('room_id= ?' => $roomId));
                        if (false === $res) {
                                return 0;  // bool false returned, query failed
                        } else {
                                //update records in kicked users column
                                $select = $db->select()
                                                ->from('huddle_user_chat',array('kickedusers_id'))
                                                ->where("room_id = ?",$roomId);
                                $resultSet = $db->fetchRow($select);
                                $existUsersAry = explode(',',$resultSet['kickedusers_id']);
                                $newAry = array_diff($existUsersAry,$userIdsAry);
                                $updAry = array('kickedusers_id' => implode(',',$newAry));
                                $kicked = '';
                                $kicked = $this->getDbTable()->update($updAry, array('room_id= ?' => $roomId));
                                return 1;
                        }
                } catch (Zend_Exception $zex){}
        }

        //function to remove users from huddle room
        public function removeUsersFromRoom($userIds,$roomId) {
                $zex = '';
                $db = Zend_Db_Table::getDefaultAdapter();
                $userIdsAry = explode(",",$userIds);
                //select existing user
                $select = $db->select()
                                ->from('huddle_user_chat',array('members_id'))
                                ->where("room_id = ?",$roomId);
                $resultSet = $db->fetchRow($select);
                $existUsersAry = explode(',',$resultSet['members_id']);
                $newAry = array_diff($existUsersAry,$userIdsAry);
                if(count($newAry) == 0)
                        return '2';
                $updAry = array('members_id' => implode(',',$newAry));
                try {
                        $res = $this->getDbTable()->update($updAry, array('room_id= ?' => $roomId));
                        if (false === $res) {
                                return 0;  // bool false returned, query failed
                        } else {
                                //update records in kicked users column
                                $select = $db->select()
                                                ->from('huddle_user_chat',array('kickedusers_id'))
                                                ->where("room_id = ?",$roomId);
                                $resultSet = $db->fetchRow($select);
                                $existUsersAry = explode(',',$resultSet['kickedusers_id']);
                                if(count($existUsersAry) > 0 && !empty($existUsersAry[0]))
                                        $newAry = array_unique(array_merge($existUsersAry,$userIdsAry));
                                else
                                        $newAry = $userIdsAry;
                                $updAry = array('kickedusers_id' => implode(',',$newAry));
                                $kicked = '';
                                $kicked = $this->getDbTable()->update($updAry, array('room_id= ?' => $roomId));
                                return 1;
                        }
                } catch (Zend_Exception $zex){}
        }

}