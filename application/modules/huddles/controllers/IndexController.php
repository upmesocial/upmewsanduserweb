<?php
class Huddles_IndexController extends Zend_Controller_Action {

        public function init() {
            /* Initialize action controller here */
            if(Zend_Registry::isRegistered('userdata')) {
                $logged_user_data = Zend_Registry::get('userdata');
                $loggedUserId = $logged_user_data->user_id;
                $user_account_status = $logged_user_data->user_account_status;
                $loggedUserType = 'U';
            }
            if(Zend_Registry::isRegistered('businessdata')) {
                $logged_user_data = Zend_Registry::get('businessdata');
                $loggedUserId = $logged_user_data->business_id;
                $user_account_status = $logged_user_data->status;
                $loggedUserType = 'B';
            }
            if(!empty($logged_user_data)) {
                $limit= 30;
                $photosObj = new Mobile_Model_Photos();
                $followersgallery = $photosObj->getTimelineClickedImagesVideosDetails($loggedUserId,$loggedUserType, $limit);
                //echo "<pre>";print_r($followersgallery);exit;
                //echo count($followersgallery);exit;
                $this->view->assign('followersgallery', $followersgallery);
            }
        }

        public function indexAction() {
                // action body
        }

        public function createroomAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
            
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $insAry = array();
                $name = trim($request->getParam('name'));
                if($name == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Room Name should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $insAry['room_name'] = $name;

                $subject = trim($request->getParam('subject'));
                if($subject == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Subject should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $insAry['room_subject'] = $subject;

                $description = trim($request->getParam('description'));
                if($description == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Description should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $insAry['room_description'] = $description;

                $owner_id = trim($request->getParam('owner_id'));
                if($owner_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Owner Id should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $insAry['owner_id'] = $owner_id;

                $members_id = trim($request->getParam('members_id'));
                if($members_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Members Id should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $insAry['members_id'] = $members_id;

                $room_type = trim($request->getParam('room_type'));
                if($room_type == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Room Type should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
            
                if($room_type != 'private' && $room_type != 'public') {
                        $resultAry = array('service_status'=>'error',"content" => 'Invalid Room Type','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                
                $insAry['room_type'] = $room_type;

                $keywords = $request->keywords;
                /*if($keywords == '') {
                        echo $myjson->customEncode(array('service_status'=>'error',"error_msg" =>  "Keyword can't be Empty"));
                        return;
                }*/

                $target_id = trim($request->getParam('target_id'));
                /*if($target_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Target Id should not be empty','responsecode'=>'480');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry);
                        return;
                }*/
                $insAry['target_id'] = $target_id;

                //$photo_path =trim($data['photo_path']);
                if(!empty($_FILES['image']['name'])) {
                        $dir_upload = UPLOAD_PATH.'images/original/';
                        $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                        $medium_path = UPLOAD_PATH.'images/medium/';
                        $mobile_path = UPLOAD_PATH.'images/mobile/';//$resAry['reference_id'] = $ins;
                        $android_path = UPLOAD_PATH.'images/android/';
                        $huddle_path = UPLOAD_PATH.'images/huddle/';

                        $image = new UpmeSocial_Controller_Action_Helper_Image();
                        $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path,$huddle_path);

                         // Upload images at Amazone S3 server
                        $uploadfiles = new Mobile_Model_Uploadfiles();
                        $resp = $uploadfiles->uploadImagesAtS3Server($arrFileName['image'],$owner_id);
                                                
                        //Saving data based on phototype
                        $insAry['room_photo_path'] = IMAGES_PATH.$owner_id."/".$arrFileName['image'];
                } else {
                    $insAry['room_photo_path'] = '';
                    $arrFileName['width'] = 0;
                    $arrFileName['height'] = 0;
                    $arrFileName['android_width'] = 0;
                    $arrFileName['android_height'] = 0;
                }
                /*$width = trim($request->getParam('width'));*/
                $insAry['width'] = $arrFileName['width'];
                /*$height = trim($request->getParam('height'));*/
                $insAry['height'] = $arrFileName['height'];
                $insAry['android_width'] = $arrFileName['android_width'];
                $insAry['android_height'] = $arrFileName['android_height'];
                $insAry['created_date'] = date("Y-m-d H:i:s");
                $huddleObj = new Huddles_Model_Huddles();
                $insId = $huddleObj->createRoom($insAry);

                //Notification for huddle.
                $notificationObj = new Notifications_Model_Notifications();
                
                if($insId) {
                        // FOR NOTIFICATIONS for Targeting a person
                        if($target_id != '') {
                            $targetersAry  = explode(",",$target_id);
                            foreach($targetersAry as $target){
                                $target_user_type = "U";
                                $resAry['user_id'] = $target;
                                $resAry['user_type'] = $target_user_type;
                                $resAry['reference_id'] = $insId;
                                $resAry['notifications_type'] = 'huddle_target';
                                $resAry['action_date'] = date('Y-m-d H:i:s');
                                $ins = $notificationObj->insertNewRecord($resAry);
                            }
                        }

                        // FOR NOTIFICATIONS for Huddle a members
                        if($room_type == "private")
                        {
                            if($members_id != '') {
                                $membersAry  = explode(",",$members_id);
                                foreach($membersAry as $member){
                                    $member_user_type = "U";
                                    $res1Ary['user_id'] = $member;
                                    $res1Ary['user_type'] = $member_user_type;
                                    $res1Ary['reference_id'] = $insId;
                                    $res1Ary['notifications_type'] = 'huddle_member';
                                    $res1Ary['action_date'] = date('Y-m-d H:i:s');
                                    $ins1 = $notificationObj->insertNewRecord($res1Ary);
                                }
                            }
                       }




                        $this->createbusinesstargethuddles($insAry['owner_id'],$insId,$keywords);
                        //$insAry['room_id'] = $insId;
                        $resultAry = array('service_status'=>'success',"content" =>$insId,'responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                } else {
                        $resultAry = array('service_status'=>'error',"content" =>'Error while creating room','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
        }

        public function getroominfoAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode(); 
                $request = $this->getRequest();
                $rid = trim($request->getParam('rid'));
                if($rid == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Room Id should not be empty');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $huddleObj = new Huddles_Model_Huddles();
                $info = $huddleObj->getRoomInfo($rid);
                $resultAry = array('service_status'=>'success',"content" =>$info);
                echo $myjson->customEncode($resultAry);
                return;
        }

        public function getuserroomsAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $request = $this->getRequest();
                //echo "<pre>";print_r($request->getParams());exit;
                $userid = trim($request->getParam('user_id'));
                $usertype = trim($request->getParam('user_type'));
                if($userid == '' || $userid == 0) {
                        $resultAry = array('service_status'=>'error',"error_message" => 'Userid should not be empty');
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            
                        echo $myjson->customEncode($resultAry);
                        return;
                }

                $roomType = trim($request->getParam('room_type'));
                $huddleObj = new Huddles_Model_Huddles();
                $info = $huddleObj->getAllUserRooms($userid,$usertype,$roomType);
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($userid, $usertype, '1');

                //Hard coded vlaue for show new icon.
                $typeOwner = 1;
                $typePublic = 1;
                $typePrivate = 1;

                //echo '<pre>';print_r($info);exit;
                if(count($info) == 0){
                    $resultAry = array('service_status'=>'error',"error_message" =>"No rooms Available","notificationcnt" => $notificationcnt['notificationcnt']);
                }
                
                if(count($info) > 0){
                    $resultAry = array('service_status'=>'success',"content" =>$info,"notificationcnt" => $notificationcnt['notificationcnt'],"typeOwner" =>$typeOwner,"typePublic" =>$typePublic,"typePrivate" =>$typePrivate);
                }
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();       
                //echo "<pre>";print_r($resultAry);exit;
                echo $myjson->customEncode($resultAry);
                return;
	}

        public function getuserroomsbyroomtypeAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $userid = trim($request->getParam('user_id'));
                $usertype = trim($request->getParam('user_type'));
                if($userid == '' || $userid == 0) {
                        $resultAry = array('service_status'=>'error',"error_message" => 'Userid should not be empty');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $limit = $request->getParam('limit');
                if($limit == '')
                    $limit = 20;
                $page_num = $request->getParam('page_num');
                if($page_num == '')
                    $page_num = 0;
                $huddleObj = new Huddles_Model_Huddles();
                $info = $huddleObj->getAllUserRoomsOnRoomType($userid,$usertype,$page_num,$limit);
                $notificationObj = new Notifications_Model_Notifications();
                $notificationcnt = $notificationObj->notificationcnt($userid, $usertype, '1');
                //echo '<pre>';print_r($info);exit;
                if(count($info) == 0) {
                    $resultAry = array('service_status'=>'error',"error_message" =>"No rooms Available","notificationcnt" => $notificationcnt['notificationcnt']);
                }
                if(count($info) > 0) {
                    $resultAry = array('service_status'=>'success',"content" =>$info,"notificationcnt" => $notificationcnt['notificationcnt']);
                }
                echo $myjson->customEncode($resultAry);
                return;
	}

	//function to get all chat log of a room and users info of that room
	public function getroomchatlogAction() {
		$this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
		$myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
		$roomId = trim($request->getParam('rid'));
		if($roomId == 0 || $roomId == '') {
			$resultAry = array('service_status'=>'error',"content" => 'Room Id should not be empty');
			echo $myjson->customEncode($resultAry);
			return;
		}
		$huddleObj = new Huddles_Model_Huddles();
		$info = $huddleObj->getRoomChatLog($roomId);
                $notificationObj = new Notifications_Model_Notifications();
                $user_id = $request->getParam('user_id');
                $user_type = $request->getParam('user_type');
                if(!empty($user_id) && !empty($user_type)) {
                        $notificationcnt = $notificationObj->notificationcnt($user_id, $user_type, '1');
                        $resultAry = array('service_status'=>'success',"content" =>$info,'notificationcnt' => $notificationcnt['notificationcnt']);
		} else {
                        $resultAry = array('service_status'=>'success',"content" =>$info);
                }
		echo $myjson->customEncode($resultAry);
		return;
	}

	//function to get all chat log of a room and users info of that room
	public function getuserroomschatlogAction() {
		$this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
		$request = $this->getRequest();
		$user_id = trim($request->getParam('user_id'));
		if($user_id == 0 || $user_id == '') {
			$resultAry = array('service_status'=>'error',"content" => 'User Id should not be empty');
                        echo $myjson->customEncode($resultAry);
			return;
		}
		$huddleObj = new Huddles_Model_Huddles();
		$info = $huddleObj->getRoomsChatLogOfUser($user_id);
		$notificationObj = new Notifications_Model_Notifications();
                $user_type = $request->getParam('user_type');
                if(!empty($user_id) && !empty($user_type)) {
                    $notificationcnt = $notificationObj->notificationcnt($user_id, $user_type, '1');
                    $resultAry = array('service_status'=>'success',"content" =>$info,'notificationcnt' => $notificationcnt['notificationcnt']);
		} else {
                    $resultAry = array('service_status'=>'success',"content" =>$info);
                }
		echo $myjson->customEncode($resultAry);
		return;
	}

	//function to inset into chat logs
	public function insertchatlogAction() {
		$this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
		$request = $this->getRequest();
		$room_Id = trim($request->getParam('room_id'));
		if($room_Id == 0 || $room_Id == '') {
			$resultAry = array('service_status'=>'error',"content" => 'Room Id should not be empty','userinfocode' => '1000');
                        echo $myjson->customEncode($resultAry);
			return;
		}
		$user_Id = trim($request->getParam('user_id'));
		if($user_Id == 0 || $user_Id == '') {
			$resultAry = array('service_status'=>'error',"content" => 'User Id should not be empty','userinfocode' => '1000');
			echo $myjson->customEncode($resultAry);
			return;
		}
		$message = trim($request->getParam('message'));
		//if($message == 0 || $message == '') {
		//	$resultAry = array('service_status'=>'error',"content" => 'Message should not be empty');
		//	echo $myjson->customEncode($resultAry);
		//	return;
		//}
		$data = array(
                                'room_id' => $room_Id,
                                'user_id' => $user_Id,
				'message' => $message
                        );
		$huddleObj = new Huddles_Model_Huddles();
		$info = $huddleObj->insertChatLog($data);
		if($info == 'err')
			$resultAry = array('service_status'=>'error',"content" => 'You are no longer a part of this Huddle. Looks like Admin has kicked you from the room.','userinfocode' => '1000');
		else	
			$resultAry = array('service_status'=>'success',"content" => $info,'userinfocode' => '1000');
		echo $myjson->customEncode($resultAry);
		return;
	}

	//function to close a huddle room
	public function closehuddleroomAction() {
		$this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
		$huddleObj = new Huddles_Model_Huddles();
		$info = $huddleObj->closeChatRooms();
	}

        public function viewhuddlesAction() {
                if(Zend_Registry::isRegistered('userdata')) {
                        $logged_user_data = Zend_Registry::get('userdata');
                        $loggedUserId = $logged_user_data->user_id;
                        //$loggedUserType = 'U';
                }
                if($loggedUserId == '') {
                        $this->_redirect('user');
                }
                if($this->getRequest()->getParam('type') == 'target') {
                    $type = 'target';
                } else {
                    $type = 'owner';
                }
                $huddleObj = new Huddles_Model_Huddles();
                $info = $huddleObj->getAllUserRooms($loggedUserId,'owner');
                $targetinfo = $huddleObj->getAllUserRooms($loggedUserId,'target');
                //echo '<pre>';print_r($targetinfo);exit;
                $this->view->roomsinfo = $info;
                $this->view->type = $type;
                $this->view->targetinfo = $targetinfo;
        }

	//function to display notification details
	public function targetdetailsAction() {
		$this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
		$request = $this->getRequest();
		$userId = trim($request->getParam('user_id'));
		if($userId == 0 || $userId == '') {
			$resultAry = array('service_status'=>'error',"content" => 'User Id should not be empty');
                        echo $myjson->customEncode($resultAry);
			return;
		}
		$huddleObj = new Huddles_Model_Huddles();
		//get rooms count that targeted the user
		$roomCnt = $huddleObj->getTagetedRoomsCnt($userId);
		if($roomCnt == 0){
			$resultAry = array('service_status'=>'success',"content" => 'Currently No users are talking about you');
			echo $myjson->customEncode($resultAry);
			return;
		}
		//get users count that targeted the user
		$usersCnt = $huddleObj->getUsersTargetedCnt($userId);
		if($roomCnt == 1)
			$msg = $usersCnt.' people are talking about you in a Huddle room';
		else
			$msg = $usersCnt.' people are talking about you in '.$roomCnt.' Huddle rooms';
		$resultAry = array('service_status'=>'success',"content" => $msg);
		echo $myjson->customEncode($resultAry);
		return;
	}

        public function createbusinesstargethuddles($user_id,$room_id,$keywords) {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $businesscatObj = new Business_Model_Businesscategories();
                $notifyid = array();
                $businesskeys = @explode(',', $keywords);
                foreach($businesskeys as $businesskey):
                        $key = @explode('(',$businesskey);
                        preg_match_all('/\((.*?)\)/', $businesskey, $businesses);
                        if(count($businesses)) {
                                if(count($businesses[1])) {
                                        $business = $businesses[1];
                                        if(count($business)) {
                                                $business_id = $businesscatObj->getbusinessid($business[0]);
                                                if(!empty($business_id)) {
                                                        $insId = $businesscatObj->insertbusinesstargetroom($user_id, $business_id['business_id'], $room_id, $key[0]);
                                                        $resAry['user_id'] = $business_id['business_id'];
                                                        $resAry['user_type'] = 'B';
                                                        $resAry['reference_id'] = $insId;
                                                        $resAry['notifications_type'] = 'business_target';
                                                        $resAry['action_date'] = date('Y-m-d H:i:s');
                                                        $notificationObj = new Notifications_Model_Notifications();
                                                        $notifyid[] = $notificationObj->insertNewRecord($resAry);
                                                }
                                        }
                                }
                        }
                endforeach;
        }

        public function adduserstoroomAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $userId = trim($request->getParam('user_id'));
                if($userId == '' || $userId == 0) {
                        $resultAry = array('service_status'=>'error',"content" => 'Choose atleast one user');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $roomId = trim($request->getParam('room_id'));
                if($roomId == '' || $roomId == 0) {
                        $resultAry = array('service_status'=>'error',"content" => 'You must choose atlease one room');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $huddleObj = new Huddles_Model_Huddles();
                $upd = $huddleObj->addUsersToRoom($userId,$roomId);
                if($upd) {
                        $resultAry = array('service_status'=>'success',"content" => 'Selected members added to the room successfully');
                        echo $myjson->customEncode($resultAry);
                        return;
                } else {
                        $resultAry = array('service_status'=>'error',"content" => 'Something wrong while adding new members');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
        }

        public function removeusersfromroomAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();
                $userId = trim($request->getParam('user_id'));
                if($userId == '' || $userId == 0) {
                        $resultAry = array('service_status'=>'error',"content" => 'Choose atleast one user');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $roomId = trim($request->getParam('room_id'));
                if($roomId == '' || $roomId == 0) {
                        $resultAry = array('service_status'=>'error',"content" => 'You must choose atlease one room');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                $huddleObj = new Huddles_Model_Huddles();
                $upd = $huddleObj->removeUsersFromRoom($userId,$roomId);
                

                //Notification for remove user form room 

                $resAry['user_id'] = $userId;
                $resAry['user_type'] = "U";
                $resAry['reference_id'] = $roomId;
                $resAry['notifications_type'] = 'kicked_out_user';
                $resAry['action_date'] = date('Y-m-d H:i:s');
                $notificationObj = new Notifications_Model_Notifications();
                $ins = $notificationObj->insertNewRecord($resAry);



                if($upd == 2) {
                        $resultAry = array('service_status'=>'error',"content" => 'You should not remove all users from the room');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                if($upd) {
                        $resultAry = array('service_status'=>'success',"content" => 'Selected members removed from the room successfully');
                        echo $myjson->customEncode($resultAry);
                        return;
                } else {
                        $resultAry = array('service_status'=>'error',"content" => 'Something wrong while removing members');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
        }

        public function joinnewmemberinroomAction() {
                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                $request = $this->getRequest();


                $owner_id = trim($request->getParam('owner_id'));
                if($owner_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Owner Id should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
                

                $members_id = trim($request->getParam('members_id'));
                if($members_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Members Id should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }

                $room_id = trim($request->getParam('room_id'));
                if($room_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Room Id should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }

                $accept_id = trim($request->getParam('accept_id'));
                $decline_id = trim($request->getParam('decline_id'));

                if($accept_id == '' AND $decline_id == '') {
                        $resultAry = array('service_status'=>'error',"content" => 'Accept and Decline Both Id should not be empty','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }

                $updateAry = array();
                $updateAry['owner_id'] = $owner_id;
                $updateAry['members_id'] = $members_id;
                if($accept_id !='')
                $updateAry['accept_id'] = $accept_id;
                if($decline_id !='')
                $updateAry['decline_id'] = $decline_id;


                $huddleObj = new Huddles_Model_Huddles();
                $updateId = $huddleObj->joinRoom($updateAry,$room_id);


                if($updateId) {
                        $resultAry = array('service_status'=>'success',"content" =>$updateId,'responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                } else {
                        $resultAry = array('service_status'=>'error',"content" =>'Error while member joining room','responsecode'=>'480');
                        echo $myjson->customEncode($resultAry);
                        return;
                }
        }

}