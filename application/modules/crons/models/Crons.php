<?php
class Crons_Model_Crons {

        protected $_dbTable;
        
        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Notifications_Model_DbTable_Notifications');
                }
                return $this->_dbTable;
        }

        public function gettomorrowexpireuppers() {
                $where ="DATE_SUB(C.expiration_date,INTERVAL 1 DAY) = CURDATE()";
                $select = $this->getDbTable()->select()
                                ->from(array('C'=>'tbl_coupons'),array('C.*'))
                                ->where($where)
                                ->group('C.business_id')
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }
        
        public function getexpireuppers() {
                $where ="DATE(C.expiration_date) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)";
                $select = $this->getDbTable()->select()
                                ->from(array('C'=>'tbl_coupons'),array('C.*'))
                                ->where($where)
                                ->group('C.business_id')
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function updatepreviousquotes() {
                $date = date('Y-m-d');
                $db = Zend_Db_Table::getDefaultAdapter();
                $updAry['status'] = 'I';
                //echo '<pre>';print_r($updAry);exit;
                $qry = "UPDATE tbl_quotes SET status = 'I' WHERE created_date < '".$date."'";
                return $db->query($qry);
        }

        public function insertpreviouspopulardata($data, $id='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($id != '')
                    $res = $db->update('tbl_previous_popular_list', $data, array('id= ?' => $id));
                else
                    $res = $db->insert('tbl_previous_popular_list', $data);
                return $res;
        }

        public function getpreviouspopulardata() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                ->from(array('P'=>'tbl_previous_popular_list'))
                                ->setIntegrityCheck(false);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

}