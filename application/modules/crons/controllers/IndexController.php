<?php
class Crons_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
    }

    public function gettomorrowexpireduppersAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cronObj = new Crons_Model_Crons();
        $result = $cronObj->gettomorrowexpireuppers();
        foreach ($result as $key=>$value){
            $purchasecouponsObj = new Business_Model_Purchasecoupons;
            $customers = $purchasecouponsObj->getmycustomersinfo($value['business_id']);
            foreach ($customers as $key=>$row){
                $resAry['reference_id'] = $value['coupon_id'];
                $resAry['user_id'] = $row['user_id'];
                $resAry['user_type'] = 'U';
                $resAry['notifications_type'] = 'tomorrow_expired';
                $resAry['action_date'] = date('Y-m-d H:i:s');
                $notificationObj = new Notifications_Model_Notifications();
                $id = $notificationObj->insertNewRecord($resAry);
            }
        }
    }

    public function getexpireduppersAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cronObj = new Crons_Model_Crons();
        $result = $cronObj->getexpireuppers();
        foreach ($result as $key=>$value){
            $purchasecouponsObj = new Business_Model_Purchasecoupons;
            $customers = $purchasecouponsObj->getmycustomersinfo($value['business_id']);
            foreach ($customers as $key=>$row){
                $resAry['reference_id'] = $value['coupon_id'];
                $resAry['user_id'] = $row['user_id'];
                $resAry['user_type'] = 'U';
                $resAry['notifications_type'] = 'expired';
                $resAry['action_date'] = date('Y-m-d H:i:s');
                $notificationObj = new Notifications_Model_Notifications();
                $id = $notificationObj->insertNewRecord($resAry);
            }
        }
    }

public function updatepreviousquotesAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $cronObj = new Crons_Model_Crons();
        $result = $cronObj->updatepreviousquotes();
    }

public function updatepreviouspopulardataAction() {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $userObj = new User_Model_User();
            $cronObj = new Crons_Model_Crons();
            $user = $userObj->getPopularUser();
            $business = $userObj->getPopularBusiness();
            $school = $userObj->popularschool();
            $college = $userObj->popularcollege();
            $workplace = $userObj->popularworkplace();
            $data['user_id'] = $user['user_id'];
            $data['user_name'] = $user['realname'];
            $data['business_id'] = $business['business_id'];
            $data['business_name'] = $business['realname'];
            $data['school_id'] = $school['school_id'];
            $data['school_name'] = $school['school_name'];
            $data['college_id'] = $college['college_id'];
            $data['college_name'] = $college['college_name'];
            $data['work_place_id'] = $workplace['work_place_id'];
            $data['workplace'] = $workplace['work_place_name'];
            $data['created_date'] = date('Y-m-d');
            $olddata = $cronObj->getpreviouspopulardata();
            if(!empty($olddata['id']))
                $id = $cronObj->insertpreviouspopulardata($data,$olddata['id']);
            else
                $id = $cronObj->insertpreviouspopulardata($data);
    }

    public function checkopenfireconnectionstatusAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        //echo "<pre>";print_r($_SERVER);exit;
        $url = $_SERVER['SERVER_ADDR'].":9090";
        //live server  54.225.82.162
        //org server 54.225.121.38
        //$url = "54.225.82.162:9090";
        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($handle);

        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        if($httpCode != 200) {
            $userEmail = 'narayana@rizecorp.net';
            //$userEmail = 'varaprasad@rizecorp.net';

//            $subject = "Openfire Server Connection Failure";
//            $message = "Open fire connection was failed at ".date('Y-m-d H:i:s')." with the STATUS CODE ".$httpCode;
//            mail($userEmail,$subject,$message,'"From:" . info@upme.com');

            $m = new UpmeSocial_HtmlMailer();
            $m->setSubject("Openfire Server Connection Failure");
            $m->addTo($userEmail)
                ->setViewParam('httpCode',$httpCode)
                ->setViewParam('date',date('Y-m-d H:i:s'));
            $m->sendHtmlTemplate("openfire_connection_failure.phtml");

        } else {
            echo $httpCode;
        }
        curl_close($handle);
    }

	public function userspushnotificationAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $pushObj = new UpmeSocial_Controller_Action_Helper_PushNotifications();
        $userObj = new User_Model_User();
        $notificationObj = new Notifications_Model_Notifications();
        $notificationsdata = $notificationObj->getunsendpushnotifications();
        foreach ($notificationsdata as $notification) :

            $userId = $notification['user_id'];
            $userType = $notification['user_type'];

            // echo  $userId .'userid';
            // Send Push Notification for Android
            $unique_code = $userObj->getuniquecode($userId);
            echo  $unique_code['push_unique_token'] .'userid---------'. $userId . '    ';
            if(isset($unique_code['push_unique_token']) && $unique_code['push_unique_token'] != '') {
                $push_uni_code = array($unique_code['push_unique_token']);
                $os_type = $unique_code['os_type'];
                $notificationcount = $notificationObj->notificationcnt($userId, $userType, '1');
                $notificationcnt = $notificationcount['notificationcnt'];
                if($notification['notifications_type'] == 'message') {
                    $data = $notificationObj->getMsgNotification($notification);
                }
                if($notification['notifications_type'] == 'Followed') {
                    $data = $notificationObj->getFollowedNotifications($notification);
                }
                if($notification['notifications_type'] == 'following') {
                    $data = $notificationObj->getfollowingNotifications($notification);
                }
                if($notification['notifications_type'] == 'purchasecoupon') {
                    $data = $notificationObj->getPurchaseupperNotifications($notification);
                }
                if($notification['notifications_type'] == 'new_upper') {
                    $data = $notificationObj->getNewupperNotifications($notification);
                }
                if($notification['notifications_type'] == 'level_upgrade') {
                    $data = $notificationObj->getLevelupgradeNotifications($notification);
                }
                if($notification['notifications_type'] == 'scribble_blasted') {
                    $data = $notificationObj->getScribbleblastedNotifications($notification);
                }
                if($notification['notifications_type'] == 'photo_blasted') {
                    $data = $notificationObj->getPhotoblastedNotifications($notification);
                }
                if($notification['notifications_type'] == 'video_blasted') {
                    $data = $notificationObj->getVideoblastedNotifications($notification);
                }
                if($notification['notifications_type'] == 'tomorrow_expired') {
                    $data = $notificationObj->getTomorrowexpiredNotifications($notification);
                }
                if($notification['notifications_type'] == 'expired') {
                    $data = $notificationObj->getExpiredNotifications($notification);
                }
                if($notification['notifications_type'] == 'photo_uploads') {
                    $data = $notificationObj->getPhotouploadsNotifications($notification);
                }
                if($notification['notifications_type'] == 'video_uploads') {
                    $data = $notificationObj->getVideouploadsNotifications($notification);
                }
                if($notification['notifications_type'] == 'huddle_target') {
                    $data = $notificationObj->getHuddleTargetNotifications($notification);
                }
                if($notification['notifications_type'] == 'business_target') {
                    $data = $notificationObj->getBusinessTargetNotifications($notification);
                }
                $update = $notificationObj->updatePushNotifications($userId,$userType);
                if($notificationcnt > 1)
                    $message = $data['message']." and more..";
                else
                    $message = $data['message'];

                if($os_type == 'android') {
                    // $push_message = array("test" => $message,'notificationCount' => $notificationcnt);
                    // $push_res = $pushObj->android($push_uni_code,$push_message);
                }

                if($os_type == 'iphone') {
                    define("PRODUCTION_MODE",false);//true
                    $serverId = 1;
                    $serverName = $_SERVER['SERVER_NAME'];
                    if(PRODUCTION_MODE) {
                        $apnsHost = 'gateway.sandbox.push.apple.com';
                    } else {
                        $apnsHost = 'gateway.push.apple.com';
                    }
                    $apnsPort = 2195;
                    if(PRODUCTION_MODE) {
                        // Use a development push certificate
                        $apnsCert = LOCALHOST_URL .'upmesocialDevPush.pem';
                        // echo "Developement Push notification   ";
                    } else {
                        // Use a production push certificate
                        $apnsCert = LOCALHOST_URL .'upmesocialProductionPush.pem'; //upmesocialProductionPush.pem
                    }

                    // Notification content
                    $payload = array();
                    //Basic message
                    $payload['aps'] = array(
                                            'alert' => $message,
                                            'sound' => 'default',
                                        );
                    $payload['server'] = array(
                                            'serverId' => $serverId,
                                            'name' => $serverName
                                        );
                    // Add some custom data to notification
                    $payload['data'] = array(
                                            'foo' => "bar"
                                        );
                    $payload = json_encode($payload);

                    $streamContext = stream_context_create();
                    stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
                    stream_context_set_option($streamContext, 'ssl', 'passphrase', '');

                    $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 5, STREAM_CLIENT_CONNECT, $streamContext);
                    // echo "SSL Connection Done";
                    if (!$apns) {
                         // echo "SSL Connection Fail";
                    } else {
                         // echo "SSL Connection Done";
                    }
                    // echo "</push_connection_status>";
                    $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $unique_code['push_unique_token'])) . chr(0) . chr(mb_strlen($payload)) . $payload;

                    $result = fwrite($apns, $apnsMessage, strlen($apnsMessage));
                    if($result) {
                         // echo ' push success';
                    } else {
                         // echo ' push fail';
                    }
                    // socket_close($apns);
                    fclose($apns);
                }
            }
        endforeach;
    }

    // public function testpushAction()
    // {
   
    //     require_once 'Zend/Mobile/Push/Apns.php';
    //     $this->_helper->layout()->disableLayout();
    //     $this->_helper->viewRenderer->setNoRender();

    //     $message = new Zend_Mobile_Push_Message_Apns();
    //     $message->setAlert('Zend Mobile Push notification');
    //     $message->setBadge(1);
    //     $message->setSound('default');
    //     $message->setId(time());
    //     $message->setToken('5c810c850f75acd567a7962be9adcbcdc25a592af1f65496e1b7cdea174c04e7');
 
    //     $apns = new Zend_Mobile_Push_Apns();

    //     $apns->setCertificate(LOCALHOST_URL .'upmesocialDevPush1.pem');
    //     // if you have a passphrase on your certificate:
    //     $apns->setCertificatePassphrase('');
         
    //     try {
    //         $apns->connect(Zend_Mobile_Push_Apns::SERVER_SANDBOX_URI);
    //     } catch (Zend_Mobile_Push_Exception_ServerUnavailable $e) {
    //         // you can either attempt to reconnect here or try again later
    //          echo 'APNS Connection';
    //         exit(1);
    //     } catch (Zend_Mobile_Push_Exception $e) {
    //         echo 'APNS Connection Error:' . $e->getMessage();
    //         exit(1);
    //     }
         
    //     try {
    //         $apns->send($message);
    //     } catch (Zend_Mobile_Push_Exception_InvalidToken $e) {
    //         // you would likely want to remove the token from being sent to again
    //         echo $e->getMessage();
    //     } catch (Zend_Mobile_Push_Exception $e) {
    //         // all other exceptions only require action to be sent
    //         echo $e->getMessage();
    //     }
    //     echo 'APNS Connection Done:';
    //     $apns->close();
        
    // }

    public function testpushAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();

        $req = $this->getRequest();
        $deviceToken = $req->getParam('device_token');

        echo "device token : ". $deviceToken;
        // $deviceToken = "fba425e6681e4894eb82fa850fc796514b8c1b95979b8cd17e3a37f29c1516c9";
        // Put your private key's passphrase here:
        $passphrase = '';

        // Put your alert message here:
        $message = 'Upme social tset push notification.....!!!';

        define("PRODUCTION_MODE",false);//true
        if(PRODUCTION_MODE) {
         $apnsHost = 'ssl://gateway.sandbox.push.apple.com:2195';
        } else {
         $apnsHost = 'ssl://gateway.push.apple.com:2195';
        }
        if(PRODUCTION_MODE) {
          // Use a development push certificate
         $apnsCert = LOCALHOST_URL .'upmesocialDevPush.pem';
         echo "Developement Push notification   ";
        } else {
        // Use a production push certificate
            echo "Production Push notification   ";
        $apnsCert = LOCALHOST_URL .'upmesocialProductionPush.pem'; //upmesocialProductionPush.pem
        }

        // $apnsCert = LOCALHOST_URL . 'upmesocialDevPush.pem';
        ////////////////////////////////////////////////////////////////////////////////
        
        echo "APN HOST: " . $apnsHost;
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $apnsCert);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        // Open a connection to the APNS server
        $fp = stream_socket_client($apnsHost, $err,$errstr, 10,STREAM_CLIENT_ASYNC_CONNECT, $ctx);
      
        if (!$fp)
        exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array(
        'alert' => $message,
        'sound' => 'default'
        );

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
        echo '\n Message not delivered' . PHP_EOL;
        else
        echo '\n Message successfully delivered' . PHP_EOL;

        // Close the connection to the server
        fclose($fp);

    }

}
