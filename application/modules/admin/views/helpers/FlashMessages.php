<?php

class Zend_View_Helper_FlashMessages extends Zend_View_Helper_Abstract
{
    public function flashMessages()
    {
        $messages = Zend_Controller_Action_HelperBroker::getStaticHelper('FlashMessenger')->getMessages();
        $output = '';
        if (!empty($messages)) {
            
            $output .= '<section id="statusButtons">';
            foreach ($messages as $message) {
                $output .= '
                            <div class="'.key($message).'">
                            <a class="close" data-dismiss="alert" href="#">×</a>'.current($message).'</div>';
            }
            $output .= '</section>';
        }
        
        return $output;
    }
}

