<?php
class Admin_Form_AdminBusinessuser extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id','adminbuinessuser');
        $this->setAttrib('class','form-horizontal span12');
        $businessuserhidden = new Zend_Form_Element_Hidden(array('name' => 'business_id', 'id' => 'business_id'));
        $id = Zend_Controller_Front::getInstance()->getRequest()->getParam( 'id');//echo $id;exit;
        
        $business_name = new Zend_Form_Element_Text(array('name' => 'business_name', 'id' => 'business_name'));
        $business_name->setLabel($this->getView()->translate('Business Name'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addValidator('Db_NoRecordExists', true, array('table' => 'tbl_business_users', 'field' => 'business_name','exclude' => array ('field' => 'business_id', 'value' => $id)))
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $description = new Zend_Form_Element_Text(array('name' => 'description', 'id' => 'description'));
        $description->setLabel($this->getView()->translate('Description'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $type = new Zend_Form_Element_Select(array('name' => 'type', 'id' => 'type','class'=>'required'));
        $type->setLabel($this->getView()->translate('Type of Business'))//to display label
                    ->setRequired(true)
                    ->addMultiOptions(array(
                    'b2b' => 'Business To Business',
                    'b2c' => 'Business To Company',
                    'both'=> 'Both'
                        )) 
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));
        
        $categoriesobj  = new Business_Model_Businesscategories();
        $categories     = $categoriesobj->get_business_categories();
        $catArr = array();
        $catArr[] = "Please Select";
        foreach ($categories as $result):
          $catArr[$result["id"]] =$result["category_name"];   
        endforeach;
        //echo '<pre>';print_r($counArr);exit;
        $category_id = new Zend_Form_Element_Select(array('name' => 'category_id', 'id' => 'category_id','class'=>'required'));
        $category_id->setLabel($this->getView()->translate('Categories'))//to display label
                    ->setRequired(true)
                    ->addMultiOptions($catArr)
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));
        
        $username = new Zend_Form_Element_Text(array('name' => 'username', 'id' => 'username'));
        $username->setLabel($this->getView()->translate('User Name'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addValidator('Db_NoRecordExists', true, array('table' => 'tbl_users', 'field' => 'username','exclude' => array ('field' => 'user_id', 'value' => $id)))
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $password = new Zend_Form_Element_Password(array('name' => 'password', 'id' => 'password'));
        $password->setLabel($this->getView()->translate('Password'))//to display label
                //->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        
        $security_question = new Zend_Form_Element_Text(array('name' => 'security_question', 'id' => 'security_question'));
        $security_question->setLabel($this->getView()->translate('Security Question'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $security_answer = new Zend_Form_Element_Text(array('name' => 'security_answer', 'id' => 'security_answer'));
        $security_answer->setLabel($this->getView()->translate('Security Answer'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        
        $email = new Zend_Form_Element_Text(array('name' => 'email', 'id' => 'email'));
        $email->setLabel($this->getView()->translate('Email'))//to display label
            ->setRequired(true)
            ->setAttrib('class','required email')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->addValidator('EmailAddress')
            ->addValidator('Db_NoRecordExists', true, array('table' => 'tbl_users', 'field' => 'email'))
            ->clearDecorators() //clear all default assigned decorators		
            ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
            ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
            ->addDecorator('Label')
            ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                        )   //create html div tag with customized attributes
            ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $website = new Zend_Form_Element_Text(array('name' => 'website', 'id' => 'website','class'=>'required'));
        $website->setLabel($this->getView()->translate('Website'))//to display label
            ->setRequired(true)
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty')
            ->clearDecorators() //clear all default assigned decorators		
            ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
            //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
            ->addDecorator('Label')
            ->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'control-group span7 input-append date','id'=>'ex_date','data-date-format'=>'dd-mm-yyyy','data-date'=>"12-02-2012")
            ); //create html div tag with customized attributes

        $firstname = new Zend_Form_Element_Text(array('name' => 'firstname', 'id' => 'firstname', 'minlength'=>'3','maxlength'=>'25'));
        $firstname->setLabel($this->getView()->translate('First Name'))//to display label
                    ->setRequired(true)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $lastname = new Zend_Form_Element_Text(array('name' => 'lastname', 'id' => 'lastname', 'minlength'=>'3','maxlength'=>'25'));
        $lastname->setLabel($this->getView()->translate('Last Name'))//to display label
                    ->setRequired(true)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $address_line_1 = new Zend_Form_Element_Text(array('name' => 'address_line_1', 'id' => 'address_line_1'));
        $address_line_1->setLabel($this->getView()->translate('Address1'))//to display label
                    ->setRequired(true)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $address_line_2 = new Zend_Form_Element_Text(array('name' => 'address_line_2', 'id' => 'address_line_2'));
        $address_line_2->setLabel($this->getView()->translate('Address2'))//to display label
                    ->setRequired(true)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        /*$countriesobj         = new User_Model_Countries();
        $countries     = $countriesobj->get_countries();
        $counArr = array();
        $counArr[] = "Please Select";
        foreach ($countries as $result):
          $counArr[$result["id"]] =$result["country_name"];   
        endforeach;
        //echo '<pre>';print_r($counArr);exit;
        $country = new Zend_Form_Element_Select(array('name' => 'country', 'id' => 'country','class'=>'required','onchange' => 'fngetstates(this.value);'));
        $country->setLabel($this->getView()->translate('Country'))//to display label
                    ->setRequired(true)
                    ->addMultiOptions($counArr)
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));
        //$statesobj     = new User_Model_States();
        //$states     = $statesobj->get_states();//echo '<pre>';print_r($states);exit;
        $state = new Zend_Form_Element_Select(array('name' => 'state', 'id' => 'state','class'=>'required','onchange' => 'fngetcities(this.value);'));
        $state->setLabel($this->getView()->translate('State'))//to display label
                    ->setRequired(true)
                    //->addMultiOptions($states)
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));
        //$citiesobj     = new User_Model_States();
        //$cities     = $citiesobj->get_states();
        $city = new Zend_Form_Element_Select(array('name' => 'city', 'id' => 'city','class'=>'required'));
        $city->setLabel($this->getView()->translate('City'))//to display label
                    ->setRequired(true)
                    //->addMultiOptions($cities)
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));*/
        $zipcode = new Zend_Form_Element_Text(array('name' => 'zipcode', 'id' => 'zipcode'));
        $zipcode->setLabel($this->getView()->translate('Zipcode'))//to display label
                    ->setRequired(true)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $phone_no = new Zend_Form_Element_Text(array('name' => 'phone_no', 'id' => 'phone_no'));
        $phone_no->setLabel($this->getView()->translate('Phone No'))//to display label
                    ->setRequired(true)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $mobile_no = new Zend_Form_Element_Text(array('name' => 'mobile_no', 'id' => 'mobile_no'));
        $mobile_no->setLabel($this->getView()->translate('Mobile No'))//to display label
                    ->setRequired(true)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $image_path = new Zend_Form_Element_File(array('name' => 'image_path', 'id' => 'image_path'));
        $image_path->setLabel($this->getView()->translate('Upload Photo'))//to display label
                        ->setMaxFileSize(10 * 1024 * 1024)
                        ->addValidator('Count', false, 1)// ensure only 1 file
                        ->addValidator('Size', false, 10 * 1024 * 1024)// limit to 10 meg
                        ->addValidator('Extension', false, 'jpg,jpeg,png,gif')// only JPEG, PNG, and GIFs
                        ->addValidator('ImageSize', false,array('minwidth'=>150,'minheight'=>120,'maxwidth' => MAX_IMAGE_WIDTH,'maxheight' => MAX_IMAGE_HEIGHT))
                        ->setDestination("uploads/images/original/")
                //->clearDecorators() //clear all default assigned decorators
                //->addDecorator('File') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                                array('data'=>'HtmlTag'),
                                array('tag' => 'div', 'class' => 'control-group span5')
                            )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $options = array('A' => 'Active', 'I' => 'Inactive');
        $status = new Zend_Form_Element_Radio('status');
        $status->setLabel($this->getView()->translate('Status'))
                ->setRequired(true)
                ->setSeparator('')
                ->setValue('A')
                ->setMultiOptions($options);
        $status->clearDecorators() //clear all default assigned decorators
                ->setAttrib('label_class', 'radio inline')    
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'),
                    array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'openOnly' => false));


        $submit = new Zend_Form_Element_Button('submit');
        $submit->setAttrib('class','btn btn-primary');
        $submit->setAttrib('type','submit');
        //$submit->setLabel('Submit');//to display label
        $submit->clearDecorators(); //clear all default assigned decorators		
        $submit->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $submit->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box

        $submit->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'form-action-area')
                )  //create html div tag with customized attributes		
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group', 'placement' => 'prepand', 'openOnly' => true));

        $cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('class','btn btn-primary')
                        ->setAttrib('onclick','window.location.href="'.SITE_URL.'admin/businessusers"');
        $cancel->clearDecorators(); //clear all default assigned decorators		
        $cancel->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $cancel->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $cancel->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'form-action-area')
                )  //create html div tag with customized attributes
        ->addDecorator(array('labelDivOpen'=>'HtmlTag'), array('tag'=>'div','class'=>'control-group','closeOnly' => true));

        $this->addElements(
            array(
                    $businessuserhidden,
                    $business_name,
                    $description,
                    $type,
                    $category_id,
                    $username,                                            
                    $password,
                    $security_question,
                    $security_answer,
                    $email,
                    $website,
                    $firstname,
                    $lastname,
                    $address_line_1,
                    $address_line_2,
                    //$country,
                    //$state,
                    //$city,
                    $zipcode,
                    $phone_no,
                    $mobile_no,
                    $image_path,
                    $status,
                    $submit,
                    $cancel
                )
            );
    }

    public function isValid( $data )
    {
        $isValid = parent::isValid( $data );
        if( !$isValid )
        {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 )
            {
                foreach( $arrErrors as $key => $value )
                {
                    if( is_array( $value ) && count( $value ) > 0 )
                    {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                        $objElement->setAttrib('class', $objElement->getAttrib('class') . 'invalid errorclass');
                    }
                }
            }
            return false;
        }
        return true;
    }
}
?>