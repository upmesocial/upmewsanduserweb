<?php
class Admin_Form_AdminCoolpoints extends Zend_Form
{
    public function init()
    {
        $id = Zend_Controller_Front::getInstance()->getRequest()->getParam( 'id', null );
        $this->setMethod('post');
        $this->setAttrib('id','addcoolpoints');
        $this->setAttrib('class','form-horizontal span12');
        $this->setAttrib('enctype','multipart/form-data');
        $hidden = new Zend_Form_Element_Hidden(array('name' => 'id', 'id' => 'id'));
        //creating textbox element
        if($id == '')
            $name =new Zend_Form_Element_Text(array('name'=>'name','id'=>'name','class'=>'required'));
        else
            $name =new Zend_Form_Element_Text(array('name'=>'name','id'=>'name','class'=>'required','readonly'=>true));
        $name->setLabel($this->getView()->translate('Name'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
        
        $points =new Zend_Form_Element_Text(array('name'=>'points','id'=>'points','class'=>'required'));
        $points->setLabel($this->getView()->translate('Points'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
		
                
        $submit = new Zend_Form_Element_Button('submit');
        $submit->setAttrib('class','btn btn-primary')
                ->setAttrib('type','submit');
        $submit->clearDecorators(); //clear all default assigned decorators		
        $submit->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $submit->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box		
        $submit->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'form-action-area')
                        )  //create html div tag with customized attributes		
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group', 'placement' => 'prepand', 'openOnly' => true));    

        $cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('class','btn btn-primary')
                        ->setAttrib('onclick','window.location.href="'.SITE_URL.'admin/coolpoints"');
        $cancel->clearDecorators(); //clear all default assigned decorators		
        $cancel->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $cancel->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $cancel->addDecorator(
        array('data'=>'HtmlTag'), 
        array('tag' => 'div', 'class' => 'form-action-area')
        )  //create html div tag with customized attributes
        ->addDecorator(array('labelDivOpen'=>'HtmlTag'), array('tag'=>'div','class'=>'control-group','closeOnly' => true));
		       
        $this->addElements(
            array(
                    $hidden,
                    $name,
                    $points,
                    $submit,
                    $cancel
            )
        );
    }

    public function isValid( $data )
    {
        $isValid = parent::isValid( $data );
        if( !$isValid )
        {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 )
            {
                foreach( $arrErrors as $key => $value )
                {
                    if( is_array( $value ) && count( $value ) > 0 )
                    {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                            $objElement->setAttrib( 'class', $objElement->getAttrib( 'class' ) . ' errorclass' );
                    }
                }
            }
            return false;
        }	   
        return true;
    }   	
}