<?php
class Admin_Form_AdminQuotes extends Zend_Form {

    public function init() {
        $this->setMethod('post');
        $this->setAttrib('id','addquotes');
        $this->setAttrib('class','form-horizontal span12');
        $this->setAttrib('enctype','multipart/form-data');
        $hidden = new Zend_Form_Element_Hidden(array('name' => 'id', 'id' => 'id'));
        //creating textbox element
        $category =new Zend_Form_Element_Text(array('name'=>'quote','id'=>'quote','class'=>'required'));
        $category->setLabel($this->getView()->translate('Quote'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addValidator('Db_NoRecordExists', true, array('table'=>'tbl_quotes','field'=>'quote'))
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'),
                    array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));

        $options = array('1' => 'Active', '0' => 'Inactive');
        $status = new Zend_Form_Element_Radio('status');
        $status->setLabel($this->getView()->translate('Status'))
                ->setRequired(true)
                ->setSeparator('')
                ->setValue('1')
                ->setMultiOptions($options);
        $status->clearDecorators() //clear all default assigned decorators
                ->setAttrib('label_class', 'radio inline')
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'openOnly' => false));
                
        $submit = new Zend_Form_Element_Button('submit');
        $submit->setAttrib('class','btn btn-primary')
                ->setAttrib('type','submit');
        $submit->clearDecorators(); //clear all default assigned decorators		
        $submit->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $submit->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box		
        $submit->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'form-action-area')
                        )  //create html div tag with customized attributes		
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group', 'placement' => 'prepand', 'openOnly' => true));    

        $cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('class','btn btn-primary')
                        ->setAttrib('onclick','window.location.href="'.SITE_URL.'admin/categories/index/"');
        $cancel->clearDecorators(); //clear all default assigned decorators		
        $cancel->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $cancel->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $cancel->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'form-action-area')
                )  //create html div tag with customized attributes
        ->addDecorator(array('labelDivOpen'=>'HtmlTag'), array('tag'=>'div','class'=>'control-group','closeOnly' => true));

        $this->addElements(
            array(
                    $hidden,
                    $category,
                    $status,
                    $submit,
                    $cancel
            )
        );
    }

    public function isValid( $data ) {
        $isValid = parent::isValid( $data );
        if( !$isValid ) {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 ) {
                foreach( $arrErrors as $key => $value ) {
                    if( is_array( $value ) && count( $value ) > 0 ) {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                            $objElement->setAttrib( 'class', $objElement->getAttrib( 'class' ) . ' errorclass' );
                    }
                }
            }
            return false;
        }
        return true;
    }

}