<?php
class Admin_Form_AdminChangePassword extends Zend_Form
{
    public function init()
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $this->setMethod('post');
        //$this->setAction($view->SITE_URl().'/admin/index/changepassword');
        $this->setAttrib('id','changepassword');
        $this->setAttrib('class','form-horizontal span12');
        $changepasswordhidden = new Zend_Form_Element_Hidden(array('name' => 'id', 'id' => 'id'));

        $password = new Zend_Form_Element_Password(array('name'=>'password','id'=>'password','class'=>'required'));
        $password->setLabel($this->getView()->translate('Password'))//to display label
                        ->setAttrib('renderPassword',true)
                        ->setRequired(true)
                        ->addFilter('StringTrim')
                        ->addValidator('NotEmpty')
                        ->clearDecorators() //clear all default assigned decorators		
                        ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                        ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                        ->addDecorator('Label')
                        ->addDecorator(
                                array('data'=>'HtmlTag'), 
                                array('tag' => 'div', 'class' => 'control-group span7')
                        )   //create html div tag with customized attributes
                        ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));

        $new_password = new Zend_Form_Element_Password(array('name' => 'new_password', 'id' => 'new_password','class'=>'required'));
        $new_password->setLabel($this->getView()->translate('New Password'))//to display label
                        ->setAttrib('renderPassword',true)
                        ->setRequired(true)
                        ->addFilter('StringTrim')
                        ->addValidator('NotEmpty')
                        ->clearDecorators() //clear all default assigned decorators		
                        ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                        ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                        ->addDecorator('Label')
                        ->addDecorator(
                                array('data'=>'HtmlTag'), 
                                array('tag' => 'div', 'class' => 'control-group span7')
                        )   //create html div tag with customized attributes
                        ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'openOnly' => false));

        $confirm_password = new Zend_Form_Element_Password(array('name' => 'confirm_password', 'id' => 'confirm_password','class'=>'required'));
        $confirm_password->setLabel($this->getView()->translate('Confirm Password'))//to display label
                        ->setAttrib('renderPassword',true)
                        ->setRequired(true)
                        ->addFilter('StringTrim')
                        ->addValidator('NotEmpty')
                        ->clearDecorators() //clear all default assigned decorators		
                        ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                        ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                        ->addDecorator('Label')
                        ->addDecorator(
                                array('data'=>'HtmlTag'), 
                                array('tag' => 'div', 'class' => 'control-group span7')
                        )   //create html div tag with customized attributes
                        ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'openOnly' => false));

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setAttrib('class','btn btn-primary')
                        ->setAttrib('type','submit');
        //$submit->setLabel('Submit');//to display label
        $submit->clearDecorators(); //clear all default assigned decorators		
        $submit->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $submit->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $submit->addDecorator(
                                array('data'=>'HtmlTag'), 
                                array('tag' => 'div', 'class' => 'form-action-area')
        )  //create html div tag with customized attributes		
        ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group', 'placement' => 'prepand', 'openOnly' => true));    

        $cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('class','btn btn-primary')
                        ->setAttrib('onclick','window.location.href="'.SITE_URL.'admin/index/dashboard"');
        //$submit->setLabel('Submit');//to display label
        $cancel->clearDecorators(); //clear all default assigned decorators		
        $cancel->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $cancel->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $cancel->addDecorator(
                                array('data'=>'HtmlTag'), 
                                array('tag' => 'div', 'class' => 'form-action-area')
        )  //create html div tag with customized attributes		
        ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group',  'closeOnly' => true));

        $this->addElements(
            array(
                $password,
                $new_password,
                $confirm_password,
                $submit,
                $cancel
            )
        );
    }

    public function isValid( $data )
    {
        $isValid = parent::isValid( $data );
        if( !$isValid )
        {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 )
            {
                foreach( $arrErrors as $key => $value )
                {
                    if( is_array( $value ) && count( $value ) > 0 )
                    {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                        {
                                $objElement->setAttrib('class',$objElement->getAttrib('class') . 'invalid errorclass');
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

}