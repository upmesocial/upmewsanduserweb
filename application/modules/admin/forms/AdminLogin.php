<?php
class Admin_Form_AdminLogin extends Zend_Form
{
    public function init()
    {
        $this->addPrefixPath('UpmeSocial_Form_Decorator','UpmeSocial/Form/Decorator','decorator');
        $this->setMethod('post');
        $this->setAttrib('id','adminLoginForm');
        $this->setAttrib('class','form-login');
        $this->setAttrib('style','border:0px');
        $username = new Zend_Form_Element_Text(array('name'=>'user_name','id'=>'user_name','placeholder'=>'User Name'));
        $username->setLabel($this->getView()->translate('User Name:'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim','StringToLower')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label',array('class'=>'admin_control-label', 'style'=>'margin-right:15px;'))
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group')
                    )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false))
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'errDiv', 'placement' => 'prepend', 'closeOnly' => false));
        $username->addDecorators(array('FormElements',
                                    array('LoginInformation',array('placement'=>'prepend','text'=>'Login','closeOnly'=>false)),
                                'Form'))
                ->removeDecorator('form');

        $password = new Zend_Form_Element_Password(array('name' => 'admin_pwd', 'id' => 'admin_pwd','placeholder'=>'Password'));
        $password->setLabel($this->getView()->translate('Password:'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim','StringToLower')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label',array('class'=>'admin_control-label', 'style'=>'margin-right:15px;'))
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group')
                    )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        
        $submit = new Zend_Form_Element_Button('Login');
        $submit->setAttrib('class','btn btn-primary');
        $submit->setAttrib('type','submit');
        //$submit->setLabel('Submit');//to display label
        $submit->clearDecorators(); //clear all default assigned decorators		
        $submit->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $submit->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $submit->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'admin_controls')
                )  //create html div tag with customized attributes		
            ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group', 'placement' => 'prepand', 'openOnly' => true));

        $this->addElements(
                    array(
                        $username,
                        $password,
                        $submit
                    )
        );
    }

    public function isValid( $data )
    {
        $isValid = parent::isValid( $data );
        if( !$isValid )
        {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 )
            {
                foreach( $arrErrors as $key => $value )
                {
                    if( is_array( $value ) && count( $value ) > 0 )
                    {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                        $objElement->setAttrib('class', $objElement->getAttrib('class') . 'invalid errorclass');
                    }
                }
            }
            return false;
        }
        return true;
    }   	
}
?>