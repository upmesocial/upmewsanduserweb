<?php
class Admin_Form_AdminUserEdit extends Zend_Form {
    public function init() {
        $this->setMethod('post');
        $this->setAttrib('id','UserEdit');
        //$this->addElementPrefixPath('validate');
        $this->setAttrib('class','form-horizontal span12');
        $countryid = $this->getAttrib('countryid');
        $stateid = $this->getAttrib('stateid');
        $cityid = $this->getAttrib('cityid');
        $userhidden = new Zend_Form_Element_Hidden(array('name' => 'user_id', 'id' => 'user_id'));
        $id = Zend_Controller_Front::getInstance()->getRequest()->getParam( 'id');//echo $id;exit;
        $username = new Zend_Form_Element_Text(array('name' => 'username', 'id' => 'username'));
        $username->setLabel($this->getView()->translate('User Name'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addValidator('Db_NoRecordExists', true, array('table' => 'tbl_users', 'field' => 'username','exclude' => array ('field' => 'user_id', 'value' => $id)))
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $password = new Zend_Form_Element_Password(array('name' => 'password', 'id' => 'password'));
        $password->setLabel($this->getView()->translate('Password'))//to display label
                //->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $email = new Zend_Form_Element_Text(array('name' => 'email', 'id' => 'email'));
        $email->setLabel($this->getView()->translate('Email'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required email')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->addValidator('EmailAddress')
                ->addValidator('Db_NoRecordExists', true, array('table' => 'tbl_users', 'field' => 'email'))
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                                array('data'=>'HtmlTag'),
                                array('tag' => 'div', 'class' => 'control-group span7')
                            )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $dob = new Zend_Form_Element_Text(array('name' => 'dob', 'id' => 'dob','class'=>'required','readonly'=>true));
        $dob->setLabel($this->getView()->translate('Date Of Birth'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7 input-append date','id'=>'ex_date','data-date-format'=>'dd-mm-yyyy','data-date'=>"12-02-2012")
                ); //create html div tag with customized attributes

        $firstname = new Zend_Form_Element_Text(array('name' => 'firstname', 'id' => 'firstname', 'minlength'=>'3','maxlength'=>'25'));
        $firstname->setLabel($this->getView()->translate('First Name'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $lastname = new Zend_Form_Element_Text(array('name' => 'lastname', 'id' => 'lastname', 'minlength'=>'3','maxlength'=>'25'));
        $lastname->setLabel($this->getView()->translate('Last Name'))//to display label
                //->setRequired(true)
                //->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $gender = new Zend_Form_Element_Radio(array('name' => 'gender', 'id' => 'gender','class'=>'text required'));
        $gender->setLabel($this->getView()->translate('Gender'))//to display label
                ->setRequired(true)
                ->setSeparator('')
                ->setValue('M')
                ->setAttrib('class','required')
                ->setMultiOptions(array('M' => 'Male','F' => 'Female'))
                ->addErrorMessage($this->getView()->translate('Please select Gender.'))
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'openOnly' => false));

        $address_line_1 = new Zend_Form_Element_Text(array('name' => 'address_line_1', 'id' => 'address_line_1'));
        $address_line_1->setLabel($this->getView()->translate('Address1'))//to display label
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $address_line_2 = new Zend_Form_Element_Text(array('name' => 'address_line_2', 'id' => 'address_line_2'));
        $address_line_2->setLabel($this->getView()->translate('Address2'))//to display label
                ->setRequired(true)
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $countriesobj = new User_Model_Countries();
        $countries = $countriesobj->get_countries();
        $counArr = array();
        $counArr[] = "--- Select Country ---";
        foreach ($countries as $result):
            $counArr[$result["id"]] = $result["country_name"];   
        endforeach;
        //echo '<pre>';print_r($counArr);exit;
        $country = new Zend_Form_Element_Select(array('name' => 'country', 'id' => 'country','class'=>'required','onchange' => 'fngetstates(this.value);'));
        $country->setLabel($this->getView()->translate('Country'))//to display label
                ->setRequired(true)
                ->addMultiOptions($counArr)
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));

        $statesobj = new User_Model_States();
        $states = $statesobj->get_states($countryid);
        $stateArr = array();
        $stateArr[] = "--- Select States ---";
        foreach ($states as $result):
            $stateArr[$result["id"]] = $result["state_name"];   
        endforeach;
        $state = new Zend_Form_Element_Select(array('name' => 'state', 'id' => 'state','class'=>'required','onchange' => 'fngetcities(this.value);'));
        $state->setLabel($this->getView()->translate('State'))//to display label
                ->setRequired(true)
                ->addMultiOptions($stateArr)
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators		
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'control-group span7')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));

        $citiesobj = new User_Model_Cities();
        $cities = $citiesobj->get_cities($stateid);
        $cityArr = array();
        $cityArr[] = "--- Select City ---";
        foreach ($cities as $result):
            $cityArr[$result["id"]] = $result["city_name"];
        endforeach;
        $city = new Zend_Form_Element_Select(array('name' => 'city', 'id' => 'city','class'=>'required','onchange' => 'fngetzipcodes(this.value);'));
        $city->setLabel($this->getView()->translate('City'))//to display label
                    ->setRequired(true)
                    ->addMultiOptions($cityArr)
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    //->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'prepend', 'openOnly' => false));

        $zipcodes = $citiesobj->getZipcodesByCity($cityid);
        $zipcodeArr = array();
        $zipcodeArr[] = "--- Select Zipcodes ---";
        foreach ($zipcodes as $result):
            $zipcodeArr[$result["id"]] = $result["zipcode"];
        endforeach;
        $zipcode = new Zend_Form_Element_Select(array('name' => 'zipcode', 'id' => 'zipcode'));
        $zipcode->setLabel($this->getView()->translate('Zipcode'))//to display label
                    ->setRequired(true)
                    ->addMultiOptions($zipcodeArr)
                    ->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $schoolsobj = new User_Model_User();
        $schools = $schoolsobj->getSchools();
        $schoolArr = array();
        $schoolArr[] = "--- Select School ---";
        foreach ($schools as $result):
            $schoolArr[$result["school_id"]] = $result["school_name"];   
        endforeach;
        $high_school_info = new Zend_Form_Element_Select(array('name' => 'high_school_info', 'id' => 'high_school_info'));
        $high_school_info->setLabel($this->getView()->translate('Highschool'))//to display label
                    //->setRequired(true)
                    ->addMultiOptions($schoolArr)
                    //->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    //->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $collegesobj = new User_Model_User();
        $colleges = $collegesobj->getColleges();
        $collegeArr = array();
        $collegeArr[] = "--- Select College ---";
        foreach ($colleges as $result):
            $collegeArr[$result["college_id"]] = $result["college_name"];
        endforeach;
        $college_info = new Zend_Form_Element_Select(array('name' => 'college_info', 'id' => 'college_info'));
        $college_info->setLabel($this->getView()->translate('College University'))//to display label
                    //->setRequired(true)
                    ->addMultiOptions($collegeArr)
                    //->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    //->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $workplacesobj = new User_Model_User();
        $workplaces = $workplacesobj->getWorkPlaces();
        $workplaceArr = array();
        $workplaceArr[] = "--- Select Work Place ---";
        foreach ($workplaces as $result):
            $workplaceArr[$result["work_place_id"]] = $result["work_place_name"];
        endforeach;
        $employer_info = new Zend_Form_Element_Select(array('name' => 'employer_info', 'id' => 'employer_info'));
        $employer_info->setLabel($this->getView()->translate('Work Place'))//to display label
                    //->setRequired(true)
                    ->addMultiOptions($workplaceArr)
                    //->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    //->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));

        $mobile_no = new Zend_Form_Element_Text(array('name' => 'mobile_no', 'id' => 'mobile_no'));
        $mobile_no->setLabel($this->getView()->translate('Mobile No'))//to display label
                    //->setRequired(true)
                    //->setAttrib('class','required')
                    ->addFilter('StringTrim')
                    ->addValidator('NotEmpty')
                    ->clearDecorators() //clear all default assigned decorators		
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $profile_pic_path = new Zend_Form_Element_File(array('name' => 'profile_pic_path', 'id' => 'profile_pic_path'));
        $profile_pic_path->setLabel($this->getView()->translate('Upload Photo'))//to display label
                    //->setMaxFileSize(10 * 1024 * 1024)
                    ->addValidator('Count', false, 1)// ensure only 1 file
                    ->addValidator('Size', false, 10 * 1024 * 1024)// limit to 10 meg
                    ->addValidator('Extension', false, 'jpg,jpeg,png,gif')// only JPEG, PNG, and GIFs
                    ->addValidator('ImageSize', false,array('minwidth'=>150,'minheight'=>120,'maxwidth' => MAX_IMAGE_WIDTH,'maxheight' => MAX_IMAGE_HEIGHT))
                    ->setDestination("uploads/images/original/")
                    //->clearDecorators() //clear all default assigned decorators
                    //->addDecorator('File') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                            array('data'=>'HtmlTag'),
                            array('tag' => 'div', 'class' => 'control-group span5')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $profile_video_path = new Zend_Form_Element_File(array('name' => 'profile_video_path', 'id' => 'profile_video_path'));
        $profile_video_path->setLabel($this->getView()->translate('Upload Video'))//to display label
                    //->setMaxFileSize(10 * 1024 * 1024)
                    ->addValidator('Count', false, 1)// ensure only 1 file
                    ->addValidator('Size', false, array('min' => '10kB', 'max' => '100MB'))// limit to 10 meg
                    ->addValidator('Extension', false, 'avi,mov,wmv,mpeg,mp4,flv,mpg')// only JPEG, PNG, and GIFs
                    ->setDestination("uploads/videos/")
                    //->clearDecorators() //clear all default assigned decorators
                    //->addDecorator('File') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                            array('data'=>'HtmlTag'),
                            array('tag' => 'div', 'class' => 'control-group span5')
                    )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'closeOnly' => false));
        $options = array('A' => 'Active', 'G' => 'Guest', 'D'=>'Disable');
        $user_account_status = new Zend_Form_Element_Radio('user_account_status');
        $user_account_status->setLabel($this->getView()->translate('Status'))
                    ->setRequired(true)
                    ->setSeparator('')
                    ->setValue('A')
                    ->setMultiOptions($options);
        $user_account_status->clearDecorators() //clear all default assigned decorators
                    ->setAttrib('label_class', 'radio inline')    
                    ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                    ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                    ->addDecorator('Label')
                    ->addDecorator(
                        array('data'=>'HtmlTag'),
                        array('tag' => 'div', 'class' => 'control-group span7')
                        )   //create html div tag with customized attributes
                    ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'openOnly' => false));

        $options = array('J' => 'User', 'S' => 'Student', 'A' => 'Artist', 'C' => 'Celebrity');
        $user_type = new Zend_Form_Element_Radio('user_type');
        $user_type->setLabel($this->getView()->translate('Type'))
                ->setRequired(true)
                ->setSeparator('')
                ->setValue('J')
                ->setMultiOptions($options);
        $user_type->clearDecorators() //clear all default assigned decorators
                ->setAttrib('label_class', 'radio inline')    
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                    array('data'=>'HtmlTag'),
                    array('tag' => 'div', 'class' => 'control-group span7')
                    )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'clearfix', 'placement' => 'append', 'openOnly' => false));

        $submit = new Zend_Form_Element_Button('submit');
        $submit->setAttrib('class','btn btn-primary');
        $submit->setAttrib('type','submit');
        //$submit->setLabel('Submit');//to display label
        $submit->clearDecorators(); //clear all default assigned decorators		
        $submit->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $submit->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box

        $submit->addDecorator(
                array('data'=>'HtmlTag'), 
                array('tag' => 'div', 'class' => 'form-action-area')
                )  //create html div tag with customized attributes		
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group', 'placement' => 'prepand', 'openOnly' => true));

        $cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('class','btn btn-primary')
                        ->setAttrib('onclick','window.location.href="'.SITE_URL.'admin/users/index/"');
        $cancel->clearDecorators(); //clear all default assigned decorators		
        $cancel->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $cancel->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $cancel->addDecorator(
                    array('data'=>'HtmlTag'), 
                    array('tag' => 'div', 'class' => 'form-action-area')
                )  //create html div tag with customized attributes
        ->addDecorator(array('labelDivOpen'=>'HtmlTag'), array('tag'=>'div','class'=>'control-group','closeOnly' => true));

        $this->addElements(
            array(
                    $userhidden,
                    $username,                                            
                    $password,
                    $email,
                    $dob,
                    $firstname,
                    $lastname,
                    $gender,
                    $address_line_1,
                    $address_line_2,
                    $country,
                    $state,
                    $city,
                    $zipcode,
                    $high_school_info,
                    $college_info,
                    $employer_info,
                    $mobile_no,
                    $profile_pic_path,
                    //$profile_video_path,
                    $user_account_status,
                    $user_type,
                    $submit,
                    $cancel
                )
        );
    }

    public function isValid($data) {
        return true;
        $isValid = parent::isValid($data);
        if( !$isValid ) {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 ) {
                foreach( $arrErrors as $key => $value ) {
                    if( is_array( $value ) && count( $value ) > 0 ) {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                            $objElement->setAttrib('class', $objElement->getAttrib('class') . 'invalid errorclass');
                    }
                }
            }
            return false;
        }
        return true;
    }
} ?>