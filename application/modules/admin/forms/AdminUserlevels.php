<?php
class Admin_Form_AdminUserlevels extends Zend_Form
{
    public function init()
    {
        $this->setMethod('post');
        $this->setAttrib('id','adduserlevels');
        $this->setAttrib('class','form-horizontal span12');
        $this->setAttrib('enctype','multipart/form-data');
        $hidden = new Zend_Form_Element_Hidden(array('name' => 'id', 'id' => 'id'));
        //creating textbox element
        $minimum_coolpoints =new Zend_Form_Element_Text(array('name'=>'minimum_coolpoints','id'=>'minimum_coolpoints','class'=>'required'));
        
        $minimum_coolpoints->setLabel($this->getView()->translate('Minimum Cool Points'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                //->addValidator('Db_NoRecordExists', true, array('table'=>'tbl_user_levels','field'=>'minimum_coolpoints'))
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
        
        // for Minimum Followers
        $minimum_followers =new Zend_Form_Element_Text(array('name'=>'minimum_followers','id'=>'minimum_followers','class'=>'required'));
        $minimum_followers->setLabel($this->getView()->translate('Minimum Followers'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                //->addValidator('Db_NoRecordExists', true, array('table'=>'tbl_user_levels','field'=>'minimum_coolpoints'))
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
        
        // for Minimum Followings
        $minimum_followings =new Zend_Form_Element_Text(array('name'=>'minimum_followings','id'=>'minimum_followings','class'=>'required'));
        $minimum_followings->setLabel($this->getView()->translate('Minimum Followings'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                //->addValidator('Db_NoRecordExists', true, array('table'=>'tbl_user_levels','field'=>'minimum_coolpoints'))
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
        
        
        
        $level_name =new Zend_Form_Element_Text(array('name'=>'level_name','id'=>'level_name','class'=>'required'));
        $level_name->setLabel($this->getView()->translate('Level Name'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                //->addValidator('Db_NoRecordExists', true, array('table'=>'tbl_user_levels','field'=>'level_name'))
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
        $level_no =new Zend_Form_Element_Text(array('name'=>'level_no','id'=>'level_no','class'=>'required'));
        $level_no->setLabel($this->getView()->translate('Level No'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                //->addValidator('Db_NoRecordExists', true, array('table'=>'tbl_user_levels','field'=>'level_no'))
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
		
        $submit = new Zend_Form_Element_Button('submit');
        $submit->setAttrib('class','btn btn-primary')
                ->setAttrib('type','submit');
        $submit->clearDecorators(); //clear all default assigned decorators		
        $submit->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $submit->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box		
        $submit->addDecorator(
                        array('data'=>'HtmlTag'), 
                        array('tag' => 'div', 'class' => 'form-action-area')
                        )  //create html div tag with customized attributes		
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag' => 'div','class'=>'control-group', 'placement' => 'prepand', 'openOnly' => true));    

        $cancel = new Zend_Form_Element_Button('cancel');
        $cancel->setAttrib('class','btn btn-primary')
                        ->setAttrib('onclick','window.location.href="'.SITE_URL.'admin/userlevels/index/"');
        $cancel->clearDecorators(); //clear all default assigned decorators		
        $cancel->addDecorator('ViewHelper'); //use content from library/zend/view/helper/text.php
        $cancel->addDecorator('Errors'); //when validation fails, 'Errors' class will assign for the text box
        $cancel->addDecorator(
        array('data'=>'HtmlTag'), 
        array('tag' => 'div', 'class' => 'form-action-area')
        )  //create html div tag with customized attributes
        ->addDecorator(array('labelDivOpen'=>'HtmlTag'), array('tag'=>'div','class'=>'control-group','closeOnly' => true));
		       
        $this->addElements(
            array(
                    $hidden,
                    $minimum_coolpoints,
                    $minimum_followers,
                    $minimum_followings,
                    $level_name,
                    $level_no,
                    $submit,
                    $cancel
            )
        );
    }

    public function isValid( $data )
    {
        $isValid = parent::isValid( $data );
        if( !$isValid )
        {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 )
            {
                foreach( $arrErrors as $key => $value )
                {
                    if( is_array( $value ) && count( $value ) > 0 )
                    {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                            $objElement->setAttrib( 'class', $objElement->getAttrib( 'class' ) . ' errorclass' );
                    }
                }
            }
            return false;
        }	   
        return true;
    }   	
}?>