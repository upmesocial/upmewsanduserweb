<?php
class Admin_Form_AdminStates extends Zend_Form
{
    public function init()
    {
        //$this->setMethod('post');
        $hidden = new Zend_Form_Element_Hidden(array('name' => 'id', 'id' => 'id'));
        //creating textbox element
        $states =new Zend_Form_Element_Text(array('name'=>'states','id'=>'states','class'=>'required'));
        $states->setLabel($this->getView()->translate('States'))//to display label
                ->setRequired(true)
                ->setAttrib('class','required')
                ->addFilter('StringTrim')
                ->addValidator('NotEmpty')
                ->clearDecorators() //clear all default assigned decorators
                ->addDecorator('ViewHelper') //use content from library/zend/view/helper/text.php
                ->addDecorator('Errors') //when validation fails, 'Errors' class will assign for the text box
                ->addDecorator('Label')
                ->addDecorator(
                array('data'=>'HtmlTag'),
                array('tag' => 'div', 'class' => 'control-group span5')
                )   //create html div tag with customized attributes
                ->addDecorator(array('labelDivOpen' => 'HtmlTag'), array('tag'=>'div','class'=>'clearfix','placement'=>'append','closeOnly'=>false));
		       
        $this->addElements(
            array(
                    $hidden,
                    $states
                    
            )
        );
    }

    public function isValid( $data )
    {
        $isValid = parent::isValid( $data );
        if( !$isValid )
        {
            $arrErrors = parent::getErrors();
            if( is_array( $arrErrors ) && count( $arrErrors ) > 0 )
            {
                foreach( $arrErrors as $key => $value )
                {
                    if( is_array( $value ) && count( $value ) > 0 )
                    {
                        $objElement = parent::getElement( $key );
                        if( $objElement )
                            $objElement->setAttrib( 'class', $objElement->getAttrib( 'class' ) . ' errorclass' );
                    }
                }
            }
            return false;
        }	   
        return true;
    }   	
}