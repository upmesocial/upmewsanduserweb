<?php

class Admin_Model_Cms
{
	protected $_id;
	protected $_page_title;
        protected $_page_content;
	protected $_status;
	
	public function __construct(array $options = null)
	{
            if (is_array($options))
            {
                    $this->setOptions($options);
            }
	}
	public function __set($name, $value)
	{
            $method = 'set' . $name;
            if (('mapper' == $name) || !method_exists($this, $method))
            {
                throw new Exception('Invalid Categories property');
            }
            $this->$method($value);
	}	
	public function __get($name)
	{
            $method = 'get' . $name;
            if (('mapper' == $name) || !method_exists($this, $method))
            {
                throw new Exception('Invalid Categories property');
            }
            return $this->$method();
	}	
	public function setOptions(array $options)
	{
            $methods = get_class_methods($this);
            //echo '<pre>';print_r($options);exit;
            foreach ($options as $key => $value)
            {
                $method = 'set' . ucfirst($key);

                if (in_array($method, $methods))
                {
                    $this->$method($value);
                }
            }
            return $this;
	}
        
	public function setId($id)
	{
            $this->_id = (int) $id;                
            return $this;
	}
	
	public function getId()
	{
            return $this->_id;
	}
        
	public function setPage_title($page_title)
	{
            $this->_page_title = (string) $page_title;
            return $this;
	}
	
	public function getPage_title()
	{
            return $this->_page_title;
	}
        
        public function setPage_content($page_content)
        {
            $this->_page_content = (string) $page_content;
            return $this;
        }
        public function getPage_content()
        {
            return $this->_page_content;
        }
        public function setStatus($status)
	{
            $this->_status = (string) $status;
            return $this;
	}
	public function getStatus()
	{
            return $this->_status;
	}
		
}
