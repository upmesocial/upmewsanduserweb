<?php class Admin_Model_UsersMapper {
    protected $_dbTable;
    protected $_primary = 'users_id';
    //primary key auto-increment? True for yes false for natural key
    protected $_sequence = FAlSE;

    public function setDbTable($dbTable) {
            if (is_string($dbTable)) {
                    $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                    throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
    }

    public function getDbTable() {
            if (null === $this->_dbTable) {
                    $this->setDbTable('Admin_Model_DbTable_Users');
            }
            return $this->_dbTable;
    }

    public function save($data,$id='') {
            try {
                    if($id != '') {
                            $res = $this->getDbTable()->update($data, array('users_id= ?' => $id));
                    } else {
                            $dataObject = (object) $data;
                            $row['username'] = $dataObject->username;
                            if($dataObject->password != ''){            
                                $row['password'] = md5($dataObject->password.SECURITY_SALT);
                            }
                            $row['email'] = $dataObject->email;
                            $row['dob'] = $dataObject->dob;
                            $row['firstname'] = $dataObject->firstname;
                            $row['lastname'] = $dataObject->lastname;
                            $row['gender'] = $dataObject->gender;
                            if($dataObject->address_line_1 !=''){
                                $row['address_line_1'] = $dataObject->address_line_1;
                            }
                            if($dataObject->address_line_2 !=''){
                                $row['address_line_2'] = $dataObject->address_line_2;
                            }
                            $row['country'] = $dataObject->country;
                            $row['state'] = $dataObject->state;
                            $row['city'] = $dataObject->city;
                            $row['zipcode'] = $dataObject->zipcode;
                            if($dataObject->high_school_info !=''){
                                $row['high_school_info'] = $dataObject->high_school_info;
                            }
                            if($dataObject->college_info !=''){
                                $row['college_info'] = $dataObject->college_info;
                            }
                            if($dataObject->employer_info !=''){
                            $row['employer_info'] = $dataObject->employer_info;
                            }
                            $row['mobile_no'] = $dataObject->mobile_no;
                            $row['user_account_status'] = $dataObject->user_account_status;
                            if($dataObject->user_type != ''){
                                $row['user_type'] = $dataObject->user_type;
                            }
                            //echo 'db:<pre>';print_R($row);exit;
                            $res = $this->getDbTable()->insert($row);
                            return $res;
                    }
                    if (false === $res) {
                            return 0;  // bool false returned, query failed
                    } else {
                            return 1;
                    }
            } catch (Zend_Exception $zex){}
    }

    public function find($id) {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                    return;
            }
            return $row = $result->current();
    }
            
    public function fetchAll() {
            $resultSet = $this->getDbTable()->fetchAll(
                            $this->getDbTable()->select()
                                ->joinLeft(array('U'=>'tbl_users'),array('U.*'))
                                ->order("user_id DESC")
                                ->setIntegrityCheck(false)
                        );
            return $resultSet;
    }
        
    public function fetchSortresults($keyword ='',$sort='') {
            //echo $keyword;
            if($sort == 'AO') {
                    $order = "U.firstname ASC";
            } elseif($sort == 'ND') {
                    $order = "U.firstname DESC";
            } elseif($sort == 'NA') {
                    $order = "U.firstname ASC";
            } elseif($sort == 'JD') {
                    $order = "U.created_date DESC";
            } elseif($sort == 'JA') {
                    $order = "U.created_date ASC";
            } elseif($sort == 'SD') {
                    $order = "U.user_account_status DESC";
            } elseif($sort == 'SA') {
                    $order = "U.user_account_status ASC";
            } else {
                    $order = "U.user_id DESC";
            }
            //echo $order;
            if($keyword !='')
                    $where = " U.firstname LIKE '%".addslashes($keyword)."%' or U.lastname LIKE '%".addslashes($keyword)."%' or U.email LIKE '%".addslashes($keyword)."%'";
            else
                $where = "1=1";
            //echo $where;
            $resultSet =  $this->getDbTable()->select()
                            ->from(array('U'=>'tbl_users'),array('U.*'))
                            ->where($where)
                            ->order($order)
                            ->setIntegrityCheck(false);
            //echo $resultSet;exit;
            $resultSet = $this->getDbTable()->fetchAll($resultSet);
            //echo '<pre>';print_r($resultSet->toArray());exit;
            return $resultSet;
    }

    public function fetchRowByUsertID($ID) {
            $resultSet = $this->getDbTable()->fetchRow(
                                $this->getDbTable()->select()
                                ->joinLeft(array('U'=>'tbl_users'),array('U.*'))
                                ->where('U.user_id = "'.$ID.'"')
                                ->setIntegrityCheck(false)
                        );
            return $resultSet;
    }

    public function saveUser(array $data, $id) {
            //convert array to standard object for convience
            $dataObject = (object) $data;
            //the user_id will always exist when we deal with the profile
            $row = $this->find($id);       
            //echo "<pre>".$id;print_r($dataObject);exit;
            $row->username              = $dataObject->username;
            if($dataObject->password != ''){            
                $row->password = md5($dataObject->password.SECURITY_SALT);
            }
            $row->email                 = $dataObject->email;
            $row->dob                   = $dataObject->dob;
            $row->firstname             = $dataObject->firstname;
            $row->lastname              = $dataObject->lastname;
            $row->gender                = $dataObject->gender;
            if($dataObject->address_line_1 !=''){
                $row->address_line_1        = $dataObject->address_line_1;
            }
            if($dataObject->address_line_2 !=''){
                $row->address_line_2        = $dataObject->address_line_2;
            }
            $row->country               = $dataObject->country;
            $row->state                 = $dataObject->state;
            $row->city                  = $dataObject->city;
            $row->zipcode               = $dataObject->zipcode;
            if($dataObject->high_school_info !=''){
            $row->high_school_info      = $dataObject->high_school_info;
            }
            if($dataObject->college_info !=''){
            $row->college_info          = $dataObject->college_info;
            }
            if($dataObject->employer_info !=''){
            $row->employer_info         = $dataObject->employer_info;
            }
            $row->mobile_no             = $dataObject->mobile_no;
            if($dataObject->profile_pic_path !=''){
            $row->profile_pic_path      = $dataObject->profile_pic_path;
            }
            //$row->profile_video_path    = $dataObject->profile_video_path;
            $row->user_account_status   = $dataObject->user_account_status;
            if($dataObject->user_type !=''){
            $row->user_type      = $dataObject->user_type;
            }
            //save or update row
            $row->save($id);
            //return the whole row object, we'll use it to save data to 'profile'
            return $row;
    }
        
    public function delete($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE U.* FROM  tbl_users U WHERE U.user_id = ".$id);
        $id = $select->execute();
        return $id;
    }
    
    public function deleteall($where)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE U.* FROM  tbl_users U WHERE ".$where."");
        $result = $select->execute();
        return $result;
    }
    
    public function update($updatearr, $id)
    {
        $result = $this->getDbTable()->update($updatearr, array('user_id= ?' => $id));
        return $result;
    }
    public function updateall($updatearr, $where)
    {
        $result = $this->getDbTable()->update($updatearr,$where);
        //echo 'Hi<pre>';print_r($result);exit;
        return $result;
    }

    public function getcelebrities() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select    = $this->getDbTable()->select()
                        ->from(array('C'=>'tbl_user_celeb_temp'),array('C.user_id'))
                        ->joinLeft(array('U'=>'tbl_users'),"C.user_id = U.user_id")
                        ->where('U.user_id != ""')
                        ->setIntegrityCheck(false)
                ;
        $resultSet = $db->fetchAll($select);
        return $resultSet;
    }

    public function fetchCelebritySortresults($keyword ='',$sort='') {
            $db = Zend_Db_Table::getDefaultAdapter();
            if($sort == 'AO'){
                    $order = "U.firstname ASC";
                    }
            else{
                    $order = "U.user_id DESC";
                    }
            if($keyword !='')
                    $where = " U.firstname LIKE '%".addslashes($keyword)."%' or U.lastname LIKE '%".addslashes($keyword)."%' or U.email LIKE '%".addslashes($keyword)."%'";
            else
                $where = "1=1 AND U.user_id != ''";
            $resultSet =  $this->getDbTable()->select()
                            ->from(array('C'=>'tbl_user_celeb_temp'),array('C.user_id'))
                            ->joinLeft(array('U'=>'tbl_users'),"C.user_id = U.user_id")
                            ->where($where)
                            ->order($order)
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($resultSet);
            return $resultSet;
    }

    public function fetchtotalusercount() {
            $resultSet = $this->getDbTable()->fetchAll(
                            $this->getDbTable()->select()
                                ->joinLeft(array('U'=>'tbl_users'),array('U.*'))
                                ->order("user_id DESC")
                                ->setIntegrityCheck(false)
                        );
            return COUNT($resultSet);
    }

    public function fetchlastdayusercount($today) {
            $resultSet = $this->getDbTable()->fetchAll(
                            $this->getDbTable()->select()
                                ->joinLeft(array('U'=>'tbl_users'),array('U.*'))
                                ->where("U.created_date < '".$today."'")
                                ->order("user_id DESC")
                                ->setIntegrityCheck(false)
                        );
            return COUNT($resultSet);
    }

    public function fetchtotalcelebritycount() {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('U'=>'tbl_users'),array('U.user_id'))
                            ->where('U.user_type = "C"')
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchlastdaycelebritycount($today) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('U'=>'tbl_users'),array('U.user_id'))
                            ->where('U.user_type = "C"')
                            ->where("U.created_date < '".$today."'")
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchtotalupperscount() {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('C'=>'tbl_coupons'),array('C.coupon_id'))
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchlastdayupperscount($today) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('C'=>'tbl_coupons'),array('C.coupon_id'))
                            ->where("C.created_date < '".$today."'")
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchtotalactiveupperscount() {
            $date = date('Y-m-d H:i:s');
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $this->getDbTable()->select()
                            ->from(array('C'=>'tbl_coupons'),array('C.coupon_id'))
                            ->where("C.expiration_date > '".$date."'")
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchlastdayactiveupperscount($yesterday) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('C'=>'tbl_coupons'),array('C.coupon_id'))
                            ->where("C.expiration_date > '".$yesterday."'")
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchtotalpurchasedupperscount() {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('P'=>'tbl_purchase_coupons'),array('P.purchase_id'))
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchlastdaypurchasedupperscount($today) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('P'=>'tbl_purchase_coupons'),array('P.purchase_id'))
                            ->where("P.purchase_date < '".$today."'")
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchtotalactivepurchasedupperscount() {
            $date = date('Y-m-d H:i:s');
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('P'=>'tbl_purchase_coupons'),array('P.purchase_id'))
                            ->joinLeft(array('C'=>'tbl_coupons'),'P.coupon_id = C.coupon_id','')
                            ->where("C.expiration_date > '".$date."'")
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

    public function fetchlastdayactivepurchasedupperscount($yesterday) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('P'=>'tbl_purchase_coupons'),array('P.purchase_id'))
                            ->joinLeft(array('C'=>'tbl_coupons'),'P.coupon_id = C.coupon_id','')
                            ->where("C.expiration_date > '".$yesterday."'")
                            ->setIntegrityCheck(false);
            $resultSet = $db->fetchAll($select);
            return COUNT($resultSet);
    }

} ?>