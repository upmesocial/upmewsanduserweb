<?php class Admin_Model_Workplaces {
        protected $_work_place_name;
        protected $_work_place_city;
        protected $_popular_order;
        protected $_status;
        protected $_work_place_id;

        public function __construct(array $options = null) {
                if (is_array($options)) {
                        $this->setOptions($options);
                }
        }

        public function __set($name, $value) {
                $method = 'set' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                $this->$method($value);
        }

        public function __get($name) {
                $method = 'get' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                return $this->$method();
        }

        public function setOptions(array $options) {
                $methods = get_class_methods($this);
                foreach ($options as $key => $value) {
                        $method = 'set' . ucfirst($key);
                        if (in_array($method, $methods)) {
                                $this->$method($value);
                        }
                }
                return $this;
        }

        /***** CUSTOM FUNCTIONS FOR SETTING and GETTING FIELD VALUES*************/
        public function setWorkplace_name($work_place_name) {
                $this->_work_place_name = (string) $work_place_name;
                return $this;
        }

        public function getWorkplace_name() {
                return $this->_work_place_name;
        }

        public function setWorkplace_city($work_place_city) {
                $this->_work_place_city = (string) $work_place_city;
                return $this;
        }

        public function getWorkplace_city() {
                return $this->_work_place_city;
        }

        public function setPopularorder($popular_order) {
                $this->_popular_order = (string) $popular_order;
                return $this;
        }

        public function getPopularorder() {
                return $this->_popular_order;
        }

        public function setWorkplace_id($work_place_id) {
                $this->_work_place_id = (int) $work_place_id;
                return $this;
        }

        public function getWorkplace_id() {
                return $this->_work_place_id;
        }

} ?>