<?php
class Admin_Model_AdminMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
            if (is_string($dbTable)) {
                    $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                    throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
    }

    public function getDbTable() {
            if (null === $this->_dbTable) {
                    $this->setDbTable('Admin_Model_DbTable_Admin');
            }
            return $this->_dbTable;
    }

    public function save($data,$id='') {
            if($id !='') {
                    //$this->getDbTable()->update($data, array('id = ?' => $id));
                    try {
                            $res = $this->getDbTable()->update($data, array('id = ?' => $id));
                            if (false === $res) {
                                    return 0;  // bool false returned, query failed
                            } else {
                                    return 1;
                            }
                    } catch (Zend_Exception $e) {

                    } 
            } else
                    $this->getDbTable()->insert($data);
    }

    public function find($id) {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                    return;
            }
            return $row = $result->current();
    }

    public function fetchAll() {
            $resultSet = $this->getDbTable()->fetchAll();
            return $resultSet;
    }

    public function duplicatePasswordCheck($id, $password) {
            $password   = md5($password.SECURITY_SALT);
            $select     = $this->getDbTable()->select()
                            ->from(array('tbl_admin_users'),array('count(id) as Cnt'))
                            ->where("id     =?",$id)
                            ->where("admin_pwd =?",$password);
            $resultSet  = $this->getDbTable()->fetchRow($select);
            return $resultSet['Cnt'];
    }

} ?>