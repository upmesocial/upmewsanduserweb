<?php class Admin_Model_Colleges {
        protected $_college_name;
        protected $_college_city;
        protected $_popular_order;
        protected $_status;
        protected $_college_id;

        public function __construct(array $options = null) {
                if (is_array($options)) {
                        $this->setOptions($options);
                }
        }

        public function __set($name, $value) {
                $method = 'set' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                $this->$method($value);
        }

        public function __get($name) {
                $method = 'get' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                return $this->$method();
        }

        public function setOptions(array $options) {
                $methods = get_class_methods($this);
                foreach ($options as $key => $value) {
                        $method = 'set' . ucfirst($key);
                        if (in_array($method, $methods)) {
                                $this->$method($value);
                        }
                }
                return $this;
        }

        /***** CUSTOM FUNCTIONS FOR SETTING and GETTING FIELD VALUES*************/
        public function setCollege_name($college_name) {
                $this->_college_name = (string) $college_name;
                return $this;
        }

        public function getCollege_name() {
                return $this->_college_name;
        }

        public function setCollege_city($college_city) {
                $this->_college_city = (string) $college_city;
                return $this;
        }

        public function getCollege_city() {
                return $this->_college_city;
        }

        public function setPopularorder($popular_order) {
                $this->_popular_order = (string) $popular_order;
                return $this;
        }

        public function getPopularorder() {
                return $this->_popular_order;
        }

        public function setCollege_id($college_id) {
                $this->_college_id = (int) $college_id;
                return $this;
        }

        public function getCollege_id() {
                return $this->_college_id;
        }

} ?>