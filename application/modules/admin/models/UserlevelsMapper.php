<?php

class Admin_Model_UserlevelsMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Admin_Model_DbTable_Userlevels');
        }
        return $this->_dbTable;
    }

    public function save(Admin_Model_Userlevels $userlevels)
    {
        $data = array('minimum_coolpoints'=>stripslashes($userlevels->getMinimum_coolpoints()),
                        'minimum_followers'=>stripslashes($userlevels->getMinimum_followers()),
                        'minimum_followings'=>stripslashes($userlevels->getMinimum_followings()),
                        'level_name'=>stripslashes($userlevels->getLevel_name()),
                        'level_no'=>stripslashes($userlevels->getLevel_no()),
                    );
        
        if (null === ($id = $userlevels->getId()) || $userlevels->getId() == 0) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
    public function find($id)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        else
            return $row = $result->current();
    }
    public function fetchNameByLevelno($ID) {
            $resultSet = $this->getDbTable()->fetchRow(
                                $this->getDbTable()->select()
                                ->joinLeft(array('U'=>'tbl_user_levels'),array('U.*'))
                                ->where('U.level_no = "'.$ID.'"')
                                ->setIntegrityCheck(false)
                        );
            return $resultSet;
    }
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->select()->order("id ASC");
        return $resultSet;
    }
    public function delete($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE C.* FROM  tbl_user_levels C WHERE C.id = ".$id);
        $id = $select->execute();
        return $id;
    }
    
    public function deleteall($where)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE C.* FROM  tbl_user_levels C WHERE ".$where."");
        $result = $select->execute();
        return $result;
    }
    
    public function update($updatearr, $id)
    {
        $result = $this->getDbTable()->update($updatearr, array('id= ?' => $id));
        return $result;
    }
    public function updateall($updatearr, $where)
    {
        $result = $this->getDbTable()->update($updatearr,$where);
        //echo 'Hi<pre>';print_r($result);exit;
        return $result;
    }
}
?>