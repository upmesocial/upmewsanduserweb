<?php

class Admin_Model_Userlevels
{
    protected $_id;
    protected $_minimum_coolpoints;
    protected $_minimum_followers;
    protected $_minimum_followings;
    protected $_level_name;
    protected $_level_no;
	
    public function __construct(array $options = null)
    {

        if (is_array($options))
        {
            $this->setOptions($options);
        }

    }
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method))
        {
                throw new Exception('Invalid Userlevels property');
        }
        $this->$method($value);
    }	
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method))
        {
                throw new Exception('Invalid Userlevels property');
        }
        return $this->$method();
    }	
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        //echo '<pre>';print_r($options);exit;
        foreach ($options as $key => $value)
        {
            $method = 'set' . ucfirst($key);

            if (in_array($method, $methods))
            {
                $this->$method($value);
            }
        }
        return $this;
    }        
    public function setId($id)
    {
        $this->_id = (int) $id;                
        return $this;
    }	
    public function getId()
    {
        return $this->_id;
    }        
    
    public function setMinimum_coolpoints($minimum_coolpoints)
    {
        $this->_minimum_coolpoints = (string) $minimum_coolpoints;
        return $this;
    }	
    public function getMinimum_coolpoints()
    {
        return $this->_minimum_coolpoints;
    }
    
    public function getMinimum_followers()
    {
        return $this->_minimum_followers;
    }
    public function setMinimum_followers($minimum_followers)
    {
        $this->_minimum_followers = (string) $minimum_followers;
        return $this;
    }
    
    public function getMinimum_followings()
    {
        return $this->_minimum_followings;
    }
    public function setMinimum_followings($minimum_followings)
    {
        $this->_minimum_followings = (string) $minimum_followings;
        return $this;
    }
    
    
    public function setLevel_name($level_name)
    {
        $this->_level_name = (string) $level_name;
        return $this;
    }	
    public function getLevel_name()
    {
        return $this->_level_name;
    }        
    public function setLevel_no($level_no)
    {
        $this->_level_no = (string) $level_no;
        return $this;
    }	
    public function getLevel_no()
    {
        return $this->_level_no;
    }
}
?>