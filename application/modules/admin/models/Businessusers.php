<?php class Admin_Model_Businessusers {
    
        protected $_business_name;
        protected $_description;
        protected $_category_id;
        protected $_username;
        protected $_password;
        protected $_security_question;
        protected $_security_answer;
        protected $_email;
        protected $_website;
        protected $_firstname;
        protected $_lastname;
        protected $_address_line_1;
        protected $_address_line_2;
        protected $_country;
        protected $_state;
        protected $_city;
        protected $_zipcode;
        protected $_phone_no;
        protected $_mobile_no;
        protected $_image_path;
        protected $_business_id;

        public function __construct(array $options = null) {
                if (is_array($options)) {
                        $this->setOptions($options);
                }
        }

        public function __set($name, $value) {
                $method = 'set' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                $this->$method($value);
        }

        public function __get($name) {
                $method = 'get' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                return $this->$method();
        }

        public function setOptions(array $options) {
                $methods = get_class_methods($this);
                foreach ($options as $key => $value) {
                        $method = 'set' . ucfirst($key);
                        if (in_array($method, $methods)) {
                                $this->$method($value);
                        }
                }
                return $this;
        }

        /***** CUSTOM FUNCTIONS FOR SETTING and GETTING FIELD VALUES*************/
        public function setBusiness_name($business_name) {
                $this->_business_name = (string) $business_name;
                return $this;
        }
        public function getBusiness_name() {
                return $this->_business_name;
        }
        public function setDescription($description) {
                $this->_description = (string) $description;
                return $this;
        }
        public function getDescription() {
                return $this->_description;
        }
        public function setCategory_id($category_id) {
                $this->_category_id = (string) $category_id;
                return $this;
        }
        public function getCategory_id() {
                return $this->_category_id;
        } 
        
        public function setUsername($username) {
                $this->_username = (string) $username;
                return $this;
        }
        public function getUsername() {
                return $this->_username;
        }        
        public function setPassword($password) {
                $this->_password = (string) $password;
                return $this;
        }
        public function getPassword() {
                return $this->_password;
        }
        public function setSecurity_question($security_question) {
                $this->_security_question = (string) $security_question;
                return $this;
        }
        public function getSecurity_question() {
                return $this->_security_question;
        }
        public function setSecurity_answer($security_answer) {
                $this->_security_answer = (string) $security_answer;
                return $this;
        }
        public function getSecurity_answer() {
                return $this->_security_answer;
        }
        public function setEmail($email) {
                $this->_email = (string) $email;
                return $this;
        }
        public function getEmail() {
                return $this->_email;
        }
        public function setWebsite($website) {
                $this->_website = (string) $website;
                return $this;
        }
        public function getWebsite() {
                return $this->_website;
        }
        public function setFirstname($firstname) {
                $this->_firstname = (string) $firstname;
                return $this;
        }
        public function getFirstname() {
                return $this->_firstname;
        }
        public function setLastname($lastname) {
                $this->_lastname = (string) $lastname;
                return $this;
        }
        public function getLastname() {
                return $this->_lastname;
        }
        public function setAddress_line_1($address_line_1) {
                $this->_address_line_1 = (string) $address_line_1;
                return $this;
        }
        public function getAddress_line_1() {
                return $this->_address_line_1;
        }
        public function setAddress_line_2($address_line_2) {
                $this->_address_line_2 = (string) $address_line_2;
                return $this;
        }
        public function getAddress_line_2() {
                return $this->_address_line_2;
        }
        public function setCountry($country) {
                $this->_country = (string) $country;
                return $this;
        }
        public function getCountry() {
                return $this->_country;
        }
        public function setState($state) {
                $this->_state = (string) $state;
                return $this;
        }
        public function getState() {
                return $this->_state;
        }
        public function setCity($city) {
                $this->_city = (string) $city;
                return $this;
        }
        public function getCity() {
                return $this->_city;
        }
        public function setZipcode($zipcode) {
                $this->_zipcode = (string) $zipcode;
                return $this;
        }
        public function getZipcode() {
                return $this->_zipcode;
        }
        public function setPhone_no($phone_no) {
                $this->_phone_no= (string) $phone_no;
                return $this;
        }
        public function getPhone_no() {
                return $this->_phone_no;
        }
        public function setMobile_no($mobile_no) {
                $this->_mobile_no= (string) $mobile_no;
                return $this;
        }
        public function getMobile_no() {
                return $this->_mobile_no;
        }
        public function setImage_path($image_path) {
                $this->_image_path = (string) $image_path;
                return $this;
        }
        public function getImage_path() {
                return $this->_image_path;
        }
        public function setBusiness_id($business_id) {
                $this->_business_id = (int) $business_id;
                return $this;
        }
        public function getBusiness_id() {
                return $this->_business_id;
        }
}
?>