<?php class Admin_Model_WorkplacesMapper {
        protected $_dbTable;
        protected $_primary = 'work_place_id';
        //primary key auto-increment? True for yes false for natural key
        protected $_sequence = FAlSE;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Admin_Model_DbTable_Workplaces');
                }
                return $this->_dbTable;
        }

        public function save($data,$id='') {
                try {
                        if($id != '') {
                                $res = $this->getDbTable()->update($data, array('work_place_id= ?' => $id));
                        } else {
                                $res = $this->getDbTable()->insert($data);
                        }
                        if (false === $res) {
                                return 0;  // bool false returned, query failed
                        } else {
                                return 1;
                        }
                } catch (Zend_Exception $zex){}
        }

        public function find($id) {
                $result = $this->getDbTable()->find($id);
                if (0 == count($result)) {
                        return;
                }
                return $row = $result->current();
        }

        public function fetchAll() {
                $resultSet = $this->getDbTable()->fetchAll(
                                $this->getDbTable()->select()
                                    ->joinLeft(array('W'=>'tbl_work_place'),array('W.*'))
                                    ->order("W.work_place_id DESC")
                                    ->setIntegrityCheck(false)
                            );
                return $resultSet;
        }

        public function fetchRowByWorkplaceID($ID) {
                $resultSet = $this->getDbTable()->fetchRow(
                                    $this->getDbTable()->select()
                                    ->joinLeft(array('W'=>'tbl_work_place'),array('W.*'))
                                    ->where('W.work_place_id = "'.$ID.'"')
                                    ->setIntegrityCheck(false)
                            );
                return $resultSet;
        }

        public function fetchSortresults($keyword ='',$sort='') {
                if($sort == 'AO') {
                        $order = "W.work_place_name ASC";
                } else {
                        $order = "W.work_place_id DESC";
                }
                if($keyword !='')
                        $where = " W.work_place_name LIKE '%".addslashes($keyword)."%'";
                else
                    $where = "1=1";
                //echo $where;
                $resultSet =  $this->getDbTable()->select()
                                ->from(array('W'=>'tbl_work_place'),array('W.*'))
                                ->where($where)
                                ->order($order)
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($resultSet);
                return $resultSet;
        }

        public function delete($id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE W.* FROM tbl_work_place W WHERE W.work_place_id = ".$id);
                $id = $select->execute();
                return $id;
        }

        public function deleteall($where) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE W.* FROM tbl_work_place W WHERE ".$where."");
                $result = $select->execute();
                return $result;
        }

        public function update($updatearr, $id) {
                $result = $this->getDbTable()->update($updatearr, array('work_place_id= ?' => $id));
                return $result;
        }

        public function updateall($updatearr, $where) {
                $result = $this->getDbTable()->update($updatearr,$where);
                return $result;
        }

} ?>