<?php
class Admin_Model_HelpVideosMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
            if (is_string($dbTable)) {
                    $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                    throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
    }

    public function getDbTable() {
            if (null === $this->_dbTable) {
                    $this->setDbTable('Admin_Model_DbTable_HelpVideos');
            }
            return $this->_dbTable;
    }

    public function save($helpvideos) {
            //echo '<pre>';print_r($helpvideos);exit;
            $data = array('iphone_path'=>stripslashes($helpvideos['iphone_path']),
                        'android_path'=>stripslashes($helpvideos['android_path']),
                        'web_path'=>stripslashes($helpvideos['web_path']),
                        'status'=>$helpvideos['status'],
                        'created_date' => date('Y-m-d H:m:s')
                    );
            
            if (null === ($id = $helpvideos['id']) || $helpvideos['id'] == 0) {
                    unset($data['id']);
                    $this->getDbTable()->insert($data);
            } else {
                    $this->getDbTable()->update($data, array('id = ?' => $id));
            }
    }

    public function find($id) {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                    return;
            } else
                    return $row = $result->current();
    }

    public function fetchAll() {
            $resultSet = $this->getDbTable()->select()->order("id DESC");
            return $resultSet;
    }
    
    public function fetchRowByHelpVideoID($ID) {
            $resultSet = $this->getDbTable()->fetchRow(
                                $this->getDbTable()->select()
                                ->where('id = "'.$ID.'"')
                                ->setIntegrityCheck(false)
                        );
            return $resultSet;
    }
    
    public function fetchSortresults($keyword ='',$sort='') {
            if($sort == 'AO') {
                    $order = "H.id ASC";
            } else {
                    $order = "H.id DESC";
            }
            if($keyword !='')
                    $where = " H.iphone_path LIKE '%".addslashes($keyword)."%' OR H.android_path LIKE '%".addslashes($keyword)."%' OR H.web_path LIKE '%".addslashes($keyword)."%'";
            else
                    $where = "1=1";
            $resultSet = $this->getDbTable()->select()
                            ->from(array('H'=>'tbl_help_videos'),array('H.*'))
                            ->where($where)
                            ->order($order)
                            ->setIntegrityCheck(false);
            $resultSet = $this->getDbTable()->fetchAll($resultSet);
            return $resultSet;
    }
    
    public function delete($id) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->query("DELETE FROM tbl_help_videos H WHERE H.id = ".$id);
            $id = $select->execute();
            return $id;
    }

    public function deleteall($where) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->query("DELETE FROM tbl_help_videos H WHERE ".$where."");
            $result = $select->execute();
            return $result;
    }

    public function update($updatearr, $id) {
            $result = $this->getDbTable()->update($updatearr, array('id= ?' => $id));
            return $result;
    }

    public function updateall($updatearr, $where) {
            $result = $this->getDbTable()->update($updatearr,$where);
            return $result;
    }

}