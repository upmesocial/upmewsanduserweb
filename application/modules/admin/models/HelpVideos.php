<?php
class Admin_Model_HelpVideos {
	protected $_id;
	protected $_iphone_path;
        protected $_android_path;
        protected $_web_path;
        protected $_created_date;
        protected $_status;

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

        public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Help Video Property');
		}
		$this->$method($value);
	}

        public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Help Video Property');
		}
		return $this->$method();
	}

        public function setOptions(array $options) {
		$methods = get_class_methods($this);
                foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setIphonePath($iphone_path) {
		$this->_iphone_path = (string) $iphone_path;
		return $this;
	}

	public function getIphonePath() {
		return $this->_iphone_path;
	}

        public function setAndroidPath($android_path) {
		$this->_android_path = (string) $android_path;
		return $this;
	}

	public function getAndroidPath() {
		return $this->_android_path;
	}

        public function setWebPath($web_path) {
		$this->_web_path = (string) $web_path;
		return $this;
	}

	public function getWebPath() {
		return $this->_web_path;
	}

	public function setStatus($status) {
		$this->_status = (string) $status;
		return $this;
	}

	public function getStatus() {
		return $this->_status;
	}

}