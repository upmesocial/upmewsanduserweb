<?php

class Admin_Model_CmsMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Admin_Model_DbTable_Cms');
        }
        return $this->_dbTable;
    }

    public function save(Admin_Model_Cms $cms)
    {
        $data = array('page_title'=>stripslashes($cms->getPage_title()),
                        'page_content'=>stripslashes($cms->getPage_content()),
                    'status'=>$cms->getStatus(),
                    );        
        if (null === ($id = $cms->getId()) || $cms->getId() == 0) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
    public function find($id)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        else
            return $row = $result->current();
    }
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->select()->order("id ASC");
        return $resultSet;
    }
    
    public function delete($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE C.* FROM  tbl_cms_pages C WHERE C.id = ".$id);
        $id = $select->execute();
        return $id;
    }
    
    public function deleteall($where)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE C.* FROM  tbl_cms_pages C WHERE ".$where."");
        $result = $select->execute();
        return $result;
    }
    
    public function update($updatearr, $id)
    {
        $result = $this->getDbTable()->update($updatearr, array('id= ?' => $id));
        return $result;
    }
    public function updateall($updatearr, $where)
    {
        $result = $this->getDbTable()->update($updatearr,$where);
        //echo 'Hi<pre>';print_r($result);exit;
        return $result;
    }
}

