<?php
class Admin_Model_Admin {

        protected $_user_name;
        protected $_admin_pwd;
        protected $_admin_email;
        protected $_admin_name;
        protected $_id;

        public function __construct(array $options = null) {
                if (is_array($options)) {
                        $this->setOptions($options);
                }
        }

        public function __set($name, $value) {
                $method = 'set' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                $this->$method($value);
        }

        public function __get($name) {
                $method = 'get' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                return $this->$method();
        }

        public function setOptions(array $options) {
                $methods = get_class_methods($this);
                foreach ($options as $key => $value) {
                        $method = 'set' . ucfirst($key);
                        if (in_array($method, $methods)) {
                                $this->$method($value);
                        }
                }
                return $this;
        }

        /***** CUSTOM FUNCTIONS FOR SETTING and GETTING FIELD VALUES*************/
        //////////// FOR USERNAME //////////////
        public function setUser_name($user_name) {
                $this->_user_name = (string) $user_name;
                return $this;
        }

        public function getUser_name() {
                return $this->_user_name;
        }

        //////////// FOR PASSWORD //////////////
        public function setAdmin_pwd($admin_pwd) {
                $this->_admin_pwd = (string) $admin_pwd;
                return $this;
        }

        public function getAdmin_pwd() {
                return $this->_admin_pwd;
        }

        //////////// FOR ADMIN EMAIL //////////////
        public function setAdmin_email($admin_email) {
                $this->_admin_email = (string) $admin_email;
                return $this;
        }

        public function getAdmin_email() {
                return $this->_admin_email;
        }

        //////////// FOR ADMIN EMAIL //////////////
        public function setAdmin_name($admin_name) {
                $this->_admin_name = (string) $admin_name;
                return $this;
        }

        public function getAdmin_name() {
                return $this->_admin_name;
        }

        //////////  FOR ADMIN STATUS //////////////////
        public function setStatus($status) {
                $this->_status= (string) $status;
                return $this;
        }

        public function getStatus() {
                return $this->_status;
        }

        ////////// FOR ADMIN ID //////////// getCreatedDate
        public function setId($id) {
                $this->_id = (int) $id;
                return $this;
        }

        public function getId() {
                return $this->_id;
        }

}