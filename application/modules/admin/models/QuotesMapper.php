<?php
class Admin_Model_QuotesMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
            if (is_string($dbTable)) {
                    $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                    throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
    }

    public function getDbTable() {
            if (null === $this->_dbTable) {
                    $this->setDbTable('Admin_Model_DbTable_Quotes');
            }
            return $this->_dbTable;
    }

    public function save(Admin_Model_Quotes $quotes) {
            $data = array('quote'=>stripslashes($quotes->getQuote()),
                        'status'=>$quotes->getStatus(),
                        'created_date' => date('Y-m-d')
                    );
            if (null === ($id = $quotes->getId()) || $quotes->getId() == 0) {
                    unset($data['id']);
                    $this->getDbTable()->insert($data);
            } else {
                    $this->getDbTable()->update($data, array('id = ?' => $id));
            }
    }

    public function find($id) {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                    return;
            } else
                    return $row = $result->current();
    }

    public function fetchAll() {
            $resultSet = $this->getDbTable()->select()->order("quote ASC");
            return $resultSet;
    }
    
    public function fetchRowByQuoteID($ID) {
            $resultSet = $this->getDbTable()->fetchRow(
                                $this->getDbTable()->select()
                                ->where('id = "'.$ID.'"')
                                ->setIntegrityCheck(false)
                        );
            return $resultSet;
    }
    
    public function fetchSortresults($keyword ='',$sort='') {
            if($sort == 'AO') {
                    $order = "Q.quote ASC";
            } else {
                    $order = "Q.id DESC";
            }
            if($keyword !='')
                    $where = " Q.quote LIKE '%".addslashes($keyword)."%'";
            else
                    $where = "1=1";
            $resultSet = $this->getDbTable()->select()
                            ->from(array('Q'=>'tbl_quotes'),array('Q.*'))
                            ->where($where)
                            ->order($order)
                            ->setIntegrityCheck(false);
            $resultSet = $this->getDbTable()->fetchAll($resultSet);
            return $resultSet;
    }
    
    public function delete($id) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->query("DELETE FROM tbl_quotes Q WHERE Q.id = ".$id);
            $id = $select->execute();
            return $id;
    }

    public function deleteall($where) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->query("DELETE FROM tbl_quotes Q WHERE ".$where."");
            $result = $select->execute();
            return $result;
    }

    public function update($updatearr, $id) {
            $result = $this->getDbTable()->update($updatearr, array('id= ?' => $id));
            return $result;
    }

    public function updateall($updatearr, $where) {
            $result = $this->getDbTable()->update($updatearr,$where);
            return $result;
    }

}