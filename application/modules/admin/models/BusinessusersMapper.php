<?php class Admin_Model_BusinessusersMapper {
    
    protected $_dbTable;
    protected $_primary = 'business_id';
    //primary key auto-increment? True for yes false for natural key
    protected $_sequence = FAlSE;

    public function setDbTable($dbTable) {
            if (is_string($dbTable)) {
                    $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                    throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
    }

    public function getDbTable() {
            if (null === $this->_dbTable) {
                    $this->setDbTable('Admin_Model_DbTable_Businessusers');
            }
            return $this->_dbTable;
    }

    public function save($data,$id='') {
            if($id !='') {
                    $this->getDbTable()->update($data, array('business_id = ?' => $id));
            } else
                    $this->getDbTable()->insert($data);
    }

    public function find($id) {
            $result = $this->getDbTable()->find($id);
            if (0 == count($result)) {
                    return;
            }
            return $row = $result->current();
    }
            
    public function fetchAll() {
            $resultSet = $this->getDbTable()->fetchAll(
                                $this->getDbTable()->select()
                                        ->joinLeft(array('U'=>'tbl_business_users'),array('U.*'))
                                        ->order("U.business_id DESC")
                                        ->setIntegrityCheck(false)
                        );
            return $resultSet;
    }

    public function fetchRowByBusinessuserID($ID) {
            $resultSet = $this->getDbTable()->fetchRow(
                                $this->getDbTable()->select()
                                ->joinLeft(array('U'=>'tbl_business_users'),array('U.*'))
                                ->where('U.business_id = "'.$ID.'"')
                                ->setIntegrityCheck(false)
                        );
            //echo '<pre>';print_r($resultSet);exit;
            return $resultSet;
    }

    public function getreviewscntbyid($id) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('R'=>'tbl_business_review'),array('R.*'))
                            ->where("to_user_id =?",$id)
                            ->setIntegrityCheck(false)
                    ;
            $resultSet = $db->fetchAll($select);
            return count($resultSet);
        }

    public function getfollowerscntbyid($id) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select    = $this->getDbTable()->select()
                            ->from(array('F'=>'tbl_business-user_followers'),array('F.*'))
                            ->where("business_id =?",$id)
                            ->setIntegrityCheck(false)
                    ;
            $resultSet = $db->fetchAll($select);
            return count($resultSet);
        }

    public function fetchSortresults($keyword ='',$sort='') {
            if($sort == 'AO'){
                    $order = "B.firstname ASC";
            }else{
                    $order = "B.business_id DESC";
            }
            if($keyword !='')
                    $where = " B.firstname LIKE '%".addslashes($keyword)."%' or B.lastname LIKE '%".addslashes($keyword)."%' or B.email LIKE '%".addslashes($keyword)."%'";
            else
                $where = "1=1";
            //echo $where;
            $resultSet =  $this->getDbTable()->select()
                            ->from(array('B'=>'tbl_business_users'),array('B.*'))
                            ->where($where)
                            ->order($order)
                            ->setIntegrityCheck(false);
            //echo $resultSet;exit;
            $resultSet = $this->getDbTable()->fetchAll($resultSet);
            //echo '<pre>';print_r($resultSet->toArray());exit;
            return $resultSet;
    }

    public function saveUser(array $data, $id) {
            //convert array to standard object for convience
            $dataObject = (object) $data;
            //the user_id will always exist when we deal with the profile
            $row = $this->find($id);       
            //echo "<pre>".$id;print_r($dataObject);exit;
            $row->business_name         = $dataObject->business_name;
            $row->description           = $dataObject->description;
            $row->type                  = $dataObject->type;
            $row->category_id           = $dataObject->category_id;
            $row->username              = $dataObject->username;
            if($dataObject->password!=''){
            $row->password              = $dataObject->password;}
            $row->security_question     = $dataObject->security_question;
            $row->security_answer       = $dataObject->security_answer;
            $row->email                 = $dataObject->email;
            $row->website               = $dataObject->website;
            $row->firstname             = $dataObject->firstname;
            $row->lastname              = $dataObject->lastname;
            if($dataObject->address_line_1!=''){
            $row->address_line_1        = $dataObject->address_line_1;}
            if($dataObject->address_line_2!=''){
            $row->address_line_2        = $dataObject->address_line_2;}
            $row->country               = $dataObject->country;
            $row->state                 = $dataObject->state;
            $row->city                  = $dataObject->city;
            $row->zipcode               = $dataObject->zipcode;
            $row->phone_no              = $dataObject->phone_no;
            $row->mobile_no             = $dataObject->mobile_no;
            if($dataObject->image_path!=''){
            $row->image_path            = $dataObject->image_path;}
            //save or update row
            $row->save($id);
            //return the whole row object, we'll use it to save data to 'profile'
            return $row;
    }

    public function delete($id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE B.* FROM  tbl_business_users B WHERE B.business_id = ".$id);
        $id = $select->execute();
        return $id;
    }

    public function deleteall($where) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE B.* FROM  tbl_business_users B WHERE ".$where."");
        $result = $select->execute();
        return $result;
    }
    
    public function update($updatearr, $id) {
        $result = $this->getDbTable()->update($updatearr, array('business_id= ?' => $id));
        return $result;
    }

    public function updateall($updatearr, $where) {
        $result = $this->getDbTable()->update($updatearr,$where);
        return $result;
    }

    public function fetchtotalbusinessusercount() {
            $resultSet = $this->getDbTable()->fetchAll(
                                $this->getDbTable()->select()
                                        ->joinLeft(array('U'=>'tbl_business_users'),array('U.*'))
                                        ->order("U.business_id DESC")
                                        ->setIntegrityCheck(false)
                        );
            return COUNT($resultSet);
    }

    public function fetchlastdaybusinessusercount($today) {
            $resultSet = $this->getDbTable()->fetchAll(
                            $this->getDbTable()->select()
                                ->joinLeft(array('U'=>'tbl_business_users'),array('U.*'))
                                ->where("U.business_created_date < '".$today."'")
                                ->order("business_id DESC")
                                ->setIntegrityCheck(false)
                        );
            return COUNT($resultSet);
    }

} ?>