<?php

class Admin_Model_CategoriesMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Admin_Model_DbTable_Categories');
        }
        return $this->_dbTable;
    }

    public function save(Admin_Model_Categories $categories)
    {
        $data = array('category_name'=>stripslashes($categories->getCategory_name()),
                    'status'=>$categories->getStatus(),
                    );        
        if (null === ($id = $categories->getId()) || $categories->getId() == 0) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
    public function find($id)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        else
            return $row = $result->current();
    }
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->select()->order("category_name ASC");
        return $resultSet;
    }
    
    public function fetchRowByCategoryID($ID) {
            $resultSet = $this->getDbTable()->fetchRow(
                                $this->getDbTable()->select()
                                ->joinLeft(array('B'=>'tbl_business_categories'),array('B.*'))
                                ->where('B.id = "'.$ID.'"')
                                ->setIntegrityCheck(false)
                        );
            return $resultSet;
    }
    
    public function fetchSortresults($keyword ='',$sort='') {
                //echo $keyword;
                if($sort == 'AO'){
                        $order = "C.category_name ASC";
                        }
                else{
                        $order = "C.id DESC";
                        }
                //echo $order;
                if($keyword !='')
                        $where = " C.category_name LIKE '%".addslashes($keyword)."%'";
                else
                    $where = "1=1";
                //echo $where;
                $resultSet =  $this->getDbTable()->select()
                                ->from(array('C'=>'tbl_business_categories'),array('C.*'))
                                ->where($where)
                                ->order($order)
                                ->setIntegrityCheck(false);
                //echo $resultSet;exit;
                $resultSet = $this->getDbTable()->fetchAll($resultSet);
                //echo '<pre>';print_r($resultSet->toArray());exit;
                return $resultSet;
        }
    
    public function delete($id)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE C.* FROM  tbl_business_categories C WHERE C.id = ".$id);
        $id = $select->execute();
        return $id;
    }
    
    public function deleteall($where)
    {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->query("DELETE C.* FROM  tbl_business_categories C WHERE ".$where."");
        $result = $select->execute();
        return $result;
    }
    
    public function update($updatearr, $id)
    {
        $result = $this->getDbTable()->update($updatearr, array('id= ?' => $id));
        return $result;
    }
    public function updateall($updatearr, $where)
    {
        $result = $this->getDbTable()->update($updatearr,$where);
        //echo 'Hi<pre>';print_r($result);exit;
        return $result;
    }
}

