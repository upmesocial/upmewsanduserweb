<?php

class Admin_Model_Categories
{
	protected $_id;
	protected $_category_name;
	protected $_status;
	
	public function __construct(array $options = null)
	{
            
		if (is_array($options))
		{
                    
			$this->setOptions($options);
		}
                
	}
	public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method))
		{
			throw new Exception('Invalid Categories property');
		}
		$this->$method($value);
	}	
	public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method))
		{
			throw new Exception('Invalid Categories property');
		}
		return $this->$method();
	}	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
                //echo '<pre>';print_r($options);exit;
		foreach ($options as $key => $value)
		{
			$method = 'set' . ucfirst($key);
                        
			if (in_array($method, $methods))
			{
				$this->$method($value);
			}
		}
		return $this;
	}
        
	public function setId($id)
	{
		$this->_id = (int) $id;                
		return $this;
	}
	
	public function getId()
	{
		return $this->_id;
	}
        
	public function setCategory_name($category_name)
	{
            
		$this->_category_name = (string) $category_name;
		return $this;
	}
	
	public function getCategory_name()
	{
		return $this->_category_name;
	}
        
	public function setStatus($status)
	{
		$this->_status = (string) $status;
		return $this;
	}
	
	public function getStatus()
	{
		return $this->_status;
	}
		
}
