<?php
class Admin_Model_Quotes {
	protected $_id;
	protected $_quote;
	protected $_status;

	public function __construct(array $options = null) {
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

        public function __set($name, $value) {
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Quotes property');
		}
		$this->$method($value);
	}

        public function __get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Quotes property');
		}
		return $this->$method();
	}

        public function setOptions(array $options) {
		$methods = get_class_methods($this);
                foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setId($id) {
		$this->_id = (int) $id;
		return $this;
	}

	public function getId() {
		return $this->_id;
	}

	public function setQuote($quote) {
		$this->_quote = (string) $quote;
		return $this;
	}

	public function getQuote() {
		return $this->_quote;
	}

	public function setStatus($status) {
		$this->_status = (string) $status;
		return $this;
	}

	public function getStatus() {
		return $this->_status;
	}

}