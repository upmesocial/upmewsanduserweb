<?php class Admin_Model_Schools {
        protected $_school_name;
        protected $_school_city;
        protected $_popular_order;
        protected $_status;
        protected $_school_id;

        public function __construct(array $options = null) {
                if (is_array($options)) {
                        $this->setOptions($options);
                }
        }

        public function __set($name, $value) {
                $method = 'set' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                $this->$method($value);
        }

        public function __get($name) {
                $method = 'get' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                return $this->$method();
        }

        public function setOptions(array $options) {
                $methods = get_class_methods($this);
                foreach ($options as $key => $value) {
                        $method = 'set' . ucfirst($key);
                        if (in_array($method, $methods)) {
                                $this->$method($value);
                        }
                }
                return $this;
        }

        /***** CUSTOM FUNCTIONS FOR SETTING and GETTING FIELD VALUES*************/
        public function setSchool_name($school_name) {
                $this->_school_name = (string) $school_name;
                return $this;
        }

        public function getSchool_name() {
                return $this->_school_name;
        }

        public function setSchool_city($school_city) {
                $this->_school_city = (string) $school_city;
                return $this;
        }

        public function getSchool_city() {
                return $this->_school_city;
        }

        public function setPopularorder($popular_order) {
                $this->_popular_order = (string) $popular_order;
                return $this;
        }

        public function getPopularorder() {
                return $this->_popular_order;
        }

        public function setSchool_id($school_id) {
                $this->_school_id = (int) $school_id;
                return $this;
        }

        public function getSchool_id() {
                return $this->_school_id;
        }

} ?>