<?php

class Admin_Model_Coolpoints
{
	protected $_id;
	protected $_name;
	protected $_points;
	
	public function __construct(array $options = null)
	{
            
		if (is_array($options))
		{
                    
			$this->setOptions($options);
		}
                
	}
	public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method))
		{
			throw new Exception('Invalid Categories property');
		}
		$this->$method($value);
	}	
	public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method))
		{
			throw new Exception('Invalid Coolpoints property');
		}
		return $this->$method();
	}	
	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
                //echo '<pre>';print_r($options);exit;
		foreach ($options as $key => $value)
		{
			$method = 'set' . ucfirst($key);
                        
			if (in_array($method, $methods))
			{
				$this->$method($value);
			}
		}
		return $this;
	}
        
	public function setId($id)
	{
		$this->_id = (int) $id;                
		return $this;
	}
	
	public function getId()
	{
		return $this->_id;
	}
        
	public function setName($name)
	{
            
		$this->_name = (string) $name;
		return $this;
	}
	
	public function getName()
	{
		return $this->_name;
	}
        
	public function setPoints($points)
	{
		$this->_points = (string) $points;
		return $this;
	}
	
	public function getPoints()
	{
		return $this->_points;
	}
		
}
