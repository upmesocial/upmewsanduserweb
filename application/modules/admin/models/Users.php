<?php class Admin_Model_Users {
    
        protected $_username;
        protected $_password;
        protected $_email;
        protected $_dob;
        protected $_gender;
        protected $_firstname;
        protected $_lastname;
        protected $_gender;
        protected $_address_line_1;
        protected $_address_line_2;
        protected $_country;
        protected $_state;
        protected $_city;
        protected $_zipcode;
        protected $_high_school_info;
        protected $_college_info;
        protected $_employer_info;
        protected $_mobile_no;
        protected $_profile_pic_path;
        protected $_profile_video_path;
        protected $_user_account_status;
        protected $_user_type;
        protected $_id;

        public function __construct(array $options = null) {
                if (is_array($options)) {
                        $this->setOptions($options);
                }
        }

        public function __set($name, $value) {
                $method = 'set' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                $this->$method($value);
        }

        public function __get($name) {
                $method = 'get' . $name;
                if (('mapper' == $name) || !method_exists($this, $method)) {
                        throw new Exception('Invalid Admin property');
                }
                return $this->$method();
        }

        public function setOptions(array $options) {
                $methods = get_class_methods($this);
                foreach ($options as $key => $value) {
                        $method = 'set' . ucfirst($key);
                        if (in_array($method, $methods)) {
                                $this->$method($value);
                        }
                }
                return $this;
        }

        /***** CUSTOM FUNCTIONS FOR SETTING and GETTING FIELD VALUES*************/
        ////////// FOR FULL NAME //////////
        public function setUsername($username) {
                $this->_username = (string) $username;
                return $this;
        }

        public function getUsername() {
                return $this->_username;
        }        

        public function setPassword($password) {
                $this->_password = (string) $password;
                return $this;
        }

        public function getPassword() {
                return $this->_password;
        }

        public function setEmail($email) {
                $this->_email = (string) $email;
                return $this;
        }

        public function getEmail() {
                return $this->_email;
        }

        public function setDob($dob) {
                $this->_dob = (string) $dob;
                return $this;
        }

        public function getDob() {
                return $this->_dob;
        }

        public function setFirstname($firstname) {
                $this->_firstname = (string) $firstname;
                return $this;
        }

        public function getFirstname() {
                return $this->_firstname;
        }

        public function setLastname($lastname) {
                $this->_lastname = (string) $lastname;
                return $this;
        }

        public function getLastname() {
                return $this->_lastname;
        }  

        public function setGender($gender) {
                $this->_gender = (string) $gender;
                return $this;
        }

        public function getGender() {
                return $this->_gender;
        }

        public function setAddress_line_1($address_line_1) {
                $this->_address_line_1 = (string) $address_line_1;
                return $this;
        }

        public function getAddress_line_1() {
                return $this->_address_line_1;
        }

        public function setAddress_line_2($address_line_2) {
                $this->_address_line_2 = (string) $address_line_2;
                return $this;
        }

        public function getAddress_line_2() {
                return $this->_address_line_2;
        }

        public function setCountry($country) {
                $this->_country = (string) $country;
                return $this;
        }

        public function getCountry() {
                return $this->_country;
        }

        public function setState($state) {
                $this->_state = (string) $state;
                return $this;
        }

        public function getState() {
                return $this->_state;
        }

        public function setCity($city) {
                $this->_city = (string) $city;
                return $this;
        }

        public function getCity() {
                return $this->_city;
        }

        //////////  FOR HomeTown //////////
        public function setZipcode($zipcode) {
                $this->_zipcode = (string) $zipcode;
                return $this;
        }

        public function getZipcode() {
                return $this->_zipcode;
        }


        public function setHigh_school_info($high_school_info) {
                $this->_high_school_info= (string) $high_school_info;
                return $this;
        }

        public function getHigh_school_info() {
                return $this->_high_school_info;
        }

        public function setCollege_info($college_info) {
                $this->_college_info= (string) $college_info;
                return $this;
        }

        public function getCollege_info() {
                return $this->_college_info;
        }

        public function setEmployer_info($employer_info) {
                $this->_employer_info= (string) $employer_info;
                return $this;
        }

        public function getEmployer_info() {
                return $this->_employer_info;
        }

        public function setMobile_no($mobile_no) {
                $this->_mobile_no= (string) $mobile_no;
                return $this;
        }

        public function getMobile_no() {
                return $this->_mobile_no;
        }

        public function setProfile_pic_path($profile_pic_path) {
                $this->_profile_pic_path= (string) $profile_pic_path;
                return $this;
        }

        public function getProfile_pic_path() {
                return $this->_profile_pic_path;
        }

        public function setProfile_video_path($profile_video_path) {
                $this->_profile_video_path= (string) $profile_video_path;
                return $this;
        }

        public function getProfile_video_path() {
                return $this->_profile_video_path;
        }

        public function setUser_account_status($user_account_status) {
                $this->_user_account_status= (string) $user_account_status;
                return $this;
        }

        public function getUser_account_status() {
                return $this->_user_account_status;
        }

        public function setUser_type($user_type) {
                $this->_user_type= (string) $user_type;
                return $this;
        }

        public function getUser_type() {
                return $this->_user_type;
        }

        public function setId($id) {
                $this->_id = (int) $id;
                return $this;
        }

        public function getId() {
                return $this->_id;
        }

} ?>