<?php
class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initAppAutoload()
    {

        $autoloader = new Zend_Application_Module_Autoloader(array(
                'namespace' => 'Admin_',
                'basePath'  => APPLICATION_PATH .'/modules/admin',
                'resourceTypes' => array (
                        'form' => array(
                                        'path'      => 'forms',
                                        'namespace' => 'Form',
                                        ),
                'model' => array(
                                'path' => 'models',
                                'namespace' => 'Model',
                                ),
            )
        ));
        /*$autoloader = new Zend_Application_Module_Autoloader(array(
        'namespace' => 'App',
        'basePath' => dirname(__FILE__),
        )); */
        return $autoloader;
    }

    public function run()
    {
        Zend_Registry::set('config', new Zend_Config($this->getOptions()));
        //start the sessions
        Zend_Session::start();
        parent::run();
    }

}
?>

