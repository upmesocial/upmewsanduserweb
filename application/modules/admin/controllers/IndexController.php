<?php
class Admin_IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        
        if($_SERVER['REMOTE_ADDR']  == '192.168.1.57'){
            //echo "<pre>";print_r($this->getRequest()->getParams());exit;
        }
        
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if ($auth->hasIdentity()) {
            $this->_helper->redirector('dashboard');
        } else {
            if ('index' != $this->getRequest()->getActionName()) {
                    $this->_helper->redirector('index');
            }
        }
        $this->view->assign('page_Name',"login");
        $db         = $this->_getParam('db');
        $form = new Admin_Form_AdminLogin();
        $request    = $this->getRequest();
        $req = $request->getParam('req');
        if($request->isPost()) {
            $adapter = new Zend_Auth_Adapter_DbTable(
                $db,
                'tbl_admin_users',
                'user_name',
                'admin_pwd',
                'MD5(CONCAT(?, "'.SECURITY_SALT.'"))'
            );
            // We're authenticated! Redirect to the home page
            $adapter->setIdentity($request->getParam('user_name'));
            $adapter->setCredential($request->getParam('admin_pwd'));
            $result 	= $auth->authenticate($adapter);
            if ($result->isValid()) {
                $user = $adapter->getResultRowObject();
                $sess = new Zend_Session_Namespace('adminuser');
                $sess->user = $user;
                $auth->getStorage()->write($user);
                $db =  Zend_Db_Table::getDefaultAdapter();
                $updateqry = "Update tbl_admin_users set last_login_time  = '".date('Y-m-d H:i:s')."' WHERE id = '".Zend_Auth::getInstance()->getIdentity()->id."'";
                $updid = $db->query($updateqry);
                if($this->getRequest()->getParam('req') != ''){
                    $url  = $this->getRequest()->getParam('req');
                    $this->_redirect($url);
                }
                $this->_helper->redirector('dashboard', 'index');
            }
            $this->_helper->flashMessenger->addMessage(array('alert alert-error'=>'Invalid Login Details'));
            //$this->_helper->redirector('index','index','admin');
            
            $this->_redirect('/admin/?req='.$req) ;
            //$this->_helper->FlashMessenger->addMessage('Login Error');
            //$this->_helper->redirector->gotoUrl('fr/admin/index/index');
            //$this->view->messages = $this->_helper->flashMessenger->getMessages();
            //$this->_helper->redirector('index', 'index');
        }
        $this->view->form = $form;
    }

    /************ Dashboard Page ****************/
    public function dashboardAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if ($auth->hasIdentity()) {
            $data = array();
            $this->view->assign('page_Name',"dashboard");
            $today = date('Y-m-d');
            $yesterday = date('Y-m-d', strtotime($today . ' - 1 day'));
            $userObj = new Admin_Model_UsersMapper();
            $frontuserObj = new User_Model_User();
            $cronObj = new Crons_Model_Crons();
            $businessObj = new Admin_Model_BusinessusersMapper();
            $userdata = $userObj->fetchAll();
            $data['user_count'] = $userObj->fetchtotalusercount();
            $data['prev_user_count'] = $userObj->fetchlastdayusercount($today);
            $data['businessuser_count'] = $businessObj->fetchtotalbusinessusercount();
            $data['prev_businessuser_count'] = $businessObj->fetchlastdaybusinessusercount($today);
            $data['uppers_count'] = $userObj->fetchtotalupperscount();
            $data['prev_uppers_count'] = $userObj->fetchlastdayupperscount($today);
            $data['active_uppers_count'] = $userObj->fetchtotalactiveupperscount();
            $data['prev_active_uppers_count'] = $userObj->fetchlastdayactiveupperscount($yesterday);
            $data['purchased_uppers_count'] = $userObj->fetchtotalpurchasedupperscount();
            $data['prev_purchased_uppers_count'] = $userObj->fetchlastdaypurchasedupperscount($today);
            $data['active_purchased_uppers_count'] = $userObj->fetchtotalactivepurchasedupperscount();
            $data['prev_active_purchased_uppers_count'] = $userObj->fetchlastdayactivepurchasedupperscount($yesterday);
            $data['celebrity_count'] = $userObj->fetchtotalcelebritycount();
            $data['prev_celebrity_count'] = $userObj->fetchlastdaycelebritycount($today);
            $user = $frontuserObj->getPopularUser();
            $business = $frontuserObj->getPopularBusiness();
            $school = $frontuserObj->popularschool();
            $college = $frontuserObj->popularcollege();
            $workplace = $frontuserObj->popularworkplace();
            $previouspopulardata = $cronObj->getpreviouspopulardata();
            $data['popular_user'] = $user['realname'];
            $data['prev_popular_user'] = $previouspopulardata['user_name'];
            $data['popular_business'] = $business['realname'];
            $data['prev_popular_business'] = $previouspopulardata['business_name'];
            $data['popular_school'] = $school['school_name'];
            $data['prev_popular_school'] = $previouspopulardata['school_name'];
            $data['popular_college'] = $college['college_name'];
            $data['prev_popular_college'] = $previouspopulardata['college_name'];
            $data['popular_workplace'] = $workplace['work_place_name'];
            $data['prev_popular_workplace'] = $previouspopulardata['workplace'];
            //echo '<pre>';print_r($data);exit;
            $this->view->data = $data;
        } else {
            if ('index' != $this->getRequest()->getActionName()) {
                    $this->_helper->redirector('index');
            }
        }
    }

    /*********** Logout Action ************/
    public function logoutAction() {
        $this->_helper->viewRenderer->setNoRender();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if($auth->hasIdentity()) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $auth->clearIdentity();
            $this->_helper->redirector('index', 'index');
            Zend_Session::destroy(true, true);
            //$this->_redirect('/admin/index');
        }
    }

    public function changepasswordAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if($auth->getIdentity()->user_name == 'superadmin') {
            //$id = $this->getRequest()->getParam('id');
            $id = Zend_Auth::getInstance()->getIdentity()->id;
            $form = new Admin_Form_AdminChangePassword();
            $this->view->assign('page_Name',"changepassword");
            if($id !='') {
                $settings = new Admin_Model_AdminMapper();
                //$form = new Admin_Form_AdminAdd();
                //$form->populate($row->toArray());
                if ($this->getRequest()->isPost()) {
                    $request= $this->getRequest();
                    if ($form->isValid($request->getPost())) {
                        $password = stripslashes($request->getParam('password'));
                        $cnt = $settings->duplicatePasswordCheck($id,$password);
                        if($cnt > 0) {
                            $data = array(
                                    'admin_pwd' => md5(stripslashes($request->getParam('new_password')).SECURITY_SALT)
                                );
                            $settings->save($data,$id);
                            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Your password have been updated'));
                            $this->_helper->redirector('changepassword','index','admin');
                        } else {
                            $this->_helper->flashMessenger->addMessage(array('alert alert-error'=>'<h4 class="alert-heading">Error!</h4>Invalid Password'));
                            $this->_helper->redirector('changepassword','index','admin');
                        }
                    }
                }
            }
            $this->view->form = $form;
        } else {
            $this->_helper->flashMessenger->addMessage(array('alert alert-error'=>'<h4 class="alert-heading">UN-AUTHORIZED ACCESS!!!</h4>You dont have previlages to do the action'));
            $this->_helper->redirector('dashboard','index','admin');
        }
    }

    public function approvecelebAction() {
        //echo "<pre>";print_r($this->getRequest()->getParams());exit;
        $u = $this->getRequest()->getParam('u');
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if($auth ->hasIdentity()) {
        } else {
            if ('index' != $this->getRequest()->getActionName() || 'index' != $this->getRequest()->getControllerName()) {
                $this->_redirect('/admin/?req=admin/users/edituser/id/'.$u);
            }
        }
        
        $request = $this->getRequest();
        $user_id  = $request->getParam('u');
        $isRedirected = $request->getParam('re');
        if($user_id == '' && $isRedirected == '') {
            $this->_helper->flashMessenger->addMessage(array('alert alert-error'=>'<h4 class="alert-heading">Error while processing your request. Invalid User Id!!!'));
            $this->_helper->redirector('approveceleb','index','admin', array('re'=>2));
        }
        if($isRedirected == '' && $user_id != ''){
            $adminUserObj = new Admin_Model_UsersMapper();
            $updateArr['user_type'] = "C";
            $upd = $adminUserObj->update($updateArr, $user_id);
            
            // Delete form tbl_users_celebs_temp table after successful approval
            $UserObj = new User_Model_User();
            $UserObj->deleteCelebTemp($user_id);
            
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Your request have been updated'));
            $this->_helper->redirector('approveceleb','index','admin', array('re'=>2));
        }
    }

}