<?php
class Admin_UserlevelsController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->ajaxContext->addActionContext('index', 'html')->initContext();
    }
    public function preDispatch()
    {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if ($auth->hasIdentity())
        { 
            
        }
        else 
        {
            if ('index' != $this->getRequest()->getActionName() || 'index' != $this->getRequest()->getControllerName())
            {
                $this->_redirect('/admin/index');
            }
        }
        $this->view->assign('page_Name',"userlevelsmanagement");
    }
    public function indexAction()
    {
        $db         =  Zend_Db_Table::getDefaultAdapter();
        $request    = $this->getRequest();
        $mapper = new Admin_Model_UserlevelsMapper();
        //echo '<pre>';print_r($request);exit;
        if($request->getParam('hid_key') == 'Delete')
        { 
            $id = $request->getParam('hid_id');
            $result = $mapper->delete($id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been deleted Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('adminActions') != '')
        {
            $idsArr  = array();
            $idsArr1 = array();
            
            $idsArr  = $this->getRequest()->getParam('checkcategories');
            $cnt = sizeof($idsArr);
            for($i=0;$i<$cnt;$i++)
            {
                $idsArr1[$i] = "'".$idsArr[$i]."'";
            }
            $ids     = implode(',', $idsArr1);
            $where      = "id in (".$ids.")";
            if($this->getRequest()->getParam('adminActions') == 'D')
            {    
                $updId      = $mapper->deleteall($where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) has been Deleted Successfully'));
                return $this->_helper->redirector('index');
            }  
        }
        $userlevels = new Admin_Model_UserlevelsMapper();
        $result = $userlevels->fetchAll();
        
        $page		= $this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($result);		
        //$paginator->setItemCountPerPage(ADMIN_PAGINATION_LIMIT);
        $paginator->setItemCountPerPage('6');
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(2);
        $isAjaxReq = $this->getRequest()->isXmlHttpRequest();
        if($isAjaxReq)
            $this->_helper->layout->disableLayout();
        $this->view->assign('isAjaxReq',$isAjaxReq);
        $this->view->assign('page_Name',"userlevelsmanagement");
        $this->view->paginator=$paginator;
    }	
    public function adduserlevelAction()
    {
        $request = $this->getRequest();
        $form = new Admin_Form_AdminUserlevels();

        if ($this->getRequest()->isPost())
        {
            if ($form->isValid($request->getPost()))
            {
                $dataObj = new Admin_Model_Userlevels($form->getValues());
                //echo "<pre>";print_r($dataObj);exit;
                $mapper = new Admin_Model_UserlevelsMapper();
                $mapper->save($dataObj);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>New Record has been added Successfully'));
                $this->_helper->redirector('index','userlevels','admin');
            }
        }
        $this->view->assign('page_Name',"adduserlevel");
        $this->view->form = $form;
    }
    public function edituserlevelAction()
    { 
        $request = $this->getRequest();
        $id =$this->getRequest()->getParam('id');
        if($id !='')
        {
            $userlevels = new Admin_Model_UserlevelsMapper();
            $row = $userlevels->find($id);
            $form    = new Admin_Form_AdminUserlevels();
            $form->populate($row->toArray());
            $form->getElement('level_name')->addValidator('Db_NoRecordExists',false,array('table' => 'tbl_user_levels',
                                                                                                'field' => 'level_name',
                                                                                                'exclude' => array ('field' => 'id', 'value' => $id)));
            if ($this->getRequest()->isPost())
            {
                if ($form->isValid($request->getPost()))
                {
                    //echo "<pre>";print_r($this->getRequest()->getParams());//exit;
                    $dataObj = new Admin_Model_Userlevels($form->getValues());
                    //echo "<pre>";print_r($dataObj);exit;
                    $userlevels->save($dataObj);
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Your updates have been saved'));
                    return $this->_helper->redirector('index');
                }
            }
        $this->view->form = $form;
        }
    }	
}
?>