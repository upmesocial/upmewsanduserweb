<?php
class Admin_WorkplacesController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if ($auth->hasIdentity()) {
            $this->view->assign('page_Name',"workplacesmanagement");
            $request    = $this->getRequest();
            $mapper = new Admin_Model_WorkplacesMapper();
            $updatearr = array();
            if($request->getParam('hid_key') == 'Delete') {
                $id = $request->getParam('hid_id');
                $result = $mapper->delete($id);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been deleted Successfully'));
                return $this->_helper->redirector('index');
            }
            if($request->getParam('hid_key') == 'change') {
                $updatearr['status'] = $request->getParam('hid_status');
                $id = $request->getParam('hid_id');
                $result = $mapper->update($updatearr, $id);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record Status has been Updated Successfully'));
                return $this->_helper->redirector('index');
            }
            if($request->getParam('adminActions') != '') {
                $idsArr  = array();
                $idsArr1 = array();
                $idsArr  = $this->getRequest()->getParam('checkworkplaces');
                $cnt = sizeof($idsArr);
                for($i=0;$i<$cnt;$i++) {
                    $idsArr1[$i] = "'".$idsArr[$i]."'";
                }
                $ids     = implode(',', $idsArr1);
                if($this->getRequest()->getParam('adminActions') == '1')
                        $updatearr['status'] = '1';
                elseif($this->getRequest()->getParam('adminActions') == '0')
                        $updatearr['status'] = '0';
                $where      = "work_place_id in (".$ids.")";
                if($this->getRequest()->getParam('adminActions') == 'D') {
                    $result      = $mapper->deleteall($where);
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) has been Deleted Successfully'));
                    return $this->_helper->redirector('index');
                } else {
                    $result = $mapper->updateall($updatearr, $where);
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) Status has been Updated Successfully'));
                    return $this->_helper->redirector('index');
                }    
            }
            if($request->getParam('workplaceviewActions') != '')
                $sort = $this->getRequest()->getParam('workplaceviewActions');
            else if($request->getParam('order') != '')
                $sort = $this->getRequest()->getParam('order');
            else
                $sort = "";		
            $isAjaxReq  = $this->getRequest()->isXmlHttpRequest();
            $keyword= $this->getRequest()->getParam('search');
            if($isAjaxReq) {
                $this->_helper->layout->disableLayout();
                if($keyword !='') {
                    $result = $mapper->fetchSortresults($keyword);
                } else if($sort !='') {
                    if($sort == 'MR'){
                        $result = $mapper->fetchSortresults('',$sort);
                    } elseif($sort == 'AO') {
                        $result = $mapper->fetchSortresults('',$sort);
                    } else {
                        $result = $mapper->fetchAll();
                    }
                } else
                    $result = $mapper->fetchAll();
            } else {
                if($sort != '') {
                    if($sort == 'MR') {
                        $result = $mapper->fetchSortresults('',$sort);
                    } elseif($sort == 'AO') {
                        $result = $mapper->fetchSortresults('',$sort);
                    }
                } else {
                    $result = $mapper->fetchAll();
                }
            }
            $page = $this->_getParam('page',1);
            $paginator = Zend_Paginator::factory($result);		
            $paginator->setItemCountPerPage('10');
            $paginator->setCurrentPageNumber($page);
            $this->view->assign('sort',$sort);
            $this->view->assign('isAjaxReq',$isAjaxReq); 
            $paginator->setPageRange(2);
            //echo '<pre>';print_r($paginator);exit;
            $this->view->paginator=$paginator;
        } else {
            if ('index' != $this->getRequest()->getActionName()) {
                    $this->_helper->redirector('index');
            }
        }
    }

    public function addworkplaceAction() {
        $this->view->assign('page_Name',"addworkplace");
        $id =$this->getRequest()->getParam('id');
        $form   = new Admin_Form_AdminWorkplace();
        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest();
            if ($form->isValid($request->getPost())) {
                $newName = $request->getParams();
                //echo '<Pre>';print_r($newName);exit;
                $formData   = $form->getValues();
                $formData['work_place_name'] = $newName['work_place_name'];
                $formData['work_place_city'] = $newName['work_place_city'];
                $formData['popular_order'] = '0';
                $formData['status'] = $newName['status'];
                $workplace = new Admin_Model_WorkplacesMapper();
                $UserID = $workplace->save($formData,$id);
                if($UserID == '1')
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been updated Successfully'));
                else
                    $this->_helper->flashMessenger->addMessage(array('alert alert-error'=>'<h4 class="alert-heading">Error!</h4>Some error Occured'));
                $this->_helper->redirector('index','workplaces','admin');
            }
        }
        $this->view->form = $form;
    }

    public function editworkplaceAction() { 
        $this->view->assign('page_Name',"addworkplace");
        $id =$this->getRequest()->getParam('id');
        if($id !='') {
            $workplace = new Admin_Model_WorkplacesMapper();
            $row2 = $workplace->fetchRowByWorkplaceID($id);
            $form = new Admin_Form_AdminWorkplace();
            if(!empty($row2))
                $form->populate($row2->toArray());
            if ($this->getRequest()->isPost()) {
                $request= $this->getRequest();
                if ($form->isValid($request->getPost())) {
                    $newName = $request->getParams();
                    $formData   = $form->getValues();
                    $formData['work_place_name'] = $newName['work_place_name'];
                    $formData['work_place_city'] = $newName['work_place_city'];
                    $formData['status'] = $newName['status'];
                    $val = $workplace->save($formData, $row2->work_place_id);
                    if($val == '1')
                        $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been updated Successfully'));
                    else
                        $this->_helper->flashMessenger->addMessage(array('alert alert-error'=>'<h4 class="alert-heading">Error!</h4>Some error Occured'));
                    $this->_helper->redirector('index','workplaces','admin');
                }
            }
            $this->view->form   = $form;
        }
    }

    public function viewworkplaceAction() {
        $id = $this->getRequest()->getParam('id');
        $this->view->assign('page_Name',"workplacesmanagement");
        if($id !='') {
            $workplace  = new Admin_Model_WorkplacesMapper();
            $result = $workplace->fetchRowByWorkplaceID($id);            
            $this->view->result = $result;
        }
    }

}