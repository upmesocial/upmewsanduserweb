<?php class Admin_BusinessusersController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->ajaxContext->addActionContext('index', 'html')
                                ->initContext();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if($auth ->hasIdentity()) {
        } else {
            if ('index' != $this->getRequest()->getActionName() || 'index' != $this->getRequest()->getControllerName()) {
                $this->_redirect('/admin/index');
            }
        }
    }
        
    public function indexAction() {
        $request    = $this->getRequest();
        $mapper = new Admin_Model_BusinessusersMapper();
        $updatearr = array();
        if($request->getParam('hid_key') == 'Delete') {
            $id = $request->getParam('hid_id');
            $result = $mapper->delete($id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been deleted Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('hid_key') == 'change') {
            $updatearr['status'] = $request->getParam('hid_status');
            $id = $request->getParam('hid_id');
            $result = $mapper->update($updatearr, $id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record Status has been Updated Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('adminActions') != '') {
            $idsArr  = array();
            $idsArr1 = array();
            $idsArr  = $this->getRequest()->getParam('checkbusinessusers');
            $cnt = sizeof($idsArr);
            for($i=0;$i<$cnt;$i++) {
                $idsArr1[$i] = "'".$idsArr[$i]."'";
            }
            $ids     = implode(',', $idsArr1);
            if($this->getRequest()->getParam('adminActions') == 'A')
                    $updatearr['status'] = 'A';
            elseif($this->getRequest()->getParam('adminActions') == 'I')
                    $updatearr['status'] = 'I';
            $where      = "id in (".$ids.")";
            if($this->getRequest()->getParam('adminActions') == 'D') {
                $result      = $mapper->deleteall($where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) has been Deleted Successfully'));
                return $this->_helper->redirector('index');
            } else {
                $result = $mapper->updateall($updatearr, $where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) Status has been Updated Successfully'));
                return $this->_helper->redirector('index');
            }
        }
        if($request->getParam('businessuserviewActions') != '')
            $sort = $this->getRequest()->getParam('businessuserviewActions');
        else if($request->getParam('order') != '')
            $sort = $this->getRequest()->getParam('order');
        else
            $sort = "";
        $isAjaxReq  = $this->getRequest()->isXmlHttpRequest();
        $keyword= $this->getRequest()->getParam('search');
        if($isAjaxReq) {
            $this->_helper->layout->disableLayout();
            if($keyword !='') {
                $result = $mapper->fetchSortresults($keyword);
            } else if($sort !='') {
                if($sort == 'MR') {
                    $result = $mapper->fetchSortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $mapper->fetchSortresults('',$sort);
                } else {
                    $result = $mapper->fetchAll();
                }
            } else
                $result = $mapper->fetchAll();
        } else {
            if($sort != '') {
                if($sort == 'MR') {
                    $result = $mapper->fetchSortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $mapper->fetchSortresults('',$sort);
                }
            } else {
                $result = $mapper->fetchAll();
            }
        }
        $page = $this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($result);
        //$paginator->setItemCountPerPage(ADMIN_PAGINATION_LIMIT);
        $paginator->setItemCountPerPage('10');
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(2);
        $this->view->keyword = $keyword;
        $this->view->assign('sort',$sort);
        $this->view->assign('isAjaxReq',$isAjaxReq);
        $this->view->assign('page_Name',"businessusersmanagement");
        $this->view->paginator=$paginator;
    }

    public function editbusinessuserAction() {
        $this->view->assign('page_Name',"businessusersmanagement");
        $id =$this->getRequest()->getParam('id');
        if($id !='') {
            $businessuser   = new Admin_Model_BusinessusersMapper();
            $row2   = $businessuser->fetchRowByBusinessuserID($id);
            $form   = new Admin_Form_AdminBusinessuser();
            if(!empty($row2))
                $form->populate($row2->toArray());
            $form->getElement('email')->addValidator('Db_NoRecordExists',false,
                    array('table' => 'tbl_business_users',
                            'field' => 'email',
                            'exclude' => array ('field' => 'business_id', 'value' => $id)
                    )
            );
            $form->getElement('username')->addValidator('Db_NoRecordExists',false,
                    array('table' => 'tbl_business_users',
                            'field' => 'username',
                            'exclude' => array ('field' => 'business_id', 'value' => $id)
                    )
            );
            if ($this->getRequest()->isPost()) {
                $request= $this->getRequest();
                if ($form->isValid($request->getPost())) {
                    $newName = '';
                    if(sizeof($form->image_path->getFileName()) != 0) {
                        $originalFilename = pathinfo($form->image_path->getFileName());
                        if($originalFilename['extension']=='jpeg')
                            $originalFilename['extension']='jpg';
                        $newName = time() .'.'. strtolower($originalFilename['extension']);
                        $form->image_path->addFilter('Rename', $newName);
                        if($form->image_path->receive()) {
                                $fullPathNameFile = UPLOAD_PATH.'/business/original/'.$newName;
                                $image = new UpmeSocial_Controller_Action_Helper_Image();
                                $source = UPLOAD_PATH.'/business/original/';
                                $medium = UPLOAD_PATH.'/business/medium/';
                                $destination = UPLOAD_PATH.'/business/thumbnails/';
                                $largethumbpath = UPLOAD_PATH.'/business/original/';
                                /*** For getting large thumb ***/
                                list($largethumb_width,$largethumb_height) = $image->getNewDimensions($fullPathNameFile,800,600,'');
                                $image->ImageSize($largethumbpath,$newName,$largethumb_width,$largethumb_height,$source);
                                /*** For getting medium thumb ***/
                                list($medium_width,$medium_height) = $image->getNewDimensions($fullPathNameFile,180,391,false);
                                $image->ImageSize($medium,$newName,$medium_width,$medium_height,$source);
                                /*** For getting small thumb ***/
                                list($thumb_width,$thumb_height) = $image->getNewDimensions($fullPathNameFile,100,100,false);
                                $image->cropImage($destination,$newName,$thumb_width,$thumb_height,$source);
                        }
                    }
                    $formData   = $form->getValues();
                    $formData['image_path'] = $newName;
                    if($formData['image_path'] != '') {
                        @unlink(UPLOAD_PATH.'business/original/'.$row2['image_path']);
                        @unlink(UPLOAD_PATH.'business/thumbnails/'.$row2['image_path']);
                        @unlink(UPLOAD_PATH.'business/medium/'.$row2['image_path']);
                    }
                    $businessuser->saveUser($formData, $row2->business_id);
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been updated Successfully'));
                    $this->_helper->redirector('index','businessusers','admin');
                }
            }
            $this->view->form   = $form;
        }
    }

    public function addbusinessuserAction() {
        $this->view->assign('page_Name',"addbusinessuser");
        $id =$this->getRequest()->getParam('id');
        $form   = new Admin_Form_AdminBusinessuser();
        $form->getElement('email')
            ->addValidator('Db_NoRecordExists',
                        false,
                        array('table' => 'tbl_business_users',
                                'field' => 'email',
                                'exclude' => array ('field' => 'business_id', 'value' => $id)
                        )
            );
        $form->getElement('username')
            ->addValidator('Db_NoRecordExists',
                        false,
                        array('table' => 'tbl_business_users',
                                'field' => 'username',
                                'exclude' => array ('field' => 'business_id', 'value' => $id)
                        )
            );
        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest();
            if ($form->isValid($request->getPost())) {
                $newName = '';
                if(sizeof($form->image_path->getFileName()) != 0) {
                    $originalFilename = pathinfo($form->image_path->getFileName());
                    if($originalFilename['extension']=='jpeg')
                        $originalFilename['extension']='jpg';
                    $newName = time() .'.'. strtolower($originalFilename['extension']);
                    $form->image_path->addFilter('Rename', $newName);
                    if($form->image_path->receive()) {
                        $fullPathNameFile = UPLOAD_PATH.'/business/original/'.$newName;
                        $image = new UpmeSocial_Controller_Action_Helper_Image();
                        $source = UPLOAD_PATH.'/business/original/';
                        $medium = UPLOAD_PATH.'/business/medium/';
                        $destination = UPLOAD_PATH.'/business/thumbnails/';
                        $largethumbpath = UPLOAD_PATH.'/business/original/';
                        /*** For getting large thumb ***/
                        list($largethumb_width,$largethumb_height) = $image->getNewDimensions($fullPathNameFile,800,600,'');
                        $image->ImageSize($largethumbpath,$newName,$largethumb_width,$largethumb_height,$source);
                        /*** For getting medium thumb ***/
                        list($medium_width,$medium_height) = $image->getNewDimensions($fullPathNameFile,180,391,false);
                        $image->ImageSize($medium,$newName,$medium_width,$medium_height,$source);
                        /*** For getting small thumb ***/
                        list($thumb_width,$thumb_height) = $image->getNewDimensions($fullPathNameFile,100,100,false);
                        $image->cropImage($destination,$newName,$thumb_width,$thumb_height,$source);
                    }
                }
                $formData   = $form->getValues();
                $formData['image_path'] = $newName;
                $businessuser   = new Admin_Model_BusinessusersMapper();
                $UserID = $businessuser->save($formData,$id);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been updated Successfully'));
                $this->_helper->redirector('index','businessusers','admin');
            }
        }
        $this->view->form = $form;
    }

    public function viewbusinessuserAction() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $this->getRequest()->getParam('id');
        $this->view->assign('page_Name',"businessusersmanagement");
        if($id !='') {
            $businessusers  = new Admin_Model_BusinessusersMapper();
            $result = $businessusers->fetchRowByBusinessuserID($id);            
            $businesscategories = new Admin_Model_CategoriesMapper();
            $category = $businesscategories->fetchRowByCategoryID($result['category_id']);
            $coupons  = new Business_Model_Coupons();
            $couponscnt = $coupons->getcouponscntbyid($id);
            $businessusers  = new Admin_Model_BusinessusersMapper();
            $reviewscnt = $businessusers->getreviewscntbyid($id);
            $businessusers  = new Admin_Model_BusinessusersMapper();
            $followerscnt = $businessusers->getfollowerscntbyid($id);
            $this->view->result = $result;
            $this->view->category = $category;
            $this->view->couponscnt = $couponscnt;
            $this->view->reviewscnt = $reviewscnt;
            $this->view->followerscnt = $followerscnt;
        }
    }

} ?>