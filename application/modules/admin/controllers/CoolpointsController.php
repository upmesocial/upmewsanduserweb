<?php
class Admin_CoolpointsController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->ajaxContext->addActionContext('index', 'html')->initContext();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if ($auth->hasIdentity()) {
            
        } else {
            if ('index' != $this->getRequest()->getActionName() || 'index' != $this->getRequest()->getControllerName()) {
                $this->_redirect('/admin/index');
            }
        }
        $this->view->assign('page_Name',"coolpointsmanagement");
    }

    public function indexAction() {
        $request = $this->getRequest();
        $mapper = new Admin_Model_CategoriesMapper();
        $updatearr = array();
        if($request->getParam('hid_key') == 'Delete') {
            $id = $request->getParam('hid_id');
            $result = $mapper->delete($id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been deleted Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('adminActions') != '') {
            $idsArr = array();
            $idsArr1 = array();
            $idsArr = $this->getRequest()->getParam('checkcoolpoints');
            $cnt = sizeof($idsArr);
            for($i=0;$i<$cnt;$i++) {
                $idsArr1[$i] = "'".$idsArr[$i]."'";
            }
            $ids = implode(',', $idsArr1);
            $where = "id in (".$ids.")";
            if($this->getRequest()->getParam('adminActions') == 'D') {
                $result = $mapper->deleteall($where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) has been Deleted Successfully'));
                return $this->_helper->redirector('index');
            } else {
                $result = $mapper->updateall($updatearr, $where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) Status has been Updated Successfully'));
                return $this->_helper->redirector('index');
            }    
        }
        $categories = new Admin_Model_CoolpointsMapper();
        $result = $categories->fetchAll();
        $page = $this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($result);		
        //$paginator->setItemCountPerPage(ADMIN_PAGINATION_LIMIT);
        $paginator->setItemCountPerPage('5');
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(2);
        $isAjaxReq = $this->getRequest()->isXmlHttpRequest();
        if($isAjaxReq)
            $this->_helper->layout->disableLayout();
        $this->view->assign('isAjaxReq',$isAjaxReq);
        $this->view->paginator=$paginator;
    }

    public function addupsdownsAction() {
        $request = $this->getRequest();
        $form = new Admin_Form_AdminCoolpoints();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                $dataObj = new Admin_Model_Coolpoints($form->getValues());
                $mapper = new Admin_Model_CoolpointsMapper();
                $mapper->save($dataObj);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>New Record has been added Successfully'));
                $this->_helper->redirector('index','upsdowns','admin');
            }
        }
        $this->view->assign('page_Name',"addupsdowns");
        $this->view->form = $form;
    }

    public function addcoolpointsAction() {
        $request = $this->getRequest();
        $upsdowns = new Admin_Model_CoolpointsMapper();
        $form    = new Admin_Form_AdminCoolpoints();
        $form->getElement('name')->addValidator('Db_NoRecordExists',
                                                false,
                                                array('table' => 'tbl_coolpoints_settings',
                                                    'field' => 'name'
                                                )
                                            );
        if($this->getRequest()->isPost()) {
            if($form->isValid($request->getPost())) {
                $dataObj = new Admin_Model_Coolpoints($form->getValues());
                $mapper = new Admin_Model_CoolpointsMapper();
                $mapper->save($dataObj);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Your updates have been saved'));
                return $this->_helper->redirector('index');
            }
        }
        $this->view->form = $form;
    }

    public function editcoolpointsAction() {
        $request = $this->getRequest();
        $id = $this->getRequest()->getParam('id');
        if($id !='') {
            $upsdowns = new Admin_Model_CoolpointsMapper();
            $row = $upsdowns->find($id);
            $form    = new Admin_Form_AdminCoolpoints();
            $form->populate($row->toArray());
            $form->getElement('name')->addValidator('Db_NoRecordExists',
                                                    false,
                                                    array('table' => 'tbl_coolpoints_settings',
                                                        'field' => 'name',
                                                        'exclude' => array ('field' => 'id', 'value' => $id)
                                                    )
                                                );
            if($this->getRequest()->isPost()) {
                if($form->isValid($request->getPost())) {
                    $dataObj = new Admin_Model_Coolpoints($form->getValues());
                    $mapper = new Admin_Model_CoolpointsMapper();
                    $mapper->save($dataObj);
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Your updates have been saved'));
                    return $this->_helper->redirector('index');
                }
            }
            $this->view->form = $form;
        }
    }

} ?>