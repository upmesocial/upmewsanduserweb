<?php class Admin_UsersController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->ajaxContext->addActionContext('index', 'html')
                                ->initContext();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if($auth ->hasIdentity()) {
            
        } else {
            if ('index' != $this->getRequest()->getActionName() || 'index' != $this->getRequest()->getControllerName()) {
                $this->_redirect('/admin/index');
            }
        }
    }

    public function indexAction() {
        $request    = $this->getRequest();
        $users = new Admin_Model_UsersMapper();
        //$mapper = new Admin_Model_UsersMapper();
        $updatearr = array();
        if($request->getParam('hid_key') == 'Delete') { 
            $id = $request->getParam('hid_id');
            $result = $users->delete($id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been deleted Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('hid_key') == 'change') {
            $updatearr['user_account_status'] = $request->getParam('hid_status');
            $id = $request->getParam('hid_id');
            $result = $users->update($updatearr, $id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record Status has been Updated Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('adminActions') != '') {
            $idsArr  = array();
            $idsArr1 = array();
            $idsArr  = $this->getRequest()->getParam('checkusers');
            $cnt = sizeof($idsArr);
            for($i=0;$i<$cnt;$i++) {
                $idsArr1[$i] = "'".$idsArr[$i]."'";
            }
            $ids     = implode(',', $idsArr1);
            if($this->getRequest()->getParam('adminActions') == 'A')
                    $updatearr['user_account_status'] = 'A';
            elseif($this->getRequest()->getParam('adminActions') == 'I')
                    $updatearr['user_account_status'] = 'I';
            $where      = "user_id in (".$ids.")";
            if($this->getRequest()->getParam('adminActions') == 'D') {
                $result      = $mapper->deleteall($where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) has been Deleted Successfully'));
                return $this->_helper->redirector('index');
            } else {
                $result = $mapper->updateall($updatearr, $where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) Status has been Updated Successfully'));
                return $this->_helper->redirector('index');
            }
        }
        if($request->getParam('userviewActions') != '')
            $sort = $this->getRequest()->getParam('userviewActions');
        else if($request->getParam('order') != '')
            $sort = $this->getRequest()->getParam('order');
        else
            $sort = "";		
        $isAjaxReq  = $this->getRequest()->isXmlHttpRequest();
        $keyword= $this->getRequest()->getParam('search');
        if($isAjaxReq) {
            $this->_helper->layout->disableLayout();
            if($keyword !='') {
                $result = $users->fetchSortresults($keyword);
            } else if($sort !='') {
                if($sort == 'MR') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'ND') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'NA') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'JD') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'JA') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'SD') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'SA') {
                    $result = $users->fetchSortresults('',$sort);
                } else {
                    $result = $users->fetchAll();
                }
            } else
                $result = $users->fetchAll();
        } else {
            if($sort != '') {
                if($sort == 'MR') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'ND') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'NA') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'JD') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'JA') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'SD') {
                    $result = $users->fetchSortresults('',$sort);
                } elseif($sort == 'SA') {
                    $result = $users->fetchSortresults('',$sort);
                } else {
                    $result = $users->fetchAll();
                }
            } else {
                $result = $users->fetchAll();
            }
        }
        $page = $this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($result);
        $paginator->setItemCountPerPage('10');
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(2);
        $this->view->keyword = $keyword;
        $this->view->assign('sort',$sort);
        $this->view->assign('isAjaxReq',$isAjaxReq);
        $this->view->assign('page_Name',"usersmanagement");
        $this->view->paginator=$paginator;
    }

    public function edituserAction() {
        $this->view->assign('page_Name',"adduser");
        $countriesobj = new User_Model_Countries();
        $countries = $countriesobj->get_countries();
        $id =$this->getRequest()->getParam('id');
        if($id !='') {
            $user = new Admin_Model_UsersMapper();
            $row2 = $user->fetchRowByUsertID($id);
            //Fetching States Based On CountryId
            $statesObj = new User_Model_States();
            $states = $statesObj->get_states($row2['country']);
            $states = $states->toArray();
            //Fetching Cities Based On StateId
            $cityObj = new User_Model_Cities();
            $cities = $cityObj->get_cities($row2['state']);
            $cities = $cities->toArray();
            //Fetching Zipcode Basedon CityId
            $zipcodes = $cityObj->getZipcodesByCity($row2['city']);
            $form = new Admin_Form_AdminUseredit(array('countryid'=>$row2['country'], 'stateid'=>$row2['state'], 'cityid' =>$row2['city']));
            if(!empty($row2))
                $form->populate($row2->toArray());
            $form->getElement('email')
                ->addValidator('Db_NoRecordExists',false,
                        array('table' => 'tbl_users',
                                'field' => 'email',
                                'exclude' => array ('field' => 'user_id', 'value' => $id)
                        )
                );
            $form->getElement('username')
                ->addValidator('Db_NoRecordExists',false,
                        array('table' => 'tbl_users',
                                'field' => 'username',
                                'exclude' => array ('field' => 'user_id', 'value' => $id)
                        )
                );
            if ($this->getRequest()->isPost()) {
                $request= $this->getRequest();
                //if($_SERVER['REMOTE_ADDR'] == '192.168.1.81') { echo '<pre>';print_r($this->getRequest()->getPost());print_r($_FILES);exit; }
                if ($form->isValid($request->getPost())) {
                    //echo '<pre>';print_r($request->getPost());exit;
                    $user_id = $request->getPost('user_id');
                    if(!empty($_FILES['profile_pic_path']['name'])) {
                        $dir_upload = UPLOAD_PATH.'images/original/';
                        $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                        $medium_path = UPLOAD_PATH.'images/medium/';
                        $mobile_path = UPLOAD_PATH.'images/mobile/';
                        $fileName = $_FILES['profile_pic_path']['name'];
                        $image = new UpmeSocial_Controller_Action_Helper_Image();
                        $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path);
                        //Saving imagesedit to user
                        $data['profile_pic_path'] = $arrFileName['profile_pic_path'];
                        $album['user_id'] = $user_id;
                        $album['user_type'] = 'U';
                        $album['album_name'] = 'Profile Photos';
                        $mobileobj = new Mobile_Model_Albums();
                        //echo '<pre>';print_r($album);exit;
                        $val = $mobileobj->albumnameexistsrnot($album);
                        $album['created_date'] = date('Y-m-d H:i:s');
                        //echo '<pre>';print_R($val);exit;
                        if(!empty($val)) {
                            $albumdet = $mobileobj->getAlbumsByUserId($user_id, 'U');
                            $id = $mobileobj->update($album,$albumdet['0']['id']);
                            $albumid = $albumdet['0']['id'];
                        } else {
                            $album['is_default'] = '1';
                            $albumid = $mobileobj->insert($album);
                        }
                        $photoObj = new Mobile_Model_Photos();
                        $coverphoto = $photoObj->albumidexistsrnot($albumid,$user_id,'U');
                        $photodata['album_id'] = $albumid;
                        $photodata['photo_name'] = 'Profile Photo';
                        $photodata['photo_original_name'] = $fileName;
                        $photodata['photo_modified_name'] = $data['profile_pic_path'];
                        $photodata['user_id'] = $user_id;
                        $photodata['user_type'] = 'U';
                        if(!empty($coverphoto['photo_id']))
                            $photodata['cover_photo'] = '0';
                        else
                            $photodata['cover_photo'] = '1';
                        $photodata['created_date'] = date('Y-m-d H:i:s');
                        $photo_id = $photoObj->insert($photodata);
                        $formData['profile_pic_path'] = $data['profile_pic_path'];
                        $user->saveUser($formData, $row2->user_id);
                    }
                    $formData   = $request->getPost();
                    $user->saveUser($formData, $row2->user_id);
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been updated Successfully'));
                    $this->_helper->redirector('index','users','admin');
                }
            }
            //echo 'con<pre>';print_r($row2->country);exit;
            $this->view->form = $form;
            $this->view->result = $row2;
            $this->view->countries = $countries;
            $this->view->states = $states;
            $this->view->cities = $cities;
            $this->view->zipcodes = $zipcodes;
        }
    }

    public function adduserAction() {
        $this->view->assign('page_Name',"adduser");
        $id =$this->getRequest()->getParam('id');
        $form   = new Admin_Form_AdminUseredit();
        $form->getElement('email')
                ->addValidator('Db_NoRecordExists',
                            false,
                            array('table' => 'tbl_users',
                                    'field' => 'email',
                                    'exclude' => array ('field' => 'id', 'value' => $id)
                            )
                );
        $form->getElement('username')
                ->addValidator('Db_NoRecordExists',
                            false,
                            array('table' => 'tbl_users',
                                    'field' => 'username',
                                    'exclude' => array ('field' => 'id', 'value' => $id)
                            )
                );
        if ($this->getRequest()->isPost()) {
            $request = $this->getRequest();
            //if($_SERVER['REMOTE_ADDR'] == '192.168.1.81') { echo '<pre>';print_r($this->getRequest()->getPost());print_r($_FILES);exit; }
            if ($form->isValid($request->getPost())) {
                $formData = $request->getPost();
                //if($_SERVER['REMOTE_ADDR'] == '192.168.1.81') { echo '<pre>';print_R($formData);exit; }
                $user_type = $formData['user_type'];
                $user   = new Admin_Model_UsersMapper();
                $UserID = $user->save($formData);
                if(!empty($_FILES['profile_pic_path']['name'])) {
                    //if($_SERVER['REMOTE_ADDR'] == '192.168.1.81') { echo $UserID.'<pre>';print_r($formData);print_r($_FILES);exit; }
                    $dir_upload = UPLOAD_PATH.'images/original/';
                    $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                    $medium_path = UPLOAD_PATH.'images/medium/';
                    $mobile_path = UPLOAD_PATH.'images/mobile/';
                    $fileName = $_FILES['profile_pic_path']['name'];
                    $image = new UpmeSocial_Controller_Action_Helper_Image();
                    $arrFileName = $image->saveImage($dir_upload,$thumb_path,$medium_path,$mobile_path);
                    $updateAry['profile_pic_path'] = $arrFileName['profile_pic_path'];
                    $album['user_id'] = $UserID;
                    $album['user_type'] = 'U';
                    $album['album_name'] = 'Profile Photos';
                    $album['is_default'] = '1';
                    $albumobj = new Mobile_Model_Albums();
                    //$albumobj->albumnameexistsrnot($album);
                    $album['created_date'] = date('Y-m-d H:i:s');
                    $albumid = $albumobj->insert($album);
                    $photoObj = new Mobile_Model_Photos();
                    $coverphoto = $photoObj->albumidexistsrnot($albumid,$UserID,'U');
                    $data['album_id'] = $albumid;
                    $data['photo_name'] = 'Profile Photo';
                    $data['photo_original_name'] = $fileName;
                    $data['photo_modified_name'] = $arrFileName['profile_pic_path'];
                    $data['user_id'] = 'U';
                    $data['user_type'] = $user_type;
                    if(!empty($coverphoto['photo_id']))
                        $data['cover_photo'] = '0';
                    else
                        $data['cover_photo'] = '1';
                    $data['created_date'] = date('Y-m-d H:i:s');
                    $photo_id = $photoObj->insert($data);
                    //echo '<pre>';print_r($updateAry);exit;
                    $id = $user->save($updateAry, $UserID);
                }
                if(!empty($UserID))
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been updated Successfully'));
                $this->_helper->redirector('index','users','admin');
            }
        }
        $this->view->form = $form;
    }
    
    public function viewuserAction() {
        $id = $this->getRequest()->getParam('id');
        $this->view->assign('page_Name',"usersmanagement");
        if($id !='') {
            $users  = new Admin_Model_UsersMapper();
            $result = $users->fetchRowByUsertID($id);
            $userlevels  = new Admin_Model_UserlevelsMapper();
            $levels = $userlevels->fetchNameByLevelno($result['user_level']);
            $coolpoints  = new User_Model_User();
            $usercoolpoints = $coolpoints->getUserCoolPoints($id,'U');
            $scribbles  = new Scribbles_Model_Scribbles();
            $scribblescnt = $scribbles->getScribbleCnt($id,'U');
            $this->view->result = $result;
            $this->view->levels = $levels;
            $this->view->coolpoints = $usercoolpoints;
            $this->view->scribblescnt = $scribblescnt;
        }
    }

    public function ajaxstatesAction() {
        $this->view->assign('page_Name',"usersmanagement");
        $id =$this->getRequest()->getParam('id');
        $user = new Admin_Model_UsersMapper();
        $result = $user->fetchRowByUsertID($id);
        $form = new Admin_Form_AdminUseredit();
        $this->view->form = $form;
    }

    public function celebrityusersAction() {
        $request = $this->getRequest();
        $users = new Admin_Model_UsersMapper();
        $result = $users->getcelebrities();
        if($request->getParam('userviewActions') != '')
            $sort = $this->getRequest()->getParam('userviewActions');
        else
            $sort = "";
        $isAjaxReq  = $this->getRequest()->isXmlHttpRequest();
        $keyword= $this->getRequest()->getParam('search');
        if($isAjaxReq) {
            $this->_helper->layout->disableLayout();
            if($keyword !='') {
                $result = $users->fetchCelebritySortresults($keyword);
            } else if($sort !='') {
                if($sort == 'MR') {
                    $result = $users->fetchCelebritySortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $users->fetchCelebritySortresults('',$sort);
                } else {
                    $result = $users->getcelebrities();
                }
            } else
                $result = $users->getcelebrities();
        } else {
            if($keyword !='') {
                $result = $users->fetchCelebritySortresults($keyword);
            } else if($sort != '') {
                if($sort == 'MR') {
                    $result = $users->fetchCelebritySortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $users->fetchCelebritySortresults('',$sort);
                }
            } else {
                $result = $users->getcelebrities();
            }
        }
        $page = $this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($result);
        $paginator->setItemCountPerPage('10');
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(2);
        $this->view->keyword = $keyword;
        $this->view->assign('sort',$sort);
        $this->view->assign('isAjaxReq',$isAjaxReq);
        $this->view->assign('page_Name',"registeredcelebrityusersmanagement");
        $this->view->paginator=$paginator;
    }

} ?>