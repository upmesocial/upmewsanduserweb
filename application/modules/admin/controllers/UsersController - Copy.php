<?php class Admin_UsersController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->ajaxContext->addActionContext('index', 'html')
                                ->initContext();
    }
    public function preDispatch() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if($auth ->hasIdentity()) {
        } else {
            if ('index' != $this->getRequest()->getActionName() || 'index' != $this->getRequest()->getControllerName()) {
                $this->_redirect('/admin/index');
            }
        }
    }
    public function indexAction()
    {
        $request    = $this->getRequest();
        $users = new Admin_Model_UsersMapper();
        $updatearr = array();
        if($request->getParam('hid_key') == 'Delete'){ 
            $id = $request->getParam('hid_id');
            $result = $mapper->delete($id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been deleted Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('hid_key') == 'change'){ 
            $updatearr['admin_status'] = $request->getParam('hid_status');
            $id = $request->getParam('hid_id');
            $result = $mapper->update($updatearr, $id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record Status has been Updated Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('adminActions') != ''){
            $idsArr  = array();
            $idsArr1 = array();
            $idsArr  = $this->getRequest()->getParam('checkusers');
            $cnt = sizeof($idsArr);
            for($i=0;$i<$cnt;$i++){
                $idsArr1[$i] = "'".$idsArr[$i]."'";
            }
            $ids     = implode(',', $idsArr1);
            if($this->getRequest()->getParam('adminActions') == 'A')
                    $updatearr['admin_status'] = 'A';
            elseif($this->getRequest()->getParam('adminActions') == 'I')
                    $updatearr['admin_status'] = 'I';
            $where      = "user_id in (".$ids.")";
            if($this->getRequest()->getParam('adminActions') == 'D'){    
                $result      = $mapper->deleteall($where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) has been Deleted Successfully'));
                return $this->_helper->redirector('index');
            }else{   
                $result = $mapper->updateall($updatearr, $where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) Status has been Updated Successfully'));
                return $this->_helper->redirector('index');
            }    
        }
        if($request->getParam('userviewActions') != '')
            $sort = $this->getRequest()->getParam('userviewActions');
        else if($request->getParam('order') != '')
            $sort = $this->getRequest()->getParam('order');
        else
            $sort = "";		
        $isAjaxReq  = $this->getRequest()->isXmlHttpRequest();
        $keyword= $this->getRequest()->getParam('search');
        if($isAjaxReq) {
            $this->_helper->layout->disableLayout();
            if($keyword !='') {
                $result = $users->fetchSortresults($keyword);
            }else if($sort !=''){
                if($sort == 'MR'){
                    $result = $users->fetchSortresults('',$sort);
                }	
                elseif($sort == 'AO'){
                    $result = $users->fetchSortresults('',$sort);
                }else{
                    $result = $users->fetchAll();
                }
            }else
                $result = $users->fetchAll();
        }else{
            if($sort != ''){
                if($sort == 'MR'){
                    $result = $users->fetchSortresults('',$sort);
                }elseif($sort == 'AO'){
                    $result = $users->fetchSortresults('',$sort);
                }
            }else{
                $result = $users->fetchAll();
            }
        }
        $page = $this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($result);		
        $paginator->setItemCountPerPage('10');
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(2);
        $this->view->keyword = $keyword;
        $this->view->assign('sort',$sort);
        $this->view->assign('isAjaxReq',$isAjaxReq);        
        $this->view->assign('page_Name',"usersmanagement");
        $this->view->paginator=$paginator;
    }
    public function edituserAction() { 
        $request = $this->getRequest();
        $this->view->assign('page_Name',"usersmanagement");
        $countriesobj         = new User_Model_Countries();
        $userObj = new User_Model_User();
        $countries            = $countriesobj->get_countries();
        $id =$this->getRequest()->getParam('id');
        if($id !='') {
            $user   = new Admin_Model_UsersMapper();
            $row2   = $user->fetchRowByUsertID($id);
            //$form   = new Admin_Form_AdminUseredit();
            if($request->getParam('email') != '') {
                //check email existancy
                $userEmail = $this->getRequest()->getParam('email');
                if(!$userObj->isEmailAvail($userEmail)) {
                    $resultAry = array('service_status'=>'error',"content" => 'Email not available');
                    echo json_encode($resultAry);
                    return false;
                }
            }
            if($request->getParam('username') != '') {
                //check username existance
                $userName = $this->getRequest()->getParam('username');
                if(!$userObj->isUsernameAvail($userName)) {
                    $resultAry = array('service_status'=>'error',"content" => 'Username not available');
                    echo json_encode($resultAry);
                    return false;
                }
            }
            if (!empty($request)) {echo 'if';
                $request= $this->getRequest(); echo "<prE>";print_r($request);
                $newName = '';
                if(sizeof($request->profile_pic_path) != 0) {
                    $originalFilename = pathinfo($request->profile_pic_path);echo $originalFilename;exit;
                    if($originalFilename['extension']=='jpeg')
                        $originalFilename['extension']='jpg';
                    $newName = time() .'.'. strtolower($originalFilename['extension']);
                    $request->profile_pic_path->addFilter('Rename', $newName);
                    if($request->profile_pic_path->receive()) {
                        $fullPathNameFile = UPLOAD_PATH.'/images/original/'.$newName;
                        $image = new UpmeSocial_Controller_Action_Helper_Image();
                        $source = UPLOAD_PATH.'/images/original';
                        $medium = UPLOAD_PATH.'/images/medium';
                        $destination = UPLOAD_PATH.'/images/thumbnails';
                        $largethumbpath = UPLOAD_PATH.'/images/original';
                        /*** For getting large thumb ***/
                        list($largethumb_width,$largethumb_height) = $image->getNewDimensions($fullPathNameFile,800,600);
                        $image->ImageSize($largethumbpath,$newName,$largethumb_width,$largethumb_height,$source);
                        /*** For getting medium thumb ***/
                        list($medium_width,$medium_height) = $image->getNewDimensions($fullPathNameFile,180,391,false);
                        $image->ImageSize($medium,$newName,$medium_width,$medium_height,$source);
                        /*** For getting small thumb ***/
                        $thumb_width  = 100;
                        $thumb_height = 100;
                        list($thumb_width,$thumb_height) = $image->getNewDimensions($fullPathNameFile,100,100,false);
                        $image->cropImage($destination,$newName,$thumb_width,$thumb_height,$source);
                    }
                }
                $formData   = $request->getParams();echo '<pre>';print_r($formData);exit;
                $formData['photo'] = $newName;
                if($formData['photo'] != '') {
                    @unlink(UPLOAD_PATH.'images/original/'.$row2['photo']);
                    @unlink(UPLOAD_PATH.'images/thumbnails/'.$row2['photo']);
                    @unlink(UPLOAD_PATH.'images/medium/'.$row2['photo']);
                }
                $user->saveUser($formData, $row2->user_id);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been updated Successfully'));
                $this->_helper->redirector('index','users','admin');
            }echo 'else';exit;
        }
        $this->view->result   = $row2;
        $this->view->countries   = $countries;
    }
    public function viewuserAction() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $this->getRequest()->getParam('id');
        $this->view->assign('page_Name',"usersmanagement");
        if($id !='') {
            $users  = new Admin_Model_UsersMapper();
            $result = $users->fetchRowByUsertID($id);
            $this->view->result = $result;
        }
    }
    public function ajaxstatesAction()
    {
        $this->view->assign('page_Name',"usersmanagement");
        $id =$this->getRequest()->getParam('id');
        $user   = new Admin_Model_UsersMapper();
        $result   = $user->fetchRowByUsertID($id);
        $form   = new Admin_Form_AdminUseredit();
        $this->view->form   = $form;
    }
} 
?>