<?php
class Admin_HelpvideosController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->ajaxContext->addActionContext('index', 'html')->initContext();
    }

    public function preDispatch() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('adminuser'));
        if ($auth->hasIdentity()) { 
            
        } else {
            if ('index' != $this->getRequest()->getActionName() || 'index' != $this->getRequest()->getControllerName()) {
                $this->_redirect('/admin/index');
            }
        }
        $this->view->assign('page_Name',"helpvideos");
    }

    public function indexAction() {
        $db = Zend_Db_Table::getDefaultAdapter();
        $request = $this->getRequest();
        $mapper = new Admin_Model_HelpVideosMapper();
        $updatearr = array();
        if($request->getParam('hid_key') == 'Delete') {
            $id = $request->getParam('hid_id');
            $result = $mapper->delete($id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record has been deleted Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('hid_key') == 'change') {
            $updatearr['status'] = $request->getParam('hid_status');
            $id = $request->getParam('hid_id');
            $result = $mapper->update($updatearr, $id);
            $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record Status has been Updated Successfully'));
            return $this->_helper->redirector('index');
        }
        if($request->getParam('adminActions') != '') {
            $idsArr  = array();
            $idsArr1 = array();
            $idsArr  = $this->getRequest()->getParam('checkhelpvideos');
            $cnt = sizeof($idsArr);
            for($i=0;$i<$cnt;$i++) {
                $idsArr1[$i] = "'".$idsArr[$i]."'";
            }
            $ids     = implode(',', $idsArr1);
            if($this->getRequest()->getParam('adminActions') == 'A')
                    $updatearr['status'] = '1';
            elseif($this->getRequest()->getParam('adminActions') == 'I')
                    $updatearr['status'] = '0';
            $where      = "id in (".$ids.")";
            if($this->getRequest()->getParam('adminActions') == 'D') {
                $result      = $mapper->deleteall($where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) has been Deleted Successfully'));
                return $this->_helper->redirector('index');
            } else {
                $result = $mapper->updateall($updatearr, $where);
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Record(s) Status has been Updated Successfully'));
                return $this->_helper->redirector('index');
            }    
        }
        if($request->getParam('userviewActions') != '')
            $sort = $this->getRequest()->getParam('userviewActions');
        else if($request->getParam('order') != '')
            $sort = $this->getRequest()->getParam('order');
        else
            $sort = "";		
        $isAjaxReq  = $this->getRequest()->isXmlHttpRequest();
        $keyword= $this->getRequest()->getParam('search');
        if($isAjaxReq) {
            $this->_helper->layout->disableLayout();
            if($keyword !='') {
                $result = $mapper->fetchSortresults($keyword);
            } else if($sort !='') {
                if($sort == 'MR') {
                    $result = $mapper->fetchSortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $mapper->fetchSortresults('',$sort);
                } else {
                    $result = $mapper->fetchAll();
                }
            } else
                $result = $mapper->fetchAll();
        } else {
            if($sort != '') {
                if($sort == 'MR') {
                    $result = $mapper->fetchSortresults('',$sort);
                } elseif($sort == 'AO') {
                    $result = $mapper->fetchSortresults('',$sort);
                }
            } else {
                $result = $mapper->fetchAll();
            }
        }
        $page = $this->_getParam('page',1);
        $paginator = Zend_Paginator::factory($result);		
        //$paginator->setItemCountPerPage(ADMIN_PAGINATION_LIMIT);
        $paginator->setItemCountPerPage('10');
        $paginator->setCurrentPageNumber($page);
        $paginator->setPageRange(2);
        $this->view->keyword = $keyword;
        $this->view->assign('sort',$sort);
        $this->view->assign('isAjaxReq',$isAjaxReq);        
        $this->view->assign('page_Name',"helpvideosmanagement");
        $this->view->paginator = $paginator;
    }

    public function addhelpvideoAction() {
        $request = $this->getRequest();
        $form = new Admin_Form_AdminHelpVideos();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                //echo '<Pre>';print_r($request->getPost());exit;
                $dataObj = new Admin_Model_HelpVideos();
                $mapper = new Admin_Model_HelpVideosMapper();
                $mapper->save($request->getPost());
                $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>New Record has been added Successfully'));
                $this->_helper->redirector('index','helpvideos','admin');
            }
        }
        $this->view->assign('page_Name',"addhelpvideo");
        $this->view->form = $form;
    }

    public function edithelpvideoAction() {
        $request = $this->getRequest();
        $id =$this->getRequest()->getParam('id');
        if($id !='') {
            $helpvideos = new Admin_Model_HelpVideosMapper();
            $row = $helpvideos->find($id);
            $form    = new Admin_Form_AdminHelpVideos();
            $form->populate($row->toArray());
//            $form->getElement('iphone_path')->addValidator('Db_NoRecordExists',false,array('table' => 'tbl_help_videos',
//                                                                                            'field' => 'iphone_path',
//                                                                                            'exclude' => array ('field' => 'id', 'value' => $id)));
//            $form->getElement('android_path')->addValidator('Db_NoRecordExists',false,array('table' => 'tbl_help_videos',
//                                                                                            'field' => 'android_path',
//                                                                                            'exclude' => array ('field' => 'id', 'value' => $id)));
//            $form->getElement('web_path')->addValidator('Db_NoRecordExists',false,array('table' => 'tbl_help_videos',
//                                                                                            'field' => 'web_path',
//                          
//                                                                                                                                                              'exclude' => array ('field' => 'id', 'value' => $id)));
            if ($this->getRequest()->isPost()) {
                if ($form->isValid($request->getPost())) {
                    $dataObj = new Admin_Model_HelpVideos();
                    //echo "<pre>";print_r($dataObj);exit;
                    $mapper = new Admin_Model_HelpVideosMapper();
                    $mapper->save($form->getValues());
                    $this->_helper->flashMessenger->addMessage(array('alert alert-success'=>'<h4 class="alert-heading">Success!</h4>Your updates have been saved'));
                    return $this->_helper->redirector('index');
                }
            }
            $this->view->form = $form;
        }
    }

} ?>