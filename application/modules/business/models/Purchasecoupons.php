<?php
class Business_Model_Purchasecoupons {

	protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Business_Model_DbTable_Purchasecoupons');
                }
                return $this->_dbTable;
        }

        public function insert($data) { 
                $res = $this->getDbTable()->insert($data);
                return $res;
        }

        public function getpurchasecouponsinfo($user_id, $pagenum='', $limit='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select    = $db->select()
                                ->from(array('P'=>'tbl_purchase_coupons'),array('user_id','coupon_id','purchase_date'))
                                ->joinLeft(array('C' => 'tbl_coupons'),"C.coupon_id = P.coupon_id", array('coupon_id','title','description','coupon_price','expiration_date','image_path as coupon_image','business_id','original_price','discounted_price'))
                                ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','lat','lng'))
                                ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                                ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                                ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                                ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                                ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                                ->where("P.user_id =?",$user_id)
                                ->where("C.status = '1'")
                                ->order("P.purchase_date DESC");
                
                if(($pagenum!='' || $pagenum == 0) && $limit!=''){
                    $offset = $pagenum * $limit;
                    $select->limit($limit, $offset);
                }
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                foreach($resultSet as $key=>$coupon) {
                    $cnt = $this->purchasedcouponcount($coupon['coupon_id']);
                    $resultSet[$key]['couponcnt'] = $cnt['couponscnt'];
                }
                return $resultSet;
        }

        public function getactivepurchasecouponsinfo($user_id) {
                $date = date('Y-m-d H:i:s');
                $db = Zend_Db_Table::getDefaultAdapter();
                $select    = $db->select()
                                ->from(array('P'=>'tbl_purchase_coupons'),array('user_id','coupon_id as purchasecoupon_id','purchase_date'))
                                ->joinLeft(array('C' => 'tbl_coupons'),"C.coupon_id = P.coupon_id", array('coupon_id','title','description','expiration_date','image_path as coupon_image','business_id','coupon_price','original_price','discounted_price'))
                                ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','zipcode'))
                                ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                                ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                                ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                                ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                                ->where("P.user_id =?",$user_id)
                                ->where("C.expiration_date > ?",$date)
                                ->where("C.status = 1")
                                ->group("C.coupon_id");
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                foreach($resultSet as $key=>$coupon) {
                    $cnt = $this->purchasedcouponcount($coupon['purchasecoupon_id']);
                    $resultSet[$key]['couponcnt'] = $cnt['couponscnt'];
                }
                return $resultSet;
        }

        public function getallpurchasecouponsinfo() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('P'=>'tbl_purchase_coupons'),array('user_id','coupon_id as purchasecoupon_id','purchase_date'))
                                ->joinLeft(array('C' => 'tbl_coupons'),"C.coupon_id = P.coupon_id", array('coupon_id','title','description','coupon_price','expiration_date','image_path as coupon_image','business_id','original_price','discounted_price'))
                                ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','zipcode','lat','lng'))
                                ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                                ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                                ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                                ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                                ->where("C.status = 1");
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                foreach($resultSet as $key=>$coupon) {
                    $cnt = $this->purchasedcouponcount($coupon['purchasecoupon_id']);
                    $resultSet[$key]['couponcnt'] = $cnt['couponscnt'];
                }
                return $resultSet;
        }

        public function getmycustomersinfo($business_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select    = $db->select()
                                ->from(array('P'=>'tbl_purchase_coupons'),array('user_id','coupon_id'))
                                ->joinLeft(array('C'=>'tbl_coupons'),"C.coupon_id = P.coupon_id",array('business_id','coupon_id'))
                                ->joinLeft(array('U' => 'tbl_users'),"P.user_id = U.user_id", array('firstname','lastname','username', 'realname' => 'CONCAT(U.firstname, " ",U.lastname)','description','profile_pic_path'))
                                ->where("C.business_id =?",$business_id)
                                ->where("C.status = 1")
                                ->group('U.user_id');
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function purchasedcouponcount($coupon_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('P' => 'tbl_purchase_coupons'), array("COUNT('P.purchase_id') as couponscnt"))
                            ->where('P.coupon_id = ?',$coupon_id);
                //echo $select;exit;
                $cnt = $db->fetchrow($select);
                return $cnt;
        }

}