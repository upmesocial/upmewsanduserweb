
<?php
class Business_Model_Followers {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                       throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Business_Model_DbTable_Followers');
                }
                return $this->_dbTable;
        }

        //function to get all users who are following
        function getFollowers($BusinessId, $loggedBusinessId, $businessType = 'B', $limit = '', $offset = 0) {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($limit != '') {
                        $limit = $limit;
                        $offset = $offset;
                }
                //echo $BusinessId.'-->'.$businessType;exit;
                if($loggedBusinessId == $BusinessId) {
                        $select = $db->select()
                                    ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path'))
                                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                                    ->where('F.follower_id = ?',$BusinessId)
                                    //->orwhere('F.user_id = ?',$userId)
                                    ->where('F.follower_type = ?',$businessType)
                                    ->order ('F.created_date DESC')
                                    ->limit($limit,$offset);
                } else {
                        $select = $db->select()
                                    ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path'))
                                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                                    ->where('F.follower_id = ?',$BusinessId)
                                    //->orwhere('F.user_id = ?',$userId)
                                    ->where('F.follower_type = ?',$businessType)
                                    ->order ('F.created_date DESC')
                                    ->limit($limit,$offset);
                }
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) { //echo '<pre>';print_r($resultSet);exit;
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['name'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['name'] = $resultSet[$k]['firstname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        //function to get all users who are following
        function getFollowing($businessId,$businessType = 'B', $limit = '') {
                $db = Zend_Db_Table::getDefaultAdapter();
                 if($limit != ''){
                    $limit = $limit;
                 }
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.user_id = ?',$businessId)
                        ->where('F.user_type = ?',$businessType)
                        ->where('F.follower_type IN("U", "B")')
                        ->order ('F.created_date DESC')
                        ->limit($limit);
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['follower_type']  == 'B') {
                                $ary['name'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['follower_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type']  == 'U') {
                                $ary['name'] = $resultSet[$k]['firstname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        //function to get count all users who are following
        function getFollowersCnt($businessId,$businessType = 'B') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('followerCnt'=>'count(F.user_id)'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array(''))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                        ->where('F.follower_id = ?',$businessId)
                        ->where('F.follower_type = ?',$businessType)
                        ->order ('F.created_date DESC');
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                //echo "<pre>";print_r($resultSet);exit;
                return $resultSet['0']['followerCnt'];
        }

        //function to get count of all users who are following
        function getFollowingCnt($businessId,$businessType = 'B') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('followingCnt'=>'count(F.user_id)'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array(''))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type = 'U' ", array(''))
                        ->where('F.user_id = ?',$businessId)
                        ->where('F.user_type = ?',$businessType)
                        ->order ('F.created_date DESC');
                  $resultSet = $db->fetchAll($select);
                //echo "<pre>";print_r($resultSet);exit;
                return $resultSet['0']['followingCnt'];
        }

        public function getFollowerIds($userId,$userType = 'U') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.follower_id = ?',$userId)
                        ->where('F.follower_type = ?',$userType)
                        ->order ('F.created_date DESC');
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        //Insert New Record to Database
        public function insertNewRecord($data){
            return $this->getDbTable()->insert($data);
        }
        
        public function deleteFriend($data){
            if($_SERVER['REMOTE_ADDR'] =='192.168.1.57'){
                //echo "<pre>";print_r($data);exit;
            }
            $db = Zend_Db_Table::getDefaultAdapter();
            $where = "F.user_id = '".$data['user_id']."' AND F.user_type = '".$data['user_type']."' AND F.follower_id = '".$data['follower_id']."' AND F.follower_type = '".$data['follower_type']."'";
            //$id = $db->delete('tbl_followers','follower_id= "'.$data['follower_id'].'" AND follower_type = "'.$data['follower_type'].'" AND user_id  ="'.$data['user_id'].'" AND user_type ="'.$data['user_type'].'"');
            $select = $db->query("DELETE F.*, N.* FROM tbl_followers F
                                LEFT JOIN tbl_notifications N ON (N.reference_id = F.followers_id AND (N.notifications_type = 'Followed' OR N.notifications_type = 'following'))
                                WHERE ".$where);
            $id = $select->execute();
            return $id;
        }
        
        public function isLoggedUserFollowingOtherUser($uid, $userType) { 
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            if($auth->hasIdentity()){
                $loggedUserId = $auth->getIdentity()->user_id;
                $loggedUserType = $auth->getIdentity()->user_type;
            } else {
                $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
                if($auth->hasIdentity()) {
                    $loggedUserId = $auth->getIdentity()->business_id;
                    $loggedUserType = $auth->getIdentity()->user_type;
                }
            }
            if($loggedUserType != 'B')
                $loggedUserType = "U";
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','follower_id','user_type','follower_type'))
                        ->where('F.follower_id= "'.$uid.'" AND F.follower_type = "'.$userType.'" AND F.user_id  ="'.$loggedUserId.'" AND F.user_type ="'.$loggedUserType.'"');
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getFriendsList($user_id,$user_type,$searchword) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','bfname'=>'firstname','blname'=>'lastname','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.follower_id = ?',$user_id)
                        ->where('F.follower_type = ?',$user_type)
                        ->where('B.firstname LIKE "'.$searchword.'%" OR B.lastname LIKE "'.$searchword.'%" OR B.business_name LIKE "'.$searchword.'%" OR U.firstname LIKE "'.$searchword.'%" OR U.lastname LIKE "'.$searchword.'%"');
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                //echo '<pre>';print_R($resultSet);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['firstname'] = $resultSet[$k]['bfname'];
                                $ary['lastname'] = $resultSet[$k]['blname'];
                                $ary['image'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['image'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }

        function getFollowingbusiness($businessId,$businessType = 'B', $limit = '') {
                $db = Zend_Db_Table::getDefaultAdapter();
                if($limit != '') {
                    $limit = $limit;
                }
                $select = $db->select()
                            ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','image_path','firstname','lastname'))
                            ->where('F.user_id = ?',$businessId)
                            ->where('F.user_type = ?',$businessType)
                            ->where('F.follower_type = ?',$businessType)
                            ->order ('F.created_date DESC')
                            ->limit($limit);
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['follower_type']  == 'B') {
                                $ary['businessname'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['follower_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        }
                }
                return $result;
        }
        
        public function getFollowersByUserID($userId, $userType,$limit='') {
            $db = Zend_Db_Table::getDefaultAdapter();
             if($limit != '')
                $limit = $limit;
            $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.follower_id = ?',$userId)
                        //->orwhere('F.user_id = ?',$userId)
                        ->where('F.follower_type = "U"')
                        ->order ('F.created_date DESC')
                        ->limit($limit);
            $resultSet = $db->fetchAll($select);
            $result = array();
            for($k = 0; $k < count($resultSet);$k++) { //echo '<pre>';print_r($resultSet);exit;
                    if($resultSet[$k]['user_type']  == 'B') {
                            $ary['name'] = $resultSet[$k]['business_name'];
                            $ary['firstname'] = $resultSet[$k]['business_name'];
                            $ary['lastname'] = '';
                            $ary['user_id'] = $resultSet[$k]['business_id'];
                            $ary['follower_id'] = $resultSet[$k]['follower_id'];
                            $ary['user_type'] = $resultSet[$k]['user_type'];
                            $ary['image_name'] = $resultSet[$k]['image_path'];
                            $result[] = $ary;
                    } else if($resultSet[$k]['user_type']  == 'U') {
                            $ary['name'] = $resultSet[$k]['firstname'];
                            $ary['firstname'] = $resultSet[$k]['firstname'];
                            $ary['lastname'] = $resultSet[$k]['lastname'];
                            $ary['user_id'] = $resultSet[$k]['user_id'];
                            $ary['follower_id'] = $resultSet[$k]['follower_id'];
                            $ary['user_type'] = $resultSet[$k]['user_type'];
                            $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                            $result[] = $ary;
                    }
            }
            return $result;
        }
        
        function getMyFollowers($BusinessId, $loggedBusinessId, $businessType, $limit, $pagenum) {
                $db = Zend_Db_Table::getDefaultAdapter();
                //echo $BusinessId.'-->'.$businessType;exit;
                if($loggedBusinessId == $BusinessId) {
                        $select = $db->select()
                                    ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path', 'name' => 'IF(F.follower_type="B", B.business_name, U.firstname)'))
                                    ->where('F.follower_id = ?',$BusinessId)
                                    //->orwhere('F.user_id = ?',$userId)
                                    ->where('F.follower_type = ?',$businessType);
                } else {
                        $select = $db->select()
                                    ->from(array('F' => 'tbl_followers'), array('user_id','user_type','followers_id','follower_id'))
                                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.user_id AND F.user_type = 'B' ", array('business_id','business_name','image_path', 'name' => 'IF(F.follower_type="B", B.business_name, U.firstname)'))
                                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                                    ->where('F.follower_id = ?',$BusinessId)
                                    //->orwhere('F.user_id = ?',$userId)
                                    ->where('F.follower_type = ?',$businessType);
                }
                $select->order (array('name ASC','U.firstname','B.business_name','F.created_date DESC'));
                $offset = $pagenum * $limit;
                $select->limit($limit,$offset);
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) { //echo '<pre>';print_r($resultSet);exit;
                        if($resultSet[$k]['user_type']  == 'B') {
                                $ary['name'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['realname'] = $resultSet[$k]['business_name'];
                                $ary['user_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['user_type']  == 'U') {
                                $ary['name'] = $resultSet[$k]['firstname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                                $ary['user_id'] = $resultSet[$k]['user_id'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['user_type'] = $resultSet[$k]['user_type'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                
                // FOr Sorting Array which is mixed with business and User details by realname Asc ORDER
                $b = array();
                $c = array();
                 foreach($result as $k=>$v) {
                    $b[$k] = strtolower($v['realname']);
                }
                asort($b);
                foreach($b as $key=>$val) {
                    $c[] = $result[$key];
                }
                
                return $c;
        }

        //function to get all users who are following
        function getMyFollowing($businessId,$businessType, $limit, $pagenum) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','image_path'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path'))
                        ->where('F.user_id = ?',$businessId)
                        ->where('F.user_type = ?',$businessType)
                        ->where('F.follower_type IN("U", "B")')
                        ->order (array('B.business_name','F.created_date DESC'));
                $offset = $pagenum * $limit;
                $select->limit($limit,$offset);
                $resultSet = $db->fetchAll($select);
                $result = array();
                for($k = 0; $k < count($resultSet);$k++) {
                        if($resultSet[$k]['follower_type']  == 'B') {
                                $ary['name'] = $resultSet[$k]['business_name'];
                                $ary['firstname'] = $resultSet[$k]['business_name'];
                                $ary['lastname'] = '';
                                $ary['realname'] = $resultSet[$k]['business_name'];
                                $ary['follower_id'] = $resultSet[$k]['business_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['image_name'] = $resultSet[$k]['image_path'];
                                $result[] = $ary;
                        } else if($resultSet[$k]['follower_type']  == 'U') {
                                $ary['name'] = $resultSet[$k]['firstname'];
                                $ary['firstname'] = $resultSet[$k]['firstname'];
                                $ary['lastname'] = $resultSet[$k]['lastname'];
                                $ary['realname'] = $resultSet[$k]['firstname']." ".$resultSet[$k]['lastname'];
                                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                                $result[] = $ary;
                        }
                }
                // FOr Sorting Array which is mixed with business and User details by realname Asc ORDER
                $b = array();
                $c = array();
                 foreach($result as $k=>$v) {
                    $b[$k] = strtolower($v['realname']);
                }
                asort($b);
                foreach($b as $key=>$val) {
                    $c[] = $result[$key];
                }
                
                return $c;
        }
        
        public function checkDuplicationFollow($uid, $userType, $loggedUserId, $loggedUserType){
            //$uid = 1; $userType = "B";$loggedUserId = "1"; $loggedUserType = "U";
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('user_id','follower_id','user_type','follower_type'))
                        ->where('F.follower_id= "'.$uid.'" AND F.follower_type = "'.$userType.'" AND F.user_id  ="'.$loggedUserId.'" AND F.user_type ="'.$loggedUserType.'"');
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }
}
