<?php
class Business_Model_Business {
        protected $_dbTable;
        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Business_Model_DbTable_Business');
                }
                return $this->_dbTable;
        }

        public function save($businessinfo) {
                return $this->getDbTable()->insert($businessinfo);
        }

        public function updatebusiness($businessinfo, $bid) {
                if(!empty($bid)){
                        return $this->getDbTable()->update($businessinfo, array('business_id = ?' => $bid));
                }
        }

        public function getBusinessUserDetails($bId){
                $select = $this->getDbTable()->select()
                                        ->from(array('B'=>'tbl_business_users'))
                                        ->joinLeft(array('C'=>'tbl_country'),'B.country = C.id AND C.status="1"',array('country_name'))
                                        ->joinLeft(array('S'=>'tbl_state'),'B.state = S.id AND B.country = S.country_id AND S.status="1"',array('state_name'))
                                        ->joinLeft(array('CTY'=>'tbl_city'),'B.city = CTY.id AND B.state = CTY.state_id AND CTY.status = "1"',array('city_name'))
                                        ->joinLeft(array('Z'=>'tbl_zipcode'),'CTY.id = Z.city_id AND Z.status = "1"',array('zipcode as zip'))
                                        ->joinLeft(array('BC'=>'tbl_business_categories'),'BC.id = B.category_id',array('category_name'))
                                        ->where('business_id = ?',$bId)
                                        ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getAllUserDetails() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                        ->from($this->getDbTable(), array('business_id','user_type','image_path'));
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getBusinessUserImage($bId) {
                $select = $this->getDbTable()->select()
                                            ->from('tbl_business_users',array('image_path'))
                                            ->where('business_id = ?',$bId);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        //function to check businessuser existancy
        public function isUserAvail($userId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                    ->from($this->getDbTable(), 'COUNT(business_id)')
                                    ->where('business_id = ?',$userId);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0) 
                        return true;
                else
                        return false;
        }

        //function to check username existancy
        public function isUsernameAvail($username,$userId='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from($this->getDbTable(), 'COUNT(business_id)')
                                        ->where('username = ?',$username);
                if($userId != '')
                        $select->where('business_id != ?',$userId);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0)
                        return true;
                else
                        return false;
        }

        //function to check email existancy
        public function isEmailAvail($email,$userId='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                        ->from($this->getDbTable(), 'COUNT(business_id)')
                                        ->where('email = ?',$email);
                if($userId != '')
                        $select->where('business_id != ?',$userId);
                $cnt = (int) $db->fetchOne($select);
                if($cnt == 0)
                        return true;
                else
                        return false;
        }
        
        public function getMyFollowerStatistics($business_id, $weekCnt){
                $db = Zend_Db_Table::getDefaultAdapter();
                $now = date('Y-m-d H:i:s');
                $weekAry = array();
                for($i=1;$i<=$weekCnt;$i++){
                    $j = $i-1;
                    $x = date("Y-m-d H:i:s",strtotime("-$i week"));
                    $y = date("Y-m-d  H:i:s",strtotime("-$j week"));
                    // get Count of Last Week Followers
                    $select1 = $db->select()
                                    ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                                    ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.created_date BETWEEN '".$x."' AND '".$y."'");
                    $result = $db->fetchAll($select1);
                    $weekAry[$i] = $result[0]['cnt'];
                }
                return $weekAry;
        }
        
        public function getTotalSalesStatistics($business_id, $weekCnt) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $now = date('Y-m-d H:i:s');
                $weekAry = array();
                for($i=1;$i<=$weekCnt;$i++) {
                    $j = $i-1;
                    $x = date("Y-m-d H:i:s",strtotime("-$i week"));
                    $y = date("Y-m-d  H:i:s",strtotime("-$j week"));
                    // get Count of Last Week Followers
                    $select1 = $db->select()
                                    ->from(array('C'=>'tbl_coupons'), array(''))
                                    ->joinInner(array('PC'=>'tbl_purchase_coupons'),'PC.coupon_id = C.coupon_id',array('cnt' => 'COUNT(PC.coupon_id)'))
                                    ->where("C.business_id='".$business_id."' AND PC.purchase_date BETWEEN '".$x."' AND '".$y."'");
                    $result = $db->fetchAll($select1);
                    $weekAry[$i] = $result[0]['cnt'];
                }
                //echo '<pre>';print_r($weekAry);//exit;
                return $weekAry;
        }
        
        public function getCouponClickStatistics($business_id, $weekCnt) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $now = date('Y-m-d H:i:s');
                $weekAry = array();
                for($i=1;$i<=$weekCnt;$i++) {
                    $j = $i-1;
                    $x = date("Y-m-d H:i:s",strtotime("-$i week"));
                    $y = date("Y-m-d  H:i:s",strtotime("-$j week"));
                    // get Count of Last Week Followers
                    $select1 = $db->select()
                                    ->from(array('C'=>'tbl_coupons'), array(''))
                                    ->joinInner(array('PC'=>'tbl_purchase_coupons'),'PC.coupon_id = C.coupon_id',array('couponcnt' => 'COUNT(PC.coupon_id)'))
                                    ->where("C.business_id='".$business_id."' AND PC.purchase_date BETWEEN '".$x."' AND '".$y."'");
                    
                    //echo $select1;exit;
                    $result = $db->fetchAll($select1);
                    $weekAry[$i] = $result[0]['couponcnt'];
                }
                return $weekAry;
        }
        
        public function getBusinessUserexistOrNot($bId){
                $select = $this->getDbTable()->select()
                                            ->from(array('B'=>'tbl_business_users'), array('business_id'))
                                            ->where('business_id = ?',$bId)
                                            ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }
        
        public function getLngLatByBusinessId($business_id) {
                $select = $this->getDbTable()->select()
                                        ->from(array('B'=>'tbl_business_users'), array('lng', 'lat'))
                                        ->where('business_id = ?',$business_id)
                                        ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getpopularbusiness($pagenum='', $limit='') {
                /*SELECT B.business_id, count(PC.coupon_id) as cnt FROM `tbl_business_users` B
                    LEFT JOIN tbl_coupons C ON C.business_id = B.business_id
                    LEFT JOIN tbl_purchase_coupons PC ON PC.coupon_id = C.coupon_id
                    WHERE 1
                    Group BY B.business_id
                    ORDER BY cnt Desc*/
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('B' => 'tbl_business_users'),array('business_id','business_name','description','category_id','username','email','firstname','lastname','address_line_1','address_line_2','image_path','user_type','lat','lng','business_created_date'))
                                ->joinLeft(array('Z' => 'tbl_zipcode'), "B.zipcode = Z.id", array('zipcode'))
                                ->joinLeft(array('C' => 'tbl_coupons'), "B.business_id = C.business_id", array(''))
                                ->joinLeft(array('PC' => 'tbl_purchase_coupons'), "PC.coupon_id = C.coupon_id", array('businesscnt' => 'COUNT(PC.coupon_id)'))
                                ->group('B.business_id')
                                ->order('businesscnt DESC');
                if(($pagenum!= '' || $pagenum = 0) && $limit!= '') {
                    $offset = $pagenum * $limit;
                    $select->limit($limit, $offset);
                }
                $resultset = $db->fetchAll($select);
                return $resultset;
        }

        public function getHotSpots($lat='', $lng='', $limit='') {
            $db = Zend_Db_Table::getDefaultAdapter();            
            
            $where ="1=1 AND (((acos(sin((".$lat."*pi()/180)) * sin((B.lat*pi()/180))+cos((".$lat."*pi()/180)) * cos((B.lat*pi()/180)) * cos(((".$lng."- B.lng)*pi()/180))))*180/pi())*60*1.1515) < 50";
            $select = "SELECT COUNT(PC.coupon_id) AS `couponcnt`, PC.coupon_id, C.image_path as coupon_Image,  B.business_id, B.business_name, B.image_path, B.user_type, B.username, B.lat, B.lng, B.zipcode, address_line_1, address_line_2, (((acos(sin((".$lat."*pi()/180)) * sin((B.lat*pi()/180))+cos((".$lat."*pi()/180)) * cos((B.lat*pi()/180)) * cos(((".$lng."- B.lng)*pi()/180))))*180/pi())*60*1.1515) as distance  
                            FROM tbl_business_users AS B 
                            INNER JOIN tbl_coupons AS C ON B.business_id = C.business_id
                            INNER JOIN tbl_purchase_coupons AS PC ON PC.coupon_id = C.coupon_id
                            WHERE ".$where." 
                            GROUP BY C.coupon_id
                            ORDER BY couponcnt DESC, distance ASC";
            if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                //echo $select;exit;
            }
            
            if($limit != ''){
                $select = $select." LIMIT 0, ".$limit;
            }
            //echo $select;exit;
            $businessArray = $db->fetchAll($select);
            if(empty($businessArray))
                $businessArray = array();

            return $businessArray;
        }
        
        public function getTotalUsersInAreaStatistics($zipcode, $weekCnt) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $now = date('Y-m-d H:i:s');
            $where = " (U.zipcode= '".$zipcode."' AND U.user_account_status !='D' AND U.user_account_status !='I')";     
            $weekAry = array();
            for($i=1;$i<=$weekCnt;$i++) {
                $j = $i-1;
                $x = date("Y-m-d H:i:s",strtotime("-$i week"));
                $y = date("Y-m-d  H:i:s",strtotime("-$j week"));
                // get Count of Last Week Followers
                $select1 = $db->select()
                                ->from(array('U' => 'tbl_users'),array('userCnt'=>'count(U.user_id)', 'zipcode'))
                                ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array(''))
                                ->where("U.created_date BETWEEN '".$x."' AND '".$y."' AND ".$where)
                                ->order(array('U.zipcode ASC','CP.cool_points DESC'));
                $result = $db->fetchAll($select1);
                if($_SERVER['REMOTE_ADDR'] == '182.72.66.214'){
                   //echo "<pre>".$select1;print_r($result);//exit;
                }
                $weekAry[$i] = $result[0]['userCnt'];
            }
            return $weekAry;
        }
        
        public function checkQrAvailable($code) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $this->getDbTable()->select()
                                     ->from($this->getDbTable(), array('business_id', 'user_type'))
                                    ->where("qrcode = '".$code."'");
            //echo $select;exit;
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }
        
        public function updateQRCode($business_id, $code) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $updArr['qrcode'] = $code;
            $db->update('tbl_business_users' ,$updArr, "business_id = '".$business_id."'");
        }
        
        public function getTopBusinessHotSpots($lat='', $lng='', $limit='') {
            $db = Zend_Db_Table::getDefaultAdapter();            
            
            $where ="1=1";
            $select = "SELECT COUNT(PC.coupon_id) AS `couponcnt`, PC.coupon_id, C.image_path as coupon_Image,  B.business_id, B.business_name, B.image_path, B.user_type, B.username, B.lat, B.lng, B.zipcode, address_line_1, address_line_2, (((acos(sin((".$lat."*pi()/180)) * sin((B.lat*pi()/180))+cos((".$lat."*pi()/180)) * cos((B.lat*pi()/180)) * cos(((".$lng."- B.lng)*pi()/180))))*180/pi())*60*1.1515) as distance  
                            FROM tbl_business_users AS B 
                            INNER JOIN tbl_coupons AS C ON B.business_id = C.business_id
                            INNER JOIN tbl_purchase_coupons AS PC ON PC.coupon_id = C.coupon_id
                            WHERE ".$where." 
                            GROUP BY C.coupon_id
                            ORDER BY couponcnt DESC, distance ASC";
            if($_SERVER['REMOTE_ADDR'] == '192.168.1.63') {
                //echo $select;exit;
            }
            
            if($limit != ''){
                $select = $select." LIMIT 0, ".$limit;
            }
            //echo $select;exit;
            $businessArray = $db->fetchAll($select);
            if(empty($businessArray))
                $businessArray = array();

            return $businessArray;
        }
        
        public function followBusiness($qrcode) {
            
        }
        
        public function getMyFollowerAnalyticsData($business_id, $lastMonth, $uptoToday){
                $db = Zend_Db_Table::getDefaultAdapter();
                $followerAry = array();
                $where = " F.created_date BETWEEN '".$lastMonth."' AND '".$uptoToday."'";
                
                // get Count of Male Followers Between the Age 13-17
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='M'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '13' AND '17'")
                            ->where($where);
                //echo $select;exit;
                $result = $db->fetchAll($select);
                $followerAry[1]['M'] = $result[0]['cnt'];                
                
                // get Count of Female Followers Between the Age 13-17
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='F'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '13' AND '17'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[1]['F'] = $result[0]['cnt'];
                
                // get Count of male Followers Between the Age 18-24
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='M'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '18' AND '24'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[2]['M'] = $result[0]['cnt'];
                
                // get Count of Female Followers Between the Age 18-24
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='F'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '18' AND '24'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[2]['F'] = $result[0]['cnt'];
                
                // get Count of male Followers Between the Age 25-34
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='M'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '25' AND '34'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[3]['M'] = $result[0]['cnt'];
                
                // get Count of Female Followers Between the Age 25-34
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='F'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '25' AND '34'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[3]['F'] = $result[0]['cnt'];
                
                // get Count of male Followers Between the Age 35-44
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='M'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '35' AND '44'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[4]['M'] = $result[0]['cnt'];
                
                // get Count of Female Followers Between the Age 35-44
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='F'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '35' AND '44'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[4]['F'] = $result[0]['cnt'];
                
                // get Count of male Followers Between the Age 45-54
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='M'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '45' AND '54'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[5]['M'] = $result[0]['cnt'];
                
                // get Count of Female Followers Between the Age 45-54
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='F'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '45' AND '54'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[5]['F'] = $result[0]['cnt'];
                
                // get Count of male Followers Between the Age 55-64
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='M'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '55' AND '64'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[6]['M'] = $result[0]['cnt'];
                
                // get Count of Female Followers Between the Age 55-64
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='F'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) BETWEEN '55' AND '64'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[6]['F'] = $result[0]['cnt'];
                
                // get Count of male Followers for the Age 65+
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='M'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) >= '65'")
                            ->where($where);
                $result = $db->fetchAll($select);        
                if($_SERVER['REMOTE_ADDR'] == '182.72.66.214'){
                   //echo $select;//exit;
                }
                $followerAry[7]['M'] = $result[0]['cnt'];
                
                // get Count of female Followers for the Age 65+
                $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U' ", array(''))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U'")
                            ->where("U.gender='F'")
                            ->where("(DATEDIFF(CURRENT_DATE, STR_TO_DATE(U.dob, '%Y-%m-%d'))/365) >= '65'")
                            ->where($where);
                $result = $db->fetchAll($select);             
                $followerAry[7]['F'] = $result[0]['cnt'];
                if($_SERVER['REMOTE_ADDR'] == '182.72.66.214'){
                    //echo "<pre>";print_r($followerAry);exit;
                }
                
                return $followerAry;
        }
        
        public function getUserFollowersByCountry($business_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U'", array(''))
                            ->joinLeft(array('C' => 'tbl_country')," U.country = C.id AND C.status = '1'", array('country_name'))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U' AND U.country!='0'")
                            ->group("C.id");
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getUserFollowersByCity($business_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U'", array(''))
                            ->joinLeft(array('C' => 'tbl_city')," U.city = C.id AND C.status = '1'", array('city_name'))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U' AND U.city!= '0'")
                            ->group("C.id");
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getUserFollowersByLanguage($business_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array("F"=>"tbl_followers"), array('cnt' => 'count(F.user_id)'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.user_id AND F.user_type = 'U'", array(''))
                            ->joinLeft(array('L' => 'tbl_languages')," U.user_language = L.language_code AND L.status = '1'", array('language_name'))
                            ->where("F.follower_id='".$business_id."' AND F.follower_type = 'B' AND F.user_type='U' AND U.user_language!= '0'")
                            ->group("L.language_id");
            $result = $db->fetchAll($select);
            return $result;
        }
        
        public function getCouponDetailsOfUser($business_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array("C"=>"tbl_coupons"), array('coupon_id','title'))
                            ->where("C.business_id='".$business_id."' AND C.expiration_date > '".date('Y-m-d')."'");
            //echo $select;exit;
            $result = $db->fetchAll($select);
            //echo "<pre>";print_r($result);exit;
            return $result;
        }
        
        public function getCouponClickAnalyticStatistics($business_id, $date1, $date2, $coupon_id){
            $db = Zend_Db_Table::getDefaultAdapter();
            //echo $coupon_id;exit;
//            // Get Coupon Id's By Business User
//            $couponObj = new Business_Model_Coupons();
//            $couponsAry = $couponObj->getcouponsIdsByBusinessId($business_id);
//            //echo "<pre>";print_r($couponsAry);exit;
//            $cnt = count($couponsAry);
//            $i = 1;
//            $ids = '';
//            foreach($couponsAry as $coupon){
//                if($i < $cnt)
//                    $ids .= "'".$coupon['coupon_id']."'".",";
//                if($i==$cnt)
//                    $ids .= "'".$coupon['coupon_id']."'";
//                $i++;
//            }
            //$date1 = "2013-05-30";
            //$date2 = "2013-05-04";
            
            //echo $date1."==>".$date2;//exit;
            $diff = abs(strtotime($date2) - strtotime($date1));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            
            //printf("%d years, %d months, %d days\n", $years, $months, $days);exit;
            
             if($_SERVER['REMOTE_ADDR'] =='182.72.66.214' || $_SERVER['REMOTE_ADDR'] =='192.168.1.57'){
                
                //printf("%d years, %d months, %d days\n", $years, $months, $days);exit;
            }
            
            
            /*$sql = "SELECT DATE_FORMAT(purchase_date, '%Y') as 'year',
                    DATE_FORMAT(purchase_date, '%m') as 'month', DATE_FORMAT(purchase_date, '%d') as 'date',
                    COUNT(coupon_id) as 'total'
                    FROM tbl_purchase_coupons
                    GROUP BY DATE_FORMAT(purchase_date, '%Y%m%d')"; */
            $where = "  purchase_date BETWEEN '".$date2."' AND '".date('y-m-d', strtotime($date1.'+1  day'))."'";
            
            if($days == 0 && $months==0 && $years == 0){
                $where = "  purchase_date LIKE '".$date2."%'";
                $value = "d";
                $select = $db->select()
                            ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'total'=>"COUNT(coupon_id)"));
                $select->where("C.coupon_id IN (".$coupon_id.")");
                $select->where($where)
                            ->group("DATE_FORMAT(C.purchase_date, '%".$value."')");
                //echo $select;exit;
                $result = $db->fetchAll($select); 
            }
            
            // if only days is teh difference between dates
            if($days > 0  && $days < 7 && $months == 0 && $years == 0){ // DISPLAY DAI:Y BASIS
                $value = "d";
                $select = $db->select()
                            ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'total'=>"COUNT(coupon_id)"));
                $select->where("C.coupon_id IN (".$coupon_id.")");
                $select->where($where)
                            ->group("DATE_FORMAT(C.purchase_date, '%".$value."')");
                //echo $select;exit;
                if($_SERVER['REMOTE_ADDR'] =='182.72.66.214' || $_SERVER['REMOTE_ADDR'] =='192.168.1.57'){
                    //echo $select;exit;
                }
                $result = $db->fetchAll($select);
            }
            
            if($days > 7 && $months == 0 && $years == 0){ // DISPLAY WEEKLY BASIS
                $value = "w";
                $weekAry = array();
                $week_diff = strtotime($date1, 0) - strtotime($date2, 0);
                $weekCnt = floor($week_diff / 604800);
                for($i=0;$i<$weekCnt;$i++){
                    $where = " purchase_date BETWEEN '".date('Y-m-d', strtotime($date2.'+'.$i.'  week'))."' AND '".date('Y-m-d', strtotime($date2.'+'.($i+1).'  week'))."'";
                    //echo $where;//exit;
                    $select = $db->select()
                            ->from(array('C'=>'tbl_purchase_coupons'), array('total'=>"COUNT(coupon_id)"));
                    $select->where("C.coupon_id IN (".$coupon_id.")");
                    $select->where($where);

                    $weekAry = $db->fetchAll($select);    
                    $result[$i]['date'] = $i+1;
                    $result[$i]['total'] = $weekAry[0]['total'];
                }
                //echo "<pre>";print_r($result);exit;
                
            }
            
            if($days >= 0 && $months > 0 && $years == 0){ // DISPLAY MONTYLY BASIS
                $value = "m";
                $select = $db->select()
                            ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'total'=>"COUNT(coupon_id)"));
                if($coupon_id != 'all')
                    $select->where("C.coupon_id IN (".$coupon_id.")");
                $select->where($where)
                        ->group("DATE_FORMAT(C.purchase_date, '%".$value."')");
                if($_SERVER['REMOTE_ADDR'] =='182.72.66.214' || $_SERVER['REMOTE_ADDR'] =='192.168.1.57') {
                    //echo $select;exit;
                }
                $result = $db->fetchAll($select);
            }
            
            if($days >= 0 && $months >= 0 && $years > 0){ // DISPLAY YEARLY BASIS
                $value = "Y";
                $select = $db->select()
                            ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'total'=>"COUNT(coupon_id)"));
                $select->where("C.coupon_id IN (".$coupon_id.")");
                $select->where($where)
                            ->group("DATE_FORMAT(C.purchase_date, '%".$value."')");
                //echo $select;exit;
                $result = $db->fetchAll($select);
            }
            
           if($_SERVER['REMOTE_ADDR'] =='182.72.66.214' || $_SERVER['REMOTE_ADDR'] =='192.168.1.57'){
                //echo $select;//exit;
                //echo "<pre>";print_r($result);exit;
            }
            
            $result['type'] = $value;
            return $result;
        }
        
        public function getTotalSalesAnalyticStatistics($business_id, $date1, $date2, $couponIds, $display_type){
            $db = Zend_Db_Table::getDefaultAdapter();
            //echo $date1."==>".$date2;exit;
            $diff = abs(strtotime($date2) - strtotime($date1));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            
            //printf("%d years, %d months, %d days\n", $years, $months, $days);exit;
            
            if($display_type == '')
            {
                if($days > 0  && $days < 7 && $months == 0 && $years == 0)
                    $display_type = 'd';
                if($days > 7 && $months == 0 && $years == 0)
                    $display_type = 'w';
                if($days >= 0 && $months > 0 && $years == 0)
                    $display_type = 'm';
                if($days >= 0 && $months >= 0 && $years > 0)
                    $display_type = 'Y';
            }
            
            $where = " purchase_date BETWEEN '".$date2."' AND '".$date1."'";
            
            if($display_type == 'd'){ // DISPLAY DAI:Y BASIS
                $value = "d";
                $select = $db->select()
                        ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'cnt' => 'count(C.coupon_id)'));
                            if($couponIds != '' && $couponIds != 0) {
                                $select->where("C.coupon_id IN (".$couponIds.")");
                            }
                $select->where("C.coupon_id IN (".$couponIds.")");
                $select->where($where);
                $result = $db->fetchAll($select);
                if($_SERVER['REMOTE_ADDR'] == '182.72.66.214'){
                   //echo $select;//exit;
                   //echo "<prE>";print_r($result);exit;
                }
            }
            
            if($display_type == 'w'){ // DISPLAY WEEKLY BASIS
                $value = "w";
                $weekAry = array();
                $week_diff = strtotime($date1, 0) - strtotime($date2, 0);
                $weekCnt = floor($week_diff / 604800);
                if($weekCnt == 0) $weekCnt = 1;
                for($i=0;$i<$weekCnt;$i++){
                    $where = " purchase_date BETWEEN '".date('Y-m-d', strtotime($date2.'+'.$i.'  week'))."' AND '".date('Y-m-d', strtotime($date2.'+'.($i+1).'  week'))."'";
                    $select = $db->select()
                            ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'total'=>"COUNT(coupon_id)"));
                    $select->where("C.coupon_id IN (".$couponIds.")");
                    $select->where($where);
                    //echo $select."<br >";//exit;
                    $weekAry = $db->fetchAll($select);    
                    $result[$i]['date'] = $i+1;
                    $result[$i]['total'] = $weekAry[0]['total'];
                }
                //echo "<pre>";print_r($result);exit;   
            }
            
            if($display_type == 'm'){ // DISPLAY MONTYLY BASIS
                $value = "m";
                $select = $db->select()
                        ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'cnt' => 'count(C.coupon_id)'));
                $select->where("C.coupon_id IN (".$couponIds.")");
                $select->where($where);
                //echo $select;exit;
                $result = $db->fetchAll($select);
            }
            
            if($display_type == 'Y'){ // DISPLAY YEARLY BASIS
                $value = "Y";
                $select = $db->select()
                        ->from(array('C'=>'tbl_purchase_coupons'), array('date'=>"DATE_FORMAT(purchase_date, '%".$value."')",'cnt' => 'count(C.coupon_id)'));
                $select->where("C.coupon_id IN (".$couponIds.")");
                $select->where($where);
                //echo $select;exit;
                $result = $db->fetchAll($select);
            }
            //echo "<pre>";print_r($result);exit;
            $result['type'] = $value;
            $result['days'] = $days;
            $result['months'] = $months;
            $result['years'] = $years;
            return $result;
        }
        
        public function getTransactionReports($business_id, $startDate, $endDate, $coupon_ids, $pagenum, $limit) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $where = " PC.purchase_date BETWEEN '".$startDate."' AND '".$endDate."'";
            if($startDate == $endDate){
                $where = " PC.purchase_date LIKE '".$startDate."%'";
            }
            if($coupon_ids != 'all') {
                $select = $db->select()
                                ->from(array('PC'=>'tbl_purchase_coupons'), array('*'))
                                ->joinLeft(array('C' => 'tbl_coupons'), "PC.coupon_id= C.coupon_id", array('*'))
                                ->joinLeft(array('U' => 'tbl_users')," U.user_id = PC.user_id", array('realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'username', 'profile_pic_path'))
                                ->where("PC.coupon_id IN (".$coupon_ids.")")
                                ->where($where)
                                ->order(array('PC.purchase_date DESC'));
            } else {
                $select = $db->select()
                                ->from(array('PC'=>'tbl_purchase_coupons'), array('*'))
                                ->joinLeft(array('C' => 'tbl_coupons'), "PC.coupon_id= C.coupon_id", array('*'))
                                ->joinLeft(array('U' => 'tbl_users')," U.user_id = PC.user_id", array('realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'username', 'profile_pic_path'))
                                ->where($where)
                                ->order(array('PC.purchase_date DESC'));
            }
            $offset = $pagenum * $limit;
            $select->limit($limit, $offset);
            $result = $db->fetchAll($select);
            return $result;
            
        }
        
        public function getTotalTransactionReports($business_id, $startDate, $endDate, $coupon_ids) {
            if($coupon_ids == '')
                $coupon_ids = 0;
            $db = Zend_Db_Table::getDefaultAdapter();
            $where = " purchase_date BETWEEN '".$startDate."' AND '".$endDate."'";
             if($startDate == $endDate){
                $where = " PC.purchase_date LIKE '".$startDate."%'";
            }
            if($coupon_ids != 'all') {
                $select = $db->select()
                                ->from(array('PC'=>'tbl_purchase_coupons'), array('cnt' =>'count(coupon_id)'))
                                ->where("PC.coupon_id IN (".$coupon_ids.")")
                                ->where($where);
            } else {
                $select = $db->select()
                                ->from(array('PC'=>'tbl_purchase_coupons'), array('cnt' =>'count(coupon_id)'))
                                ->where($where);
            }
            $result = $db->fetchAll($select);
            return $result[0]['cnt'];
        }
        
        public function getTotalActualCostAndDiscount($business_id, $startDate, $endDate, $coupon_ids) {
            $db = Zend_Db_Table::getDefaultAdapter();
            if($coupon_ids == '')
                $coupon_ids = 0;
            $where = " PC.purchase_date BETWEEN '".$startDate."' AND '".$endDate."'";
            if($startDate == $endDate){
                $where = " PC.purchase_date LIKE '".$startDate."%'";
            }
            if($coupon_ids != 'all') {
                $select = $db->select()
                                ->from(array('PC'=>'tbl_purchase_coupons'), array('*'))
                                ->joinLeft(array('C' => 'tbl_coupons'), "PC.coupon_id= C.coupon_id", array('*'))
                                ->where("PC.coupon_id IN (".$coupon_ids.")")
                                ->where($where)
                                ->group('C.coupon_id')
                                ->order(array('PC.purchase_date DESC'));
            } else {
                $select = $db->select()
                                ->from(array('PC'=>'tbl_purchase_coupons'), array('*'))
                                ->joinLeft(array('C' => 'tbl_coupons'), "PC.coupon_id= C.coupon_id", array('*'))
                                ->where($where)
                                ->group('C.coupon_id')
                                ->order(array('PC.purchase_date DESC'));
            }
            $result = $db->fetchAll($select);
            $totalActualCost = 0;
            $totalDiscount = 0;
            $newAry =array();
            $newAry['totalActualCost'] = 0;
            $newAry['totalDiscount'] = 0;
            if(count($result) > 0){
                foreach($result as $res){
                    $totalActualCost = $totalActualCost+$res['original_price'];
                    $totalDiscount = $totalDiscount+$res['discounted_price'];
                }
                $newAry['totalActualCost'] = $totalActualCost;
                $newAry['totalDiscount'] = $totalDiscount;
            }
            return $newAry;
        }
        
         public function getBusinessUserDetailsTEST($bId){
                $select = $this->getDbTable()->select()
                                        ->from(array('B'=>'tbl_business_users'))
                                        ->joinLeft(array('C'=>'tbl_country'),'B.country = C.id AND C.status="1"',array('country_name'))
                                        ->joinLeft(array('S'=>'tbl_state'),'B.state = S.id AND B.country = S.country_id AND S.status="1"',array('state_name'))
                                        ->joinLeft(array('CTY'=>'tbl_city'),'B.city = CTY.id AND B.state = CTY.state_id AND CTY.status = "1"',array('city_name'))
                                        ->joinLeft(array('Z'=>'tbl_zipcode'),'CTY.id = Z.city_id AND Z.status = "1"',array('zipcode as zip'))
                                        ->joinLeft(array('BC'=>'tbl_business_categories'),'BC.id = B.category_id',array('category_name'))
                                        ->where('business_id = ?',$bId)
                                        ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet->toArray();
        }

        public function cancelaccount($bid) {
                if(!empty($bid)) {
                        $businessinfo['status'] = 'I';
                        return $this->getDbTable()->update($businessinfo, array('business_id = ?' => $bid));
                }
        }
        
        public function updatePasswordByEmail($updAry, $email) {
                $updAry['password'] = md5($updAry['password'].SECURITY_SALT);
                if(!empty($email)) {
                    try{
                            $res = $this->getDbTable()->update($updAry, array('email= ?' => $email));
                            if (false === $res) {
                                return 0;  // bool false returned, query failed
                            } else {
                                return 1;
                            }
                        } catch (Zend_Exception $zex){} 
                }
        }
        
        public function getuserdatabyemail($email) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from('tbl_business_users',array('business_id','business_name','username'))
                                ->where('email = ?',$email);
                $resultset = $db->fetchRow($select);
                return $resultset;
        }
}