<?php

class Business_Model_Reviews
{
    protected $_dbTable;
    public function setDbTable($dbTable) {
            if (is_string($dbTable)) {
                    $dbTable = new $dbTable();
            }
            if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                    throw new Exception('Invalid table data gateway provided');
            }
            $this->_dbTable = $dbTable;
            return $this;
    }

    public function getDbTable() {
            if (null === $this->_dbTable) {
                    $this->setDbTable('Business_Model_DbTable_Reviews');
            }
            return $this->_dbTable;
    }
    
    public function getBusinessReviewsById($userID,$loggedUserId, $user_type, $limit = ''){
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserId) {
            $select = $this->getDbTable()->select()
                            ->from(array('R' => 'tbl_business_review'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = R.from_user_id AND R.from_user_type = 'B' ", array('business_id','business_name','image_path','business_username' =>'username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = R.from_user_id AND R.from_user_type = 'U'", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->where('(R.to_user_id = "'.$userID.'" AND R.to_user_type = "'.$user_type.'")')
                            ->group('R.review_id')
                            ->order('R.created_date DESC')
                            ->setIntegrityCheck(false);
        } else {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_business_review'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','business_username' =>'username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."'", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->where('(S.from_user_id = "'.$userID.'" OR S.to_user_id = "'.$userID.'")')
                            ->group('S.review_id')
                            ->order("created_date DESC")
                            ->setIntegrityCheck(false); 
        }
        if($limit != '')
            $select->limit($limit,0);
        //echo $select;//exit;

        $resArray = $db->fetchAll($select);
        
        $totalArray = array();
        
        foreach($resArray as $result)//modified by laxman
        {
            if($result['from_user_type'] == 'B')
            {
                $result['user_id'] = $result['business_id'];
                $result['firstname'] = $result['business_name'];
                $result['profile_pic_path'] = $result['image_path'];
                $result['username'] = $result['business_username'];
            }
            
            $totalArray[] = $result;
        }
        //echo "<pre>";print_r($totalArray);exit;
        return $totalArray;   
    }

    public function getReviewsCnt($user_id,$user_type) {
        //echo $user_id.'-->'.$user_type;exit;
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                    ->from(array('R' => 'tbl_business_review'))
                    ->where('to_user_id = ?',$user_id)
                    ->where('to_user_type = ?',$user_type)
                    ->where('parent_id = "0"');
        $result = COUNT($db->fetchAll($select));
        //echo '<pre>';print_r($result);exit;
        return $result;
    }
    
    public function insertReview($data) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $ins = $db->insert('tbl_business_review',$data);
        $ins = $db->lastInsertId();
        return $ins;
    }
    
    //function to get single scribble information
    public function getReviewInfo($reviewId){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                    ->from(array('S' => 'tbl_business_review'))
                    ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','username as business_username'))
                    ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                    ->where('S.review_id = ?',$reviewId)
                    ->setIntegrityCheck(false);
        $resArray = $db->fetchAll($select);
        return $resArray;
    }
    
    public function getParentBusinessReviewsById($userID,$loggedUserId, $user_type, $limit, $pagenum){
        $db = Zend_Db_Table::getDefaultAdapter();
        if($user_type != "B")
            $user_type = "U";
        if($userID == $loggedUserId && $user_type == 'B') {
            $select = $this->getDbTable()->select()
                            ->from(array('R' => 'tbl_business_review'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = R.from_user_id AND R.from_user_type = 'B' ", array('business_id','business_name','image_path','business_username' =>'username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = R.from_user_id AND R.from_user_type = 'U'", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->where('((R.to_user_id = "'.$loggedUserId.'" AND R.to_user_type = "'.$user_type.'") OR (R.from_user_id = "'.$userID.'" AND to_user_id ="0")) AND parent_id = "0"')
                            ->group('R.review_id')
                            ->order('R.created_date DESC')
                            ->setIntegrityCheck(false);
        } else {
            $select = $this->getDbTable()->select()
                            ->from(array('S' => 'tbl_business_review'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = S.from_user_id AND S.from_user_type = 'B' ", array('business_id','business_name','image_path','business_username' =>'username'))
                            ->joinLeft(array('U' => 'tbl_users')," U.user_id = S.from_user_id AND S.from_user_type = 'U' ", array('user_id','firstname','lastname','profile_pic_path','username'))
                            ->joinLeft(array('F' => 'tbl_followers')," F.user_id = '".$userID."' AND S.from_user_type = '".$user_type."' AND S.from_user_id = F.follower_id AND S.from_user_type = F.user_type", array('F.followers_id as follow_row_id', 'F.user_id as follow_user_id', 'F.user_type as follow_user_type', 'F.follower_id', 'F.follower_type', 'F.created_date as follow_created_date'))
                            ->where('
                                        (
                                            (S.from_user_id = "'.$loggedUserId.'" AND S.from_user_type = "'.$user_type.'" AND S.to_user_id = "'.$userID.'" AND S.to_user_type = "B") 
                                            OR (S.to_user_id = "'.$userID.'" AND S.to_user_type = "B")
                                        ) AND parent_id ="0"')
                            ->group('S.review_id')
                            ->order("created_date DESC")
                            ->setIntegrityCheck(false); 
        }
        $offset = $pagenum * $limit;
        $select->limit($limit,$offset);
        $resArray = $db->fetchAll($select);
        //echo "<pre>";print_r($resArray);exit;
        
        
        return $resArray;
    }
    
    public function getChildReviewsByParentId($parent_id){
        $resArray = array();
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                        ->from(array('R' => 'tbl_business_review'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = R.from_user_id AND R.from_user_type = 'B' ", array('business_id','business_name','image_path','business_username' =>'username'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = R.from_user_id AND R.from_user_type = 'U'", array('user_id','firstname','lastname','profile_pic_path','username'))
                        ->where('parent_id != "0" AND parent_id = "'.$parent_id.'"')
                        ->group('R.review_id')
                        ->order('R.created_date DESC')
                        ->setIntegrityCheck(false);
        //echo $select;exit;
        
        $resArray = $db->fetchAll($select);
        //echo "<pre>";print_r($resArray);exit;
        
        
        return $resArray;
    }
    
    public function getChildReviewsCntByParentId($parent_id){
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getDbTable()->select()
                        ->from(array('R' => 'tbl_business_review'), array('childReviewCnt'=>'COUNT(review_id)'))
                        ->where('parent_id != "0" AND parent_id = "'.$parent_id.'"')
                        ->group('review_id')
                        ->order('created_date DESC')
                        ->setIntegrityCheck(false);
        //echo $select;exit;
        
        $resArray = $db->fetchAll($select);
        
        if(!empty($resArray) && $resArray[0]['childReviewCnt'] != '')
            return $resArray[0]['childReviewCnt'];
        return 0;
    }
}

