<?php
class Business_Model_Businesscategories {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Business_Model_DbTable_Businesscategories');
                }
                return $this->_dbTable;
        }

        public function get_business_categories() {
                $select = $this->getDbTable()->select()
                                            ->where('status = "1"')
                                            ->order('category_name ASC');
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getTopPopularBusinessCategories() {
            /** select `BC`.`id`, `BC`.`category_name` count(B.category_id) as cnt 
                    from tbl_business_categories BC
                  left join tbl_business_users B on B.category_id = BC.id
                  GROUP BY BC.category_name
                  ORDER BY cnt DESC
             */
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('BC' => 'tbl_business_categories'), array('id','category_name', 'count(BC.id) as catCnt'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.category_id = BC.id", array(''))
                        ->where('BC.status = "1"')
                        ->group('BC.id')
                        ->order ('catCnt DESC');
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getcategoryname($id) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('C' => 'tbl_business_categories'), array('category_name'))
                            ->where('C.id =?',$id);
            $resultSet = $db->fetchRow($select);
            return $resultSet;
        }

        /*** For Keywords ***/
        public function keywordexistrnot($key) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('K' => 'tbl_keywords'))
                            ->where('K.keyword =?',$key);
            $resultSet = $db->fetchRow($select);
            return $resultSet;
        }

        public function keyexists($keyid,$bid) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('BK' => 'tbl_business_keywords'))
                            ->where('BK.keyid =?', $keyid)
                            ->where('BK.uid =?', $bid);
            $resultSet = $db->fetchRow($select);
            return $resultSet;
        }

        public function insertkey($key) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $data['keyword'] = $key;
            $db->insert('tbl_keywords',$data);
            $ins = $db->lastInsertId();
            return $ins;
        }

        public function insertbkeydata($keydata) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $ins = $db->insert('tbl_business_keywords',$keydata);
            return $ins;
        }

        public function getbusinesskeywords($bid) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('BK' => 'tbl_business_keywords'),'')
                            ->joinLeft(array('K' => 'tbl_keywords'), "K.id = BK.keyid", array('keyword'))
                            ->where('BK.uid =?',$bid);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getbusinessbykeyword($keyword,$bids='') {
            if(!empty($bids)) {
                $where = '(K.keyword  LIKE "%'.addslashes($keyword).'%" AND U.business_id NOT IN ('.$bids.')) OR U.business_name LIKE "'.addslashes($keyword).'%"';
            } else {
                $where = 'K.keyword  LIKE "%'.addslashes($keyword).'%" OR U.business_name LIKE "'.addslashes($keyword).'%" ';
            }
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('K' => 'tbl_keywords'), 'keyword')
                            ->joinLeft(array('BK' => 'tbl_business_keywords'), "K.id = BK.keyid", '')
                            ->joinLeft(array('U'=>'tbl_business_users'), "U.business_id = BK.uid", array('business_name','business_id','user_type'))
                            ->where($where);
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getbusinessid($businessname) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('B' => 'tbl_business_users'), 'business_id')
                            ->where('B.business_name =?', $businessname);
            $resultSet = $db->fetchRow($select);
            return $resultSet;
        }

        public function insertbusinesstargetroom($uid, $bid, $rid, $key) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $data['user_id'] = $uid;
            $data['room_id'] = $rid;
            $data['business_id'] = $bid;
            $data['keyword'] = $key;
            $db->insert('huddles_business_target',$data);
            $ins = $db->lastInsertId();
            return $ins;
        }

        public function businesstargetexistsrnot($bid) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                            ->from(array('HBT' => 'huddles_business_target'),'room_id')
                            ->joinLeft(array('HUC' => 'huddle_user_chat'),"HBT.room_id = HUC.room_id",'')
                            ->where('HBT.business_id =?', $bid)
                            ->where('HUC.open_status = "0"');
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }

        public function getBusinessByCategoryId($pagenum, $limit) {
            $db = Zend_Db_Table::getDefaultAdapter();
            $offset = $pagenum * $limit;
            $select1 = $db->select()
                            ->from('tbl_business_categories', 'id')
                            ->where("category_name = 'hotels' OR category_name = 'restaurant'");
            $catIds = $db->fetchAll($select1);
            $id = '';
            $cnt = count($catIds);
            foreach ($catIds as $i => $catid) {
                if($i < $cnt-1)
                    $id  .= "'".$catid['id']."',";
                if($i == $cnt-1)
                    $id  .= "'".$catid['id']."'";
            }
            if($id == '')
            	$id = 0;
            $select = $db->select()
                            ->from('tbl_business_users as B',array('business_id','business_name','description','category_id','username','email','firstname','lastname','address_line_1','address_line_2','image_path','user_type','lat','lng','business_created_date'))
                            ->joinLeft(array('Z' => 'tbl_zipcode'), "B.zipcode = Z.id", array('zipcode'))
                            ->where("category_id IN (".$id.")")
                            ->limit($limit, $offset);
            //echo $select;exit;
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }
}