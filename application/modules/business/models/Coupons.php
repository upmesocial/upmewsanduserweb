<?php
class Business_Model_Coupons {

	protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Business_Model_DbTable_Coupons');
                }
                return $this->_dbTable;
        }

        public function insert($data) { 
                $res = $this->getDbTable()->insert($data);
                if(!empty($res)) {
                        return $res;
                } else {
                        return '';
                }
        }

        public function getcouponsinfo($business_id) {
                $date = date('Y-m-d H:i:s');
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('C' => 'tbl_coupons'), array('coupon_id','title','description','expiration_date','image_path','coupon_price','original_price','discounted_price'))
                            ->joinLeft(array('P' => 'tbl_purchase_coupons'), "P.coupon_id = C.coupon_id", array('couponcnt' => 'COUNT(P.coupon_id)'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_id','business_name','address_line_1','address_line_2'))
                            ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = B.category_id ", array('category_name'))
                            ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                            ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                            ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                            ->where("C.business_id =".$business_id)
                            //->where("B.business_id =".$business_id)
                            ->where("C.status = 1")
                            ->where("expiration_date > ?",$date)
                            ->group('C.coupon_id');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getallcouponsinfo($pagenum='', $limit='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('C' => 'tbl_coupons'), array('coupon_id','title','description','expiration_date','image_path as coupon_image','coupon_price','original_price','discounted_price'))
                            ->joinLeft(array('P' => 'tbl_purchase_coupons'), "C.coupon_id = P.coupon_id", array('couponcnt' => 'COUNT(P.coupon_id)'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_id','business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','zipcode','lat','lng'))
                            ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                            ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                            ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                            ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                            ->where("C.status = 1")
                            ->group("C.coupon_id");
                if(($pagenum!='' || $pagenum == 0) && $limit!='') {
                        $offset = $pagenum * $limit;
                        $select->limit($limit, $offset);
                }
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getcouponbyid($coupon_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C' => 'tbl_coupons'))
                                ->joinLeft(array('P' => 'tbl_purchase_coupons'), "C.coupon_id = P.coupon_id", array('couponcnt' => 'COUNT(P.coupon_id)'))
                                ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_id','business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','lat','lng'))
                                ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                                ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                                ->where("C.coupon_id =?",$coupon_id);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function getcouponsbydate($pagenum, $limit) {
                $date = date('Y-m-d H:i:s');
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('C' => 'tbl_coupons'), array('coupon_id','title','description','expiration_date','image_path as coupon_image','coupon_price','original_price','discounted_price'))
                            ->joinLeft(array('P' => 'tbl_purchase_coupons'), "C.coupon_id = P.coupon_id", array('COUNT(P.coupon_id) as couponcnt'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_id','business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','zipcode','lat','lng'))
                            ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                            ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                            ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                            ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                            ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                            ->where("C.expiration_date > ?",$date)
                            ->where("C.status = '1'")
                            ->group('C.coupon_id');
                if(($pagenum!='' || $pagenum == 0) && $limit!='') {
                        $offset = $pagenum * $limit;
                        $select->limit($limit, $offset);
                }
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getcouponbydate($user_id='', $pagenum = '', $limit = '') {
                $date = date('Y-m-d H:i:s');
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('C' => 'tbl_coupons'), array('coupon_id','title','description','expiration_date','image_path as coupon_image','coupon_price','original_price','discounted_price'))
                            ->joinLeft(array('P' => 'tbl_purchase_coupons'), "C.coupon_id = P.coupon_id", array('COUNT(P.coupon_id) as couponcnt'))
                            ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = C.business_id ", array('business_id','business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','lat','lng'))
                            ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                            ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                            ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                            ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                            ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                            ->where("C.expiration_date > ?",$date)
                            ->where("P.user_id = ?",$user_id)
                            ->where("C.status = '1'")
                            ->group('C.coupon_id');
                if(($pagenum!='' || $pagenum == 0) && $limit!='') {
                        $offset = $pagenum * $limit;
                        $select->limit($limit, $offset);
                }
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getcouponsbybusinessid($business_id, $pagenum='', $limit='') {
                $date = date('Y-m-d H:i:s');
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                            ->from(array('C' => 'tbl_coupons'), array('business_id','coupon_id','title','description','expiration_date','coupon_price','image_path as coupon_image','original_price','discounted_price'))
                            ->joinLeft(array('P' => 'tbl_purchase_coupons'), "C.coupon_id = P.coupon_id", array('couponcnt' => 'COUNT(P.coupon_id)'))
                            ->joinLeft(array('B'=>'tbl_business_users'),"C.business_id = B.business_id",array('business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','lat','lng'))
                            ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                            ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                            ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                            ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                            ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                            ->where("C.expiration_date > ?",$date)
                            ->where("C.business_id =".$business_id)
                            ->where("C.status = '1'")
                            ->group('C.coupon_id')
                            ->order('C.coupon_id DESC');
                if(($pagenum!='' || $pagenum == 0) && $limit!='') {
                        $offset = $pagenum * $limit;
                        $select->limit($limit, $offset);
                }
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getcouponscntbyid($id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                ->where("business_id =?",$id);
                $resultSet = $db->fetchAll($select);
                return count($resultSet);
        }

        public function deleteCoupon($coupon) {
                @unlink(UPLOAD_PATH."images/original/".$coupon['image_path']);
                @unlink(UPLOAD_PATH."images/medium/".$coupon['image_path']);
                @unlink(UPLOAD_PATH."images/thumbnails/".$coupon['image_path']);
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->query("DELETE P.*,N.* FROM  tbl_purchase_coupons P
                                        LEFT JOIN tbl_notifications N ON (N.reference_id = P.purchase_id AND N.notifications_type = 'purchasecoupon')
                                        WHERE P.coupon_id = ".$coupon['coupon_id']);
                $id = $select->execute();
                $select = $db->query("DELETE C.*,N.* FROM  tbl_coupons C
                                        LEFT JOIN tbl_notifications N ON (N.reference_id = C.coupon_id AND N.notifications_type = 'new_upper')
                                        WHERE C.coupon_id = ".$coupon['coupon_id']);
                $id = $select->execute();
                return $id;
        }

        public function getcouponsIdsByBusinessId($id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                ->from(array('C'=>'tbl_coupons'), array('coupon_id'))
                                ->where("business_id =?",$id);
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getUppersByKeyword($search_key, $pagenum='', $limit='') {
                $db = Zend_Db_Table::getDefaultAdapter();
                $where = " (C.status = '1' AND C.expiration_date > NOW())";
                if($search_key != '') {
                        if(strlen($search_key) < 5) {
                                $where .= ' AND (C.title LIKE "%'.addslashes($search_key).'%")';
                        }
                        if(strlen($search_key) >= 5) {
                                $wrdArr =array();
                                $wrdArr = explode(" ", $search_key);
                                $wrdCnt = count($wrdArr);
                                if($wrdCnt > 1) {
                                        $subWrd = '';
                                        foreach($wrdArr as $wrd) {
                                                $subWrd .= '"'.$wrd.'" ';
                                        }
                                        $search_key = $subWrd;
                                }
                                $where .= ' AND MATCH(C.title) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
                        }
                }
                $select = $db->select()
                                ->from(array('C' => 'tbl_coupons'), array('business_id','coupon_id','title','description','expiration_date','coupon_price','image_path as coupon_image','original_price','discounted_price'))
                                ->joinLeft(array('P' => 'tbl_purchase_coupons'), "C.coupon_id = P.coupon_id", array('couponcnt' => 'COUNT(P.coupon_id)'))
                                ->joinLeft(array('B'=>'tbl_business_users'),"C.business_id = B.business_id",array('business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','lat','lng'))
                                ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                                ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                                ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                                ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                                ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                                ->where($where)
                                ->group('C.coupon_id')
                                ->order('C.coupon_id DESC');
                /*$select = $this->getDbTable()->select()
                                ->from(array('C'=>'tbl_coupons'), array('*'))
                                ->where($where);*/
                if(($pagenum!='' || $pagenum == 0) && $limit!='') {
                    $offset = $pagenum * $limit;
                    $select->limit($limit, $offset);
                }
                //echo $select;exit;
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function getUppersByUserId($user_id, $pagenum, $limit, $keyword = '') {
                $db = Zend_Db_Table::getDefaultAdapter();
                // Get Business Followers of the User Id Specified
                $select = $db->select()
                            ->from(array('F'=>'tbl_followers'), array(''))
                            ->joinLeft(array('B'=>'tbl_business_users'), "F.follower_id=B.business_id and F.follower_type='B' AND F.user_id = '".$user_id."' AND F.user_type='U'", array('business_id','lat','lng'))
                            ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                            ->where("F.follower_id=B.business_id and F.follower_type='B' AND F.user_id = '".$user_id."' AND F.user_type='U'");
                $resultSet = $db->fetchAll($select);
                $cnt = count($resultSet);
                $ids = '';
                if($cnt > 0) {
                        foreach($resultSet as $key=>$result) {
                                if(($key+1) < $cnt)
                                        $ids .= "'".$result['business_id']."',";
                                if(($key+1) == $cnt)
                                        $ids .= "'".$result['business_id']."'";
                        }
                }
                if($ids == '')
                        $ids = 0;
                $select1 = $db->select()
                                ->from(array('C' => 'tbl_coupons'), array('business_id','coupon_id','title','description','expiration_date','coupon_price','image_path as coupon_image','original_price','discounted_price'))
                                ->joinLeft(array('P' => 'tbl_purchase_coupons'), "C.coupon_id = P.coupon_id", array('couponcnt' => 'COUNT(P.coupon_id)'))
                                ->joinLeft(array('B'=>'tbl_business_users'),"C.business_id = B.business_id",array('business_name','address_line_1','address_line_2','image_path as business_image','firstname as business_firstname','lastname as business_lastname','username as business_username','zipcode','lat','lng'))
                                ->joinLeft(array('BC' => 'tbl_business_categories')," BC.id = C.category_id ", array('category_name'))
                                ->joinLeft(array('Ct' => 'tbl_country')," Ct.id = B.country ", array('country_name'))
                                ->joinLeft(array('S' => 'tbl_state')," S.id = B.state ", array('state_name'))
                                ->joinLeft(array('Cty' => 'tbl_city')," Cty.id = B.city ", array('city_name'))
                                ->joinLeft(array('Z' => 'tbl_zipcode')," Z.id = B.zipcode ", array('zipcode'))
                                ->where("C.business_id IN (".$ids.")")
                                ->where("C.status = '1' AND expiration_date >= NOW()")
                                ->group('C.coupon_id')
                                ->order('C.coupon_id DESC');
                if($keyword != '') {
                        $select1->where("C.title LIKE '%".addslashes($keyword)."%' OR C.description LIKE '%".addslashes($keyword)."%' OR B.business_name LIKE '%".addslashes($keyword)."%'");
                }
                if(($pagenum != '' || $pagenum == 0) && $limit != '') {
                        $offset = $pagenum * $limit;
                        $select1->limit($limit, $offset);
                }
                $resultSet1 = $db->fetchAll($select1);
                return $resultSet1;
        }

        public function businessactivecouponscount($bid) {
                $date = date('Y-m-d H:i:s');
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $this->getDbTable()->select()
                                ->where("business_id =?",$bid)
                                ->where("expiration_date > ?",$date);
                $resultSet = $db->fetchAll($select);
                return count($resultSet);
        }

        public function updatecoupondata($data,$upper_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                return  $db->update('tbl_coupons', $data, 'coupon_id = "'.$upper_id.'"');
        }

}