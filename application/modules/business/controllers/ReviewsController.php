<?php
class Business_ReviewsController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
//        if(Zend_Registry::isRegistered('businessdata')) {
//            $value = Zend_Registry::get('businessdata');
//        }
//        $value = Zend_Registry::get('businessdata');
    }

    public function indexAction() {
        // Getting all Reviews
        $reviewArray = array();
        $request = $this->getRequest();
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):20);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);
        $reviewObj = new Business_Model_Reviews();
        if($request->getParam('userid') != '') {
            $business_id = $request->getParam('userid');
            // if logged user is a Business Type
            if(Zend_Registry::isRegistered('businessdata')) {
                $userdata = Zend_Registry::get('businessdata');
                $user_type = $userdata->user_type;
                $logged_user_id = $userdata->business_id;
            }
            // if logged user is People Type
            if(Zend_Registry::isRegistered('userdata')) {
                $userdata = Zend_Registry::get('userdata');
                $user_type = $userdata->user_type;
                $logged_user_id = $userdata->user_id;
            }
            $tempId = $logged_user_id;
            // FOR Fetching Reviews
            $reviewArray = $reviewObj->getParentBusinessReviewsById($business_id, $logged_user_id, $user_type, $limit, $pagenum);
        } else {
            if(Zend_Registry::isRegistered('businessdata')) {
                $userdata = Zend_Registry::get('businessdata');
                $business_id = $userdata->business_id;
                $user_type = $userdata->user_type;
                // FOR Fetching Reviews
                $reviewArray = $reviewObj->getParentBusinessReviewsById($business_id,  $business_id, $user_type,  $limit, $pagenum);
            }
            $tempId = $business_id;
        }
        // get child Reviews count by Parent ID
        foreach($reviewArray as $key=>$review){
            //echo "<pre>";print_r($review);exit;
            $parent_id = $review['review_id'];
            $childReviewsCnt = $reviewObj->getChildReviewsCntByParentId($parent_id, $business_id, $tempId, $user_type);
            $reviewArray[$key]['childReviewsCnt'] = $childReviewsCnt;
        }
        $this->view->reviewArray = $reviewArray;
        $this->view->limit = $limit;
        $this->view->pagenum = $pagenum;
    }

    public function postreviewAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        // for Logged User Info
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_details = Zend_Registry::get('businessdata');
            $logged_user_id = $logged_user_details->business_id;
        }
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_details = Zend_Registry::get('userdata');
            $logged_user_id = $logged_user_details->user_id;
        }
        $logged_user_type = $logged_user_details->user_type;
        // if User Posted a review
        if($request->getParam('For') != '' && $request->getParam('For') == 'Post_Review') {
            $this->_helper->viewRenderer->setNoRender();
            $parent_Id = $request->getParam('parentId');
            if($parent_Id == '') {
                $parent_Id = 0;
            }
            $insAry = array(
                            'parent_id' => $parent_Id,
                            'from_user_id' => $request->getParam('fromId'),
                            'from_user_type' => strtoupper($request->getParam('fromType')),
                            'to_user_id' => $request->getParam('toId'),
                            'to_user_type' => "B",
                            'review_message' => $request->getParam('review'),
                            'created_date' => date('Y-m-d H:i:s')
                    );
            $reviewObj = new Business_Model_Reviews();
            $ins = $reviewObj->insertReview($insAry);
            if($ins!= '' && $ins!= 0) {
                $reviewInfo = $reviewObj->getReviewInfo($ins);
                if($reviewInfo[0]['from_user_type'] == 'U') {
                    if($reviewInfo['0']['profile_pic_path'] != '' && file_exists(UPLOAD_PATH."images/thumbnails/".$reviewInfo['0']['profile_pic_path']))
                        $imgPath = SITE_URL.'/uploads/images/thumbnails/'.$reviewInfo['0']['profile_pic_path'];
                    else
                        $imgPath = SITE_URL.'/images/no-user.gif';
                    $name = ucfirst($reviewInfo['0']['firstname'])." ".ucfirst($reviewInfo['0']['lastname']);
                    $usrName = $reviewInfo['0']['username'];
                }
                if($reviewInfo[0]['from_user_type'] == 'B') {
                    $imgPath = SITE_URL.'/uploads/images/thumbnails/'.$reviewInfo['0']['image_path'];
                    $name = ucfirst($reviewInfo['0']['business_name']);
                    $usrName = $reviewInfo['0']['business_username'];
                }
                // for Emotions
                $strAry = array(':0',':x',':-D',':-)',':-p',':(',':D',':^0',';|','?:|',';-)');
                $replaceAry = array('<img src="'.SITE_URL.'images/emoticons/angel.png">','<img src="'.SITE_URL.'images/emoticons/kiss.png">','<img src="'.SITE_URL.'images/emoticons/laugh.png">','<img src="'.SITE_URL.'images/emoticons/plain.png">','<img src="'.SITE_URL.'images/emoticons/raspberry.png">','<img src="'.SITE_URL.'images/emoticons/sad.png">','<img src="'.SITE_URL.'images/emoticons/smile.png">','<img src="'.SITE_URL.'images/emoticons/smile-big.png">','<img src="'.SITE_URL.'images/emoticons/surprise.png">','<img src="'.SITE_URL.'images/emoticons/uncertain.png">','<img src="'.SITE_URL.'images/emoticons/wink.png">');
                $reply_html = '<a id="reply'.$reviewInfo['0']['review_id'].'" class="various2 fancybox.ajax pointer" onclick="replyReview('.$reviewInfo['0']['review_id'].','.$logged_user_id.',\''.$logged_user_type.'\','.$reviewInfo['0']['to_user_id'].')"><img class="tab_icon2" src="'.SITE_URL.'images/reply.png" ></a>';
                $output = str_replace($strAry,$replaceAry,$reviewInfo[0]['review_message']);
                $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                $html = '';
                $html = '<div class="tab_row" id="tab_row'.$reviewInfo['0']['review_id'].'">
                            <img width="66" height="66" src="'.$imgPath.'">
                                <div class="tab_row_cnt_otr">
                                <h1>'.$this->view->translate($name).'</h1>
                                <h2>@'.$this->view->translate($usrName).'</h2>
                                <span class="time" style="float:right;">'.$this->view->translate($timeObj->humaneDate($reviewInfo[0]['created_date'])).'</span><br>
                                <h2  class="scrb-text">'.$this->view->translate(stripslashes(urldecode($output))).'</h2><br>
                                    <div class="clear"></div>
                                </div>
                                '.$reply_html.'
                        </div>';
                echo json_encode(array('service_status'=>'success',"content" => $html));                
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"content" => 'Error in posting Review!!!. Please try again.'));
                return;
            }
        }
        if(Zend_Registry::isRegistered('userdata')){
            $logged_user_data = Zend_Registry::get('userdata');
            $logged_user_id = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')){
            $logged_user_data = Zend_Registry::get('businessdata');
            $logged_user_id = $logged_user_data->business_id;
        }
        $logged_user_type = $logged_user_data->user_type;
        $fromUserId = $request->getParam('user_id');
        if($fromUserId == '')
            $fromUserId = $logged_user_id;
        $isfollowingUser = 0;
        $isLoggedUser = 0;
        if($logged_user_id == $fromUserId && $logged_user_type == "B") {
            $isLoggedUser = 1;
        }
        // Check if reviewing user folowing Business or not  
        //$followObj  = new User_Model_Followers();
        if($isLoggedUser == 0){
            $followObj  = new Business_Model_Followers();
            $detailsArray = $followObj->isLoggedUserFollowingOtherUser($fromUserId, 'B');
            if(count($detailsArray) == 0) {
                $isfollowingUser = 1;
            }
        }
        $this->view->isLoggedUser = $isLoggedUser;
        $this->view->isfollowingUser = $isfollowingUser;
        //echo $isfollowingUser;exit;
    }
    
    public function replyreview2Action() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        //echo "<pre>";print_r($request->getParams());exit;
        $isfollowingUser = 0;
        $isLoggedUser = 0;
        // if User Posted a review
        if($request->getParam('For') != '' && $request->getParam('For') == 'Reply_Review') {
            $this->_helper->viewRenderer->setNoRender();
            // for Logged User Info
            if(Zend_Registry::isRegistered('businessdata')) {
                $logged_user_details = Zend_Registry::get('businessdata');
                $logged_user_id = $logged_user_details->business_id;
            }
            if(Zend_Registry::isRegistered('userdata')) {
                $logged_user_details = Zend_Registry::get('userdata');
                $logged_user_id = $logged_user_details->user_id;
            }
            $logged_user_type = $logged_user_details->user_type;
            $replyingUserId = $request->getParam('toId');
            if($logged_user_id == $replyingUserId && $logged_user_type == "B") {
                $isLoggedUser = 1;
            }
            // Check if reviewing user folowing Business or not  
            //$followObj  = new User_Model_Followers();
            if($isLoggedUser == 0){
                $followObj  = new Business_Model_Followers();
                $detailsArray = $followObj->isLoggedUserFollowingOtherUser($replyingUserId, 'B');
                //echo "<pre>";print_r($detailsArray);exit;
                if(count($detailsArray) == 0) {
                    echo json_encode(array('service_status'=>'error',"message" => "You need to follow the Business to Post a Review!!!"));
                    return;
                }
            }
            if(strlen($request->getParam('review')) > 150) {
                echo json_encode(array('service_status'=>'error',"message" => "Review Limit Exceeded!!!"));
                return;
            }
            // for Inserting review
            $parent_Id = $request->getParam('parentId');
            if($parent_Id == '') {
                $parent_Id = 0;
            }
            $insAry = array(
                            'parent_id' => $parent_Id,
                            'from_user_id' => $request->getParam('fromId'),
                            'from_user_type' => strtoupper($request->getParam('fromType')),
                            'to_user_id' => $request->getParam('toId'),
                            'to_user_type' => "B",
                            'review_message' => $request->getParam('review'),
                            'created_date' => date('Y-m-d H:i:s')
                    );
            $reviewObj = new Business_Model_Reviews();
            $ins = $reviewObj->insertReview($insAry);
            if($ins!= '' && $ins!= 0) {
                if($parent_Id != 0) {
                    $reviewInfo = array();
                    $childscribbles = $reviewObj->getChildReviewsByParentId($parent_Id);
                }
                if($parent_Id == 0) {
                    $childscribbles = $reviewObj->getReviewInfo($ins);
                }
                if(count($childscribbles) > 0) {
                    $html = '<div  class="child_level" style="width: 500px; padding-left: 5px; max-height: 500px !important;overflow-x: hidden;overflow-y: auto;">';
                    foreach($childscribbles as $reviewInfo) {
                        $reply_html = '';
                        $original_review_id = $reviewInfo['review_id'];
                        if($reviewInfo['parent_id']!=0)
                            $original_review_id = $reviewInfo['parent_id'];
                        if($reviewInfo['from_user_type'] == 'U') {
                            if($reviewInfo['profile_pic_path'] != '' && file_exists(UPLOAD_PATH."images/thumbnails/".$reviewInfo['profile_pic_path']))
                                $imgPath = SITE_URL.'/uploads/images/thumbnails/'.$reviewInfo['profile_pic_path'];
                            else
                                $imgPath = SITE_URL.'/images/no-user.gif';
                            $name = ucfirst($reviewInfo['firstname'])." ".ucfirst($reviewInfo['lastname']);
                            $usrName = $reviewInfo['username'];
                        }
                        if($reviewInfo['from_user_type'] == 'B') {
                            if($reviewInfo['image_path'] != '' && file_exists(UPLOAD_PATH."images/thumbnails/".$reviewInfo['image_path']))
                                $imgPath = SITE_URL.'/uploads/images/thumbnails/'.$reviewInfo['image_path'];
                            else
                                $imgPath = SITE_URL.'/images/no-user.gif';
                            $name = ucfirst($reviewInfo['business_name']);
                            $usrName = $reviewInfo['business_username'];
                        }
                        // for Emotions
                        $strAry = array(':0',':x',':-D',':-)',':-p',':(',':D',':^0',';|','?:|',';-)');
                        $replaceAry = array('<img src="'.SITE_URL.'images/emoticons/angel.png">','<img src="'.SITE_URL.'images/emoticons/kiss.png">','<img src="'.SITE_URL.'images/emoticons/laugh.png">','<img src="'.SITE_URL.'images/emoticons/plain.png">','<img src="'.SITE_URL.'images/emoticons/raspberry.png">','<img src="'.SITE_URL.'images/emoticons/sad.png">','<img src="'.SITE_URL.'images/emoticons/smile.png">','<img src="'.SITE_URL.'images/emoticons/smile-big.png">','<img src="'.SITE_URL.'images/emoticons/surprise.png">','<img src="'.SITE_URL.'images/emoticons/uncertain.png">','<img src="'.SITE_URL.'images/emoticons/wink.png">');
                        $reply_html = '<a id="reply'.$reviewInfo['review_id'].'" class="various2 fancybox.ajax pointer" onclick="replyReview('.$original_review_id.','.$logged_user_id.',\''.$logged_user_type.'\','.$reviewInfo['to_user_id'].')"><img class="tab_icon2" src="'.SITE_URL.'images/reply.png" ></a>';
                        $output = str_replace($strAry,$replaceAry,$reviewInfo['review_message']);
                        $timeObj = new UpmeSocial_View_Helper_Datetimediff();
                        $html .= '<div class="tab_row" id="tab_row'.$reviewInfo['review_id'].'">
                                    <img width="66" height="66" src="'.$imgPath.'">
                                        <div class="tab_row_cnt_otr" style="width: 400px !important;">
                                        <h1>'.$this->view->translate($name).'</h1>
                                        <h2>@'.$this->view->translate($usrName).'</h2>
                                        <span class="time" style="float:right;">'.$this->view->translate($timeObj->humaneDate($reviewInfo['created_date'])).'</span><br>
                                        <h2  class="scrb-text">'.$this->view->translate(stripslashes(urldecode($output))).'</h2><br>
                                            <div class="clear"></div>
                                        </div>
                                        '.$reply_html.'
                                </div>';
                    }
                    $html .= '</div>';
                }
                echo json_encode(array('service_status'=>'success',"content" => $html));                
                return;
            } else {
                echo json_encode(array('service_status'=>'error',"message" => 'Error in posting Review!!!. Please try again.'));
                return;
            }
        }
        $this->view->isLoggedUser = $isLoggedUser;
        $this->view->isfollowingUser = $isfollowingUser;
        $parentId = $request->getParam('parentId');
        $this->view->parentId = $parentId;
        $toUserId = $request->getParam('toUserId');
        $this->view->toUserId = $toUserId;
    }

    public function replyreviewAction() {
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $parentId = $request->getParam('parentId');
        $frmUserId = $request->getParam('frmUserId');
        $frmUsrType = $request->getParam('frmUsrType');
        $toUserId = $request->getParam('toUserId');
        $this->view->parentId = $parentId;
        $this->view->frmUserId = $frmUserId;
        $this->view->frmUsrType = $frmUsrType;
        $this->view->toUserId = $toUserId;
    }
    
    public function getchildreviewsAction(){
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        //echo "<pre>";print_r($request->getParams());exit;
        $parent_id = $request->getParam('parent_id');
        $user_Id = $request->getParam('from_user_id');
        
        // to get logged user Detials
        if(Zend_Registry::isRegistered('businessdata')) {
            $userdata = Zend_Registry::get('businessdata');
            $user_type = $userdata->user_type;
            $logged_user_id = $userdata->business_id;
        }
        // if logged user is People Type
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
            $user_type = $userdata->user_type;
            $logged_user_id = $userdata->user_id;
        } 
        $reviewObj = new Business_Model_Reviews();
        $childReviews = array();
        $childReviews = $reviewObj->getChildReviewsByParentId($parent_id);
        $this->view->reviewArray = $childReviews;
        
   }

    public function getmorereviewsAction(){
        $this->_helper->layout->disableLayout();
        $reviewArray = array();
        $request = $this->getRequest();
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):20);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);
        $reviewObj = new Business_Model_Reviews();
        if($request->getParam('userid') != '') {
            $business_id = $request->getParam('userid');
            // if logged user is a Business Type
            if(Zend_Registry::isRegistered('businessdata')) {
                $userdata = Zend_Registry::get('businessdata');
                $user_type = $userdata->user_type;
                $logged_user_id = $userdata->business_id;
            }
            // if logged user is People Type
            if(Zend_Registry::isRegistered('userdata')) {
                $userdata = Zend_Registry::get('userdata');
                $user_type = $userdata->user_type;
                $logged_user_id = $userdata->user_id;
            }
            $tempId = $logged_user_id;
            // FOR Fetching Reviews
            $reviewArray = $reviewObj->getParentBusinessReviewsById($business_id, $logged_user_id, $user_type, $limit, $pagenum);
        } else {
            if(Zend_Registry::isRegistered('businessdata')) {
                $userdata = Zend_Registry::get('businessdata');
                $business_id = $userdata->business_id;
                $user_type = $userdata->user_type;
                // FOR Fetching Reviews
                $reviewArray = $reviewObj->getParentBusinessReviewsById($business_id,  $business_id, $user_type,  $limit, $pagenum);
            }
            $tempId = $business_id;
        }
        // get child Reviews count by Parent ID
        foreach($reviewArray as $key=>$review){
            //echo "<pre>";print_r($review);exit;
            $parent_id = $review['review_id'];
            $childReviewsCnt = $reviewObj->getChildReviewsCntByParentId($parent_id, $business_id, $tempId, $user_type);
            $reviewArray[$key]['childReviewsCnt'] = $childReviewsCnt;
        }
        //echo "<pre>";print_r($reviewArray);exit;
        $this->view->reviewArray = $reviewArray;
        $this->view->limit = $limit;
        $this->view->pagenum = $pagenum;
   }

}