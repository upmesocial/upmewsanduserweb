<?php
class Business_IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {
        // action body
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        if($auth->hasIdentity()) {
            $business_id = $auth->getIdentity()->business_id;
            $businessdb = new Business_Model_Business();
            $businessdata = $businessdb->getBusinessUserDetails($business_id);
            $this->view->assign("businessdata",$businessdata);
        }
    }

    public function loginAction() {
        $uName = $this->getRequest()->getParam('uname');
        $uPwd = $this->getRequest()->getParam('pwd');
        if($uName == '' || $uPwd == '')
            $this->_helper->redirector('index','index','business');
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $adapter = new Zend_Auth_Adapter_DbTable(
                        $this->_getParam('db'),
                        'tbl_business_users',
                        'username',
                        'password',
                        'MD5(CONCAT(?, "'.SECURITY_SALT.'"))'
                );
        // We're authenticated! Redirect to the home page
        $adapter->setIdentity($uName);
        $adapter->setCredential($uPwd);
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        $result = $auth->authenticate($adapter);
        if ($result->isValid()) {
            $user = $adapter->getResultRowObject();
            $auth->getStorage()->write($user);
            $resultAry = array('service_status'=>'success',"content" => $user);
        } else {
            $resultAry = array('service_status'=>'error',"content" => '');
        }
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry);
    }

    public function logoutAction() {
        $this->_helper->viewRenderer->setNoRender();
        $buzdata = new Zend_Session_Namespace('buzdata');
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        if($auth->hasIdentity()) {
            $auth->clearIdentity();
            setcookie('lbusinessname', '', time() -3600, '/');
            setcookie('lbusinesspwd', '', time() -3600, '/');
        }
        $buzdata->unsetAll();
        Zend_Registry::_unsetInstance();
        $this->_helper->redirector('index','index','business');
    }

    public function datavalidateAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $businessdb = new Business_Model_Business();
        $buzdata = new Zend_Session_Namespace('buzdata');
        if(!empty($buzdata->bId))
            $bid = $buzdata->bId;
        else
            $bid = '';
        $buzdata = new Zend_Session_Namespace('buzdata');
        if($this->getRequest()->getParam('For') == 'username') {
            $username = $this->getRequest()->getParam('username');
            $unameexistsrnot = $businessdb->isUsernameAvail($username,$bid);
            if(!empty($unameexistsrnot)) {
                echo 'notexists';
            } else {
                echo 'exists';
            }
        }
        if($this->getRequest()->getParam('For') == 'email') {
            $email = $this->getRequest()->getParam('email');
            $emailexistsrnot = $businessdb->isEmailAvail($email,$bid);
            if(!empty($emailexistsrnot)) {
                echo 'notexists';
            } else {
                echo 'exists';
            }
        }
    }

    public function businessinfoAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        if(!empty($loggedUserId)) {
            $this->_redirect('business/dashboard');
        }
        $request = $this->getRequest();
        //echo '<pre>';print_r($request->getParams());exit;
        $businesscatObj = new Business_Model_Businesscategories();
        $buzdata = new Zend_Session_Namespace('buzdata');
        if(!empty($buzdata->bId)) {
            $bid = $buzdata->bId;
            $businessdb = new Business_Model_Business();
            $businessuserdata = $businessdb->getBusinessUserDetails($bid);
            $businesskeywords = $businesscatObj->getbusinesskeywords($bid);
            //echo '<pre>';print_r($businesskeywords);exit;
            foreach($businesskeywords as $businesskey):
                $businesskeyword[] = $businesskey['keyword'];
            endforeach;
            $bkeyword = implode(',', $businesskeyword);
            //echo $bkeyword.'<pre>';print_r($businesskeyword);exit;
            //Fetching States Based On CountryId
            $statesdb = new User_Model_States();
            $states = $statesdb->get_states($businessuserdata['country']);
            //Fetching Cities Based On StateId
            $cityObj = new User_Model_Cities();
            $cities = $cityObj->get_cities($businessuserdata['state']);
            //Fetching Zipcode Basedon CityId
            $zipcodes = $cityObj->getZipcodesByCity($businessuserdata['city']);
            //echo '<pre>';print_r($businessuserdata);print_r($zipcodes);exit;
            $this->view->assign('business',$businessuserdata);
            $this->view->assign('keywords',$bkeyword);
            $this->view->assign('states',$states);
            $this->view->assign('cities',$cities);
            $this->view->assign('zipcode',$zipcodes);
        }
        if($this->getRequest()->isPost()) {
            $data = $request->getParam('business');
            $keydata = $request->getParam('keywords');
            //echo $keydata;exit;
            $keys = explode(',', $keydata);
            foreach($keys as $i=>$key):
                $existsrnot = $businesscatObj->keywordexistrnot($key);
                //echo 'exists: <pre>';print_r($existsrnot);exit;
                if($existsrnot == '') {
                    $kid = $businesscatObj->insertkey($key);
                    $keyids[$i] = $kid;
                } else {
                    $keyids[$i] = $existsrnot['id'];
                }
            endforeach;
            //echo '<pre>';print_r($keyids);exit;
            // For Fetching Country Name
            $countryObj = new User_Model_Countries();
            $countryArr = $countryObj->getCountryNameByCountryID($data['country']);
            $country_name = $countryArr['0']['country_name'];
            // For Fetching State Name
            $stateObj = new User_Model_States();
            $stateArr = $stateObj->getStateNameByStateID($data['state']);
            $state_name = $stateArr['0']['state_name'];
            // For FetchingCity Name
            $cityObj = new User_Model_Cities();
            $cityArr = $cityObj->getCityNameByCityID($data['city']);
            $city_name = $cityArr['0']['city_name'];
            $address = $city_name.", ".$state_name.", ".$country_name;
            $address = str_replace(" ", "+", $address);
            $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$country_name");
            $json = json_decode($json);
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $lng = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
            if($lat != '' && $lng != '') {
                $data['lat'] = $lat;
                $data['lng'] = $lng;
            }
            $businessdb = new Business_Model_Business();
            if(!empty($bid)) {
                $data['status'] = 'P';
                $businessdb->updatebusiness($data, $bid);
                foreach($keyids as $keyid):
                    $bkeydata['uid'] = $bid;
                    $bkeydata['keyid'] = $keyid;
                    $keyexists = $businesscatObj->keyexists($keyid,$bid);
                    if($keyexists == '') {
                        $bkid = $businesscatObj->insertbkeydata($bkeydata);
                    }
                endforeach;
                return $this->_helper->redirector('profileinfo');
            } else {
                $data['status'] = 'P';
                $id = $businessdb->save($data);
                foreach($keyids as $keyid):
                    $bkeydata['uid'] = $id;
                    $bkeydata['keyid'] = $keyid;
                    $bkid = $businesscatObj->insertbkeydata($bkeydata);
                endforeach;
                Zend_Session::start();
                $buzdata = new Zend_Session_Namespace('buzdata');
                $buzdata->bId = $id;
                $buzdata->firstname = $data['firstname'];
                $buzdata->lastname = $data['lastname'];
                $buzdata->email = $data['email'];
                return $this->_helper->redirector('profileinfo');
            }
        }
        $countriesdb = new User_Model_Countries();
        $countries = $countriesdb->get_countries();
        $this->view->assign('action', "businessinfo");
        $this->view->assign("countries",$countries);
    }

    public function profileinfoAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        $businessdb = new Business_Model_Business();
        $businesscategoiresdb = new Business_Model_Businesscategories();
        if(!empty($loggedUserId)) {
            $this->_redirect('business/dashboard');
        }
        $request = $this->getRequest();
        $buzdata = new Zend_Session_Namespace('buzdata');
        if($buzdata->bId == '') {
            $this->_redirect('business/businessinfo');
        }
        $bid = $buzdata->bId;
        if(!empty($bid)) {
            $businessuserdata = $businessdb->getBusinessUserDetails($bid);
            $this->view->assign('business',$businessuserdata);
        }
        if($this->getRequest()->isPost()) {
            $data = $request->getParam('business');
            $buzdata = new Zend_Session_Namespace('buzdata');
            $buzdata->username = $data['username'];
            $buzdata->password = $data['password'];
            $data['password'] = md5($data['password'].SECURITY_SALT);
            $data['status'] = 'P';
            $businessdb->updatebusiness($data, $bid);
            /***** Mail Functionality For Registration Without Payment *****/
            //echo '<pre>';print_r($buzdata->email);exit;
            if($_SERVER['SERVER_NAME'] != '192.168.1.57') {
                $m = new UpmeSocial_HtmlMailer();
                $m->setSubject("Login Credentials");
                $m->addTo($buzdata->email)
                    ->setViewParam('user_id',$bid)
                    ->setViewParam('firstname',$buzdata->firstname)
                    ->setViewParam('lastname',$buzdata->lastname)
                    ->setViewParam('username',$data['username'])
                    ->setViewParam('password',$buzdata->password);//$postAry['password']
                $m->sendHtmlTemplate("logindetails.phtml");
            }
            return $this->_helper->redirector('findfriends');
        }
        $categories = $businesscategoiresdb->get_business_categories();
        $this->view->assign('pwd', $buzdata->password);
        $this->view->assign('action', "profileinfo");
        $this->view->assign("categories",$categories);
    }

    public function findfriendsAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        if(!empty($loggedUserId)) {
            $this->_redirect('business/dashboard');
        }
        $buzdata = new Zend_Session_Namespace('buzdata');
        if($buzdata->bId == '') {
            $this->_redirect('business/businessinfo');
        }
        if($this->getRequest()->isPost()) {
            return $this->_helper->redirector('profilepicture');
        }
        $this->view->assign('action', "findfriends");
    }

    public function profilepictureAction() {
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
        }
        if(!empty($loggedUserId)) {
            $this->_redirect('business/dashboard');
        }
        $form = new Business_Form_ProfileUploadForm();
        $request = $this->getRequest();
        $buzdata = new Zend_Session_Namespace('buzdata');
        if($buzdata->bId == '') {
            $this->_redirect('business/businessinfo');
        }
        $bid = $buzdata->bId;
        $uname = $buzdata->username;
        $pwd = $buzdata->password;
        $data = '';
        if($request->isPost()) {
            //echo '<pre>';print_r($_FILES);exit;
            if ($form->isValid($request->getPost())) {
                if(!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == 0) {
                    $dir_upload = UPLOAD_PATH.'images/original/';
                    $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                    $medium_path = UPLOAD_PATH.'images/medium/';
                    $mobile_path = UPLOAD_PATH.'images/mobile/';
                    //$thumb_width  = 100;
                    //$thumb_height = 100;
                    $fileName = $_FILES['photo']['name'];
                    //$fullPathNameFile = $dir_upload.$fileName;
                    $image = new UpmeSocial_Controller_Action_Helper_Image();
                    $arrFileName = $image->saveBusinessImage($dir_upload,$thumb_path,$medium_path,$mobile_path);
                    //Saving images to business_user
                    $businessdb = new Business_Model_Business();
                    $data['image_path'] = $arrFileName['photo'];
                    $data['status'] = 'A';

                    $album['user_id'] = $bid;
                    $album['user_type'] = 'B';
                    $album['album_name'] = 'Profile Photos';
                    $album['is_default'] = '1';
                    $albumobj = new Mobile_Model_Albums();
                    $val = $albumobj->albumnameexistsrnot($album);
                    $album['created_date'] = date('Y-m-d H:i:s');
                    if(!empty($val['id'])) {
                        $id = $albumobj->update($album,$val['id']);
                        $album_id = $val['id'];
                    } else {
                        $album_id = $albumobj->insert($album);
                    }
                    $photoObj = new Mobile_Model_Photos();
                    //Saving data based on phototype
                    $photodata['album_id'] = $album_id;
                    $photodata['photo_name'] = 'Profile Photo';
                    $photodata['photo_original_name'] = $fileName;
                    $photodata['photo_modified_name'] = $arrFileName['photo'];
                    $photodata['user_id'] = $bid;
                    $photodata['user_type'] = 'B';
                    $coverphoto = $photoObj->albumidexistsrnot($album_id,$bid,'B');
                    if(!empty($coverphoto['photo_id']))
                        $photodata['cover_photo'] = '0';
                    else
                        $photodata['cover_photo'] = '1';
                    $photodata['created_date'] = date('Y-m-d H:i:s');
                    $photo_id = $photoObj->insert($photodata);
                    $businessdb->updatebusiness($data, $bid);
                    $uName = $uname;
                    $uPwd = $pwd;
                    if($uName == '' || $uPwd == '')
                        $this->_helper->redirector('index','index','business');
                    $adapter = new Zend_Auth_Adapter_DbTable(
                            $this->_getParam('db'),
                            'tbl_business_users',
                            'username',
                            'password',
                            'MD5(CONCAT(?, "'.SECURITY_SALT.'"))'
                    );
                    // We're authenticated! Redirect to the home page
                    $adapter->setIdentity($uName);
                    $adapter->setCredential($uPwd);
                    $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
                    $result = $auth->authenticate($adapter);
                    if ($result->isValid()) {
                        $user = $adapter->getResultRowObject();
                        $auth->getStorage()->write($user);
                        $resultAry = array('service_status'=>'success',"content" => $user);
                    } else {
                        $resultAry = array('service_status'=>'error',"content" => '');
                    }
                    $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry);
                    $this->_helper->redirector('businessprofile');
                } else {
                    $resultAry = array('service_status'=>'error',"content" => '');
                }
            }
        }
        $this->view->form = $form;
        $this->view->assign('action', "profilepicture");
    }

    public function businessprofileAction() {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
            //$userauth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            if (!$auth->hasIdentity()) {
                $auth1 = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
                $businessuserdata = $auth->getIdentity();
                //echo '<pre>';print_r($businessuserdata);exit;
                if(!$auth->hasIdentity() || !$auth1->hasIdentity()) {
                    $this->_helper->redirector('index');
                }
            }
            $user_id = $this->getRequest()->getParam('userid');
            $businessdb = new Business_Model_Business();
            $followerdb = new Business_Model_Followers;
            $reviewsdb = new Business_Model_Reviews();
            if(!empty($user_id) && $user_id != 'userid') {
                $checkavailable = $businessdb->isUserAvail($user_id);
                if(!empty($checkavailable)) {
                    //echo '<pre>';print_r($businessuserdata);exit;
                    if(!empty($businessuserdata) && $businessuserdata->user_type != "B") {
                        $this->_redirect('user/userprofile');
                    }
                    $this->_redirect('business/businessprofile');
                } else {
                    $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
                }
            } else {
                $user_id = $auth->getIdentity()->business_id;
                $buzdata = new Zend_Session_Namespace('buzdata');
                if(!empty($buzdata->bId)) {
                    $user_id = $buzdata->bId;
                }
                $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
            }
            if(empty($businessuserdata) && !empty($user_id) && $user_id != 'userid') {
                $this->_helper->redirector('index','businessprofile','business');
                return;
            }
            if(!empty($businessuserdata)) {
                $following = $followerdb->getFollowing($businessuserdata->business_id,'B');
                $followers = $followerdb->getFollowers($businessuserdata->business_id,'','B','10');
                $followingcnt = $followerdb->getFollowingCnt($businessuserdata->business_id,'B');
                $followerscnt = $followerdb->getFollowersCnt($businessuserdata->business_id,'B');
                $reviewscnt = $reviewsdb->getReviewsCnt($businessuserdata->business_id,'B');
                $isfollow = $followerdb->isLoggedUserFollowingOtherUser($user_id,'B');
                $couponsObj = new Business_Model_Coupons;
                $couponsinfo = $couponsObj->getcouponsinfo($businessuserdata->business_id);
                if($couponsinfo == '') {
                    $couponsinfo = '';
                }
            } else {
                $following = $followerdb->getFollowing($this->getRequest()->getParam('userid'),'B');
                $followers = $followerdb->getFollowers($this->getRequest()->getParam('userid'),'','B','10');
                $followingcnt = $followerdb->getFollowingCnt($this->getRequest()->getParam('userid'),'B');
                $followerscnt = $followerdb->getFollowersCnt($this->getRequest()->getParam('userid'),'B');
                $reviewscnt = $reviewsdb->getReviewsCnt($this->getRequest()->getParam('userid'),'B');
                $couponsObj = new Business_Model_Coupons;
                $couponsinfo = $couponsObj->getcouponsinfo($this->getRequest()->getParam('userid'));
                $this->view->assign('page_Name',"businessprofile");
                $this->view->assign('businessuserdata', $businessuserdata);
                $this->view->assign('followerscnt', $followerscnt);
                $this->view->assign('followingcnt', $followingcnt);
                $this->view->assign('reviewscnt', $reviewscnt);
                $this->view->assign('following', $following);
                $this->view->assign('followers', $followers);
                $this->view->assign('isfollow', $isfollow);
                $this->view->assign('couponsinfo', $couponsinfo);
            }
            if($businessuserdata->qrcode == '') {
                // FOR Storing Unique QR CODE in DB
                $length = 5;
                $char_key = '';
                $num_key = '';
                $characters = array("B","C","D","F","G","H","J","K","L","M","N",
                "P","Q","R","S","T","V","W","X","Y","Z","b","c","d","f","g","h",
                "j","k","l","m","n","p","q","r","s","t","v","w","x","y","z");
                for ($i = 0; $i < $length-2; $i++) {
                    $char_key .= $characters[array_rand($characters)];
                }
                $charArr = str_split($char_key);
                $numbers = array("1","2","3","4","5","6","7","8","9");
                for ($i = 0; $i < $length-3; $i++) {
                    $num_key .= $numbers[array_rand($numbers)];
                }
                $numArr = str_split($num_key);
                $key = '';
                $keys = array_merge($numArr, $charArr);
                $keyString = str_shuffle(implode('', $keys));
                $qrCode =  $keyString.$user_id;
                // to check if QRCode String is Updated in DB or not
                $qrArr = $businessdb->checkQrAvailable($qrCode);
                if(count($qrArr) == 0) {
                    $upd = $businessdb->updateQRCode($user_id, $qrCode);
                }
            } else {
                $qrCode = $businessuserdata->qrcode;
            }
            //For Business Image Slider
            $mobileobj = new Mobile_Model_Albums();
            $imagegallery = $mobileobj->getBusinessAlbumsByUserId($user_id, 'B');
            // FOr GENERATING QR CODE IF NOT GENERATED AND STORING IN FOLDER
            $QRCodeHelper = new UpmeSocial_View_Helper_QRCode();
            $QRC = $QRCodeHelper->googleQRCode($businessuserdata->username, SITE_URL."business/followbusiness/".$qrCode);
            $this->view->assign('qrCode',$qrCode);
            $this->view->assign('QRC',$QRC);
            $this->view->assign('page_Name',"businessprofile");
            $this->view->assign('businessuserdata', $businessuserdata);
            $this->view->assign('imagegallery', $imagegallery);
            $this->view->assign('followerscnt', $followerscnt);
            $this->view->assign('followingcnt', $followingcnt);
            $this->view->assign('reviewscnt', $reviewscnt);
            $this->view->assign('following', $following);
            $this->view->assign('followers', $followers);
            $this->view->assign('isfollow', $isfollow);
            $this->view->assign('couponsinfo', $couponsinfo);
            if(!empty($user_id))
                    $this->view->assign('user_id', $user_id);
    }

    public function businessmysettingsAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        if (!$auth->hasIdentity()) {
            $this->_helper->redirector('index');
        }
        $businessdb = new Business_Model_Business();
        $businesscategoiresdb = new Business_Model_Businesscategories();
        $user_id = $auth->getIdentity()->business_id;
        $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
        $businesskeywords = $businesscategoiresdb->getbusinesskeywords($user_id);
        //echo '<pre>';print_r($businesskeywords);exit;
        foreach($businesskeywords as $businesskey):
            $businesskeyword[] = $businesskey['keyword'];
        endforeach;
        if(!empty($businesskeyword)) {
            $bkeyword = implode(',', $businesskeyword);
        }
        //Fetching Countries List
        $countriesdb = new User_Model_Countries();
        $countries = $countriesdb->get_countries();
        //Fetching States Based On CountryId
        $statesdb = new User_Model_States();
        $states = $statesdb->get_states($businessuserdata->country);
        //Fetching cities Based On StateId
        $citiesdb = new User_Model_Cities();
        $cities = $citiesdb->get_cities($businessuserdata->state);
        //Fetching Zipcode Based On CityId
        $zipcodes = $citiesdb->getZipcodesByCity($businessuserdata->city);
        //Fetching Countries, states and citye based on country and state id
        //$location = $countriesdb->getlocation($businessuserdata->country,$businessuserdata->state);
        //Fectching Categories List
        $categories = $businesscategoiresdb->get_business_categories();
        //Assigning Values to View Page
        $this->view->assign('page_Name',"businessmysettings");
        $this->view->assign('businessdata',$businessuserdata);
        if(!empty($bkeyword)) {
            $this->view->assign('keywords',$bkeyword);
        }
        $this->view->assign("countries",$countries);
        $this->view->assign("states",$states);
        $this->view->assign("cities",$cities);        
        $this->view->assign("zipcode",$zipcodes);
        $this->view->assign("categories",$categories);
    }
    
    public function businessphotosAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        $request = $this->getRequest();
        $mobileobj = new Mobile_Model_Albums();
        $photoObj = new Mobile_Model_Photos();
        $videoObj = new Videos_Model_Videos();
        $businessdb = new Business_Model_Business();
        if (!$auth->hasIdentity()) {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $businessuserdata = $businessdb->getBusinessUserDetails($request->getParam('business_id'));
            if(!$auth->hasIdentity()) {
                $this->_helper->redirector('index');
            }
        }
        //echo '<pre>';print_r($request->getParams());exit;
        if($request->getParam('business_id') == '') {
            $user_id = $auth->getIdentity()->business_id;
            $user_type = $auth->getIdentity()->user_type;
            $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
            //Fectching Categories List
            $businesscategoiresdb = new Business_Model_Businesscategories();
            $categories = $businesscategoiresdb->get_business_categories();
            $imagegallery = $mobileobj->getBusinessAlbumsByUserId($user_id, $user_type);
            $videogallery = $videoObj->getBusinessVideosByUserId($user_id, $user_type);
            //Assigning Values to View Page
            $this->view->assign('page_Name',"businessphotos");
            $this->view->assign('businessuserdata',$businessuserdata);
            $this->view->assign("categories",$categories);
            $this->view->assign('imagegallery',$imagegallery);
            $this->view->assign('videogallery',$videogallery);
        }
        if($request->getParam('business_id') != '') {
            $businessdb = new Business_Model_Business();
            if(Zend_Registry::isRegistered('businessdata')) {
                $logged_user_data = Zend_Registry::get('businessdata');
            }
            $user_id = $request->getParam('business_id');
            $checkavailable = $businessdb->isUserAvail($user_id);
            if(!empty($checkavailable)) {
                $this->_redirect('user/userprofile');
            }
            $user_type = "B"; //$logged_user_data->user_type;
            if($request->isPost()) {
                if(!empty($_FILES['business_photo']['name'])) {
                    $dir_upload = UPLOAD_PATH.'images/original/';
                    $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                    $medium_path = UPLOAD_PATH.'images/medium/';
                    $mobile_path = UPLOAD_PATH.'images/mobile/';
                    $android_path = UPLOAD_PATH.'images/android/';
                    $fileName = $_FILES['business_photo']['name'];
                    $image = new UpmeSocial_Controller_Action_Helper_Image();
                    $arrFileName = $image->saveBusinessImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
                    //echo '<pre>';print_R($arrFileName);exit;
                    //Creating album for Business Images
                    $album['user_id'] = $user_id;
                    $album['user_type'] = $user_type;
                    $album['album_name'] = 'Business Photos';
                    $val = $mobileobj->albumnameexistsrnot($album);
                    //echo $val;exit;
                    $album['created_date'] = date('Y-m-d H:i:s');
                    if(!empty($val)) {
                        $albumdet = $mobileobj->getBusinessAlbumsByUserId($user_id, 'B');
                        $id = $mobileobj->update($album,$albumdet['0']['id']);
                        $album_id = $albumdet['0']['id'];
                    }
                    if($val == '') {
                        $album['is_default'] = '1';
                        $album_id = $mobileobj->insert($album);
                    }
                    $coverphoto = $photoObj->albumidexistsrnot($album_id,$user_id,$user_type);
                    //Saving data  to Photos tbl
                    $photodata['album_id'] = $album_id;
                    $photodata['photo_name'] = 'Business Photo';
                    $photodata['photo_original_name'] = $fileName;
                    $photodata['photo_modified_name'] = $arrFileName['business_photo'];
                    $photodata['width'] = $arrFileName['width'];
                    $photodata['height'] = $arrFileName['height'];
                    $photodata['android_width'] = $arrFileName['android_width'];
                    $photodata['android_height'] = $arrFileName['android_height'];
                    $photodata['user_id'] = $user_id;
                    $photodata['user_type'] = $user_type;
                    if(!empty($coverphoto['photo_id']))
                        $photodata['cover_photo'] = '0';
                    else
                        $photodata['cover_photo'] = '1';
                    $photodata['created_date'] = date('Y-m-d H:i:s');
                    $photo_id = $photoObj->insert($photodata);
                    $this->_redirect('business/businessphotos/'.$user_id);
                }
                if(!empty($_FILES['business_video']['name'])) {
                    $dir_upload = UPLOAD_PATH.'videos/';
                    $original_name = $_FILES['business_video']['name'];
                    if(isset($_FILES['business_video'])) {
                        $ext = pathinfo($_FILES['business_video']['name']);
                        $_FILES['business_video']['name'] = $user_id.'_'.time().'.'.$ext['extension'];
                        $_FILES['business_video']['posted_by'] = $user_id;
                    }
                    $upload = new Zend_File_Transfer_Adapter_Http();
                    $upload->setDestination($dir_upload);
                    $config = array(
                                'uploadPath'		=> UPLOAD_PATH.'videos/',
                                'outputPath'		=> UPLOAD_PATH.'videos/',
                                'thumbPath'		=> UPLOAD_PATH.'videos/',
                                'conversionLog'		=> '/var/www/convertvideo/flvfiles/conversionLog.log',
                                'errorLog'		=> '/var/www/convertvideo/flvfiles/errorLog.log',
                                'conversionScript' 	=> '/var/www/convertvideo/processVideo.php',
                                'outputFormat'		=> 'mp4', // either 'mp4' or 'flv'
                                'bitRate'		=> 32000,
                                'sampleRate'		=> 22050,
                                'videoMaxWidth'		=> 320,
                                'videoMaxHeight' 	=> 240,
                                'thumbMaxWidth' 	=> 320,
                                'thumbMaxHeight'	=> 240,
                                'minDuration'		=> 1,
                                'videoThumbDepth'	=> 25, // % into video to get thumbnail
                        );
                    $file = isset($_FILES['business_video']) ? $_FILES['business_video'] : null;
                    $converter = new UpmeSocial_Controller_Action_Helper_VideoConverter($file, $config);
                    $details = $converter->getDetails();
                    //echo $original_name.'<pre>';print_r($details);exit;
                    $converter->processVideo();
                    $updateAry['profile_video_path'] = $details['title'].'.'.$ext['extension'];
                    $updateAry['profile_video_thumbnail_path'] = $details['thumbnail'];
                    $videoAry['video_name'] = $original_name;
                    $videoAry['video_path'] = $details['title'].'.'.$ext['extension'];
                    $videoAry['thumbnail_path'] = $details['thumbnail'];
                    $videoAry['user_id'] = $user_id;
                    $videoAry['user_type'] = $user_type;
                    $videoAry['created_date'] = date('Y-m-d H:i:s');
                    $video_id = $videoObj->insertVideo($videoAry);
                    $this->_redirect('business/businessphotos/'.$user_id);
                }
            }
            //Fectching Categories List
            /*$businesscategoiresdb = new Business_Model_Businesscategories();
            $categories = $businesscategoiresdb->get_business_categories();*/
            $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
            $imagegallery = $mobileobj->getBusinessAlbumsByUserId($user_id, $user_type);
            $videogallery = $videoObj->getBusinessVideosByUserId($user_id, $user_type);
            //Assigning Values to View Page
            $this->view->assign('page_Name',"businessphotos");
            $this->view->assign('businessuserdata',$businessuserdata);
            //$this->view->assign("categories",$categories);
            $this->view->assign('imagegallery',$imagegallery);
            $this->view->assign('videogallery',$videogallery);
        }
        if($businessuserdata->qrcode == '') {
            // FOR Storing Unique QR CODE in DB
            $length = 5;
            $char_key = '';
            $num_key = '';
            $characters = array("B","C","D","F","G","H","J","K","L","M","N",
            "P","Q","R","S","T","V","W","X","Y","Z","b","c","d","f","g","h",
            "j","k","l","m","n","p","q","r","s","t","v","w","x","y","z");
            for ($i = 0; $i < $length-2; $i++) {
                $char_key .= $characters[array_rand($characters)];
            }
            $charArr = str_split($char_key);
            $numbers = array("1","2","3","4","5","6","7","8","9");
            for ($i = 0; $i < $length-3; $i++) {
                $num_key .= $numbers[array_rand($numbers)];
            }
            $numArr = str_split($num_key);
            $key = '';
            $keys = array_merge($numArr, $charArr);
            $keyString = str_shuffle(implode('', $keys));
            $qrCode =  $keyString.$user_id;
            // to check if QRCode String is Updated in DB or not
            $qrArr = $businessdb->checkQrAvailable($qrCode);
            if(count($qrArr) == 0) {
                $upd = $businessdb->updateQRCode($user_id, $qrCode);
            }
        }
        if($businessuserdata->qrcode != '') {
            $qrCode = $businessuserdata->qrcode;
        }
        // FOr GENERATING QR CODE IF NOT GENERATED AND STORING IN FOLDER
        $QRCodeHelper = new UpmeSocial_View_Helper_QRCode();
        $QRC = $QRCodeHelper->googleQRCode($businessuserdata->username, SITE_URL."business/followbusiness/".$qrCode);
        $this->view->assign('qrCode',$qrCode);
        $this->view->assign('QRC',$QRC);
    }

    public function deletephotoAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        $request = $this->getRequest();
        $photoid = $request->getParam('photo_id');
        $photoObj = new Mobile_Model_Photos();
        $albumObj = new Mobile_Model_Albums();
        $businessdb = new Business_Model_Business();
        if (!$auth->hasIdentity()) {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $businessuserdata = $businessdb->getBusinessUserDetails($request->getParam('business_id'));
            if(!$auth->hasIdentity()){
            $this->_helper->redirector('index');}
        }
        $Photos = $photoObj->getphotobyphotoid($photoid);
        $album_id = $Photos['album_id'];
        $albumphotos = $albumObj->getalbumphotos($album_id);
        $cnt = COUNT($albumphotos);
        if($cnt == 1) {
            $deletephoto = $albumObj->deleteAlbum($album_id);
        } else {
            $deletephoto = $photoObj->deletePhoto($photoid);
        }
        if($deletephoto == '1') {
            echo '1';exit;
        }
    }

    public function deletevideoAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        $request = $this->getRequest();
        $videoid = $request->getParam('video_id');
        $videoObj = new Videos_Model_Videos();
        $businessdb = new Business_Model_Business();
        if (!$auth->hasIdentity()) {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $businessuserdata = $businessdb->getBusinessUserDetails($request->getParam('business_id'));
            if(!$auth->hasIdentity()){
            $this->_helper->redirector('index');}
        }
        $video = $videoObj->getvideosbyvideoid($videoid);
        $deletephoto = $videoObj->deleteVideo($video);
        if($deletephoto == '1') {
            echo '1';exit;
        }
    }
    
    public function businessmapAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        $request = $this->getRequest();
        if (!$auth->hasIdentity()) {
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $request = $this->getRequest();
            $businessdb = new Business_Model_Business();
            $businessuserdata = $businessdb->getBusinessUserDetails($request->getParam('business_id'));
            if(!$auth->hasIdentity()) {
                $this->_helper->redirector('index');
            }
        }
        $user_id = $this->getRequest()->getParam('business_id');
        if(!empty($user_id)) {
            $this->view->assign('user_id',$user_id);
        }
        if($request->getParam('business_id') == '') {
            $businessdb = new Business_Model_Business();
            $user_id = $auth->getIdentity()->business_id;
            $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
            //Fetching cities Based On StateId
            $citiesdb = new User_Model_Cities();
            $cities = $citiesdb->get_cities($businessuserdata->state);
            //Assigning Values to View Page
            $this->view->assign('businessuserdata',$businessuserdata);
            $this->view->assign("cities",$cities);
        } else {
            $businessdb = new Business_Model_Business();
            $user_id = $request->getParam('business_id');
            $checkavailable = $businessdb->isUserAvail($user_id);
            if(!empty($checkavailable)) {
                $this->_redirect('user/userprofile');
            }
            $businessuserdata = $businessdb->getBusinessUserDetails($user_id);//echo '<pre>';print_r($businessuserdata);exit;
            //Assigning Values to View Page
            $this->view->assign('page_Name',"businessmap");
            $this->view->assign('businessuserdata',$businessuserdata);
        }
        if($businessuserdata->qrcode == '') {
            // FOR Storing Unique QR CODE in DB
            $length = 5;
            $char_key = '';
            $num_key = '';
            $characters = array("B","C","D","F","G","H","J","K","L","M","N",
            "P","Q","R","S","T","V","W","X","Y","Z","b","c","d","f","g","h",
            "j","k","l","m","n","p","q","r","s","t","v","w","x","y","z");
            for ($i = 0; $i < $length-2; $i++) {
                $char_key .= $characters[array_rand($characters)];
            }
            $charArr = str_split($char_key);
            $numbers = array("1","2","3","4","5","6","7","8","9");
            for ($i = 0; $i < $length-3; $i++) {
                $num_key .= $numbers[array_rand($numbers)];
            }
            $numArr = str_split($num_key);
            $key = '';
            $keys = array_merge($numArr, $charArr);
            $keyString = str_shuffle(implode('', $keys));
            $qrCode =  $keyString.$user_id;
            // to check if QRCode String is Updated in DB or not
            $qrArr = $businessdb->checkQrAvailable($qrCode);
            if(count($qrArr) == 0) {
                $upd = $businessdb->updateQRCode($user_id, $qrCode);
            }
        } else {
            $qrCode = $businessuserdata->qrcode;
        }
        // FOr GENERATING QR CODE IF NOT GENERATED AND STORING IN FOLDER
        $QRCodeHelper = new UpmeSocial_View_Helper_QRCode();
        $QRC = $QRCodeHelper->googleQRCode($businessuserdata->username, SITE_URL."business/followbusiness/".$qrCode);
        $this->view->assign('qrCode',$qrCode);
        $this->view->assign('QRC',$QRC);
    }

    public function editdataAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $businessdb = new Business_Model_Business();
        $businesscatObj = new Business_Model_Businesscategories();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        if (!$auth->hasIdentity()) {
            $this->_helper->redirector('index');
        }
        $user_id = $auth->getIdentity()->business_id;
        $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
        $bid = $businessuserdata->business_id;
        if($this->getRequest()->getParam('For') == 'username') {
            $username = $this->getRequest()->getParam('username');
            $unameexistsrnot = $businessdb->isUsernameAvail($username, $bid);
            if(!empty($unameexistsrnot)) {
                if($username == $businessuserdata->username)
                    echo 'same';
                if($username != $businessuserdata->username) {
                    $data['username'] = $username;
                    $data['business_updated_date'] = date('Y-m-d H:i:s');
                    $id = $businessdb->updatebusiness($data, $bid);
                    if($id == '1')
                        echo 'success';
                    else
                        echo 'fail';
                }
            } else {
                echo 'exists';
            }
        }
        if($this->getRequest()->getParam('For') == 'password') {
            $password = $this->getRequest()->getParam('password');
            $data['password'] = md5($password.SECURITY_SALT);
            $data['business_updated_date'] = date('Y-m-d H:i:s');
            $id = $businessdb->updatebusiness($data, $bid);
            if($id == '1')
                echo 'success';
            else
                echo 'fail';
        }
        if($this->getRequest()->getParam('For') == 'email') {
            $email = $this->getRequest()->getParam('email');
            $emailexistsrnot = $businessdb->isEmailAvail($email, $bid);
            if(!empty($emailexistsrnot)) {
                if($email == $businessuserdata->email)
                    echo 'same';
                if($email != $businessuserdata->email) {
                    $data['email'] = $email;
                    $data['business_updated_date'] = date('Y-m-d H:i:s');
                    $id = $businessdb->updatebusiness($data, $bid);
                    if($id == '1')
                        echo 'success';
                    else
                        echo 'fail';
                }
            } else {
                echo 'exists';
            }
        }
        if($this->getRequest()->getParam('For') == 'company info') {
            $keydata = $this->getRequest()->getParam('keywords');
            $keyids = array();
            $keys = explode(',', $keydata);
            foreach($keys as $i=>$key):
                $existsrnot = $businesscatObj->keywordexistrnot($key);
                //echo 'exists: <pre>';print_r($existsrnot);exit;
                if($existsrnot == '') {
                    $kid = $businesscatObj->insertkey($key);
                    $keyids[$i] = $kid;
                } else {
                    $keyids[$i] = $existsrnot['id'];
                }
            endforeach;
            foreach($keyids as $keyid):
                    $bkeydata['uid'] = $bid;
                    $bkeydata['keyid'] = $keyid;
                    $keyexists = $businesscatObj->keyexists($keyid,$bid);
                    if($keyexists == '') {
                        $bkid = $businesscatObj->insertbkeydata($bkeydata);
                    }
                endforeach;
            //echo '<pre>';print_r($keyids);exit;
            $data['firstname'] = $this->getRequest()->getParam('firstname');
            $data['lastname'] = $this->getRequest()->getParam('lastname');
            $data['business_name'] = $this->getRequest()->getParam('business_name');
            $data['country'] = $this->getRequest()->getParam('country');
            $data['state'] = $this->getRequest()->getParam('state');
            $data['city'] = $this->getRequest()->getParam('city');
            $data['zipcode'] = $this->getRequest()->getParam('zipcode');
            $data['website'] = $this->getRequest()->getParam('website');
            $data['security_question'] = $this->getRequest()->getParam('security_question');
            $data['security_answer'] = $this->getRequest()->getParam('security_answer');
            $data['phone_no'] = $this->getRequest()->getParam('phone_no');
            $data['category_id'] = $this->getRequest()->getParam('business_category');
            $data['address_line_1'] = $this->getRequest()->getParam('address1');
            $data['address_line_2'] = $this->getRequest()->getParam('address2');
            $data['business_updated_date'] = date('Y-m-d H:i:s');
            $id = $businessdb->updatebusiness($data, $bid);
            $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
            $user = $auth->getIdentity();
            $user->firstname = $data['firstname'];
            $auth->getStorage()->write($user);
            if($id == '1')
                echo 'success';
            else
                echo 'fail';
        }
        if($this->getRequest()->getParam('For') == 'description') {
            $description = $this->getRequest()->getParam('description');
            $data['description'] = $description;
            $data['business_updated_date'] = date('Y-m-d H:i:s');
            $id = $businessdb->updatebusiness($data, $bid);
            if($id == '1')
                echo 'success';
            else
                echo 'fail';
        }
    }

    public function profilepictureeditAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        if (!$auth->hasIdentity()) {
            $this->_helper->redirector('index');
        }
        $this->_helper->layout->disableLayout();
        $request = $this->getRequest();
        $businessdb = new Business_Model_Business();
        $user_id = $auth->getIdentity()->business_id;
        //$businessdata = $businessdb->getBusinessUserDetails($user_id);
        $imagedet = $businessdb->getBusinessUserImage($user_id);
        $image_name = $imagedet['image_path'];
        $form = new Business_Form_ProfileUploadForm();
        if($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                if(!empty($_FILES['photo']['name'])) {
                    $dir_upload = UPLOAD_PATH.'images/original/';
                    $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                    $medium_path = UPLOAD_PATH.'images/medium/';
                    $mobile_path = UPLOAD_PATH.'images/mobile/';
                    $android_path = UPLOAD_PATH.'images/android/';
                    $fileName = $_FILES['photo']['name'];
                    $image = new UpmeSocial_Controller_Action_Helper_Image();
                    $arrFileName = $image->saveBusinessImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
                    //Saving imagesedit to business_user
                    $data['image_path'] = $arrFileName['photo'];
                    $album['user_id'] = $user_id;
                    $album['user_type'] = 'B';
                    $album['album_name'] = 'Profile Photos';
                    $mobileobj = new Mobile_Model_Albums();
                    $val = $mobileobj->albumnameexistsrnot($album);
                    $album['created_date'] = date('Y-m-d H:i:s');
                    if(!empty($val)) {
                        $albumdet = $mobileobj->getAlbumsByUserId($user_id, 'B');
                        $id = $mobileobj->update($album,$albumdet['0']['id']);
                        $albumid = $albumdet['0']['id'];
                    } else {
                        $album['is_default'] = '1';
                        $albumid = $mobileobj->insert($album);
                    }
                    $photoObj = new Mobile_Model_Photos();
                    $coverphoto = $photoObj->albumidexistsrnot($albumid,$user_id,'B');
                    $photodata['album_id'] = $albumid;
                    $photodata['photo_name'] = 'Profile Photo';
                    $photodata['photo_original_name'] = $fileName;
                    $photodata['photo_modified_name'] = $data['image_path'];
                    $photodata['width'] = $arrFileName['width'];
                    $photodata['height'] = $arrFileName['height'];
                    $photodata['android_width'] = $arrFileName['android_width'];
                    $photodata['android_height'] = $arrFileName['android_height'];
                    $photodata['user_id'] = $user_id;
                    $photodata['user_type'] = 'B';
                    if(!empty($coverphoto['photo_id']))
                        $photodata['cover_photo'] = '0';
                    else
                        $photodata['cover_photo'] = '1';
                    $photodata['created_date'] = date('Y-m-d H:i:s');
                    /*if(!empty($coverphoto)) {
                        $photo_id = $photoObj->update($photodata,$albumid);
                    } else {*/
                    $photo_id = $photoObj->insert($photodata);
                    $this->_helper->viewRenderer->setNoRender();
                    //}
                    $id = $businessdb->updatebusiness($data, $user_id);
                    if($id == '1') {
                        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
                        echo $myjson->customEncode(array('service_status'=>'success',"message" => 'Uploaded Successfully!!!'));
                        return;
                    }
                    $image_name = $data['image_path'];
                }
            }
        }
        $this->view->form = $form;
        //$this->view->assign('businessdata', $businessdata);
        $this->view->assign('image_name', $image_name);
    }

    public function createcouponAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        if (!$auth->hasIdentity()) {
            $this->_helper->redirector('index');
        }
        $businessdb = new Business_Model_Business();
        $businessfollowingObj = new Business_Model_Followers();
        $businessuser_id = $auth->getIdentity()->business_id;
        $businessdata = $businessdb->getBusinessUserDetails($businessuser_id);//echo '<pre>';print_r($businessdata);
        $businessfollowingdata = $businessfollowingObj->getFollowers($businessuser_id,$businessuser_id,'B');
        $uppercode = $this->__generateuppercode();
        //echo '<pre>';print_r($uppercode);exit;
        if($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getParam('business');//echo '1<pre>';print_r($data);
            //$data['expiration_date'] = $this->getRequest()->getParam('expiration_date');echo '2<pre>';print_r($data);
            $data['business_id'] = $businessuser_id;
            $data['created_date'] = date('Y-m-d H:i:s');
            if(!empty($_FILES['coupon_image']['name'])) {
                $dir_upload = UPLOAD_PATH.'images/original/';
                $thumb_path = UPLOAD_PATH.'images/thumbnails/';
                $medium_path = UPLOAD_PATH.'images/medium/';
                $mobile_path = UPLOAD_PATH.'images/mobile/';
                $android_path = UPLOAD_PATH.'images/android/';
                //$fileName = $_FILES['coupon_image']['name'];
                //$fullPathNameFile = $dir_upload.$fileName;
                //echo '<pre>';print_r($_FILES);exit;
                $image = new UpmeSocial_Controller_Action_Helper_Image();
                $arrFileName = $image->saveBusinessImage($dir_upload,$thumb_path,$medium_path,$mobile_path,$android_path);
                //Saving images to business_user
                $data['image_path'] = $arrFileName['coupon_image'];
            }
            $couponsObj = new Business_Model_Coupons;
            $id = $couponsObj->insert($data);
            foreach ($businessfollowingdata as $key => $value) {
                $resAry['reference_id'] = $id;
                $resAry['user_id'] = $value['user_id'];
                $resAry['user_type'] = $value['user_type'];
                $resAry['notifications_type'] = 'new_upper';
                $resAry['action_date'] = date('Y-m-d H:i:s');
                $notificationObj = new Notifications_Model_Notifications();
                $notificationObj->insertNewRecord($resAry);
            }
            if(!empty($id)) {
                //$this->view->assign("successmsg","Uppers has been inserted Successfully");
                $this->_helper->redirector('myuppers','index','business');
            } else {
                $this->view->assign("errormsg","Some Thing went Wrong");
            }
        }
        //Fectching Categories List
        $businesscategoiresdb = new Business_Model_Businesscategories();
        $categories = $businesscategoiresdb->get_business_categories();
        $this->view->assign("categories",$categories);
        $this->view->assign('businessdata', $businessdata);
        $this->view->assign('uppercode', $uppercode);
    }
    
    protected function __generateuppercode() {
        $string = '1234567890';
	$length =5;
	$code = "";
	for ($i= 0; $i<$length;$i++){
            $code = $code.$string[mt_rand(0, strlen($string)-1)];
	}
	return $code;
    }

    public function deletecouponAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $coupon_id = $this->getRequest()->getParam('coupon_id');
        $couponsObj = new Business_Model_Coupons;
        $coupons = $couponsObj->getcouponbyid($coupon_id);
        if(!empty($coupons)) {
            $coupondata = $couponsObj->deleteCoupon($coupons);
            echo '1';
        } else {
            echo '0';
        }
    }
    
    public function purchasecouponAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        if($auth->hasIdentity()){
            $user_id = $auth->getIdentity()->user_id;
            $user_type = 'U';
        }
        $request = $this->getRequest();
        //$business_id = $request->getParam('business_id');//echo '<pre>';print_r($business_id);exit;
        $coupon_id = $request->getParam('coupon_id');
        $couponsObj = new Business_Model_Coupons;
        $coupons = $couponsObj->getcouponbyid($coupon_id);//echo '<pre>';print_r($coupons);exit;
        if($user_id == '') {
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        if($coupon_id == '') {
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Upper Id can not be Empty!!!'));
            return;
        }
        $data['user_id'] = $user_id;
        $data['coupon_id'] = $coupon_id;
        $data['purchase_date'] = date('Y-m-d H:i:s');
        $purchasecouponsObj = new Business_Model_Purchasecoupons;
        $stat = $purchasecouponsObj->insert($data);

        $resAry['reference_id'] = $stat;
        $resAry['user_id'] = $user_id;
        $resAry['user_type'] = $user_type;
        $resAry['notifications_type'] = 'purchasecoupon';
        $resAry['action_date'] = date('Y-m-d H:i:s');
		//if($_SERVER['REMOTE_ADDR'] == '192.168.1.63' || $_SERVER['REMOTE_ADDR'] == '182.72.66.214') {
//        	echo "<pre>1";print_r($resAry);exit;
//		}
        $notificationObj = new Notifications_Model_Notifications();
        $stat = $notificationObj->insertNewRecord($resAry);
        if($stat){
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'success',"content" =>$stat));           
            return;
        } else {
            $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Error while processing!!!. Please try again.'));
            return;
        }
        //$puchasecoupons = $purchasecouponsObj->getpurchasecouponsinfo($business_id);
        //echo '<pre>';print_r($puchasecoupons);exit;
    }
    
    public function mycustomersAction(){
        $loggedUserId = '';
        
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $business_id = $logged_user_data->business_id;
            $business_type = 'B';
            $loggedUserId = $logged_user_data->business_id;
        }
        
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if($loggedUserId =='') {
            $this->_redirect('business/dashboard');
        }
        
        $businessdb = new Business_Model_Business();
        $purchasecouponsObj = new Business_Model_Purchasecoupons;
        if($this->getRequest()->getParam('userid') != '')
            $business_id = $this->getRequest()->getParam('userid');
        if($business_id == '') {
            $this->_redirect('business/');
        }
        $checkavailable = $businessdb->isUserAvail($business_id);
        if(!empty($checkavailable) && $business_type != 'B') {
            $this->_redirect('user/userprofile');
        }
        if(!empty($checkavailable) && $business_type == 'B') {
            $this->_redirect('business/businessprofile');
        }
        $result = $purchasecouponsObj->getmycustomersinfo($business_id);
        $businessdata = $businessdb->getBusinessUserDetails($business_id);
        $this->view->assign('page_Name',"mycustomers");
        $this->view->assign("result",$result);
        $this->view->assign("businessdata",$businessdata);
    }
    
    public function myuppersAction(){
        $loggedUserId = '';
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $business_id = $logged_user_data->business_id;
            $business_type = 'B';
            $loggedUserId = $logged_user_data->business_id;
        }
        
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if($loggedUserId =='') {
            $this->_redirect('business/dashboard');
        }
        
        $businessdb = new Business_Model_Business();
        $couponsObj = new Business_Model_Coupons;
        if($this->getRequest()->getParam('userid') != '') {
            $business_id = $this->getRequest()->getParam('userid');
        }
        if($business_id == '') {
            $this->_redirect('business');
        }
        $checkavailable = $businessdb->isUserAvail($business_id);
        if(!empty($checkavailable) && $business_type != 'B') {
            $this->_redirect('user/userprofile');
        }
        if(!empty($checkavailable) && $business_type == 'B') {
            $this->_redirect('business/businessprofile');
        }
        $coupons = $couponsObj->getcouponsbybusinessid($business_id);//echo '<pre>';print_r($coupons);exit;
        $businessdata = $businessdb->getBusinessUserDetails($business_id);
        $this->view->assign('business_id', $business_id);
        $this->view->assign('page_Name',"myuppers");
        $this->view->assign("result",$coupons);
        $this->view->assign("businessdet",$businessdata);
    }
    
    public function mybusinessAction(){
        $loggedUserId = '';
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $business_id = $logged_user_data->business_id;
            $business_type = 'B';
            $loggedUserId = $logged_user_data->business_id;
        }
        
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
        }
        if($loggedUserId =='') {
            $this->_redirect('business/dashboard');
        }
        $businessdb = new Business_Model_Business();
        $followerdb = new User_Model_Followers;
        $couponsObj = new Business_Model_Coupons;
        if($this->getRequest()->getParam('userid') != '')
            $business_id = $this->getRequest()->getParam('userid');
        if($business_id == '') {
            $this->_redirect('business/');
        }
        $checkavailable = $businessdb->isUserAvail($business_id);
        if(!empty($checkavailable) && $business_type != 'B') {
            $this->_redirect('user/userprofile');
        }
        if(!empty($checkavailable) && $business_type == 'B') {
            $this->_redirect('business/businessprofile');
        }
        $coupons = $couponsObj->getcouponsbybusinessid($business_id);//echo '<pre>';print_r($coupons);exit;
        $businessdata = $businessdb->getBusinessUserDetails($business_id);
        $following = $followerdb->getFollowingbusiness($business_id,'B');
        //echo '<pre>';print_r($following);exit;
        $this->view->assign('page_Name',"mybusiness");
        $this->view->assign("result",$coupons);
        $this->view->assign("businessdata",$businessdata);
        $this->view->assign("following",$following);
    }
    
    public function dashboardAction() {
        if(Zend_Registry::isRegistered('businessdata')) {
            $business_user_details = Zend_Registry::get('businessdata');
        }
        if(Zend_Registry::isRegistered('userdata')) {
            $user_details = Zend_Registry::get('userdata');
        }
        //echo "<pre>";print_r($business_user_details);exit;
        if(empty($business_user_details) && !empty($user_details)) {
            $this->_redirect('user/userprofile');
        } elseif(empty($business_user_details)) {
            return $this->_helper->redirector('index');
        }

        $totalWeeks = 14;
        $business_id = $business_user_details->business_id;
        $businessObj = new Business_Model_Business();
        $businessdata = $businessObj->getBusinessUserDetails($business_id);
        
        /********** TO GET DASHBOARD STATISTICS**************/
        // Followers Statistics
        $followersStatistics  = $businessObj->getMyFollowerStatistics($business_id, $totalWeeks);
        $followerData = $followersStatistics;
        $followersStatistics['myFollowersCnt'] = number_format($followersStatistics['1'],'0', '.', ',');
        $followerData='["Weeks","count"],';
        for($i=1;$i<=$totalWeeks;$i++) {
            if($i < $totalWeeks) {
                $followerData .= "['".$i."',".$followersStatistics[$i]."],";
            }
            if($i == $totalWeeks) {
                $followerData .= "['".$i."',".$followersStatistics[$i]."]";
            }
        }

        switch (true) {
                case ($followersStatistics['1'] ==0 && $followersStatistics['2'] ==0) : {
                    $followersStatistics['myFollowersPercentage'] = 0;
                    $followersStatistics['myFollowersIconType'] = "Up";
                    break;
                }

                case ($followersStatistics['1'] ==0 && $followersStatistics['2'] > 0) : {
                    $followersStatistics['myFollowersPercentage'] = $followersStatistics['2'];
                    $followersStatistics['myFollowersIconType'] = "Down";
                    break;
                }

                case ($followersStatistics['1'] > 0 && $followersStatistics['2'] ==0) : {
                    $followersStatistics['myFollowersPercentage'] = $followersStatistics['1'];
                    $followersStatistics['myFollowersIconType'] = "Up";
                    break;
                }

                case ($followersStatistics['1'] > 0 && $followersStatistics['2'] > 0) : {
                    $followersStatistics['myFollowersPercentage'] = abs((($followersStatistics['1']-$followersStatistics['2'])/$followersStatistics['1'] *100));
                    if($followersStatistics['1'] >= $followersStatistics['2']) {
                        $followersStatistics['myFollowersIconType'] = "Up";    
                    }
                    if($followersStatistics['1'] < $followersStatistics['2']) {
                        $followersStatistics['myFollowersIconType'] = "Down";    
                    }
                    break;
                }

                default :
                    break;
        }

        // Total Sales Statistics
        $totalSalesStatistics  = $businessObj->getTotalSalesStatistics($business_id, $totalWeeks);
        $totalSalesData = $totalSalesStatistics;
        $totalSalesStatistics['totalSalesCnt'] = number_format($totalSalesStatistics['1'],'0', '.', ',');
        //echo '<pre>';print_r($totalSalesStatistics);exit;
        switch (true) {
            case ($totalSalesStatistics['1'] == 0 && $totalSalesStatistics['2'] == 0) : {
                $totalSalesStatistics['topSalesPercentage'] = 0;
                $totalSalesStatistics['topSalesIconType'] = "Up";
                break;
            }

            case ($totalSalesStatistics['1'] == 0 && $totalSalesStatistics['2'] > 0) : {
                $totalSalesStatistics['topSalesPercentage'] = $totalSalesStatistics['2'];
                $totalSalesStatistics['topSalesIconType'] = "Down";
                break;
            }

            case ($totalSalesStatistics['1'] > 0 && $totalSalesStatistics['2'] == 0) : {
                $totalSalesStatistics['topSalesPercentage'] = $totalSalesStatistics['1'];
                $totalSalesStatistics['topSalesIconType'] = "Up";
                break;
            }

            case ($totalSalesStatistics['1'] > 0 && $totalSalesStatistics['2'] > 0) : {
                $totalSalesStatistics['topSalesPercentage'] = abs((($totalSalesStatistics['1']-$totalSalesStatistics['2'])/$totalSalesStatistics['1'] *100));
                if($totalSalesStatistics['1'] >= $totalSalesStatistics['2']) {
                    $totalSalesStatistics['topSalesIconType'] = "Up";    
                }
                if($totalSalesStatistics['1'] < $totalSalesStatistics['2']) {
                    $totalSalesStatistics['topSalesIconType'] = "Down";    
                }
                break;
            }

            default :
                break;
        }
        
        // Total Sales Statistics
        $totalSalesData='["Weeks","count"],';
        for($i=1;$i<=$totalWeeks;$i++) {
            if($i < $totalWeeks) {
                $totalSalesData .= "['".$i."',".$totalSalesStatistics[$i]."],";
            }
            if($i == $totalWeeks) {
                $totalSalesData .= "['".$i."',".$totalSalesStatistics[$i]."]";
            }
        }

        // To get Coupon Clicks Details
        $couponClickStatistics  = $businessObj->getCouponClickStatistics($business_id, $totalWeeks);
        $couponClickData = $couponClickStatistics;
        $couponClickStatistics['couponClickCnt'] = number_format($couponClickStatistics['1'],'0', '.', ',');   

        switch (true) {
            case ($couponClickStatistics['1'] ==0 && $couponClickStatistics['2'] ==0) : {
                $couponClickStatistics['couponClickPercentage'] = 0;
                $couponClickStatistics['couponClickIconType'] = "Up";
                break;
            }

            case ($couponClickStatistics['1'] ==0 && $couponClickStatistics['2'] > 0) : {
                $couponClickStatistics['couponClickPercentage'] = $couponClickStatistics['2'];
                $couponClickStatistics['couponClickIconType'] = "Down";
                break;
            }

            case ($couponClickStatistics['1'] > 0 && $couponClickStatistics['2'] ==0) : {
                $couponClickStatistics['couponClickPercentage'] = $couponClickStatistics['1'];
                $couponClickStatistics['couponClickIconType'] = "Up";
                break;
            }

            case ($couponClickStatistics['1'] > 0 && $couponClickStatistics['2'] > 0) : {
                $couponClickStatistics['couponClickPercentage'] = abs((($couponClickStatistics['1']-$couponClickStatistics['2'])/$couponClickStatistics['1'] *100));
                if($couponClickStatistics['1'] >= $couponClickStatistics['2']) {
                    $couponClickStatistics['couponClickIconType'] = "Up";    
                }
                if($couponClickStatistics['1'] < $couponClickStatistics['2']) {
                    $couponClickStatistics['couponClickIconType'] = "Down";    
                }
                break;
            }

            default :
                break;
        }
        
        // Coupon Click Statistics
        $couponClickData='["Weeks","count"],';
        for($i=1;$i<=$totalWeeks;$i++) {
            if($i < $totalWeeks) {
                $couponClickData .= "['".$i."',".$couponClickStatistics[$i]."],";
            }
            if($i == $totalWeeks) {
                $couponClickData .= "['".$i."',".$couponClickStatistics[$i]."]";
            }
        }
        
        // To get total users in that area Details
        $usersInAreaStatistics  = $businessObj->getTotalUsersInAreaStatistics($businessdata->zipcode, $totalWeeks);
        $usersInAreaData = $usersInAreaStatistics;
        $usersInAreaStatistics['totalAreaUsersCnt'] = number_format($usersInAreaStatistics['1'],'0', '.', ',');
        
        switch (true) {
            case ($usersInAreaStatistics['1'] ==0 && $usersInAreaStatistics['2'] ==0) : {
                $usersInAreaStatistics['usersAreaPercentage'] = 0;
                $usersInAreaStatistics['usersAreaIconType'] = "Up";
                break;
            }

            case ($usersInAreaStatistics['1'] ==0 && $usersInAreaStatistics['2'] > 0) : {
                $usersInAreaStatistics['usersAreaPercentage'] = $usersInAreaStatistics['2'];
                $usersInAreaStatistics['usersAreaIconType'] = "Down";
                break;
            }

            case ($usersInAreaStatistics['1'] > 0 && $usersInAreaStatistics['2'] ==0) : {
                $usersInAreaStatistics['usersAreaPercentage'] = $usersInAreaStatistics['1'];
                $usersInAreaStatistics['usersAreaIconType'] = "Up";
                break;
            }

            case ($usersInAreaStatistics['1'] > 0 && $usersInAreaStatistics['2'] > 0) : {
                $usersInAreaStatistics['usersAreaPercentage'] = abs((($usersInAreaStatistics['1']-$usersInAreaStatistics['2'])/$usersInAreaStatistics['1'] *100));
                if($usersInAreaStatistics['1'] >= $usersInAreaStatistics['2']) {
                    $usersInAreaStatistics['usersAreaIconType'] = "Up";    
                }
                if($usersInAreaStatistics['1'] < $usersInAreaStatistics['2']) {
                    $usersInAreaStatistics['usersAreaIconType'] = "Down";    
                }
                break;
            }

            default :
                break;
        }
        
        // Coupon Click Statistics
        $usersInAreaData='["Weeks","count"],';
        for($i=1;$i<=$totalWeeks;$i++) {
            if($i < $totalWeeks) {
                $usersInAreaData .= "['".$i."',".$usersInAreaStatistics[$i]."],";
            }
            if($i == $totalWeeks) {
                $usersInAreaData .= "['".$i."',".$usersInAreaStatistics[$i]."]";
            }
        }
            
        //$this->view->assign('qrCode',$qrCode);
        $this->view->assign('followerData',$followerData);
        $this->view->assign('totalSalesData',$totalSalesData);
        $this->view->assign('couponClickData',$couponClickData);
        $this->view->assign('usersInAreaData',$usersInAreaData);
        $this->view->followersStatistics = $followersStatistics;
        $this->view->totalSalesStatistics = $totalSalesStatistics;
        $this->view->couponClickStatistics = $couponClickStatistics;
        $this->view->usersInAreaStatistics = $usersInAreaStatistics;
        $this->view->businessdata = $businessdata;
        
    }
    
    public function viewfollowersAction() {
        if(Zend_Registry::isRegistered('userdata')){
            $userdata = Zend_Registry::get('userdata');
            $loggedUserId = $userdata->user_id;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')){
            $businessdata = Zend_Registry::get('businessdata');
            $loggedUserId = $businessdata->business_id;
            $loggedUserType = 'B';
        }
            
        $request = $this->getRequest();
        //echo '<pre>';print_r($request->getParams());exit;
        $userid = $request->getParam('business_id');
        if($userid == '')
            $userid = $loggedUserId;
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):5);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);
        $followObj = new Business_Model_Followers();
        if($loggedUserId == $userid) { // if logged user seeing his followers
            $userfollowers = $followObj->getMyFollowers($userid, $loggedUserId,  "B", $limit, $pagenum);
            $userfollowing = $followObj->getMyFollowing($userid, "B", $limit, $pagenum);
            $followingCnt = count($userfollowing);
            if(count($userfollowers) > 0){
                foreach($userfollowers as $key => $followingUsr) {

                    if($followingCnt > 0) {
                        foreach($userfollowing as $key1 => $Usrfollows) {   
                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                   $userfollowers[$key]['isFollowing'] = "1";
                                   break;
                                }  else {
                                   $userfollowers[$key]['isFollowing'] = "0";
                                }
                        }
                    } else {
                        $userfollowers[$key]['isFollowing'] = "0";
                    }
                }
            }
        } else {
            // if Logged user is viewing other user's followers, CHecking following permissions for logged user with other user followers
            $userfollowers = $followObj->getMyFollowers($userid, $loggedUserId,  "B", $limit, $pagenum);
            $loggedUserFollowing = $followObj->getMyFollowing($loggedUserId, $loggedUserType,0, 0);
            $followingCnt = count($loggedUserFollowing);
            if(count($userfollowers) > 0){
            foreach($userfollowers as $key => $followingUsr) {
                    if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                            if($followingUsr['user_id'] == $loggedUserId && $followingUsr['user_type'] == $loggedUserType) {
                                $userfollowers[$key]['isFollowing'] = "2";
                            } else {
                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                   $userfollowers[$key]['isFollowing'] = "1";
                                   break;
                                }  else {
                                   $userfollowers[$key]['isFollowing'] = "0"; 
                                }
                            }
                        }
                    } else {
                        $userfollowers[$key]['isFollowing'] = "0";
                    }
                }
            }
        }
        $this->view->limit = $limit;
        $this->view->pagenum = $pagenum;
        $this->_helper->layout->disableLayout();
        $this->view->assign('userfollowers', $userfollowers);
    }

    public function viewfollowingAction() {
        if(Zend_Registry::isRegistered('userdata')){
            $userdata = Zend_Registry::get('userdata');
            $loggedUserId = $userdata->user_id;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')){
            $businessdata = Zend_Registry::get('businessdata');
            $loggedUserId = $businessdata->business_id;
            $loggedUserType = 'B';
        }
            
        $request = $this->getRequest();//echo '<pre>';print_r($request->getParams());exit;
        $userid = $request->getParam('business_id');
        if($userid == '')
            $userid = $loggedUserId;
        
        $limit = (($request->getParam('limit') != '')?$request->getParam('limit'):5);
        $pagenum = (($request->getParam('pagenum') != '')?$request->getParam('pagenum'):0);
        
        $followObj = new Business_Model_Followers();
        if($loggedUserId == $userid){ // if logged user seeing whom he follow
            $userfollowing = $followObj->getMyFollowing($userid, 'B', $limit, $pagenum); 
            if(count($userfollowing) > 0){
                foreach($userfollowing as $key3=>$usrfollow){ 
                    $userfollowing[$key3]['isFollowing'] = "1";
                }
            }
        } else {
            $userfollowing = $followObj->getMyFollowing($userid, 'B', $limit, $pagenum);
            $loggedUserFollowing = $followObj->getMyFollowing($loggedUserId,$loggedUserType, 0, 0);
            $loggedUserFollowingCnt = count($loggedUserFollowing);
            if(count($userfollowing) > 0){ 
                foreach($userfollowing as $key => $viewUsrFollowing) { 
                    if($loggedUserFollowingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $loggedUsrFollowing) {  
                            if($viewUsrFollowing['follower_id'] == $loggedUserId && $viewUsrFollowing['follower_type'] == $loggedUserType) {
                                $userfollowing[$key]['isFollowing'] = "2";
                            } else {
                                if(($viewUsrFollowing['follower_id'] == $loggedUsrFollowing['follower_id']) && ($viewUsrFollowing['follower_type'] == $loggedUsrFollowing['follower_type'])) {
                                   $userfollowing[$key]['isFollowing'] = "1";
                                   break;
                                }else {
                                   $userfollowing[$key]['isFollowing'] = "0"; 
                                }
                            }
                        }
                    } else {   //if logged  user is not following anyone 
                        foreach($userfollowing as $key => $viewUsrFollowing) {
                            if($viewUsrFollowing['follower_id'] == $loggedUserId && $viewUsrFollowing['follower_type'] == $loggedUserType) {
                                $userfollowing[$key]['isFollowing'] = "2";
                            } else {
                                $userfollowing[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                }
            } 
        }   
        $this->view->limit = $limit;
        $this->view->pagenum = $pagenum;
        $this->_helper->layout->disableLayout();
        $this->view->assign('userfollowing', $userfollowing);
    }

    public function myfollowersAction() {
        if(Zend_Registry::isRegistered('userdata')){
            $userdata = Zend_Registry::get('userdata');
            $loggedUserId = $userdata->user_id;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')){
            $businessdata = Zend_Registry::get('businessdata');
            $loggedUserId = $businessdata->business_id;
            $loggedUserType = 'B';
        }
        if($loggedUserId == '') {
            $this->_redirect('businessuser');
        }
        $businessdb = new Business_Model_Business();
        $request = $this->getRequest();
        //echo '<pre>';print_r($request->getParams());exit;
        $userid = $request->getParam('userid');
        $checkavailable = $businessdb->isUserAvail($userid);
        if(!empty($checkavailable) && $loggedUserType == 'U') {
            $this->_redirect('user/userprofile');
        }
        if(!empty($checkavailable) && $loggedUserType == 'B') {
            $this->_redirect('business/businessprofile');
        }
        $this->view->assign('userid', $userid);
        if($userid == '')
            $userid = $loggedUserId;
        
        $limit = '';
        if($request->getParam('limit') ==''){
            $limit = $limit;
        }
        $followObj = new Business_Model_Followers();
        if($loggedUserId == $userid) { // if logged user seeing his followers
            $userfollowers = $followObj->getFollowers($userid, $loggedUserId, $loggedUserType, $limit);
            //echo '<pre>';print_r($userfollowers);exit;
            $userfollowing = $followObj->getFollowing($userid, $loggedUserType, $limit);
            $followingCnt = count($userfollowing);
            if(count($userfollowers) > 0) {
                foreach($userfollowers as $key => $followingUsr) {
                    if($followingCnt > 0) {
                        foreach($userfollowing as $key1 => $Usrfollows) {
                            if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                $userfollowers[$key]['isFollowing'] = "1";
                                break;
                            }  else {
                                $userfollowers[$key]['isFollowing'] = "0";
                            }
                        }
                    } else {
                        $userfollowers[$key]['isFollowing'] = "0";
                    }
                }
            }
        } else { //echo $userid.'-->'.$loggedUserId.'-->'.$loggedUserType;exit;
            // if Logged user is viewing other user's followers, CHecking following permissions for logged user with other user followers
            $userfollowers = $followObj->getFollowers($userid, $loggedUserId, 'B', $limit);
            $loggedUserFollowing = $followObj->getFollowing($loggedUserId, $loggedUserType, $limit);
            $followingCnt = count($loggedUserFollowing);
            if(count($userfollowers) > 0){
            foreach($userfollowers as $key => $followingUsr) {
                    if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                            if($followingUsr['user_id'] == $loggedUserId && $followingUsr['user_type'] == $loggedUserType) {
                                $userfollowers[$key]['isFollowing'] = "2";
                            } else {
                                if(($followingUsr['user_id'] == $Usrfollows['follower_id']) && ($followingUsr['user_type'] == $Usrfollows['follower_type'])) {
                                    $userfollowers[$key]['isFollowing'] = "1";
                                    break;
                                }  else {
                                    $userfollowers[$key]['isFollowing'] = "0"; 
                                }
                            }
                        }
                    } else {
                        $userfollowers[$key]['isFollowing'] = "0";
                    }
                }
            }
        }
        //$this->_helper->layout->disableLayout();
        $this->view->assign('userfollowers', $userfollowers);
    }

    public function createalbumsforallbusinessAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $userObj = new Business_Model_Business();
        $data = $userObj->getAllUserDetails();
        //echo '<pre>';print_r($data);exit;
        foreach ($data as $key => $user) {
            $user_id = $user['business_id'];
            $user_image = $user['image_path'];
            $albumObj = new Mobile_Model_Albums();
            $albuminfo = $albumObj->albumexistsrnotwithisdefault($user_id, 'B');
            if(empty($albuminfo)) {
                $album['user_id'] = $user_id;
                $album['user_type'] = 'B';
                $album['album_name'] = 'Profile Photos';
                $album['is_default'] = '1';
                $album['created_date'] = date('Y-m-d H:i:s');
                $albumid = $albumObj->insert($album);
                $photoObj = new Mobile_Model_Photos();
                $photo['album_id'] = $albumid;
                $photo['photo_original_name'] = '';
                $photo['photo_modified_name'] = $user_image;
                $photo['user_id'] = $user_id;
                $photo['user_type'] = 'B';
                $photo['cover_photo'] = '1';
                $photo['created_date'] = date('Y-m-d H:i:s');
                $photo_id = $photoObj->insert($photo);
            }
        }
        echo 'successfully done';
    }

    public function makefriendsAction() {
        /***** To update User Status ****/
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        if(Zend_Registry::isRegistered('businessdata')){
            $businessdata = Zend_Registry::get('businessdata');
            $user_id = $businessdata->business_id;
            $user_type = 'B';
        }
        if(Zend_Registry::isRegistered('userdata')){
            $userdata = Zend_Registry::get('userdata');
            $user_id = $userdata->user_id;
            $user_type = 'U';
        }
        
        $updateAry = array();
        if($request->getParam('user_id')== '') {
            $updateAry['user_id'] = $user_id;
            $updateAry['user_type'] = $user_type;
        }  else {
            $updateAry['user_id'] = $request->getParam('user_id');
            $updateAry['user_type'] = $request->getParam('user_type');
        }
        if(isset($request)) {
            $updateAry['follower_id'] = $request->getParam('follower_id');
            $updateAry['follower_type'] = $request->getParam('follower_type');
            if($updateAry['user_type'] =='B' && $request->getParam('follower_type')=='U'){
                echo "error";
            } else {
                // CHech if alredy user id FOlowing
                $followObj = new Business_Model_Followers();
                $duplicationArray = $followObj->checkDuplicationFollow($updateAry['follower_id'], $updateAry['follower_type'], $updateAry['user_id'], $updateAry['user_type']);
                if(count($duplicationArray) == 0) {
                    $fid = $followObj->insertNewRecord($updateAry);
                    $resAry['reference_id'] = $fid;
                    $resAry['user_id'] = $request->getParam('follower_id');
                    $resAry['user_type'] = $request->getParam('follower_type');
                    $resAry['notifications_type'] = 'following';
                    $resAry['action_date'] = date('Y-m-d H:i:s');
                    $notificationObj = new Notifications_Model_Notifications();
                    $id = $notificationObj->insertNewRecord($resAry);
                    $resAry['reference_id'] = $fid;
                    $resAry['user_id'] = $updateAry['user_id'];
                    $resAry['user_type'] = $updateAry['user_type'];
                    $resAry['notifications_type'] = 'Followed';
                    $resAry['action_date'] = date('Y-m-d H:i:s');
                    $notificationObj = new Notifications_Model_Notifications();
                    $id = $notificationObj->insertNewRecord($resAry);
                    echo "success";
                }
            }
        } else {
            echo "error";
        }
        exit(0);
    }

    public function unmakefriendsAction() {
        /***** To update User Status ****/
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        if(Zend_Registry::isRegistered('businessdata')){
            $businessdata = Zend_Registry::get('businessdata');
            $user_id = $businessdata->business_id;
            $user_type = 'B';
        }
        if(Zend_Registry::isRegistered('userdata')){
            $userdata = Zend_Registry::get('userdata');
            $user_id = $userdata->user_id;
            $user_type = 'U';
        }
        $updateAry = array();
        if($request->getParam('user_id') == '') {
            $updateAry['user_id'] = $user_id;
            $updateAry['user_type'] = $user_type;
        }  else {
            $updateAry['user_id'] = $request->getParam('user_id');
            $updateAry['user_type'] = $request->getParam('user_type');
        }
        if(isset($request)) {
            $updateAry['follower_id'] = $request->getParam('follower_id');
            $updateAry['follower_type'] = $request->getParam('follower_type');
            if($updateAry['user_type'] =='B' && $request->getParam('follower_type')=='U'){
                echo "error";
            }else{
                $followObj = new Business_Model_Followers();
                $id = $followObj->deleteFriend($updateAry);
                echo "success";
            }
        } else {
            echo "error";
        }
        exit(0);
    }
    
    public function followbusinessAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();
        $request = $this->getRequest();
        $qrCode = $request->getParam('qrcode');
        if($qrCode == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'QRCode can not be Empty!!!'));
            return;
        }
        $logged_user_id = $request->getParam('logged_user_id');
        if($logged_user_id == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Id can not be Empty!!!'));
            return;
        }
        if(!is_numeric($logged_user_id)) {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'Invalid User Id!!!'));
            return;
        }
        $logged_user_type = $request->getParam('logged_user_type');
        if($logged_user_type == '') {
            echo $myjson->customEncode(array('service_status'=>'error',"error_msg" => 'User Type can not be Empty!!!'));
            return;
        }
        // Get USer Details By QrCode
        $businessObj = new Business_Model_Business();        
        $usrArray = $businessObj->checkQrAvailable($qrCode);
        if(count($usrArray) != 0) {
            // CHeck if already Following
            $usrObj = new User_Model_Followers();
            $isfollowing = $usrObj->isLoggedUserFollowingOtherUser($usrArray[0]['business_id'],"B", $logged_user_id, $logged_user_type);
            if(count($isfollowing) == 0) {
                $updateAry = array();
                $updateAry['user_id'] = $logged_user_id;
                $updateAry['user_type'] = $logged_user_type;
                $updateAry['follower_id'] = $usrArray[0]['business_id'];
                $updateAry['follower_type'] = "B";
                $followObj = new User_Model_Followers();
                $fid = $followObj->insertNewRecord($updateAry);
                $resAry['reference_id'] = $fid;
                $resAry['user_id'] = $logged_user_id;
                $resAry['user_type'] = $logged_user_type;
                $resAry['notifications_type'] = 'Followed';
                $resAry['action_date'] = date('Y-m-d H:i:s');
                $notificationObj = new Notifications_Model_Notifications();
                $id = $notificationObj->insertNewRecord($resAry);
                $resAry['reference_id'] = $fid;
                $resAry['user_id'] = $usrArray[0]['business_id'];
                $resAry['user_type'] = "B";
                $resAry['notifications_type'] = 'following';
                $resAry['action_date'] = date('Y-m-d H:i:s');
                $id = $notificationObj->insertNewRecord($resAry);
                $resultAry = array('service_status'=>'success',"content" => 'Followed Successfully','responsecode'=>'1000');
            } else {
                $resultAry = array('service_status'=>'error',"error_message" => 'You are already following the Business','responsecode'=>'1000');
            }
        } else {
            $resultAry = array('service_status'=>'error',"error_message" => 'User Id Not Exist','responsecode'=>'1000');
        }
        echo $myjson->customEncode($resultAry);
    }

    public function getmorefollowersAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $offset = $this->getRequest()->getParam('offset');
        $userId = $this->getRequest()->getParam('user_id');
        $followerscnt = $this->getRequest()->getParam('followerscnt');
        $limit = 10;
        $followerObj = new Business_Model_Followers();
        $followers = $followerObj->getFollowers($userId,'','B',$limit,$offset);
        $jsonfollwers = array();
        $cnt= count($followers);
        $limitchk = $cnt-1;
        $newoffset = $cnt+$offset;
        $morelimit = $offset+$limit;
        foreach($followers as $key => $res):
            if(file_exists(UPLOAD_PATH.'images/thumbnails/'.$res['image_name']) && !empty($res['image_name']))
                $image_path = SITE_URL.'uploads/images/thumbnails/'.$res['image_name'];
            else
                $image_path = SITE_URL.'images/img15.png';
            if($res['user_type'] == 'U') {
                if($key == $limitchk && $morelimit <= $followerscnt) {
                    $jsonfollwers[$key] = '<a title="'.$res['firstname'].'" href="'.SITE_URL.'user/userprofile/'.$res['user_id'].'">
                                                <img class="B_img4" src="'.$image_path.'" width="72" height="72">
                                            </a><div class="clear">&nbsp;</div>
                                            <a style="float: right; cursor: pointer;" id="morediv" onclick="fetchmorefollowers('.$res['follower_id'].','.$newoffset.','.$followerscnt.');">More</a>';
                } else {
                    $jsonfollwers[$key] = '<a title="'.$res['firstname'].'" href="'.SITE_URL.'user/userprofile/'.$res['user_id'].'">
                                                <img class="B_img4" src="'.$image_path.'" width="72" height="72">
                                            </a>';
                }
            } elseif ($res['user_type'] == 'B') {
                if($key == $limitchk && $morelimit <= $followerscnt) {
                    $jsonfollwers[$key] = '<a title="'.$res['name'].'" href="'.SITE_URL.'business/businessprofile/'.$res['user_id'].'">
                                                <img class="B_img4" src="'.$image_path.'" width="72" height="72">
                                            </a><div class="clear">&nbsp;</div>
                                            <a style="float: right; cursor: pointer;" id="morediv" onclick="fetchmorefollowers('.$res['follower_id'].','.$newoffset.','.$followerscnt.');">More</a>';
                } else {
                    $jsonfollwers[$key] = '<a title="'.$res['name'].'" href="'.SITE_URL.'business/businessprofile/'.$res['user_id'].'">
                                                <img class="B_img4" src="'.$image_path.'" width="72" height="72">
                                            </a>';
                }
            }
        endforeach;
        echo json_encode($jsonfollwers);
    }

    public function detailedanalyticsAction() {
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $business_id = $logged_user_data->business_id;
            $business_type = 'B';
        }
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $business_id = $logged_user_data->user_id;
            $business_type = 'U';
        }
        if(empty($logged_user_data)) {
            return $this->_helper->redirector('index');
        }
        $request = $this->getRequest();
        $user_id = $request->getParam('userid');
        if($request->getParam($user_id) == '') {
            $user_id = $business_id;
        }
        $businessObj = new Business_Model_Business();

        // to get Following User Countries
        $countries = $businessObj->getUserFollowersByCountry($user_id);
        $this->view->countries = $countries;

        //to get Following User States
        $cities = $businessObj->getUserFollowersByCity($user_id);
        $this->view->cities = $cities;

        //to get Following User Languages
        $languages = $businessObj->getUserFollowersByLanguage($user_id);
        $this->view->languages = $languages;

        // TO get All Coupon names Owned by the Business
        $couponAry = $businessObj->getCouponDetailsOfUser($user_id);
        $this->view->couponAry = $couponAry;

        // FOR MY FOLLOWER GRAPH
        $myFollowerData = array();
        $dates = $request->dates;
        if($request->dates != '') {
            $datesAry = explode('÷', $dates);
            $lastMonth = $datesAry[0];
            $uptoToday = $datesAry[1];
        } else {
            $lastMonth = date("Y-m-d", strtotime("-1 month", time()));
            $uptoToday = date("Y-m-d", time());
        }

        // for Chart
        $myFollowerData = $businessObj->getMyFollowerAnalyticsData($user_id, $lastMonth, $uptoToday);
        $followerData = '';
        $followerData ='["Age","Male","Female"],';
        $followerData .= "['13-17',".$myFollowerData[1]['M'].",".$myFollowerData[1]['F']."],";
        $followerData .= "['18-24',".$myFollowerData[2]['M'].",".$myFollowerData[2]['F']."],";
        $followerData .= "['25-34',".$myFollowerData[3]['M'].",".$myFollowerData[3]['F']."],";
        $followerData .= "['35-44',".$myFollowerData[4]['M'].",".$myFollowerData[4]['F']."],";
        $followerData .= "['45-54',".$myFollowerData[5]['M'].",".$myFollowerData[5]['F']."],";
        $followerData .= "['55-64',".$myFollowerData[6]['M'].",".$myFollowerData[6]['F']."],";
        $followerData .= "['65+',".$myFollowerData[7]['M'].",".$myFollowerData[7]['F']."]";
        $this->view->followerData = $followerData;
        $this->view->lastMonth = $lastMonth;
        $this->view->uptoToday = $uptoToday;

        // FOR COUPON CLICKS GRAPH
        $couponClickAry = array();
        $dates2 = $request->dates2;
        $coupon_id = $request->coupon_id;
        if($request->dates2 != '') {
            $datesAry2 = explode('÷', $dates2);
            $lastMonth2 = $datesAry2[0];
            $uptoToday2 = $datesAry2[1];
        } else {
            $lastMonth2 = date("Y-m-d", strtotime("-1 month", time()));
            $uptoToday2 = date("Y-m-d", time());
        }
        if($coupon_id == '' && empty($couponAry)) {
            $coupon_id = 0;
        }
        if($coupon_id == '' && !empty($couponAry)) {
            $coupon_id = $couponAry['0']['coupon_id'];
        }
        $couponClickAry = $businessObj->getCouponClickAnalyticStatistics($user_id, $uptoToday2, $lastMonth2, $coupon_id);
        $couponClickData = '';

        // for Chart
        if($couponClickAry['type'] == 'd')$type = "Day"; 
        elseif($couponClickAry['type'] == 'm')$type = "Month";
        elseif($couponClickAry['type'] == 'w')$type = "Week";
        elseif($couponClickAry['type'] == 'Y')$type = "Year";
        $couponClickData .='["'.$type.'","count"],';

        // to show Daywise
        if($couponClickAry['type'] == 'd') {
            $day_start = date('d', strtotime($lastMonth2));
            $day_end = date('d', strtotime($uptoToday2));
            $d1 = strtotime($lastMonth2);
            $d2 = strtotime($uptoToday2);
            $day_diff = ($d2-$d1)/(60*60*24);
            if($_SERVER['REMOTE_ADDR'] =='182.72.66.214' || $_SERVER['REMOTE_ADDR'] =='192.168.1.57') {
               //echo $day_start."--".$day_end."==".$day_diff;exit;
            }
            $dateVar = $lastMonth2;
            for($i=1;$i<=($day_diff+1);$i++) {
                $d1 = date('d', strtotime($dateVar));
                $j = 0;
                foreach($couponClickAry as $coupon) {
                    if($coupon['date'] == $d1) {
                        $j = 1;
                        $couponClickData .= "['".$d1."',".$coupon['total']."],";
                    }
                }
                if($j == 0) {
                    $couponClickData .= "['".$d1."',0],";
                }
                $dateVar = date('Y-m-d', strtotime($dateVar . ' + 1 day'));
            }
        }

        // to show Daywise
        if($couponClickAry['type'] == 'w') {
            $week_diff = strtotime($uptoToday2, 0) - strtotime($lastMonth2, 0);
            $weekCnt = floor($week_diff / 604800);
            for($i=0;$i<$weekCnt;$i++) {
                if($i < ($weekCnt-1)) {
                    $couponClickData .= "['Week".($i+1)."',0],";
                } else {
                    $couponClickData .= "['Week".($i+1)."',".$couponClickAry[$i]['total']."],";
                }
            }
        }

        // to show Monthwise
        if($couponClickAry['type'] == 'm') {
            $month_start = date('m', strtotime($lastMonth2));
            $month_end = date('m', strtotime($uptoToday2));
            $month_diff = $month_end-$month_start;
            $monthVar = $lastMonth2;
            for($i=0;$i<=$month_diff;$i++) {
                $m1 = date('m', strtotime($monthVar));
                $m2 = date('M', strtotime($monthVar));
                $j = 0;
                foreach($couponClickAry as $coupon) {
                    if($coupon['date'] == $m1) {
                        $j = 1;
                        $couponClickData .= "['".$m2."',".$coupon['total']."],";
                    }
                }
                if($j == 0) {
                    $couponClickData .= "['".$m2."',0],";
                }
                $monthVar = date('Y-m-d', strtotime($monthVar . ' + 1 month'));
            }
        }

        // to show Yearwise
        if($couponClickAry['type'] == 'Y') {
            $year_start = date('Y', strtotime($lastMonth2));
            $year_end = date('Y', strtotime($uptoToday2));
            $year_diff = $year_end-$year_start;
            $yearVar = $lastMonth2;
            for($i=1;$i<=$year_diff;$i++) {
                $y1 = date('Y', strtotime($yearVar));
                $j = 0;
                foreach($couponClickAry as $coupon) {
                    if($coupon['date'] == $y1) {
                        $j = 1;
                        $couponClickData .= "['".$y1."',".$coupon['total']."],";
                    }
                }
                if($j == 0) {
                    $couponClickData .= "['".$y1."',0],";
                }
                $yearVar = date('Y-m-d', strtotime($yearVar . ' + 1 year'));
            }
        }
        $this->view->couponClickData = $couponClickData;
        $this->view->lastMonth2 = $lastMonth2;
        $this->view->uptoToday2 = $uptoToday2;
        $this->view->coupon_id = $coupon_id;

        // FOR Total Sales Graph
        $dates3 = $request->dates3;
        if($request->dates3 != '') {
            $datesAry3 = explode('÷', $dates3);
            $lastMonth3 = $datesAry3[0];
            $uptoToday3 = $datesAry3[1];
        } else {
            $lastMonth3 = date('Y-m-01');
            $uptoToday3 = date("Y-m-d", time());
        }
        $display_type = $request->getParam('display_type');
        $totalSalesAry = array();
        $cnt = count($couponAry);
        $i = 1;
        $couponIds = '';
        foreach($couponAry as $coupon1) {
            if($i < $cnt)
                $couponIds .= "'".$coupon1['coupon_id']."'".",";
            if($i==$cnt)
                $couponIds .= "'".$coupon1['coupon_id']."'";
            $i++;
        }
        if($couponIds == '')
            $couponIds = 0;
        $totalSalesAry = $businessObj->getTotalSalesAnalyticStatistics($user_id, $uptoToday3, $lastMonth3, $couponIds, $display_type);
        if($display_type == '')
            $display_type = $totalSalesAry['type'];
        $totalSalesData = '';

        // for Chart
        if($totalSalesAry['type'] == 'd')$type = "Day"; 
        elseif($totalSalesAry['type'] == 'm')$type = "Month";
        elseif($totalSalesAry['type'] == 'w')$type = "Week";
        elseif($totalSalesAry['type'] == 'Y')$type = "Year";
        $totalSalesData .='["'.$type.'","count"],';

        // to show Daywise
        if($display_type == 'd') {
            $day_start = date('d', strtotime($lastMonth3));
            $day_end = date('d', strtotime($uptoToday3));
            $d1 = strtotime($lastMonth3);
            $d2 = strtotime($uptoToday3);
            $day_diff = ($d2-$d1)/(60*60*24);
            $dateVar = $lastMonth3;
            for($i=1;$i<=($day_diff+1);$i++) {
                $d1 = date('d', strtotime($dateVar));
                $j = 0;
                foreach($totalSalesAry as $coupon) {
                    if($coupon['date'] == $d1) {
                        $j = 1;
                        $totalSalesData .= "['".$d1."',".$coupon['cnt']."],";
                    }
                }
                if($j == 0) {
                    $totalSalesData .= "['".$d1."',0],";
                }
                $dateVar = date('Y-m-d', strtotime($dateVar . ' + 1 day'));
            }
        }

        // to show WeekWise
        if($display_type == 'w') {
            $week_diff = strtotime($uptoToday3, 0) - strtotime($lastMonth3, 0);
            $weekCnt = floor($week_diff / 604800);
            if($weekCnt == 0) $weekCnt = 1;
            for($i=0;$i<$weekCnt;$i++) {
                if($i > $weekCnt) {
                    $totalSalesData .= "['Week".($i+1)."',0],";
                } else {
                    $totalSalesData .= "['Week".($i+1)."',".$totalSalesAry[$i]['total']."],";
                }
            }
        }

        // to show Monthwise
        if($display_type == 'm') {
            $month_start = date('m', strtotime($lastMonth3));
            $month_end = date('m', strtotime($uptoToday3));
            $month_diff = $month_end-$month_start;
            $monthVar = $lastMonth3;
            for($i=0;$i<=$month_diff;$i++) {
                $m1 = date('m', strtotime($monthVar));
                $m2 = date('M', strtotime($monthVar));
                $j = 0;
                foreach($totalSalesAry as $coupon) {
                    if($coupon['date'] == $m1) { 
                        $j = 1;
                        $totalSalesData .= "['".$m2."',".$coupon['cnt']."],";
                    }
                }
                if($j == 0) {
                    $totalSalesData .= "['".$m2."',0],";
                }
                $monthVar = date('Y-m-d', strtotime($monthVar . ' + 1 month'));
            }
        }

        // to show Yearwise
        if($display_type == 'Y') {
            $year_start = date('Y', strtotime($lastMonth3));
            $year_end = date('Y', strtotime($uptoToday3));
            $year_diff = $year_end-$year_start;
            $yearVar = $lastMonth3;
            if($year_diff == 0) $year_diff = 1;
            for($i=1;$i<=$year_diff;$i++){
                $y1 = date('Y', strtotime($yearVar));
                $j = 0;
                foreach($totalSalesAry as $coupon){
                    //echo "<pre>";print_r($coupon);//exit;
                    if($coupon['date'] == $y1) {
                        $j = 1;
                        $totalSalesData .= "['".$y1."',".$coupon['cnt']."],";
                    }
                }
                if($j == 0){
                    $totalSalesData .= "['".$y1."',0],";
                }
                $yearVar = date('Y-m-d', strtotime($yearVar . ' + 1 year'));
            }
        }
        $this->view->totalSalesData = $totalSalesData;
        $this->view->totalSalesAry = $totalSalesAry;
        $this->view->lastMonth3 = $lastMonth3;
        $this->view->uptoToday3 = $uptoToday3;
        $this->view->display_type = $display_type;
    }
    
    public function downloadfileAction() {
        // disable layout and view
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $filename = $this->getRequest()->getParam('filename');
        $path_file = UPLOAD_PATH.'qrcode/';
        $file = $path_file.$filename; 
        //zfdebug(mime_content_type($file)); die();
        if (file_exists($file)) {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false); // required for certain browsers 
        header('Content-Type: '.mime_content_type($file));
        header('Content-Disposition: attachment; filename="'. basename($file) . '";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        readfile($file);
        } else {
            echo "File does not exist";
        }
    }

    public function helpAction(){
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $business_id = $logged_user_data->business_id;
            $business_type = 'B';
        }
        $businessdb = new Business_Model_Business();
        //$couponsObj = new Business_Model_Coupons;
        if($this->getRequest()->getParam('userid') != '') {
            $business_id = $this->getRequest()->getParam('userid');
        }
        if($business_id == '') {
            $this->_redirect('business/');
        }
        $checkavailable = $businessdb->isUserAvail($business_id);
        if(!empty($checkavailable) && $business_type != 'B') {
            $this->_redirect('user/userprofile');
        }
        if(!empty($checkavailable) && $business_type == 'B') {
            $this->_redirect('business/businessprofile');
        }
        $businessdata = $businessdb->getBusinessUserDetails($business_id);
        $this->view->assign('page_Name',"help");
        $this->view->assign('business_id', $business_id);
        $this->view->assign("businessdet",$businessdata);
    }

    public function techsupportAction(){
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $business_id = $logged_user_data->business_id;
            $business_type = 'B';
        }
        $businessdb = new Business_Model_Business();
        //$couponsObj = new Business_Model_Coupons;
        if($this->getRequest()->getParam('userid') != '') {
            $business_id = $this->getRequest()->getParam('userid');
        }
        if($business_id == '') {
            $this->_redirect('business/');
        }
        $checkavailable = $businessdb->isUserAvail($business_id);
        if(!empty($checkavailable) && $business_type != 'B') {
            $this->_redirect('user/userprofile');
        }
        if(!empty($checkavailable) && $business_type == 'B') {
            $this->_redirect('business/businessprofile');
        }
        $businessdata = $businessdb->getBusinessUserDetails($business_id);
        $this->view->assign('page_Name',"techsupport");
        $this->view->assign('business_id', $business_id);
        $this->view->assign("businessdet",$businessdata);
    }

    public function businessmediaAction() {
        $this->_helper->layout->disableLayout();
        $photoid = $this->getRequest()->getParam('photo_id');
        $type = $this->getRequest()->getParam('type');
        $photoObj = new Mobile_Model_Photos();
        $videoObj = new Videos_Model_Videos();
        if($type == 'photo') {
            $data = $photoObj->getphotobyphotoid($photoid);
        }
        if($type == 'video') {
            $data = $videoObj->getvideosbyvideoid($photoid);
        }
        $this->view->assign('type',$type);
        $this->view->assign('media',$data);
    }

    public function businesscouponAction() {
        $this->_helper->layout->disableLayout();
        if(isset($_REQUEST['Type']) && $_REQUEST['Type'] == 'PreviewCoupon') {
            $this->view->assign("type","preview");
            $coupondet = $_REQUEST;
            $category = $_REQUEST['category'];
        } else {
            $coupon_id = $this->getRequest()->getParam('couponid');
            $couponObj = new Business_Model_Coupons();
            $coupondet = $couponObj->getcouponbyid($coupon_id);
            $category_id = $coupondet['category_id'];
            $businesscategoiresdb = new Business_Model_Businesscategories();
            $category = $businesscategoiresdb->getcategoryname($category_id);
            $category = $category['category_name'];
        }
        $this->view->assign("category",$category);
        $this->view->assign('coupon', $coupondet);
    }

    public function businesshuddlesAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
        $request = $this->getRequest();
        $businessdb = new Business_Model_Business();
        $businesscatObj = new Business_Model_Businesscategories();
        $huddelObj = new Huddles_Model_Huddles();
        if (!$auth->hasIdentity()) {
            $auth1 = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
            $businessuserdata = $businessdb->getBusinessUserDetails($request->getParam('business_id'));
            if(!$auth1->hasIdentity() || !$auth->hasIdentity()) {
                $this->_helper->redirector('index');
            }
        }
        $roominfo = array();
        if($request->getParam('business_id') == '') {
            $user_id = $auth->getIdentity()->business_id;
            $user_type = $auth->getIdentity()->user_type;
            $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
            $targetroomids = $businesscatObj->businesstargetexistsrnot($user_id);
            //echo '<pre>';print_r($targetroomids);exit;
            foreach($targetroomids as $i=>$roomid):
                    $roominfo[] = $huddelObj->getRoomInfo($roomid);
                    $roominfo[$i]['chatcnt'] = $huddelObj->getchatcnt($roomid);
                    $members[] = explode(',',$roominfo[$i]['members_id']);
                    $owners[] = explode(',',$roominfo[$i]['owner_id']);
                    $roominfo[$i]['huddlerscnt'] = COUNT($members[$i])+COUNT($owners[$i]);
            endforeach;
            //Assigning Values to View Page
            $this->view->assign('page_Name',"businesshuddles");
            if(!empty($roominfo)) {
                $this->view->assign('roominfo',$roominfo);
            }
            $this->view->assign('businessuserdata',$businessuserdata);
        } else {
            $businessdb = new Business_Model_Business();
            $user_id = $request->getParam('business_id');
            $checkavailable = $businessdb->isUserAvail($user_id);
            if(!empty($checkavailable)) {
                $this->_redirect('user/userprofile');
            }
            $user_type = "B";
            $businessuserdata = $businessdb->getBusinessUserDetails($user_id);
            $targetroomids = $businesscatObj->businesstargetexistsrnot($user_id);
            foreach($targetroomids as $i=>$roomid):
                    $roominfo[] = $huddelObj->getRoomInfo($roomid);
                    $roominfo[$i]['chatcnt'] = $huddelObj->getchatcnt($roomid);
                    $members[] = explode(',',$roominfo[$i]['members_id']);
                    $owners[] = explode(',',$roominfo[$i]['owner_id']);
                    $roominfo[$i]['huddlerscnt'] = COUNT($members[$i])+COUNT($owners[$i]);
            endforeach;
            //Assigning Values to View Page
            $this->view->assign('page_Name',"businesshuddles");
            if(!empty($roominfo)) {
                $this->view->assign('roominfo',$roominfo);
            }
            $this->view->assign('businessuserdata',$businessuserdata);
        }
        if($businessuserdata->qrcode == '') {
            // FOR Storing Unique QR CODE in DB
            $length = 5;
            $char_key = '';
            $num_key = '';
            $characters = array("B","C","D","F","G","H","J","K","L","M","N",
            "P","Q","R","S","T","V","W","X","Y","Z","b","c","d","f","g","h",
            "j","k","l","m","n","p","q","r","s","t","v","w","x","y","z");
            for ($i = 0; $i < $length-2; $i++) {
                $char_key .= $characters[array_rand($characters)];
            }
            $charArr = str_split($char_key);
            $numbers = array("1","2","3","4","5","6","7","8","9");
            for ($i = 0; $i < $length-3; $i++) {
                $num_key .= $numbers[array_rand($numbers)];
            }
            $numArr = str_split($num_key);
            $keys = array_merge($numArr, $charArr);
            $keyString = str_shuffle(implode('', $keys));
            $qrCode =  $keyString.$user_id;
            // to check if QRCode String is Updated in DB or not
            $qrArr = $businessdb->checkQrAvailable($qrCode);
            if(count($qrArr) == 0) {
                $upd = $businessdb->updateQRCode($user_id, $qrCode);
            }
        } else {
            $qrCode = $businessuserdata->qrcode;
        }
        // FOR GENERATING QR CODE IF NOT GENERATED AND STORING IN FOLDER
        $QRCodeHelper = new UpmeSocial_View_Helper_QRCode();
        $QRC = $QRCodeHelper->googleQRCode($businessuserdata->username, SITE_URL."business/followbusiness/".$qrCode);
        $this->view->assign('qrCode',$qrCode);
        $this->view->assign('QRC',$QRC);
    }

    public function businesschatlogAction(){
        $this->_helper->layout->disableLayout();
        //$this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $roomId = trim($request->getParam('rid'));
        $huddleObj = new Huddles_Model_Huddles();
        $info = $huddleObj->getRoomChatLog($roomId);
        $members = explode(',',$info['RoomInfo']['members_id']);
        $owners = explode(',',$info['RoomInfo']['owner_id']);
        $info['huddlerscnt'] = COUNT($members)+COUNT($owners);
        $this->view->assign('roominfo',$info);
    }

    public function transactionreportsAction(){
        $request = $this->getRequest();
        //echo "<pre>";print_r($request->getParams());exit;
        
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $business_id = $logged_user_data->business_id;
            $business_type = 'B';
        }
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $business_id = $logged_user_data->user_id;
            $business_type = 'U';
        }
        if(empty($logged_user_data)) {
            return $this->_helper->redirector('index');
        }
        $user_id = $request->getParam('userid');
        if($request->getParam($user_id) == ''){
            $user_id = $business_id;
        }
        
        $businessObj = new Business_Model_Business();
        
        // TO get All Coupon names Owned by the Business
        $couponAry = $businessObj->getCouponDetailsOfUser($user_id);
        $this->view->couponAry = $couponAry;
        //echo "<pre>";print_r($request->getParams());exit;    
        
        $limit = "4";
        $pagenum = "0";
        if($request->pagenum != '')
            $pagenum = $request->pagenum;
        
        $dates = $request->dates;
        if($request->dates != ''){
            $datesAry = explode('÷', $dates);
            $startDate = $datesAry[0];
            $endDate = $datesAry[1];
        } else {
            $startDate = date("Y-m-01");
            if($request->startDate != '')
                $startDate = $request->startDate;
            $endDate = date("Y-m-d", time());
            if($request->$endDate != '')
                $endDate = $request->$endDate;
        }
        
        // to Get Upper Details
        $coupon_id = $request->getParam('coupon_id');
        if($coupon_id == '') {
            $cnt = count($couponAry);
            $i = 1;
            $couponIds = '';
            foreach($couponAry as $coupon1){
                if($i < $cnt)
                    $couponIds .= "'".$coupon1['coupon_id']."'".",";
                if($i==$cnt)
                    $couponIds .= "'".$coupon1['coupon_id']."'";
                $i++;
            }
            $coupon_id = $couponIds;
        }
        $transactionReports = array();
        $transactionReportsCnt = $businessObj->getTotalTransactionReports($user_id, $startDate, $endDate, $coupon_id);
        if($transactionReportsCnt != 0) {
            $transactionReports = $businessObj->getTransactionReports($user_id, $startDate, $endDate, $coupon_id, $pagenum, $limit);
        }
        // TO get Total Actual Cost
        $totalActualCostAndDiscount = $businessObj->getTotalActualCostAndDiscount($user_id, $startDate, $endDate, $coupon_id);
        $this->view->transactionReports = $transactionReports;
        $this->view->totalActualCostAndDiscount = $totalActualCostAndDiscount;
        $this->view->startDate = $startDate;
        $this->view->endDate = $endDate;
        $this->view->coupon_id = $coupon_id;
        $this->view->reportsCnt = $transactionReportsCnt;
        $this->view->pagenum = $pagenum;
        $this->view->limit = $limit;
    }

    public function cancelaccountAction() {
        $this->view->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $businessId = $this->getRequest()->getParam('businessId');
        if(!empty($businessId)) {
            $businessObj = new Business_Model_Business();
            $businessObj->cancelaccount($businessId);
            echo 'success';exit;
        }
    }

    public function deactiveAction() {
        if(Zend_Registry::isRegistered('businessdata')) {
            $businessdata = Zend_Registry::get('businessdata');
        }
        $this->view->assign('businessdata', $businessdata);
    }

    public function businessactiveAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        if(!empty($_REQUEST['business_id']) && !empty($_REQUEST['status'])) {
            /***** To update Business Status ****/
            if($_REQUEST['status'] == 'A') {
                $updateAry['status'] = $_REQUEST['status'];
                $bid = $_REQUEST['business_id'];
                $businessObj = new Business_Model_Business();
                $businessObj->updatebusiness($updateAry, $bid);
                /** Setting Updated Images data to sessions **/
                $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('businessuser'));
                $user = $auth->getIdentity();
                $user->status = $updateAry['status'];
                $auth->getStorage()->write($user);
                echo "success";exit;
            } else {
                echo "error";exit;
            }
        }
    }
    
    public function forgotpasswordAction(){
        $this->_helper->layout->disableLayout();
    }
    
    public function getnewpasswordAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $email = $this->getRequest()->getParam('email');
        $mkey = $this->getRequest()->getParam('mkey');
        $businessObj = new Business_Model_Business();
        if(!preg_match('/^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$/', $email)) {
                $resultAry = array('service_status'=>'error',"content" => 'Invalid Email Format');
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry); 
                return false;
        }
        $available = $businessObj->isEmailAvail($email);
        if(!empty($available)) {
                $resultAry = array('service_status'=>'error',"content" => 'Email not available');
                $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry); 
                return false;
        }
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $data['password'] = substr(str_shuffle($chars),0,6);
        $uid = $businessObj->updatePasswordByEmail($data, $email);
        $userdata = $businessObj->getuserdatabyemail($email);
//                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57'){
//                    echo $data['password'];
//                }
        if($_SERVER['SERVER_NAME'] != '192.168.1.57') {
                $m = new UpmeSocial_HtmlMailer();
                $m->setSubject("UPMEsocial - Updateded Login Credentials");
                $m->addTo($email)
                    ->setViewParam('user_id',$userdata['business_id'])
                    ->setViewParam('firstname',$userdata['business_name'])
                    ->setViewParam('lastname','')
                    ->setViewParam('username',$userdata['username'])
                    ->setViewParam('password',$data['password'])
                    ->setViewParam('email',$email);
                $m->sendHtmlTemplate("forgotpassworddetails.phtml");
        }
        if($uid) {
                $resultAry = array('service_status'=>'success',"content" => "Your password has been send to Registered Email");
        } else {
                $resultAry = array('service_status'=>'error',"error_msg" => '');
        }
        $myjson = new UpmeSocial_Controller_Action_Helper_MyJsonEncode();            echo $myjson->customEncode($resultAry);
                
    }

}