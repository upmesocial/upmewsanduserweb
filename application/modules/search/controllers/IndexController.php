<?php
class Search_IndexController extends Zend_Controller_Action
{

    public function init() {
        /* Initialize action controller here */
        if(Zend_Registry::isRegistered('businessdata')) {
            $userdata = Zend_Registry::get('businessdata');
            $user_type = $userdata->user_type;
        }
        // if logged user is People Type
        if(Zend_Registry::isRegistered('userdata')) {
            $userdata = Zend_Registry::get('userdata');
            $user_type = $userdata->user_type;
        }
        if($user_type == '') {
            $category = $this->getRequest()->getParam(category);
            if($category == 'Business')
                $this->_helper->redirector('index','index','business');
            if($category == 'User')
                $this->_helper->redirector('index','index','user');
        }
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $user_account_status = $logged_user_data->user_account_status;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $user_account_status = $logged_user_data->status;
            $loggedUserType = 'B';
        }
        if(!empty($logged_user_data)) {
            $limit= 30;
            $photosObj = new Mobile_Model_Photos();
            $followersgallery = $photosObj->getTimelineClickedImagesVideosDetails($loggedUserId,$loggedUserType, $limit);
            //echo "<pre>";print_r($followersgallery);exit;
            //echo count($followersgallery);exit;
            $this->view->assign('followersgallery', $followersgallery);
        }
    }

    public function indexAction() {
        // action body
        $this->_helper->redirector('top_popular_people','search','user');
    }

    public function toppopularpeopleAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $logged_user_Id = $auth->getIdentity()->user_id;
        $logged_user_type = $auth->getIdentity()->user_type;
        if($logged_user_type != 'B') {
            $logged_user_type = 'U';
        }
        $limit = 300;
        $keyword = '';
        // Get Top Popular People based on Cool Points
        $searchObj = new Search_Model_Search();
        $topPopularPeopleArray =  $searchObj->getTopPopularPeople($limit,$keyword);
        // Get Logged User Following Users-list
        $followObj = new User_Model_Followers();
        $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
        $followingCnt = count($loggedUserFollowing);
        if(count($topPopularPeopleArray) > 0) {
            foreach($topPopularPeopleArray as $key => $popularUsr) {
                $popularUsrType = $popularUsr['user_type'];
                if($popularUsr['user_type'] != 'B') {
                    $popularUsrType = 'U';
                }
                if($followingCnt > 0) {
                    foreach($loggedUserFollowing as $Usrfollows) {
                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                            $topPopularPeopleArray[$key]['isFollowing'] = "2";
                        } else {
                            if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                               $topPopularPeopleArray[$key]['isFollowing'] = "1";
                               break;
                            }  else {
                               $topPopularPeopleArray[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                } else {
                    $topPopularPeopleArray[$key]['isFollowing'] = "0";
                }
            }
        }
        $this->view->topPopularPeopleArray = $topPopularPeopleArray;
        $this->view->logged_user_Id = $logged_user_Id;
    }
    
    public function localpeopleAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $logged_user_Id = $auth->getIdentity()->user_id;
        $logged_user_type = $auth->getIdentity()->user_type;
        $logged_user_city = $auth->getIdentity()->city;
        $logged_user_state = $auth->getIdentity()->state;
        $logged_user_country = $auth->getIdentity()->country;
        if($logged_user_type != 'B') {
            $logged_user_type = 'U';
        }
        // Get Local People based on CITY, STATE & COUNTRY of Logged User
        $searchObj = new Search_Model_Search();
        $localPeopleArray =  $searchObj->getLocalPeople($logged_user_city, $logged_user_state, $logged_user_country);
        // Get Logged User Following Users-list
        $followObj = new User_Model_Followers();
        $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
        $followingCnt = count($loggedUserFollowing);
        if(count($localPeopleArray) > 0) {
            foreach($localPeopleArray as $key => $popularUsr) {
                $popularUsrType = $popularUsr['user_type'];
                if($popularUsr['user_type'] != 'B') {
                    $popularUsrType = 'U';
                }
                if($followingCnt > 0) {
                    foreach($loggedUserFollowing as $Usrfollows) {
                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                            $localPeopleArray[$key]['isFollowing'] = "2";
                        } else {
                            if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                $localPeopleArray[$key]['isFollowing'] = "1";
                                break;
                            }  else {
                                $localPeopleArray[$key]['isFollowing'] = "0"; 
                            }
                        }
                    }
                } else {
                    $localPeopleArray[$key]['isFollowing'] = "0";
                }
            }
        }
        $this->view->localPeopleArray = $localPeopleArray;
        $this->view->logged_user_Id = $logged_user_Id;
    }

    public function lamepeopleAction() {
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $logged_user_Id = $auth->getIdentity()->user_id;
        $logged_user_type = $auth->getIdentity()->user_type;
        if($logged_user_type != 'B') {
            $logged_user_type = 'U';
        }
        $limit = 300;
        $keyword='';
        // Get LAME People Based on Cool Points
        $searchObj = new Search_Model_Search();
        $lamePeopleArray =  $searchObj->getLamePeople($limit, $keyword);
        // Get Logged User Following Users-list
        $followObj = new User_Model_Followers();
        $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
        $followingCnt = count($loggedUserFollowing);
        if(count($lamePeopleArray) > 0) {
            foreach($lamePeopleArray as $key => $popularUsr) {
                $popularUsrType = $popularUsr['user_type'];
                if($popularUsr['user_type'] != 'B') {
                    $popularUsrType = 'U';
                }
                if($followingCnt > 0) {
                    foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                        if($popularUsr['user_id'] == $logged_user_Id && $popularUsrType == $logged_user_type) {
                            $lamePeopleArray[$key]['isFollowing'] = "2";
                        } else {
                            if(($popularUsr['user_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                $lamePeopleArray[$key]['isFollowing'] = "1";
                                break;
                            } else {
                                $lamePeopleArray[$key]['isFollowing'] = "0"; 
                            }
                        }
                    }
                } else {
                    $lamePeopleArray[$key]['isFollowing'] = "0";
                }
            }
        }
        $this->view->lamePeopleArray = $lamePeopleArray;
        $this->view->logged_user_Id = $logged_user_Id;
    }

    public function toppopularbusinessAction() {
        // to get Popular business Categories
        $busiCatObj = new Business_Model_Businesscategories();
        $categories = $busiCatObj->getTopPopularBusinessCategories();
        $this->view->businesscategory = $categories;
        // Get Logged User Id & user_type
        $auth = Zend_Auth::getInstance()->setStorage(new Zend_Auth_Storage_Session('normaluser'));
        $logged_user_Id = $auth->getIdentity()->user_id;
        $logged_user_type = $auth->getIdentity()->user_type;
        if($logged_user_type != 'B') {
            $logged_user_type = 'U';
        }
        $isAjaxReq  = $this->getRequest()->isXmlHttpRequest();
        if($isAjaxReq) {
            $this->_helper->layout->disableLayout();
            $request  = $this->getRequest();
            $businessId = $request->getParam('id');
            $searchObj  = new Search_Model_Search();
            $topBusiness = $searchObj->getTopBusinessesById($businessId);
            // get Logged User Following Users-list
            $followObj = new User_Model_Followers();
            $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
            $followingCnt = count($loggedUserFollowing);
            if(count($topBusiness) > 0) {
                foreach($topBusiness as $key => $popularBusiness) {
                    $popularBusinessType = $popularBusiness['user_type'];
                    if($popularBusiness['user_type'] != 'B') {
                        $popularBusinessType = 'U';
                    }
                    if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                            if($popularBusiness['business_id'] == $logged_user_Id && $popularBusinessType == $logged_user_type) {
                                $topBusiness[$key]['isFollowing'] = "2";
                            } else {
                                if(($popularBusiness['business_id'] == $Usrfollows['follower_id']) && ($popularBusinessType == $Usrfollows['follower_type'])) {
                                    $topBusiness[$key]['isFollowing'] = "1";
                                    break;
                                }  else {
                                    $topBusiness[$key]['isFollowing'] = "0"; 
                                }
                            }
                        }
                    } else {
                        $topBusiness[$key]['isFollowing'] = "0";
                    }
                }
            }
        } else {
            $businessId = $categories[0]['id'];
            $searchObj  = new Search_Model_Search();
            $topBusiness = $searchObj->getTopBusinessesById($businessId);
            // get Logged User Following Users-list
            $followObj = new User_Model_Followers();
            $loggedUserFollowing = $followObj->getFollowing($logged_user_Id, $logged_user_type);
            $followingCnt = count($loggedUserFollowing);
            if(count($topBusiness) > 0) {
                foreach($topBusiness as $key => $popularBusiness) {
                    $popularBusinessType = $popularBusiness['user_type'];
                    if($popularBusiness['user_type'] != 'B') {
                        $popularBusinessType = 'U';
                    }
                    if($followingCnt > 0) {
                        foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                            if($popularBusiness['business_id'] == $logged_user_Id && $popularBusinessType == $logged_user_type) {
                                $topBusiness[$key]['isFollowing'] = "2";
                            } else {
                                if(($popularBusiness['business_id'] == $Usrfollows['follower_id']) && ($popularBusinessType == $Usrfollows['follower_type'])) {
                                    $topBusiness[$key]['isFollowing'] = "1";
                                    break;
                                } else {
                                    $topBusiness[$key]['isFollowing'] = "0"; 
                                }
                            }
                        }
                    } else {
                        $topBusiness[$key]['isFollowing'] = "0";
                    }
                }
            }
        }
        $this->view->topBusiness = $topBusiness;
        $this->view->assign('isAjaxReq',$isAjaxReq);   
    }

    public function advancedsearchAction() {
        $request = $this->getRequest();
        $keyword = $request->keyword;
        $searchObj  = new Search_Model_Search();
        $category  = $request->category;
        if($category == '') {
            $category = "People";
        }
        if($request->catergory == 'Business') {
            $category = 'Business';
        }
        // to get Logged USer Details
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $logged_user_Id = $logged_user_data->user_id;
            $logged_user_type = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $logged_user_Id = $logged_user_data->business_id;
            $logged_user_type = 'B';
        }
        $searchArray = array();
        $searchDetails = array();
        //echo "<pre>".$logged_user_type;print_r($request->getParams());exit;
        /************ FOR LEFT SIDE BAR SEARCH *************/
        if($request->q != '') {
            $q = $request->q;
            $val = $request->q;
        } else {
            $q = "Enter a Keyword";
            if($request->keyword != '') {$val = $request->keyword;} else {$val = '';}
        }
        if($logged_user_type == 'B' && $q != '') {
            $searchDetails = $searchObj->getBusinessDetails($q, $logged_user_Id, $logged_user_type);
            //echo '<pre>';print_r($searchDetails);exit;
            if(count($searchDetails) > 0) {
                foreach($searchDetails as $key=>$searchU) {
                    $searchArray[$key]['user_id'] = $searchU['business_id'];
                    $searchArray[$key]['username'] = $searchU['username'];
                    $searchArray[$key]['uname'] = $searchU['business_name'];
                    $searchArray[$key]['firstname'] = $searchU['firstname'];
                    $searchArray[$key]['image_path'] = $searchU['image_path'];
                    $searchArray[$key]['user_type'] = 'B';
                }
            }
            $searchDetails = $searchArray;
        } 
        
        if($logged_user_type == 'U' && $q != '' && $q != 'Enter a Keyword') {
            $searchDetails = $searchObj->getPeopleDetails($q, $logged_user_Id, $logged_user_type);
            if(count($searchDetails) > 0) {
                foreach($searchDetails as $key=>$searchU){
                    $searchArray[$key]['user_id'] = $searchU['user_id'];
                    $searchArray[$key]['username'] = $searchU['username'];
                    $searchArray[$key]['uname'] = $searchU['uname'];
                    $searchArray[$key]['firstname'] = $searchU['firstname'];
                    $searchArray[$key]['image_path'] = $searchU['profile_pic_path'];
                    $searchArray[$key]['user_type'] = 'U';
                }
            }
            $searchDetails = $searchArray;
        }
            
        // SEARCH IN TYPE PEOPLE
        if($request->category == "People" && $keyword != '') {
            $searchDetails = $searchObj->getPeopleDetails($keyword, $logged_user_Id, $logged_user_type);
            if(count($searchDetails) > 0) {
                foreach($searchDetails as $key =>$searchU) {
                    $searchArray[$key]['user_id'] = $searchU['user_id'];
                    $searchArray[$key]['username'] = $searchU['username'];
                    $searchArray[$key]['uname'] = $searchU['uname'];
                    $searchArray[$key]['firstname'] = $searchU['firstname'];
                    $searchArray[$key]['image_path'] = $searchU['profile_pic_path'];
                    $searchArray[$key]['user_type'] = 'U';
                }
            }
            $searchDetails = $searchArray;
        }
            
        // SEARCH IN TYPE BUSINESS
        if($request->category == "Business"  && $keyword != '') {
            $searchDetails = $searchObj->getBusinessDetails($keyword, $logged_user_Id, $logged_user_type);
            if(count($searchDetails) > 0) {
                foreach($searchDetails as $key =>$searchU){
                    $searchArray[$key]['user_id'] = $searchU['business_id'];
                    $searchArray[$key]['username'] = $searchU['username'];
                    $searchArray[$key]['uname'] = $searchU['business_name'];
                    $searchArray[$key]['firstname'] = $searchU['firstname'];
                    $searchArray[$key]['image_path'] = $searchU['image_path'];
                    $searchArray[$key]['user_type'] = 'B';   
                }
            }
            $searchDetails = $searchArray;
        }
            
        // SEARCH IN TYPE COLLEGE/UNIVERSITY
        if($request->category == "College") {
            $searchDetails = $searchObj->getCollegeDetails($keyword);
            if(count($searchDetails) > 0) {
                foreach($searchDetails as $key =>$searchU){
                    $searchArray[$key]['college_id'] = $searchU['college_id'];
                    $searchArray[$key]['college_name'] = $searchU['college_name'];
                    $searchArray[$key]['college_city'] = $searchU['college_city'];
                    $searchArray[$key]['followerscnt'] = $searchU['followerscnt'];
                    $searchArray[$key]['type'] = "C";
                }
            }
            $searchDetails = $searchArray;
        }

        // SEARCH IN TYPE SCHOOLS
        if($request->category == "School") {
            $searchDetails = $searchObj->getSchoolDetails($keyword);
            if(count($searchDetails) > 0) {
                foreach($searchDetails as $key =>$searchU){
                    $searchArray[$key]['college_id'] = $searchU['school_id'];
                    $searchArray[$key]['college_name'] = $searchU['school_name'];
                    $searchArray[$key]['college_city'] = $searchU['school_city'];
                    $searchArray[$key]['followerscnt'] = $searchU['followerscnt'];
                    $searchArray[$key]['type'] = "S";
                }
            }
            $searchDetails = $searchArray;
        }

        // SEARCH IN TYPE WORK PLACE
        if($request->category == "Workplace") {
                $searchDetails = $searchObj->getWorkPlaceDetails($keyword);
                if(count($searchDetails) > 0) {
                    foreach($searchDetails as $key =>$searchU){
                        $searchArray[$key]['college_id'] = $searchU['work_place_id'];
                        $searchArray[$key]['college_name'] = $searchU['work_place_name'];
                        $searchArray[$key]['college_city'] = $searchU['work_place_city'];
                        $searchArray[$key]['followerscnt'] = $searchU['followerscnt'];
                        $searchArray[$key]['type'] = "W";
                    }
                }
                $searchDetails = $searchArray;
            }
         
        $filterArray = array();
        $states= array();
        $cities= array();
        /*************** FOR FILTER SEARCH ****************/        
        if($request->filterSearchBtn != '' && $request->filterSearchBtn=='Filter Search'){
            $filterArray['gender'] = $request->getParam('gender');
            $filterArray['agefrom'] = $request->getParam('agefrom');            
            $filterArray['ageto'] = $request->getParam('ageto');
            $filterArray['email'] = $request->getParam('email');    
            $filterArray['country'] = $request->getParam('country');    
            $filterArray['state'] = $request->getParam('state');    
            $filterArray['city'] = $request->getParam('city');    
            $filterArray['school'] = $request->getParam('school');    
            $filterArray['college'] = $request->getParam('college');    
            $filterArray['workplace'] = $request->getParam('workplace');    
            $searchDetails = $searchObj->getFilterSearchDetails($filterArray);
            $this->view->filterArray = $filterArray;
            if(count($searchDetails) > 0) {
                foreach($searchDetails as $key =>$searchU){
                    $searchArray[$key]['user_id'] = $searchU['user_id'];
                    $searchArray[$key]['username'] = $searchU['username'];
                    $searchArray[$key]['uname'] = $searchU['realname'];
                    $searchArray[$key]['firstname'] = $searchU['firstname'];
                    $searchArray[$key]['image_path'] = $searchU['profile_pic_path'];
                    $searchArray[$key]['user_type'] = 'U';
                }
                $searchDetails = $searchArray;
            }
            
            // get state details if Country is selected
            if($filterArray['country'] != '') {
                $stateObj = new User_Model_States();
                $states = $stateObj->get_states($filterArray['country']);
            }
            
            // get City details if state is selected
            if($filterArray['state'] != '') {
                $cityObj = new User_Model_Cities();
                $cities = $cityObj->get_cities($filterArray['state']);
            }
        }
                
        // get All Countries for Filter Dropdown
        $countriesObj = new User_Model_Countries();
        $countries = $countriesObj->get_countries();
        $countries = $countries->toArray();
        $userObj = new User_Model_User();
        // get Schools Details for Filter Dropdown
        $schoolsArray = $userObj->getSchools();
        // to get colleges for filter DropDown
        $colleges = $userObj->getColleges();
        // to get Workplaces for filter DropDown
        $workplaces = $userObj->getWorkPlaces();

        if(count($searchDetails) > 0) {
        // get Logged User Following Users-list
            $followObj = new User_Model_Followers();
            $loggedUserFollowing = $followObj->getAllFollowing($logged_user_Id, $logged_user_type);
            $followingCnt = count($loggedUserFollowing);
            if($category == 'People' || $category == 'Business') {
                foreach($searchDetails as $key => $search) {
                    $searchUserType = $search['user_type'];
                    $user_id = $search['user_id'];
                    if($search['user_type'] != 'B' && $category == 'People') {
                        $searchUserType = 'U';
                    }
                    if($category == 'Businsess') {
                        $searchUserType = 'B';
                    }
                    if($logged_user_type == "B" && $category == 'People') {
                        $searchDetails[$key]['isFollowing'] = "2";
                    } else {
                        if($followingCnt > 0) {
                            foreach($loggedUserFollowing as $key1 => $Usrfollows) {
                                if($user_id == $logged_user_Id && $searchUserType == $logged_user_type) {
                                    $searchDetails[$key]['isFollowing'] = "2";
                                } else {
                                    if(($user_id == $Usrfollows['follower_id']) && ($searchUserType == $Usrfollows['follower_type'])) {
                                       $searchDetails[$key]['isFollowing'] = "1";
                                       break;
                                    }  else {
                                       $searchDetails[$key]['isFollowing'] = "0"; 
                                    }
                                }
                            }
                        } else {
                            if($user_id == $logged_user_Id && $searchUserType == $logged_user_type) {
                                $searchDetails[$key]['isFollowing'] = "2";
                            } else {
                                $searchDetails[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                }
            }
            if($request->category == 'College' || $request->category == 'School' || $request->category == 'Workplace') {
                if(count($searchDetails) > 0) {
                    foreach($searchDetails as $key => $popularUsr) {
                        if($request->category == 'College')
                            $popularUsrType = 'C';
                        if($request->category == 'School')
                            $popularUsrType = 'S';
                        if($request->category == 'Workplace')
                            $popularUsrType = 'W';
                        if($followingCnt > 0) {
                            foreach($loggedUserFollowing as $Usrfollows) {
                                if($popularUsr['college_id'] == $logged_user_Id && $popularUsrType == $popularUsr['type']) {
                                    $searchDetails[$key]['isFollowing'] = "2";
                                } else {
                                    if(($popularUsr['college_id'] == $Usrfollows['follower_id']) && ($popularUsrType == $Usrfollows['follower_type'])) {
                                        $searchDetails[$key]['isFollowing'] = "1";
                                        break;
                                    }  else {
                                        $searchDetails[$key]['isFollowing'] = "0"; 
                                    }
                                }
                            }
                        } else {
                            if($popularUsr['college_id'] == $logged_user_Id && $popularUsrType == 'U') {
                                $searchDetails[$key]['isFollowing'] = "2";
                            } else {
                                $searchDetails[$key]['isFollowing'] = "0";
                            }
                        }
                    }
                }
            }
        }
        $this->view->searchDetails = $searchDetails;
        $this->view->keyword = $keyword;
        $this->view->category = $category;
        $this->view->q = $q;
        $this->view->val = $request->q;
        $this->view->schools = $schoolsArray;
        $this->view->colleges = $colleges;
        $this->view->workplaces = $workplaces;
        $this->view->countries = $countries;
        $this->view->states = $states;
        $this->view->cities = $cities;
    }

}