<?php class Search_Model_Search {

    public function getTopPopularPeople($limit, $pagenum, $search_key = '') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (U.user_account_status !='D' AND U.user_account_status !='I')";
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1){
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd){
                            $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
            }
        }
        $select = $db->select()
                        ->from(array('U'=>'tbl_users'), array('user_id','firstname','lastname','gender','profile_pic_path','user_type','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'user_level', 'city'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'));
        $select->where($where);
        $select->order(array('CP.cool_points DESC', 'U.firstname ASC'));
        $offset = $pagenum * 30;
        $select .=" LIMIT ".$offset.",".$limit;
        $popularArray = $db->fetchAll($select);
        if(empty($popularArray))
                $popularArray = array();
        return $popularArray;
    }

    public function getLocalPeople($city, $state, $country) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                    ->from(array('U' => 'tbl_users'),array('gender','user_id','firstname','lastname','profile_pic_path','user_type','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'U.city','U.state', 'U.country', 'user_level'))
                    ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' => 'level_name'))
                    ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'));
        if($city != '0' OR $state != '0' OR $country != '0')
            $select->where('U.city = "'.$city.'" OR U.state="'.$state.'" OR U.country="'.$country.'"');
        $select->order(array('U.city ASC', 'U.state', 'U.country','CP.cool_points DESC'));
        $popularArray = $db->fetchAll($select);
        if(empty($popularArray))
            $popularArray = array();
        return $popularArray;
    }

    public function getLamePeople($limit, $pagenum, $search_key = '') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (U.user_account_status !='D' AND U.user_account_status !='I')";            
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd) {
                        $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
            }
        }
        $select = $db->select()
                        ->from(array('U' => 'tbl_users'), array('user_id','firstname','lastname','gender','profile_pic_path','user_type','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'user_level', 'city'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                        ->joinLeft(array('CP'=>'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'));
        $select->where($where);
        $select->order(array('CP.cool_points ASC', 'U.firstname ASC', 'U.user_id Desc'));
        $offset = $pagenum * 30;
        $select .= " LIMIT ".$offset.",".$limit;
        $popularArray = $db->fetchAll($select);
        if(empty($popularArray))
            $popularArray = array();
        return $popularArray;
    }

    public function getTopBusinessesById($businessId) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from(array('B' => 'tbl_business_users'), array('business_id','business_name', 'image_path', 'username' => 'business_name', 'user_type'))
                        ->where('category_id ="'.$businessId.'"')
                        ->order('business_created_date DESC');
        $popularArray = $db->fetchAll($select);
        if(empty($popularArray))
            $popularArray = array();
        return $popularArray;
    }

    public function getPeopleDetails($search_key, $logged_user_Id, $logged_user_type) {
        $where = " (U.user_account_status !='D' AND U.user_account_status !='I')";
        $db = Zend_Db_Table::getDefaultAdapter();
        $resultArray = array();
        if($search_key != '') {
            if(strlen($search_key) < 4) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
                $from = array('user_id', 'uname'=>'CONCAT(U.firstname, " ", U.lastname)', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'user_type', 'username','orderflag'=> "if(U.user_id = '".$logged_user_Id."', 1, 0)");
                //$order = array('U.firstname ASC', 'U.lastname ASC', 'U.username ASC');
                $order = array('orderflag DESC');
            }
            if(strlen($search_key) >= 4) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd) {
                        $subWrd .= '"+'.$wrd.'*"';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
                $from = array('user_id', 'uname'=>'CONCAT(U.firstname, " ", U.lastname)', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'user_type', 'username', 'relevence'=>$db->quoteInto('MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($search_key))),'orderflag'=> "if(U.user_id = '".$logged_user_Id."', 1, 0)");
                $order = array('orderflag DESC','relevence DESC', 'U.username ASC');
            }
        }
        $select = $db->select()
                        ->from(array('U' => 'tbl_users'), $from)
                        ->where($where)
                        ->order($order);
        //echo $select;exit;
        $resultArray = $db->fetchAll($select);
        if(empty($resultArray))
                $resultArray = array();
        return $resultArray;
    }

    public function getBusinessDetails($keyword, $logged_user_Id, $logged_user_type = '') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (B.status !='D' AND B.status !='I')";
        $resultArray = array();
        if($keyword != '') {
            if(strlen($keyword) < 4) {
                    $where .= ' AND (B.firstname LIKE "%'.addslashes($keyword).'%" OR B.lastname LIKE "%'.addslashes($keyword).'%" OR B.username LIKE "%'.addslashes($keyword).'%"  OR B.business_name LIKE "%'.addslashes($keyword).'%")';
                    $from = array('business_id', 'uname'=>'CONCAT(firstname, " ", lastname)', 'business_name','username','firstname','lastname','image_path','user_type', 'orderflag'=> "if(B.business_id = '".$logged_user_Id."', 1, 0)");
                    //$order = array('business_name ASC', 'username ASC');
                    $order = array('orderflag DESC');
            }
            if(strlen($keyword) >= 4) {
                $wrdArr =array();
                $wrdArr = explode(" ", $keyword);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd) {
                        $subWrd .= '"+'.$wrd.'*"';
                    }
                    $keyword = $subWrd;
                }
                $where .= ' AND MATCH(B.business_name, B.firstname, B.lastname, B.username) AGAINST ("+*'.urldecode(addslashes($keyword)).'*" IN BOOLEAN MODE) ';
                $from = array('business_id', 'uname'=>'CONCAT(firstname, " ", lastname)', 'business_name', 'realname'=>'CONCAT(firstname, " ", lastname)', 'firstname', 'lastname', 'image_path', 'user_type', 'username', 'relevence'=>$db->quoteInto('MATCH(business_name, firstname, lastname, username) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($keyword))), 'orderflag'=> "if(B.business_id = '".$logged_user_Id."', 1, 0)");
                $order = array('orderflag DESC','relevence DESC', 'B.username ASC');
            }
        }
        $select = $db->select()
                        ->from(array('B' => 'tbl_business_users'), $from)
                        ->where($where)
                        ->order($order);
        $resultArray = $db->fetchAll($select);
        if(empty($resultArray))
                $resultArray = array();
        return $resultArray;
    }

    public function getCollegeDetails($keyword) { 
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (status ='1')";
        $resultArray = array();
        if($keyword != '') {
            if(strlen($keyword) < 4) {
                $where .= ' AND (college_name LIKE "%'.addslashes($keyword).'%")';
                $from = array('college_id', 'college_name','college_city');
                $order = array('followerscnt DESC','B.popular_order ASC');
            }
            if(strlen($keyword) >= 4) {
                $wrdArr =array();
                $wrdArr = explode(" ", $keyword);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1){
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd) {
                            $subWrd .= '"+'.$wrd.'*"';
                    }
                    $keyword = $subWrd;
                }
                $where .= ' AND MATCH(B.college_name) AGAINST ("+*'.urldecode(addslashes($keyword)).'*" IN BOOLEAN MODE) ';
                $from = array('college_id', 'college_name','college_city', 'relevence'=>$db->quoteInto('MATCH(college_name) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($keyword))));
                $order = array('relevence DESC', 'followerscnt DESC', 'B.college_name ASC');
            }
        }
        $select = $db->select()
                        ->from(array('B' => 'tbl_colleges'), $from)
                        ->joinLeft(array('F' => 'tbl_followers'), "B.college_id = F.follower_id AND F.follower_type = 'C'", array('followerscnt' => 'COUNT(F.follower_id)'))
                        ->where($where)
                        ->group('B.college_id')
                        ->order($order);
        $resultArray = $db->fetchAll($select);
        if(empty($resultArray) || $resultArray[0]['college_id'] == '')
            $resultArray = array();
        return $resultArray;
    }

    public function getSchoolDetails($keyword) { 
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (status ='1')";
        $resultArray = array();
        if($keyword != ''){
            if(strlen($keyword) < 4) {
                $where .= ' AND (school_name LIKE "%'.addslashes($keyword).'%")';
                $from = array('school_id', 'school_name','school_city');
                $order = array('followerscnt DESC','B.popular_order ASC');
            }
            if(strlen($keyword) >= 4) {
                $wrdArr =array();
                $wrdArr = explode(" ", $keyword);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1){
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd){                            
                            $subWrd .= '"+'.$wrd.'*"';
                    }
                    $keyword = $subWrd;
                }
                $where .= ' AND MATCH(B.school_name) AGAINST ("+*'.urldecode(addslashes($keyword)).'*" IN BOOLEAN MODE) ';
                $from = array('school_id', 'school_name','school_city', 'relevence'=>$db->quoteInto('MATCH(school_name) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($keyword))));
                $order = array('relevence DESC', 'followerscnt DESC', 'B.school_name ASC');
            }
        }
        $select = $db->select()
                                ->from(array('B' => 'tbl_schools'), $from)
                                ->joinLeft(array('F' => 'tbl_followers'), "B.school_id = F.follower_id AND F.follower_type = 'S'", array('followerscnt' => 'COUNT(F.follower_id)'))
                                ->where($where)
                                ->group('B.school_id')
                                ->order($order);
        //echo $select;exit;
        $resultArray = $db->fetchAll($select);
        //echo "<pre>";print_r($resultArray);exit;
        if(empty($resultArray) || $resultArray[0]['school_id'] == '')
            $resultArray = array();
        return $resultArray;
    }

    public function getWorkPlaceDetails($keyword) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (status ='1')";
        $resultArray = array();
        if($keyword != ''){
            if(strlen($keyword) < 4) {
                $where .= ' AND (work_place_name LIKE "%'.addslashes($keyword).'%")';
                $from = array('work_place_id', 'work_place_name','work_place_city');;
                $order = array('followerscnt DESC','B.popular_order ASC');
            }
            if(strlen($keyword) >= 4) {
                $wrdArr =array();
                $wrdArr = explode(" ", $keyword);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1){
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd){                            
                            $subWrd .= '"+'.$wrd.'*"';
                    }
                    $keyword = $subWrd;
                }
                $where .= ' AND MATCH(B.work_place_name) AGAINST ("+*'.urldecode(addslashes($keyword)).'*" IN BOOLEAN MODE) ';
                $from = array('work_place_id', 'work_place_name','work_place_city', 'relevence'=>$db->quoteInto('MATCH(work_place_name) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($keyword))));
                $order = array('relevence DESC', 'followerscnt DESC', 'B.work_place_name ASC');
            }
        }
        $select = $db->select()
                        ->from(array('B' => 'tbl_work_place'), $from)
                        ->joinLeft(array('F' => 'tbl_followers'), "B.work_place_id = F.follower_id AND F.follower_type = 'C'", array('followerscnt' => 'COUNT(F.follower_id)'))
                        ->where($where)
                        ->group('B.work_place_id')
                        ->order($order);
        $resultArray = $db->fetchAll($select);
        if(empty($resultArray) || $resultArray[0]['work_place_id'] == '')
            $resultArray = array();
        return $resultArray;
    }

    public function getUserZipCodeByID($logged_user_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select()
                        ->from(array('U' => 'tbl_users'), array('zipcode'))
                        ->where("U.user_id ='".$logged_user_id."'");
        $resultArray = $db->fetchAll($select);
        return $resultArray;
    }

    public function getLocalPeopleByZipcode($zipcode, $limit, $pagenum, $search_key) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (U.zipcode= '".$zipcode."' AND U.user_account_status !='D' AND U.user_account_status !='I')";            
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr = array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    //$i = 1;
                    foreach($wrdArr as $wrd) {
                            $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
            }
        }
        $select = $db->select()
                        ->from(array('U' => 'tbl_users'),array('user_id','firstname','lastname','gender', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)','profile_pic_path','user_type','username', 'U.city','U.state', 'U.country', 'user_level', 'zipcode', 'created_Date'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'));
        $select->where($where);
        $select->order(array('U.created_date DESC'));
        $offset = $pagenum * 30;
        $select .=" LIMIT ".$offset.",".$limit;
        $localArray = $db->fetchAll($select);
        if(empty($localArray))
            $localArray = array();
        return $localArray;
    }

    public function getcelebrities($pagenum, $limit, $search_key='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = " (U.user_type ='C' AND U.user_account_status !='D' AND U.user_account_status !='I')";
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd) {
                        $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
            }
        }
        $offset = $pagenum * $limit;
        $select = $db->select()
                        ->from(array('U'=>'tbl_users'), array('user_id','firstname','lastname','profile_pic_path','user_type','username', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'user_level','city'))
                        ->joinLeft(array('L' => 'tbl_user_levels'),"U.user_level = L.level_no",array('user_level_name' =>'level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'))
                        ->where($where)
                        ->order(array('CP.cool_points ASC', 'U.firstname ASC'))
                        ->limit($limit,$offset);
        $result = $db->fetchAll($select);
        return $result;
    }

    public function getNearestBusiness($lat, $lng, $pagenum, $limit, $search_key) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where ="1=1";
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (B.business_name LIKE "%'.addslashes($search_key).'%"OR B.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    foreach($wrdArr as $wrd) {
                            $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(B.business_name, B.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
            }
        }
        if($lat != '' && $lng != '') {
            $select = "SELECT B.business_id, B.business_name, B.image_path, B.user_type, B.username, B.city, B.lat, B.lng, Z.zipcode, address_line_1, address_line_2, (((acos(sin((".$lat."*pi()/180)) * sin((B.lat*pi()/180))+cos((".$lat."*pi()/180)) * cos((B.lat*pi()/180)) * cos(((".$lng."- B.lng)*pi()/180))))*180/pi())*60*1.1515) as distance  FROM tbl_business_users AS B LEFT JOIN tbl_zipcode AS Z ON Z.id=B.zipcode WHERE ".$where." ORDER BY B.business_name, distance ASC";
        } else {
            $select = "SELECT B.business_id, B.business_name, B.image_path, B.user_type, B.username, B.city, B.lat, B.lng, Z.zipcode, address_line_1, address_line_2 FROM tbl_business_users AS B LEFT JOIN tbl_zipcode AS Z ON Z.id=B.zipcode WHERE ".$where." ORDER BY B.business_name ASC";
        }
        $offset = $pagenum * $limit;
        $select .=" LIMIT ".$offset.",".$limit;
        $businessArray = $db->fetchAll($select);
        if(empty($businessArray))
            $businessArray = array();
        return $businessArray;
    }

    public function getFriends($limit, $search_key='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where ="1=1";
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (B.business_name LIKE "%'.addslashes($search_key).'%"OR B.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd) {
                        $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(B.business_name, B.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
            }
        }
        $select = $db->select()
                        ->from(array('F' => 'tbl_followers'), array('follower_id','follower_type','followers_id'))
                        ->joinLeft(array('B' => 'tbl_business_users')," B.business_id = F.follower_id AND F.follower_type = 'B' ", array('business_id','business_name','image_path','business_city'=>'city'))
                        ->joinLeft(array('U' => 'tbl_users')," U.user_id = F.follower_id AND F.follower_type = 'U' ", array('user_id','firstname','lastname','gender','profile_pic_path', 'user_level', 'city'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                        ->joinLeft(array('C' => 'tbl_city')," C.id = U.city", array('city' =>'city_name'))
                        ->joinLeft(array('BC' => 'tbl_city')," BC.id = B.city", array('business_city' =>'city_name'))
                        ->where($where)
                        ->group('F.follower_id')
                        ->order ('F.created_date DESC')
                        ->limit($limit);
        $resultSet = $db->fetchAll($select);
        //echo '<pre>';print_r($resultSet);exit;
        $result = array();
        for($k = 0; $k < count($resultSet);$k++) {
            if($resultSet[$k]['follower_type']  == 'B') {
                $ary['name'] = $resultSet[$k]['business_name'];
                $ary['firstname'] = $resultSet[$k]['business_name'];
                $ary['lastname'] = '';
                $ary['follower_id'] = $resultSet[$k]['business_id'];
                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                $ary['user_level'] = $resultSet[$k]['user_level'];
                $ary['gender'] = $resultSet[$k]['gender'];
                $ary['user_level_name'] = $resultSet[$k]['user_level_name'];
                if(!empty($resultSet[$k]['business_city']))
                    $ary['city'] = $resultSet[$k]['business_city'];
                else
                    $ary['city'] = '';
                $ary['image_name'] = $resultSet[$k]['image_path'];
                $result[] = $ary;
            } else if($resultSet[$k]['follower_type']  == 'U') {
                $ary['name'] = $resultSet[$k]['firstname'];
                $ary['firstname'] = $resultSet[$k]['firstname'];
                $ary['lastname'] = $resultSet[$k]['lastname'];
                $ary['follower_id'] = $resultSet[$k]['follower_id'];
                $ary['follower_type'] = $resultSet[$k]['follower_type'];
                $ary['user_level'] = $resultSet[$k]['user_level'];
                $ary['gender'] = $resultSet[$k]['gender'];
                $ary['user_level_name'] = $resultSet[$k]['user_level_name'];
                if(!empty($resultSet[$k]['city']))
                    $ary['city'] = $resultSet[$k]['city'];
                else
                    $ary['city'] = '';
                $ary['image_name'] = $resultSet[$k]['profile_pic_path'];
                $ary['cool_points'] = $resultSet[$k]['cool_points'];
                $result[] = $ary;
            }
        }
        return $result;
    }

    public function getAllUsers($logged_user_Id, $limit, $pagenum, $search_key='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where ="(U.user_account_status !='D' AND U.user_account_status !='I') ";
        //$where ="1=1 and U.user_id != '".$logged_user_Id."'";
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    foreach($wrdArr as $wrd) {
                            $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname,U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE)';
            }
        }
        $offset = $pagenum * $limit;
        $select = $db->select()
                        ->from(array('U' => 'tbl_users'), array('user_id','firstname','lastname', 'username', 'profile_pic_path', 'user_level', 'user_type', 'city', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)', 'orderflag'=> "if(U.user_id = '".$logged_user_Id."', 1, 0)"))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                        ->where($where)
                        ->group('U.user_id')
                        ->order (array('orderflag DESC','U.created_date DESC'))
                        ->limit($limit,$offset);
        $resultSet = $db->fetchAll($select);
        return $resultSet;
    }

    public function getFilterSearchDetails($data, $limit='') {
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = "(U.user_account_status !='D' AND U.user_account_status !='I')";
        // for age keyword
        if(isset($data['keyword']) && $data['keyword'] != '') {
            $search_key = $data['keyword'];
            if(strlen($search_key) < 5) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    foreach($wrdArr as $wrd) {
                        $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname,U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE)';
            }
        }

        //for gender
        /*if(isset($data['mkey']) && $data['mkey'] != '') {
            $where .=" AND U.gender = '".$data['gender']."'";
        } else {*/
            if(isset($data['gender']) && $data['gender'] != '') {
                if(isset($data['gender'][0]) && $data['gender'][0] != '')
                    $data['gender'][0] = "'".$data['gender'][0]."'";
                else
                    $data['gender'][0] = "'".$data['gender']."'";
                if(isset($data['gender'][1]) && $data['gender'][1] != '') {
                    $data['gender'][1] = "'".$data['gender'][1]."'";
                }
                $data['gender'] = implode(',', $data['gender']);
                $where .=" AND U.gender IN(".$data['gender'].")";
            }
        //}

        // for age from
        if(isset($data['agefrom']) && $data['agefrom'] != '') {
            $where .=" AND U.dob <= DATE_SUB(NOW(),INTERVAL ".$data['agefrom']." YEAR)";
        }

        // for Age To
        if(isset($data['ageto']) && $data['ageto'] != '') {
            $where .=" AND U.dob >= DATE_SUB(NOW(),INTERVAL ".$data['ageto']." YEAR)";
        }

        // for email
        if(isset($data['email']) && $data['email'] != '') {
            $where .=" AND U.email = '".$data['email']."'";
        }

        // for Country
        if(isset($data['country']) && $data['country'] != '') {
            $where .=" AND U.country = '".$data['country']."'";
        }

        // for State
        if(isset($data['state']) && $data['state'] != '') {
            $where .=" AND U.state = '".$data['state']."'";
        }

        // for City
        if(isset($data['city']) && $data['city'] != '') {
            $where .=" AND U.city = '".$data['city']."'";
        }

        // for School
        if(isset($data['school']) && $data['school'] != '') {
            $where .=" AND U.high_school_info = '".$data['school']."'";
        }

        // for College
        if(isset($data['college']) && $data['college'] != '') {
            $where .=" AND U.college_info = '".$data['college']."'";
        }

        // for Work Place
        if(isset($data['workplace']) && $data['workplace'] != '') {
            $where .=" AND U.employer_info = '".$data['workplace']."'";
        }

        $select = $db->select()
                        ->from(array('U' => 'tbl_users'), array('user_id','firstname','lastname', 'dob', 'username', 'profile_pic_path', 'user_level', 'user_type', 'city', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                        ->where($where)
                        ->group('U.user_id')
                        ->order (array('U.firstname','U.created_date DESC'));
        $offset = 0;
        if($limit != '')
            $select->limit($limit,$offset);
        //if($_SERVER['REMOTE_ADDR'] == '192.168.1.53') { echo $select;exit; }
        $resultSet = $db->fetchAll($select);
        return $resultSet;
    }

    public function getCompleteSearchDetails($data, $logged_user_id, $pagenum, $limit) {
        //echo '<pre>';print_r($data);exit;
        $db = Zend_Db_Table::getDefaultAdapter();
        $where = "(U.user_account_status !='D' AND U.user_account_status !='I')";
        if($data['category'] == '') {
            $data['category'] = 'People';
        }
        if(isset($data['keyword']) && $data['keyword'] != '') {
            $search_key = $data['keyword'];
            if(strlen($search_key) < 4) {
                if($data['category'] == 'People') {
                    $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
                    $from = array('user_id', 'uname'=>'CONCAT(U.firstname, " ", U.lastname)', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'user_level', 'user_type', 'username', 'city', 'orderflag'=> "if(U.user_id = '".$logged_user_id."', 1, 0)");
                    $order = array('orderflag DESC', 'U.created_date DESC');
                }
                if($data['category'] == 'Business') {
                    $where .= ' AND (B.firstname LIKE "%'.addslashes($search_key).'%" OR B.lastname LIKE "%'.addslashes($search_key).'%" OR B.username LIKE "%'.addslashes($search_key).'%"  OR B.business_name LIKE "%'.addslashes($search_key).'%")';
                    $from = array('business_id as user_id', 'uname'=>'CONCAT(firstname, " ", lastname)', 'business_name','username','firstname','lastname','image_path','user_type', 'city', 'orderflag'=> "if(B.business_id = '".$logged_user_id."', 1, 0)");
                    $order = array('orderflag DESC');
                }
                if($data['category'] == 'School') {
                    $where .= ' AND (S.school_name LIKE "%'.addslashes($search_key).'%")';
                    $from = array('school_id as id', 'school_name as name','school_city as city');
                    $order = array('followerscnt DESC','S.popular_order ASC');
                }
                if($data['category'] == 'College') {
                    $where .= ' AND (C.college_name LIKE "%'.addslashes($search_key).'%")';
                    $from = array('college_id as id', 'college_name as name','college_city as city');
                    $order = array('followerscnt DESC','C.popular_order ASC');
                }
                if($data['category'] == 'Workplace') {
                    $where .= ' AND (W.work_place_name LIKE "%'.addslashes($search_key).'%")';
                    $from = array('work_place_id as id', 'work_place_name as name','work_place_city as city');
                    $order = array('followerscnt DESC','W.popular_order ASC');
                }
            }
            if(strlen($search_key) >= 4) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    foreach($wrdArr as $wrd) {
                        $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                if($data['category'] == 'People') {
                    $where .= ' AND MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE)';
                    $from = array('user_id', 'uname'=>'CONCAT(U.firstname, " ", U.lastname)', 'realname'=>'CONCAT(U.firstname, " ", U.lastname)', 'firstname', 'lastname', 'profile_pic_path', 'user_type', 'username','city', 'relevence'=>$db->quoteInto('MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($search_key))),'orderflag'=> "if(U.user_id = '".$logged_user_id."', 1, 0)");
                    $order = array('orderflag DESC', 'U.created_date DESC', 'relevence DESC', 'U.username ASC');
                }
                if($data['category'] == 'Business') {
                    $where .= ' AND MATCH(B.firstname, B.lastname, B.business_name, B.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE)';
                    $from = array('business_id as user_id', 'uname'=>'CONCAT(firstname, " ", lastname)', 'business_name', 'realname'=>'CONCAT(firstname, " ", lastname)', 'firstname', 'lastname', 'image_path', 'user_type', 'username', 'city', 'relevence'=>$db->quoteInto('MATCH(business_name, firstname, lastname, username) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($search_key))), 'orderflag'=> "if(B.business_id = '".$logged_user_id."', 1, 0)");
                    $order = array('orderflag DESC', 'relevence DESC', 'B.username ASC');
                }
                if($data['category'] == 'School') {
                    $where .= ' AND MATCH(S.school_name) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE)';
                    $from = array('school_id as id', 'school_name as name','school_city as city', 'relevence'=>$db->quoteInto('MATCH(school_name) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($search_key))));
                    $order = array('relevence DESC', 'followerscnt DESC', 'S.school_name ASC');
                }
                if($data['category'] == 'College') {
                    $where .= ' AND MATCH(C.college_name) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE)';
                    $from = array('college_id as id', 'college_nameas name','college_city as city', 'relevence'=>$db->quoteInto('MATCH(college_name) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($search_key))));
                    $order = array('relevence DESC', 'followerscnt DESC', 'C.college_name ASC');
                }
                if($data['category'] == 'Workplace') {
                    $where .= ' AND MATCH(W.work_place_name) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE)';
                    $from = array('work_place_id as id', 'work_place_name as name','work_place_city as city', 'relevence'=>$db->quoteInto('MATCH(work_place_name) AGAINST ("+*?*" IN BOOLEAN MODE)', urldecode(addslashes($search_key))));
                    $order = array('relevence DESC', 'followerscnt DESC', 'W.work_place_name ASC');
                }
            }
        }

        //for gender
        if(isset($data['gender']) && $data['gender'] != '') {
            if(isset($data['gender'][0]) && $data['gender'][0] != '')
                $data['gender'][0] = "'".$data['gender'][0]."'";
            else
                $data['gender'][0] = "'".$data['gender']."'";
            if(isset($data['gender'][1]) && $data['gender'][1] != '') {
                $data['gender'][1] = "'".$data['gender'][1]."'";
            }
            $data['gender'] = implode(',', $data['gender']);
            if($data['category'] == 'People')
                $where .=" AND U.gender IN(".$data['gender'].")";
        }

        // for Age From
        if(isset($data['agefrom']) && $data['agefrom'] != '') {
            if($data['category'] == 'People')
                $where .=" AND U.dob <= DATE_SUB(NOW(),INTERVAL ".$data['agefrom']." YEAR)";
        }

        // for Age To
        if(isset($data['ageto']) && $data['ageto'] != '') {
            if($data['category'] == 'People')
                $where .=" AND U.dob >= DATE_SUB(NOW(),INTERVAL ".$data['ageto']." YEAR)";
        }

        // for email
        if(isset($data['email']) && $data['email'] != '') {
            if($data['category'] == 'People')
                $where .=" AND U.email = '".$data['email']."'";
            if($data['category'] == 'Business')
                $where .=" AND B.email = '".$data['email']."'";
        }

        // for Country
        if(isset($data['country']) && $data['country'] != '') {
            if($data['category'] == 'People')
                $where .=" AND U.country = '".$data['country']."'";
            if($data['category'] == 'Business')
                $where .=" AND B.country = '".$data['country']."'";
        }

        // for State
        if(isset($data['state']) && $data['state'] != '') {
            if($data['category'] == 'People')
                $where .=" AND U.state = '".$data['state']."'";
            if($data['category'] == 'Business')
                $where .=" AND B.state = '".$data['state']."'";
        }

        // for City
        if(isset($data['city']) && $data['city'] != '') {
            if($data['category'] == 'People')
                $where .=" AND U.city = '".$data['city']."'";
            if($data['category'] == 'Business')
                $where .=" AND B.city = '".$data['city']."'";
        }

        // for School
        if(isset($data['school']) && $data['school'] != '') {
            if($data['category'] == 'People')
            $where .=" AND U.high_school_info = '".$data['school']."'";
        }

        // for College
        if(isset($data['college']) && $data['college'] != '') {
            if($data['category'] == 'People')
            $where .=" AND U.college_info = '".$data['college']."'";
        }

        // for Work Place
        if(isset($data['workplace']) && $data['workplace'] != '') {
            if($data['category'] == 'People')
            $where .=" AND U.employer_info = '".$data['workplace']."'";
        }

        if($data['category'] == 'People') {
            $select = $db->select()
                            ->from(array('U' => 'tbl_users'), $from)
                            ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                            ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id  AND  CP.user_type = 'U'", array('cool_points'))
                            ->where($where)
                            ->group('U.user_id')
                            ->order($order);
        } else if($data['category'] == 'Business') {
            $select = $db->select()
                            ->from(array('B' => 'tbl_business_users'), $from)
                            ->where($where)
                            ->group('B.business_id')
                            ->order($order);
        } else if($data['category'] == 'School') {
            $select = $db->select()
                            ->from(array('S' => 'tbl_schools'), $from)
                            ->joinLeft(array('F' => 'tbl_followers'), "S.school_id = F.follower_id AND F.follower_type = 'S'", array('followerscnt' => 'COUNT(F.follower_id)','follower_type as type'))
                            ->where($where)
                            ->group('S.school_id')
                            ->order($order);
        } else if($data['category'] == 'College') {
            $select = $db->select()
                            ->from(array('C' => 'tbl_colleges'), $from)
                            ->joinLeft(array('F' => 'tbl_followers'), "C.college_id = F.follower_id AND F.follower_type = 'C'", array('followerscnt' => 'COUNT(F.follower_id)','follower_type as type'))
                            ->where($where)
                            ->group('C.college_id')
                            ->order($order);
        } else if($data['category'] == 'Workplace') {
            $select = $db->select()
                            ->from(array('W' => 'tbl_work_place'), $from)
                            ->joinLeft(array('F' => 'tbl_followers'), "W.work_place_id = F.follower_id AND F.follower_type = 'W'", array('followerscnt' => 'COUNT(F.follower_id)','follower_type as type'))
                            ->where($where)
                            ->group('W.work_place_id')
                            ->order($order);
        }
        $offset = $pagenum * $limit;
        $select->limit($limit,$offset);
        $resultSet = $db->fetchAll($select);
        return $resultSet;
    }

    public function getSuggestedFriendsByZipcode($logged_user_id, $logged_user_type, $zipcode, $limit, $pagenum, $search_key) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select1 = $db->select()
                        ->from(array('F'=>'tbl_followers'), array('F.follower_id'))
                        ->where("F.user_id='".$logged_user_id."' AND F.follower_type='".$logged_user_type."'");
        $where = " U.user_id != '".$logged_user_id."' AND U.user_id NOT IN (".$select1.") AND U.zipcode= '".$zipcode."' AND U.user_account_status !='D' AND U.user_account_status !='I'";            
        if($search_key != '') {
            if(strlen($search_key) < 5) {
                $where .= ' AND (U.firstname LIKE "%'.addslashes($search_key).'%" OR U.lastname LIKE "%'.addslashes($search_key).'%" OR U.username LIKE "%'.addslashes($search_key).'%")';
            }
            if(strlen($search_key) >= 5) {
                $wrdArr =array();
                $wrdArr = explode(" ", $search_key);
                $wrdCnt = count($wrdArr);
                if($wrdCnt > 1) {
                    $subWrd = '';
                    //$i =1;
                    foreach($wrdArr as $wrd) {
                            $subWrd .= '"'.$wrd.'" ';
                    }
                    $search_key = $subWrd;
                }
                $where .= ' AND MATCH(U.firstname, U.lastname, U.username) AGAINST ("+*'.urldecode(addslashes($search_key)).'*" IN BOOLEAN MODE) ';
            }
        }
        $select = $db->select()
                        ->from(array('U' => 'tbl_users'),array('user_id','firstname','lastname', 'realname' => 'CONCAT(U.firstname, " ", U.lastname)','gender','profile_pic_path','user_type','username', 'U.city','U.state', 'U.country', 'user_level', 'zipcode', 'created_Date'))
                        ->joinLeft(array('UL' => 'tbl_user_levels')," UL.id = U.user_level", array('user_level_name' =>'level_name'))
                        ->joinLeft(array('CP' => 'tbl_user_coolpoints')," U.user_id = CP.user_id AND CP.user_type = 'U'", array('cool_points'));
        $select->where($where);
        $select->order(array('U.firstname ASC','U.lastname ASC'));
        $offset = $pagenum * $limit;
        $select->limit($limit, $offset);
        $localArray = $db->fetchAll($select);
        if(empty($localArray))
            $localArray = array();
        return $localArray;
    }

}