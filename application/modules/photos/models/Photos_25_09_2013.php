<?php
class Photos_Model_Photos {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Photos_Model_DbTable_Photos');
                }
                return $this->_dbTable;
        }

        public function getphotowithcomments($userid, $user_type, $photo_id,$limit='100') {
                //echo $userid.'-->'.$user_type.'-->'.$photo_id.'-->';exit;
                $select = $this->getDbTable()->select()
                                ->from(array('P'=>'tbl_photos'),array('photo_id','photo_original_name','photo_modified_name','user_id','user_type'))
                                ->joinLeft(array('C'=>'tbl_photo_comments'),'C.photo_id = P.photo_id',array('user_posted'=>'user_id','comments','commented_date','commented_user_type' =>'user_type'))
                                ->joinLeft(array('U'=>'tbl_users'),'U.user_id = C.user_id AND C.user_type="U"',array('user_image'=>'profile_pic_path','username','firstname','lastname'))
                                ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = C.user_id AND C.user_type="B"',array('business_image'=>'image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname'))
                                ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$userid.'" AND P1.user_type="'.$user_type.'")',array('photo_id as isBlastedPhoto'))
                                ->where("P.photo_id =?",$photo_id)
                                ->order("C.comment_id DESC")
                                ->limit($limit)
                                ->setIntegrityCheck(false);
                if($_SERVER['REMOTE_ADDR'] == '192.168.1.57') {
                    //echo $select;exit;
                }                
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getPhotoCommentCnt($photo_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C'=>'tbl_photo_comments'),array('COUNT' =>"COUNT('C.photo_id')"))
                                ->where("C.photo_id =?",$photo_id);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function isAvailable($logged_User_Id,$logged_User_Type,$photo_id) {
                $select = $this->getDbTable()->select()
                                ->from('tbl_photos',array('photo_id'))
                                ->where('user_id = ?', $logged_User_Id)
                                ->where('user_type = ?', $logged_User_Type)
                                ->where('blast_id = ?',$photo_id);
                //echo $select;exit;
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getdaycount($loggedUserId, $loggedUserType) {
            $date = date('Y-m-d');
			$db = Zend_Db_Table::getDefaultAdapter();
			$select = $db->select()
							->from('tbl_scribbles as S', array('COUNT(S.scribble_id) as count'))
							->joinLeft('tbl_blasts as B', 'S.blast_id = B.scribble_id', '')
							->where('B.user_id = ?',$loggedUserId)
							->where('S.from_user_id = ?',$loggedUserId)
							->where('date(S.created_date) = ?',$date);
			
			$resultSet = $db->fetchRow($select);
			$scribbleCnt = $resultSet['count'];
			
			$select = $db->select()
							->from('tbl_photos', array('COUNT(photo_id) as count'))
							->where('user_id = ?',$loggedUserId)
							->where('user_type = ?',$loggedUserType)
							->where('blast_id != 0')
							->where('date(blasted_date) = ?',$date);
							
			$resultSet = $db->fetchRow($select);
			$resultSet['count']+= $scribbleCnt;
			return $resultSet;
        }
        
        public function blastPhoto($dataAry) {
                $db = Zend_Db_Table::getDefaultAdapter();
                // for Blasting Photo
                //$inserted = $db->insert('tbl_photos',$dataAry);
                return $this->getDbTable()->insert($dataAry);            
        }
        
        public function getPhotoDetailsByPhotoId($photo_id) {
                $select = $this->getDbTable()->select()
                                ->from(array('P'=>'tbl_photos'),array('photo_id','album_id','photo_original_name','photo_modified_name','user_id','user_type','width','height','android_width','android_height'))
                                ->where("P.photo_id =?",$photo_id);
                //echo $select;exit;
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }
        
        public function updateParentPhotoBlastedDate($updAry, $photo_id) {
            if(!empty($updAry) && $photo_id != ''){
                try {
                        $db = Zend_Db_Table::getDefaultAdapter();
                        return  $db->update('tbl_photos', $updAry, ' photo_id = "'.$photo_id.'" OR blast_id = "'.$photo_id.'"');
                } catch (Zend_Exception $zex){}
            }
                
        }

        public function getPhotoDetailsByPhotoModifiedName($photo_modified_name) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('P'=>'tbl_photos'),array('COUNT(photo_id) as count'))
                                ->where("P.photo_modified_name =?",$photo_modified_name);
                //echo $select;exit;
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function updatephotowidthnheight($data, $photo_modified_name) {
            //echo 'DB:<pre>';print_r($data);exit;
            $db = Zend_Db_Table::getDefaultAdapter();
            return  $db->update('tbl_photos', $data, 'photo_modified_name = "'.$photo_modified_name.'"');
        }
        
        public function getAllOriginalPhotos(){
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select()
                        ->from(array('P'=>'tbl_photos'),array('photo_id', 'blast_id', 'blasted_date', 'created_date'))
                        ->where('P.blast_id =0');
            $resultSet = $db->fetchAll($select);
            return $resultSet;
        }
        
        public function updatePhotoBlastedDateByBlastedIds($photo_id, $blasted_date, $blasted_ids){
            if($photo_id != '' && $blasted_date != '' && $blasted_ids != ''){
               try {
                        $updAry['blasted_date'] = $blasted_date;
                        $db = Zend_Db_Table::getDefaultAdapter();
                        return  $db->update('tbl_photos', $updAry, ' photo_id IN ('.$blasted_ids.') AND blast_id = "'.$photo_id.'"');    
               } catch (Zend_Exception $zex){}
            }
            
            
        }

}