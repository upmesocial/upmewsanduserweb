<?php
class Photos_Model_Photocomments {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Photos_Model_DbTable_Photocomments');
                }
                return $this->_dbTable;
        }

        public function savephotocomment($data) {//echo '<pre>';print_r($data);exit;
                return $this->getDbTable()->insert($data);
        }

}