<?php
class Photos_Model_Photos {

        protected $_dbTable;

        public function setDbTable($dbTable) {
                if (is_string($dbTable)) {
                        $dbTable = new $dbTable();
                }
                if (!$dbTable instanceof Zend_Db_Table_Abstract) {
                        throw new Exception('Invalid table data gateway provided');
                }
                $this->_dbTable = $dbTable;
                return $this;
        }

        public function getDbTable() {
                if (null === $this->_dbTable) {
                        $this->setDbTable('Photos_Model_DbTable_Photos');
                }
                return $this->_dbTable;
        }

        public function getphotowithcomments($userid, $user_type, $photo_id,$limit='100') {
                $select = $this->getDbTable()->select()
                                ->from(array('P'=>'tbl_photos'),array('photo_id','photo_original_name','photo_modified_name','user_id','user_type','gps_location','gps_lat','gps_lng'))
                                ->joinLeft(array('C'=>'tbl_photo_comments'),'C.photo_id = P.photo_id',array('user_posted'=>'user_id','comments','commented_date','commented_user_type' =>'user_type'))
                                ->joinLeft(array('U'=>'tbl_users'),'U.user_id = C.user_id AND C.user_type="U"',array('user_image'=>'profile_pic_path','username','firstname','lastname'))
                                ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = C.user_id AND C.user_type="B"',array('business_image'=>'image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname'))
                                ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$userid.'" AND P1.user_type="'.$user_type.'")',array('photo_id as isBlastedPhoto'))
                                ->where("P.photo_id =?",$photo_id)
                                ->order("C.comment_id DESC")
                                ->limit($limit)
                                ->setIntegrityCheck(false);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }

        public function getPhotoCommentCnt($photo_id) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('C'=>'tbl_photo_comments'),array('COUNT' =>"COUNT('C.photo_id')"))
                                ->where("C.photo_id =?",$photo_id);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function isAvailable($logged_User_Id,$logged_User_Type,$photo_id) {
                $select = $this->getDbTable()->select()
                                ->from('tbl_photos',array('photo_id'))
                                ->where('user_id = ?', $logged_User_Id)
                                ->where('user_type = ?', $logged_User_Type)
                                ->where('blast_id = ?',$photo_id);
                $resultSet = $this->getDbTable()->fetchRow($select);
                return $resultSet;
        }

        public function getdaycount($loggedUserId, $loggedUserType) {
                $date = date('Y-m-d');
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from('tbl_scribbles as S', array('COUNT(S.scribble_id) as count'))
                                ->joinLeft('tbl_blasts as B', 'S.blast_id = B.scribble_id', '')
                                ->where('B.user_id = ?',$loggedUserId)
                                ->where('S.from_user_id = ?',$loggedUserId)
                                ->where('date(S.created_date) = ?',$date);
                $resultSet = $db->fetchRow($select);
                $scribbleCnt = $resultSet['count'];
                $select = $db->select()
                                ->from('tbl_photos', array('COUNT(photo_id) as count'))
                                ->where('user_id = ?',$loggedUserId)
                                ->where('user_type = ?',$loggedUserType)
                                ->where('blast_id != 0')
                                ->where('date(blasted_date) = ?',$date);
                $resultSet = $db->fetchRow($select);
                $resultSet['count'] += $scribbleCnt;
                return $resultSet;
        }
        
        public function blastPhoto($dataAry) {
                $db = Zend_Db_Table::getDefaultAdapter();
                // for Blasting Photo
                //$inserted = $db->insert('tbl_photos',$dataAry);
                return $this->getDbTable()->insert($dataAry);            
        }
        
        public function getPhotoDetailsByPhotoId($photo_id) {
                $select = $this->getDbTable()->select()
                                ->from(array('P'=>'tbl_photos'),array('photo_id','album_id','photo_original_name','photo_modified_name','user_id','user_type','width','height','android_width','android_height','gps_location','gps_lat','gps_lng'))
                                ->where("P.photo_id =?",$photo_id);
                $resultSet = $this->getDbTable()->fetchAll($select);
                return $resultSet;
        }
        
        public function updateParentPhotoBlastedDate($updAry, $photo_id) {
                if(!empty($updAry) && $photo_id != '') {
                        try {
                                $db = Zend_Db_Table::getDefaultAdapter();
                                return  $db->update('tbl_photos', $updAry, ' photo_id = "'.$photo_id.'" OR blast_id = "'.$photo_id.'"');
                        } catch (Zend_Exception $zex){}
                }
                
        }

        public function getPhotoDetailsByPhotoModifiedName($photo_modified_name) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('P'=>'tbl_photos'),array('COUNT(photo_id) as count'))
                                ->where("P.photo_modified_name =?",$photo_modified_name);
                $resultSet = $db->fetchRow($select);
                return $resultSet;
        }

        public function updatephotowidthnheight($data, $photo_modified_name) {
                $db = Zend_Db_Table::getDefaultAdapter();
                return $db->update('tbl_photos', $data, 'photo_modified_name = "'.$photo_modified_name.'"');
        }

        public function getAllOriginalPhotos() {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('P'=>'tbl_photos'),array('photo_id', 'blast_id', 'blasted_date', 'created_date', 'gps_location', 'gps_lat', 'gps_lng'))
                                ->where('P.blast_id =0');
                $resultSet = $db->fetchAll($select);
                return $resultSet;
        }

        public function updatePhotoBlastedDateByBlastedIds($photo_id, $blasted_date, $blasted_ids) {
                if($photo_id != '' && $blasted_date != '' && $blasted_ids != '') {
                        $updAry = array();
                        try {
                                $updAry['blasted_date'] = $blasted_date;
                                $db = Zend_Db_Table::getDefaultAdapter();
                                return  $db->update('tbl_photos', $updAry, ' photo_id IN ('.$blasted_ids.') AND blast_id = "'.$photo_id.'"');
                        } catch (Zend_Exception $zex) {}
                }
        }

        public function getphotodetailedinfo($logged_userId,$userId,$photoId) {
                $db = Zend_Db_Table::getDefaultAdapter();
                $select = $db->select()
                                ->from(array('P' => 'tbl_photos'),array('photo_id as ID', 'created_date as date', 'photo_name as photoname', 'description as photodesc', 'photo_modified_name as name', 'width', 'height', 'android_width', 'android_height', 'user_id as userID', 'photo_modified_name as Path', 'user_type as userType', 'gps_location', 'gps_lat', 'gps_lng', 'blast_id as BlastId', 'blasted_date as BlastedDate'))
                                ->joinLeft(array('U'=>'tbl_users'),'U.user_id = P.user_id AND P.user_type="U"',array('profile_pic_path','username', "CONCAT(U.firstname, ' ', U.lastname) realname",'user_level as userLevel'))
                                ->joinLeft(array('L' => 'tbl_user_levels'),'L.id = U.user_level', array('user_level_name' => 'level_name'))
                                ->joinLeft(array('C'=>'tbl_photo_comments'), 'C.photo_id = P.photo_id AND P.photo_id = '.$photoId,array('cmntCnt'=>'count(C.photo_id)'))
                                ->joinLeft(array('P1'=>'tbl_photos'),'(P1.blast_id = P.photo_id AND P1.user_id = "'.$logged_userId.'" AND P1.user_type="U")',array('P1.photo_id as isBlasted'))
                                ->where('P.photo_id = ?',$photoId);
                $resultSet = $db->fetchRow($select);
                $resultSet['type'] = 'photo';
                if($resultSet['cmntCnt'] > 0) {
                        $photoCmntSelect = $db->select()
                                ->from(array('PC'=>'tbl_photo_comments'),array('PC.comments','PC.commented_date'))
                                ->joinLeft(array('U'=>'tbl_users'),'U.user_id = PC.user_id AND PC.user_type="U"',array('profile_pic_path','username','firstname','lastname', 'user_id', 'usertype'=>'user_type'))
                                ->joinLeft(array('B'=>'tbl_business_users'),'B.business_id = PC.user_id AND PC.user_type="B"',array('image_path','business_username'=>'username','business_firstname'=>'firstname','business_lastname'=>'lastname', 'business_id', 'B_usertype'=>'user_type'))
                                ->where("PC.photo_id =?",$photoId)
                                ->order('PC.commented_date DESC')
                                ->limit(2);
                        $photoCommentsArr = array();
                        $photoCommentsArr = $db->fetchAll($photoCmntSelect);
                        if(count($photoCommentsArr) > 0) {
                                $i =1;
                                foreach($photoCommentsArr as $comment) {
                                        $cmntIndex = 'comment'.$i;
                                        $name = 'commenter_name'.$i;
                                        $id = 'commenter_id'.$i;
                                        $useType = 'commenter_type'.$i;
                                        $pic_path = 'profile_pic_path'.$i;
                                        $date = 'commenter_date'.$i;
                                        if($comment['comments'] != '')   {
                                                $resultSet[$cmntIndex] = $comment['comments'];
                                                if($comment['usertype'] != '') {
                                                        $resultSet[$id] = $comment['user_id'];
                                                        $resultSet[$useType] = 'U';
                                                        $resultSet[$pic_path] = $comment['profile_pic_path'];
                                                        $resultSet[$name] = $comment['firstname']." ".$comment['lastname'];
                                                        $resultSet[$date] = $comment['commented_date'];
                                                }
                                                if($comment['B_usertype'] != '') {
                                                        $resultSet[$id] = $comment['business_id'];
                                                        $resultSet[$useType] = "B";
                                                        $resultSet[$pic_path] = $comment['image_path'];
                                                        $resultSet[$name] = $comment['business_firstname']." ".$comment['business_lastname'];
                                                        $resultSet[$date] = $comment['commented_date'];
                                                }
                                        }
                                        $i++;
                                } // foreach
                        }
                }
                return $resultSet;
        }

}