<?php
class Photos_IndexController extends Zend_Controller_Action {

    public function init() {
        /* Initialize action controller here */
        if(Zend_Registry::isRegistered('userdata')) {
            $logged_user_data = Zend_Registry::get('userdata');
            $loggedUserId = $logged_user_data->user_id;
            $user_account_status = $logged_user_data->user_account_status;
            $loggedUserType = 'U';
        }
        if(Zend_Registry::isRegistered('businessdata')) {
            $logged_user_data = Zend_Registry::get('businessdata');
            $loggedUserId = $logged_user_data->business_id;
            $user_account_status = $logged_user_data->status;
            $loggedUserType = 'B';
        }
        if(!empty($logged_user_data)) {
            $limit= 30;
            $photosObj = new Mobile_Model_Photos();
            $followersgallery = $photosObj->getTimelineClickedImagesVideosDetails($loggedUserId,$loggedUserType, $limit);
            //echo "<pre>";print_r($followersgallery);exit;
            //echo count($followersgallery);exit;
            $this->view->assign('followersgallery', $followersgallery);
        }
    }

    public function indexAction() {
        // action body
    }

    public function businessphotosAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $mobileobj = new Mobile_Model_Albums();
        $user_id = $this->getRequest()->getParam('business_id');
        $user_type = $this->getRequest()->getParam('user_type');
        $imagegallery = $mobileobj->getBusinessAlbumsByUserId($user_id, $user_type);
        if(!empty($imagegallery)) {
                $resultAry = array('service_status'=>'success',"content" => $imagegallery);
        } else {
                $resultAry = array('service_status'=>'error',"error_msg" => 'No Images to View');
        }
        echo json_encode($resultAry); 
    }

    public function businessvideosAction() {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $videoObj = new Videos_Model_Videos();
        $user_id = $this->getRequest()->getParam('business_id');
        $user_type = $this->getRequest()->getParam('user_type');
        $videogallery = $videoObj->getBusinessVideosByUserId($user_id, $user_type);
        if(!empty($videogallery)) {
                $resultAry = array('service_status'=>'success',"content" => $videogallery);
        } else {
                $resultAry = array('service_status'=>'error',"error_msg" => 'No Videos To View');
        }
        echo json_encode($resultAry); 
    }
    
    public function samplemobilephotosresizeAction(){
        ini_set('max_execution_time', 300);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->_redirect('user');
        // read folder contents.
        $upload_path = UPLOAD_PATH;
        $oldFolderPath = $upload_path."images/original/";
        $newFolderPath = $upload_path."images/mobile/";
        $newImageArray = array();
        if (is_dir($oldFolderPath)) {
            if ($dh = opendir($oldFolderPath)) {
                $imageObj = new UpmeSocial_Controller_Action_Helper_Image();
                $photoObj = new Photos_Model_Photos();
                while (($file = readdir($dh)) !== false) {
                    echo "filename: ".$file."<br />";
                    if($file != "." && $file != ".." && file_exists($oldFolderPath."/".$file) && $file != "._.DS_Store" && $file != ".DS_Store") {
                        $newImageArray[] = $imageObj->testSaveMobileImage($oldFolderPath, $newFolderPath, $file);
                    }
                }
                closedir($dh);
            }
        }
        foreach($newImageArray as $image):
            $photo = $photoObj->getPhotoDetailsByPhotoModifiedName($image['0']);
            if($photo['count'] >=1) {
                $data['width'] = $image['1'];
                $data['height'] = $image['2'];
                $update = $photoObj->updatephotowidthnheight($data, $image['0']);
            }
        endforeach;
    }
    
    public function samplemobilephotosresizeforandroidAction() {//echo 'hi';exit;
        ini_set('max_execution_time', 300);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        //$this->_redirect('user');
        // read folder contents.
        $upload_path = UPLOAD_PATH;
        $oldFolderPath = $upload_path."images/original/";
        $newFolderPath = $upload_path."images/android/";
        $newImageArray = array();
        if (is_dir($oldFolderPath)) {
            if ($dh = opendir($oldFolderPath)) {
                $imageObj = new UpmeSocial_Controller_Action_Helper_Image();
                $photoObj = new Photos_Model_Photos();
                while (($file = readdir($dh)) !== false) {
                    echo "filename: ".$file."<br />";
                    if($file != "." && $file != ".." && file_exists($oldFolderPath."/".$file) && $file != "._.DS_Store" && $file != ".DS_Store") {
                        $newImageArray[] = $imageObj->testSaveMobileImageForAndroid($oldFolderPath, $newFolderPath, $file);
                    }
                }
                closedir($dh);
            }
        }
        foreach($newImageArray as $image):
            $photo = $photoObj->getPhotoDetailsByPhotoModifiedName($image['0']);
            if($photo['count'] >=1) {
                $data['android_width'] = $image['1'];
                $data['android_height'] = $image['2'];
                $update = $photoObj->updatephotowidthnheight($data, $image['0']);
            }
        endforeach;
    }

    public function samplehuddlephotosresizeAction() {
        ini_set('max_execution_time', 300);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->_redirect('user');
        // read folder contents.
        $upload_path = UPLOAD_PATH;
        $oldFolderPath = $upload_path."images/original/";
        $newFolderPath = $upload_path."images/huddle/";
        $newImageArray = array();
        if (is_dir($oldFolderPath)) {
            if ($dh = opendir($oldFolderPath)) {
                $imageObj = new UpmeSocial_Controller_Action_Helper_Image();
                $huddleObj = new Huddles_Model_Huddles();
                while (($file = readdir($dh)) !== false) {
                    $filesArray = array("1369144154.jpg","1369171329.jpg", "1369246676.jpg", "1369435904.jpg");
                    //$filesArray = array("1370856928.jpg","1370587113.jpg", "1370582628.jpg", "1370439246.jpg", "1370438450.jpg", "1370340536.jpg", "1370013828.jpg", "1369927547.jpg","1369925278.jpg","1369924274.jpg","1369921996.jpg","1369840354.jpg","");
                    if(in_array($file, $filesArray)) {
                        echo "filename: ".$file."<br />";
                        $newImageArray[] = $imageObj->testSaveHuddleImage($oldFolderPath, $newFolderPath, $file);
                    }
                }
                closedir($dh);
            }
        }
        foreach($newImageArray as $image):
            $photo = $huddleObj->getHuddleDetailsByRoomPhotoPath($image['0']);
            if($photo['count'] >=1) {
                $data['width'] = $image['1'];
                $data['height'] = $image['2'];
                $update = $huddleObj->updatehuddlewidthnheight($data, $image['0']);
            }
        endforeach;
    }
	
    public function samplephotosresizeAction() {
        ini_set('max_execution_time', 0);
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->_redirect('user');
        // read folder contents.
        $upload_path = UPLOAD_PATH;
        $oldFolderPath = $upload_path."images/original/";
        $newFolderPath = $upload_path."images/thumbnails/";
        //$newFolderPath = $upload_path."images/medium/";
        $newImageArray = array();
        if (is_dir($oldFolderPath)) {
            if ($dh = opendir($oldFolderPath)) {
                $imageObj = new UpmeSocial_Controller_Action_Helper_Image();
                while (($file = readdir($dh)) !== false) {
                    echo "filename: ".$file."<br />";
                    if($file != "." && $file != ".." && file_exists($oldFolderPath."/".$file) && $file != "._.DS_Store" && $file != ".DS_Store") {
                        $newImageArray[] = $imageObj->testSaveImage($oldFolderPath, $newFolderPath, $file);
                    }
                }
                closedir($dh);
            }
        }
    }
    
    public function cropimgwithbackgroundAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $image = Zend_Image( 'var/www/upme/public/uploads/images/original/1365592040.jpg', new Zend_Image_Driver_Imagick );
        echo 'Size: ' . $image->getWidth() . '×' . $image->getHeight();
    }

}