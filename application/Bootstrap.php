<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

        protected function setconstants($constants) {
                foreach ($constants as $key=>$value) {
                        if(!defined($key)) {
                                define($key, $value);
                        }
                }
        }

        protected function _initAppAutoload() {
                $autoloader = new Zend_Application_Module_Autoloader(array(
                        'namespace' => 'Admin_',
                        'basePath'  => APPLICATION_PATH .'/modules/admin',
                        'resourceTypes' => array (
                                'form' => array(
                                'path' => 'forms',
                                'namespace' => 'Form',
                    ),
                        'model' => array(
                                'path' => 'models',
                                'namespace' => 'Model',
                        ),
                    )
                ));
                /*$autoloader = new Zend_Application_Module_Autoloader(array(
                'namespace' => 'App',
                'basePath' => dirname(__FILE__),
                )); */
                return $autoloader;
        }

        protected function _initAutoloaders() {
                $user_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'User', 'basePath' => APPLICATION_PATH . '/modules/user'));

                $business_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Business', 'basePath' => APPLICATION_PATH . '/modules/business'));

                $scribbles_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Scribbles', 'basePath' => APPLICATION_PATH . '/modules/scribbles'));

                $mobile_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Mobile', 'basePath' => APPLICATION_PATH . '/modules/mobile'));

                $photos_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Photos', 'basePath' => APPLICATION_PATH . '/modules/photos'));

                $videos_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Videos', 'basePath' => APPLICATION_PATH . '/modules/videos'));

                $admin_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Admin', 'basePath' => APPLICATION_PATH . '/modules/admin'));

                $search_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Search', 'basePath' => APPLICATION_PATH . '/modules/search'));

                $huddels_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Huddles', 'basePath' => APPLICATION_PATH . '/modules/huddles'));
                
                $notifications_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Notifications', 'basePath' => APPLICATION_PATH . '/modules/notifications'));
                
                $crons_loader = new Zend_Application_Module_Autoloader(array('namespace' => 'Crons', 'basePath' => APPLICATION_PATH . '/modules/crons'));
        }

        protected function _initRotas() {
                $router = Zend_Controller_Front::getInstance()->getRouter();

                // User Sign Up Routing
                $route = new Zend_Controller_Router_Route('/user/signup',array('module' => 'user','controller' => 'index','action' => 'signup'));
                $router->addRoute('userSignupRoute', $route);
                
                // User Unsubscribe
                $route = new Zend_Controller_Router_Route('/user/unsubscribe',array('module' => 'user','controller' => 'index','action' => 'unsubscribe'));
                $router->addRoute('userUnsubscribeRoute', $route);

                // User Login  Routing
                $route = new Zend_Controller_Router_Route('/user/login',array('module' => 'user','controller' => 'index','action' => 'login'));
                $router->addRoute('userLoginRoute', $route);

                // User Facebook Login Routing
                $route = new Zend_Controller_Router_Route('/user/facebook/',array('module' => 'user','controller' => 'index','action' => 'facebook'));
                $router->addRoute('userFacebookLoginRoute', $route);

                // User Profile  Routing
                $route = new Zend_Controller_Router_Route('/user/userprofile/:username',array('module' => 'user','controller' => 'index','action' => 'userprofile','username' => 'username'));
                $router->addRoute('userProfileRoute', $route);
                
                // User Dashboard
                $route = new Zend_Controller_Router_Route('/user/dashboard',array('module' => 'user','controller' => 'index','action' => 'dashboard'));
                $router->addRoute('userDashboardRoute', $route);

                // Popular School
                $route = new Zend_Controller_Router_Route('/user/popularschools',array('module' => 'user','controller' => 'index','action' => 'popularschools'));
                $router->addRoute('userPopularSchoolsRoute', $route);

                // Popular Colleges
                $route = new Zend_Controller_Router_Route('/user/popularcolleges',array('module' => 'user','controller' => 'index','action' => 'popularcolleges'));
                $router->addRoute('userPopularCollegesRoute', $route);

                // Popular Work Places
                $route = new Zend_Controller_Router_Route('/user/popularworkplaces',array('module' => 'user','controller' => 'index','action' => 'popularworkplaces'));
                $router->addRoute('userPopularWorkPlacesRoute', $route);

                //UserMessage
                $route = new Zend_Controller_Router_Route('/user/messages',array('module' => 'user','controller' => 'index','action' => 'messages'));
                $router->addRoute('userMessageRoute', $route);

                //UserNotifications
                $route = new Zend_Controller_Router_Route('/notifications/getallnotifications',array('module' => 'notifications','controller' => 'index','action' => 'getallnotifications'));
                $router->addRoute('userNotificationsRoute', $route);

                //UserEdit
                $route = new Zend_Controller_Router_Route('/user/edit',array('module' => 'user','controller' => 'index','action' => 'edit'));
                $router->addRoute('userEditRoute', $route);

                //UserEdit Account Settings
                $route = new Zend_Controller_Router_Route('/user/accountsettings',array('module' => 'user','controller' => 'index','action' => 'accountsettings'));
                $router->addRoute('userAccountsettingsRoute', $route);
                
                //Users View Uppers
                $route = new Zend_Controller_Router_Route('/user/myuppers',array('module' => 'user','controller' => 'index','action' => 'myuppers'));
                $router->addRoute('userMyuppersRoute', $route);

                //User Account Deactivate
                $route = new Zend_Controller_Router_Route('/user/deactive',array('module' => 'user','controller' => 'index','action' => 'deactive'));
                $router->addRoute('userdeactiveRoute', $route);

                //User Account Active
                $route = new Zend_Controller_Router_Route('/user/useractive',array('module' => 'user','controller' => 'index','action' => 'useractive'));
                $router->addRoute('useractiveRoute', $route);

                //User Profile View Followers
                $route = new Zend_Controller_Router_Route('/user/viewfollowers',array('module' => 'user','controller' => 'index','action' => 'viewfollowers'));
                $router->addRoute('userviewfollowersRoute', $route);

                //User Profile View Following
                $route = new Zend_Controller_Router_Route('/user/viewfollowing',array('module' => 'user','controller' => 'index','action' => 'viewfollowing'));
                $router->addRoute('userviewfollowingRoute', $route);
                
                //Other Profile View
                $route = new Zend_Controller_Router_Route('/user/viewotheruser',array('module' => 'user','controller' => 'index','action' => 'viewotheruser'));
                $router->addRoute('userviewotheruserRoute', $route);

                //Update User Description
                $route = new Zend_Controller_Router_Route('/user/updateuserdescription',array('module' => 'user','controller' => 'index','action' => 'updateuserdescription'));
                $router->addRoute('userupdateuserdescriptionRoute', $route);

                //Update User Details
                $route = new Zend_Controller_Router_Route('/user/updateuserdetails',array('module' => 'user','controller' => 'index','action' => 'updateuserdetails'));
                $router->addRoute('userupdateuserdetailsRoute', $route);

                //Update User Status
                $route = new Zend_Controller_Router_Route('/user/updateuserstatus',array('module' => 'user','controller' => 'index','action' => 'updateuserstatus'));
                $router->addRoute('userupdateuserstatusRoute', $route);

                //Update User Location
                $route = new Zend_Controller_Router_Route('/user/updateuserlocation',array('module' => 'user','controller' => 'index','action' => 'updateuserlocation'));
                $router->addRoute('userupdateuserlocationRoute', $route);

                //Update User Privacy Details
                $route = new Zend_Controller_Router_Route('/user/updateuserprivacy',array('module' => 'user','controller' => 'index','action' => 'updateuserprivacy'));
                $router->addRoute('userupdateuserprivacyRoute', $route);

                //Update User Statyus Disable Details
                $route = new Zend_Controller_Router_Route('/user/statusdisable',array('module' => 'user','controller' => 'index','action' => 'statusdisable'));
                $router->addRoute('userupdatestatusdisableRoute', $route);

                //Update User Password Details
                $route = new Zend_Controller_Router_Route('/user/updateuserpassword',array('module' => 'user','controller' => 'index','action' => 'updateuserpassword'));
                $router->addRoute('userupdateuserpasswordRoute', $route);

                //Make Friends as Follow
                $route = new Zend_Controller_Router_Route('/user/makefriends',array('module' => 'user','controller' => 'index','action' => 'makefriends'));
                $router->addRoute('usermakefriendsRoute', $route);

                //Find Friends
                $route = new Zend_Controller_Router_Route('/user/findfriends',array('module' => 'user','controller' => 'index','action' => 'findfriends'));
                $router->addRoute('userFindFriendsRoute', $route);
                
                // User/Business Followers  Routing
                $route = new Zend_Controller_Router_Route('/user/followers/followers/:userid/:logged_user_Id/:usertype/:limit',array('module' => 'user','controller' => 'followers','action' => 'followers','userid' => 'userid','usertype' => strtoupper('usertype')));
                $router->addRoute('userFollowersRoute', $route);

                // User/Business Following  Routing
                $route = new Zend_Controller_Router_Route('/user/followers/following/:userid/:logged_user_Id/:usertype/:limit',array('module' => 'user','controller' => 'followers','action' => 'following','userid' => 'userid','usertype' => strtoupper('usertype')));
                $router->addRoute('userFollowingRoute', $route);

                // User/Business Followers Count Routing
                $route = new Zend_Controller_Router_Route('/user/followers/followerscnt/:userid/:usertype',array('module' => 'user','controller' => 'followers','action' => 'followerscnt','userid' => 'userid','usertype' => strtoupper('usertype')));
                $router->addRoute('userFollowersCntRoute', $route);

                // User/Business Following Count Routing
                $route = new Zend_Controller_Router_Route('/user/followers/followingcnt/:userid/:usertype',array('module' => 'user','controller' => 'followers','action' => 'followingcnt','userid' => 'userid','usertype' => strtoupper('usertype')));
                $router->addRoute('userFollowingCntRoute', $route);

                //Validate Signup
                $route = new Zend_Controller_Router_Route('/user/validatesignup',array('module' => 'user','controller' => 'index','action' => 'validatesignup'));
                $router->addRoute('userValidateSignupRoute', $route);

                //Validate Signup
                $route = new Zend_Controller_Router_Route('/user/profilepicture',array('module' => 'user','controller' => 'index','action' => 'profilepicture'));
                $router->addRoute('userProfilePictureRoute', $route);

                //Validate Signup
                $route = new Zend_Controller_Router_Route('/user/profilevideo',array('module' => 'user','controller' => 'index','action' => 'profilevideo'));
                $router->addRoute('userProfileVideoRoute', $route);

                //View User Albums
                $route = new Zend_Controller_Router_Route('/user/viewalbums',array('module' => 'user','controller' => 'index','action' => 'viewalbums'));
                $router->addRoute('userAlbumViewRoute', $route);

                //View Media
                $route = new Zend_Controller_Router_Route('/user/media/:type/:user_id',array('module' => 'user','controller' => 'index','action' => 'media', 'type' => '', 'user_id' => ''));
                $router->addRoute('userMediaRoute', $route);
                
                //View User Albums for Other User
                $route = new Zend_Controller_Router_Route('/user/viewalbums/:user_id',array('module' => 'user','controller' => 'index','action' => 'viewalbums'));
                $router->addRoute('userAlbumViewForOtherUserRoute', $route);

                //View User Album Photos
                $route = new Zend_Controller_Router_Route('/user/viewalbumphotos/:user_id/:album_id',array('module' => 'user','controller' => 'index','action' => 'viewalbumphotos','user_id'=>'','album_id' => ''));
                $router->addRoute('userAlbumPhotosRoute', $route);

                //View User Album Photo with Comments
                $route = new Zend_Controller_Router_Route('/user/viewalbumphotowithcomments/:user_id/:album_id/:photo_id',array('module' => 'user','controller' => 'index','action' => 'viewalbumphotowithcomments','user_id'=>'','album_id' => '','photo_id' => ''));
                $router->addRoute('userAlbumPhotosWithCommentsRoute', $route);

                //View User Photo Comments
                $route = new Zend_Controller_Router_Route('/user/viewcomments/:limit/:photo_id/:user_id/:user_type',array('module' => 'user','controller' => 'index','action' => 'viewcomments','limit' =>'','photo_id' => ''));
                $router->addRoute('userPhotoCommentsRoute', $route);

                //View User Photo with OtherUser Comments
                $route = new Zend_Controller_Router_Route('/user/userprofile/viewcomments/:limit/:photo_id',array('module' => 'user','controller' => 'index','action' => 'viewcomments','limit' =>'','photo_id' => ''));
                $router->addRoute('userPhotoCommentsOtherUserRoute', $route);

                //View User Videos
                $route = new Zend_Controller_Router_Route('/user/viewvideos',array('module' => 'user','controller' => 'index','action' => 'viewvideos'));
                $router->addRoute('userVideoViewRoute', $route);

                //View User Video with Comments
                $route = new Zend_Controller_Router_Route('/user/viewvideowithcomments/:user_id/:video_id',array('module' => 'user','controller' => 'index','action' => 'viewvideowithcomments','user_id'=>'','video_id' => ''));
                $router->addRoute('userVideoWithCommentsRoute', $route);
                
                //View User Videos for Other User
                $route = new Zend_Controller_Router_Route('/user/viewvideos/:user_id',array('module' => 'user','controller' => 'index','action' => 'viewvideos'));
                $router->addRoute('userVideoViewForOtherUserRoute', $route);

                //View User Video Comments
                $route = new Zend_Controller_Router_Route('/user/viewvideocomments/:limit/:video_id',array('module' => 'user','controller' => 'index','action' => 'viewvideocomments','limit' =>'','video_id' => ''));
                $router->addRoute('userVideoCommentsRoute', $route);

                //View User Video with OtherUser Comments
                $route = new Zend_Controller_Router_Route('/user/userprofile/viewvideocomments/:limit/:video_id',array('module' => 'user','controller' => 'index','action' => 'viewvideocomments','limit' =>'','video_id' => ''));
                $router->addRoute('userVideoCommentsOtherUserRoute', $route);

                //User Logout Routing
                $route = new Zend_Controller_Router_Route('/user/logout',array('module' => 'user','controller' => 'index','action' => 'logout'));
                $router->addRoute('userLogoutRoute', $route);

                //User Index Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/index/',array('module' => 'scribbles','controller' => 'index','action' => 'index'));
                $router->addRoute('userIndexScribbleRoute', $route);

                //User More LEvel Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/morelevelscribbles/',array('module' => 'scribbles','controller' => 'index','action' => 'morelevelscribbles'));
                $router->addRoute('userMoreLevelScribbleRoute', $route);

                //User Posting Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/postscribbleweb',array('module' => 'scribbles','controller' => 'index','action' => 'postscribbleweb'));
                $router->addRoute('userPostScribbleRoute', $route);

                //User Reply Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/replyscribble',array('module' => 'scribbles','controller' => 'index','action' => 'replyscribble'));
                $router->addRoute('userReplyScribbleRoute', $route);

                //User Up Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/upscribble',array('module' => 'scribbles','controller' => 'index','action' => 'upscribble'));
                $router->addRoute('userUpScribbleRoute', $route);

                //User down Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/downscribble',array('module' => 'scribbles','controller' => 'index','action' => 'downscribble'));
                $router->addRoute('userDownScribbleRoute', $route);

                //User Blast Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/blastscribble',array('module' => 'scribbles','controller' => 'index','action' => 'blastscribble'));
                $router->addRoute('userBlastScribbleRoute', $route);
                
                //User Blast Photo
                $route = new Zend_Controller_Router_Route('/user/blastphoto',array('module' => 'user','controller' => 'index','action' => 'blastphoto'));
                $router->addRoute('userBlastPhotoRoute', $route);

                //User View Scribble
                $route = new Zend_Controller_Router_Route('/scribbles/viewscribbles/:user_id',array('module' => 'scribbles','controller' => 'index','action' => 'viewscribbles','user_id' => 'user_id'));
                $router->addRoute('userViewScribbleRoute', $route);
                
                //User Scribble Huddle Target
                $route = new Zend_Controller_Router_Route('/scribble/targetdetails',array('module' => 'scribbles','controller' => 'index','action' => 'targetdetails'));
                $router->addRoute('userScribbleTargetDetailsRoute', $route);

                //Other User Profile
                $route = new Zend_Controller_Router_Route('/user/userprofile/:user_id',array('module' => 'user','controller' => 'index','action' => 'userprofile'));
                $router->addRoute('userOtherUserProfileRoute', $route);
                
                //Top Popular People
                $route = new Zend_Controller_Router_Route('/user/search',array('module' => 'search','controller' => 'index','action' => 'index'));
                $router->addRoute('topPopularPeopleIndexRoute', $route);
                
                //Top Popular People
                $route = new Zend_Controller_Router_Route('/user/search/index/',array('module' => 'search','controller' => 'index','action' => 'index'));
                $router->addRoute('topPopularPeopleIndex1Route', $route);


                //Top Popular business
                $route = new Zend_Controller_Router_Route('/user/search/top_popular_business',array('module' => 'search','controller' => 'index','action' => 'toppopularbusiness'));
                $router->addRoute('topPopularBusinessRoute', $route);
                
                //Top Popular People
                $route = new Zend_Controller_Router_Route('/user/search/top_popular_people',array('module' => 'search','controller' => 'index','action' => 'toppopularpeople'));
                $router->addRoute('topPopularPeopleRoute', $route);
                
                //Local People
                $route = new Zend_Controller_Router_Route('/user/search/local_people',array('module' => 'search','controller' => 'index','action' => 'localpeople'));
                $router->addRoute('localPeopleRoute', $route);
                
                //Lame People
                $route = new Zend_Controller_Router_Route('/user/search/lame_people',array('module' => 'search','controller' => 'index','action' => 'lamepeople'));
                $router->addRoute('lamePeopleRoute', $route);
                
                //Top Popular Business
                $route = new Zend_Controller_Router_Route('/user/search/top_popular_business',array('module' => 'search','controller' => 'index','action' => 'toppopularbusiness'));
                $router->addRoute('topPopularBusinessRoute', $route);
                
                //Advanced Search
                $route = new Zend_Controller_Router_Route('/user/search/advanced_search',array('module' => 'search','controller' => 'index','action' => 'advancedsearch'));
                $router->addRoute('advancedsearchRoute', $route);
                
                //User Forgot Password
                $route = new Zend_Controller_Router_Route('/user/forgotpassword',array('module' => 'user','controller' => 'index','action' => 'forgotpassword'));
                $router->addRoute('userForgotPasswordRoute', $route);
                
                //Get User Reset password for Forgot Password
                $route = new Zend_Controller_Router_Route('/user/getnewpassword',array('module' => 'mobile','controller' => 'index','action' => 'forgotpassword'));
                $router->addRoute('userGetNewForgotPasswordRoute', $route);
                
                //Business Forgot Password
                $route = new Zend_Controller_Router_Route('/business/forgotpassword',array('module' => 'business','controller' => 'index','action' => 'forgotpassword'));
                $router->addRoute('businessForgotPasswordRoute', $route);
                
                //Get Business Reset password for Forgot Password
                $route = new Zend_Controller_Router_Route('/business/getnewpassword',array('module' => 'business','controller' => 'index','action' => 'getnewpassword'));
                $router->addRoute('businessGetNewForgotPasswordRoute', $route);

                //Business Advanced Search
                $route = new Zend_Controller_Router_Route('/business/search/advanced_search',array('module' => 'search','controller' => 'index','action' => 'advancedsearch'));
                $router->addRoute('businessadvancedsearchRoute', $route);

                //Huddles
                //Users Huddles
                /*$route = new Zend_Controller_Router_Route('/huddles/viewhuddles',array('module' => 'huddles','controller' => 'index','action' => 'viewhuddles'));
                $router->addRoute('userHuddlesRoute', $route);*/

                //View Huddles
                $route = new Zend_Controller_Router_Route('/huddles/viewhuddles/:type',array('module' => 'huddles','controller' => 'index','action' => 'viewhuddles', 'type' => '', 'user_id' => ''));
                $router->addRoute('userHuddlesRoute', $route);

                // Business User Dashboard
                $route = new Zend_Controller_Router_Route('business/dashboard',array('module' => 'business','controller' => 'index','action' => 'dashboard'));
                $router->addRoute('businessUserDashboardRoute', $route);
                
                // Business User detailed analytics
                $route = new Zend_Controller_Router_Route('business/detailedanalytics',array('module' => 'business','controller' => 'index','action' => 'detailedanalytics'));
                $router->addRoute('businessUserDetailedAnalyticsRoute', $route);

                //Business User BusinessInfo Routing
                $route = new Zend_Controller_Router_Route('/business/businessinfo',array('module' => 'business','controller' => 'index','action' => 'businessinfo'));
                $router->addRoute('businessInfoRoute', $route);

                //Business User email and username validation
                $route = new Zend_Controller_Router_Route('/business/datavalidate',array('module' => 'business','controller' => 'index','action' => 'datavalidate'));
                $router->addRoute('businessDataValidateRoute', $route);

                //Business Login Routing
                $route = new Zend_Controller_Router_Route('/business/login',array('module' => 'business','controller' => 'index','action' => 'login'));
                $router->addRoute('businessLoginRoute', $route);

                //Business User Photo Routing
                $route = new Zend_Controller_Router_Route('/business/businessmedia/:photo_id/:type',array('module' => 'business','controller' => 'index','action' => 'businessmedia','photo_id' => '', 'type' => ''));
                $router->addRoute('businessPhotoRoute', $route);

                //Business User Profile Routing
                $route = new Zend_Controller_Router_Route('/business/businessprofile/:userid',array('module' => 'business','controller' => 'index','action' => 'businessprofile','userid' => ''));
                $router->addRoute('businessProfileRoute', $route);

                //Business User MySettings Routing
                $route = new Zend_Controller_Router_Route('/business/businessmysettings/',array('module' => 'business','controller' => 'index','action' => 'businessmysettings'));
                $router->addRoute('businessMysettingsRoute', $route);

                //Business User MyCustomers Routing
                $route = new Zend_Controller_Router_Route('/business/mycustomers/:userid',array('module' => 'business','controller' => 'index','action' => 'mycustomers','userid' => ''));
                $router->addRoute('businessMycustomersRoute', $route);

                //Business User MyUppers Routing
                $route = new Zend_Controller_Router_Route('/business/myuppers/:userid',array('module' => 'business','controller' => 'index','action' => 'myuppers','userid' => ''));
                $router->addRoute('businessMyuppersRoute', $route);

                //Business User MyBusiness Routing
                $route = new Zend_Controller_Router_Route('/business/mybusiness/:userid',array('module' => 'business','controller' => 'index','action' => 'mybusiness','userid' => ''));
                $router->addRoute('businessMybusinessRoute', $route);

                //Business User MyFollowers Routing
                $route = new Zend_Controller_Router_Route('/business/myfollowers/:userid',array('module' => 'business','controller' => 'index','action' => 'myfollowers','userid' => ''));
                $router->addRoute('businessMyfollowersRoute', $route);

                //Business User Coupon Routing
                $route = new Zend_Controller_Router_Route('/business/businesscoupon/:couponid',array('module' => 'business','controller' => 'index','action' => 'businesscoupon','couponid' => ''));
                $router->addRoute('businessCouponRoute', $route);

                //Business User Help Routing
                $route = new Zend_Controller_Router_Route('/business/help/:userid',array('module' => 'business','controller' => 'index','action' => 'help','userid' => ''));
                $router->addRoute('businessHelpRoute', $route);

                //Business User Technical SUpport Routing
                $route = new Zend_Controller_Router_Route('/business/techsupport/:userid',array('module' => 'business','controller' => 'index','action' => 'techsupport','userid' => ''));
                $router->addRoute('businessTechsupportRoute', $route);

                //Business Huddles Routing
                $route = new Zend_Controller_Router_Route('/business/businesshuddles/:business_id',array('module' => 'business','controller' => 'index','action' => 'businesshuddles','business_id' => ''));
                $router->addRoute('businessHuddlesRoute', $route);

                //Business User View Followers Routing
                $route = new Zend_Controller_Router_Route('/business/viewfollowers/',array('module' => 'business','controller' => 'index','action' => 'viewfollowers'));
                $router->addRoute('businessViewfollowersRoute', $route);

                //Business User View Following Routing
                $route = new Zend_Controller_Router_Route('/business/viewfollowing/',array('module' => 'business','controller' => 'index','action' => 'viewfollowing'));
                $router->addRoute('businessViewfollowingRoute', $route);

                //Business User Make Friends Routing
                $route = new Zend_Controller_Router_Route('/business/makefriends/',array('module' => 'business','controller' => 'index','action' => 'makefriends'));
                $router->addRoute('businessMakefriendsRoute', $route);

                //Business User FOllow through QRCODE Routing
                $route = new Zend_Controller_Router_Route('/business/followbusiness/:qrcode/:logged_user_id/:logged_user_type',array('module' => 'business','controller' => 'index','action' => 'followbusiness'));
                $router->addRoute('businessQRCodeFollowRoute', $route);
                
                //Business User UnMake Friends Routing
                $route = new Zend_Controller_Router_Route('/business/unmakefriends/',array('module' => 'business','controller' => 'index','action' => 'unmakefriends'));
                $router->addRoute('businessUnmakefriendsRoute', $route);
                
                //Business User Photos Routing
                $route = new Zend_Controller_Router_Route('/business/businessphotos/',array('module' => 'business','controller' => 'index','action' => 'businessphotos'));
                $router->addRoute('businessPhotosRoute', $route);
                
                //Business User Photos Routing
                $route = new Zend_Controller_Router_Route('/business/businessphotos/:business_id',array('module' => 'business','controller' => 'index','action' => 'businessphotos','business_id'=>'business_id'));
                $router->addRoute('businessPhotoswithIDRoute', $route);
                
                //Business User Map Routing
                $route = new Zend_Controller_Router_Route('/business/businessmap/',array('module' => 'business','controller' => 'index','action' => 'businessmap'));
                $router->addRoute('businessMapRoute', $route);
                
                //Business User Map Routing
                $route = new Zend_Controller_Router_Route('/business/businessmap/:business_id',array('module' => 'business','controller' => 'index','action' => 'businessmap','business_id'=>'business_id'));
                $router->addRoute('businessMapwithIDRoute', $route);

                //Business User Photo Edit Routing
                $route = new Zend_Controller_Router_Route('/business/profilepictureedit/',array('module' => 'business','controller' => 'index','action' => 'profilepictureedit'));
                $router->addRoute('businessPhotoEditRoute', $route);

                $route = new Zend_Controller_Router_Route('/business/profileinfo',array('module' => 'business','controller' => 'index','action' => 'profileinfo'));
                $router->addRoute('businessProfileInfoRoute', $route);

                $route = new Zend_Controller_Router_Route('/business/findfriends',array('module' => 'business','controller' => 'index','action' => 'findfriends'));
                $router->addRoute('businessfindfriendsRoute', $route);

                $route = new Zend_Controller_Router_Route('/business/profilepicture',array('module' => 'business','controller' => 'index','action' => 'profilepicture'));
                $router->addRoute('businessProfilePictureRoute', $route);

                $route = new Zend_Controller_Router_Route('/business/createcoupon',array('module' => 'business','controller' => 'index','action' => 'createcoupon'));
                $router->addRoute('businessCreateCouponRoute', $route);

                //Business Logout Routing
                $route = new Zend_Controller_Router_Route('/business/logout',array('module' => 'business','controller' => 'index','action' => 'logout'));
                $router->addRoute('businessLogoutRoute', $route);

                // TO SAVE Edit Businessdata
                $route = new Zend_Controller_Router_Route('/business/editdata',array('module' => 'business','controller' => 'index','action' => 'editdata'));
                $router->addRoute('businesseEditRoute', $route);

                // Business Transaction Reports
                $route = new Zend_Controller_Router_Route('/business/transactionreports',array('module' => 'business','controller' => 'index','action' => 'transactionreports'));
                $router->addRoute('businessTransactionReportsRoute', $route);
                
                // TO Get States
                $route = new Zend_Controller_Router_Route('/user/getstates',array('module' => 'user','controller' => 'ajax','action' => 'getstates'));
                $router->addRoute('businessGetStatesRoute', $route);

                // TO Get Cities
                $route = new Zend_Controller_Router_Route('/user/getcities',array('module' => 'user','controller' => 'ajax','action' => 'getcities'));
                $router->addRoute('businessGetCitiesRoute', $route);
                
                // TO Get Zipcodes
                $route = new Zend_Controller_Router_Route('/user/getzipcodes',array('module' => 'user','controller' => 'ajax','action' => 'getzipcodes'));
                $router->addRoute('GetZipcodesRoute', $route);
                
                // Popst REview Route
                $route = new Zend_Controller_Router_Route('/reviews/postreview',array('module' => 'business','controller' => 'reviews','action' => 'postreview'));
                $router->addRoute('postReviewRoute', $route);
                
                // Reply  REview Route
                $route = new Zend_Controller_Router_Route('/reviews/replyreview',array('module' => 'business','controller' => 'reviews','action' => 'replyreview'));
                $router->addRoute('replyReviewRoute', $route);
                
                // TO Get Notifications
                $route = new Zend_Controller_Router_Route('/notifications/getnotificationswithid',array('module' => 'notifications','controller' => 'index','action' => 'getnotificationswithid'));
                $router->addRoute('notificationsGetnotificationswithidRoute', $route);
                
                // TO SET Crons for Tomorrow Expired Uppers
                $route = new Zend_Controller_Router_Route('/crons/gettomorrowexpireduppers',array('module' => 'crons','controller' => 'index','action' => 'gettomorrowexpireduppers'));
                $router->addRoute('cronsGettomorrowexpireduppersRoute', $route);
                
                //To set Crons For Expired Uppers
                $route = new Zend_Controller_Router_Route('/crons/getexpireduppers',array('module' => 'crons','controller' => 'index','action' => 'gettomorrowexpireduppers'));
                $router->addRoute('cronsGetexpireduppersRoute', $route);
                
                // TO get All Scribble Blasted USers
                $route = new Zend_Controller_Router_Route('/user/getallscribbleblastedusers',array('module' => 'scribbles','controller' => 'index','action' => 'getallscribbleblastedusers'));
                $router->addRoute('getAllScribbleBlastedUserswRoute', $route);
                
                // TO get All Photo Blasted USers
                $route = new Zend_Controller_Router_Route('/user/getallphotoblastedusers',array('module' => 'scribbles','controller' => 'index','action' => 'getallphotoblastedusers'));
                $router->addRoute('getAllPhotoBlastedUsersRoute', $route);
        }
        
         protected function _initLayoutHelper()
        {

                $this->bootstrap('frontController');
                $layout = Zend_Controller_Action_HelperBroker::addHelper(new UpmeSocial_Controller_Action_Helper_LayoutLoader());
                $options = $this->getOptions(); 
                $this->getApplication()->setAutoloaderNamespaces(array($options["appnamespace"])); 
        }

		protected function _initDatabase(){
			$resource = $this->getPluginResource('multidb');
			$resource->init();
			Zend_Registry::set('db1',$resource->getDb('db1'));
			Zend_Registry::set('db2',$resource->getDb('db2'));

		}
}
